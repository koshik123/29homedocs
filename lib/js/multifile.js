var imgCount = 1;
var current_imgs = 1;

// Attach More Images

$(document).on('click', '.attach_more_image', function() {
    if (current_imgs < 5) {
        if ($('#img' + current_imgs).length) {
            $(".errmsgs").hide();
            var new_count = current_imgs + 1;
            $('#box' + new_count).show();
            $("#rmv_multi_" + current_imgs).remove();
            current_imgs++;
        } else {
            $(".errmsgs").show();
            $(".errmsgs").html("Upload all Images to add New Image");
        }
    } else {
        $(".errmsgs").show();
        $(".errmsgs").html("You can upload only 5 Images");
    }

});

$('.img_button').click(function() {
    var option = $(this).data("option");
    $("#loader_" + option).show();
    setTimeout(function() {
        $("#loader_" + option).hide();
    }, 3000);
});

// Attach File

$('.filename').change(function() {
    $("#image_attach_wrap").show();
    var option = $(this).data("option");
    $("#loader_" + option).show();
    var file = this.files[0];
    var id = this.id;
    var reader = new FileReader();
    reader.onload = function(event) {
        var i = document.createElement('img');
        i.src = event.target.result;
        i.id = 'img' + id;
        i.onload = function() {
            image_width = $(i).width();
            image_height = $(i).height();

            if (image_width > image_height) {
                i.style.width = "100%";
            } else {
                i.style.height = "auto";
            }
            i.style.display = "block";
        }
        $('#img' + id).remove();
        /* $("#loader_" + option).hide();*/
        $('#box' + id).append(i);
        $('#box' + id).prepend('<button type="button" id="rmv_multi_' + option + '" data-option="' + option + '" class="label_img"><i class="icon ni ni-cross-sm"></i></button>');
        $('#btn' + id).hide();
        $("#img_button_dummy").show();
        $(".errmsgs").hide();

        if (current_imgs == 1) {
            var new_count = current_imgs + 1;
            $('#box' + new_count).show();
            current_imgs++;
        } else {
            var previous = current_imgs - 1;
            if ($('#img' + previous).length) {
                var new_count = current_imgs + 1;
                $('#box' + new_count).show();
                current_imgs++;
            }
        }
        $(document).on('click', '#img' + id, function() { $('#' + id).trigger('click') });
    };
    reader.onerror = function(event) {
        console.error("File could not be read! Code " + event.target.error.code);
    };
    reader.readAsDataURL(file);
    setTimeout(function() {
        $("#loader_" + option).hide();
    }, 3000);
});

// Remove the Image Thumb

$(document).on('click', '.label_img', function() {
    var option = $(this).data("option");
    $('#img' + option).remove();
    $('#btn' + option).show();
    $(this).remove();
});
