
function closeWin() {
    myWindow.close();   
}

$(document).on('click','.close',function() {
  $(".alert").fadeOut(500); 
   return false;   
 });


$(document).on('click','.historyback',function() {
  window.history.back();
 });

 $(".close_popup").click(function() {
    $("#pop_up_window").hide();
    $(".pop_bg_wrap").hide();
    $(".popup_bg").hide();
    $("#popcontent").html("");
 });

 $("#close_home_popup").click(function() {
    $(".home_popup_wrap").hide();
    $(".popup_bg").hide();
 });



$(document).ready(function() {

/*----------------------------
        Login Functions
 ------------------------------*/

// User Login

$("#login_auth").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
        }
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
        password: {
            required: "Please Enter your Password",
        }
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=userLogin",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = base_path;
                    //location.reload();
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

$("#changePassword").validate({
    rules: {
        password: {
            required: true
        },
        new_password: {
            required: true,
        },
        re_password: {
            required: true,
            equalTo: "#new_password"
        }
    },
    messages: {
        password: {
            required: "Please Enter your Password",
        },
        new_password: {
            required: "Please Enter New Password",
        },
        re_password: {
            required: "Please Enter your New Password Again",
        }
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=changePassword",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = base_path + "profile/changepassword?a=success";
                    //location.reload();
                } else {
                    $(".form-error").html(data);
                    $(".form-error").show();
                }
            }
        });
        return false;
    }
});


// Edit Profile 

$("#editProfile").validate({
    rules: {
        name: {
            required: true

        },        
        mobile: {
            required: true,
            digits: true
        },
        email: {
            required: true
        },   
    },
    messages: {
        name: {
            required: "Please Enter Admin Name",
        },       
        mobile: {
            required: "Please Enter Mobile Number",
        },
        email: {
            required: "Please Enter Email Address",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=editProfile",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = base_path + "profile?p=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Forgot Password

$("#forgot_password").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=forgot_password",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                $(".form-error").html(data);
            }
        });
        return false;
    }
});

// Reset Password

$("#resetpass").validate({
    rules: {
        new_pass: {
            required: true,
        },
        confirm_pass: {
            required: true,
            equalTo: "#newpassword"
        }
    },
    messages: {
        new_pass: {
            required: "Please Enter Your New Password",
        },
        confirm_pass: {
            required: "Please Enter Your Confirm Password",
        }
    },
    submitHandler: function(form) {
        var newpassword = $("#newpassword").val();
        var token = $("#token").val();
        var value = token + "`" + newpassword;
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=resetPassword",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = base_path + "?reset=success";
                } else {
                    $(".form-error").html(data);
                }

            }
        });
        return false;
    }
});


/*=====================================
        General Documents
=======================================*/

    // View Info 

    $(document).on('click', '.viewGenDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=viewGenDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".genDocInfo").html(response['layout']);
                $('.genDocModel').modal('show');
            }
        })
        return false;
    });


    $(document).on('click', '.raiseAdhocRequest', function() {
        var id = $(this).data("option");
        var value = id ;
        if (value!=''){
            $('.adhocModel').modal('show');
            $('.genDocModel').modal('hide');
            $('.privateDocModel').modal('hide');
        }else{
            $('.adhocModel').modal('hide');
            $('.genDocModel').modal('show');
            $('.privateDocModel').modal('show');
        }   
        return false;
    });

    $(document).on('click', '.galleryraiseAdhocRequest', function() {
            $('.raiseAdhocRequest').modal('show');
           
    });

   /* $(document).on('click', '.raiseAdhocRequest', function() {
        var id = $(this).data("option");
        
    });*/

    /*=====================================
        Private Documents
    =======================================*/

    // View Info 

    $(document).on('click', '.viewPrivateDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=viewPrivateDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".privateDocInfo").html(response['layout']);
                $('.privateDocModel').modal('show');
            }
        })
        return false;
    });

    /*=====================================
        Private Common Documents
    =======================================*/

    // View Info 

    $(document).on('click', '.viewPrivateCommonDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=viewPrivateCommonDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".privateDocInfo").html(response['layout']);
                $('.privateDocModel').modal('show');
            }
        })
        return false;
    });

    /*=====================================
            Adhoc requests
    =======================================*/

    $("form[name='addAdhocRequests']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: base_path + "resource/ajax_adhoc_multiple_file.php?page=addticket",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "adhocrequests?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });
    

    // Add Adhoc Request

    $("#addAdhocRequests_old_1").validate({
        rules: {
            request_id: {
                required: true,
            },
             subject: {
                required: true,
            },
             remarks: {
                required: true,
            },
        },
        messages: {
            request_id: {
                required: "Please Select Request Type",
            },
            subject: {
                required: "Please Enter your Subject",
            },
            remarks: {
                required: "Please Enter your Message",
            },
        },
        submitHandler: function(form) {
            var form_content = $("#addAdhocRequests").serializeArray();
            var img_data = [];
            for (var i = 1; i <= 10; i++) {
                if (document.getElementById('img' + i)) {
                    var maxWidth = 1400;
                    var maxHeight = 900;
                    var source_img_obj = document.getElementById('img' + i);
                    var cvs = document.createElement('canvas');
                    var width = source_img_obj.naturalWidth;
                    var height = source_img_obj.naturalHeight;

                    if ((width > maxWidth) || height > maxHeight) {
                        if (width > maxWidth)
                            ratio = maxWidth / width;
                        else if (height > maxHeight)
                            ratio = maxHeight / height;
                    } else {
                        ratio = 1;
                    }

                    cvs.width = width * ratio;
                    cvs.height = height * ratio;

                    /*if (source_img_obj.naturalWidth > 4000 || source_img_obj.naturalHeight > 4000) {
                        var quality = 0.25;
                    } else if (source_img_obj.naturalWidth > 2000 || source_img_obj.naturalHeight > 2000) {
                        var quality = 0.5;
                    } else {
                        var quality = 0.5;
                    }*/
                    var quality = 0.7;
                    var ctx = cvs.getContext("2d").drawImage(source_img_obj, 0, 0, cvs.width, cvs.height);
                    var newImageData = cvs.toDataURL("image/jpeg", quality);
                    var img_name = $('#btn' + i).val();
                    img_data.push({ index: i, name: img_name, image_data: newImageData });
                }
            }
            var flag = true;
            //alert(form_content);
            if (flag) {
                $.ajax({
                    url: base_path + 'resource/post_on_wall.php?page=addAdhocRequests',
                    type: 'POST',
                    data: { post_data: img_data, content: form_content },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  base_path + "adhocrequests?a=success";
                        } else {
                            $(".errmsgs").show();
                            $(".errmsgs").html(data);
                        }
                    }
                });
            }
            
            return false;
        }
    });


    // Add Adhoc Request

    $("#addAdhocRequest_old").validate({
        rules: {
            request_id: {
                required: true,
            },
             subject: {
                required: true,
            },
             remarks: {
                required: true,
            },
        },
        messages: {
            request_id: {
                required: "Please Select Request Type",
            },
            subject: {
                required: "Please Enter your Subject",
            },
            remarks: {
                required: "Please Enter your Message",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: base_path + "resource/ajax_redirect.php?page=addAdhocRequest",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "adhocrequests?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });


     // Adhoc Request

    $("#replayAdhocRequest_old").validate({
        rules: {
            remarks: {
                required: true,
            },
            
        },
        messages: {
            remarks: {
                required: "Please Enter your message",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: base_path + "resource/ajax_redirect.php?page=replayAdhocRequest",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "adhocrequests?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Add Adhoc Request

    $("form[name='replayAdhocRequest']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: base_path + "resource/ajax_adhocreply_multiple_file.php?page=replayAdhocRequest",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "adhocrequests?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });


    $("#replayAdhocRequest_old").validate({
        rules: {
            remarks: {
                required: true,
            },
            
        },
        messages: {
            remarks: {
                required: "Please Enter your message",
            },
        },
        submitHandler: function(form) {
            var form_content = $("#replayAdhocRequest").serializeArray();
            var img_data = [];
            for (var i = 1; i <= 10; i++) {
                if (document.getElementById('img' + i)) {
                    var maxWidth = 1400;
                    var maxHeight = 900;
                    var source_img_obj = document.getElementById('img' + i);
                    var cvs = document.createElement('canvas');
                    var width = source_img_obj.naturalWidth;
                    var height = source_img_obj.naturalHeight;

                    if ((width > maxWidth) || height > maxHeight) {
                        if (width > maxWidth)
                            ratio = maxWidth / width;
                        else if (height > maxHeight)
                            ratio = maxHeight / height;
                    } else {
                        ratio = 1;
                    }

                    cvs.width = width * ratio;
                    cvs.height = height * ratio;

                    /*if (source_img_obj.naturalWidth > 4000 || source_img_obj.naturalHeight > 4000) {
                        var quality = 0.25;
                    } else if (source_img_obj.naturalWidth > 2000 || source_img_obj.naturalHeight > 2000) {
                        var quality = 0.5;
                    } else {
                        var quality = 0.5;
                    }*/
                    var quality = 0.7;
                    var ctx = cvs.getContext("2d").drawImage(source_img_obj, 0, 0, cvs.width, cvs.height);
                    var newImageData = cvs.toDataURL("image/jpeg", quality);
                    var img_name = $('#btn' + i).val();
                    img_data.push({ index: i, name: img_name, image_data: newImageData });
                }
            }
            var flag = true;
            //alert(form_content);
            if (flag) {
                $.ajax({
                    url: base_path + 'resource/post_on_wall.php?page=replayAdhocRequest',
                    type: 'POST',
                    data: { post_data: img_data, content: form_content },
                    beforeSend: function() {
                        //$(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  base_path + "adhocrequests?a=success";
                        } else {
                            $(".errmsgs").show();
                            $(".errmsgs").html(data);
                        }
                    }
                });
            }
            
            return false;
        }
    });




    // Close Ticket

    $(".closeTicket").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=closeTicket",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });
    

     /*=====================================
           General Documents Adhoc Request
    =======================================*/

    $("form[name='generalDocRequests']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: base_path + "resource/ajax_adhocdoc_multiple_file.php?page=generalDocRequests",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "adhocrequests?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });

    /*=====================================
              Manage KYC
    =======================================*/

    $("form[name='addKyc']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: base_path + "resource/ajax_kyc_multiple_file.php?page=addKyc",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = base_path + "kyc/application?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });

    // Delete KYC Attachment Files

    $(document).on("click", ".deletKYCAttachmentImage", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Documents ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: base_path + "resource/ajax_redirect.php?page=deletKYCAttachmentImage",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {

                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "File has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Speaker retained", "error");
            }
        });
        return false;
    });

    // Edit KYC Attachment images  

    $(document).on('click', '.editKYCAttachmentImage', function() {
        var id = $(this).data("option");
        var sno = $(this).data("type");
        var value = id + "`" + sno ;
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=editKYCAttachmentImage",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".edituploadfileModel").html(response['layout']);
                $('.edituploadfile').modal('show');
            }
        })
        return false;
    });

    // update Multiple files Attachment

    $("#updateKYCFiles").validate({
        rules: {
            image: {
                required: true
            }
        },
        messages: {
            image: {
                required: "Please Select File",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('updateDocumentsFiles');
            var formData = new FormData(formname);
            var option = $("#option").val();
            $(".page_loading").show();
            var content = $(form).serialize();
            $.ajax({
                url: base_path + "resource/ajax_propertydocument_file.php",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    //alert(data);
                    data = data.split('`');
                    if (data[0] == 1) {
                        $("#customerImage").val(data[1]);
                        if (data[0] == 1) {
                            var content = $(form).serialize();
                            $.ajax({
                                type: "POST",
                                url: base_path + "resource/ajax_redirect.php?page=updateDocumentsFiles",
                                dataType: "html",
                                data: content,
                                beforeSend: function() {
                                    $(".page_loading").show();
                                },
                                success: function(data) {
                                    //alert(data);
                                    $(".page_loading").hide();
                                    data = data.split('`');
                                    if (data['0'] == 1) {
                                        $('.edituploadfile').modal('hide');
                                        // $("#agendamentrow_" + option).replaceWith(data['1']);
                                        location.reload();
                                        return true;
                                    } else {
                                        $(".perror").html(data['1']);
                                    }
                                    return false;
                                }
                            });
                        }
                    } else {
                        $(".imgerr").html(data[1]);
                        return false;
                    }
                }
            });

            return false;
        }
    });

     // Delete Flat Type Master

    $(document).on("click", ".removeKYCImg", function() {
        var id = $(this).data("option");
        swal({
            title: "Are sure you to delete this File ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: base_path + "resource/ajax_redirect.php?page=removeKYCImg",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            //$("#attacjmentrow_" + id).remove();
                            //swal("Deleted!", "Infomation has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "File retained", "error");
            }
        });
        return false;
    });

    
    
    $(".selectProperty").click(function () {
        var value = $(this).data("option");
       // alert(value);
        $.ajax({
            type: "POST",
            url: base_path + "resource/ajax_redirect.php?page=selectProperty",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
              //  alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                     window.location = base_path ;
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    });

    //--- don't delete--//

});


