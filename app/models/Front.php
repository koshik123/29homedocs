<?php

require_once 'Model.php';
require_once 'app/core/classes/PHPMailerAutoload.php';

class Front extends Model
{
	
	/*--------------------------------------------- 
					User Info
	----------------------------------------------*/

	// General User info for all pages

	function userInfo($id)
	{
		$today = date("Y-m-d");
		if(isset($_SESSION["crm_property_id"])){
			$property_id = $_SESSION["crm_property_id"];
			$query = "SELECT C.id,(C.name) as username,C.gender,C.mobile,C.email,C.address,C.city ,C.state,C.pincode, (SELECT title as tot FROM ".PROPERTY_TBL." WHERE id=$property_id ) as title FROM ".CUSTOMER_TBL." C  WHERE C.id ='".$id."' ";
		}else{
			$query = "SELECT C.id,(C.name) as username,C.gender,C.mobile,C.email,C.address,C.city ,C.state,C.pincode FROM ".CUSTOMER_TBL." C  WHERE C.id ='".$id."' ";
		}
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	function PopupCheck()
	{
		$layout ="";
		$today = date("Y-m-d");
		$query = "SELECT id,customer_ids  FROM ".NEWS_TBL." WHERE show_popup='1' AND deleted_status='0' AND popup_till>='$today' ORDER BY id DESC LIMIT 1 ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$popup = $this->getPopupCheck($list['customer_ids']);
				$layout.= "$popup";
			}
		}
		return $layout;
	}

	function getPopupCheck($customer_ids)
  	{

  		$layout = "";
		$dates_array = explode(",", $customer_ids);
		foreach ($dates_array as $value) {
			$q = "SELECT id FROM ".CUSTOMER_TBL." WHERE id='$value' ";
			$query = $this->selectQuery($q);
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
				$list 			= $this->editPagePublish($details);
				$checkvalue 			= $list['id']==$_SESSION["crm_member_id"] ? 1 : 0 ;

	            $layout .= "$checkvalue";
		    		$i++;
		    	}
		    }else{
		    	$layout .= "0";
		    }
		}
	    return $layout;
  	}


	function menuInfo()
	{
		$today = date("Y-m-d");
		$query = "SELECT id,menu,tour_code FROM ".MENU_SETTINGS."  WHERE 1 ";
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	function getPropertyInfo($property_id)
	{
		$today = date("Y-m-d");
		$query = "SELECT  P.token,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.room_id,P.block_id,P.floor_id,P.address,P.city,P.state,P.pincode,P.content,P.image,P.status,P.added_by,P.created_at,B.block,F.floor,E.name,FL.flat_variant   FROM ".PROPERTY_TBL." P LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=P.added_by) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) WHERE P.id ='".$property_id."'  ";
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}


	// Select Property
	
	function selectReportProperty()
	{
  		$layout = "";
  		$customer_id = $_SESSION["crm_member_id"];
	    $q = "SELECT * FROM ".PROPERTY_TBL." WHERE customer_id='$customer_id' AND deleted_status='0' AND status='1' AND cancel_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
	    			<li><a href='javascript:void();' class='selectProperty' data-option='".($list['id'])."'>".$list['title']." </a></li>
	    		";
	    		$i++;
	    		/*<li><a href='".BASEPATH."home/branches/".$list['id']."'>".$list['branch_name'].", ".$list['branch_location']." </a></li>*/
	    		
	    	}
	    }
	    return $layout;
	}

	/*--------------------------------------------- 
			 	General Settings
	----------------------------------------------*/

	
	function getRequestTypeList($current="")
	{
		$layout ="";
		$query = "SELECT id,token,request_type,status  FROM ".REQUEST_TYPE_TBL." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['request_type'])."</option>";
			}
		}
		return $layout;
	}

	


	/*--------------------------------------------- 
			Manage Brochure
	----------------------------------------------*/

	function manageBrochure()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT B.id,B.token,B.title,B.property_id,B.description,B.brochures,B.created_at,B.added_by,E.name  FROM ".PROPERTY_BROUCHERS." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE B.property_id='$property_id' AND  B.deleted_status='0'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    	
	    		$layout .= "
	    			<li class='sp-pdl-item'>
		                <div class='sp-pdl-desc'>		                    
		                    <div class='sp-pdl-info'>
		                        <h6 class='sp-pdl-title'><span class='sp-pdl-name'>".ucwords($list['title'])."</span> </h6>
		                        <div class='sp-pdl-meta'>
		                            
		                            <span class='release'>
		                                <span class='text-soft'>Release Date: </span> <span>".date("d M Y",strtotime($list['created_at']))."</span>
		                            </span>
		                        </div>
		                    </div>
		                </div>
		                <div class='sp-pdl-btn'>
		                    <a target='_blank' href='".DOCUMENTS."".$list['brochures']."' download='".DOCUMENTS."".$list['brochures']."' class='btn btn-primary'>Download</a>
		                </div>
		            </li>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage Bank Info
	----------------------------------------------*/

	function manageBankInfo()
  	{
  		$layout = "";
  		//$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT  P.id,P.name,P.account_number,P.bank,P.ifcs_code,P.branch,P.city,P.state,P.pincode,P.status,P.added_by,P.created_at,E.name FROM ".PAYMENT_INFO." P LEFT JOIN ".EMPLOYEE." E ON (E.id=P.added_by) WHERE P.status='1' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    	
	    		$layout .= "
	    			<div class='col-md-4'>
                    <div class='support-topic-item card card-bordered'>
                        <a class='support-topic-block card-inner' href='javascript:void();'>
                            <div class='support-topic-context'>
                                <h5 class='support-topic-title title'>".ucwords($list['bank'])."</h5>
                                <div class='support-topic-info'>Acc No: ".($list['account_number'])."</div>
                                <div class='support-topic-info'>Acc Name: ".ucwords($list['name'])."</div>
                                <div class='support-topic-info'>IFSC Code: ".($list['ifcs_code'])."</div>
                                <div class='support-topic-info'>Branch: ".ucwords($list['city'])."</div>
                                
                            </div>
                           
                        </a>
                    </div>
                </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage Contact Info
	----------------------------------------------*/

	function manageContactInfo()
  	{
  		$layout = "";
  		//$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT C.id,C.name,C.role,C.email,C.mobile ,C.status,C.added_by,C.created_at  FROM ".CONTACT_INFO." C LEFT JOIN ".EMPLOYEE." E ON (E.id=C.added_by) WHERE C.status='1' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<div class='col-md-4'>
                    <div class='support-topic-item card card-bordered'>
                        <a class='support-topic-block card-inner' href='javascript:void();'>
                            <div class='support-topic-context'>
                                <h5 class='support-topic-title title'>".ucwords($list['name'])."</h5>
                                <div class='support-topic-info'>Role: ".ucwords($list['role'])."</div>
                                <div class='support-topic-info'>Mobile: ".($list['mobile'])."</div>
                                <div class='support-topic-info'>Email: ".($list['email'])."</div>
                            </div>
                        </a>
                    </div>
                </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage General Documents
	----------------------------------------------*/

	function managegeneraldocuments()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.token,G.title,G.description,G.documents,G.document_type,G.added_by,G.status,G.created_at  FROM ".GENDRAL_DOCUMENTS." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.status='1' AND G.deleted_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$type_icon   = $this->getFileIcon($list['document_type']);
	    		$layout .= "<a href='javascript:void();' class='viewGenDocInfo' data-value='".$i."' data-option='".$list['id']."'>
			    				<div class='nk-file-item nk-file'>
		                            <div class='nk-file-info'>
		                                <div class='nk-file-title'>
		                                    ".$type_icon."
		                                    <div class='nk-file-name'>
		                                        <div class='nk-file-name-text'>
		                                            <a href='javascript:void();' class='title viewGenDocInfo' data-value='".$i."' data-option='".$list['id']."'>".ucwords($list['title'])."</a>
		                                            
		                                        </div>
		                                    </div>
		                                </div>
		                                
		                            </div>
		                        </div>
                        </a>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getFileIcon($value)
  	{
  		$type_icon = "";
  		if ($value=="doc" || $value=="doc") {
	    			$type_icon .= "<div class='nk-file-icon'>
                                    <span class='nk-file-icon-type'>
                                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                            <g>
                                                <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#599def'></path>
                                                <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#c2e1ff'></path>
                                                <rect x='27' y='31' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='36' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='41' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='46' width='12' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                            </g>
                                        </svg>
                                    </span>
                                </div>";
		}elseif($value=="dot"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                    <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#7e95c4'></path>
                                    <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#b7ccea'></path>
                                    <rect x='27' y='31' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='35' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='39' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='43' width='14' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='47' width='8' height='2' rx='1' ry='1' style='fill:#fff'></rect></svg>
                            </span>
                        </div>";
		}elseif($value=="pdf"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 90 90'><rect x='15' y='5' width='56' height='70' rx='6' ry='6' fill='#e3e7fe' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><path d='M69.88,85H30.12A6.06,6.06,0,0,1,24,79V21a6.06,6.06,0,0,1,6.12-6H59.66L76,30.47V79A6.06,6.06,0,0,1,69.88,85Z' fill='#fff' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><polyline points='60 16 60 31 75 31.07' fill='none' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><path d='M57.16,60.13c-.77,0-1.53,0-2.28.08l-.82.07c-.28-.31-.55-.63-.81-1a32.06,32.06,0,0,1-4.11-6.94,28.83,28.83,0,0,0,.67-3.72,16.59,16.59,0,0,0-.49-7.29c-.29-.78-1-1.72-1.94-1.25S46.08,42.2,46,43.28a11,11,0,0,0,.12,2.63,20.88,20.88,0,0,0,.61,2.51c.23.76.49,1.51.77,2.25-.18.59-.37,1.16-.56,1.72-.46,1.28-1,2.49-1.43,3.65l-.74,1.7C44,59.52,43.18,61.26,42.25,63a27.25,27.25,0,0,0-5.72,2.85,12.36,12.36,0,0,0-2.26,2A4.33,4.33,0,0,0,33,70.24a1.62,1.62,0,0,0,.59,1.39,2.36,2.36,0,0,0,2,.27c2.19-.48,3.86-2.48,5.29-4.15a46.09,46.09,0,0,0,3.27-4.41h0a47.26,47.26,0,0,1,6.51-1.63c1.06-.18,2.15-.34,3.26-.44a15.74,15.74,0,0,0,2.54,2.07,11.65,11.65,0,0,0,2.28,1.16,15.78,15.78,0,0,0,2.45.65,7,7,0,0,0,1.3.07c1,0,2.4-.44,2.49-1.71a1.93,1.93,0,0,0-.2-1C64,61,61.33,60.55,60.1,60.34A17,17,0,0,0,57.16,60.13Zm-16,4.68c-.47.75-.91,1.44-1.33,2-1,1.48-2.2,3.25-3.9,3.91a3,3,0,0,1-1.2.22c-.4,0-.79-.21-.77-.69a2,2,0,0,1,.3-.89,5,5,0,0,1,.7-1,11.3,11.3,0,0,1,2.08-1.79,24.17,24.17,0,0,1,4.4-2.33C41.36,64.49,41.27,64.65,41.18,64.81ZM47,45.76a9.07,9.07,0,0,1-.07-2.38,6.73,6.73,0,0,1,.22-1.12c.1-.3.29-1,.61-1.13.51-.15.67,1,.73,1.36a15.91,15.91,0,0,1-.36,5.87c-.06.3-.13.59-.21.88-.12-.36-.24-.73-.35-1.09A19.24,19.24,0,0,1,47,45.76Zm3.55,15A46.66,46.66,0,0,0,45,62a14.87,14.87,0,0,0,1.38-2.39,29.68,29.68,0,0,0,2.42-5.92,28.69,28.69,0,0,0,3.87,6.15l.43.51C52.22,60.48,51.36,60.6,50.52,60.74Zm13.15,2.64c-.07.41-.89.65-1.27.71A6.84,6.84,0,0,1,59,63.74a10,10,0,0,1-2.15-1.06,12.35,12.35,0,0,1-1.9-1.51c.73,0,1.47-.07,2.21-.06a18.42,18.42,0,0,1,2.23.15,7.6,7.6,0,0,1,4,1.63C63.63,63.07,63.7,63.24,63.67,63.38Z' fill='#6576ff'/></svg>
                            </span>
                        </div>";
		}elseif($value=="jpeg" || $value=="jpg" || $value=="png" || $value=="JPG" || $value=="JPEG" || $value=="PNG"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                    <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#755de0'></path>
                                    <path d='M27.2223,43H44.7086s2.325-.2815.7357-1.897l-5.6034-5.4985s-1.5115-1.7913-3.3357.7933L33.56,40.4707a.6887.6887,0,0,1-1.0186.0486l-1.9-1.6393s-1.3291-1.5866-2.4758,0c-.6561.9079-2.0261,2.8489-2.0261,2.8489S25.4268,43,27.2223,43Z' style='fill:#fff'></path>
                                    <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#b5b3ff'></path></svg>
                            </span>
                        </div>";
		}elseif($value=="xlsx"){
			$type_icon  .= "<div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                        <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#36c684' />
                                        <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#95e5bd' />
                                        <path d='M42,31H30a3.0033,3.0033,0,0,0-3,3V45a3.0033,3.0033,0,0,0,3,3H42a3.0033,3.0033,0,0,0,3-3V34A3.0033,3.0033,0,0,0,42,31ZM29,38h6v3H29Zm8,0h6v3H37Zm6-4v2H37V33h5A1.001,1.001,0,0,1,43,34ZM30,33h5v3H29V34A1.001,1.001,0,0,1,30,33ZM29,45V43h6v3H30A1.001,1.001,0,0,1,29,45Zm13,1H37V43h6v2A1.001,1.001,0,0,1,42,46Z' style='fill:#fff' /></svg>
                                </span>
                            </div>";
        }else{
        	$type_icon   .= "<div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                        <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#36c684' />
                                        <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#95e5bd' />
                                        <path d='M42,31H30a3.0033,3.0033,0,0,0-3,3V45a3.0033,3.0033,0,0,0,3,3H42a3.0033,3.0033,0,0,0,3-3V34A3.0033,3.0033,0,0,0,42,31ZM29,38h6v3H29Zm8,0h6v3H37Zm6-4v2H37V33h5A1.001,1.001,0,0,1,43,34ZM30,33h5v3H29V34A1.001,1.001,0,0,1,30,33ZM29,45V43h6v3H30A1.001,1.001,0,0,1,29,45Zm13,1H37V43h6v2A1.001,1.001,0,0,1,42,46Z' style='fill:#fff' /></svg>
                                </span>
                            </div>";
        }

        return $type_icon;
	}

	/*--------------------------------------------- 
			Manage General Documents
	----------------------------------------------*/

	function managePrivatedocuments()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT D.id,D.property_id ,D.documents_id,D.type,D.status,D.created_at,F.doc_title  FROM ".PROPERTY_DOCUMENTS." D LEFT JOIN ".EMPLOYEE." E ON (E.id=D.added_by) LEFT JOIN ".FLATVILLA_DOC_TYPE." F ON (F.id=D.documents_id) WHERE D.property_id='$property_id'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    				<div class='nk-block-head-xs'>
				            <div class='nk-block-between g-2'>
				                <div class='nk-block-head-content'>
				                    <h6 class='nk-block-title title'>".ucwords($list['doc_title'])."</h6>
				                </div>
				            </div>
				        </div>
				        <div class='toggle-expand-content expanded' data-content='recent-files' style='margin-bottom: 20px;'>
		    				<div class='nk-files nk-files-view-group'>
		    					<div class='nk-files-group'>
		    						<div class='nk-files-list'>
				            			".$this->getPrivateDoc($list['documents_id'],$property_id)."
				            		</div>
				            	</div>
				            </div>
					    </div>
				        
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageCommonPrivatedocuments()
  	{
  		$layout = "";
  		$result = array();
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT D.id,D.property_id ,D.title ,D.description,D.documents,D.document_type,D.status,D.created_at  FROM ".PROPERTY_GENDRAL_DOCUMENTS." D WHERE D.property_id='$property_id' " ;
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$type_icon   = $this->getFileIcon($list['document_type']);
	    		//$doc_status = (($list['doc_status']!=0) ? "<li><a href='".DOCUMENTS."".$list['documents']."' target='_blank' class=''><em class='icon ni ni-download'></em><span>Download</span></a></li>" : "");
	    		$layout .= "
	    				<a href='javascript:void();' class='viewPrivateCommonDocInfo' data-value='".$i."' data-option='".$list['id']."'>            
			                <div class='nk-file-item nk-file'>
			                    <div class='nk-file-info'>
			                        <div class='nk-file-title'>
			                            ".$type_icon."
			                            <div class='nk-file-name'>
			                                <div class='nk-file-name-text'>
			                                    <a href='javascript:void();' class='title viewPrivateCommonDocInfo'  data-value='".$i."' data-option='".$list['id']."'>".ucwords($list['title'])."</a>
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
		                </a>
						 ";
	    		
	    		$i++;
	    	}
	    }
	    $result['count']  = $count ;
	    $result['layout'] = $layout ;
	    return $result;
  	}

  	function getPrivateDoc($documents_id,$property_id)
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT D.id,D.property_id ,D.document_id ,D.document_list_id,D.doc_name,D.description,D.document_file,D.doc_type,D.doc_status ,D.status,D.created_at  FROM ".PROPERTY_DOCUMENTS_ITEMS." D LEFT JOIN ".EMPLOYEE." E ON (E.id=D.added_by)  WHERE D.property_id='$property_id' AND D.document_id='$documents_id' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$type_icon   = $this->getFileIcon($list['doc_type']);
	    		$doc_status = (($list['doc_status']!=0) ? "<li><a href='".DOCUMENTS."".$list['document_file']."' target='_blank' class=''><em class='icon ni ni-download'></em><span>Download</span></a></li>" : "");
	    		$layout .= "
	    				<a href='javascript:void();' class='viewPrivateDocInfo' data-value='".$i."' data-option='".$list['id']."'>            
		                <div class='nk-file-item nk-file'>
		                    <div class='nk-file-info'>
		                        <div class='nk-file-title'>
		                            ".$type_icon."
		                            <div class='nk-file-name'>
		                                <div class='nk-file-name-text'>
		                                    <a href='javascript:void();' class='title viewPrivateDocInfo'  data-value='".$i."' data-option='".$list['id']."'>".ucwords($list['doc_name'])."</a>
		                                    
		                                </div>
		                            </div>
		                        </div>
		                       
		                    </div>
		                    
		                </div>
		                </a>
						 ";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage Gallery
	----------------------------------------------*/

	function manageGalleryold()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.token,G.album_title,G.description,G.image,G.sort_order,G.status,G.created_at FROM ".GALLERY_TBL." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.status='1' ORDER BY G.sort_order ASC " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    				<a href='".BASEPATH."gallery/details/".$list['token']."' >
		    				<div class='nk-file-item nk-file'>
			                    <div class='nk-file-info'>
			                        <div class='nk-file-title'>
			                            <div class='nk-file-icon'>
			                                <a class='nk-file-icon-link' href='".BASEPATH."gallery/details/".$list['token']."'>
			                                    <span class='nk-file-icon-type widthmodify'>
			                                        <img src='".SRCIMG."".$list['image']."'>
			                                    </span>
			                                </a>
			                            </div>
			                            <div class='nk-file-name'>
			                                <div class='nk-file-name-text'>
			                                    <a href='".BASEPATH."gallery/details/".$list['token']."' class='title'>".ucwords($list['album_title'])."</a>
			                                </div>
			                            </div>
			                        </div>
			                       
			                    </div>
			                </div>
		                </a>
	    		";
	    		/*<div class='nk-file-actions'>
		                        <div class='dropdown'>
		                            <a href='' class='dropdown-toggle btn btn-sm btn-icon btn-trigger' data-toggle='dropdown'><em class='icon ni ni-more-h'></em></a>
		                            <div class='dropdown-menu dropdown-menu-right'>
		                                <ul class='link-list-plain no-bdr'>
		                                    <li><a href='".BASEPATH."gallery/details/".$list['token']."' ><em class='icon ni ni-eye'></em><span>Details</span></a></li>
		                                   <li><a href='#file-details' data-toggle='modal'><em class='icon ni ni-alert-circle-fill'></em><span> Adhoc request </span></a></li>
		                                </ul>
		                            </div>
		                        </div>
		                    </div>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageGallery()
  	{
  		$layout = "";
  		//$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.token,G.album_title,G.description,G.image,G.sort_order,G.status,G.created_at,(SELECT COUNT(id) as tot FROM ".GALLERY_IMAGE." WHERE gallery_id=G.id ) AS image_count,E.name  FROM ".GALLERY_TBL." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.status='1' AND G.deleted_status='0' ORDER BY G.sort_order ASC " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
		                <div class='col-md-3 col-xs-12 col-masonry'>
							<a href='".BASEPATH."gallery/details/".$list['token']."' class='gallery_link'>
								<div class='gallery_list'>
									<div class='img_wrap'>
										<div class='img'>
											<img src='".SRCIMG."".$list['image']."' class='img-responsive'>
										</div>
										<div class='album_title'>
											<p>".ucwords($list['album_title'])."</p>
										</div>
									</div>
									<span class='clearfix'></span>
									<div class='album_body'> 
										<h4 class='text-center'><i class='icon ni ni-camera'></i> Total Pictures: ".$list['image_count']."</h4>
									</div>
				                    <span class='clearfix'></span>
								</div>
							</a>
						</div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function viewGalleryImages($album_id)
	{
		$layout ="";
		$q = "SELECT * FROM ".GALLERY_IMAGE." WHERE gallery_id='$album_id' ";
		$query = $this->selectQuery($q);	
		if(mysqli_num_rows($query) > 0){
			$i=1;
	      	while($list = mysqli_fetch_array($query)){
	      		$layout .= "
	      			<div class='col-md-3 col-masonry'>
						<a class='zooming' href='".SRCIMG.$list['image']."' title=''>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail'>
							<p class='view_album_posted_by raise_ticket'><a href='".BASEPATH."gallery/communityrequest/".$list['id']."'  class='title btn btn-sm btn-info '> Adhoc Request</a></p>
						</a>
						
					</div>
					";	
	            $i++;
	      	}
	    }else{
	    	$layout = "<div class='col-md-12'>
			    		<div class='no_album_img'> 
		    				<h1><span class='fa fa-picture-o'></span></h1>
		    				<p>No Images added to the Album.</p>
		    			</div>
			    	</div>";
	    }
	    return $layout;
	}


	/*--------------------------------------------- 
			Manage Private Gallery
	----------------------------------------------*/

	function managePrivateGallery()
  	{
  		$layout = "";
  		$result = array();
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.property_id,G.images,G.status,G.created_at,(SELECT COUNT(id) as tot FROM ".PROPERTY_GALLERY." WHERE property_id=G.property_id ) AS image_count FROM ".PROPERTY_GALLERY." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.property_id='$property_id' ORDER BY G.id ASC LIMIT 1 " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			
	                 <div class='col-md-3 col-xs-12 col-masonry'>
							<a href='".BASEPATH."gallery/viewdetails/".$list['id']."' class='gallery_link'>
								<div class='gallery_list'>
									<div class='img_wrap'>
										<div class='img'>
											<img src='".SRCIMG."".$list['images']."' class='img-responsive'>
										</div>
										<div class='album_title'>
											<p>Common Gallery</p>
										</div>
									</div>
									<span class='clearfix'></span>
									<div class='album_body'> 
										<h4 class='text-center'><i class='icon ni ni-camera'></i> Total Pictures: ".$list['image_count']."</h4>
									</div>
									
								</div>
							</a>
						</div>
	    		";
	    		$i++;
	    	}
	    }
	    $result['count'] = $count;
	    $result['layout'] = $layout;
	    return $result;
  	}


	function managePrivateGallery_old()
  	{
  		$layout = "";
  		$result = array();
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.property_id ,G.images,G.status,G.created_at FROM ".PROPERTY_GALLERY." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.property_id='$property_id' ORDER BY G.id ASC LIMIT 1 " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<a href='".BASEPATH."gallery/viewdetails/".$list['id']."' >
	    				<div class='nk-file-item nk-file'>
		                    <div class='nk-file-info'>
		                        <div class='nk-file-title'>
		                            <div class='nk-file-icon'>
		                                <a class='nk-file-icon-link' href='".BASEPATH."gallery/viewdetails'>
		                                    <span class='nk-file-icon-type widthmodify'>
		                                        <img src='".SRCIMG."".$list['images']."'>
		                                    </span>
		                                </a>
		                            </div>
		                            <div class='nk-file-name'>
		                                <div class='nk-file-name-text'>
		                                    <a href='".BASEPATH."gallery/viewdetails' class='title'>Common Gallery</a>
		                                </div>
		                            </div>
		                        </div>
		                        
		                    </div>
		                </div>
	                </a>	

	    		";
	    		$i++;
	    	}
	    }
	    $result['count'] = $count;
	    $result['layout'] = $layout;
	    return $result;
  	}
  	/*<a href='".BASEPATH."gallery/viewdetails/".$list['id']."' >
        <div class='cards'>
		  <img src='".SRCIMG."".$list['images']."' alt='Avatar' style='width:600px'>
		  <div class='containers'>
		    <h4><b>Common Gallery</b></h4> 
		  </div>
		</div>
	</a>*/


  	function viewPrivateCommonGalleryImages()
	{
		$layout ="";
		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT * FROM ".PROPERTY_GALLERY." WHERE property_id='$property_id' ";
		$query = $this->selectQuery($q);	
		if(mysqli_num_rows($query) > 0){
			$i=1;
	      	while($list = mysqli_fetch_array($query)){
	      		$layout .= "
	      			<div class='col-md-3 col-masonry'>
						<a class='zooming' href='".SRCIMG.$list['images']."' title=''>
							<img src='".SRCIMG.$list['images']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail'>
						</a>
						<p class='view_album_posted_by raise_ticket'><a href='".BASEPATH."gallery/privatecommonrequest/".$list['id']."' class='title btn btn-sm btn-info '> Adhoc Request</a></p>
					</div>
					";	
	            $i++;
	      	}
	    }
	    return $layout;
	}

	function managePrivateRoomGallery()
  	{
  		$layout = "";
  		$result = array();
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.property_id,G.roomlist_id,G.image,G.status,G.created_at,F.room_type,(SELECT COUNT(id) as tot FROM ".PROPERTY_ROOMS_LIST." WHERE roomlist_id=G.roomlist_id ) AS image_count  FROM ".PROPERTY_ROOMS_LIST." G LEFT JOIN ".FLATTYPE_MASTER_LIST." F ON (F.id=G.roomlist_id) WHERE G.property_id='$property_id' GROUP BY G.roomlist_id  " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "	
	                <div class='col-md-3 col-xs-12 col-masonry'>
						<a href='".BASEPATH."gallery/viewgallery/".$list['roomlist_id']."' class='gallery_link'>
							<div class='gallery_list'>
								<div class='img_wrap'>
									<div class='img'>
										<img src='".SRCIMG."".$list['image']."' class='img-responsive'>
									</div>
									<div class='album_title'>
										<p>".ucwords($list['room_type'])."</p>
									</div>
								</div>
								<span class='clearfix'></span>
								<div class='album_body'> 
									<h4 class='text-center'><i class='icon ni ni-camera'></i> Total Pictures: ".$list['image_count']."</h4>
								</div>
								<span class='clearfix'></span>
								
							</div>
						</a>
					</div>

	    		";
	    		$i++;
	    	}
	    }
	    $result['count'] = $count;
	    $result['layout'] = $layout;
	    return $result;
	   
  	}

	function managePrivateRoomGallery_old()
  	{
  		$layout = "";
  		$result = array();
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT G.id,G.property_id,G.roomlist_id,G.image,G.status,G.created_at,F.room_type FROM ".PROPERTY_ROOMS_LIST." G LEFT JOIN ".FLATTYPE_MASTER_LIST." F ON (F.id=G.roomlist_id) WHERE G.property_id='$property_id' GROUP BY G.roomlist_id  " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "	
	    			<a href='".BASEPATH."gallery/viewgallery/".$list['roomlist_id']."' >
	    				<div class='nk-file-item nk-file'>
		                    <div class='nk-file-info'>
		                        <div class='nk-file-title'>
		                            <div class='nk-file-icon'>
		                                <a class='nk-file-icon-link' href='".BASEPATH."gallery/viewgallery/".$list['roomlist_id']."'>
		                                    <span class='nk-file-icon-type widthmodify'>
		                                        <img src='".SRCIMG."".$list['image']."'>
		                                    </span>
		                                </a>
		                            </div>
		                            <div class='nk-file-name'>
		                                <div class='nk-file-name-text'>
		                                    <a href='".BASEPATH."gallery/viewgallery/".$list['roomlist_id']."' class='title'>".ucwords($list['room_type'])."</a>
		                                </div>
		                            </div>
		                        </div>
		                       
		                    </div>
		                </div>
	                </a>
	    		";
	    		$i++;
	    	}
	    }
	    $result['count'] = $count;
	    $result['layout'] = $layout;
	    return $result;
	    /*<a href='".BASEPATH."gallery/viewgallery/".$list['roomlist_id']."' >
                <div class='cards'>
				  <img src='".SRCIMG."".$list['image']."' alt='Avatar' style='width:600px'>
				  <div class='containers'>
				    <h4><b>".ucwords($list['room_type'])."</b></h4> 
				  </div>
				</div>
			</a>*/
	    /**/
  	}

  	function viewprivateroomgallery($roomlist_id)
	{
		$layout ="";
		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT * FROM ".PROPERTY_ROOMS_LIST." WHERE property_id='$property_id' AND roomlist_id='$roomlist_id'  ";
		$query = $this->selectQuery($q);	
		if(mysqli_num_rows($query) > 0){
			$i=1;
	      	while($list = mysqli_fetch_array($query)){
	      		$layout .= "
	      			<div class='col-md-3 col-masonry'>
						<a class='zooming' href='".SRCIMG.$list['image']."' title=''>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail'>
						</a>
						<p class='view_album_posted_by raise_ticket'><a href='".BASEPATH."gallery/privaterequest/".$list['id']."' class='title btn btn-sm btn-info '> Adhoc Request</a></p>
					</div>
					";	
	            $i++;
	      	}
	    }
	    return $layout;
	}


	function getPropertyDocumentLists()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
  		$info = $this->getDetails(PROPERTY_TBL,"*"," id='".$property_id."' ");
  		$varientinfo = $this->getDetails(FLATTYPE_MASTER,"*"," id='".$info['room_id']."' ");
		$q = "SELECT * FROM ".FLATTYPE_MASTER_LIST." WHERE flattype_id='".$info['room_id']."' GROUP BY flattype_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		
	    		$layout .= "
	    			<div class='card-inner'>
                        <div class='card-title-group'>
                            <div class='card-title'>
                                <h6 class='title'>Flat Variant: ".ucwords($varientinfo['flat_variant'])."</h6>
                            </div>
                        </div>
                    </div>
                    <div class=''>
                        <div class='user-card'>
                            <div class='nk-tb-list nk-tb-ulist'>
                                <div class='nk-tb-item nk-tb-head'>
                                    <div class='nk-tb-col' style='width: 1%;'><span>#</span></div>
                                    <div class='nk-tb-col tb-col-mb' style='width: 15%;'><span>Room</span></div>
                                    <div class='nk-tb-col tb-col-mb' style='width: 10%;'><span>Sqft</span></div>
                                    <div class='nk-tb-col tb-col-md' style='width: 50%;'><span>Description</span></div>
                                </div>
                                ".$this->getDocumentListItems($info['room_id'])."     
                            </div>
                        </div>
                    </div>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getDocumentListItems($room_id="")
  	{
  		$layout = "";
		$q = "SELECT * FROM ".FLATTYPE_MASTER_LIST."  WHERE flattype_id='".$room_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 	   = $this->editPagePublish($details);
	    		$layout .= "
	    			<div class='nk-tb-item'>
                        <div class='nk-tb-col nk-tb-col-check'>
                            ".$i."
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>
                                <div class='user-info'>
                                    <span class='tb-lead'>".ucwords($list['room_type'])." <span class='dot dot-success d-md-none ml-1'></span></span>
                                </div>
                            </div>
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>".$list['sqft']."
                            </div>
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>".$list['description']."
                            </div>
                        </div>
                       
                    </div>
	    		";
	    		/*<a href='<?php echo IMGPATH ?>sample.pdf' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file-pdf'></em></a>*/
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

	/*--------------------------------------------- 
			Manage Adhoc Requests
	----------------------------------------------*/

	function manageadhocrequests()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS name  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) WHERE A.property_id='$property_id' ORDER BY A.created_at DESC   " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );
	    		$layout .= "
	    			<tr class='tb-ticket-item $readcolor'>
                        <td class='tb-ticket-id'><a href='".BASEPATH."adhocrequests/details/".$list['id']."'>".$list['ticket_uid']."</a></td>
                        <td class='tb-ticket-desc'>
                            <a href='javascript:void();'><span class='title'>".ucwords($list['subject'])."</span></a>
                        </td>
                        <td class='tb-ticket-date tb-col-md'>
                            ".ucwords($list['name'])."
                        </td>
                        <td class='tb-ticket-seen tb-col-md'>
                           <span class='date'>".date("d M Y",strtotime($list['created_at']))."</span>
                        </td>
                        <td class='tb-ticket-status'>
                            ".$request_status."
                        </td>
                        <td class='tb-ticket-action'>
                            <a href='".BASEPATH."adhocrequests/details/".$list['id']."' class='btn btn-icon btn-trigger'>
                                <em class='icon ni ni-chevron-right'></em>
                            </a>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getAdhocInfo($adhoc_id)
  	{
  		$layout = "";
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.ref_id,A.type,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name, (SELECT id as tots FROM ".REQUEST_LOGS." WHERE ref_id=A.id AND type='created' ) AS log_id  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE A.id='$adhoc_id' ORDER BY A.id ASC  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );

	    		$words = explode(" ", $list['name']);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
            	}

                $requesttype = (($list['request_type']=="user" ? "bg-primary" : "bg-warning" ));
                $requesttype1 = (($list['request_type']=="user" ? "Customer" : "Support Team" ));	

                $attachment = $this->getTicketAttachment($list['log_id'],$list['id']);

                /*if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<ul class='ticket-msg-attach-list'>
                        		".$attachment['layout']."
                        	</ul>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }*/

                if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<div class='row'>
                        		".$attachment['layout']."
                        	</div>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }
                
                $type = $this->getAdhocReferenceInformation($list['ref_id'],$list['type']);
	    		$layout .= "
	    			<div class='ticket-msg-item'>
                        <div class='ticket-msg-from'>
                            <div class='ticket-msg-user user-card'>
                                <div class='user-avatar ".$requesttype."'>
                                    <span>".$acronym."</span>
                                </div>
                                <div class='user-info'>
                                    <span class='lead-text'>".ucwords($list['name'])."</span>
                                    <span class='text-soft'>".$requesttype1."</span>
                                </div>
                            </div>
                            <div class='ticket-msg-date'>
                                <span class='sub-text'>".date("M d, Y h:i A",strtotime($list['created_at']))."</span>
                            </div>
                        </div>
                        <div class='ticket-msg-comment'>
                        	<p>".$type."</p>
                            <p>".$list['remarks']."</p>
                        </div>
                        	".$attachmentinfo."
                    </div>
	    		";
	    		/*<p><strong>".ucwords($list['subject'])."</strong></p>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getAdhocReferenceInformation($ref_id,$type)
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];

  		if ($type=="general document") {
  			$q = "SELECT G.id,G.token,G.title,G.description,G.documents,G.document_type,G.added_by,G.status,G.created_at  FROM ".GENDRAL_DOCUMENTS." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.id='$ref_id' " ;
		    $query = $this->selectQuery($q);	
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['document_type']);

		    		$documents_file = (($list['documents']!="") ? DOCUMENTS.$list['documents'] : 'javascript:void();' );
		    		$target_blank   = (($list['documents']!="") ? "target='_blank'"  : "" );

		    		$layout .= "<a href='".$documents_file."' $target_blank>
				    				<div class='nk-file-item nk-file'>
			                            <div class='nk-file-info'>
			                                <div class='nk-file-title'>
			                                    ".$type_icon."
			                                    <div class='nk-file-name'>
			                                        <div class='nk-file-name-text'>
			                                            <a href='".$documents_file."' $target_blank class='title ' >".ucwords($list['title'])."</a>
			                                        </div>
			                                    </div>
			                                </div>
			                                
			                            </div>
			                        </div>
	                        	</a>
		    		";
		    		
		    		$i++;
		    	}
		    }
  		}elseif($type=="private document"){
  			$q = "SELECT D.id,D.property_id ,D.document_id ,D.document_list_id,D.doc_name,D.description,D.document_file,D.doc_type,D.doc_status ,D.status,D.created_at  FROM ".PROPERTY_DOCUMENTS_ITEMS." D LEFT JOIN ".EMPLOYEE." E ON (E.id=D.added_by)  WHERE D.id='$ref_id' " ;
		    $query = $this->selectQuery($q);	
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['doc_type']);
		    		$documents_file = (($list['document_file']!="") ? DOCUMENTS.$list['document_file'] : 'javascript:void();' );
		    		$target_blank = (($list['document_file']!="") ? "target='_blank'"  : "" );
		    		$layout .= "
		    				<a href='".$documents_file."' $target_blank>            
				                <div class='nk-file-item nk-file'>
				                    <div class='nk-file-info'>
				                        <div class='nk-file-title'>
				                            ".$type_icon."
				                            <div class='nk-file-name'>
				                                <div class='nk-file-name-text'>
				                                    <a class='title' href='".$documents_file."' $target_blank>".ucwords($list['doc_name'])."</a>
				                                    
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
			                </a>
							 ";
		    		
		    		$i++;
		    	}
		    }
  		}elseif($type=="private general document"){
			$q = "SELECT D.id,D.property_id ,D.title ,D.description,D.documents,D.document_type,D.status,D.created_at  FROM ".PROPERTY_GENDRAL_DOCUMENTS." D WHERE D.id='$ref_id' " ;
		    $query = $this->selectQuery($q);	
		    $count = mysqli_num_rows($query);
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['document_type']);

		    		$documents_file = (($list['documents']!="") ? DOCUMENTS.$list['documents'] : 'javascript:void();' );
		    		$target_blank   = (($list['documents']!="") ? "target='_blank'"  : "" );

		    		$layout .= "
		    				<a href='javascript:void();' href='".$documents_file."' $target_blank>            
				                <div class='nk-file-item nk-file'>
				                    <div class='nk-file-info'>
				                        <div class='nk-file-title'>
				                            ".$type_icon."
				                            <div class='nk-file-name'>
				                                <div class='nk-file-name-text'>
				                                    <a href='".$documents_file."' $target_blank class='title'>".ucwords($list['title'])."</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
			                </a>
							 ";
		    		
		    		$i++;
		    	}
		    }

  		}elseif($type=="community gallery"){
  			$q = "SELECT * FROM ".GALLERY_IMAGE." WHERE id='$ref_id' ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='' href='".SRCIMG.$list['image']."' target='_blank' title=''>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade img-responsive img-thumbnail'  style='width: 200px;height: auto;'>
						</a>
						
						
						";	
		            $i++;
		      	}
		    }

  		}elseif($type=="private common gallery"){
  			$q = "SELECT * FROM ".PROPERTY_GALLERY." WHERE id='$ref_id' ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='zooming' href='".SRCIMG.$list['images']."' title='' target='_blank'>
							<img src='".SRCIMG.$list['images']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style='width: 200px;height: auto;'>
						</a>
						";	
		            $i++;
		      	}
		    }
  			
  		}elseif($type=="private gallery"){
  			$q = "SELECT * FROM ".PROPERTY_ROOMS_LIST." WHERE id='$ref_id'  ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='zooming' href='".SRCIMG.$list['image']."' title='' target='_blank'>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style='width: 200px;height: auto;'>
						</a>
						";	
		            $i++;
		      	}
		    }
  		}
	    return $layout;
  	}

  		
  	function getReplyAdhocInfo($adhoc_id)
  	{
  		$layout = "";

		$q = "SELECT L.id,L.type,L.request_type,L.created_by,L.created_to,L.ref_id,L.remarks,L.status,L.created_at,L.updated_at,E.name,C.name as cus_name FROM ".REQUEST_LOGS." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.created_to) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.created_by) WHERE L.ref_id='$adhoc_id' AND L.type!='created'   ORDER BY L.id ASC  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		if ($list['request_type']=="user" ) {
	    			$words = explode(" ", $list['cus_name']);
	                $acronym = "";
	                foreach ($words as $w) {
	                $acronym .= $w[0];
	            	}

	            	$name = ucwords($list['cus_name']);
	    		}else{
	    			$words = explode(" ", $list['name']);
	                $acronym = "";
	                foreach ($words as $w) {
	                $acronym .= $w[0];
	            	}

	            	$name = ucwords($list['name']);
	    		}

                $requesttype = (($list['request_type']=="user" ? "bg-primary" : "bg-warning" ));
                $requesttype1 = (($list['request_type']=="user" ? "Customer" : "Support Team" ));

                $attachment = $this->getTicketAttachment($list['id'],$list['ref_id']);

                /*if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<ul class='ticket-msg-attach-list'>
                        		".$attachment['layout']."
                        	</ul>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }*/

                if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<div class='row'>
                        		".$attachment['layout']."
                        	</div>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }

	    		$layout .= "
	    			<div class='ticket-msg-item'>
                        <div class='ticket-msg-from'>
                            <div class='ticket-msg-user user-card'>
                                <div class='user-avatar ".$requesttype."'>
                                    <span>".$acronym."</span>
                                </div>
                                <div class='user-info'>
                                    <span class='lead-text'>".$name."</span>
                                    <span class='text-soft'>".$requesttype1."</span>
                                </div>
                            </div>
                            <div class='ticket-msg-date'>
                                <span class='sub-text'>".date("M d, Y h:i A",strtotime($list['created_at']))."</span>
                            </div>
                        </div>
                        <div class='ticket-msg-comment'>
                            <p>".$list['remarks']."</p>
                        </div>
                       	".$attachmentinfo."
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getTicketAttachment($ref_id,$adhoc_id)
  	{
  		$layout = "";
  		$result = array();
		$q = "SELECT I.id,I.ref_id,I.adoc_id,I.property_id,I.customer_id,I.image_name,I.file_type,I.description,I.status,I.created_at,L.request_type FROM ".ADOC_IMAGES." I LEFT JOIN ".REQUEST_LOGS." L ON (L.id=I.ref_id) WHERE I.ref_id='$ref_id' AND I.adoc_id='$adhoc_id' " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		
	    		$type = (($list['request_type']=='user') ? FRONTSRCIMG.$list['image_name'] : SRCIMG.$list['image_name'] );
	    		
	    		//$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? "<img src='".$type."' alt=''>" : "<img src='".IMGPATH."file.png' alt=''>" );

	    		$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? $type : IMGPATH."file.png" );

	    		$layout .= "
                        <div class='col-md-4 col-sm-12 col-xs-12 '>
							<a href='".$type."' target='_blank'>
								<div class='event_item_list' >
									<div class='event_image' style='background-image: url($image); background-repeat: no-repeat; background-position: center top; background-size: cover;'>
									</div>
									<div class='event_info'>
										<div class='info_wrap'>
											<h5> ".substr($list['description'], 0, 70)." </h5>
										</div>
									</div>
								</div>
							</a>
						</div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    /*<li>
            <a href='".$type."' target='_blank'>
                ".$image."
                <em class='icon ni ni-download'></em>
            </a>
        </li>*/
	    $result['count'] = $count ;
	    $result['layout'] = $layout;
	    return $result;
  	}

	/*--------------------------------------------- 
			Manage KYC
	----------------------------------------------*/

	function managekyc()
  	{
  		$layout = "";
  		$property_id = $_SESSION["crm_property_id"];
  		$customer_id = $_SESSION["crm_member_id"];
		$q = "SELECT K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type ,K.status,K.approval_status,K.created_at,T.kyc_type
			 FROM ".KYC_LIST." K LEFT JOIN ".KYC_TYPE." T ON (T.id=K.kyctype_id) WHERE K.customer_id='$customer_id' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);

	    		$approval_status1 = (($list['approval_status']=='1') ? "<span class='tb-status text-success'>Approved</span>
                            " : "<span class='tb-status text-warning'>Pending</span>" );
	    		/*<span data-toggle='tooltip' title='Approved at 18 Dec, 2019 01:02 PM' data-placement='top'><em class='icon ni ni-info'></em></span>*/
	    		$approval_status = (($list['approval_status']=='2') ? "<span class='tb-status text-danger'>Reject</span>
                            " : $approval_status1 );
	    		$layout .= "
	    			<div class='nk-tb-item'> 
	    				<div class='nk-tb-col tb-col-mb'>
                            <span class='tb-lead-sub'>".$i."</span>
                        </div>
                        <div class='nk-tb-col tb-col-mb'>
                            <span class='tb-lead-sub'>".ucwords($list['kyc_type'])."</span>
                        </div>
                        <div class='nk-tb-col tb-col-md'>
                            <ul class='list-inline list-download'>
                                <li>Doc <a href='".FRONTSRCIMG.$list['image_name']."' target='_blank' class='popup'><em class='icon ni ni-download'></em></a></li>
                            </ul>
                        </div>
                        <div class='nk-tb-col tb-col-lg'>
                            <span class='tb-date'>".date("d M, Y h:i g",strtotime($list['created_at']))."</span>
                        </div>
                        <div class='nk-tb-col tb-col-md'>
                            ".$approval_status."
                        </div>
                        <div class='nk-tb-col tb-col-lg'>
                            -
                        </div>
                        <div class='nk-tb-col nk-tb-col-tools'>
                            <ul class='nk-tb-actions gx-1'>
                                <li>
                                    <div class='drodown'>
                                        <a href='javascript:void();' class='dropdown-toggle btn btn-icon btn-trigger' data-toggle='dropdown'><em class='icon ni ni-more-h'></em></a>
                                        <div class='dropdown-menu dropdown-menu-right'>
                                            <ul class='link-list-opt no-bdr'>
                                                <li><a href='".BASEPATH."kyc/details/".$list['id']."'><em class='icon ni ni-eye'></em><span>View Details</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
	    		";
	    		/*<div class='user-card'>
                                <div class='user-avatar user-avatar-xs bg-orange-dim'>
                                    <span>JS</span>
                                </div>
                                <div class='user-name'>
                                    <span class='tb-lead'>Janet Snyder</span>
                                </div>
                            </div>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	function getKYCList($customer_id)
  	{
  		$layout = "";
  		$result = array();
		$q = "SELECT K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type ,K.status,K.approval_status,K.created_at,T.kyc_type
			 FROM ".KYC_LIST." K LEFT JOIN ".KYC_TYPE." T ON (T.id=K.kyctype_id) WHERE K.customer_id='$customer_id' " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);
	    		$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? FRONTSRCIMG.$list['image_name'] : IMGPATH."pdffile.png" );

	    		$pic 	    = $list['image_name']!="" ? "<a target='blank' href='".FRONTSRCIMG.$list['image_name']."'><img style='width:70px;' class='img-responsive img-thumbnail' src='".$image."' target='blank' alt=''></a>": "No files!";
	    		$layout .= "
	    		<tr id='attacjmentrow_".$list['id']."'>
	    			<td width='45%'>".ucwords($list['kyc_type'])."</td>
	    			<td width='45%'>$pic</td>
	    			<td width='10%'> 
	    				<a href='javascript:void()' class='btn btn-danger btn-sm deletKYCAttachmentImage' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
	    			</td>
	    		</tr>";
	    		$i++;
	    		/*<a href='javascript:void()' class='btn btn-success btn-sm editKYCAttachmentImage' data-value='".$i."'  data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-edit'></span></a>	*/
	    	}
	    }
	    $result['count'] = $count;
	    $result['layout'] = $layout ;
	    return $result;	
  	}

  	/*--------------------------------------------- 
				Invoice
	----------------------------------------------*/

	function manageInvoice()
  	{
  		$layout = "";
  		$customer_id = $_SESSION["crm_member_id"];
		$q = "SELECT I.id,I.inv_uid,I.inv_number,I.inv_date,I.type,I.raised_to,I.documents,I.document_type,I.remarks,I.status,I.added_by,I.created_at,I.updated_at,C.name   FROM ".INVOICE_TBL." I LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=I.customer_id) WHERE I.customer_id='$customer_id' AND I.deleted_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			
                    <tr class='tb-odr-item'>
                        <td class='tb-odr-info'>
                        	<span class='tb-odr-id' style='min-width: 20px;'>$i</span>
                            <span class='tb-odr-id' style='min-width: 170px;'><a href='".BASEPATH."invoice/details/".$list['id']."'>".$list['inv_number']."</a></span>

                            <span class='tb-odr-date'>".date("d M Y",strtotime($list['inv_date']))."</span>
                        </td>
                        <td class='tb-odr-amount'>
                        	 
                            <span class='tb-odr-total'>
                                <span class='amount'>".ucwords($list['type'])."</span>
                            </span>
                            <span class='tb-odr-status'>
                               ".ucwords($list['raised_to'])."
                            </span>
                        </td>
                        <td class='tb-odr-action'>
                            <div class='tb-odr-btns d-none d-sm-inline'>
                               
                                <a href='".BASEPATH."invoice/details/".$list['id']."' class='btn btn-dim btn-sm btn-primary'>View</a>
                            </div>
                            <a href='".BASEPATH."invoice/details/".$list['id']."' class='btn btn-pd-auto d-sm-none'><em class='icon ni ni-chevron-right'></em></a>
                        </td>
                    </tr>


	    		";
	    		/* <a href='".BASEPATH."invoice/invprint' target='_blank' class='btn btn-icon btn-white btn-dim btn-sm btn-primary'><em class='icon ni ni-printer-fill'></em></a>*/
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				News
	----------------------------------------------*/

	function manageNews()
  	{
  		$layout = "";
  		$customer_id = $_SESSION["crm_member_id"];
		$q = "SELECT id,token,title,date,description,image,status,customer_ids FROM ".NEWS_TBL." WHERE deleted_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details 	 = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$posttoall 	 = $this->posttoall($list['id']);
	    		
	    		$dates_array = explode(",", $list['customer_ids']);
				foreach ($dates_array as $value) {
				    $checkedId = $customer_id==$value ? $list['id']  : 0  ;
				    $posttoSelected 	 = $this->posttoSelected($checkedId);
				}
	    		$layout .= "
                  	$posttoall 
                  	$posttoSelected 
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function posttoall($id)
  	{
  		$layout = "";
		$q = "SELECT N.id,N.token,N.title,N.date,N.description,N.image,N.status,N.added_by,E.name FROM ".NEWS_TBL." N LEFT JOIN ".EMPLOYEE." E ON (E.id=N.added_by) WHERE N.post_to='all' AND N.id='$id' AND N.deleted_status='0'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$img = SRCIMG.$list['image'] ;
	    		$image = (($list['image']!="") ? "<div class='event_image' style='background-image: url(".SRCIMG.$list['image']."); background-repeat: no-repeat; background-position: center top; background-size: cover;'>
							</div>"  : "");
	    		$image_class = (($list['image']=="") ? "width_100"  : "");
	    		$layout .= "
                    <div class='col-md-6 col-sm-12 col-xs-12'>
						<a href='".BASEPATH."news/details/".$list['token']."' class='news_item_link'>
							<div class='news_item_list $image_class'>
								$image
								<div class='news_info'> 
									<h2><em class='icon ni ni-calendar'></em>  ".date("d F Y", strtotime($list['date']))."</h2> 
									<h1> ".$this->publishContent(substr($list['title'], 0,80))."</h1>
									<h3>".$this->publishContent(substr($list['description'], 0,140))."</h3>
									
									<div class='stats_wrap'>
										<ul> 
											<li><p><em class='icon ni ni-user'></em> Posted by: ".substr($list['name'], 0,50)."</p></li> 
										</ul>
										<span class='clearfix'></span>
									</div>
								</div>

							</div>
						</a>
					</div>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function posttoSelected($id)
  	{

  		$layout = "";
		$q = "SELECT N.id,N.token,N.title,N.date,N.description,N.image,N.status,N.added_by,E.name FROM ".NEWS_TBL." N LEFT JOIN ".EMPLOYEE." E ON (E.id=N.added_by) WHERE N.post_to='selected' AND N.id='$id' AND N.deleted_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$img = SRCIMG.$list['image'] ;
	    		$image = (($list['image']!="") ? "<div class='event_image' style='background-image: url(".SRCIMG.$list['image']."); background-repeat: no-repeat; background-position: center top; background-size: cover;'>
							</div>"  : "");
	    		$image_class = (($list['image']=="") ? "width_100"  : "");
	    		$layout .= "
	    			
                    <div class='col-md-6 col-sm-12 col-xs-12'>
						<a href='".BASEPATH."news/details/".$list['token']."' class='news_item_link'>
							<div class='news_item_list $image_class'>
								$image
								<div class='news_info'> 
									<h2><em class='icon ni ni-calendar'></em>  ".date("d F Y", strtotime($list['date']))."</h2> 
									<h1> ".$this->publishContent(substr($list['title'], 0,70))."</h1>
									
									<h3>".$this->publishContent(substr($list['description'], 0,140))."</h3>
									
									<div class='stats_wrap'>
										<ul> 
											<li><p><em class='icon ni ni-user'></em> Posted by: ".substr($list['name'], 0,50)."</p></li> 
										</ul>
										<span class='clearfix'></span>
									</div>
								</div>

							</div>
						</a>
					</div>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function posttoall_OLD($id)
  	{
  		$layout = "";
		$q = "SELECT id,token,title,date,description,image,status FROM ".NEWS_TBL." WHERE post_to='all' AND id='$id' AND deleted_status='0'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$img = SRCIMG.$list['image'] ;
	    		$layout .= "
	    			
                    <div class='single-item col-md-4 col-sm-12 col-12'>
		                <div class='blog-box-layout4'>
		                    <div class='item-img spec_img'>
		                        <a href='".BASEPATH."news/details/".$list['token']."'>
		                            <div class='image img-fluid' style='background-image: url($img); background-repeat: no-repeat; background-position: center top; background-size: cover; transform: scale(1);'>
		                                </div>
		                        </a>
		                        <div class='post-date'>".date("d",strtotime($list['date']))."
		                            <span>".date("M",strtotime($list['date']))."</span>
		                        </div>
		                    </div>
		                    <div class='item-content news_max_height'>
		                        <h3 class='item-title'>
		                            <a href='".BASEPATH."news/details/".$list['token']."'>".ucwords($list['title'])."</a>
		                        </h3>
		                        <p>".substr(strip_tags($list['description']), 0, 150)."..<a href='".BASEPATH."news/details/".$list['token']."'>read more</a></p>
		                    </div>
		                </div>
		            </div>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function posttoSelected_OLD($id)
  	{

  		$layout = "";
		$q = "SELECT id,token,title,date,description,image,status FROM ".NEWS_TBL." WHERE post_to='selected' AND id='$id' AND deleted_status='0' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$img = SRCIMG.$list['image'] ;
	    		$layout .= "
	    			
                    <div class='single-item col-md-4 col-sm-12 col-12'>
		                <div class='blog-box-layout4'>
		                    <div class='item-img spec_img'>
		                        <a href='".BASEPATH."news/details/".$list['token']."'>
		                            <div class='image img-fluid' style='background-image: url($img); background-repeat: no-repeat; background-position: center top; background-size: cover; transform: scale(1);'>
		                                </div>
		                        </a>
		                        <div class='post-date'>".date("d",strtotime($list['date']))."
		                            <span>".date("M",strtotime($list['date']))."</span>
		                        </div>
		                    </div>
		                    <div class='item-content news_max_height'>
		                        <h3 class='item-title'>
		                            <a href='".BASEPATH."news/details/".$list['token']."'>".ucwords($list['title'])."</a>
		                        </h3>
		                        <p>".substr(strip_tags($list['description']), 0, 150)."..<a href='".BASEPATH."news/details/".$list['token']."'>read more</a></p>
		                    </div>
		                </div>
		            </div>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getPaymentStats($payment_schedule_id="")
	{	
		$today  = date("Y-m-d");
		$property_id = @$_SESSION['crm_property_id'];
		$query = "SELECT id,
				(SELECT SUM(percentage) as tots FROM ".PAYMENT_SCHEDULE_LIST." WHERE 1 AND payment_schedule_id='".$payment_schedule_id."'  AND property_id='".$property_id."' ) as percentage_count,
				(SELECT SUM(amt_receive) as tots FROM ".PAYMENT_SCHEDULE_LIST." WHERE 1 AND payment_schedule_id='".$payment_schedule_id."' AND property_id='".$property_id."') as amt_receive_count			
				FROM ".PAYMENT_SCHEDULE_LIST." WHERE 1 ";
		$exe = $this->selectQuery($query);	
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	/*------------------------------
  		Payment Schedule  List
  	--------------------------------*/

  	function getPaymentScheduleList($payment_schedule_id="",$property_id="")
  	{
  		$layout = "";
  		$property_id = @$_SESSION['crm_property_id'];
  		$result = array();
		$q = "SELECT P.id,P.payed_date ,P.stage_id ,P.percentage,P.amt_payable,P.amt_receive,P.balance,P.due_date,S.stage_name FROM ".PAYMENT_SCHEDULE_LIST." P LEFT JOIN ".STAGE_MASTER." S ON (S.id=P.stage_id) WHERE P.property_id='".$property_id."' AND P.payment_schedule_id='".$payment_schedule_id."' ORDER BY P.id  " ;
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 	   = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr>
                        <td>".ucwords($list['stage_name'])."</td>
                        <td>".($list['percentage'])."</td>
                        <td> ".number_format($list['amt_payable'],2)."</td>
                        <td> ".number_format($list['amt_receive'],2)."</td>
                        <td> ".number_format($list['balance'],2)."</td>
                        <td> ".date("d/m/Y",strtotime($list['due_date']))."</td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    $result['count']  = $count ;
	    $result['layout'] = $layout;

	    return $result;
  	}



}

?>