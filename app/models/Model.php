<?php

class Model
{
	
	/*--------------------------------------------- 
					Base Methods
	----------------------------------------------*/ 

	

	function __construct()
	{
		date_default_timezone_set("Asia/Calcutta");

	}
	
	function encryptPassword($data){
		return $encrypted = sha1($data);		
	}
	function decryptPassword($data){
		return $decrypted1 = sha1($data);
	}
	function encryptData($data){
		return $encrypted = base64_encode(base64_encode($data));		
	}
	function decryptData($data){
		return $decrypted1 = base64_decode(base64_decode($data));
	}
	
	function check_query($table,$column,$where){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$query = "SELECT $column FROM $table WHERE $where";
		$exe = mysqli_query($connect,$query);
		$no_rows = @mysqli_num_rows($exe);
		return $no_rows;
	}

	function getDetails($table,$column,$where){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$query = "SELECT $column FROM $table WHERE $where";
		$exe = mysqli_query($connect,$query);
		$rows = mysqli_fetch_array($exe);
		return $rows;
	}
	function selectQuery($query){
		$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
		$rows = mysqli_query($connect,$query);
		return $rows;
	}
	
	function deleteRow($table,$where)
	{
		$q = "DELETE FROM $table WHERE $where ";
		$exe = $this->selectQuery($q);
		if($exe){
			return 1;
		}else{
			return 0;
		}
	}
	function cleanString($data)
  	{
      //$string = str_replace("'", "\'", $data);
      //$string = str_replace('"', '\"', $string);
      $string = trim($data);
      $string = $this->escapeString($string);
      return $string;
  	}
  	
  	function publishContent($data)
  	{
  		 $string = str_replace("\'", "'", $data);
  		 $string = str_replace('\"', '"', $string);
  		 return $string;
  	}

  	// Editpage Publish Content

	function editPagePublish($data)
	{
		$response = array();
		foreach ($data as $key => $value) {
			$response[$key] = $this->publishContent($value);
		}
		return $response;
	}	
  	 function escapeString($data)
  	{
  		//$escaped = mysqli_real_escape_string($data);
		//$escaped = array_map('htmlentities', $escaped);
		$escaped = ($data);
		return $escaped;
  	}
  	function hyphenize($string) {
   		return preg_replace(
            	array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
           		array('-', ''),
              urldecode(strtolower($string))
        );
	}

	function unHyphenize($string) {
   		return str_replace('-', " ", $string);
	}

  	// Random String

  	function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	// Generate MObile TOken

	function mobileToken($length) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	// Convert URL to Array
	function convertUrlToArray($url,$page){
		$url = str_replace(BASEPATH.$page."?", "", $url);
	    $urlExploded 	=  explode("&", $url);
	    $return 		= array();
	    foreach ($urlExploded as $param){
	        $explodedPar = explode("=", $param);
	        $return[$explodedPar[0]] = $explodedPar[1];
	    }
	    return $return;
	}


	// Function Error Msg

	function errorMsg($value){
		$err = "
				<div class='alert alert-fill alert-danger alert-dismissible alert-icon'>
                    <em class='icon ni ni-cross-circle'></em> <strong>".$value."</strong>! <button class='close' data-dismiss='alert'></button>
                </div>

				";
		return $err;
	}

	function successMsg($value){
		$err = "<div class='alert alert-fill alert-info alert-dismissible alert-icon'>
                    <em class='icon ni ni-check'></em> <strong>".$value."</strong>! <button class='close' data-dismiss='alert'></button>
                </div>";
		return $err;
	}

	

	/*--------------------------------------------- 
				Get the Last  Activity
	----------------------------------------------*/ 

	function get_time_ago($time_stamp)
	{
		$time_difference = strtotime('now') - $time_stamp;
	
		if ($time_difference >= 60 * 60 * 24 * 365.242199)
		{
			/*
			 * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 365.242199 days/year
			 * This means that the time difference is 1 year or more
			 */
			return $this -> get_time_ago_string($time_stamp, 60 * 60 * 24 * 365.242199, 'year');
		}
		elseif ($time_difference >= 60 * 60 * 24 * 30.4368499)
		{
			/*
			 * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 30.4368499 days/month
			 * This means that the time difference is 1 month or more
			 */
			return $this -> get_time_ago_string($time_stamp, 60 * 60 * 24 * 30.4368499, 'month');
		}
		elseif ($time_difference >= 60 * 60 * 24 * 7)
		{
			/*
			 * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 7 days/week
			 * This means that the time difference is 1 week or more
			 */
			return $this -> get_time_ago_string($time_stamp, 60 * 60 * 24 * 7, 'week');
		}
		elseif ($time_difference >= 60 * 60 * 24)
		{
			/*
			 * 60 seconds/minute * 60 minutes/hour * 24 hours/day
			 * This means that the time difference is 1 day or more
			 */
			return $this -> get_time_ago_string($time_stamp, 60 * 60 * 24, 'day');
		}
		elseif ($time_difference >= 60 * 60)
		{
			/*
			 * 60 seconds/minute * 60 minutes/hour
			 * This means that the time difference is 1 hour or more
			 */
			return $this -> get_time_ago_string($time_stamp, 60 * 60, 'hour');
		}
		else
		{
			/*
			 * 60 seconds/minute
			 * This means that the time difference is a matter of minutes
			 */
			return $this -> get_time_ago_string($time_stamp, 60, 'minute');
		}
	}
	
	function get_time_ago_string($time_stamp, $divisor, $time_unit)
	{
		$time_difference = strtotime("now") - $time_stamp;
		$time_units      = floor($time_difference / $divisor);
	
		settype($time_units, 'string');
	
		if ($time_units === '0')
		{
			return 'less than 1 ' . $time_unit . ' ago';
		}
		elseif ($time_units === '1')
		{
			return '1 ' . $time_unit . ' ago';
		}
		else
		{
			/*
			 * More than "1" $time_unit. This is the "plural" message.
			 */
			// TODO: This pluralizes the time unit, which is done by adding "s" at the end; this will not work for i18n!
			return $time_units . ' ' . $time_unit . 's ago';
		}
	}

	/*--------------------------------------------- 
					Date Functions
	----------------------------------------------*/ 

	// General Date format

	function date($data)
	{
		return date("d M Y", strtotime($data));
	}

	// General Month Name format

	function month($data)
	{
		return date('M', mktime(0, 0, 0, $data, 1));
	}

	// GEt month days

	function getDaysList($day="")
	{
		$layout="";
		for ($i=1;$i<32;$i++) {
			$selected = (($i==$day) ? 'selected' : ''); 
			$layout.= "<option value='".str_pad($i, 2, '0', STR_PAD_LEFT)."' $selected>".str_pad($i, 2, '0', STR_PAD_LEFT)."</option>";
		}
		return $layout;
	}

	// get months lisk

	function getMonthsList($month="")
	{
		$layout="";
		for($m=1; $m<=12; $m++){
			$selected = (($m==$month) ? 'selected' : ''); 
			$layout.= "<option value='".str_pad($m, 2, '0', STR_PAD_LEFT)."' $selected>".date('M', mktime(0, 0, 0, $m, 1))."</option>";
		}
		return $layout;
	}


	/*--------------------------------------------- 
				Search Course Functions
	----------------------------------------------*/

	// Year List

	function getYearsList($yr="")
	{
		$layout="";
		$c_year = date("Y");
		for ($i=$c_year; $i >DEGREE_YEAR  ; $i--) { 
			$selected = (($yr==$i) ? 'selected' : '');
			$layout.= "<option value='$i' $selected>$i</option>";
		}
		return $layout;
	}


	/*--------------------------------------------- 
					Mail Functions
	----------------------------------------------*/

	function send_mail($sender,$sender_mail,$receiver_mail,$subject,$message,$bcc=""){

  		$mail = new PHPMailer;
		$mail->isSMTP(true);
		$mail->Port = SMTP_PORT; 
		$mail->Host = MAIL_HOST;
		$mail->SMTPAuth = true;
		$mail->Username = MAIL_USERNAME;
		$mail->Password = MAIL_PASSWORD;
		//$mail->SMTPSecure = 'tls';
		$mail->From = $sender_mail;
		$mail->FromName = COMPANY_NAME;//$sender;
		$mail->addAddress($receiver_mail, '');
		$mail->AddBCC($bcc, '');
		//$mail->addReplyTo(NO_REPLY, COMPANY_NAME);
		$mail->WordWrap = 50;
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body    = $message;
		return $mail->send();
	}

	// Adoch Request 

 	function newKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data){
 		$cusinfo 		= $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
 		$propertyinfo   = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$property_id."' ");

	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear Admin, </span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>


														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>
															A new Adhoc request ".$ticket_uid." has been rasied by the ".ucwords($cusinfo['name'])." for the property ".ucwords($propertyinfo['title']).". The details are as below.</p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>

														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>Customer Name: ".ucwords($cusinfo['name'])."<br>Mobile: ".$cusinfo['mobile']." <br>Email: ".$cusinfo['email']." <br>Property: ".ucwords($propertyinfo['title'])." </p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>

														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p><b>Request Details:</b> </p><p>".ucwords($data['subject'])." <br> ".ucwords($data['remarks'])."</p></td>
														</tr>

														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>														
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

  	// Adoch Request success notification

 	function submitKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data){
 		$cusinfo 		= $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
 		$propertyinfo   = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$property_id."' ");

	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear ".ucwords($cusinfo['name']).", </span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>
															A new Adhoc request ".$ticket_uid." has been rasied for the property ".ucwords($propertyinfo['title']).". The details are as below.</p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p><b>Request Details:</b> </p><p>".ucwords($data['subject'])." <br> ".ucwords($data['remarks'])."</p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>Thanks for writing to us. One of our support agent will look on to the request and get back to you.</p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>Login to you account to view more details. </p></td>
														</tr>


														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>														
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

  	// Adoch Request success notification

 	function replyKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data,$ref_id){
 		$info = $this -> getDetails(ADOC_REQUEST,"*"," id ='".$ref_id."' ");
 		$cusinfo 		= $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
 		$propertyinfo   = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$property_id."' ");
 		$today = date ("d M'Y H:i a");
	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear Admin, </span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>
															A new reply for the Adhoc request ".$ticket_uid." has been posted by the ".ucwords($cusinfo['name'])." for the property ".ucwords($propertyinfo['title']).". The details are as below.</p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>

														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>Customer Name: ".ucwords($cusinfo['name'])."<br>Mobile: ".$cusinfo['mobile']." <br>Email: ".$cusinfo['email']." <br>Property: ".ucwords($propertyinfo['title'])." </p></td>
														</tr>

														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p><b>Request Details:</b> </p><p>".ucwords($info['subject'])." <br> ".ucwords($info['remarks'])."</p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p><b>New Reply on ".$today.":</b> </p><p>".ucwords($info['remarks'])."</p></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>														
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

  	// KYC success notification

 	function KYCDocumnetSubmitNotification($customer_name){
 		
	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear Admin, </span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>New KYC document has been posted by the ".ucwords($customer_name).". Kindly login to the admin dashboard to check on the status.</p></td>
														</tr>
														
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>														
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

  	// Forgot Password Tempalate

 	function forgotPasswordTemp($name,$token){
 		
	    $layout = "
			<div bgcolor='#d9e8f3' style='margin:0;padding:0'>
				<table cellpadding='0' cellspacing='0' border='0' width='100%' bgcolor='#e1e8ed' style='background-color:#d9e8f3;padding:0;margin:0;line-height:1px;font-size:1px'>
					<tbody>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'>							
							</td>
						</tr>
						<tr>
							<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
								<table align='center' width='750' style='width:750px;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#ffffff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td height='10' style='line-height:1px;display:block;height:10px;padding:0;margin:0;font-size:1px'></td>
										</tr>
									</tbody>
								</table>  
								<table align='center' width='750' style='width:750px;background-color:#fff;padding:0;margin:0;line-height:1px;font-size:1px' bgcolor='#fff' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table cellpadding='0' cellspacing='0' border='0' width='100%' style='width:100%;padding:0;margin:0;line-height:1px;font-size:1px' align='left'>
													<tbody>
														<tr>
															<td align='left' width='15' style='width:15px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='left' width='160' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<a href='".BASEPATH."' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;' target='_blank'>
																<img align='left' width='160' src='".BASEPATH."resource/uploads/logo.png' style='width:160px;padding-bottom:2px;margin:0;padding:0;display:block;border:none;outline:none' class='CToWUd'></a> 
															</td>
															<td align='left' width='10' style='width:10px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
															<td align='right' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:5px;margin:0px;font-weight:300;line-height:100%;text-align:right'></td>
														</tr>	
													</tbody>
												</table> 
											</td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:10px 0 0 0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td colspan='2' height='1' style='line-height:1px;display:block;height:1px;background-color:#e1e8ed;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>

								<table align='center' width='750' style='width:750px;background-color:#ffffff;padding:0;margin:0;line-height:1px;font-size:1px' cellpadding='0' cellspacing='0' border='0'>
									<tbody>
										<tr>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
											<td align='center' style='padding:0;margin:0;line-height:1px;font-size:1px'>
												<table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
													<tbody>
														<tr>
															<td height='30' style='height:30px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'> <span style='font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:20px;padding:0px;margin:0px;font-weight:600;line-height:100%;text-align:left'>Dear ".$name.",</span> </td>
														</tr>
														<tr>
															<td height='12' style='height:12px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>


														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:16px;padding:0px;margin:0px;font-weight:300;line-height:23px;text-align:left'><p>You have requested for Password Reset for your account. To Reset your password just click the link below and create a new password for your account. </p></td>
														</tr>
														<tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																<table border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																	<tbody>
																		<tr>
																			<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
																				<table width='100%' border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																					<tbody>
																						<tr>
																							<td style='padding:0;margin:0;line-height:1px;font-size:1px'>
																								<table border='0' cellspacing='0' cellpadding='0' style='padding:0;margin:0;line-height:1px;font-size:1px'>
																									<tbody>
																										<tr>
																											<td align='center' bgcolor='#e16f30' style='padding:0;margin:0;line-height:1px;font-size:1px;border-radius:4px;line-height:18px'>
																												<a href='".BASEPATH."resetpassword/email/".$token."?utm_ref=email&utm_type=reset&utf=$token' style='text-decoration:none;border-style:none;border:0;padding:0;margin:0;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:22px;font-weight:500;color:#ffffff;text-align:center;text-decoration:none;border-radius:4px;padding:11px 30px;border:1px solid #e16f30;display:inline-block' target='_blank'>
																										 		<strong>Reset Password</strong></a>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table>
																			</td>
																		</tr>
																	</tbody>
																</table> 
															</td>
														</tr>
														<td height='22' style='height:22px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														<tr>
															<td align='left' style='padding:0;margin:0;line-height:1px;font-family:Helvetica,Arial,sans-serif;color:#66757f;font-size:13px;padding:0px;margin:0px;font-weight:300;line-height:16px;text-align:left'> <br><br><br><p>Regards,<br>
																".COMPANY_NAME."<br>
																Mobile: ".MOBILE." <br>
																Email: ".EMAIL."<br>
																Web: ".WEBSITE."<br>
																Address: ".ADDRESS."</p></td>
														</tr>		
														<tr>
															<td height='40' style='height:40px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
														</tr>
														
														
														
													</tbody>
												</table> 
											</td>
											<td width='50' style='width:50px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
										</tr>
									</tbody>
								</table>
						 	</td>
						</tr>
						<tr>
							<td align='center' height='70' style='height:70px;padding:0;margin:0;line-height:1px;font-size:1px'></td>
						</tr>
					</tbody>
				</table>
			</div>";
		return $layout;
  	}

	// Contact Form HTML Template

	function contactTemplate($data)
	{
		$msg= "<table width='500' border='0' align='center' cellpadding='10' cellspacing='10' style='font-family:Arial, Helvetica, sans-serif; font-size:10pt; border:1px solid #ccc;'> ";
		$msg.= "<tr>";
		$msg.= "<td width='16' height='25'>&nbsp;</td>";
		$msg.= "<td >Name</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['name']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Email Id </td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['email']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Mobile No</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['mobile']."</td>";
		$msg.= "</tr>";
		$msg.= "<tr>";
		$msg.= "<td >&nbsp;</td>";
		$msg.= "<td >Message</td>";
		$msg.= "<td ><strong>:</strong></td>";
		$msg.= "<td >".$data['message']."</td>";
		$msg.= "</tr>";
		$msg.= "</table>";
		return $msg;
	}

	// Contact Success

	function contactUserTemp($name)
	{
		$msg= "<h2>Dear ".ucwords($name).",</h2>
		     	<p>Thank you for contacting us. We will get back to you soon.</p><br><br>
				Regards,<br>
				<strong>".COMPANY_NAME."</strong><br>
				Web: www.credaicoimbatore.com <br>
				Email: credaicbe@gmail.com <br>
				Phone: (+91) 94422 62202. <br>
				";
		return $msg;
	}

	// subscribe Success

	function subscribeUserTemp()
	{
		$msg= "<h2>Dear Sir/Mam,</h2>
		     	<p>Thank you for subscribe the monthly Newsletter.</p><br><br>
				Regards,<br>
				<strong>".COMPANY_NAME."</strong><br>
				Web: www.credaicoimbatore.com <br>
				Email: credaicbe@gmail.com <br>
				Phone: (+91) 94422 62202. <br>
				";
		return $msg;
	}

	function indianMoneyFormate($amount) {
     $Arraycheck = array("4"=>"K","5"=>"K","6"=>"L","7"=>"L","8"=>"Cr","9"=>"Cr");
     // define decimal values
     $numberLength = strlen($amount); //count the length of numbers
     if ($numberLength > 3) {
        foreach ($Arraycheck as $Lengthnum=>$unitval) {
            if ($numberLength == $Lengthnum) {
               if ($Lengthnum % 2 == 0) {
                  $RanNumber = substr($amount,1,2);
                  $NmckGtZer = ($RanNumber[0]+$RanNumber[1]);
                  if ($NmckGtZer < 1) { 
                      $RanNumber = "0";
                  } else {
                     if ($RanNumber[1] == 0) {
                        $RanNumber[1] = "0";
                  } 
             }
      $amount = substr($amount,0,$numberLength-$Lengthnum+1) . "." . $RanNumber . " $unitval ";
    } else {
         $RanNumber = substr($amount,2,2);
         $NmckGtZer = ($RanNumber[0]+$RanNumber[1]);
         if ($NmckGtZer < 1) { 
            $RanNumber  = 0;
        } else {
            if ($RanNumber[1] == 0)  {
                $RanNumber[1] = "0";
            }   
        }
         $amount = substr($amount,0,$numberLength-$Lengthnum+2) . ".". $RanNumber . " $unitval";   
       }
     }
	 }
	 } else {
	     '₹ '.$amount;    
	 }
	 return '₹ '.$amount;
	 }

	/*---------	------------------------------------
				Session In and Out
	----------------------------------------------*/ 

	// Get User Agent

	function get_user_agent() {
		if ( !isset( $_SERVER['HTTP_USER_AGENT'] ) )
		return '-';		
		$ua = strip_tags( html_entity_decode( $_SERVER['HTTP_USER_AGENT'] ));
		$ua = preg_replace('![^0-9a-zA-Z\':., /{}\(\)\[\]\+@&\!\?;_\-=~\*\#]!', '', $ua );			
		return substr( $ua, 0, 254 );
	}
	
	// Get User IP Address

	function get_IP() {
		$ip = '';
		// Precedence: if set, X-Forwarded-For > HTTP_X_FORWARDED_FOR > HTTP_CLIENT_IP > HTTP_VIA > REMOTE_ADDR
		$headers = array( 'X-Forwarded-For', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP', 'HTTP_VIA', 'REMOTE_ADDR' );
		foreach( $headers as $header ) {
			if ( !empty( $_SERVER[ $header ] ) ) {
				$ip = $_SERVER[ $header ];
				break;
			}
		}		
		// headers can contain multiple IPs (X-Forwarded-For = client, proxy1, proxy2). Take first one.
		if ( strpos( $ip, ',' ) !== false )
			$ip = substr( $ip, 0, strpos( $ip, ',' ) );		
		return $ip;
	}	

	// Change the Date format

	function changeDateFormat($date)
	{
		$array = explode("/", $date);
		$new_date = $array[2]."/".$array[1]."/".$array[0];
		$date 			= date_create($new_date);
		$final_date		= date_format($date,"Y-m-d");
		return $final_date;
	}

	function newchangeDateFormat($date)
	{
		$array = explode("/", $date);
		$new_date = $array[2]."/".$array[0]."/".$array[1];
		$date 			= date_create($new_date);
		$final_date		= date_format($date,"Y-m-d");
		return $final_date;
	}


	// Session In

	function sessionIn($user_type,$finance_type,$id,$referer="",$medium="")
	{		
		$auth_user_agent =	$this->get_user_agent();
		$auth_ip_address =	$this->get_IP();
		$curr 	= date("Y-m-d H:i:s");
		$q 		= "INSERT INTO ".SESSION_TBL." SET logged_id ='".$id."', user_type='$user_type', portal_type='$finance_type', auth_referer='$referer', auth_medium='$medium', auth_user_agent='$auth_user_agent', auth_ip_address='$auth_ip_address', session_in='$curr'  ";
		$exe = $this->selectQuery($q);
		if ($exe) {
			return 1;
		}else{
			return 0;
		}
	}

	// Session Out

	function sessionOut($id)
	{
		$today 	= date("Y-m-d");
		$curr 	= date("Y-m-d H:i:s");
		$info 	= $this->getDetails(SESSION_TBL,"id"," logged_id='".$id."' ORDER BY id DESC LIMIT 1");
		$q 		= "UPDATE ".SESSION_TBL." SET session_out='$curr' WHERE logged_id='".$id."' AND id='".$info['id']."'  ";
		$exe = $this->selectQuery($q);
		if($exe) {
			return 1;
		}else{
			return 0;
		}
	}


	function getKYCType($current="")
	{
		$layout ="";
		$query = "SELECT id,token,kyc_type,status  FROM ".KYC_TYPE." WHERE status='1' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['kyc_type'])."</option>";
			}
		}
		return $layout;
	}



}


?>