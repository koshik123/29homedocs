<?php

require_once './../global/global-config.php';
require_once '../config/config.php';
require_once '../app/models/Model.php';
require_once 'classes/PHPMailerAutoload.php';

// Require AWS
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

class Ajaxcontroller extends Model
{	

	/*--------------------------------------------- 
				Login Authentication
	----------------------------------------------*/ 

	// User login

	function userLogin($data){
		$email 		= $data['email'];
		$passw 		= $data['password'];
    	$password 	= $this->encryptPassword($passw);
		$check = $this -> check_query(CUSTOMER_TBL,"id"," email ='$email' AND password ='$password' AND status='1' ");
		if($check == 1){        	
        	$userinfo = $this->getDetails(CUSTOMER_TBL,"id,email"," email='$email' AND password='$password' ");
        	$propertyCountInfo =  $this->check_query(PROPERTY_TBL,"id"," deleted_status='0' AND status='1' AND cancel_status='0' AND customer_id='".$userinfo['id']."' ");
        	$propertyInfo =  $this->getDetails(PROPERTY_TBL,"id"," deleted_status='0' AND status='1' AND cancel_status='0' AND customer_id='".$userinfo['id']."' ");

         	$_SESSION["crm_member_id"] = $userinfo["id"];  
         	if ($propertyCountInfo==1) {
         		$_SESSION["crm_property_id"] = $propertyInfo["id"];
         		$_SESSION["crm_property_count"] = $propertyCountInfo;  
         	}elseif($propertyCountInfo>1){
         		$_SESSION["crm_property_count"] = $propertyCountInfo; 
         	}else{
         		$_SESSION["crm_property_count"] = $propertyCountInfo; 
         	}
         	//return $propertyCountInfo;
         	/*if ($userinfo["property_id"]!='') {
         		$_SESSION["crm_property_id"] = $userinfo["property_id"];  
         	}*/
         	if($this->sessionIn($user_type="user",$finance_type="Post CRM",$_SESSION["crm_member_id"],"web","browser")){
         		return 1;
         	}   
        }else{
        	return $this->errorMsg("Sorry your account details are wrong.");
        }
	}

	// Edit Profile 

	function editProfile($data)
	{
		if(isset($_SESSION['edit_profile_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['edit_profile_key']){			
					$user_id 		= $_SESSION["crm_member_id"];

					$curr 			= date("Y-m-d H:i:s");
					$token 			= $this->generateRandomString("40");
					$id 			= $this->decryptData($data['session_token']);
					$query = "UPDATE ".CUSTOMER_TBL." SET 
						token 					= '".$this->hyphenize($data['name'])."',
						name 					= '".$this->cleanString($data['name'])."',
						email 					= '".$this->cleanString($data['email'])."',
						mobile 					= '".$this->cleanString($data['mobile'])."',
						gender 					= '".$this->cleanString($data['gender'])."',
						address 				= '".$this->cleanString($data['address'])."',
						state 					= '".$this->cleanString($data['state'])."',
						city 					= '".$this->cleanString($data['city'])."',
						pincode 				= '".$this->cleanString($data['pincode'])."', 
						updated_at 				= '$curr' WHERE id='$id' ";
					$exe = $this->selectQuery($query);
					if($exe){
						unset($_SESSION['edit_profile_key']);
						return 1;
					}else{
						return "Sorry!! Unexpected Error Occurred. Please try again.";
					}				
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	// Change Password

	function changePassword($data)
	{
		if(isset($_SESSION['change_password_key'])){
			if($this->cleanString($data['fkey']) == $_SESSION['change_password_key']){
				$curr 			= date("Y-m-d H:i:s");
   		 		$check_password = $this->check_query(CUSTOMER_TBL,"id", " id= '".$_SESSION['crm_member_id']."' AND
			          password = '".$this->encryptPassword($data['password'])."' ");
			    if($check_password==1){
			        $query= "UPDATE ".CUSTOMER_TBL." SET
			                password = '". $this->encryptPassword($data['re_password'])."',
			                updated_at='$curr' WHERE id='".$_SESSION['crm_member_id']."' "; 
	                $exe = $this->selectQuery($query);
	                if($exe){
						unset($_SESSION['change_password_key']);
						return 1;
					}else{
						return "Sorry!! Unexpected Error Occurred. Please try again.";
					}
			    }else{
			        return $this->errorMsg("Your Current Password is wrong.");
			    }
			}else{
				return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}
	}

	// Forgot password

	function forgot_password($data){
		$email 		= $data['email'];
		$check = $this -> check_query(CUSTOMER_TBL,"id"," email='$email' ");
		if($check == 1){
			$curr 			= date("Y-m-d H:i:s"); 
			$userinfo 		= $this ->getDetails(CUSTOMER_TBL,"*"," email='$email' ");
			$q 				= "UPDATE ".CUSTOMER_TBL." SET resetpassword='1' WHERE id='".$userinfo['id']."' ";
			$exe = $this->selectQuery($q);
			if($exe){
				$sender_mail    = NO_REPLY;
	            $subject        = COMPANY_NAME." - Reset your password";
	            $receiver       = $userinfo['email'];
	            //$user_token     = $userinfo['em_token'];
	            $user_token 	= $this->encryptData($userinfo['token']);
	            $email_temp     = $this->forgotPasswordTemp($userinfo['name'],$user_token);            
	            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
	            //return $email_temp ;
	            if ($sendemail) {
	            	return $this->successMsg("Password Reset Link have been Sent to your email Address - ".$userinfo['email']." ");	            	
	            }			
			}else{
				return $this->errorMsg("Sorry Unexpected error Occurred Please Try again.");	
			}  	
        }else{
        	return $this->errorMsg("Sorry Entered Email Address doesn't exists. Please enter your registered email address.");        	
        }
	}

	// Reset the password 

	function resetPassword($data)
	{
		$data 			= explode("`",$data);
		//$token			= $this->decryptData($this->cleanString($data[0]));
		$token			= ($this->cleanString($data[0]));
		$password		= $this->cleanString($data[1]);
		$hash_password 	= $this->encryptPassword($password);
		$curr 			= date("Y-m-d H:i:s");
		$check 			= $this->check_query(CUSTOMER_TBL,"id"," token='$token' and resetpassword='1' ");
		if($check==1){
			$q = "UPDATE ".CUSTOMER_TBL." SET
		 		password			= '$hash_password',
		 		has_psw 			= '$password',
		 		updated_at			= '$curr',
	 			resetpassword 		= '0' WHERE token='$token' ";
			$exe = $this->selectQuery($q);
			if($exe){
				unset($_SESSION['reset_password_key']);
				return 1;
			}else{
				return $this->errorMsg("Sorry!! Unexpected Error Occurred. Please try again.");
			}
		}else{
			return $this->errorMsg("Invalid user details. Please click on the Link send your email address to reset password.");
		}
	}


	/*=====================================
	        General Documents
	=======================================*/

    function viewGenDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(GENDRAL_DOCUMENTS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='$property_id' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $layout = " 
        		<div class='modal-content'>
	                <div class='modal-header align-center'>
	                    <div class='nk-file-title'>
	                        <div class='nk-file-icon'>
	                            <span class='nk-file-icon-type'>
	                                <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
	                                    <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
	                                    <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
	                                    <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
	                                    <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
	                                </svg>
	                            </span>
	                        </div>
	                        <div class='nk-file-name'>
	                            <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
	                            <div class='nk-file-name-sub'>Project</div>
	                        </div>
	                    </div>
	                    <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
	                </div>
	                <div class='modal-body'>
	                    <div class='nk-file-details'>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Title</div>
	                            <div class='nk-file-details-col'>".$info['title']."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Type</div>
	                            <div class='nk-file-details-col'>".$info['document_type']."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Owner</div>
	                            <div class='nk-file-details-col'>".ucwords($ownerinfo['name'])."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Description</div>
	                            <div class='nk-file-details-col'>".($info['description'])."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Created</div>
	                            <div class='nk-file-details-col'>".date("M d, Y",strtotime($info['created_at']))."</div>
	                        </div>
	                    </div>
	                </div><!-- .modal-body -->
	                <div class='modal-footer modal-footer-stretch bg-light'>
	                    <div class='modal-footer-between'>
	                        <div class='g'>
	                            <ul class='btn-toolbar g-3'>
	                                <li><a href='".DOCUMENTS."".$info['documents']."' download='".DOCUMENTS."".$info['documents']."' target='_blank' class='btn btn-primary file-dl-toast'>Download</a></li>
	                                <li><a href='".BASEPATH."documents/generalrequest/".$info['id']."' class='title btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	            </div>
            ";
            /* <li><a href='javascript:void();' data-option='".$info['id']."' class='title raiseAdhocRequest btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>*/
                
        $result['layout'] = $layout;
        return $result;
    }

    /*=====================================
	        Private Documents
	=======================================*/

    function viewPrivateDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='$property_id' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $doc_status = (($info['doc_status']!=0) ? "<div class='modal-footer modal-footer-stretch bg-light'>
	                    <div class='modal-footer-between'>
	                        <div class='g'>
	                            <ul class='btn-toolbar g-3'>
	                                <li><a href='".DOCUMENTS."".$info['document_file']."' download='".DOCUMENTS."".$info['document_file']."' target='_blank' class='btn btn-primary file-dl-toast'>Download</a></li>
	                                <li><a href='".BASEPATH."documents/privaterequest/".$info['id']."' class='title btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>
	                                
	                            </ul>
	                        </div>
	                    </div>
	                </div>" : "<div class='modal-footer modal-footer-stretch bg-light'>
	                    <div class='modal-footer-between'>
	                        <div class='g'>
	                            <ul class='btn-toolbar g-3'>
	                               
	                                <li><a href='".BASEPATH."documents/privaterequest/".$info['id']."' class='title btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>
	                                
	                            </ul>
	                        </div>
	                    </div>
	                </div>");
        $layout = " 
        		<div class='modal-content'>
	                <div class='modal-header align-center'>
	                    <div class='nk-file-title'>
	                        <div class='nk-file-icon'>
	                            <span class='nk-file-icon-type'>
	                                <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
	                                    <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
	                                    <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
	                                    <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
	                                    <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
	                                </svg>
	                            </span>
	                        </div>
	                        <div class='nk-file-name'>
	                            <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
	                            <div class='nk-file-name-sub'>Project</div>
	                        </div>
	                    </div>
	                    <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
	                </div>
	                <div class='modal-body'>
	                    <div class='nk-file-details'>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Title</div>
	                            <div class='nk-file-details-col'>".$info['doc_name']."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Type</div>
	                            <div class='nk-file-details-col'>".$info['doc_type']."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Owner</div>
	                            <div class='nk-file-details-col'>".ucwords($ownerinfo['name'])."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Description</div>
	                            <div class='nk-file-details-col'>".($info['description'])."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Created</div>
	                            <div class='nk-file-details-col'>".date("M d, Y",strtotime($info['created_at']))."</div>
	                        </div>
	                    </div>
	                </div>
	                ".$doc_status."
	            </div>
                 ";
        $result['layout'] = $layout;
        return $result;

    }

    /*=====================================
	        Private Common Documents
	=======================================*/

    function viewPrivateCommonDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='$property_id' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $layout = " 
        		<div class='modal-content'>
	                <div class='modal-header align-center'>
	                    <div class='nk-file-title'>
	                        <div class='nk-file-icon'>
	                            <span class='nk-file-icon-type'>
	                                <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
	                                    <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
	                                    <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
	                                    <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
	                                    <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
	                                </svg>
	                            </span>
	                        </div>
	                        <div class='nk-file-name'>
	                            <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
	                            <div class='nk-file-name-sub'>Project</div>
	                        </div>
	                    </div>
	                    <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
	                </div>
	                <div class='modal-body'>
	                    <div class='nk-file-details'>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Title</div>
	                            <div class='nk-file-details-col'>".$info['title']."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Type</div>
	                            <div class='nk-file-details-col'>".$info['document_type']."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Owner</div>
	                            <div class='nk-file-details-col'>".ucwords($ownerinfo['name'])."</div>
	                        </div>
	                        
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Description</div>
	                            <div class='nk-file-details-col'>".($info['description'])."</div>
	                        </div>
	                        <div class='nk-file-details-row'>
	                            <div class='nk-file-details-col'>Created</div>
	                            <div class='nk-file-details-col'>".date("M d, Y",strtotime($info['created_at']))."</div>
	                        </div>
	                    </div>
	                </div>
	                <div class='modal-footer modal-footer-stretch bg-light'>
	                    <div class='modal-footer-between'>
	                        <div class='g'>
	                            <ul class='btn-toolbar g-3'>
	                                <li><a href='".DOCUMENTS."".$info['documents']."' download='".DOCUMENTS."".$info['documents']."' target='_blank' class='btn btn-primary file-dl-toast'>Download</a></li>
	                                <li><a href='".BASEPATH."documents/privategeneralrequest/".$info['id']."' class='title btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>
	                                
	                            </ul>
	                        </div>
	                    </div>
	                </div>
	            </div>
                 ";
        $result['layout'] = $layout;
        return $result;

    }


	 /*----------------------------
        	Adhoc requests
    ------------------------------*/

    // Add 

    function addticket($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['add_request_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_request_key']){
                $token          = $this->generateRandomString("30");
                $property_id = $_SESSION["crm_property_id"];
                $user_id	 = $_SESSION["crm_member_id"];
                $curr           = date("Y-m-d H:i:s");
                $query = "INSERT INTO ".ADOC_REQUEST." SET
                		property_id     = '".$this->cleanString($property_id)."',
                		customer_id 	= '".$this->cleanString($user_id)."',
                		employee_id  	= '0',
                		ref_id  		= '0',
                		request_type 	= 'user',
                		type 			= 'common',
                		requesttype_id 	= '".$this->cleanString($data['request_id'])."',
                		subject 		= '".$this->cleanString($data['subject'])."',
                		remarks 		= '".$this->cleanString($data['remarks'])."',
                        added_by        = '$user_id',
                        status          = '1',
                        assign_status   = '0',
                        closed_status   = '0',
                        assign_employee_id = '0',
                        request_status  = '0',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                        
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                	$ticket_uid = "T".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".ADOC_REQUEST." SET ticket_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                    if($result){
                    	$cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
	                    // To Admin
	                    $sender_mail    = NO_REPLY;
			            $subject        = COMPANY_NAME." - New Adhoc request ".$ticket_uid." has been raised by ".ucwords($cusinfo['name']);
			            $receiver       = ADMIN_EMAIL;
			            $email_temp     = $this->newKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data);            
			            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

			            if ($sendemail) {
			            	// To Customer
		                    $csender_mail    = NO_REPLY; 
				            $csubject        = COMPANY_NAME." - Your Adhoc request ".$ticket_uid." has been raised successfully ";
				            $creceiver       = $cusinfo['email'];
				            $cemail_temp     = $this->submitKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data);            
				            $csendemail      = $this->send_mail($csender="",$csender_mail,$creceiver,$csubject,$cemail_temp,$cbcc="");

				            $options['ref_id'] 		= $last_id;
							$options['created_by'] 	= $user_id;
							$notification 			= $this->requestLogs("created",$options,$images,$data);	

							unset($_SESSION['add_request_key']);
		                    return 1;
			            }
                    }
                    
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*function addAdhocRequest($data)
    {
        $layout = "";
        if(isset($_SESSION['add_request_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_request_key']){
                $token          = $this->generateRandomString("30");
                $property_id = $_SESSION["crm_property_id"];
                $user_id	 = $_SESSION["crm_member_id"];
                $curr           = date("Y-m-d H:i:s");
                $query = "INSERT INTO ".ADOC_REQUEST." SET
                		property_id     = '".$this->cleanString($property_id)."',
                		customer_id 	= '".$this->cleanString($user_id)."',
                		employee_id  	= '0',
                		request_type 	= 'user',
                		requesttype_id 	= '".$this->cleanString($data['request_id'])."',
                		subject 		= '".$this->cleanString($data['subject'])."',
                		remarks 		= '".$this->cleanString($data['remarks'])."',
                        added_by        = '$user_id',
                        status          = '1',
                        request_status  = '0',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                         //  return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                	$ticket_uid = "T".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".ADOC_REQUEST." SET ticket_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                	$options['ref_id'] 		= $last_id;
					$options['created_by'] 	= $user_id;
					$notification 			= $this->requestLogs("created",$options);	

                    unset($_SESSION['add_request_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }*/

    /*function postOnWallImg($images=[],$data)
	{
		if(isset($_SESSION['add_request_key'])){
			if($this->cleanString($data['fkey']) ==$_SESSION['add_request_key']){
				$token          = $this->generateRandomString("30");
                $property_id = $_SESSION["crm_property_id"];
                $user_id     = $_SESSION["crm_member_id"];
                $curr           = date("Y-m-d H:i:s");
				
				
				$query = "INSERT INTO ".ADOC_REQUEST." SET
						property_id     = '".$this->cleanString($property_id)."',
                        customer_id     = '".$this->cleanString($user_id)."',
                        employee_id     = '0',
                        request_type    = 'user',
                        requesttype_id  = '".$this->cleanString($data['request_id'])."',
                        subject         = '".$this->cleanString($data['subject'])."',
                        remarks         = '".$this->cleanString($data['remarks'])."',
                        added_by        = '$user_id',
                        status          = '1',
                        request_status  = '0',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
			 	//return "0`".$q;
				$link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =  mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
				if($exe){
					$ticket_uid = "T".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".ADOC_REQUEST." SET ticket_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                    $options['ref_id']      = $last_id;
                    $options['created_by']  = $user_id;
                    $notification           = $this->requestLogs("created",$options);   
					
					if(count($images)>0){
						foreach ($images as $value) {							
				           	$output = $value;
				           	$qu = "INSERT INTO ".ADOC_IMAGES." SET
				           		adoc_id='$last_id',
				           		property_id     = '".$this->cleanString($property_id)."',
                        		customer_id     = '".$this->cleanString($user_id)."',
				           		image_name 		='$output', 
				           		status 			='1', 
				           		created_at 		= '$curr', 
				           		updated_at	 	= '$curr'  ";
				           	$exe2 = $this->selectQuery($qu);
				        }
				       
				        unset($_SESSION['add_request_key']);
                    	return 1;
					}else{
						unset($_SESSION['add_request_key']);
                    	return 1;
					}
				}else{
					return "Sorry!! Unexpected Error Occurred. Please try again.";
				}
			}else{
				return "Sorry Invalid User Entry. Try Relogin to the Panel to continue.";
			}
		}else{
			return "Sorry Invalid User Entry. Try Relogin to the Panel to continue.";
		}
	}*/

    /*--------------------------------------------- 
				Request Logs
	----------------------------------------------*/

	// Add Request Logs

	function requestLogs($type,$options,$images,$data)
	{	
		$curr  = date("Y-m-d H:i:s");
		$property_id = $_SESSION["crm_property_id"];
        $user_id	 = $_SESSION["crm_member_id"];
		$created_by  = $options['created_by'];				
		$ref_id 	 = $options['ref_id'];
		$query = "INSERT INTO ".REQUEST_LOGS." SET
			type 		= 'created',
			request_type= 'user',
			created_by  = '$created_by',
			created_to  = '0',
			ref_id 		= '$ref_id',
			remarks  	= '".$this->cleanString($data['remarks'])."',
			status 		= '1',
			created_at 	= '$curr',
			updated_at 	= '$curr' ";
		//$exe = $this->selectQuery($query);
		$link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
        $exe = mysqli_query($link,$query);
        $last_id = mysqli_insert_id($link);

		if ($exe) {
			if(count($images)>0){
				$content_array 	= $data['adhoc_row'];
				$description 	= array();
				foreach ($content_array as $key => $value) {
					$description[] 			= $this->cleanString($value['description']);		
				}
				$i = 0;
				foreach ($images as $value) {	
					$output 			 = str_replace("uploads/srcimg/", "", $value);
		           	$image_info 		 = explode(".", $output); 
					$image_type 		 = end($image_info);
					$insert_description  = $description[$i];	

					// Upload to S3
	                $targetPath = "../resource/uploads/srcimg/".$output;
	                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'front_srcimg/'.$output, S3::ACL_PUBLIC_READ);
	                unlink($targetPath);

		           	$qu = "INSERT INTO ".ADOC_IMAGES." SET
		           		adoc_id         = '$ref_id',
		           		ref_id         	= '$last_id',
		           		property_id     = '".$this->cleanString($property_id)."',
	            		customer_id     = '".$this->cleanString($user_id)."',
		           		image_name 		= '$output',
		           		file_type 		= '$image_type',
		           		description     = '".$insert_description."', 
		           		status 			= '1', 
		           		created_at 		= '$curr', 
		           		updated_at	 	= '$curr'  ";
		           	$exe2 = $this->selectQuery($qu);
		           	$i++;
		        }
	        	return 1;
			}else{
				return 1;
			}
		}else{
			return "Sorry!! Unexpected Error Occurred. Please try again.";
		}
	}

	function replayAdhocRequest($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['reply_ticket_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['reply_ticket_key']){
                $token          = $this->generateRandomString("30");
                $user_id	 = $_SESSION["crm_member_id"];
                $property_id = $_SESSION["crm_property_id"];
                $curr           = date("Y-m-d H:i:s");
                $query = "INSERT INTO ".REQUEST_LOGS." SET
                    type        = 'reply',
                    request_type= 'user',
                    created_by  = '$user_id',
                    created_to  = '0',
                    ref_id      = '".$data['ref_id']."',
                    remarks     = '".$data['remarks']."',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
                         //  return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){

                	$info = $this -> getDetails(ADOC_REQUEST,"*"," id ='".$data['ref_id']."' ");
                	$cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$info['customer_id']."' ");
                	$propertyinfo = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$info['property_id']."' ");
                	// To Admin reply 
                    
					if(count($images)>0){
		                $content_array  = $data['adhoc_row'];
		                $description    = array();
		                foreach ($content_array as $key => $value) {
		                    $description[]          = $this->cleanString($value['description']);        
		                }
		                $i = 0;
		                foreach ($images as $value) {   
		                    $output              = str_replace("uploads/srcimg/", "", $value);
		                    $image_info          = explode(".", $output); 
		                    $image_type          = end($image_info);
		                    $insert_description  = $description[$i];  

		                    // Upload to S3
			                $targetPath = "../resource/uploads/srcimg/".$output;
			                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'front_srcimg/'.$output, S3::ACL_PUBLIC_READ);
			                unlink($targetPath);
		                      
		                    $qu = "INSERT INTO ".ADOC_IMAGES." SET
		                        adoc_id         = '".$data['ref_id']."',
		                        ref_id          = '".$last_id."',
		                        property_id     = '".$this->cleanString($property_id)."',
		                        customer_id     = '".$this->cleanString($user_id)."',
		                        image_name      = '$output',
		                        file_type       = '$image_type',
		                        description     = '".$insert_description."', 
		                        status          ='1', 
		                        created_at      = '$curr', 
		                        updated_at      = '$curr'  ";
		                    $exe2 = $this->selectQuery($qu);
		                    $i++;
		                }
		                // TO Admin Reply
		                $sender_mail    = NO_REPLY;
		            	$subject        = COMPANY_NAME." - New reply for the Adhoc request ".$info['ticket_uid']." by ".ucwords($cusinfo['name'])." for the property ".ucwords($propertyinfo['title']) ;
		            	$receiver       = ADMIN_EMAIL;
		            	$email_temp     = $this->replyKYCSubmitNotification($info['ticket_uid'],$info['customer_id'],$info['property_id'],$data,$data['ref_id']);            
		            	$sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

		                unset($_SESSION['reply_ticket_key']);
                    	return 1;
		            }else{
		            	// TO Admin Reply
		            	$sender_mail    = NO_REPLY;
		            	$subject        = COMPANY_NAME." - New reply for the Adhoc request ".$info['ticket_uid']." by ".ucwords($cusinfo['name'])." for the property ".ucwords($propertyinfo['title']) ;
		            	$receiver       = ADMIN_EMAIL;
		            	$email_temp     = $this->replyKYCSubmitNotification($info['ticket_uid'],$info['customer_id'],$info['property_id'],$data,$data['ref_id']);            
		            	$sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
		                unset($_SESSION['reply_ticket_key']);
                    	return 1;
		            }
                    
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

   // Avtive & Inactive Status 

    function closeTicket($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(ADOC_REQUEST,"request_status"," id ='$data' ");
        if($info['request_status'] ==1){
            $query = "UPDATE ".ADOC_REQUEST." SET request_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".ADOC_REQUEST." SET request_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     /*----------------------------------------
         General Documents Adhoc Request
    --------------------------------------------*/

    // Add 

    function generalDocRequests($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['add_generaldoc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_generaldoc_key']){
                $token          = $this->generateRandomString("30");
                $property_id = $_SESSION["crm_property_id"];
                $user_id	 = $_SESSION["crm_member_id"];
                $curr           = date("Y-m-d H:i:s");
                $query = "INSERT INTO ".ADOC_REQUEST." SET
                		property_id     = '".$this->cleanString($property_id)."',
                		customer_id 	= '".$this->cleanString($user_id)."',
                		employee_id  	= '0',
                		ref_id  		= '".$this->cleanString($data['ref_id'])."',
                		type 			= '".$this->cleanString($data['type'])."',
                		request_type 	= 'user',
                		requesttype_id 	= '".$this->cleanString($data['request_id'])."',
                		subject 		= '".$this->cleanString($data['subject'])."',
                		remarks 		= '".$this->cleanString($data['remarks'])."',
                        added_by        = '$user_id',
                        status          = '1',
                        assign_status   = '0',
                        closed_status   = '0',
                        assign_employee_id = '0',
                        request_status  = '0',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                        
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                	$ticket_uid = "T".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".ADOC_REQUEST." SET ticket_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                    $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
                    // To Admin
                    $sender_mail    = NO_REPLY;
		            $subject        = COMPANY_NAME." - New Adhoc request ".$ticket_uid." has been raised by ".ucwords($cusinfo['name']);
		            $receiver       = ADMIN_EMAIL;
		            $email_temp     = $this->newKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data);            
		            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

		            // To Customer
                    $csender_mail    = NO_REPLY; 
		            $csubject        = COMPANY_NAME." - Your Adhoc request ".$ticket_uid." has been raised successfully ";
		            $creceiver       = $cusinfo['email'];
		            $cemail_temp     = $this->submitKYCSubmitNotification($ticket_uid,$user_id,$property_id,$data);            
		            $csendemail      = $this->send_mail($csender="",$csender_mail,$creceiver,$csubject,$cemail_temp,$cbcc="");

                	$options['ref_id'] 		= $last_id;
					$options['created_by'] 	= $user_id;
					$notification 			= $this->requestLogs("created",$options,$images,$data);	

					unset($_SESSION['add_generaldoc_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


   	/*----------------------------
        	KYC
    ------------------------------*/

    // Add 

    function addKyc($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['add_kyc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_kyc_key']){
                $token          = $this->generateRandomString("30");
                $property_id 	= $_SESSION["crm_property_id"];
                $user_id	 	= $_SESSION["crm_member_id"];
                $id 		 	= $this->decryptData($data['session_token']); 
                $curr           = date("Y-m-d H:i:s");
                if ($data['dob']!="") {
                	$newchangeDateFormat = $this->newchangeDateFormat($data['dob']);
                }else{
                	$newchangeDateFormat = '';
                }

                
                $query = "UPDATE ".CUSTOMER_TBL." SET
                		name  			= '".$this->cleanString($data['name'])."',
                		dob 			= '".$newchangeDateFormat."',
                		email 			= '".$this->cleanString($data['email'])."',
                		mobile 			= '".$this->cleanString($data['mobile'])."',
                		description  	= '".$this->cleanString($data['mobile'])."',
                		address 		= '".$this->cleanString($data['address'])."',
                		city 			= '".$this->cleanString($data['city'])."',
                		state 			= '".$this->cleanString($data['state'])."',
                		nationality  	= '".$this->cleanString($data['nationality'])."',
                		pincode 		= '".$this->cleanString($data['pincode'])."',
                		ad_contact_person = '".$this->cleanString($data['ad_contact_person'])."',
                		ad_relationship	  = '".$this->cleanString($data['ad_relationship'])."',
                		ad_mobile  		  = '".$this->cleanString($data['ad_mobile'])."',
                		ad_email 		  = '".$this->cleanString($data['ad_email'])."',
                		ad_address 		  = '".$this->cleanString($data['ad_address'])."',
                		ad_city 		  = '".$this->cleanString($data['ad_city'])."',
                		ad_state 		  = '".$this->cleanString($data['ad_state'])."',
                		ad_pincode 		  = '".$this->cleanString($data['ad_pincode'])."',
                        kyc_status      = '2',
                        kyc_submittedon = '$curr',
                        updated_at      = '$curr' WHERE id='$id' ";
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                if($exe){
                	/*$ticket_uid = "K".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".KYC." SET kyc_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);*/
                    $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$user_id."' ");
                    // TO Admin 
	                $sender_mail    = NO_REPLY;
	            	$subject        = "KYC document has been uploaded by ".ucwords($cusinfo['name']) ;
	            	$receiver       = ADMIN_EMAIL;
	            	$email_temp     = $this->KYCDocumnetSubmitNotification($cusinfo['name']);            
	            	$sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");


					if (isset($data['kyc_row'])) {
                        $kyc_row = $data['kyc_row'];
                        $kycmultifiles = $this->addMultiplekycImages($kyc_row,$id,$images);
                    }

					unset($_SESSION['add_kyc_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Multiple File Insert

    function addMultiplekycImages($kyc_row,$customer_id,$images)
    {
        $curr       = date("Y-m-d H:i:s");
        if(count($images)>0){
			$content_array 	= $kyc_row;
			$kyctype 		= array();
			foreach ($content_array as $key => $value) {
				$kyctype[] 			= $this->cleanString($value['kyctype_id']);		
			}
			$i = 0;
			foreach ($images as $value) {	
				$output 			 = str_replace("uploads/srcimg/", "", $value);
	           	$image_info 		 = explode(".", $output); 
				$image_type 		 = end($image_info);
				$insert_kyctype  	 = $kyctype[$i];	


				// Upload to S3
                $targetPath = "../resource/uploads/srcimg/".$output;
                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'front_srcimg/'.$output, S3::ACL_PUBLIC_READ);
                unlink($targetPath);

	           	$qu = "INSERT INTO ".KYC_LIST." SET
	           		customer_id     = '$customer_id',
	           		kyctype_id    	= '".$insert_kyctype."', 
	           		image_name 		= '$output',
	           		file_type 		= '$image_type',
	           		status 			= '1', 
	           		approval_status = '0', 
	           		created_at 		= '$curr', 
	           		updated_at	 	= '$curr'  ";
	           	$exe2 = $this->selectQuery($qu);
	           	$i++;
	        }
	    	return 1;
		}else{
			return 1;
		}
    }

    // Delete KYC 
	
	function deletKYCAttachmentImage($data='')
	{		
		$id    			= $data;
		$info = $this->getDetails(KYC_LIST,"image_name", "id='$id'  ");
		unlink("uploads/srcimg/".$info['image_name']);
		$q = $this->deleteRow(KYC_LIST,"id='$id'");
		return 1;
	}

	// Edit  Attachments

    function editKYCAttachmentImage($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $_SESSION['edit_kyc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(KYC_LIST,"*","id='$id' ");
        $file = (($info['image_name']=='') ? "<div class='form-group row ' >
                            <div class='col-sm-12 '> 
                                <label for='example-search-input' class='col-form-label'>Upload Document</label>
                            </div>
                            <div class='col-sm-12'>
                                <input type='file' required id='cimage' name='cimage' />
                                <input type='hidden' id='customerImage' name='files'>
                            </div>
                        </div>"  : 
                        "<div class='form-group row' >
                            <div class='col-sm-12'>
                            <label for='example-search-input' class=' col-form-label'>Document File</label>
                            </div>
                            <div class='col-sm-12'>
                            <input type='hidden' id='cimage' name='cimage' value='' />
                                <input type='hidden' id='customerImage' name='files' value='".$info['image_name']."'>
                                <input type='hidden' value='1' name='old_image'>
                                <div class='customer_pic_edit'>
                                    <div class='fileupload-preview fileupload-large thumbnail'><a target='blank' class='' href='".FRONTSRCIMG.$info['image_name']."''><img src='".IMGPATH."file.png' style='width: 120px;margin-bottom: 10px;margin-left: -15px;''></a></div>
                                    <button type='button' class='btn btn-danger btn-sm removeKYCImg' data-type='image' data-option='".$info['id']."'><i class='icon ni ni-trash'></i></button>
                                    <a target='blank' class='btn btn-success btn-sm' href='".FRONTSRCIMG.$info['image_name']."''><i class='icon ni ni-eye'></i></a>
                                </div>
                            </div>
                        </div>"
                         );

        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_kyc_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='container-fluid'>
                    	<div class='form-group row ' >
                            <div class='col-sm-12 '> 
                                <label for='example-search-input' class='col-form-label'>Select Doc Type</label>
                            </div>
                            <div class='col-sm-12'>
                                <select class='form-control form-select ' id='property_id' name='property_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                            ".$this->getKYCType()."
                        		</select>
                            </div>
                        </div>

                        ".$file."
                    </div>";
        $result['layout'] = $layout;
        return $result;

    }

    // Update Agenda Files

    function updateDocumentsFiles($data="")
    {
        //$result =  array();
        if(isset($_SESSION['edit_uploadfile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_uploadfile_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $output         = str_replace("uploads/document/", "", $data['files']);
                $image_info     = explode(".", $output); 
                $image_type     = end($image_info);
                $info           = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*", "id='$id'");
               /* if(isset($data['old_image'])) {
                    $test = "";
                }else{
                    unlink("uploads/srcimg/".$info['image']);
                }*/
                $query = "UPDATE ".PROPERTY_DOCUMENTS_ITEMS." SET 
                        document_file   = '".$this->cleanString($data['files'])."',
                        doc_status      = '1',
                        doc_type        = '$image_type',
                        updated_at      = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);
                unset($_SESSION['edit_uploadfile_key']);
                //$pic        = $data['image']!="" ? "<a target='blank' href='".SRC_IMG.$data['image']. "'><img style='width:70px;' class='img-responsive img-thumbnail' src='".SRC_IMG.$data['image']. "' alt=''></a>": "<img class='img-circle' src='".SRC_IMG."no_img.png' alt=''>" ;
                //$i      = $data['sno'];
                return 1; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function removeKYCImg($id)
    {
    	$curr           = date("Y-m-d H:i:s");
        $q = "UPDATE ".KYC_LIST." SET 
            image_name  	  ='',
            updated_at        ='$curr' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }


    // Select Property

	function selectProperty($data)
	{
		
		if(isset($_SESSION['crm_member_id'])){
			$_SESSION['crm_property_id'] = $data;
			return 1;
		}else{
			return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
		}

	}


}


?>