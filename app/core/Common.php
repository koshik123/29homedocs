<?php

class Constants
{

	public function controllernames()
	{

		/*-----------------------------------------------
		 	Define the Custom Controllers with Hyphen
		-------------------------------------------------*/

		$custom_routes = array();
		$custom_routes['about-us'] = "about";
		$custom_routes['contact-us'] = "contact";
		$custom_routes['terms-and-conditions'] = "terms";
		$custom_routes['privacy-policy'] = "privacy";
		return $custom_routes;

	}

	public function methodnames($value='')
	{ 

		/*-----------------------------------------------
			 Define the Custom Methods with Hyphen
		-------------------------------------------------*/

		$custom_methods = array(); 
		$custom_methods['terms-and'] = "terms";
		return $custom_methods;

	}
}


?>