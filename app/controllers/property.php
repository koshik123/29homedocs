<?php

class Property extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(isset($_SESSION["crm_property_id"])){
				$info = $user->getPropertyInfo($_SESSION["crm_property_id"]);
				$this->view('home/propertyinfo', 
					[	
						'active_menu' 	=> 'property',
						'active_submenu'=> '',
						'meta_title'  	=>  COLNAME.' | Bank Info',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'info' 			=> $info,
						'cost' 			=> $user->indianMoneyFormate($info['cost']),
						'room_list' 	=> $user->getPropertyDocumentLists(),
						'scripts'		=> 'home',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/propertyinfo', 
					[	
						'active_menu' 	=> 'property',
						'active_submenu'=> '',
						'meta_title'  	=>  COLNAME.' | Bank Info',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	

	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'active_submenu'=> '',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>