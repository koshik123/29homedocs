<?php

class Gallery extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/manageprivategallery', 
				[	
					'active_menu' 	=> 'gallery',
					'active_submenu'=> 'managegallery',
					'meta_title'  	=>  COLNAME.' | gallery',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->managePrivateGallery(),
					'privatelist' 	=> $user->managePrivateRoomGallery(),
					'scripts'		=> 'gallery',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function community()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/managecommunitygallery', 
				[	
					'active_menu' 	=> 'gallery',
					'active_submenu'=> 'communitygallery',
					'meta_title'  	=>  COLNAME.' | gallery',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->manageGallery(),
					'scripts'		=> 'gallery',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	
	
	public function details($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(GALLERY_TBL,"id"," token='$token' ");
		    if($check){	
		    	$info = $user->getDetails(GALLERY_TBL,"*"," token='$token'  ");	
				$this->view('home/gallerydetails', 
					[	
						'active_menu' 	=> 'gallery',
						'active_submenu'=> 'communitygallery',
						'meta_title'  	=>  COLNAME.' | Gallery',
						'page_title'  	=>  'Gallery',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($info['id']),
						'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'viewgallery',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function communityrequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(GALLERY_IMAGE,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(GALLERY_IMAGE,"*"," id='$token'  ");
		    	$galleryinfo = $user->getDetails(GALLERY_TBL,"*"," id='".$info['gallery_id']."'  ");	
		    	$type = 'community gallery';
				$this->view('home/adhocrequestscommunitygallery ', 
					[	
						'active_menu' 	=> 'gallery',
						'active_submenu'=> 'communitygallery',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'type_name' 	=>  $type,
						'galleryinfo' 	=> $galleryinfo,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function viewdetails()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/privategallerydetails', 
				[	
					'active_menu' 	=> 'gallery',
					'active_submenu'=> 'managegallery',
					'meta_title'  	=> COLNAME.' | Gallery',
					'page_title'  	=> 'Gallery',
					'images' 		=> $user->viewPrivateCommonGalleryImages(),
					'meta_keywords' => META_KEYWORDS,
					'meta_description'=> META_DESCRIPTION,
					'scripts'		=> 'viewgallery',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function privatecommonrequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(PROPERTY_GALLERY,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(PROPERTY_GALLERY,"*"," id='$token'  ");	
		    	//$galleryinfo = $user->getDetails(GALLERY_TBL,"*"," id='".$info['gallery_id']."'  ");
		    	$type = 'private common gallery';
				$this->view('home/adhocrequestscommunitygallery ', 
					[	
						'active_menu' 	=> 'gallery',
						'active_submenu'=> 'managegallery',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'type_name' 	=>  $type,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}


	public function viewgallery($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(PROPERTY_ROOMS_LIST,"id"," roomlist_id='$token' ");
		    if($check){	
		    	$info = $user->getDetails(PROPERTY_ROOMS_LIST,"*"," roomlist_id='$token'  ");	
				$this->view('home/viewprivategallery', 
					[	
						'active_menu' 	=> 'gallery',
						'active_submenu'=> 'managegallery',
						'meta_title'  	=> COLNAME.' | Gallery',
						'page_title'  	=> 'Gallery',
						'info'			=> $info,
						'token'			=> $user->encryptData($info['id']),
						'images' 		=> $user->viewprivateroomgallery($token),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'viewgallery',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),		
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}


	public function privaterequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(PROPERTY_ROOMS_LIST,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(PROPERTY_ROOMS_LIST,"*"," id='$token'  ");	
		    	$galleryinfo = $user->getDetails(FLATTYPE_MASTER_LIST,"*"," id='".$info['roomlist_id']."'  ");	
		    	$type = 'private gallery';
				$this->view('home/adhocrequestscommunitygallery ', 
					[	
						'active_menu' 	=> 'gallery',
						'active_submenu'=> 'managegallery',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'galleryinfo' 	=>  $galleryinfo,
						'type_name' 	=>  $type,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'gallery',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>