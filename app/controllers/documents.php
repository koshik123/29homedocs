<?php

class Documents extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$info = $user->getPropertyInfo($_SESSION["crm_property_id"]);
			$this->view('home/managegeneraldocuments', 
				[	
					'active_menu' 	=> 'documents',
					'active_submenu'=> 'gendoc',
					'meta_title'  	=>  COLNAME.' | Manage General Documents',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'info' 			=> $info,
					'list' 			=> $user->managegeneraldocuments(),
					'scripts'		=> 'adhoc',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function generalrequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(GENDRAL_DOCUMENTS,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(GENDRAL_DOCUMENTS,"*"," id='$token'  ");	
		    	$icon = $user->getFileIcon($info['document_type']);
		    	$type = 'general document';
				$this->view('home/adhocrequestsgendraldocument ', 
					[	
						'active_menu' 	=> 'documents',
						'active_submenu'=> 'gendoc',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'type_name' 	=>  $type,
						'icon' 			=>  $icon,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}


	public function privatedoc()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');		
			$this->view('home/manageprivatedocuments', 
				[	
					'active_menu' 	=> 'documents',
					'active_submenu'=> 'privdoc',
					'meta_title'  	=>  COLNAME.' | Manage Private Documents',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->managePrivatedocuments(),
					'common_list' 	=> $user->manageCommonPrivatedocuments(),
				//	'privategen_list'=> $user->managePrivatedocuments(),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function privaterequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(PROPERTY_DOCUMENTS_ITEMS,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*"," id='$token'  ");	
		    	$icon = $user->getFileIcon($info['doc_type']);
		    	$type = 'private document';
				$this->view('home/adhocrequestsgendraldocument ', 
					[	
						'active_menu' 	=> 'documents',
						'active_submenu'=> 'privdoc',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'type_name' 	=>  $type,
						'icon'			=>	$icon,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function privategeneralrequest($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(PROPERTY_GENDRAL_DOCUMENTS,"id"," id='$token' ");
		    if($check){	
		    	if(!isset($_SESSION['add_generaldoc_key'])){
					$_SESSION['add_generaldoc_key'] = $user->generateRandomString("40");
				}
		    	$info = $user->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*"," id='$token'  ");	
		    	$icon = $user->getFileIcon($info['document_type']);
		    	$type = 'private general document';
				$this->view('home/adhocrequestsgendraldocument ', 
					[	
						'active_menu' 	=> 'documents',
						'active_submenu'=> 'privdoc',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'type_name' 	=>  $type,
						'icon' 			=>  $icon,
						'types'  		 => $user->getRequestTypeList(),
						'token'			=>	$user->encryptData($info['id']),
					//	'images' 		=>  $user->viewGalleryImages($info['id']),
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),	
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}


	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'documents',
					'active_submenu'=> '',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>