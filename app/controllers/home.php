<?php

class Home extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$checkpopup 		= $user->PopupCheck();
			$today = date("Y-m-d");
			if($checkpopup==1){
				$pop_count 			= $user->check_query(NEWS_TBL,"id","show_popup='1' AND deleted_status='0' AND popup_till>='$today' ");
				$banner_info 		= $user->getDetails(NEWS_TBL,"*","show_popup='1' AND deleted_status='0' AND popup_till>='$today' ORDER BY id DESC LIMIT 1");
			}else{
				$pop_count 			= 0;
				$banner_info 		= '';
			}
			$this->view('home/index', 
				[	
					'active_menu' 	=> 'home',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'checkpopup'	=>	$checkpopup,
					'pop_count'		=>	$pop_count,
					'banner_info' 	=>  $banner_info,
					'scripts'		=> 'dashboard',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function projects()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/selectprojects', 
				[	
					'active_menu' 	=> 'home',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function forgotpassword()
	{
		$user = $this->model('Front');
		$this->view('home/forgotpassword', 
			[	
				'active_menu' 	=> 'home',
				'meta_title'  	=>  COLNAME.' | Home',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'scripts'		=> 'home',			
			]);
	}
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function logout()
	{
		if(isset($_SESSION['crm_member_id'])){
			$user = $this->model('Front');
			if($user->sessionOut($user_type="User",$_SESSION['crm_member_id'])){
				unset($_SESSION['crm_member_id']);
				unset($_SESSION['crm_property_id']);
				session_destroy();
			}	
		}
		header("Location:".BASEPATH);
	}	


}


?>