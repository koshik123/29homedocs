<?php

class adhocrequests extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/manageadhocrequests', 
				[	
					'active_menu' 	=> 'adhoc',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | manageadhocrequests',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list'			=> $user->manageadhocrequests(),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(!isset($_SESSION['add_request_key'])){
				$_SESSION['add_request_key'] = $user->generateRandomString("40");
			}
			$this->view('home/addadhocrequest', 
				[	
					'active_menu' 	=> 'adhoc',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Add Adhoc Request',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'types'  		 => $user->getRequestTypeList(),
					'scripts'		=> 'adhoc',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),			
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
	public function details($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(!isset($_SESSION['reply_ticket_key'])){
				$_SESSION['reply_ticket_key'] = $user->generateRandomString("40");
			}	
			$check = $user->check_query(ADOC_REQUEST,"id"," id='$token' ");
		    if($check){	
		    	$info = $user->getDetails(ADOC_REQUEST,"*"," id='$token'  ");	
				$this->view('home/adhocrequestsdetails', 
					[	
						'active_menu' 	=> 'adhoc',
						'active_submenu'=> '',
						'meta_title'  	=>  COLNAME.' | Adhoc requests',
						'page_title'  	=>  'Adhoc requests',
						'info'			=>	$info,
						'token'			=>	$user->encryptData($info['id']),
						'list'			=>  $user->getAdhocInfo($info['id']),	
						'replay_list'	=>  $user->getReplyAdhocInfo($info['id']),	
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'adhoc',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
						'menu' 	 		=> $user->menuInfo(),
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'property'		=>	$user->selectReportProperty(),	
						'member'   		=>  $user->userInfo($_SESSION["crm_member_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'adhoc',
					'active_submenu'=> '',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>