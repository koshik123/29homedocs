<?php

class Resetpassword extends Controller
{
	
	// Default Method

	public function index()
	{
		header('Location:'.BASEPATH."login");
		exit;
	}

	public function email($token='')
	{
		if($token !=""){
			$user = $this->model('Front');
			if(!isset($_SESSION['reset_password_key'])){
				$_SESSION['reset_password_key'] = $user->generateRandomString("40");
			}
			$token = $user->decryptData($token);
			$check 	= $user->check_query(CUSTOMER_TBL,"id"," token='$token' and resetpassword ='1' ");			
			if($check ==1){
				$info 	= $user->getDetails(CUSTOMER_TBL,"id"," token ='$token' and resetpassword ='1' ");
				$this->view('home/resetpassword',
				[
					'active_menu' 	=> 'home',
					'meta_title'	=>	'Reset Password - '.COMPANY_NAME,
					'page_title'	=>	'Reset Password - '.COMPANY_NAME,
					'info'			=>  $info,
					'token'			=>	$token
				]);				
			}else{
				$this->view('home/resetemailerror', 
					[	
						'active_menu' 	=> 'home',
						'meta_title'  	=> 'Invalid Validation code',
						'page_title'  	=> 'Invalid Validation code'
					]);
			}			
		}else{
			$this->view('home/resetemailerror', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> 'Invalid Validation code',
					'page_title'  	=> 'Invalid Validation code'
				]);
		}

	}

	// Error

	public function error()
	{
		$this->view('home/resetemailerror', 
			[	
				'meta_title'  	=> 'Invalid Validation code',
				'page_title'  	=> 'Invalid Validation code'
			]);
	}

}


?>