<?php

class kyc extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/managekyc', 
				[	
					'active_menu' 	=> 'kyc',
					'active_submenu'=> 'managekyc',
					'meta_title'  	=>  COLNAME.' | kyc',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list'			=> $user->managekyc(),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function application()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$info = $user->getDetails(CUSTOMER_TBL,"*"," id='".$_SESSION["crm_member_id"]."'  ");
			$this->view('home/kycapplication', 
				[	
					'active_menu' 	=> 'kyc',
					'active_submenu'=> 'kycapplication',
					'info' 			=> $info,
					'meta_title'  	=>  COLNAME.' | KYC Application',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),		
					'menu' 	 		=> $user->menuInfo(),	
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$check = $user->check_query(KYC_LIST,"id"," id='$token' ");
		    if($check){	
		    	$info    = $user->getDetails(KYC_LIST,"*"," id='$token'  ");
		    	$doc_type= $user->getDetails(KYC_TYPE,"*"," id='".$info['kyctype_id']."'  ");	
		    	$cusinfo = $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");	
				$this->view('home/kycdetails', 
					[	
						'active_menu' 	=> 'kyc',
						'active_submenu'=> 'kycapplication',
						'meta_title'  	=>  COLNAME.' | KYC Application',
						'page_title'  	=>  COLNAME,
						'cusinfo'		=>  $cusinfo,
						'info'			=>  $info,
						'doc_type'		=>  $doc_type,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),		
						'menu' 	 		=> $user->menuInfo(),	
						'property'		=>	$user->selectReportProperty(),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'gallery',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'member'   		=>  $user->userInfo($_SESSION["crm_member_id"]),
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(!isset($_SESSION['add_kyc_key'])){
				$_SESSION['add_kyc_key'] = $user->generateRandomString("40");
			}
			$info = $user->getDetails(CUSTOMER_TBL,"*"," id='".$_SESSION["crm_member_id"]."'  ");	
			$this->view('home/addkyc', 
				[	
					'active_menu' 	=> 'kyc',
					'active_submenu'=> '',
					'info' 			=> $info,
					'meta_title'  	=>  COLNAME.' | KYC Application',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'token'			=>	$user->encryptData($info['id']),
					'kyc_list' 		=> $user->getKYCList($info['id']),
					'kyc_type' 		=> $user->getKYCType(),
					'scripts'		=> 'addkyc',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),	
					'property'		=>	$user->selectReportProperty(),		
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'kyc',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>