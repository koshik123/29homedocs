<?php

class Error extends Controller
{
	
	public function index()
	{
		$user = $this->model('Front');
		$this->view('home/error',
			[
				'meta_title'	=>'404 Error - Page Not Found',
				'scripts'		=> 'error',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'menu' 	 		=> $user->menuInfo(),
				'property'		=>	$user->selectReportProperty(),	
			
			]);

	}
	public function error()
	{
		$user = $this->model('Front');
		$this->view('home/error',
			[
				'meta_title'	=>'404 Error - Page Not Found',
				'scripts'		=> 'error',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'menu' 	 		=> $user->menuInfo(),
				'property'		=>	$user->selectReportProperty(),	
				
			]);
	}
	


}


?>