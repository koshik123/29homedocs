<?php

class Paymentschedule extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$crm_property_id = @$_SESSION["crm_property_id"];
			$payment_schedule 		= $user->getDetails(PAYMENT_SCHEDULE,"*"," property_id='".$crm_property_id."' ");
			$this->view('home/managepaymentschedule', 
				[	
					'active_menu' 	=> 'payment',
					'active_submenu'=> 'manageinvoice',
					'meta_title'  	=>  COLNAME.' | Manage Payment Schedule',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'payment_schedule' => $payment_schedule,
					'payment_stats' => $user->getPaymentStats($payment_schedule['id']),
					'payment_list'	=> $user->getPaymentScheduleList($payment_schedule['id'],$crm_property_id),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'invoice',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>