<?php

class Settings extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/bankinfo', 
				[	
					'active_menu' 	=> 'bank',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Bank Info',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->manageBankInfo(),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function contactinfo()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/contactinfo', 
				[	
					'active_menu' 	=> 'contact',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Contact Info',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->manageContactInfo(),
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>