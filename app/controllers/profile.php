<?php

class Profile extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(!isset($_SESSION['edit_profile_key'])){
				$_SESSION['edit_profile_key'] = $user->generateRandomString("40");
			}
			$info_details = $user->getDetails(CUSTOMER_TBL,"*"," id='".$_SESSION["crm_member_id"]."' ");
			$info = $user->editPagePublish($info_details);
			$this->view('home/editprofile', 
				[	
					'active_menu' 	=> 'profile',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'info'			=>  $info,
					'token'			=>	$user->encryptData($info['id']),
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),
					'property'		=>	$user->selectReportProperty(),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function changepassword()
	{
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			if(!isset($_SESSION['change_password_key'])){
				$_SESSION['change_password_key'] = $user->generateRandomString("40");
			}
			$this->view('home/changepassword', 
				[	
					'active_menu' 	=> 'profile',
					'active_submenu'=> '',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),	
					'property'		=>	$user->selectReportProperty(),		
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_member_id"])){
			$user = $this->model('Front');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'profile',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'user' 	 		=> $user->userInfo($_SESSION["crm_member_id"]),	
					'menu' 	 		=> $user->menuInfo(),	
					'property'		=>	$user->selectReportProperty(),					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}
}


?>