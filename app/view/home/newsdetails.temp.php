<?php include 'includes/top.html'; ?>

<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head ">
            <div class="nk-block-head-sub"><a class="back-to" href="<?php echo BASEPATH ?>news"><em class="icon ni ni-arrow-left"></em><span>News</span></a></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal"><?php echo ucwords($data['info']['title']) ?></h2>
                    <div class="nk-block-des">
                        <p>Your news details are given below.</p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">



            <div class="row">
            <div class="col-md-12">
                 <div class="news_details_warp">
                    <?php if ($data['info']['image']!=""){ ?>
                        <div class="new_image_wrap">
                            <img src="<?php echo SRCIMG.$data['info']['image'] ?>" style="width: 100%;">
                            <?php if (1!=1): ?>
                            <div class="news_img" style='background-image: url(<?php echo SRCIMG.$data['info']['image'] ?>); background-repeat: no-repeat; background-position: center top; background-size: cover;'></div> 
                            <?php endif ?>
                             <h3 class="hidden-xs hidden-sm"><?php echo $data['info']['title'] ?></h3>
                        </div>
                        <h3 class="no_image_title visible-xs visible-sm"><?php echo $data['info']['title'] ?></h3>
                    <?php }else{ ?>
                        <h3 class="no_image_title"><?php echo $data['info']['title'] ?></h3>
                    <?php } ?>
                    <div class="news_content ">
                        <p class="news_date"><em class='icon ni ni-calendar'></em> On <?php echo date("l, d M Y",strtotime($data['info']['date'])) ?></p>
                        <p class="news_content_text">
                            <?php echo $data['info']['description'] ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
















            <?php if (1!=1): ?>
                <div class="row gy-5">
                <div class="col-lg-9">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h5 class="nk-block-title title">News Info</h5>
                        </div>
                    </div><!-- .nk-block-head -->
                    <div class="card card-bordered">
                        <ul class="data-list is-compact">
                            <li class="data-item news_warp">
                                <div class="data-col">
                                    <div class="data-labels">Title</div>
                                    <div class="data-value"><?php echo ucwords($data['info']['title']) ?></div>
                                </div>
                            </li>
                            <li class="data-item news_warp">
                                <div class="data-col">
                                    <div class="data-labels">Date</div>
                                    <div class="data-value"><?php echo date("d/m/Y",strtotime($data['info']['date'])) ?></div>
                                </div>
                            </li>
                            <li class="data-item news_warp">
                                <div class="data-col">
                                    <div class="data-labels" style="width: 21%;">Description</div>
                                    <div class="data-value">
                                        <?php echo ($data['info']['description']) ?> 
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div><!-- .card -->
                    
                </div><!-- .col -->
                <div class="col-lg-3">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h5 class="nk-block-title title">News Banner</h5>
                            <p>Here is user uploaded banner.</p>
                        </div>
                    </div><!-- .nk-block-head -->
                    <div class="card card-bordered">
                        <ul class="data-list is-compact">
                            <li class="data-item">
                                <div class="data-col">
                                    <div class="data-value"><a href="<?php echo SRCIMG ?><?php echo $data['info']['image'] ?>" target="_blank"><img src='<?php echo SRCIMG ?><?php echo $data['info']['image'] ?>' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style="width: 130px;"></a></div>
                                </div>
                            </li>
                        </ul>
                    </div><!-- .card -->
                </div>
               
            </div><!-- .row -->
            <?php endif ?>
            
        </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
    


<?php include 'includes/bottom.html'; ?>