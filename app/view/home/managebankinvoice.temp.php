<?php include 'includes/top.html'; ?>

<div class="nk-content-body">
                                <div class="nk-content-wrap">
                                    <div class="nk-block-head nk-block-head-lg">
                                        <div class="nk-block-head-sub"><span>Invoices History</span></div>
                                        <div class="nk-block-between-md g-4">
                                            <div class="nk-block-head-content">
                                                <h2 class="nk-block-title fw-normal">Bank Invoices</h2>
                                                <div class="nk-block-des">
                                                    <p>You can find all of your Invoices</p>
                                                </div>
                                            </div>
                                             <div class="nk-block-head-content">
                                                <ul class="nk-block-tools g-4 flex-wrap">
                                                    <li class="order-md-last"><a href="javascripts:void();" data-toggle="modal" class="btn btn-white btn-dim btn-outline-primary"><span>Pay</span></a></li>
                                                </ul>
                                            </div>
                                            
                                        </div>
                                    </div><!-- .nk-block-head -->
                                    <div class="nk-block">
                                        <div class="card card-bordered">
                                            <table class="table table-orders">
                                                <thead class="tb-odr-head">
                                                    <th>Order ID</th>
                                                    <th>Date</th>
                                                    <th>Bank</th>
                                                    <th>Type</th>
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                    <th >Action</th>
                                                    
                                                </thead>
                                                <tbody class="tb-odr-body">
                                                    <tr>
                                                        <td><a href="javascripts:void();">#746F5K1</a></td>
                                                        <td>23 Jan 2019, 10:45pm</td>
                                                        <td>SBI</td>
                                                        <td>NEFT</td>
                                                        <td>Rs. 2300.00</td>
                                                        <td><span class="badge badge-dot badge-success">Complete</span></td>
                                                        <td><div class="tb-odr-btns d-none d-sm-inline">
                                                                <a href="javascripts:void();" class="btn btn-dim btn-sm btn-primary">View</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascripts:void(); ">#746F5K2</a></td>
                                                        <td>23 Jan 2019, 10:45pm</td>
                                                        <td>Axis</td>
                                                        <td>NEFT</td>
                                                        <td>Rs. 2300.00</td>
                                                        <td><span class="badge badge-dot badge-success">Complete</span></td>
                                                        <td><div class="tb-odr-btns d-none d-sm-inline">
                                                                <a href="javascripts:void();" class="btn btn-dim btn-sm btn-primary">View</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="javascripts:void();">#746F5K3</a></td>
                                                        <td>23 Jan 2019, 10:45pm</td>
                                                        <td>Indian</td>
                                                        <td>NEFT</td>
                                                        <td>Rs. 2300.00</td>
                                                        <td><span class="badge badge-dot badge-success">Complete</span></td>
                                                        <td><div class="tb-odr-btns d-none d-sm-inline">
                                                                <a href="<?php echo BASEPATH ?>invoice/details " class="btn btn-dim btn-sm btn-primary">View</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                   
                                                   
                                                    
                                                </tbody>
                                            </table>
                                        </div> <!-- .card -->
                                    </div><!-- .nk-block -->
                                </div>
                                <!-- footer @s -->
                               

                          

<?php include 'includes/bottom.html'; ?>

