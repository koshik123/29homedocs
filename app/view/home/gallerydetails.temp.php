<?php include 'includes/top.html'; ?>

 <div class="nk-content-body ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between g-3">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title"><?php echo ucwords($data['info']['album_title']) ?> </h3>
                            
                        </div>
                        <div class="nk-block-head-content">
                            <a href="<?php echo BASEPATH ?>gallery/community" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></a>
                            <a href="<?php echo BASEPATH ?>gallery/community" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></a>
                        </div>
                    </div>
                </div><!-- .nk-block-head -->
                <div class="nk-block">
                    <div class="nk-files nk-files-view-grid">
                    <div class="nk-files-list">
                        
                        
                        <div class="gallery gallery_wrap row minheight " id="masonry">
                            <?php echo $data['images'] ?>
                        </div>


                        <?php if (1!=1): ?>
                            <div class="nk-file-item nk-file">
                            <img src="<?php echo IMGPATH ?>1.jpg">
                        </div><!-- .nk-file -->
                        <div class="nk-file-item nk-file">
                            <img src="<?php echo IMGPATH ?>2.jpg">
                        </div><!-- .nk-file -->
                        <?php endif ?>

                    </div>
                </div><!-- .nk-files -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

<div class="modal fade raiseAdhocRequest" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header align-center">
                    <div class="nk-file-title">
                        <div class="nk-file-name">
                            <div class="nk-file-name-text"><span class="title">Adhoc request</span></div>
                            
                        </div>
                    </div>
                    <a href="javascript:void();" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label class="form-label" for="address-county">Adhoc request type</label>
                        <select class="form-select" id="address-county" data-ui="lg">
                            <option>Type 1</option>
                            <option>Type 2</option>
                            <option>Type 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="address-l2">Subject </label>
                       <input type="text" name="" class="form-control">
                        
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="address-l2">Remarks</label>
                        <textarea class="form-control form-control-lg"></textarea>
                        
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer modal-footer-stretch bg-light">
                    <div class="modal-footer-between">
                        
                        <div class="g">
                            <ul class="btn-toolbar g-3">
                                <li><a href="javascript:void();" data-toggle="modal" data-dismiss="modal" class="btn btn-outline-light btn-white">Cancel</a></li>
                                <li><a href="javascript:void();" class="btn btn-primary file-dl-toast">Submit</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .modal-footer -->
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
</div><!-- .modal -->