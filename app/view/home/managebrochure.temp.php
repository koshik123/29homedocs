<?php include 'includes/top.html'; ?>


<div class="nk-content-body">
<div class="nk-content-wrap">
    <div class="nk-block-head ">
        <div class="nk-block-between-md g-4">
            <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">Property Brochure </h2>
                
            </div>
        </div>
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <ul class="sp-pdl-list">
           <?php echo $data['list'] ?>
        </ul><!-- .sp-pdl-list -->
    </div><!-- .nk-block -->
    
</div>




<?php include 'includes/bottom.html'; ?>

