<?php include 'includes/top.html'; ?>


 <div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head nk-block-head-lg">
            
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Property Info</h2>
                    <!-- <div class="nk-block-des">
                        <p>Your subscription renews on Oct 28, 2020 <span class="text-soft">(11 months 16 days remaining)</span> <span class="text-primary"><em class="icon ni ni-info"></em></span></p>
                    </div> -->
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card card-bordered">
                        <div class="card-inner-group">
                            <?php if(isset($_SESSION["crm_property_id"])){ ?>
                            <div class="card-inner">
                                <div class="sp-plan-head">
                                    <h6 class="title">Property Details</h6>
                                </div>
                                <div class="sp-plan-desc sp-plan-desc-mb">
                                    <ul class="row gx-1">
                                        <li class="col-sm-3">
                                            <p><span class="text-soft">Title</span> <?php echo ucwords($data['info']['title']) ?> </p>
                                        </li>
                                        <li class="col-sm-3">
                                            <p><span class="text-soft">Flat Variant</span> <?php echo ucwords($data['info']['flat_variant']) ?> </p>
                                        </li>
                                        
                                        <li class="col-sm-3">
                                            <p><span class="text-soft">Block</span> <?php echo ucwords($data['info']['block']) ?></p>
                                        </li>
                                         <li class="col-sm-3">
                                            <p><span class="text-soft">Floor</span> <?php echo ucwords($data['info']['floor']) ?></p>
                                        </li>
                                         <li class="col-sm-3">
                                            <p><span class="text-soft">Area</span> <?php echo ucwords($data['info']['projectarea']) ?></p>
                                        </li>
                                         <li class="col-sm-3">
                                            <p><span class="text-soft">Size</span> <?php echo ucwords($data['info']['projectsize']) ?> Sqft</p>
                                        </li>
                                    </ul>
                                </div>
                                <?php echo $data['room_list'] ?>
                            </div><!-- .card-inner -->
                            <?php }else{ ?>
                            <div class="card-inner">
                                <div class="sp-plan-head">
                                    <h6 class="title">Property is not mapped</h6>
                                </div>
                            </div>
                            <?php } ?>


                           <?php if(isset($_SESSION["crm_property_id"])){ ?>
                            <div class="card-inner">
                                <div class="sp-plan-link">
                                    <a href="<?php echo BASEPATH ?>brochure" class="link">
                                        <span><em class="icon ni ni-property-alt"></em>Property Brochures</span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </a>
                                </div>
                            </div><!-- .card-inner -->
                            <?php } ?>
                            <?php if ($data['menu']['menu']=='show') { ?>
                            <div class="card-inner">
                                <div class="sp-plan-link">
                                    <a href="<?php echo BASEPATH ?>virtualtour" class="link">
                                        <span><em class="icon ni ni-focus"></em> Project 360 Virtual Tour</span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if(isset($_SESSION["crm_property_id"])){ ?>
                            <div class="card-inner">
                                <div class="sp-plan-link">
                                    <a href="<?php echo BASEPATH ?>documents" class="link">
                                        <span><em class="icon ni ni-files"></em> General Documents</span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </a>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="card-inner">
                                <div class="sp-plan-link">
                                    <a href="<?php echo BASEPATH ?>gallery/community" class="link">
                                        <span><em class="icon ni ni-img-fill"></em> Community Gallery </span>
                                        <em class="icon ni ni-arrow-right"></em>
                                    </a>
                                </div>
                            </div>
                        </div><!-- .card-inner-group -->
                    </div><!-- .card -->
                </div><!-- .col -->
               
            </div>
        </div><!-- .nk-block -->
       
    </div>
    <!-- footer @s -->
    
    <!-- footer @e -->
</div>




<?php include 'includes/bottom.html'; ?>

