<?php include 'includes/top.html'; ?>


<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head">
            <div class="nk-block-head-sub"><a class="back-to historyback" href="javascript:void();"><em class="icon ni ni-arrow-left"></em><span>Back</span></a></div>
            <div class="nk-block-between-md g-4 align-items-end">
                <div class="nk-block-head-content">
                    <?php if ($data['type_name']=="community gallery"){ ?>                            
                            <h2 class="nk-block-title fw-normal"><?php echo ucwords($data['galleryinfo']['album_title']) ?></h2>
                        <?php }elseif($data['type_name']=="private common gallery"){ ?>                            
                            <h2 class="nk-block-title fw-normal">Common Gallery Images</h2> 
                        <?php }elseif($data['type_name']=="private gallery"){ ?>                            
                            <h2 class="nk-block-title fw-normal"><?php echo ucwords($data['galleryinfo']['room_type']) ?></h2> 
                        <?php } ?>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-4 flex-wrap">
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->
       
        <div class="nk-block nk-block-lg">
            <form id="generalDocRequests" class="post_form_wrap" name="generalDocRequests" method="POST" action="#" >
                <input type="hidden" value="<?php echo $_SESSION['add_generaldoc_key'] ?>" name="fkey" id="fkey">
                <input type="hidden" value="<?php echo $data['info']['id'] ?>" name="ref_id" id="ref_id">
                <input type="hidden" value="<?php echo $data['type_name'] ?>" name="type" id="type">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-bordered">
                            <div class="card-inner">
                                <div class="ticket-msgs">
                                    <div class="post_an_item_wrap">
                                        
                                            <div class="form-group">
                                                <label class="form-label" for="fva-topics">Request Type <en>*</en></label>
                                                <div class="form-control-wrap ">
                                                    <select class="form-control form-select select2-hidden-accessible" id="request_id" name="request_id" data-placeholder="Select a Request Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                        <?php echo $data['types'] ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="email">Subject <en>*</en></label>
                                                <input type="text" class="form-control" id="subject" name="subject" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="email">Message <en>*</en></label>
                                                <textarea class="form-control form-control-lg" id="remarks" name="remarks" required></textarea>
                                            </div>
                                            <?php if (1!=1): ?>
                                                <div class="form-group">
                                                    <table class="table  " id="adhoccontent">
                                                        <tbody>
                                                            <?php $adhoc_row  = 0; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="post_box_footer">
                                                    <div class="selector">
                                                        <div class="attachment">
                                                            <ul>
                                                                <li>
                                                                    <button type="button" class="tooltips img_button" data-toggle="tooltip" data-placement="top"  onclick="addProperty();" data-original-title="Add Attachment" data-option="1" id="btn1" ><i class="icon ni ni-clip-h"></i> <span>Add Attachment</span></button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <ul class="form-btn-group">
                                                        <li class="form-btn-secondary"><a href="<?php echo BASEPATH ?>adhocrequests" class="btn btn-dim btn-outline-light">cancel</a></li>
                                                        <li class="form-btn-primary"><button type="submit" class="btn btn-info">Submit</button></li>
                                                    </ul>
                                                    <span class="clearfix"></span>
                                                </div>
                                            <?php endif ?>
                                    </div>
                                </div><!-- .ticket-msgs -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb75">
                        <div class="nk-files-group">
                            <div class="nk-files-list title">
                                <?php if ($data['type_name']=="community gallery"){ ?>
                                    <h5><?php echo ucwords($data['galleryinfo']['album_title']) ?> image Preview:    </h5>
                                    <div class="nk-block">
                                        <div class="nk-files nk-files-view-grid">
                                            <div class="nk-files-list">
                                                <div class="communitygallery gallery gallery_wrap row minheight " id="masonry">
                                                    <div class='col-md-7 col-masonry'> 
                                                        <a class='zooming' href='<?php echo SRCIMG.$data['info']['image'] ?>' title=''>
                                                            <img src='<?php echo SRCIMG.$data['info']['image'] ?>' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' >
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }elseif($data['type_name']=="private common gallery"){ ?>
                                   <h5>Image Preview:    </h5>
                                    <div class="nk-block">
                                        <div class="nk-files nk-files-view-grid">
                                            <div class="nk-files-list">
                                                <div class="communitygallery gallery gallery_wrap row minheight " id="masonry">
                                                    <div class='col-md-7 col-masonry'> 
                                                        <a class='zooming' href='<?php echo SRCIMG.$data['info']['images'] ?>' title=''>
                                                            <img src='<?php echo SRCIMG.$data['info']['images'] ?>' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail'>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }elseif($data['type_name']=="private gallery"){ ?>
                                    <h5><?php echo ucwords($data['galleryinfo']['room_type']) ?> image Preview:    </h5>
                                    <div class="nk-block">
                                        <div class="nk-files nk-files-view-grid">
                                            <div class="nk-files-list">
                                                <div class="communitygallery gallery gallery_wrap row minheight " id="masonry">
                                                    <div class='col-md-7 col-masonry'> 
                                                        <a class='zooming' href='<?php echo SRCIMG.$data['info']['image'] ?>' title=''>
                                                            <img src='<?php echo SRCIMG.$data['info']['image'] ?>' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail'>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               <?php } ?>



                            </div>
                        </div>  
                        <div class="nk-block nk-block-lg">
                            <div class="card card-bordered card-preview">
                                <table class="table table-ulogs" id="adhoccontent">
                                    <thead class="thead-light">
                                        <tr>
                                            <th class="tb-col-os"><span class="overline-title">Description </span></th>
                                            <th class="tb-col-time"><span class="overline-title">File</span></th>
                                            <th class="tb-col-action"><span class="overline-title">&nbsp;</span></th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php $adhoc_row  = 0; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="post_box_footer mt15">
                            <div class="selector">
                                <div class="attachment float_right">
                                    <ul>
                                        <li>
                                            <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top"  onclick="addProperty();" data-original-title="Add Attachment"><em class="icon ni ni-plus"></em> <span>Add Attachment</span></button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
                <div class="form_submit_footer">
                    <div class="form_footer_contents">
                        <div class="text-right m-b-0">
                            <a href='<?php echo BASEPATH ?>adhocrequests' class="btn  btn-danger"><em class="icon ni ni-cross"></em> Cancel</a>
                            <button type="submit" class="btn  btn-primary"><em class="icon ni ni-check"></em> Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div><!-- .nk-block -->
    </div>
                                <!-- footer @s -->
                               

<?php include 'includes/bottom.html'; ?>


