<?php include 'includes/top.html'; ?>



<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head">
            <div class="nk-block-head-content">
                <div class="nk-block-head-sub"><span>Account Setting</span></div>
                <h2 class="nk-block-title fw-normal">My Profile</h2>
                <div class="nk-block-des">
                    <p>You have full control to manage your own account setting. <span class="text-primary"><em class="icon ni ni-info"></em></span></p>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <ul class="nk-nav nav nav-tabs">
            <li class="nav-item active current-page">
                <a class="nav-link" href="<?php echo BASEPATH ?>profile">Edit Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo BASEPATH ?>profile/changepassword">Change Password</a>
            </li>
            
        </ul><!-- nav-tabs -->
        <div class="nk-block">
            <div class="nk-block-head">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title">Personal Information</h5>
                    <div class="nk-block-des">
                        <p>Basic info, like your name and address, that you use on Nio Platform.</p>
                    </div>
                </div>
                
            </div><!-- .nk-block-head -->
            <div class="card card-bordered">
                <div class="nk-data data-list">
                    <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                        <div class="data-col">
                            <span class="data-label">Full Name</span>
                            <span class="data-value"><?php echo ucwords($data['info']['name']) ?></span>
                        </div>
                        <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                    </div><!-- .data-item -->
                    <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                        <div class="data-col">
                            <span class="data-label">Email</span>
                            <span class="data-value"><?php echo ($data['info']['email']) ?></span>
                        </div>
                        <div class="data-col data-col-end"><span class="data-more "><em class="icon ni ni-forward-ios"></em></span></div>
                    </div><!-- .data-item -->
                    <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                        <div class="data-col">
                            <span class="data-label">Phone Number</span>
                            <span class="data-value text-soft"><?php echo ucwords($data['info']['mobile']) ?></span>
                        </div>
                        <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                    </div><!-- .data-item -->
                    <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                        <div class="data-col">
                            <span class="data-label">Gender</span>
                            <span class="data-value"><?php echo (($data['info']['gender']=="") ? "Not add yet" : ucwords($data['info']['gender']) ); ?></span>
                        </div>
                        <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                    </div><!-- .data-item -->
                    <div class="data-item" data-toggle="modal" data-target="#profile-edit">
                        <div class="data-col">
                            <span class="data-label">Address</span>
                            <span class="data-value"><?php echo ($data['info']['address'].'<br>'.$data['info']['city']) ?><br><?php echo ($data['info']['pincode']) ?></span>
                        </div>
                        <div class="data-col data-col-end"><span class="data-more"><em class="icon ni ni-forward-ios"></em></span></div>
                    </div><!-- .data-item -->
                </div><!-- .nk-data -->
            </div><!-- .card -->
            <!-- Another Section -->
           
        </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
    

<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="form-error"></div>
            <a href="javascript:void();" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form id="editProfile" method="POST" accept-charset="utf-8" action="#">
                <input type="hidden" value="<?php echo $_SESSION['edit_profile_key'] ?>" name="fkey" id="fkey">
                <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                <div class="modal-body modal-body-lg">
                    <h5 class="title">Update Profile</h5>
                    <ul class="nk-nav nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal">Personal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#address">Address</a>
                        </li>
                    </ul><!-- .nav-tabs -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="full-name">Full Name <en>*</en></label>
                                    <input type="text" class="form-control form-control-lg" id="name" name="name" value="<?php echo ucwords($data['info']['name']) ?>" placeholder="Enter Full name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="display-email">Email <en>*</en></label>
                                    <input type="text" class="form-control form-control-lg"  name="email" value="<?php echo ucwords($data['info']['email']) ?>" placeholder="Enter email" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="phone-no">Mobile Number <en>*</en></label>
                                    <input type="text" class="form-control form-control-lg" id="phone-no" name="mobile" value="<?php echo ($data['info']['mobile']) ?>" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="birth-day">Gender</label>
                                    <input type="text" class="form-control form-control-lg " id="gender" name="gender" value="<?php echo ucwords($data['info']['gender']) ?>" placeholder="Enter your name">
                                </div>
                            </div>
                           
                            <div class="col-12">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <button type="submit" class="btn btn-lg btn-primary">Update Profile</button>
                                    </li>
                                    <li>
                                        <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                    </li>
                                </ul>
                            </div>
                            </div>
                        </div><!-- .tab-pane -->
                        <div class="tab-pane" id="address">
                            <div class="row gy-4">
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="address-l1">Address<en></en></label>
                                    <input type="text" class="form-control form-control-lg" id="address" name="address" value="<?php echo $data['info']['address'] ?>">
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="address-st">City <em></em></label>
                                    <input type="text" class="form-control form-control-lg" id="address-st" name="city" value="<?php echo $data['info']['city'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="state">State</label>
                                    <input type="text" class="form-control form-control-lg" id="state" name="state" value="<?php echo $data['info']['state'] ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="address-county">Pincode</label>
                                    <input type="text" class="form-control form-control-lg" id="address-county" name="pincode" value="<?php echo $data['info']['pincode'] ?>">
                                </div>
                            </div>
                            <div class="col-12">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                         <button type="submit" class="btn btn-lg btn-primary">Update Address</button>
                                    </li>
                                    <li>
                                        <a href="javascript:void();" data-dismiss="modal" class="link link-light">Cancel</a>
                                    </li>
                                </ul>
                            </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div><!-- .tab-content -->
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- .modal -->


<?php include 'includes/bottom.html'; ?>
<?php if (isset($_GET['p'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Personal Information updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>