<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?php echo IMGPATH ?>favicon.png">
    <!-- Page Title  -->
    <title><?php echo $data['page_title'] ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?php echo CSSPATH ?>dashlite.css?ver=1.5.0">
    <link id="skin-default" rel="stylesheet" href="<?php echo CSSPATH ?>theme.css?ver=1.5.0">
    <link rel="stylesheet" href="<?php echo CSSPATH ?>custom.css">
     <link href="<?php echo PLUGINS ?>sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
     <link rel="stylesheet" type="text/css" href="<?php echo PLUGINS ?>noty/noty.css">
    <script type="text/javascript">
        var base_path = "<?php echo BASEPATH ?>";
        var core_path = "<?php echo COREPATH ?>";
    </script>

    <?php if (isset($data['scripts'])) { ?>
        <?php if ($data['scripts']=="adhoc") { ?>
            <link href="<?php echo PLUGINS ?>multifile/jquery.filer.css" type="text/css" rel="stylesheet" />
        <?php } ?> 

         <?php if ($data['scripts']=="viewgallery" || $data['scripts']=="gallery" || $data['scripts']=="adhoc") { ?>
            <link href="<?php echo PLUGINS ?>lightbox/simplelightbox.min.css" rel="stylesheet">
        <?php } ?>

    <?php } ?>  
</head>
<body class="nk-body npc-subscription has-aside ui-clean ">

    