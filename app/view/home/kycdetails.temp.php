<?php include 'includes/top.html'; ?>

 <div class="nk-content-body ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between g-3">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">KYCs / <strong class="text-primary small"><?php echo ucwords($data['cusinfo']['name']) ?></strong></h3>
                            <div class="nk-block-des text-soft">
                                <ul class="list-inline">
                                   <!--  <li>Application ID: <span class="text-base">KID000844</span></li> -->
                                    <li>Submited At: <span class="text-base"> <?php echo date("d M, Y h:i a",strtotime($data['info']['created_at'])) ?></span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="nk-block-head-content">
                            <a href="<?php echo BASEPATH ?>kyc" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></a>
                            <a href="<?php echo BASEPATH ?>kyc" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none"><em class="icon ni ni-arrow-left"></em></a>
                        </div>
                    </div>
                </div><!-- .nk-block-head -->
                <div class="nk-block">
                    <div class="row gy-5">
                        <div class="col-lg-5">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Application Info</h5>
                                    <p>Submission date, approve date, status etc.</p>
                                </div>
                            </div><!-- .nk-block-head -->
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Submitted By</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['uid']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Submitted At</div>
                                            <div class="data-value"><?php echo date("d M, Y h:i a",strtotime($data['info']['created_at'])) ?></div>
                                        </div>
                                    </li>
                                    <?php 
                                    $approval_status1 = (($data['info']['approval_status']=='1') ? '<span class="badge badge-dim badge-sm badge-outline-success">Approved</span>' : '<span class="badge badge-dim badge-sm badge-outline-warning">Pending</span>' );

                                    $approval_status = (($data['info']['approval_status']=='2') ? '<span class="badge badge-dim badge-sm badge-outline-danger">Reject</span>' : $approval_status1 );

                                     ?>
                                    
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Status</div>
                                            <div class="data-value"><?php echo $approval_status ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Last Checked</div>
                                            <div class="data-value">
                                                <!-- <div class="user-card">
                                                    <div class="user-avatar user-avatar-xs bg-orange-dim">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-name">
                                                        <span class="tb-lead">Saiful Islam</span>
                                                    </div>
                                                </div> -->
                                                -
                                            </div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Last Checked At</div>
                                            <div class="data-value"><!-- 19 Dec, 2019 05:26 AM --> -</div>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- .card -->
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Uploaded Documents</h5>
                                    <p>Here is user uploaded documents.</p>
                                </div>
                            </div><!-- .nk-block-head -->
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Document Type</div>
                                            <div class="data-value"><a href="<?php echo UPLOADS ?>srcimg/<?php echo $data['info']['image_name'] ?>" target="_blank"><?php echo ucwords($data['doc_type']['kyc_type']) ?></a></div>
                                        </div>
                                    </li>
                                   <!--  <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Front Side</div>
                                            <div class="data-value">National ID Card</div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Back Side</div>
                                            <div class="data-value">National ID Card</div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Proof/Selfie</div>
                                            <div class="data-value">National ID Card</div>
                                        </div>
                                    </li> -->
                                </ul>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-lg-7">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Applicant Information</h5>
                                    <p>Basic info, like name, phone, address, country etc.</p>
                                </div>
                            </div>
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Name</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                        </div>
                                    </li>
                                    
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Email Address</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Phone Number</div>
                                            <div class="data-value text-soft"><em><?php echo ucwords($data['cusinfo']['mobile']) ?></em></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Gender</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['gender']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Date of Birth</div>
                                            <div class="data-value"><?php echo $data['cusinfo']['dob']=="" ? '' : date("d M, Y",strtotime($data['cusinfo']['dob'])) ?></div>
                                        </div>
                                    </li>

                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Full Address</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Country of Residence</div>
                                            <div class="data-value"><?php echo ucwords($data['cusinfo']['nationality']) ?></div>
                                        </div>
                                    </li>
                                   
                                    
                                </ul>
                            </div>
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>