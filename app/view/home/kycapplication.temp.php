<?php include 'includes/top.html'; ?>

<div class="nk-content-body nk-content-lg nk-content-fluid">
                <div class="container-xl wide-lg">
                    <div class="nk-content-inner">
                        <div class="nk-content-body">
                            <div class="kyc-app wide-lg ">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">KYC Verification</h2>
                                        <!-- <div class="nk-block-des">
                                            <p>To comply with regulation each participant will have to go through indentity verification (KYC/AML) to prevent fraud causes. </p>
                                        </div> -->
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner card-inner-lg">
                                            <div class="nk-kyc-app p-sm-2 text-center">
                                                
                                                <?php if ($data['info']['kyc_status']=='3'){ ?>
                                                    <div class="nk-kyc-app-icon">
                                                        <em class="icon reject ni ni-cross"></em>
                                                        <p class="reject">Rejected</p>
                                                    </div>
                                                    <div class="nk-kyc-app-text mx-auto">
                                                        <p class="lead" style="color: #fd0606;"><?php echo $data['info']['kyc_remarks'] ?></p>
                                                    </div>
                                                    <div class="nk-kyc-app-text mx-auto">
                                                        <p class="lead">Oops ! Your KYC is rejected due to the below mentioned reasons. Click on the below button link to update the KYC details.</p>
                                                    </div>

                                                    <div class="nk-kyc-app-action">
                                                        <a href="<?php echo BASEPATH ?>kyc/add" class="btn btn-lg btn-primary">Click here to update your KYC</a>
                                                    </div>
                                                <?php }elseif($data['info']['kyc_status']=='1'){ ?>
                                                    <div class="nk-kyc-app-icon">
                                                        <em class="icon approved ni ni-check"> </em>
                                                        <p class="approved">Approved</p>
                                                    </div>
                                                    <div class="nk-kyc-app-text mx-auto">
                                                        <p class="lead">Your KYC is verified and approved. If you have any changes you can update the KYC details using below link.</p>
                                                    </div>
                                                    <div class="nk-kyc-app-action">
                                                        <a href="<?php echo BASEPATH ?>kyc/add" class="btn btn-lg btn-primary">Click here to update your KYC</a>
                                                    </div>
                                                <?php }elseif($data['info']['kyc_status']=='2'){ ?>
                                                    <div class="nk-kyc-app-icon">
                                                        <em class="icon processing ni ni-repeat"></em>
                                                        <p class="processing">Pending Approval</p>
                                                    </div>
                                                    <div class="nk-kyc-app-text mx-auto">
                                                        <p class="lead">Thank you for submitting the KYC details. We are processing your request.  If you have any changes you can update the KYC details using below link.</p>
                                                    </div>
                                                    <div class="nk-kyc-app-action">
                                                        <a href="<?php echo BASEPATH ?>kyc/add" class="btn btn-lg btn-primary">Click here to update your KYC</a>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class="nk-kyc-app-icon">
                                                        <em class="icon notsubmitted ni ni-info"></em>
                                                        <p class="notsubmitted">KYC not submitted</p>
                                                    </div>
                                                    <div class="nk-kyc-app-text mx-auto">
                                                        <p class="lead">You have not submitted your necessary documents click on the below link to update your KYC.</p>
                                                    </div>
                                                    <div class="nk-kyc-app-action">
                                                        <a href="<?php echo BASEPATH ?>kyc/add" class="btn btn-lg btn-primary">Click here to complete your KYC</a>
                                                    </div>
                                                <?php } ?>
                                                
                                                
                                            </div>
                                        </div>
                                    </div><!-- .card -->
                                    <div class="text-center pt-4">
                                        <p>If you have any question, please contact our support team - <a href="<?php echo BASEPATH  ?>adhocrequests/add">Raise a Adhoc Request</a></p>
                                    </div>
                                </div> <!-- .nk-block -->
                            </div><!-- .kyc-app -->
                        </div>
                    </div>
                </div>
            </div>


<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>KYC submitted successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>