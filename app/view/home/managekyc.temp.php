<?php include 'includes/top.html'; ?>


 <div class="nk-content-body ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between g-3">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">My KYC Documents</h3>
                                            
                                        </div>
                                        
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="card card-bordered card-stretch">
                                        <div class="card-inner-group">
                                            <div class="card-inner p-0">
                                                <div class="nk-tb-list nk-tb-ulist">
                                                    <div class="nk-tb-item nk-tb-head">
                                                        <div class="nk-tb-col tb-col-mb"><span>ID</span></div>
                                                        <div class="nk-tb-col tb-col-mb"><span>Doc Type</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Documents</span></div>
                                                        <div class="nk-tb-col tb-col-lg"><span>Submitted</span></div>
                                                        <div class="nk-tb-col tb-col-md"><span>Status</span></div>
                                                        <div class="nk-tb-col tb-col-lg"><span>Checked By</span></div>
                                                        <div class="nk-tb-col nk-tb-col-tools">&nbsp;</div>
                                                    </div><!-- .nk-tb-item -->
                                                    <?php echo $data['list'] ?>
                                                </div>
                                            </div><!-- .card-inner -->
                                           
                                        </div><!-- .card-inner-group -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>



<?php include 'includes/bottom.html'; ?>



<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>KYC submitted successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>