<?php include 'includes/top.html'; ?>

<div class="nk-content-body">
                                <div class="nk-content-wrap">
                                    <div class="nk-block-head nk-block-head-lg">
                                        <div class="nk-block-head-sub"><span>FAQs</span></div>
                                        <div class="nk-block-between-md g-4">
                                            <div class="nk-block-head-content">
                                                <h2 class="nk-block-title fw-normal">Frequently Ask Questions</h2>
                                                <div class="nk-block-des">
                                                    <p>Here are some question and you can find your answer.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .nk-block-head -->
                                    <div class="nk-block">
                                        <h5 class="title text-primary">General Question</h5>
                                        <p>You can find general answer here.</p>
                                        <div id="faq-gq" class="accordion">
                                            <div class="accordion-item">
                                                <a href="javascript:void();" class="accordion-head" data-toggle="collapse" data-target="#faq-gq-1">
                                                    <h6 class="title">What is Dashlite?</h6>
                                                    <span class="accordion-icon"></span>
                                                </a>
                                                <div class="accordion-body collapse show" id="faq-gq-1" data-parent="#faq-gq">
                                                    <div class="accordion-inner">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                    </div>
                                                </div>
                                            </div><!-- .accordion-item -->
                                            <div class="accordion-item">
                                                <a href="javascript:void();" class="accordion-head collapsed" data-toggle="collapse" data-target="#faq-gq-2">
                                                    <h6 class="title">What are some of the benefits of receiving my bill electronically?</h6>
                                                    <span class="accordion-icon"></span>
                                                </a>
                                                <div class="accordion-body collapse" id="faq-gq-2" data-parent="#faq-gq">
                                                    <div class="accordion-inner">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                    </div>
                                                </div>
                                            </div><!-- .accordion-item -->
                                            <div class="accordion-item">
                                                <a href="javascript:void();" class="accordion-head collapsed" data-toggle="collapse" data-target="#faq-gq-3">
                                                    <h6 class="title">What is the relationship between Dashlite and payment?</h6>
                                                    <span class="accordion-icon"></span>
                                                </a>
                                                <div class="accordion-body collapse" id="faq-gq-3" data-parent="#faq-gq">
                                                    <div class="accordion-inner">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                    </div>
                                                </div>
                                            </div><!-- .accordion-item -->
                                            <div class="accordion-item">
                                                <a href="javascript:void();" class="accordion-head collapsed" data-toggle="collapse" data-target="#faq-gq-4">
                                                    <h6 class="title">What are the benefits of using Dashlite?</h6>
                                                    <span class="accordion-icon"></span>
                                                </a>
                                                <div class="accordion-body collapse" id="faq-gq-4" data-parent="#faq-gq">
                                                    <div class="accordion-inner">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                    </div>
                                                </div>
                                            </div><!-- .accordion-item -->
                                        </div><!-- .accordion -->
                                    </div><!-- .nk-block -->
                                  
                                  
                                </div>
                                <!-- footer @s -->
                                <div class="nk-footer">
                                    <div class="container wide-xl">
                                        <div class="nk-footer-wrap g-2">
                                            <div class="nk-footer-copyright"> &copy; 2020 DashLite. Template by <a href="javascript:void();">Softnio</a>
                                            </div>
                                            <div class="nk-footer-links">
                                                <ul class="nav nav-sm">
                                                    <li class="nav-item"><a class="nav-link" href="javascript:void();">Terms</a></li>
                                                    <li class="nav-item"><a class="nav-link" href="javascript:void();">Privacy</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- footer @e -->
                            </div>

                          

<?php include 'includes/bottom.html'; ?>

