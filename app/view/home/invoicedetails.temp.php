<?php include 'includes/top.html'; ?>

<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head ">
            <div class="nk-block-head-sub"><a class="back-to" href="<?php echo BASEPATH ?>invoice"><em class="icon ni ni-arrow-left"></em><span>Invoices</span></a></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Invoice #<?php echo $data['info']['inv_number'] ?></h2>
                    <div class="nk-block-des">
                        <p>Your invoice details are given below.</p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
                    <div class="row gy-5">
                        <div class="col-lg-5">
                            
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Ref Number</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['inv_number']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Invoice Date</div>
                                            <div class="data-value"><?php echo date("d M, Y h:i a",strtotime($data['info']['inv_date'])) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Invoice Type</div>
                                            <div class="data-value">
                                                <?php echo ucwords($data['info']['type']) ?>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Raised to</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['raised_to']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">File</div>
                                            <div class="data-value">
                                                 <a target="_blank" href="<?php echo DOCUMENTS ?><?php echo $data['info']['documents'] ?>" class="btn btn-sm btn-success"> Download Invoice</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- .card -->
                            
                        </div><!-- .col -->
                        
                       
                    </div><!-- .row -->
                </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
    


<?php include 'includes/bottom.html'; ?>