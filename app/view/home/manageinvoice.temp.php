<?php include 'includes/top.html'; ?>




<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head ">
            
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Invoices/ Receipts</h2>
                    <div class="nk-block-des">
                        <p>You can find all of your Invoices/ Receipts</p>
                    </div>
                </div>
                
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
            <div class="card card-bordered">
                <table class="table table-orders">
                    <thead class="tb-odr-head">
                        <tr class="tb-odr-item">
                            <th class="tb-odr-info">
                                <span class="tb-odr-id" style='min-width: 20px;'>#</span>
                                <span class="tb-odr-id" style='min-width: 170px;'>Ref Number</span>
                                <span class="tb-odr-date d-none d-md-inline-block">Date</span>
                            </th>
                            <th class="tb-odr-amount">
                                <span class="tb-odr-total"> Type</span>
                                <span class="tb-odr-status d-none d-md-inline-block">Raised to</span>
                            </th>
                            <th class="tb-odr-action">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody class="tb-odr-body">
                        <?php echo $data['list'] ?>
                    </tbody>
                </table>
            </div> <!-- .card -->
        </div><!-- .nk-block -->
    </div>
<!-- footer @s -->
                                

                          

<?php include 'includes/bottom.html'; ?>

