<?php include 'includes/top.html'; ?>


<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>My Adhoc requests</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Adhoc requests</h2>
                    <div class="nk-block-des">
                        <p>Here are all of your support request that you already sent.</p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-4 flex-wrap">
                        <li class="order-md-last"><a href="<?php echo BASEPATH ?>adhocrequests/add" class="btn btn-white btn-dim btn-outline-primary"><span>Submit Ticket</span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
            <div class="card card-bordered">
                <table class="table table-tickets">
                    <thead class="tb-ticket-head">
                        <tr class="tb-ticket-title">
                            <th class="tb-ticket-id"><span>Ticket</span></th>
                            <th class="tb-ticket-desc">
                                <span>Subject</span>
                            </th>
                            <th class="tb-ticket-date tb-col-md">
                                <span>Request Type</span>
                            </th>
                            <th class="tb-ticket-seen tb-col-md">
                                <span>Submited</span>
                            </th>
                            <th class="tb-ticket-status">
                                <span>Status</span>
                            </th>
                            <th class="tb-ticket-action"> &nbsp; </th>
                        </tr><!-- .tb-ticket-title -->
                    </thead>
                    <tbody class="tb-ticket-body">
                       <?php echo $data['list'] ?>
                        
                    </tbody>
                </table>
            </div>
        </div><!-- .nk-block -->
       
    </div>
                          

<?php include 'includes/bottom.html'; ?>

 <div class="modal fade" tabindex="-1" role="dialog" id="file-details">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header align-center">
                    <div class="nk-file-title">
                        <div class="nk-file-name">
                            <div class="nk-file-name-text"><span class="title">Adhoc request</span></div>
                            
                        </div>
                    </div>
                    <a href="javascript:void();" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label class="form-label" for="address-county">Adhoc request </label>
                        <select class="form-select" id="address-county" data-ui="lg">
                            <option>Gallery</option>
                            <option>Documents</option>
                            <option>KYC</option>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label class="form-label" for="address-l2">Remarks</label>
                        <textarea class="form-control form-control-lg"></textarea>
                        
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer modal-footer-stretch bg-light">
                    <div class="modal-footer-between">
                        
                        <div class="g">
                            <ul class="btn-toolbar g-3">
                                <li><a href="javascript:void();" data-toggle="modal" data-dismiss="modal" class="btn btn-outline-light btn-white">Cancel</a></li>
                                <li><a href="javascript:void();" class="btn btn-primary file-dl-toast">Submit</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .modal-footer -->
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->


<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Adhoc requests raised successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>