<?php include 'includes/top.html'; ?>



<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head">
            <div class="nk-block-head-content">
                <div class="nk-block-head-sub"><span>Account Setting</span></div>
                <h2 class="nk-block-title fw-normal">My Profile</h2>
                <div class="nk-block-des">
                    <p>You have full control to manage your own account setting. <span class="text-primary"><em class="icon ni ni-info"></em></span></p>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <ul class="nk-nav nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo BASEPATH ?>profile">Edit Profile</a>
            </li>
            <li class="nav-item active current-page">
                <a class="nav-link" href="<?php echo BASEPATH ?>profile/changepassword">Change Password</a>
            </li>
            
        </ul><!-- nav-tabs -->
        <div class="nk-block">
            <div class="nk-block-head">
                <div class="nk-block-head-content">
                    <h5 class="nk-block-title">Changepassword</h5>
                    
                </div>
            </div><!-- .nk-block-head -->
            <div class="form-error"></div>
            <form method="POST" id="changePassword">
                <input type="hidden" value="<?php echo $_SESSION['change_password_key'] ?>" name="fkey" id="fkey">
                <div class="card ">
                    <div class="form-group">
                        <label class="form-label" for="password">Enter Current Password</label>
                        <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Enter Current Password">
                    </div>
               
                    <div class="form-group">
                        <label class="form-label" for="new_password">Enter New Password</label>
                        <input type="password" class="form-control form-control-lg" id="new_password" name="new_password" placeholder="Enter New Password">
                    </div>
               
                    <div class="form-group">
                        <label class="form-label" for="re_password">Enter Confirm Password</label>
                        <input type="password" class="form-control form-control-lg" id="re_password" name="re_password" placeholder="Enter Confirm Password">
                    </div>
                
                    <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                        <li>
                             <button type="submit" class="btn btn-lg btn-primary">Update Password</button>
                        </li>
                        <li>
                            <a href="javascript:void();" data-dismiss="modal" class="link link-light">Cancel</a>
                        </li>
                    </ul>
                </div><!-- .card -->
            </form>
            <!-- Another Section -->
           
        </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
    



<?php include 'includes/bottom.html'; ?>


<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Password updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>