<?php include 'includes/top.html'; ?>


<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head">
            <div class="nk-block-head-sub"><a class="back-to" href="<?php echo BASEPATH ?>adhocrequests"><em class="icon ni ni-arrow-left"></em><span>My Adhoc requests</span></a></div>
            <div class="nk-block-between-md g-4 align-items-end">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal"><?php echo ucwords($data['info']['subject']) ?></h2>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools g-4 flex-wrap">
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
            <div class="ticket-info">
                <ul class="ticket-meta">
                    <li class="ticket-id"><span>Ticket ID:</span> <strong><?php echo $data['info']['ticket_uid'] ?></strong></li>
                    <li class="ticket-date"><span>Submitted:</span> <strong>
                        <?php echo date("M d, Y h:i A",strtotime($data['info']['created_at'])) ?></strong></li>
                </ul>
                <div class="ticket-status">
                   <?php echo $request_status = (($data['info']['request_status']=='1') ? "<span class='badge badge-danger'>Close</span>" : "<span class='badge badge-success'>Open</span>"  ); ?>
                </div>
            </div>
        </div><!-- .nk-block -->
        <div class="nk-block nk-block-lg">
            <div class="card card-bordered">
                <div class="card-inner">
                    <div class="ticket-msgs">
                       <!--  <?php  echo $data['list'] ?> -->
                        <?php  echo $data['replay_list'] ?>
                        <div class="ticket-msg-reply">
                            <h5 class="title">Reply</h5>
                            <form id="replayAdhocRequest" name="replayAdhocRequest" class="" method="POST" action="#" enctype="multipart/form-data">
                                <input type="hidden" value="<?php echo $_SESSION['reply_ticket_key'] ?>" name="fkey" id="fkey">
                                <input type="hidden" value="<?php echo $data['info']['id'] ?>" name="ref_id" id="ref_id">
                                
                                <div class="form-group">
                                    <div class="form-editor-custom">
                                        <textarea class="form-control"  name="remarks" id="remarks"  placeholder="Write a message..."></textarea>
                                            <table class="table  " id="adhoccontent">
                                                <tbody>
                                                    <?php $adhoc_row  = 0; ?>
                                                </tbody>
                                            </table>
                                        <!-- <div class="form-group image_attach_wrap no_display" id="image_attach_wrap">
                                            <div class="image_multiple_wrap">
                                                <div class="multi_box" id="box1">
                                                    <input class="filename" type="file" data-option='1' name="files[]"  id="1" style="display:none" />
                                                    <div class="loader_warp" id="loader_1">
                                                        <img src="<?php echo IMGPATH ?>post_loader.gif" alt="">
                                                    </div>
                                                </div>        
                                                <?php  for ($i=2; $i <=10 ; $i++) { ?>
                                                   <?php $no_display = (($i==1) ? "" : "no_display") ?>
                                                   <div class="multi_box <?php echo $no_display ?> " id="box<?php echo $i ?>">
                                                        <input class="filename" type="file" data-option='<?php echo $i ?>' name="files[]"  id="<?php echo $i ?>" style="display:none" />
                                                        <button type="button" class="img_button" data-option="<?php echo $i ?>" id="btn<?php echo $i ?>"  onclick="$('#<?php echo $i ?>').trigger('click'); return false;"><i class="icon ni ni-plus"></i></button>
                                                        <div class="loader_warp no_display" id="loader_<?php echo $i ?>">
                                                            <img src="<?php echo IMGPATH ?>post_loader.gif" alt="">
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <span class="clearfix"></span>
                                            </div>
                                        </div>
                                         -->
                                        <div class="form-editor-action">
                                            <ul class="form-editor-btn-group">
                                                <div class="post_box_footer">
                                                    <div class="selector">
                                                        <div class="attachment">
                                                            <ul>
                                                                <li>
                                                                     <button type="button" class="tooltips img_button" data-toggle="tooltip" data-placement="top"  onclick="addProperty();" data-original-title="Add File" data-option="1" id="btn1"  style="background: #ffff;border: #fff;font-size: 17px;"><i class="icon ni ni-clip-h"></i></button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </ul>
                                        </div>

                                    </div>
                                </div>

                               


                                <div class="form-action">
                                    <ul class="form-btn-group">
                                        <li class="form-btn-primary"><button type="submit" class="btn btn-primary">Send</a></li>
                                        <?php if ($data['info']['request_status']=='0'){ ?>
                                            <li class="form-btn-secondary "><a href="javascript:void();" class="btn btn-dim btn-outline-light closeTicket " data-option='<?php echo $data['token'] ?>'>Mark as close</a></li>
                                        <?php }else{ ?>

                                            <li class="form-btn-secondary"><a href="javascript:void();" class="btn btn-dim btn-outline-light"> Closed</a></li>
                                            <li class="form-btn-primary "><a href="javascript:void();" class="btn btn-info   closeTicket " data-option='<?php echo $data['token'] ?>'>Undo</a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </form>
                        </div><!-- .ticket-msg-reply -->
                    </div><!-- .ticket-msgs -->
                </div>
            </div>
        </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
                               

<?php include 'includes/bottom.html'; ?>


