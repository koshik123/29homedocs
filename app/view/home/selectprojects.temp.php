<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>Post CRM</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?php echo CSSPATH ?>dashlite.css?ver=1.5.0">
    <link id="skin-default" rel="stylesheet" href="<?php echo CSSPATH ?>theme.css?ver=1.5.0">
</head>

<body class="nk-body bg-white npc-general ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="wide-md text-center m-auto h-100">
                        <div class="logo-link mb-5 mt-4">
                            <img class="logo-dark logo-img logo-img-lg" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="">
                        </div>
                        <div class="row g-gs">
                            <div class="col-lg-6">
                                <a href="<?php echo BASEPATH ?>" class="dashboard-preview card card-bordered text-soft">
                                    <div class="card-inner">
                                        <h6 class="title">Project 1</h6>
                                        <p>Coimbatore</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-6">
                                <a href="<?php echo BASEPATH ?>" class="dashboard-preview card card-bordered text-soft">
                                    <div class="card-inner">
                                        <h6 class="title">Project 2</h6>
                                        <p>Pollachi</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-6">
                                <a href="<?php echo BASEPATH ?>" class="dashboard-preview card card-bordered text-soft">
                                    <div class="card-inner">
                                        <h6 class="title">Project 3</h6>
                                        <p>Coimbatore</p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-6">
                                <a href="<?php echo BASEPATH ?>" class="dashboard-preview card card-bordered text-soft">
                                    <div class="card-inner">
                                        <h6 class="title">Project 4</h6>
                                        <p>Salem</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="<?php echo JSPATH ?>bundle.js?ver=1.5.0"></script>
    <script src="<?php echo JSPATH ?>scripts.js?ver=1.5.0"></script>

</html>