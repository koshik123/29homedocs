<?php include 'includes/top.html'; ?>

<div class="nk-content-body nk-content-lg nk-content-fluid">
                <div class="container-xl wide-lg">
                    <div class="nk-content-inner">
                        <div class="nk-content-body">
                            <div class="kyc-app wide-lg ">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">Project 360 Virtual Tour</h2>
                                        <!-- <div class="nk-block-des">
                                            <p>To comply with regulation each participant will have to go through indentity verification (KYC/AML) to prevent fraud causes. </p>
                                        </div> -->
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    
                                        <iframe width="100%" height="500px" src="http://360view.gowthamhousing.com/?is=0-main-entrance"></iframe>
                                           
                                  
                                    
                                </div> <!-- .nk-block -->
                            </div><!-- .kyc-app -->
                        </div>
                    </div>
                </div>
            </div>


<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>KYC submitted successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>