<?php include 'includes/top.html'; ?>

<div class="nk-content-body">
    <div class="nk-content-wrap">
        <div class="nk-block-head ">
            <div class="nk-block-head-sub"><a class="back-to" href="<?php echo BASEPATH ?>paymentschedule"><em class="icon ni ni-arrow-left"></em><span>Payment Schedule</span></a></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                   
                    <div class="nk-block-des">
                        <p>Your Payment Schedule details are given below.</p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->
        <div class="nk-block">
            <div class="row gy-5">
                <div class="col-lg-12">
                    
                    <div class="card card-bordered">
                        <ul class="data-list is-compact">
                            
                            <li class="data-item">
                                <div class="data-col">
                                    <div class="data-label">Next Due Date</div>
                                    <div class="data-value"><?php echo  date("d M Y ",strtotime($data['payment_schedule']['due_date'])) ?></div>
                                </div>
                            </li>
                            <li class="data-item">
                                <div class="data-col">
                                    <div class="data-label">Amount Payable</div>
                                    <div class="data-value">Rs.<?php echo number_format($data['payment_schedule']['amt_payable'],2) ?></div>
                                </div>
                            </li>
                            <li class="data-item">
                                <div class="data-col">
                                    <div class="data-label">Amount Receive</div>
                                    <div class="data-value">
                                        Rs.<?php echo number_format($data['payment_schedule']['amt_receive'],2) ?>
                                    </div>
                                </div>
                            </li>
                            <li class="data-item">
                                <div class="data-col">
                                    <div class="data-label">Balance Amount</div>
                                    <div class="data-value">Rs.<?php echo number_format($data['payment_schedule']['balance'],2) ?></div>
                                </div>
                            </li>
                            
                        </ul>
                    </div><!-- .card -->
                    
                </div><!-- .col -->
                
               
            </div><!-- .row -->

            <?php if ($data['payment_list']['count']>0): ?>
                <div class="card card-bordered">
                    <table class="table table-orders">
                        <thead >
                            <tr >
                                <th >
                                    Stages
                                </th>
                                <th >
                                    %
                                </th>
                                <th >
                                    Amount Payable
                                </th>
                                <th >
                                    Amount Received
                                </th>
                                <th >
                                    Balance
                                </th>
                                <th >Due Date</th>
                            </tr>
                        </thead>
                        <tbody class="tb-odr-body">
                            <?php echo $data['payment_list']['layout'] ?>
                            <tr>
                                 <td colspan="1" class="text_center"><strong>Total</strong></td>
                                 <td><?php echo $data['payment_stats']['percentage_count'] ?></td>
                                 <td>Rs. <?php echo number_format($data['payment_schedule']['amt_payable'],2) ?></td>
                                 <td>Rs. <?php echo number_format($data['payment_stats']['amt_receive_count'],2) ?></td>
                                 <td>Rs. <?php echo number_format($data['payment_schedule']['amt_payable'] - $data['payment_stats']['amt_receive_count'],2) ?></td>
                                 <td></td>
                             </tr>
                        </tbody>
                    </table>
                </div>
            <?php endif ?>

                

        </div><!-- .nk-block -->
    </div>
    <!-- footer @s -->
    


<?php include 'includes/bottom.html'; ?>