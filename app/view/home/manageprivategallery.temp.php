
<?php include 'includes/top.html'; ?>


<div class="nk-content-body">
<div class="nk-content-wrap">
    <div class="nk-block-head ">
       
        <div class="nk-block-between-md g-4">
            <div class="nk-block-head-content">
                <h2 class="nk-block-title fw-normal">Private Gallery </h2>
                <!-- <div class="nk-block-des">
                    <p>You can download all the application that available in your plan.</p>
                </div> -->
            </div>
        </div>
    </div><!-- .nk-block-head -->
    <div class="nk-fmg-listing nk-block">
        <!-- <div class="nk-files nk-files-view-grid">
            <div class="nk-files-list">
            </div>
        </div> -->
        <div class="gallery_wrap row minheight" id="masonry">
            
            
            
            <?php if ($data['list']['count']>0): ?>
                <?php echo $data['list']['layout'] ?>
            <?php endif ?>
            <?php echo $data['privatelist']['layout'] ?>
        </div>
    </div><!-- .nk-block -->

</div>




<?php include 'includes/bottom.html'; ?>


<div class="modal fade raiseAdhocRequest" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header align-center">
                    <div class="nk-file-title">
                        <div class="nk-file-name">
                            <div class="nk-file-name-text"><span class="title">Adhoc request</span></div>
                            
                        </div>
                    </div>
                    <a href="javascript:void();" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label class="form-label" for="address-county">Adhoc request type</label>
                        <select class="form-select" id="address-county" data-ui="lg">
                            <option>Type 1</option>
                            <option>Type 2</option>
                            <option>Type 3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="address-l2">Subject </label>
                       <input type="text" name="" class="form-control">
                        
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="address-l2">Remarks</label>
                        <textarea class="form-control form-control-lg"></textarea>
                        
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer modal-footer-stretch bg-light">
                    <div class="modal-footer-between">
                        
                        <div class="g">
                            <ul class="btn-toolbar g-3">
                                <li><a href="javascript:void();" data-toggle="modal" data-dismiss="modal" class="btn btn-outline-light btn-white">Cancel</a></li>
                                <li><a href="javascript:void();" class="btn btn-primary file-dl-toast">Submit</a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- .modal-footer -->
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
</div><!-- .modal -->