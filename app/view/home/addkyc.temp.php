<?php include 'includes/top.html'; ?>

<div class="nk-content-body nk-content-lg nk-content-fluid">
                <div class="container-xl wide-lg">
                    <div class="nk-content-inner">
                        <div class="nk-content-body">
                            <div class="kyc-app wide-sm m-auto">
                                <div class="nk-block-head nk-block-head-lg wide-xs mx-auto">
                                    <div class="nk-block-head-content text-center">
                                        <h2 class="nk-block-title fw-normal">KYC Application</h2>
                                       <!--  <div class="nk-block-des">
                                            <p>To comply with regulation each participant will have to go through indentity verification (KYC/AML) to prevent fraud causes. </p>
                                        </div> -->
                                    </div>
                                </div><!-- nk-block -->

                                

                                <form id="addKyc" class="post_form_wrap" name="addKyc" method="POST" action="#">
                                <input type="hidden" value="<?php echo $_SESSION['add_kyc_key'] ?>" name="fkey" id="fkey">
                                <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                                    <div class="nk-block">
                                        <div class="card card-bordered">
                                            <div class="nk-kycfm">
                                                <div class="nk-kycfm-head">
                                                    <div class="nk-kycfm-count">01</div>
                                                    <div class="nk-kycfm-title">
                                                        <h5 class="title">Personal Details</h5>
                                                        <p class="sub-title">Your simple personal information required for identification</p>
                                                    </div>
                                                </div><!-- nk-kycfm-head -->
                                                <div class="nk-kycfm-content">
                                                    <div class="nk-kycfm-note">
                                                        <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></em>
                                                        <p>Please type carefully and fill out the form with your personal details. Your can’t edit these details once you submitted the form.</p>
                                                    </div>
                                                    <div class="row g-4">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Name <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="name" value="<?php echo $data['info']['name'] ?>" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Date of Birth <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg date-picker-alt" name="dob" value="<?php echo $data['info']['dob']=="" ? '' : date("d/m/Y",strtotime($data['info']['dob'])) ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Email Address <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="email" class="form-control form-control-lg" value="<?php echo $data['info']['email'] ?>" name="email" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Phone Number <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" value="<?php echo $data['info']['mobile'] ?>" name="mobile" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Description <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <textarea class="form-control form-control-lg" name="description"><?php echo $data['info']['description'] ?></textarea> 
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                    </div><!-- .row -->
                                                </div><!-- nk-kycfm-content -->
                                                <div class="nk-kycfm-head">
                                                    <div class="nk-kycfm-count">02</div>
                                                    <div class="nk-kycfm-title">
                                                        <h5 class="title">Your Address</h5>
                                                        <p class="sub-title">Your simple personal information required for identification</p>
                                                    </div>
                                                </div><!-- nk-kycfm-head -->
                                                <div class="nk-kycfm-content">
                                                    <div class="nk-kycfm-note">
                                                        <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></em>
                                                        <p>Your can’t edit these details once you submitted the form.</p>
                                                    </div>
                                                    <div class="row g-4">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Address  <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="address" value="<?php echo $data['info']['address'] ?>" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                       
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">City <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="city" value="<?php echo $data['info']['city'] ?>" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">State <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="state" value="<?php echo $data['info']['state'] ?>" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Nationality <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="nationality" value="<?php echo $data['info']['nationality'] ?>" required>
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Zip Code <span class="text-danger">*</span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="pincode" value="<?php echo $data['info']['pincode'] ?>" required> 
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                    </div><!-- .row -->
                                                </div><!-- nk-kycfm-content -->


                                                <div class="nk-kycfm-head">
                                                    <div class="nk-kycfm-count">03</div>
                                                    <div class="nk-kycfm-title">
                                                        <h5 class="title">Alternate Contact Details</h5>
                                                        <p class="sub-title">Your simple personal information required for identification</p>
                                                    </div>
                                                </div><!-- nk-kycfm-head -->
                                                <div class="nk-kycfm-content">
                                                    <div class="nk-kycfm-note">
                                                        <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></em>
                                                        <p>Your can’t edit these details once you submitted the form.</p>
                                                    </div>
                                                    <div class="row g-4">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Contact person name <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_contact_person" value="<?php echo $data['info']['ad_contact_person'] ?>" >
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Relationship</label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_relationship" value="<?php echo $data['info']['ad_relationship'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->

                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Mobile</label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_mobile" value="<?php echo $data['info']['ad_mobile'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Email</label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_email" value="<?php echo $data['info']['ad_email'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                         <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Address</label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_address" value="<?php echo $data['info']['ad_address'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">City <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_city" value="<?php echo $data['info']['ad_city'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">State <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_state" value="<?php echo $data['info']['ad_state'] ?>">
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="form-label-group">
                                                                    <label class="form-label">Zip Code <span class="text-danger"></span></label>
                                                                </div>
                                                                <div class="form-control-group">
                                                                    <input type="text" class="form-control form-control-lg" name="ad_pincode" value="<?php echo $data['info']['ad_pincode'] ?>"> 
                                                                </div>
                                                            </div>
                                                        </div><!-- .col -->
                                                    </div><!-- .row -->
                                                </div><!-- nk-kycfm-content -->


                                                <div class="nk-kycfm-head">
                                                    <div class="nk-kycfm-count">04</div>
                                                    <div class="nk-kycfm-title">
                                                        <h5 class="title">Document Upload</h5>
                                                        <p class="sub-title">To verify your identity, please upload any of your document.</p>
                                                    </div>
                                                </div><!-- nk-kycfm-head -->
                                                <div class="nk-kycfm-content">
                                                    <div class="nk-kycfm-note">
                                                        <em class="icon ni ni-info-fill" data-toggle="tooltip" data-placement="right" title="Tooltip on right"></em>
                                                        <p>In order to complete, please upload any of the following personal document.</p>
                                                    </div>
                                                   
                                                    <h6 class="title">To avoid delays when verifying account, Please make sure bellow:</h6>
                                                    <ul class="list list-sm list-checked">
                                                        <li>Chosen credential must not be expaired.</li>
                                                        <li>Document should be good condition and clearly visible.</li>
                                                        <li>Make sure that there is no light glare on the card.</li>
                                                    </ul>
                                                    
                                                    
                                                    <div class="nk-kycfm-upload">
                                                        <h6 class="title nk-kycfm-upload-title">Add Documents</h6>
                                                        <div class="row align-items-center">
                                                            <div class="col-sm-12">
                                                                <div class="nk-kycfm-upload-box">
                                                                     <table class="table  " id="kyccontent">
                                                                        <thead>
                                                                            <th>Doc Type</th>
                                                                            <th>File</th>
                                                                            <th>Action</th>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php if ($data['kyc_list']['count']>0): ?>
                                                                               <?php echo $data['kyc_list']['layout'] ?> 
                                                                            <?php endif ?>
                                                                            <!-- <tr id="kyc-row0">
                                                                                 <td class="text-left"><select class="form-control form-select"  name="kyc_row[0][kyctype_id]" data-placeholder="Select a ID Type" required="" aria-hidden="true"><option value="" >Select ID Type</option><?php echo $data['kyc_type'] ?></select></td>
                                                                                <td class="text-left"><input type="file" name="files[]" value="" placeholder="Select Image" class="form-control"  required/></td>
                                                                                <td class="text-center"></td>
                                                                            </tr> -->

                                                                            <?php $kyc_row  = 0; ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="button_warp">
                                                                    <button type="button" class="btn btn-primary" onclick="addKycInfo();">Add documents</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div><!-- nk-kycfm-upload -->
                                                </div><!-- nk-kycfm-content -->
                                                <div class="nk-kycfm-footer">
                                                    <!-- <div class="form-group">
                                                        <div class="custom-control custom-control-xs custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="tc-agree">
                                                            <label class="custom-control-label" for="tc-agree">I Have Read The <a href="javascript:void();">Terms Of Condition</a> And <a href="javascript:void();">Privacy Policy</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-control-xs custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="info-assure">
                                                            <label class="custom-control-label" for="info-assure">All The Personal Information I Have Entered Is Correct.</label>
                                                        </div>
                                                    </div> -->
                                                    <div class="nk-kycfm-action pt-2">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit Application</button>
                                                    </div>
                                                </div><!-- nk-kycfm-footer -->
                                            </div><!-- nk-kycfm -->
                                        </div><!-- .card -->
                                    </div><!-- nk-block -->
                                </form>
                            </div><!-- .kyc-app -->
                        </div>
                    </div>
                </div>
            </div>


<div class="modal edituploadfile fade in" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateKYCFiles" name="updateKYCFiles" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Update Info</h5>
                
                   <!--  <input type="hidden" value="<?php echo $_SESSION['add_floor_key'] ?>" name="fkey" id="fkey"> -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="edituploadfileModel"></div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>


<?php include 'includes/bottom.html'; ?>