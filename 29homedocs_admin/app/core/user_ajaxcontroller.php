<?php	
//ini_set('max_execution_time', 500);	
require_once './../../global/global-config.php';
require_once '../config/config.php';
require_once '../app/models/Model.php';
require_once 'classes/PHPMailerAutoload.php';

// Require AWS
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

class  Ajaxcontroller extends Model
{		

/*--------------------------------------------- 
				Login Authentication
----------------------------------------------*/ 

// User login

function userLogin($data){
	$email 		= $data['email'];
	$passw 		= $data['password'];
	$password 	= $this->encryptPassword($passw);
	$check = $this -> check_query(EMPLOYEE,"id"," email ='$email' AND password ='$password' AND status ='1' ");
	if($check == 1){        	
    	$userinfo = $this->getDetails(EMPLOYEE,"id,tenant_id,email,super_admin,is_manager,department_id,role_id,account_type"," email ='$email' AND password ='$password' ");
        $permissioninfo = $this -> getDetails(PERMISSION,"*"," employee_id='".$userinfo['id']."' ");

        $projectCountInfo   =  $this->check_query(PROJECT_MASTER,"id"," status='1'  ");
        //$projectInfo        =  $this->getDetails(EMPLOYEE,"id"," deleted_status='0' AND status='1' AND cancel_status='0' AND customer_id='".$userinfo['id']."' ");

        if ($projectCountInfo==1) {
        //  $_SESSION["crm_project_id"]    = $userinfo["id"];
            $_SESSION["crm_project_count"] = $projectCountInfo;  
        }elseif($projectCountInfo>1){
            $_SESSION["crm_project_count"] = $projectCountInfo; 
        }else{
            $_SESSION["crm_project_count"] = $projectCountInfo; 
        }

        $_SESSION["tenant_id"]      = $userinfo["tenant_id"];
        $_SESSION["account_type"]   = $userinfo["account_type"];

        $_SESSION["crm_admin_id"]   = $userinfo["id"];  
        $_SESSION["super_admin"]    = $userinfo["super_admin"];

        /*if ($userinfo["is_manager"]==1) {
          $_SESSION["is_manager"]     = $userinfo["is_manager"];
        }*/
        
        $_SESSION["is_manager"]         = $userinfo["is_manager"];
        $_SESSION["department_id"]      = $userinfo["department_id"];
        //Lead
        $_SESSION["lead"]               =  $permissioninfo["lead"];
        $_SESSION["manage_lead"]        =  $permissioninfo["manage_lead"];
        $_SESSION["import_lead"]        =  $permissioninfo["import_lead"];
        //Customer
        $_SESSION["customers"]          =  $permissioninfo["customers"];
        $_SESSION["manage_customer"]    =  $permissioninfo["manage_customer"];
        $_SESSION["kyc_documents"]      =  $permissioninfo["kyc_documents"];
        $_SESSION["invoice"]            =  $permissioninfo["invoice"];
        $_SESSION["import_customer"]    =  $permissioninfo["import_customer"];
        //Property
        $_SESSION["property"]           =  $permissioninfo["property"];
        $_SESSION["manage_property"]    =  $permissioninfo["manage_property"];
        $_SESSION["property_chart"]     =  $permissioninfo["property_chart"];
        $_SESSION["gallery"]            =  $permissioninfo["gallery"];
        //Adhoc
        $_SESSION["adhoc"]              =  $permissioninfo["adhoc"];
        $_SESSION["open_request"]       =  $permissioninfo["open_request"];
        $_SESSION["close_request"]      =  $permissioninfo["close_request"];
        //News
        $_SESSION["news"]               =  $permissioninfo["news"];
        $_SESSION["manage_news"]        =  $permissioninfo["manage_news"];
        //Settings
        $_SESSION["settings"]           =  $permissioninfo["settings"];
        $_SESSION["employee"]           =  $permissioninfo["employee"];
        $_SESSION["manage_department"]  =  $permissioninfo["manage_department"];
        $_SESSION["virtual_tour"]       =  $permissioninfo["virtual_tour"];
        $_SESSION["notification"]       =  $permissioninfo["notification"];
        //Reports
        $_SESSION["reports"]            =  $permissioninfo["reports"];
        $_SESSION["adhoc_report"]       =  $permissioninfo["adhoc_report"];
        $_SESSION["lead_report"]        =  $permissioninfo["lead_report"];
        $_SESSION["customer_report"]    =  $permissioninfo["customer_report"];
        $_SESSION["kyc_report"]         =  $permissioninfo["kyc_report"];
        //Master Settings
        $_SESSION["master_settings"]    =  $permissioninfo["master_settings"];
        $_SESSION["lead_type"]          =  $permissioninfo["lead_type"];
        $_SESSION["activity_type"]      =  $permissioninfo["activity_type"];
        $_SESSION["lead_source"]        =  $permissioninfo["lead_source"];
        $_SESSION["profile_master"]     =  $permissioninfo["profile_master"];
        $_SESSION["general_documents"]  =  $permissioninfo["general_documents"];
        $_SESSION["document_checklist"] =  $permissioninfo["document_checklist"];
        $_SESSION["block"]              =  $permissioninfo["block"];
        $_SESSION["floor"]              =  $permissioninfo["floor"]; 
        $_SESSION["flat_type"]          =  $permissioninfo["flat_type"];
        $_SESSION["brochure"]           =  $permissioninfo["brochure"];
        $_SESSION["contact_info"]       =  $permissioninfo["contact_info"];
        $_SESSION["payment_info"]       =  $permissioninfo["payment_info"];
        $_SESSION["area_master"]        =  $permissioninfo["area_master"];
        $_SESSION["department"]         =  $permissioninfo["department"];
        $_SESSION["role_master"]        =  $permissioninfo["role_master"];
        $_SESSION["kycdoc_type"]        =  $permissioninfo["kycdoc_type"];
        $_SESSION["company"]            =  $permissioninfo["company"]; 
        $_SESSION["trash"]              =  $permissioninfo["trash"]; 
        $_SESSION["upload_video"]       =  $permissioninfo["upload_video"]; 

        //Others
        $_SESSION["adhoc_request"]      =  $permissioninfo["adhoc_request"];
        $_SESSION["all_request"]        =  $permissioninfo["all_request"];
        $_SESSION["documents"]          =  $permissioninfo["documents"]; 
        $_SESSION["request_type"]       =  $permissioninfo["request_type"]; 
        $_SESSION["activity_report"]    =  $permissioninfo["activity_report"];


     	if($this->sessionIn($user_type="admin",$finance_type="Post CRM",$_SESSION["crm_admin_id"],"web","browser")){
     		return 1;
     	}   
    }else{
    	return $this->errorMsg("Sorry your account details are wrong.");
    }
}

    // Change ADMIN_TBL Password

    function changePassword($data)
    {
        if(isset($_SESSION['change_password_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['change_password_key']){
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check_password = $this->check_query(EMPLOYEE,"id", " id= '".$_SESSION['crm_admin_id']."' AND
                      password= '".$this->encryptPassword($data['password'])."' ");
                if($check_password==1){
                    $query= "UPDATE ".EMPLOYEE." SET
                            password = '". $this->encryptPassword($data['new_password'])."',
                            updated_at='$curr' WHERE id='".$_SESSION['crm_admin_id']."' ";
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['change_password_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";  
                    }
                }else{
                    return $this->errorMsg("Your Current Password is wrong.");
                }
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Profile 

    function editProfile($data)
    {
        if(isset($_SESSION['edit_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_profile_key']){         
                    //$user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $token          = $this->generateRandomString("40");
                    $id             = $this->decryptData($data['session_token']);
                    $project_id     = @$_SESSION['crm_project_id'];
                   // $dob            = (($data['dob']!="") ? $this->changeDateFormat($data['dob']) : '' ); 
                    $query = "UPDATE ".EMPLOYEE." SET   
                        token       = '".$this->hyphenize($data['name'])."',                 
                        name        = '".$this->cleanString($data['name'])."',
                        email       = '".$this->cleanString($data['email'])."',
                        mobile      = '".$this->cleanString($data['mobile'])."',
                        gender      = '".$this->cleanString($data['gender'])."',
                        address     = '".$this->cleanString($data['address'])."',
                        city        = '".$this->cleanString($data['city'])."',
                        state       = '".$this->cleanString($data['state'])."',
                        pincode     = '".$this->cleanString($data['pincode'])."',
                        updated_at  = '$curr' WHERE id='$id' ";
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['edit_profile_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    function virtualtoursettings($data)
    {
        if(isset($_SESSION['add_virtual_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_virtual_key']){         
                    //$user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $project_id     = @$_SESSION['crm_project_id'];
                    $query = "UPDATE ".MENU_SETTINGS." SET   
                        menu        = '".$this->cleanString($data['virtual_settings'])."', 
                        tour_code   = '".$this->cleanString($data['tour_code'])."',                 
                        status      = '1',
                        created_at  = '$curr', 
                        updated_at  = '$curr' WHERE id='1' ";
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_virtual_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Forgot password

    function forgot_password($data){
        $email      = $data['email'];
        $check = $this -> check_query(EMPLOYEE,"id"," email='$email' ");
        if($check == 1){
            $curr           = date("Y-m-d H:i:s"); 
            $userinfo       = $this ->getDetails(EMPLOYEE,"*"," email='$email' ");
            $q              = "UPDATE ".EMPLOYEE." SET resetpassword='1' WHERE id='".$userinfo['id']."' ";
            $exe = $this->selectQuery($q);
            if($exe){
                $sender_mail    = NO_REPLY;
                $subject        = COMPANY_NAME." - Reset your password";
                $receiver       = $userinfo['email'];
                //$user_token     = $userinfo['em_token'];
                $user_token     = $this->encryptData($userinfo['token']);
                $email_temp     = $this->forgotPasswordTemp($userinfo['name'],$user_token);            
                $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                //return $email_temp ;
                if ($sendemail) {
                    return $this->successMsg("Password Reset Link have been Sent to your email Address - ".$userinfo['email']." ");                 
                }           
            }else{
                return $this->errorMsg("Sorry Unexpected error Occurred Please Try again.");    
            }   
        }else{
            return $this->errorMsg("Sorry Entered Email Address doesn't exists. Please enter your registered email address.");          
        }
    }

    // Reset the password 

    function resetPassword($data)
    {
        $data           = explode("`",$data);
        //$token            = $this->decryptData($this->cleanString($data[0]));
        $token          = ($this->cleanString($data[0]));
        $password       = $this->cleanString($data[1]);
        $hash_password  = $this->encryptPassword($password);
        $curr           = date("Y-m-d H:i:s");
        $check          = $this->check_query(EMPLOYEE,"id"," token='$token' and resetpassword='1' ");
        if($check==1){
            $q = "UPDATE ".EMPLOYEE." SET
                password            = '$hash_password',
                updated_at          = '$curr',
                resetpassword       = '0' WHERE token='$token' ";
            $exe = $this->selectQuery($q);
            if($exe){
                unset($_SESSION['reset_password_key']);
                return 1;
            }else{
                return $this->errorMsg("Sorry!! Unexpected Error Occurred. Please try again.");
            }
        }else{
            return $this->errorMsg("Invalid user details. Please click on the Link send your email address to reset password.");
        }
    }

    /*----------------------------
         Members Customer
    ------------------------------*/

    // Add Customer 

    function addCustomer($data="")
    {
        if(isset($_SESSION['add_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_customer_key']){  
                $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  
                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        $password       = $this->generateRandomStringGenerator("8");
                        $psw            = $this->encryptPassword($password);
                        $project_id     = @$_SESSION['crm_project_id'];
                        $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                            token       = '".$this->hyphenize($data['name'])."',
                            tenant_id   = '".$_SESSION["tenant_id"]."',
                            priority    = '".$data['priority']."',
                            project_id  = '".$project_id."',
                            name        = '".$this->cleanString($data['name'])."',
                            gender      = '".$this->cleanString($data['gender'])."',
                            mobile      = '".$this->cleanString($data['mobile'])."',
                            email       = '".$this->cleanString($data['email'])."',                            
                            password    = '".$psw."',
                            has_psw     = '".$password."',
                            address     = '".$this->cleanString($data['address'])."',
                            city        = '".$this->cleanString($data['city'])."',
                            state       = '".$this->cleanString($data['state'])."',
                            pincode     = '".$this->cleanString($data['pincode'])."',
                            kyc_status  = '0',
                            assign_status = '0',
                            kyc_remarks = '',
                            status      = '1',
                            added_by    = '$admin_id',  
                            created_at  = '$curr',
                            updated_at  = '$curr' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);
                            

                            $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                unset($_SESSION['add_customer_key']);
                                return 1;
                            }
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Customer 

    function editCustomer($data="")
    {
        if(isset($_SESSION['edit_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_customer_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$data['mobile']."' AND id!='".$id."'  "); 
                    if($m_check == 0){  
                        $query = " UPDATE ".CUSTOMER_TBL." SET 
                            token       = '".$this->hyphenize($data['name'])."',
                            priority    = '".$data['priority']."',
                            name        = '".$this->cleanString($data['name'])."',
                            gender      = '".$this->cleanString($data['gender'])."',
                            mobile      = '".$this->cleanString($data['mobile'])."',
                            email       = '".$this->cleanString($data['email'])."',   
                            address     = '".$this->cleanString($data['address'])."',
                            city        = '".$this->cleanString($data['city'])."',
                            state       = '".$this->cleanString($data['state'])."',
                            pincode     = '".$this->cleanString($data['pincode'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_customer_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function CustomerStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(CUSTOMER_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Assign Property

    function assignproperty($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $_SESSION['assign_customer_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(CUSTOMER_TBL,"*","id='$id' ");
        $layout = " 
                <input type='hidden' value='".$_SESSION['assign_customer_key']."' name='fkey' id='materal_group_fkey'>
                <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                <div class='form-group'>
                    <label class='form-label' for='fva-topics'>Select Flat/Villa</label>
                    <div class='form-control-wrap '>
                        <select class='form-control form-select ' id='property_id' name='property_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                            ".$this->getPropertyList()."
                        </select>
                    </div>
                </div> ";
        $result['layout'] = $layout;
        return $result;

    }

    function getPropertyList($current="")
    {
        $layout ="";
        $project_id = @$_SESSION['crm_project_id'];
        $tenant_id  = @$_SESSION["tenant_id"];
        $query = "SELECT id,token,title,status  FROM ".PROPERTY_TBL." WHERE status='1' AND project_id='".$project_id."' AND tenant_id='".$tenant_id."' ";
        $exe = $this->selectQuery($query);  
        if(mysqli_num_rows($exe) > 0){
            while ($list = mysqli_fetch_array($exe)) {
                $selected = (($list['id']==$current) ? 'selected' : '');
                $layout.= "<option value='".$list['id']."' $selected>".ucwords($list['title'])."</option>";
            }
        }
        return $layout;
    }

    // Update Files

    function updateAssignProperty($data="")
    {
        //$result =  array();
        if(isset($_SESSION['assign_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['assign_customer_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".CUSTOMER_TBL." SET 
                        assign_status  = '1',
                        updated_at     = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);
                if($exe){
                    $q = "UPDATE ".PROPERTY_TBL." SET
                       customer_id      = '$id',
                       assign_status    = '1',
                       updated_at       = '$curr' WHERE id='".$data['property_id']."' ";
                    $result = $this->selectQuery($q);
                    unset($_SESSION['assign_customer_key']);
                    return 1; 
                }else{
                    return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                }
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Assign Property

    function assignPropertyToCustomer($data="")
    {
        //$result =  array();
        if(isset($_SESSION['assign_property_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['assign_property_key']){
                $id             = $this->decryptData($data['session_token']);  
                $curr           = date("Y-m-d H:i:s");
                $allotment_date = $this->changeDateFormat($data['allotment_date']);
                $query = "UPDATE ".CUSTOMER_TBL." SET                        
                        assign_status  = '1',
                        updated_at     = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);
                 if($exe){
                    $q = "UPDATE ".PROPERTY_TBL." SET
                       customer_id      = '$id',
                       allotment_date = '".$allotment_date."',
                       assign_remarks = '".$this->cleanString($data['assign_remarks'])."',
                       assign_status    = '1',
                       updated_at       = '$curr' WHERE id='".$data['property_id']."' ";
                    $result = $this->selectQuery($q);
                    unset($_SESSION['assign_property_key']);
                    return 1; 
                }else{
                    return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                }
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Assign Customer

    function assignCustomerToProperty($data="")
    {
        //$result =  array();
        if(isset($_SESSION['assign_customer_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['assign_customer_key']){
                $id             = $this->decryptData($data['session_token']);  
                $curr           = date("Y-m-d H:i:s");
                $allotment_date = $this->changeDateFormat($data['allotment_date']);
                $query = "UPDATE ".CUSTOMER_TBL." SET 
                        allotment_date = '',
                        assign_remarks = '',
                        assign_status  = '1',
                        updated_at     = '$curr' WHERE id = '".$data['customer_id']."' ";
                $exe = $this->selectQuery($query);
                 if($exe){
                    $q = "UPDATE ".PROPERTY_TBL." SET
                       customer_id      = '".$this->cleanString($data['customer_id'])."',
                       allotment_date   = '".$allotment_date."',
                       assign_remarks   = '".$this->cleanString($data['assign_remarks'])."',
                       assign_status    = '1',
                       updated_at       = '$curr' WHERE id='".$id."' ";
                    $result = $this->selectQuery($q);
                    unset($_SESSION['assign_customer_key']);
                    return 1; 
                }else{
                    return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                }
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    /*----------------------------
         Members Block
    ------------------------------*/

    // Add Block 


    function addBlock($data)
    {
        $layout = "";
        if(isset($_SESSION['add_block_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_block_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this -> check_query(BLOCK_TBL,"id"," token ='".$this->masterhyphenize($data['block'])."' ");
                if($check == 0){ 
                    $query = "INSERT INTO ".BLOCK_TBL." SET
                                token           = '".$this->masterhyphenize($data['block'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                block           = '".$this->cleanString($data['block'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_block_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }  
                }else{
                    return $this->errorMsg("Entered Block Name is already exist!");
                }    
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editBlock($id='')
    {   
        $result =  array();
        $_SESSION['edit_block_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(BLOCK_TBL,"id,block","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_block_key'];
        $result['block'] = $info['block'];
        return $result;
    }   

    // Update 

    function updateBlock($data)
    {
        if(isset($_SESSION['edit_block_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_block_key']){
                $check = $this->check_query(BLOCK_TBL,"id","  token ='".$this->masterhyphenize($data['block_info'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".BLOCK_TBL." SET 
                               token           = '".$this->masterhyphenize($data['block_info'])."',
                               block          = '".$this->cleanString($data['block_info'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['block_info'];
                        unset($_SESSION['edit_block_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Block Name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function BlockStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(BLOCK_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".BLOCK_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".BLOCK_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Members Floor
    ------------------------------*/

    // Add Floor 

    function addFloor($data)
    {
        $layout = "";
        if(isset($_SESSION['add_floor_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_floor_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this -> check_query(FLOOR_TBL,"id"," token ='".$this->masterhyphenize($data['floor'])."' ");
                if($check == 0){ 
                    $query = "INSERT INTO ".FLOOR_TBL." SET
                                token           = '".$this->masterhyphenize($data['floor'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                floor           = '".$this->cleanString($data['floor'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_floor_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                return $this->errorMsg("Entered Floor name is already exist");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editFloor($id='')
    {   
        $result =  array();
        $_SESSION['edit_floor_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(FLOOR_TBL,"id,floor","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_floor_key'];
        $result['floor'] = $info['floor'];
        return $result;
    }   

    // Update 

    function updateFloor($data)
    {
        if(isset($_SESSION['edit_floor_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_floor_key']){
                $check = $this->check_query(FLOOR_TBL,"id"," token ='".$this->masterhyphenize($data['floor_info'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".FLOOR_TBL." SET 
                               token          = '".$this->masterhyphenize($data['floor_info'])."',
                               floor          = '".$this->cleanString($data['floor_info'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['floor_info'];
                        unset($_SESSION['edit_floor_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Floor Name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function FloorStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(FLOOR_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".FLOOR_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".FLOOR_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
        Flat / Villa Documents Type
    ------------------------------*/

    // Add  

    function addFlatVillaDocumnt($data)
    {
        $layout = "";
        if(isset($_SESSION['add_fatvilla_document_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_fatvilla_document_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this -> check_query(FLATVILLA_DOC_TYPE,"id"," token ='".$this->masterhyphenize($data['document_title'])."' ");
                if($check == 0){
                    if (isset($data['documents_row'])) {
                        if (count($data['documents_row'])>0) {
                            $query = "INSERT INTO ".FLATVILLA_DOC_TYPE." SET
                                token           = '".$this->masterhyphenize($data['document_title'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                doc_title       = '".$this->cleanString($data['document_title'])."',
                                sort_order      = '".$this->cleanString($data['sort_order'])."',
                                status          = '1',
                                publish_status  = '0',
                                deleted_status  = '0',
                                deleted_by      = '0',
                                deleted_on      = '',
                                restored_by     = '0',
                                restored_on     = '',
                                restored_remarks= '',
                                added_by        = '$admin_id',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                        //return $query;
                            
                            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                            $exe =mysqli_query($link,$query);
                            $last_id = mysqli_insert_id($link);
                            if($exe){
                                if (isset($data['documents_row'])) {
                                    $document = $data['documents_row'];
                                    $documentmultifiles = $this->additionalFlatVillaDocumentTypeMultiFiles($document,$last_id);
                                }
                                unset($_SESSION['add_fatvilla_document_key']);
                                return 1;
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }  
                        }else{
                            return $this->errorMsg("Please add document types and submit.");
                        }  
                    }else{
                            return $this->errorMsg("Please add document types and submit.");
                    }    
                }else{
                    return $this->errorMsg("Entered Document title already exist!");
                } 
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit  

    function editFlatVillaDocumnt($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_fatvilla_document_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_fatvilla_document_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $check = $this -> check_query(FLATVILLA_DOC_TYPE,"id"," token ='".$this->masterhyphenize($data['document_title'])."' AND id!='".$id."' ");
                if($check == 0){
                    $info = $this -> getDetails(FLATVILLA_DOC_TYPE,"*"," id ='$id' ");
                    $countflattype = $this -> check_query(FLATVILLA_DOCTYPE_LIST,"*"," document_id ='".$info['id']."' ");
                    if (isset($data['documents_row']) || $countflattype>0 ) {
                            $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET
                                token           = '".$this->masterhyphenize($data['document_title'])."',
                                doc_title       = '".$this->cleanString($data['document_title'])."',
                                sort_order      = '".$this->cleanString($data['sort_order'])."',
                                added_by        = '$admin_id',
                                updated_at      = '$curr' WHERE id='".$id."' ";
                            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                            $exe =mysqli_query($link,$query);
                            if($exe){
                                if (isset($data['documents_row'])) {
                                    $document = $data['documents_row'];
                                    $documentmultifiles = $this->additionalFlatVillaDocumentTypeMultiFiles($document,$id);
                                }
                                unset($_SESSION['edit_fatvilla_document_key']);
                                return 1;
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }  
                        
                    }else{
                            return $this->errorMsg("Please add document types and submit.");
                    }    
                }else{
                    return $this->errorMsg("Entered Document title already exist!");
                } 
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Multiple File Insert

    function additionalFlatVillaDocumentTypeMultiFiles($document,$document_id)
    {
        $curr       = date("Y-m-d H:i:s");
        $project_id = @$_SESSION['crm_project_id'];
        if (count($document)>0) {
            foreach ($document as $each) {
                if ($each['name']!="") {
                    $contact_querry = "INSERT INTO ".FLATVILLA_DOCTYPE_LIST." SET
                        document_id     ='$document_id',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        doc_name        = '".$each['name']."' ,
                        description     = '".$each['description']."' ,
                        status          = '1',
                        created_at      ='".$curr."',
                        updated_at      ='".$curr."'  ";
                    $exe = $this->selectQuery($contact_querry);
                }
            }   
        }
        return 1;
    }

    // Avtive & Inactive Status 

    function faltVillaTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(FLATVILLA_DOC_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     // Edit Check List Document

    function editCheckListInformation($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $_SESSION['edit_checklist_doc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(FLATVILLA_DOCTYPE_LIST,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_checklist_doc_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='container-fluid'>                        
                        <div class='form-group row'>
                            <label for='example-text-input' class='col-12 col-form-label'>Document Name <en>*</en></label>
                            <div class='col-12'>
                                <input class='form-control' type='text' placeholder='Enter Document Name' name='doc_name' id='doc_name' value='".$info['doc_name']."' required>
                            </div>
                        </div>
                        
                        <div class='form-group row'>
                            <label for='example-text-input' class='col-12 col-form-label'>Description <en></en></label>
                            <div class='col-12'>
                                <textarea class='form-control' name='description' id='description'>".$info['description']."</textarea>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <div class='offset-sm-12 col-sm-12'>
                                <button type='submit' class='btn btn-danger' data-dismiss='modal'>Cancel</button>
                                <button type='submit'  class='btn btn-primary'>Submit</button>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    }

    // Update Check List Document

    function updateCheckListInfo($data="")
    {
        //$result =  array();
        if(isset($_SESSION['edit_checklist_doc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_checklist_doc_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".FLATVILLA_DOCTYPE_LIST." SET 
                        doc_name      = '".$data['doc_name']."' ,
                        description   = '".$data['description']."' WHERE id = '$id' ";
                //return    $query;     
                $exe = $this->selectQuery($query);
                unset($_SESSION['edit_checklist_doc_key']);
                $i      = $data['sno'];
                $layout ="<tr id='attacjmentrow_".$id."'>
                                <td width='15%'>".($data['doc_name'])."</td>
                                <td width='15%'>".($data['description'])."</td>
                                <td width='10%'>                                    
                                    <a href='javascript:void()' class='btn btn-success btn-sm editCheckListInformation' data-value='".$i."'  data-type='".$id."' data-option='".$id."' > <span class='icon ni ni-edit'></span></a>
                                    <a href='javascript:void()' class='btn btn-danger btn-sm deletCheckListInformation' data-value='".$id."' data-type='".$id."' data-option='".$id."'> <span class='icon ni ni-trash'></span></a>    
                                </td>
                            </tr>";
                 return "1`$layout"; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Delete Check List Document
    
    function deletCheckListInformation($data='')
    {       
        $id     = $data;
        $q = $this->deleteRow(FLATVILLA_DOCTYPE_LIST,"id='$id'");
        if($q){
            return 1;   
        }       
    }

    /*--------------------------------------------- 
                Home Banner
    ----------------------------------------------*/ 

    // Add 

    function addFlatVilla($data)
    {
        $layout = "";
        if(isset($_SESSION['add_property_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_property_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this -> check_query(PROPERTY_TBL,"id"," title ='".$data['title']."' AND block_id ='".$data['block_id']."' AND floor_id ='".$data['floor_id']."' AND room_id ='".$data['room_id']."'  ");
                if($check == 0){ 
                    $query = "INSERT INTO ".PROPERTY_TBL." SET
                                title           = '".$this->cleanString($data['title'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                customer_id     = '0',
                                allotment_date  = '',
                                assign_remarks  = '',
                                type            = '".$this->cleanString($data['type'])."',
                                house_facing    = '".$this->cleanString($data['house_facing'])."',
                                projectsize     = '".$this->cleanString($data['projectsize'])."',
                                projectarea     = '".$this->cleanString($data['projectarea'])."',
                                cost            = '".$this->cleanString($data['cost'])."',
                                room_id         = '".$this->cleanString($data['room_id'])."',
                                block_id        = '".$this->cleanString($data['block_id'])."',
                                floor_id        = '".$this->cleanString($data['floor_id'])."',
                                area_type_id    = '".$this->cleanString($data['area_type_id'])."',
                                address         = '".$this->cleanString($data['address'])."',
                                city            = '".$this->cleanString($data['city'])."',
                                state           = '".$this->cleanString($data['state'])."',
                                pincode         = '".$this->cleanString($data['pincode'])."',
                                content         = '".$this->cleanString($data['content'])."',
                                image           = '".$this->cleanString($data['image'])."',
                                added_by        = '$admin_id',
                                assign_status   = '0',
                                cancel_status   = '0',
                                reason_cancellation     = '',
                                cancel_remarks          = '',
                                status          = '1',
                                deleted_status   = '0',
                                deleted_by       = '0',
                                deleted_on       = '',
                                restored_by      = '0',
                                restored_on      = '',
                                restored_remarks = '',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                    //return $query;
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last_id = mysqli_insert_id($link);
                    if($exe){
                        $token = $this->createPropertyToken($data['title'],$last_id);
                        $update_q = "UPDATE ".PROPERTY_TBL. " SET token='$token' WHERE id='$last_id' ";
                        $update = $this->selectQuery($update_q);   
                        /*if (isset($data['property_row'])) {
                            $property = $data['property_row'];
                            $multifilesRooms = $this->addPropertyRooms($property,$images,$last_id);            
                        }*/

                        if (isset($data['document'])) {
                            $document = $data['document'];
                            $multifilesRooms = $this->addPropertyDocuments($document,$last_id);            
                        }
                        $paymentSchedule = $this->addPaymentSchedule($data,$last_id);  
                        unset($_SESSION['add_property_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }   
                }else{
                    return $this->errorMsg("Entered Property Title is already exist!");
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function addPaymentSchedule($data,$property_id)
    {   
        $curr            = date("Y-m-d H:i:s");
        $admin_id        = $_SESSION["crm_admin_id"];
        $project_id      = @$_SESSION['crm_project_id'];
        $due_date        = date('Y-m-d',strtotime("+30 days"));
        $contact_querry  = "INSERT INTO ".PAYMENT_SCHEDULE." SET
            tenant_id       = '".$_SESSION["tenant_id"]."',
            property_id     = '".$property_id."' ,
            project_id      = '".$project_id."',
            amt_payable     = '".$data['cost']."', 
            due_date        = '".$due_date."',
            amt_receive     = '0',
            balance         = '".$data['cost']."',
            status          = '1',
            added_by        = '$admin_id' ,
            created_at      = '$curr' ,
            updated_at      = '$curr'  ";
        $exe = $this->selectQuery($contact_querry);
        return 1;
    }

    // Edit 

    function editFlatVilla($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_property_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_property_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $check = $this -> check_query(PROPERTY_TBL,"id"," title ='".$data['title']."' AND block_id ='".$data['block_id']."' AND floor_id ='".$data['floor_id']."' AND room_id ='".$data['room_id']."'  AND id!='".$id."'  ");
                if($check == 0){ 
                    $query = "UPDATE ".PROPERTY_TBL." SET
                        title           = '".$this->cleanString($data['title'])."',
                        type            = '".$this->cleanString($data['type'])."',
                        house_facing    = '".$this->cleanString($data['house_facing'])."',
                        projectsize     = '".$this->cleanString($data['projectsize'])."',
                        projectarea     = '".$this->cleanString($data['projectarea'])."',
                        cost            = '".$this->cleanString($data['cost'])."',
                        room_id         = '".$this->cleanString($data['room_id'])."',
                        block_id        = '".$this->cleanString($data['block_id'])."',
                        floor_id        = '".$this->cleanString($data['floor_id'])."',
                        area_type_id    = '".$this->cleanString($data['area_type_id'])."',
                        address         = '".$this->cleanString($data['address'])."',
                        city            = '".$this->cleanString($data['city'])."',
                        state           = '".$this->cleanString($data['state'])."',
                        pincode         = '".$this->cleanString($data['pincode'])."',
                        content         = '".$this->cleanString($data['content'])."',
                        image           = '".$this->cleanString($data['image'])."',
                        added_by        = '$admin_id',
                        updated_at      = '$curr' WHERE id='".$id."' ";
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    if($exe){
                       /* $token = $this->createPropertyToken($data['title'],$id);
                        $update_q = "UPDATE ".PROPERTY_TBL. " SET token='$token' WHERE id='$last_id' ";
                        $update = $this->selectQuery($update_q);   */
                        if (isset($data['document'])) {
                            //$clean_document = $this->cleanpropertyDoc($id);
                            $document = $data['document'];
                            $multifilesRooms = $this->addPropertyDocuments($document,$id);            
                        }
                        unset($_SESSION['edit_property_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }   
                }else{
                    return $this->errorMsg("Entered Property Title is already exist!");
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function cleanpropertyDoc($property_id)
    {
        $exe = $this->deleteRow(PROPERTY_DOCUMENTS," property_id='$property_id' ");
        if ($exe) {
          return  $this->deleteRow(PROPERTY_DOCUMENTS_ITEMS," property_id='$property_id' ");
        }
    }

    function addPropertyDocuments($document,$property_id)
    {
        
        $curr           = date("Y-m-d H:i:s");
        $documents_array = $document;
        $admin_id       = $_SESSION["crm_admin_id"];
        $project_id     = @$_SESSION['crm_project_id'];
         if (count($document)>0) {
            foreach ($document as $each) {
                    $contact_querry = "INSERT INTO ".PROPERTY_DOCUMENTS." SET
                        property_id     ='$property_id' ,
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        documents_id    ='$each' ,
                        type            ='common' ,
                        status          ='1' ,
                        added_by        ='$admin_id' ,
                        created_at      ='$curr' ,
                        updated_at      ='$curr'  ";
                    $exe = $this->selectQuery($contact_querry);
                    $multidocfiles    = $this->addPropertyDocumentsItems($property_id,$each);   
            }   
        }
        return 1;
       
    }

    function addPropertyDocumentsItems($property_id,$document_id)
    {
        $layout = "";
        $admin_id       = $_SESSION["crm_admin_id"];
        $curr           = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        $tenant_id  = @$_SESSION["tenant_id"];
        $q = "SELECT id,document_id,doc_name,description,status,created_at FROM ".FLATVILLA_DOCTYPE_LIST." WHERE document_id='$document_id' AND tenant_id='".$tenant_id."' AND project_id='".$project_id."'  " ;
        $query = $this->selectQuery($q);    
        if(mysqli_num_rows($query) > 0){
            $i=1;
            while($details = mysqli_fetch_array($query)){
                $list        = $this->editPagePublish($details);
                $contact_querry = "INSERT INTO ".PROPERTY_DOCUMENTS_ITEMS." SET
                    document_id         ='".$list['document_id']."' ,
                    tenant_id           = '".$_SESSION["tenant_id"]."',
                    project_id          = '".$project_id."',
                    document_list_id    = '".$list['id']."' ,
                    property_id         = '$property_id' ,
                    doc_name            ='".$list['doc_name']."' ,
                    description         ='".$list['description']."' ,
                    document_file       = '',
                    status              ='1' ,
                    doc_status          ='0' ,
                    added_by            ='$admin_id' ,
                    created_at          ='$curr' ,
                    updated_at          ='$curr'  ";
                $exe = $this->selectQuery($contact_querry);
                $i++;
            }
        }
        return 1;
    }


    function createPropertyToken($name,$id)
    {
        $default = $this->hyphenize($name);
        $check = $this -> check_query(PROPERTY_TBL,"token"," token ='".$default."' ");
        if($check==0){
            return $default;
        }else{
            return $default.".".$id;
        }
    }

    // Multiple File Insert

    function addPropertyRooms($data="",$images="")
    {
        $layout = "";
        if(isset($_SESSION['add_propertyrooms_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_propertyrooms_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $event_id       = $this->decryptData($data["session_token"]);
                $files_array    = $images;
                $content_array  = $data['agenda_row'];
                $title          = array();
                $description    = array();
                $from           = array(); 
                $day            = array(); 
                $speaker_name   = array();
                $project_id     = @$_SESSION['crm_project_id'];
                if (count($images)>0) {
                    foreach ($content_array as $key => $value) {
                        $title[]            = $this->cleanString($value['title']);
                        $description[]      = $this->cleanString($value['description']);    
                        $from[]             = $this->cleanString($value['from']);
                        $day[]              = $this->cleanString($value['day']);
                        $speaker_name[]     = $this->cleanString($value['speaker']);                
                    }
                    $i = 0;
                    foreach ($images as $value) {
                        $output              = str_replace("uploads/srcimg/", "", $value);
                        $image_info          = explode(".", $output); 
                        $image_type          = end($image_info);
                        $insert_title        = $title[$i];
                        $insert_description  = $description[$i];
                        $insert_from         = $from[$i];
                        $insert_day          = $day[$i];
                        $insert_speaker_name = $speaker_name[$i];


                        // Upload to S3
                        $targetPath = "../resource/uploads/srcimg/".$output;
                        S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                        unlink($targetPath);

                        $agenda_querry = "INSERT INTO ".EVENT_AGENDA_TBL." SET
                            event_id        = '$event_id',
                            tenant_id       = '".$_SESSION["tenant_id"]."',
                            project_id      = '".$project_id."',
                            title           = '".$insert_title."' ,
                            start_time      = '".$insert_from."' ,
                            agenda          = '".$insert_description."' ,
                            agenda_day      = '".$insert_day."' ,
                            image           = '".$output."',
                            speaker_name    = '".$insert_speaker_name."' ,
                            created_at      = '".$curr."',
                            updated_at      = '".$curr."'  ";
                        $result = mysql_query($agenda_querry);
                        $i++;
                    }
                }
                unset($_SESSION['add_propertyrooms_key']);
                return 1;
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Remove Event Image

    function removePropertyImgIcon($data)
    {
        $query = "UPDATE ".PROPERTY_TBL." SET image='' WHERE id='$data' ";
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }   
    }

    function cancelProperty($data)
    {
        $layout = "";
        if(isset($_SESSION['cancel_property_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['cancel_property_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data["session_token"]);
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "UPDATE ".PROPERTY_TBL." SET
                    cancel_status           = '1',
                    reason_cancellation     = '".$this->cleanString($data['reason_cancellation'])."',
                    cancel_remarks          = '".$this->cleanString($data['cancel_remarks'])."',
                    cancel_by               = '$admin_id',
                    updated_at              = '$curr' WHERE id='".$id."' ";
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe   = mysqli_query($link,$query);
                if($exe){
                    $info = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$id."' ");
                    $pquery = "INSERT INTO ".PROPERTY_TBL." SET
                                title           = '".$this->cleanString($info['title'])."',
                                project_id      = '".$project_id."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                customer_id     = '0',
                                allotment_date  = '',
                                assign_remarks  = '',
                                type            = '".$this->cleanString($info['type'])."',
                                house_facing    = '".$this->cleanString($info['house_facing'])."',
                                projectsize     = '".$this->cleanString($info['projectsize'])."',
                                projectarea     = '".$this->cleanString($info['projectarea'])."',
                                cost            = '".$this->cleanString($info['cost'])."',
                                room_id         = '".$this->cleanString($info['room_id'])."',
                                block_id        = '".$this->cleanString($info['block_id'])."',
                                floor_id        = '".$this->cleanString($info['floor_id'])."',
                                area_type_id    = '".$this->cleanString($info['area_type_id'])."',
                                address         = '".$this->cleanString($info['address'])."',
                                city            = '".$this->cleanString($info['city'])."',
                                state           = '".$this->cleanString($info['state'])."',
                                pincode         = '".$this->cleanString($info['pincode'])."',
                                content         = '".$this->cleanString($info['content'])."',
                                image           = '".$this->cleanString($info['image'])."',
                                added_by        = '$admin_id',
                                assign_status   = '0',
                                cancel_status   = '0',
                                reason_cancellation     = '',
                                cancel_remarks          = '',
                                cancel_by       = '0',
                                status          = '1',
                                deleted_status   = '0',
                                deleted_by       = '0',
                                deleted_on       = '',
                                restored_by      = '0',
                                restored_on      = '',
                                restored_remarks = '',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                    //return $pquery;
                    $plink  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $pexe =mysqli_query($plink,$pquery);
                    $last_id = mysqli_insert_id($plink);
                    if($pexe){
                        $token = $this->createPropertyToken($info['title'],$last_id);
                        $update_q = "UPDATE ".PROPERTY_TBL. " SET token='$token' WHERE id='$last_id' ";
                        $update = $this->selectQuery($update_q); 

                        unset($_SESSION['cancel_property_key']);
                        return 1;
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*----------------------------
            Contact Info
    ------------------------------*/

    // Add  

    function addContactInfo($data)
    {
        $layout = "";
        if(isset($_SESSION['add_contactinfo_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_contactinfo_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".CONTACT_INFO." SET
                    token           = '".$this->hyphenize($data['name'])."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    project_id      = '".$project_id."',
                    name            = '".$this->cleanString($data['name'])."',
                    role            = '".$this->cleanString($data['role'])."',
                    email           = '".$this->cleanString($data['email'])."',
                    mobile          = '".$this->cleanString($data['mobile'])."',
                    status          = '1',
                    added_by        = '$admin_id',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                            //return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    /*if (isset($data['contacts_row'])) {
                        $contactinfo = $data['contacts_row'];
                        $ContactInfomultifiles = $this->additionalContactInfoMultiFiles($contactinfo,$last_id);
                    }*/
                    unset($_SESSION['add_contactinfo_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

     // Edit  

    function editContactInfo($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_contactinfo_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_contactinfo_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $id             = $this->decryptData($data['session_token']);
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".CONTACT_INFO." SET
                    token           = '".$this->hyphenize($data['name'])."',
                    name            = '".$this->cleanString($data['name'])."',
                    role            = '".$this->cleanString($data['role'])."',
                    email           = '".$this->cleanString($data['email'])."',
                    mobile          = '".$this->cleanString($data['mobile'])."',
                    added_by        = '$admin_id',
                    updated_at      = '$curr' WHERE id='".$id."' ";
                            //return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                if($exe){
                   
                    unset($_SESSION['edit_contactinfo_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    // Multiple File Insert

    function additionalContactInfoMultiFiles($contactinfo,$contactinfo_id)
    {
        $curr           = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        if (count($contactinfo)>0) {
            foreach ($contactinfo as $each) {
                if ($each['name']!="") {
                    $contact_querry = "INSERT INTO ".CONTACT_INFO_LIST." SET
                        contact_id   ='$contactinfo_id',
                        tenant_id    = '".$_SESSION["tenant_id"]."',
                        project_id   = '".$project_id."',
                        name         = '".$each['name']."' ,
                        designation  = '".$each['designation']."' ,
                        mobile       = '".$each['mobile']."' ,
                        email        = '".$each['email']."' ,
                        status       = '1',
                        created_at   ='".$curr."',
                        updated_at   ='".$curr."'  ";
                    $exe = $this->selectQuery($contact_querry);
                }
            }   
        }
        return 1;
    }

    // Avtive & Inactive Status 

    function ContactInfoStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(CONTACT_INFO,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CONTACT_INFO." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CONTACT_INFO." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Remove 
    function deleteContactInfo($id)
    {
        return $this->deleteRow(CONTACT_INFO," id='$id' ");
    }

     // Edit  

    function editCompanySettings($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_companysettings_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_companysettings_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $id             = $this->decryptData($data['session_token']);
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".COMPANY_SETTINGS." SET
                    phone            = '".$this->cleanString($data['phone'])."',
                    email            = '".$this->cleanString($data['email'])."',
                    website_address  = '".$this->cleanString($data['website_address'])."',
                    address          = '".$this->cleanString($data['address'])."',
                    added_by         = '$admin_id',
                    created_at       = '$curr',
                    updated_at       = '$curr' WHERE id='".$id."' ";
                            //return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                if($exe){
                    unset($_SESSION['edit_companysettings_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function CompanySettingsInfoStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(COMPANY_SETTINGS,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".COMPANY_SETTINGS." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".COMPANY_SETTINGS." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     /*----------------------------
            Payement Info
    ------------------------------*/

    // Add  

    function addPaymentInfo($data)
    {
        $layout = "";
        if(isset($_SESSION['add_paymentinfo_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_paymentinfo_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".PAYMENT_INFO." SET
                    project_id      = '".$project_id."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    name            = '".$this->cleanString($data['name'])."',
                    account_number  = '".$this->cleanString($data['account_number'])."',
                    bank            = '".$this->cleanString($data['bank'])."',
                    ifcs_code       = '".$this->cleanString($data['ifcs_code'])."',
                    branch          = '".$this->cleanString($data['branch'])."',
                    city            = '".$this->cleanString($data['city'])."',
                    state           = '".$this->cleanString($data['state'])."',
                    pincode         = '".$this->cleanString($data['pincode'])."',
                    status          = '1',
                    added_by        = '$admin_id',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                            //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['add_paymentinfo_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit  

    function editPaymentInfo($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_paymentinfo_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_paymentinfo_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".PAYMENT_INFO." SET
                    name            = '".$this->cleanString($data['name'])."',
                    account_number  = '".$this->cleanString($data['account_number'])."',
                    bank            = '".$this->cleanString($data['bank'])."',
                    ifcs_code       = '".$this->cleanString($data['ifcs_code'])."',
                    branch          = '".$this->cleanString($data['branch'])."',
                    city            = '".$this->cleanString($data['city'])."',
                    state           = '".$this->cleanString($data['state'])."',
                    pincode         = '".$this->cleanString($data['pincode'])."',
                    added_by        = '$admin_id',
                    updated_at      = '$curr' WHERE id='".$id."' ";
                            //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['edit_paymentinfo_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function PaymentInfoStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PAYMENT_INFO,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PAYMENT_INFO." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PAYMENT_INFO." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Remove 

    function deletePaymentInfo($id)
    {
        return $this->deleteRow(PAYMENT_INFO," id='$id' ");
    }

    /*----------------------------
            Employee management
    ------------------------------*/

    // Add  Employee 

    function addEmployee($data="")
    {
        if(isset($_SESSION['add_employee_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_employee_key']){  
                $e_check = $this -> check_query(EMPLOYEE,"id"," email ='".$data['email']."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(EMPLOYEE,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  
                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        $project_id     = @$_SESSION['crm_project_id'];
                        //$password       = $this->generateRandomStringGenerator("8");
                        //$psw            = $this->encryptPassword($password);
                        $em_password    = $this->encryptPassword($data['password']);
                        $query = "INSERT INTO ".EMPLOYEE." SET 
                            token        = '".$this->hyphenize($data['name'])."',
                            tenant_id    = '".$_SESSION["tenant_id"]."',
                            project_id   = '".$project_id."',
                            name         = '".$this->cleanString($data['name'])."',
                            role_id      = '".$this->cleanString($data['role_id'])."',
                            department_id = '".$this->cleanString($data['department_id'])."',
                            is_manager    = '".$this->cleanString($data['is_manager'])."',
                            gender       = '".$this->cleanString($data['gender'])."',
                            password     = '".$em_password."',
                            email        = '".$this->cleanString($data['email'])."',
                            mobile       = '".$this->cleanString($data['mobile'])."',
                            address      = '".$this->cleanString($data['address'])."',
                            city         = '".$this->cleanString($data['city'])."',
                            state        = '".$this->cleanString($data['state'])."',
                            pincode      = '".$this->cleanString($data['pincode'])."',
                            status       = '1',
                            super_admin  = '0',
                            added_by     = '$admin_id',   
                            created_at   = '$curr',
                            updated_at   = '$curr' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $employee_uid = "EMP".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".EMPLOYEE." SET employee_uid='".$employee_uid."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);

                            $employee_permission = $this->insertEmployeePermission($last_id);

                           /* $sender_mail    = NO_REPLY;
                            $subject        = "Login details for"." - ".ucwords($data['company_name'])." - Account login informatotion.";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['company_name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['member_name'],$data['email'],$password,$user_token,$last_id,$data['company_name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                unset($_SESSION['add_members_key']);
                                return 1;
                            }*/

                            unset($_SESSION['add_employee_key']);
                                return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit  Employee 

    function editEmployee($data="")
    {
        if(isset($_SESSION['edit_employee_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_employee_key']){ 
                $id = $this->decryptData($data['session_token']); 
                $e_check = $this -> check_query(EMPLOYEE,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(EMPLOYEE,"id"," mobile ='".$data['mobile']."' AND id!='".$id."' "); 
                    if($m_check == 0){  
                        $admin_id       =$_SESSION["crm_admin_id"];
                        $curr           = date("Y-m-d H:i:s");
                        $token          = $this->generateRandomString("30");
                        $query = "UPDATE ".EMPLOYEE." SET 
                            token        = '".$this->hyphenize($data['name'])."',
                            name         = '".$this->cleanString($data['name'])."',
                            role_id      = '".$this->cleanString($data['role_id'])."',
                            department_id = '".$this->cleanString($data['department_id'])."',
                            is_manager    = '".$this->cleanString($data['is_manager'])."',
                            gender       = '".$this->cleanString($data['gender'])."',
                            email        = '".$this->cleanString($data['email'])."',
                            mobile       = '".$this->cleanString($data['mobile'])."',
                            address      = '".$this->cleanString($data['address'])."',
                            city         = '".$this->cleanString($data['city'])."',
                            state        = '".$this->cleanString($data['state'])."',
                            pincode      = '".$this->cleanString($data['pincode'])."',
                            added_by     = '$admin_id',
                            updated_at   = '$curr' WHERE id='".$id."' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        if($exe){     
                            unset($_SESSION['edit_employee_key']);
                                return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                    return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                }       
                }else{
                return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    /*-------------------------------
            Employee Permissions
    --------------------------------*/

    function insertEmployeePermission($employee_id)
    {
        $curr       = date("Y-m-d H:i:s");
        $admin_id       =$_SESSION["crm_admin_id"];
        $project_id     = @$_SESSION['crm_project_id'];
        $q = "INSERT INTO ".PERMISSION."    SET
                employee_id         ='$employee_id',
                tenant_id           = '".$_SESSION["tenant_id"]."',
                project_id          = '".$project_id."',
                added_by            = '$admin_id', 
                lead                = '0',
                manage_lead         = '0',
                import_lead         = '0',
                customers           = '0',
                manage_customer     = '0',
                kyc_documents       = '0',
                invoice             = '0',
                import_customer     = '0',
                property            = '0',
                manage_property     = '0',
                property_chart      = '0',
                gallery             = '0',
                adhoc               = '0',
                open_request        = '0',
                close_request       = '0',
                news                = '0',
                manage_news         = '0',
                settings            = '0',
                employee            = '0',
                manage_department   = '0',
                virtual_tour        = '0',
                notification        = '0',
                reports             = '0',
                adhoc_report        = '0',
                lead_report         = '0',
                customer_report     = '0',
                kyc_report          = '0',
                master_settings     = '0',
                lead_type           = '0',
                activity_type       = '0',
                lead_source         = '0',
                profile_master      = '0',
                general_documents   = '0',
                document_checklist  = '0',
                block               = '0',
                floor               = '0',
                flat_type           = '0',
                brochure            = '0',
                contact_info        = '0',
                payment_info        = '0',
                area_master         = '0',
                department          = '0',
                role_master         = '0',
                kycdoc_type         = '0',
                company             = '0',
                trash               = '0',
                upload_video        = '0',

                adhoc_request       = '0',
                all_request         = '0',
                documents           = '0',
                request_type        = '0',
                activity_report     = '0',
                status = '1', 
                created_at  ='$curr ',
                updated_at  ='$curr ' ";
        $exe = $this->selectQuery($q);
        if($exe){
            return 1;
        }   
        
    }

    function employeePermissions($data)
    {
        $employee_id    = $this->decryptData($data["session_token"]);
        $curr       = date("Y-m-d H:i:s");
        if(isset($_SESSION['create_permission_key'])){
            if($data["fkey"]==$_SESSION['create_permission_key']){              
                $clean_permissions = $this->cleanPermissions($employee_id);
                if(isset($data["permission"])){
                    $permissions_array = $data["permission"];
                    if(count($permissions_array)>0) {
                        $q = "UPDATE ".PERMISSION." SET employee_id='$employee_id'  ";
                        foreach ($permissions_array as $key => $value) {
                            $q .=  ", ".$value." = '1' ";
                        }
                        $q .= ", updated_at='$curr' WHERE employee_id='$employee_id' ";
                        //echo $q; 
                       $exe = $this->selectQuery($q);
                        if($exe){
                            unset($_SESSION['create_permission_key']);
                            return 1;
                        }
                    }else{
                        return "Please Select atleast One permission to the Employee.";
                    }
                }else{
                    return "Please Select atleast One permission to the Employee.";
                }                   
                
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Clean current permission

    function cleanPermissions($employee_id)
    {
        $q = "UPDATE ".PERMISSION." SET 
                lead                = '0',
                manage_lead         = '0',
                import_lead         = '0',
                customers           = '0',
                manage_customer     = '0',
                kyc_documents       = '0',
                invoice             = '0',
                import_customer     = '0',
                property            = '0',
                manage_property     = '0',
                property_chart      = '0',
                gallery             = '0',
                adhoc               = '0',
                open_request        = '0',
                close_request       = '0',
                news                = '0',
                manage_news         = '0',
                settings            = '0',
                employee            = '0',
                manage_department   = '0',
                virtual_tour        = '0',
                notification        = '0',
                reports             = '0',
                adhoc_report        = '0',
                lead_report         = '0',
                customer_report     = '0',
                kyc_report          = '0',
                master_settings     = '0',
                lead_type           = '0',
                activity_type       = '0',
                lead_source         = '0',
                profile_master      = '0',
                general_documents   = '0',
                document_checklist  = '0',
                block               = '0',
                floor               = '0',
                flat_type           = '0',
                brochure            = '0',
                contact_info        = '0',
                payment_info        = '0',
                area_master         = '0',
                department          = '0',
                role_master         = '0',
                kycdoc_type         = '0',
                company             = '0',
                trash               = '0',
                upload_video        = '0',

                adhoc_request       = '0',
                all_request         = '0',
                documents           = '0',
                request_type        = '0',
                activity_report     = '0'
                WHERE employee_id='$employee_id'
            ";
        $exe = $this->selectQuery($q);
        if($exe){
            return 1;
        }
    }

    // Active & Inactive Status 

    function employeeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(EMPLOYEE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".EMPLOYEE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".EMPLOYEE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Change Password Employee

    function changepasswordEmployee($id='')
    {   
        $result =  array();
        $_SESSION['employee_changepassword_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(EMPLOYEE,"id","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['employee_changepassword_key'];
        return $result;
    }

    // Update Change Password

    function empChangePassword($data)
    {
        if(isset($_SESSION['employee_changepassword_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['employee_changepassword_key']){
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".EMPLOYEE." SET 
                            password= '". $this->encryptPassword($data['confirm_pass'])."',
                            updated_at='$curr' WHERE id='".$data['token']."' "; 
                $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";                  
                    unset($_SESSION['employee_changepassword_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Active & Inactive Status 

    function propertyStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROPERTY_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROPERTY_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROPERTY_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }


    /*==============================================================
                         General Documents
    ================================================================*/

    function addGendralDocuments($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['add_gendral_documents_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_gendral_documents_key']){
                $attachment     = $this->cleanString($files['pdf']);
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");

                $output         = str_replace("uploads/document/", "", $attachment);
                $doc_info       = explode(".", $output); 
                $document_type  = end($doc_info);
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".GENDRAL_DOCUMENTS." SET
                            token            = '$token',
                            tenant_id        = '".$_SESSION["tenant_id"]."',
                            project_id       = '".$project_id."',
                            title            = '".$this->cleanString($data['title'])."',
                            description      = '".$this->cleanString($data['description'])."',
                            documents        = '".$attachment."',
                            document_type    = '".$document_type."',
                            status           = '1',
                            deleted_status   = '0',
                            deleted_by       = '0',
                            deleted_on       = '',
                            restored_by      = '0',
                            restored_on      = '',
                            restored_remarks = '',
                            added_by         = '".$admin_id."',
                            created_at       = '$curr',
                            updated_at       = '$curr' ";
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['add_gendral_documents_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

     function editGendralDocuments($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['edit_gendral_documents_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_gendral_documents_key']){
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       =$_SESSION["crm_admin_id"];
                $id             = $this->decryptData($data['session_token']);

                if ($data['file_test']==1) {
                    $attachment     = '';
                    $output         = '';
                    $doc_info       = ''; 
                    $document_type  = '';
                }else{
                    $attachment     = $this->cleanString($files['pdf']);
                    $output         = str_replace("uploads/document/", "", $attachment);
                    $doc_info       = explode(".", $output); 
                    $document_type  = end($doc_info);
                }
                
                $query = "UPDATE ".GENDRAL_DOCUMENTS." SET
                    title            = '".$this->cleanString($data['title'])."',
                    description      = '".$this->cleanString($data['description'])."', ";

                    if ($data['file_test']==0) {
                    $query .= "
                            documents        = '".$attachment."',
                            document_type    = '".$document_type."',";        
                    }

                    $query .= "
                    added_by         = '".$admin_id."',
                    updated_at       = '".$curr."' WHERE id='".$id."' ";
                //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['edit_gendral_documents_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Active & Inactive Status 

    function generalDocumentStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(GENDRAL_DOCUMENTS,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".GENDRAL_DOCUMENTS." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".GENDRAL_DOCUMENTS." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Remove Doc

    function removeGeneralDocument($id)
    {
        $q = "UPDATE ".GENDRAL_DOCUMENTS." SET documents ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    /*==============================================================
                          Property Broucher
    ================================================================*/

    function addPropertyBroucher($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['add_property_broucher_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_property_broucher_key']){
                $admin_id       =$_SESSION["crm_admin_id"];
                $attachment     = $this->cleanString($files['pdf']);
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".PROPERTY_BROUCHERS." SET
                            token            = '$token',
                            tenant_id        = '".$_SESSION["tenant_id"]."',
                            project_id       = '".$project_id."',
                            title            = '".$this->cleanString($data['title'])."',
                            property_id      = '".$this->cleanString($data['project_id'])."',
                            description      = '".$this->cleanString($data['description'])."',
                            brochures        = '".$attachment."',
                            status           = '1',
                            deleted_status   = '0',
                            deleted_by       = '0',
                            deleted_on       = '',
                            restored_by      = '0',
                            restored_on      = '',
                            restored_remarks = '',
                            added_by         = '$admin_id',
                            created_at       = '$curr',
                            updated_at       = '$curr' ";
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['add_property_broucher_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function editPropertyBroucher($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['edit_property_broucher_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_property_broucher_key']){
                $admin_id       =$_SESSION["crm_admin_id"];
                $attachment     = $this->cleanString($files['pdf']);
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $query = " UPDATE ".PROPERTY_BROUCHERS." SET
                            title            = '".$this->cleanString($data['title'])."',
                            property_id      = '".$this->cleanString($data['project_id'])."',
                            description      = '".$this->cleanString($data['description'])."',";

                            if ($data['file_test']==0) {
                            $query .= "
                                    brochures        = '".$attachment."',";        
                            }
                     $query .= "                        
                            added_by         = '$admin_id',                            
                            updated_at       = '$curr' WHERE id='".$id."' ";
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['edit_property_broucher_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Remove Doc

    function removeBroucher($id)
    {
        $q = "UPDATE ".PROPERTY_BROUCHERS." SET brochures ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    // Active & Inactive Status 

    function propertyBroucherStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROPERTY_BROUCHERS,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROPERTY_BROUCHERS." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROPERTY_BROUCHERS." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Members Request type
    ------------------------------*/

    // Add Request type 

    function addRequestType($data)
    {
        $layout = "";
        if(isset($_SESSION['add_request_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_request_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                 $check = $this->check_query(REQUEST_TYPE_TBL,"id"," token ='".$this->masterhyphenize($data['request_type'])."' ");
                if ($check==0) {
                    $query = "INSERT INTO ".REQUEST_TYPE_TBL." SET
                                token           = '".$this->masterhyphenize($data['request_type'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                request_type    = '".$this->cleanString($data['request_type'])."',
                                department_id   = '".$this->cleanString($data['dept_id'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_request_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }  
                }else{
                    return $this->errorMsg("Sorry Entered Request type is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editRequestType($id='')
    {   
        $result =  array();
        $_SESSION['edit_request_type_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(REQUEST_TYPE_TBL,"id,request_type,department_id","id='$id'");
        $result['layout'] = "
            <input type='hidden' value='".$_SESSION['edit_request_type_key']."' name='fkey' id='editfkey'>
            <input type='hidden' value='".$id."' name='token' id='token'>
            <div class='tab-content'>
                <div class='tab-pane active' id='personal'>
                    <div class='row gy-4'>
                        <div class='col-md-12'>
                            <div class='form-group'>
                                <label class='form-label' for='requesttype_info'>Request type <en>*</en></label>
                                <input type='text' class='form-control' id='requesttype_info' name='request_type' value='".$info['request_type']."'>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class='form-group'>
                                <label class='form-label' for='fva-topics'>Department <en>*</en></label>
                                <div class='form-control-wrap '>
                                    <select class='form-control form-select ' id='depart_id' name='depart_id' data-placeholder='Select a Department' required='' data-select2-id='fva-top' tabindex='-1' aria-hidden='true'>
                                        ".$this->getDepartmentList($info['department_id'])."
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-12'>
                            <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                <li>
                                    <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                </li>
                                <li>
                                    <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        ";
        return $result;
    }   

    // Update 

    function updateRequestType($data)
    {
        if(isset($_SESSION['edit_request_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_request_type_key']){
                $check = $this->check_query(REQUEST_TYPE_TBL,"id"," token ='".$this->masterhyphenize($data['request_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".REQUEST_TYPE_TBL." SET 
                               token          = '".$this->masterhyphenize($data['request_type'])."',
                               request_type   = '".$this->cleanString($data['request_type'])."',
                               department_id  = '".$this->cleanString($data['depart_id'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['request_type'];
                        unset($_SESSION['edit_request_type_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Request type is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function requestTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(REQUEST_TYPE_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".REQUEST_TYPE_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".REQUEST_TYPE_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         KYC Request type
    ------------------------------*/

    // Add KYC type 

    function addKYCType($data)
    {
        $layout = "";
        if(isset($_SESSION['add_kyc_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_kyc_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(KYC_TYPE,"id"," token ='".$this->masterhyphenize($data['kyc_type'])."' ");
                if ($check==0) {
                    $query = "INSERT INTO ".KYC_TYPE." SET
                                token           = '".$this->masterhyphenize($data['kyc_type'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                kyc_type        = '".$this->cleanString($data['kyc_type'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_kyc_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered KYC type is already exist!.");
                }       
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

     // Edit 

    function editkycType($id='')
    {   
        $result =  array();
        $_SESSION['edit_kyc_type_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(KYC_TYPE,"id,kyc_type","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_kyc_type_key'];
        $result['kyc_type'] = $info['kyc_type'];
        return $result;
    }   

    // Update 

    function updatekycType($data)
    {
        if(isset($_SESSION['edit_kyc_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_kyc_type_key']){
                $check = $this->check_query(KYC_TYPE,"id"," token ='".$this->masterhyphenize($data['kyc_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".KYC_TYPE." SET 
                               token          = '".$this->masterhyphenize($data['kyc_type'])."',
                               kyc_type   = '".$this->cleanString($data['kyc_type'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['kyc_type'];
                        unset($_SESSION['edit_kyc_type_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered KYC type is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function kycTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(KYC_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".KYC_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".KYC_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
       Notification Email
    ------------------------------*/

    // Add Notification Email

    function addNotificationEmail($data)
    {
        $layout = "";
        if(isset($_SESSION['notification_email_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['notification_email_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                if (isset($data['adhoc_permission'])) {
                    $adhoc_permission =1 ;
                }else{
                    $adhoc_permission =0 ;
                }
                if (isset($data['kyc_permission'])) {
                    $kyc_permission =1 ;
                }else{
                    $kyc_permission =0 ;
                }

                $query = "INSERT INTO ".NOTIFICATION_EMAIL_TBL." SET
                    token           = '".$this->hyphenize($data['email'])."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    project_id      = '".$project_id."',
                    email           = '".$this->cleanString($data['email'])."',
                    adhoc_permission= '".($adhoc_permission)."', 
                    kyc_permission  = '".($kyc_permission)."', 
                    added_by        = '$admin_id',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                            //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['notification_email_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editNotification($id='')
    {   
        $result =  array();
        $_SESSION['edit_notification_email_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(NOTIFICATION_EMAIL_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_notification_email_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                   

                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Email <en>*</en></label>
                                        <input type='email' class='form-control form-control-lg' id='editemail' name='email' value='".$info['email']."' placeholder='Enter Email'>
                                    </div>
                                </div>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <div class='row g-3 align-center'>
                                            <div class='col-lg-12'>
                                                <label class='form-label'>Notification Type <en>*</en></label>
                                            </div>
                                            <div class='col-lg-12'>
                                                <ul class='custom-control-group g-3 align-center flex-wrap'>
                                                    <li>
                                                        <div class='custom-control custom-checkbox'>
                                                            <input type='checkbox' class='custom-control-input' name='adhoc_permission' id='adhoc_permissions' ".($info['adhoc_permission']=='1' ? 'checked' : '' ).">
                                                            <label class='custom-control-label' for='adhoc_permissions'>Adhoc Requests</label>
                                                        </div>

                                                    </li>
                                                    <li>
                                                         <div class='custom-control custom-checkbox'>
                                                            <input type='checkbox' class='custom-control-input' name='kyc_permission' id='kyc_permissions' ".($info['kyc_permission']=='1' ? 'checked' : '' ).">
                                                            <label class='custom-control-label' for='kyc_permissions'>KYC Notification</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateNotification($data)
    {
        if(isset($_SESSION['edit_notification_email_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_notification_email_key']){
                $check = $this->check_query(NOTIFICATION_EMAIL_TBL,"id"," email ='".$data['email']."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");

                    if (isset($data['adhoc_permission'])) {
                        $adhoc_permission =1 ;
                    }else{
                        $adhoc_permission =0 ;
                    }
                    if (isset($data['kyc_permission'])) {
                        $kyc_permission =1 ;
                    }else{
                        $kyc_permission =0 ;
                    }

                    $query = "UPDATE ".NOTIFICATION_EMAIL_TBL." SET 
                               token           = '".$this->hyphenize($data['email'])."',
                               email           = '".$this->cleanString($data['email'])."',
                               adhoc_permission= '".($adhoc_permission)."', 
                               kyc_permission  = '".($kyc_permission)."', 
                               added_by        = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['email'];
                        unset($_SESSION['edit_notification_email_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Email is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function notificationEmailStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(NOTIFICATION_EMAIL_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".NOTIFICATION_EMAIL_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".NOTIFICATION_EMAIL_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*--------------------------------------------- 
                Gallery
    ----------------------------------------------*/ 

    // Add 

    function addGallery($data)
    {
        $layout = "";
        if(isset($_SESSION['add_gallery_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_gallery_key']){
                //$token            = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".GALLERY_TBL." SET
                    project_id      = '".$project_id."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    album_title     = '".$this->cleanString($data['album_title'])."',
                    sort_order      = '".$this->cleanString($data['sort_order'])."',
                    description     = '".$this->cleanString($data['description'])."',
                    image           = '".$this->cleanString($data['image'])."',
                    deleted_status   = '0',
                    deleted_by       = '0',
                    deleted_on       = '',
                    restored_by      = '0',
                    restored_on      = '',
                    restored_remarks = '',
                    added_by        = '$admin_id',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                    //return $query;
                //$exe = $this->selectQuery($query);
                $link    = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe     = mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    $token = $this->createGalleryToken($data['album_title'],$last_id);
                    $update_q = "UPDATE ".GALLERY_TBL. " SET token='$token' WHERE id='$last_id' ";
                    $update = $this->selectQuery($update_q);    
                    if ($update) {
                        unset($_SESSION['add_gallery_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit

    function editGallery($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_gallery_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_gallery_key']){
                //$token            = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".GALLERY_TBL." SET
                    album_title     = '".$this->cleanString($data['album_title'])."',
                    sort_order      = '".$this->cleanString($data['sort_order'])."',
                    description     = '".$this->cleanString($data['description'])."',
                    image           = '".$this->cleanString($data['image'])."',
                    added_by        = '$admin_id',
                    updated_at      = '$curr' WHERE id='".$id."' ";
                    //return $query;
                //$exe = $this->selectQuery($query);
                $link    = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe     = mysqli_query($link,$query);
                if($exe){
                    $token = $this->createGalleryToken($data['album_title'],$id);
                    $update_q = "UPDATE ".GALLERY_TBL. " SET token='$token' WHERE id='$id' ";
                    $update = $this->selectQuery($update_q);    
                    if ($update) {
                        unset($_SESSION['edit_gallery_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function createGalleryToken($name,$id)
    {
        $default = $this->hyphenize($name);
        $check = $this -> check_query(GALLERY_TBL,"token"," token ='".$default."' ");
        if($check==0){
            return $default;
        }else{
            return $default.".".$id;
        }
    }

    // Avtive & Inactive Status 

    function galleryStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(GALLERY_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".GALLERY_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".GALLERY_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     // Remove Image

    function removeGalleryImgIcon($data)
    {
        $query = "UPDATE ".GALLERY_TBL." SET image='' WHERE id='$data' ";
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }   
    }

    // Add images to the Album

    function addGalleryImages($images=[],$token)
    {
        $id         = $this->decryptData($token);
        $curr       = date("Y-m-d H:i:s");
        $admin_id       = $_SESSION["crm_admin_id"];
        $project_id     = @$_SESSION['crm_project_id'];
        $q="";
        if(count($images)>0){
            foreach ($images as $value) {
                $output = str_replace("uploads/srcimg/", "", $value);

                // Upload to S3
                $targetPath = "../resource/uploads/srcimg/".$output;
                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                unlink($targetPath);

                $q = "INSERT INTO ".GALLERY_IMAGE. " SET 
                    project_id      = '".$project_id."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    gallery_id      = '$id', 
                    image           = '$output',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                $exe = $this->selectQuery($q);
            }
            return 1;
        }else{
            return "Please select images to upload.";
        }
    }

    // Remove Gallery Image

    function removePropertyImage($id)
    {
        return $this->deleteRow(GALLERY_IMAGE," id='$id' ");
    }

    function setmasterSession($id)
    {   
        if ($id=="mastersettings") {
            $_SESSION["mastersettings"] = $id;
        }else{
            unset($_SESSION['mastersettings']);
        }
    }



    /*------------------------------------------
                Property Gallery
    ---------------------------------------------*/

     // Add images to the Album

    function addPropertyGalleryImages($images=[],$token)
    {
        $id         = $this->decryptData($token);
        $curr       = date("Y-m-d H:i:s");
        $admin_id       = $_SESSION["crm_admin_id"];
        $project_id     = @$_SESSION['crm_project_id'];
        $q="";
        if(count($images)>0){
            foreach ($images as $value) {
                $output = str_replace("uploads/srcimg/", "", $value);

                // Upload to S3
                $targetPath = "../resource/uploads/srcimg/".$output;
                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                unlink($targetPath);

                $q = "INSERT INTO ".PROPERTY_GALLERY. " SET 
                    project_id      = '".$project_id."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    property_id     = '$id', 
                    images          = '$output',
                    status          = '1',
                    added_by        = '".$admin_id."',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                $exe = $this->selectQuery($q);
            }
            return 1;
        }else{
            return "Please select images to upload.";
        }
    }

    // Remove Gallery Image

    function removePropertyGalleryImages($id)
    {
        return $this->deleteRow(PROPERTY_GALLERY," id='$id' ");
    }

    /*------------------------------------------
                Property Room Gallery
    ---------------------------------------------*/

     // Add images to the Album

    function addPropertyRoomImages($images=[],$data)
    {
        $roomlist_id= $this->decryptData($data['token']);
        $curr       = date("Y-m-d H:i:s");
        $admin_id       = $_SESSION["crm_admin_id"];
        $project_id     = @$_SESSION['crm_project_id'];
        $q="";
        if(count($images)>0){
            foreach ($images as $value) {
                $output = str_replace("uploads/srcimg/", "", $value);

                // Upload to S3
                $targetPath = "../resource/uploads/srcimg/".$output;
                S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                unlink($targetPath);
                $q = "INSERT INTO ".PROPERTY_ROOMS_LIST. " SET 
                    property_id     = '".$this->cleanString($data['property_id'])."',
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    project_id      = '".$project_id."',
                    roomlist_id     = '$roomlist_id',
                    image           = '$output',
                    status          = '1',
                    added_by        = '".$admin_id."',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                $exe = $this->selectQuery($q);
            }
            return 1;
        }else{
            return "Please select images to upload.";
        }
    }

    // Remove Gallery Image

    function removePropertyRoomImages($id)
    {
        return $this->deleteRow(PROPERTY_ROOMS_LIST," id='$id' ");
    }


    /*----------------------------
        Flat Type Master
    ------------------------------*/

    // Add  

    function addFlattypemaster($data)
    {
        $layout = "";
        if(isset($_SESSION['add_flattype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_flattype_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];

                $check = $this -> check_query(FLATTYPE_MASTER,"id"," token ='".$this->masterhyphenize($data['flat_variant'])."' ");
                if($check == 0){ 
                    if (isset($data['flattype_row'])) {
                        if (count($data['flattype_row'])>0) {
                            $query = "INSERT INTO ".FLATTYPE_MASTER." SET
                                token           = '".$this->masterhyphenize($data['flat_variant'])."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                flat_variant    = '".$this->cleanString($data['flat_variant'])."',
                                description     = '".$this->cleanString($data['description'])."',
                                status          = '1',
                                added_by        = '$admin_id',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                        //return $query;
                            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                            $exe =mysqli_query($link,$query);
                            $last_id = mysqli_insert_id($link);
                            if($exe){
                                if (isset($data['flattype_row'])) {
                                    $flattype = $data['flattype_row'];
                                    $documentmultifiles = $this->additionalFlattypeMaster($flattype,$last_id);
                                }
                                unset($_SESSION['add_flattype_key']);
                                return 1;
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }
                        }else{
                            return $this->errorMsg("Please add the rooms and submit.");
                        }  
                    }else{
                            return $this->errorMsg("Please add the rooms and submit.");
                        }    
                }else{
                    return $this->errorMsg("Entered flat variant is already exist!");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

     // Edit  

    function editFlattypemaster($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_flattype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_flattype_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id = $this->decryptData($data['session_token']);

                $check = $this -> check_query(FLATTYPE_MASTER,"id"," token ='".$this->masterhyphenize($data['flat_variant'])."' AND id!='".$id."' ");
                if($check == 0){   
                    $info = $this -> getDetails(FLATTYPE_MASTER,"*"," id ='$id' ");
                    $countflattype = $this -> check_query(FLATTYPE_MASTER_LIST,"*"," flattype_id ='".$info['id']."' ");
                    if (isset($data['flattype_row']) || $countflattype>0 ) {
                        $query = "UPDATE ".FLATTYPE_MASTER." SET
                            token           = '".$this->masterhyphenize($data['flat_variant'])."',
                            flat_variant    = '".$this->cleanString($data['flat_variant'])."',
                            description     = '".$this->cleanString($data['description'])."',
                            updated_at      = '$curr' WHERE id='$id' ";
                                    //return $query;
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                       // $last_id = mysqli_insert_id($link);
                        if($exe){
                            if (isset($data['flattype_row'])) {
                                $flattype = $data['flattype_row'];
                                $documentmultifiles = $this->additionalFlattypeMaster($flattype,$id);
                            }
                            unset($_SESSION['edit_flattype_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Please add the rooms and submit.");
                    }    
                }else{
                    return $this->errorMsg("Entered flat variant is already exist!");
                }    
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    // Multiple File Insert

    function additionalFlattypeMaster($flattype,$flattype_id)
    {
        $curr       = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        if (count($flattype)>0) {
            foreach ($flattype as $each) {
                if ($each['room_type']!="") {
                    $contact_querry = "INSERT INTO ".FLATTYPE_MASTER_LIST." SET
                        tenant_id    = '".$_SESSION["tenant_id"]."',
                        flattype_id  ='$flattype_id',
                        project_id   = '".$project_id."',
                        room_type    = '".$each['room_type']."' ,
                        sqft         = '".$each['sqft']."' ,
                        description  = '".$each['description']."' ,
                        status       = '1',
                        created_at   ='".$curr."',
                        updated_at   ='".$curr."'  ";
                    $exe = $this->selectQuery($contact_querry);
                }
            }   
        }
        return 1;
    }

    // Avtive & Inactive Status 

    function flattypemasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(FLATTYPE_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".FLATTYPE_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".FLATTYPE_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Edit Flat Type Master Information Attachments

    function editFlatTypeMasterInformation($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $_SESSION['edit_flattype_attch_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(FLATTYPE_MASTER_LIST,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_flattype_attch_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='container-fluid'>                        
                        <div class='form-group row'>
                            <label for='example-text-input' class='col-12 col-form-label'>Room Type <en>*</en></label>
                            <div class='col-12'>
                                <input class='form-control' type='text' placeholder='Enter Room Type' name='room_type' id='room_type' value='".$info['room_type']."' required>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='example-text-input' class='col-12 col-form-label'> Sqft <en>*</en></label>
                            <div class='col-12'>
                                <input class='form-control' type='text' placeholder='Enter sqft ' name='sqft' id='sqft' value='".$info['sqft']."' required>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <label for='example-text-input' class='col-12 col-form-label'>Description <en></en></label>
                            <div class='col-12'>
                                <textarea class='form-control' name='description' id='description'>".$info['description']."</textarea>
                            </div>
                        </div>
                        <div class='form-group row'>
                            <div class='offset-sm-12 col-sm-12'>
                                <button type='submit' class='btn btn-danger' data-dismiss='modal'>Cancel</button>
                                <button type='submit'  class='btn btn-primary'>Submit</button>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    }

    // Update Flat Type Master Information

    function updateFlatTypeInfoDetails($data="")
    {
        //$result =  array();
        if(isset($_SESSION['edit_flattype_attch_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_flattype_attch_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $query = "UPDATE ".FLATTYPE_MASTER_LIST." SET 
                        room_type      = '".$data['room_type']."' ,
                        sqft           = '".$data['sqft']."' ,
                        description    = '".$data['description']."' WHERE id = '$id' ";
                //return    $query;     
                $exe = $this->selectQuery($query);
                unset($_SESSION['edit_flattype_attch_key']);
                $i      = $data['sno'];
                $layout ="<tr id='attacjmentrow_".$id."'>
                                <td width='15%'>".($data['room_type'])."</td>
                                <td width='15%'>".($data['sqft'])."</td>
                                <td width='15%'>".($data['description'])."</td>
                                <td width='10%'>                                    
                                    <a href='javascript:void()' class='btn btn-success btn-sm editFlatTypeMasterInformation' data-value='".$i."'  data-type='".$id."' data-option='".$id."' > <span class='icon ni ni-edit'></span></a>
                                    <a href='javascript:void()' class='btn btn-danger btn-sm deletFlatTypeInformation' data-value='".$id."' data-type='".$id."' data-option='".$id."'> <span class='icon ni ni-trash'></span></a>    
                                </td>
                            </tr>";
                 return "1`$layout"; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Delete Flat Type Master Information 
    
    function deletFlatTypeInformation($data='')
    {       
        $id     = $data;
        $q = $this->deleteRow(FLATTYPE_MASTER_LIST,"id='$id'");
        if($q){
            return 1;   
        }       
    }

    // Edit Private Attachments

    function uploadDocfile_old($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $type    = $data[2];
        $property_id    = $data[3];
        $_SESSION['edit_uploadfile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*","id='$id' ");
        $file = (($info['document_file']=='') ? "<div class='form-group row ' >
                            <div class='col-sm-12 '> 
                                <label for='example-search-input' class='col-form-label'>Upload Document</label>
                            </div>
                            <div class='col-sm-12'>
                                
                                <input type='file' required id='cimage' name='cimage' />
                                <input type='hidden' id='customerImage' name='files'>
                            </div>
                        </div>"  : 
                        "<div class='form-group row' >
                            <div class='col-sm-12'>
                            <label for='example-search-input' class='col-4 col-form-label'></label>
                            </div>
                            <div class='col-sm-12'>
                            <input type='hidden' id='cimage' name='cimage' value='' />
                                <input type='hidden' id='customerImage' name='files' value='".$info['document_file']."'>
                                <input type='hidden' value='1' name='old_image'>
                                <div class='customer_pic_edit'>
                                    <div class='fileupload-preview fileupload-large thumbnail'><a target='blank' class='' href='".DOCUMENTS.$info['document_file']."''><img src='".IMGPATH."file.png' style='width: 120px;margin-bottom: 10px;margin-left: -15px;''></a></div>
                                    <button type='button' class='btn btn-danger btn-sm deletPropertyDocInformation' data-type='image' data-option='".$info['id']."'><i class='icon ni ni-trash'></i></button>
                                    <a target='blank' class='btn btn-success btn-sm' href='".DOCUMENTS.$info['document_file']."''><i class='icon ni ni-eye'></i></a>
                                </div>
                            </div>
                        </div>"
                         );

        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_uploadfile_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='container-fluid'>
                        ".$file."
                    </div>";
        $result['layout'] = $layout;
        return $result;

    }

    function uploadDocfile($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $type    = $data[2];
        $property_id    = $data[3];
        $_SESSION['edit_uploadfile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='".$info['property_id']."' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $file = (($info['document_file']=='') ? "<div class='nk-file-details-col' style='width: 150px !important;'>Upload Document</div>
                            <div class='nk-file-details-col' >
                                <input type='file' required id='cimage' name='cimage' />
                                <input type='hidden' id='customerImage' name='files'>
                                <input type='hidden' id='' value='0' name='image_check'>
                            </div>": 
                            "<div class='nk-file-details-col' style='width: 150px !important;'>Upload Document</div>
                                <div class='nk-file-details-col'>
                                <input type='hidden' id='cimage' name='cimage' value='' />
                                <input type='hidden' id='customerImage' name='files' value='".$info['document_file']."'>
                                <input type='hidden' value='1' name='image_check'>
                                <div class='customer_pic_edit'>
                                    <div class='fileupload-preview fileupload-large thumbnail'><a target='blank' class='' href='".DOCUMENTS.$info['document_file']."''><img src='".IMGPATH."file.png' style='width: 120px;margin-bottom: 10px;margin-left: -15px;''></a></div>
                                    <button type='button' class='btn btn-danger btn-sm deletPropertyDocInformation' data-type='image' data-option='".$info['id']."'><i class='icon ni ni-trash'></i></button>
                                    <a target='blank' class='btn btn-success btn-sm' href='".DOCUMENTS.$info['document_file']."''><i class='icon ni ni-eye'></i></a>
                                </div>
                            </div>");

        $layout = "    
                <input type='hidden' value='".$_SESSION['edit_uploadfile_key']."' name='fkey' id='materal_group_fkey'>
                <input type='hidden' value='".$info['id']."' name='option' id='option'>
                <input type='hidden' value='".$info['id']."' name='option' id='option'>
                <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>

                <div class='modal-content ContentDocInfo' style='width: 550px;'>
                    <div class='modal-header align-center'>
                        <div class='nk-file-title'>
                            <div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
                                        <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
                                        <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
                                        <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
                                        <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
                                    </svg>
                                </span>
                            </div>
                            <div class='nk-file-name'>
                                <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
                                <div class='nk-file-name-sub'>Private Documents</div>
                            </div>
                        </div>
                        <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
                    </div>
                    <div class='modal-body'>
                        <div class='nk-file-details'>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col' style='width: 150px !important;'>Title</div>
                                <div class='nk-file-details-col'>".$info['doc_name']."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col' style='width: 150px !important;'>Description</div>
                                <div class='nk-file-details-col'>".($info['description'])."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                ".$file."
                            </div>
                            
                        </div>
                    </div>
                    <div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li>
                                        <a href='javascript:void();' data-dismiss='modal' class='btn btn-md btn-danger'>Cancel</a>
                                    </li>
                                    <li>
                                        <button type='submit' class='btn btn-md btn-primary'>Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>         
                ";
        $result['layout'] = $layout;
        return $result;

    }

    // Update Private Files

    function updateDocumentsFiles($data="")
    {
        //$result =  array();
        if(isset($_SESSION['edit_uploadfile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_uploadfile_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                
                $info           = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*", "id='$id'");
               /* if(isset($data['old_image'])) {
                    $test = "";
                }else{
                    unlink("uploads/srcimg/".$info['image']);
                }*/
                if ($data['image_check']==0) {
                    $output         = str_replace("uploads/document/", "", $data['files']);
                    $image_info     = explode(".", $output); 
                    $image_type     = end($image_info);
                    $document_file  = $this->cleanString($data['files']);
                }else{
                    $image_type     = $info['doc_type'];
                    $document_file  = $info['document_file'];
                }   
                $query = "UPDATE ".PROPERTY_DOCUMENTS_ITEMS." SET 
                        document_file   = '".$document_file."',
                        doc_status      = '1',
                        doc_type        = '".$image_type."',
                        updated_at      = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);
                unset($_SESSION['edit_uploadfile_key']);
                //$pic        = $data['image']!="" ? "<a target='blank' href='".SRC_IMG.$data['image']. "'><img style='width:70px;' class='img-responsive img-thumbnail' src='".SRC_IMG.$data['image']. "' alt=''></a>": "<img class='img-circle' src='".SRC_IMG."no_img.png' alt=''>" ;
                //$i      = $data['sno'];
                return 1; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function deletPropertyDocInformation($id)
    {
        $q = "UPDATE ".PROPERTY_DOCUMENTS_ITEMS." SET 
            document_file   ='',
            doc_status      ='0',
            doc_type        ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    // Edit Private Attachments

    function uploadGenDocfile_old($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $type    = $data[2];
        $property_id    = $data[3];
        $_SESSION['edit_uploadgenfile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*","id='$id' ");
        $file = (($info['documents']=='') ? "<div class='form-group row ' >
                            <div class='col-sm-12 '> 
                                <label for='example-search-input' class='col-form-label'>Upload Documents</label>
                            </div>
                            <div class='col-sm-12'>
                                <input type='file' required id='cimage' name='cimage' />
                                <input type='hidden' id='customerImage' name='files'>
                            </div>
                        </div>"  : 
                        "<div class='form-group row' >
                            <div class='col-sm-12'>
                            <label for='example-search-input' class='col-4 col-form-label'></label>
                            </div>
                            <div class='col-sm-12'>
                            <input type='hidden' id='cimage' name='cimage' value='' />
                                <input type='hidden' id='customerImage' name='files' value='".$info['documents']."'>
                                <input type='hidden' value='1' name='old_image'>
                                <div class='customer_pic_edit'>
                                    <div class='fileupload-preview fileupload-large thumbnail'><a target='blank' class='' href='".DOCUMENTS.$info['documents']."''><img src='".IMGPATH."file.png' style='width: 120px;margin-bottom: 10px;margin-left: -15px;''></a></div>
                                    <button type='button' class='btn btn-danger btn-sm deletPropertyGenDocInformations' data-type='image' data-option='".$info['id']."'><i class='icon ni ni-trash'></i></button>
                                    <a target='blank' class='btn btn-success btn-sm' href='".DOCUMENTS.$info['documents']."''><i class='icon ni ni-eye'></i></a>
                                </div>
                            </div>
                        </div>"
                         );

        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_uploadgenfile_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='container-fluid'>
                        ".$file."
                    </div>";
        $result['layout'] = $layout;
        return $result;

    }

     function uploadGenDocfile($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $sno    = $data[1];
        $type    = $data[2];
        $property_id    = $data[3];
        $_SESSION['edit_uploadgenfile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='".$info['property_id']."' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        
        $file = (($info['documents']=='') ? "<div class='nk-file-details-col' style='width: 150px !important;'>Upload Document</div>
                            <div class='nk-file-details-col' >
                                <input type='file' required id='cimage' name='cimage' />
                                <input type='hidden' id='customerImage' name='files'>
                                <input type='hidden' id='' value='0' name='image_check'>
                            </div>": 
                            "<div class='nk-file-details-col' style='width: 150px !important;'>Upload Document</div>
                                <div class='nk-file-details-col'>
                                <input type='hidden' id='cimage' name='cimage' value='' />
                                <input type='hidden' id='customerImage' name='files' value='".$info['documents']."'>
                                <input type='hidden' value='1' name='image_check'>
                                <div class='customer_pic_edit'>
                                    <div class='fileupload-preview fileupload-large thumbnail'><a target='blank' class='' href='".DOCUMENTS.$info['documents']."''><img src='".IMGPATH."file.png' style='width: 120px;margin-bottom: 10px;margin-left: -15px;''></a></div>
                                    <button type='button' class='btn btn-danger btn-sm deletPropertyGenDocInformations' data-type='image' data-option='".$info['id']."'><i class='icon ni ni-trash'></i></button>
                                    <a target='blank' class='btn btn-success btn-sm' href='".DOCUMENTS.$info['documents']."''><i class='icon ni ni-eye'></i></a>
                                </div>
                            </div>");

        $layout = "    
                <input type='hidden' value='".$_SESSION['edit_uploadgenfile_key']."' name='fkey' id='materal_group_fkey'>
                <input type='hidden' value='".$info['id']."' name='option' id='option'>
                <input type='hidden' value='".$info['id']."' name='option' id='option'>
                <input type='hidden' value='".$sno."' name='sno' id='sno'>          
                <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>

                <div class='modal-content ContentDocInfo' style='width: 550px;'>
                    <div class='modal-header align-center'>
                        <div class='nk-file-title'>
                            <div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
                                        <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
                                        <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
                                        <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
                                        <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
                                    </svg>
                                </span>
                            </div>
                            <div class='nk-file-name'>
                                <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
                                <div class='nk-file-name-sub'>Private Documents</div>
                            </div>
                        </div>
                        <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
                    </div>
                    <div class='modal-body'>
                        <div class='nk-file-details'>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col' style='width: 150px !important;'>Title</div>
                                <div class='nk-file-details-col'>".$info['title']."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col' style='width: 150px !important;'>Description</div>
                                <div class='nk-file-details-col'>".($info['description'])."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                ".$file."
                            </div>
                            
                        </div>
                    </div>
                    <div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li>
                                        <a href='javascript:void();' data-dismiss='modal' class='btn btn-md btn-danger'>Cancel</a>
                                    </li>
                                    <li>
                                        <button type='submit' class='btn btn-md btn-primary'>Submit</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>         
                ";
        $result['layout'] = $layout;
        return $result;

    }

    // Update Private Files

    function updateGenDocumentsFiles($data="")
    {
        //$result =  array();
        if(isset($_SESSION['edit_uploadgenfile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_uploadgenfile_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $info           = $this->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*", "id='$id'");
               if ($data['image_check']==0) {
                    $output         = str_replace("uploads/document/", "", $data['files']);
                    $image_info     = explode(".", $output); 
                    $image_type     = end($image_info);
                    $document_file  = $this->cleanString($data['files']);
                }else{
                    $image_type     = $info['document_type'];
                    $document_file  = $info['documents'];
                }   
                $query = "UPDATE ".PROPERTY_GENDRAL_DOCUMENTS." SET 
                        documents       = '".$document_file."',
                        document_type   = '$image_type',
                        updated_at      = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);
                unset($_SESSION['edit_uploadgenfile_key']);
                //$pic        = $data['image']!="" ? "<a target='blank' href='".SRC_IMG.$data['image']. "'><img style='width:70px;' class='img-responsive img-thumbnail' src='".SRC_IMG.$data['image']. "' alt=''></a>": "<img class='img-circle' src='".SRC_IMG."no_img.png' alt=''>" ;
                //$i      = $data['sno'];
                return 1; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function deletPropertyGenDocInformations($id)
    {
        $q = "UPDATE ".PROPERTY_GENDRAL_DOCUMENTS." SET 
            documents       ='',
            document_type   ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    /*==============================================================
                        Add Gendral Property Documnets
    ================================================================*/

    function addPropertyGendralDocuments($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['add_propertygendral_documents_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_propertygendral_documents_key']){
                $attachment     = $this->cleanString($files['pdf']);
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];

                $output         = str_replace("uploads/document/", "", $attachment);
                $doc_info       = explode(".", $output); 
                $document_type  = end($doc_info);
                $project_id     = @$_SESSION['crm_project_id'];

                $query = "INSERT INTO ".PROPERTY_GENDRAL_DOCUMENTS." SET
                            token            = '$token',
                            tenant_id        = '".$_SESSION["tenant_id"]."',
                            project_id       = '".$project_id."',
                            property_id      = '".$data['property_id']."',
                            title            = '".$this->cleanString($data['title'])."',
                            description      = '".$this->cleanString($data['description'])."',
                            documents        = '".$attachment."',
                            document_type    = '".$document_type."',
                            status           = '1',
                            added_by         = '$admin_id', 
                            created_at       = '$curr',
                            updated_at       = '$curr' ";
                //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['add_propertygendral_documents_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*----------------------------
            Adhoc requests
    ------------------------------*/

    // Replay 


    function replayAdhocRequest($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['reply_ticket_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['reply_ticket_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $info           = $this->getDetails(ADOC_REQUEST,"*"," id='".$data['ref_id']."'  ");
                $query = "INSERT INTO ".REQUEST_LOGS." SET
                    project_id  = '".$project_id."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    type        = 'reply',
                    request_type= 'admin',
                    created_by  = '0',
                    created_to  = '$admin_id',
                    ref_id      = '".$data['ref_id']."',
                    remarks     = '".$data['remarks']."',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
                //return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    $que = "UPDATE ".ADOC_REQUEST." SET 
                        updated_at      = '$curr' WHERE id='".$data['ref_id']."' ";
                    $result = $this->selectQuery($que);



                    $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$info['customer_id']."' ");
                    $propertyinfo = $this -> getDetails(PROPERTY_TBL,"*"," id ='".$info['property_id']."' ");
                    $admininfo = $this->getDetails(EMPLOYEE,"*"," id='".$admin_id."' ");
        

                    if(count($images)>0){
                        $content_array  = $data['adhoc_row'];
                        $description    = array();
                        foreach ($content_array as $key => $value) {
                            $description[]          = $this->cleanString($value['description']);        
                        }
                        $i = 0;
                        foreach ($images as $value) {   
                            $output              = str_replace("uploads/srcimg/", "", $value);
                            $image_info          = explode(".", $output); 
                            $image_type          = end($image_info);
                            $insert_description  = $description[$i];    


                            // Upload to S3
                            $targetPath = "../resource/uploads/srcimg/".$output;
                            S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                            unlink($targetPath);

                            $qu = "INSERT INTO ".ADOC_IMAGES." SET
                                adoc_id         = '".$data['ref_id']."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                ref_id          = '".$last_id."',
                                property_id     = '".$this->cleanString($info['property_id'])."',
                                customer_id     = '".$this->cleanString($info['customer_id'])."',
                                image_name      = '$output',
                                file_type       = '$image_type',
                                description     = '".$insert_description."', 
                                status          ='1', 
                                created_at      = '$curr', 
                                updated_at      = '$curr'  ";
                            $exe2 = $this->selectQuery($qu);
                            $i++;
                        }

                        // TO Customer Reply 
                        $sender_mail    = NO_REPLY;
                        $subject        = COMPANY_NAME." - New reply for the Adhoc request ".$info['ticket_uid']." by ".ucwords($admininfo['name']) ;
                        $receiver       = $cusinfo['email'];
                        $email_temp     = $this->replyKYCSubmitNotification($info['ticket_uid'],$info['customer_id'],$info['property_id'],$data,$data['ref_id']);            
                        $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

                        unset($_SESSION['reply_ticket_key']);
                        return 1;
                    }else{
                        // TO Customer Reply 
                        $sender_mail    = NO_REPLY;
                        $subject        = COMPANY_NAME." - New reply for the Adhoc request ".$info['ticket_uid']." by ".ucwords($admininfo['name']) ;
                        $receiver       = $cusinfo['email'];
                        $email_temp     = $this->replyKYCSubmitNotification($info['ticket_uid'],$info['customer_id'],$info['property_id'],$data,$data['ref_id']);            
                        $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

                         unset($_SESSION['reply_ticket_key']);
                        return 1;
                    }
                    
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    function replayAdhocRequest_old($data)
    {
        $layout = "";
        if(isset($_SESSION['reply_ticket_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['reply_ticket_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".REQUEST_LOGS." SET
                    project_id  = '".$project_id."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    type        = 'reply',
                    request_type= 'admin',
                    created_by  = '0',
                    created_to  = '$admin_id',
                    ref_id      = '".$data['ref_id']."',
                    remarks     = '".$data['remarks']."',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
                         //  return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    unset($_SESSION['reply_ticket_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

     // Avtive & Inactive Status 

    function closeTicket($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(ADOC_REQUEST,"request_status"," id ='$data' ");
        if($info['request_status'] ==1){
            $query = "UPDATE ".ADOC_REQUEST." SET request_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".ADOC_REQUEST." SET request_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }


    /*--------------------------------
            Assign Adhoc
    ---------------------------------*/

    // Assign Call 

    function assignAdhocModel($id='')
    {   
        $result =  array();
        $_SESSION['add_assign_adhoc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(ADOC_REQUEST,"id,customer_id","id='$id'");
        $cusinfo = $this->getDetails(CUSTOMER_TBL,"id,name,mobile,email","id='".$info['customer_id']."' ");
        $result['id']       = $id;
        $result['name']     = ucwords($cusinfo['name']);
        $result['mobile']   = $cusinfo['mobile'];
        $result['email']    = $cusinfo['email'];
        $result['fkey']     = $_SESSION['add_assign_adhoc_key'];
        return $result;
    }  

    function assignAdhoc($data="")
    {
        if(isset($_SESSION['add_assign_adhoc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_assign_adhoc_key']){          
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $project_id     = @$_SESSION['crm_project_id'];

                    $check = $this->check_query(ASSIGN_ADHOC,"id"," ref_id='".$data['adhoc_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                            $info  = $this->getDetails(ASSIGN_ADHOC,"id,created_to"," ref_id='".$data['adhoc_id']."' ORDER BY id DESC LIMIT 1 ");
                            $update = "UPDATE ".ASSIGN_ADHOC." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                    }   
                    $query = "INSERT INTO ".ASSIGN_ADHOC." SET 
                                project_id      = '".$project_id."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                type            = 'assign',
                                created_by      = '$admin_id',
                                created_to      = '".$data['employee_id']."',
                                ref_id          = '".$data['adhoc_id']."',   
                                priority        = '".$this->cleanString($data['priority'])."',
                                remarks         = '".$this->cleanString($data['remarks'])."',
                                status          = '1',
                                remove_status   = '0',  
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                    //return $query; 
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last = mysqli_insert_id($link);
                    if($exe){
                        $q =" UPDATE ".ADOC_REQUEST." SET 
                            assign_status ='1',
                            assign_employee_id = '".$data['employee_id']."',
                            closed_status = '0' WHERE id='".$data['adhoc_id']."' ";
                        $result = $this->selectQuery($q);
                        if ($result) {
                            $lead_info = $this->getDetails(ADOC_REQUEST,"*"," id='".$data['adhoc_id']."' ");
                            $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");

                            $options['ref_id']      = $data['adhoc_id'];
                            $options['created_by']  = $admin_id;
                            $options['created_to']  = $data['employee_id'];
                            $options['remarks']     = $this->cleanString($data['remarks']);
                            $notification           = $this->adhocLogs("assigned",$options); 
                            unset($_SESSION['add_assign_adhoc_key']);
                            return 1;   
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }                       
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Reassign Call 

    function reassignAdhocModel($id='')
    {   
        $result =  array();
        $_SESSION['add_reassign_adhoc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(ADOC_REQUEST,"*","id='$id'");
        $cusinfo = $this->getDetails(CUSTOMER_TBL,"id,name,mobile,email","id='".$info['customer_id']."' ");

        $assigninfo = $this->getDetails(ASSIGN_ADHOC,"*","ref_id='$id'");
        $result['id']           = $id;
        $name = ucwords($cusinfo['name']);
        $result['layout']       = "

            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['add_reassign_adhoc_key']."'>
                <input type='hidden' name='adhoc_id' id='token' value='".$id."'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Reassign Adhoc to Employee</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <p >
                                            <em class='icon ni ni-user'></em> <span style='margin-right: 10px;'>".$name."</span> 
                                            <em class='icon ni ni-mobile'></em> <span style='margin-right: 10px;'>".$cusinfo['mobile']."</span> 
                                            <em class='icon ni ni-mail'></em> <span >".$cusinfo['email']."</span>
                                        </p>
                                    </div>
                                </div>

                                
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for=''>Current Owner <en></en></label>
                                        <select class='form-control form-select ' id='' name='' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true' disabled>
                                            ".$this->getEmployeeList($assigninfo['created_to'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='employee_id'>Reassign Employee <en>*</en></label>
                                        <select class='form-control form-select ' id='employee_id' name='employee_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getAdhocEmployeeList()."
                                        </select>
                                    </div>
                                </div>   
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='priority'>Lead Priority <en>*</en></label>
                                        <select class='form-control ' name='priority' id='priority'>
                                           <option value='low'>Low</option>
                                           <option value='medium'>Medium</option>
                                           <option value='high'>High</option>
                                           <option value='urgent'>Urgent</option>
                                        </select> 
                                    </div>
                                </div> 
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='remarks' name='remarks' placeholder='Write your Remarks'></textarea>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    } 

    function reassignAdhoc($data="")
    {
        if(isset($_SESSION['add_reassign_adhoc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_reassign_adhoc_key']){            
                    $user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");  
                    $project_id     = @$_SESSION['crm_project_id'];
                    $check = $this->check_query(ASSIGN_ADHOC,"id"," ref_id='".$data['adhoc_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                        $info  = $this->getDetails(ASSIGN_ADHOC,"id,created_to"," ref_id='".$data['adhoc_id']."' ORDER BY id DESC LIMIT 1 ");
                        $call_status = "UPDATE ".ADOC_REQUEST." SET 
                                assign_status='1',
                                closed_status = '0',
                                assign_employee_id = '".$data['employee_id']."',
                                updated_at      = '$curr' WHERE id='".$data['adhoc_id']."' ";
                        $call_status = $this->selectQuery($call_status);
                        if ($call_status) {
                            $update = "UPDATE ".ASSIGN_ADHOC." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                            if($update_status){
                                $query = "INSERT INTO ".ASSIGN_ADHOC." SET 
                                    project_id      = '".$project_id."',
                                    tenant_id       = '".$_SESSION["tenant_id"]."',
                                    type            = 'reassign',
                                    created_by      = '$user_id',
                                    created_to      = '".$data['employee_id']."',
                                    ref_id          = '".$data['adhoc_id']."',
                                    priority        = '".$this->cleanString($data['priority'])."',
                                    remarks         = '".$this->cleanString($data['remarks'])."',
                                    status          = '1',  
                                    remove_status   = '0',
                                    created_at      = '$curr',
                                    updated_at      = '$curr' ";
                                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                                $exe =mysqli_query($link,$query);
                                $last = mysqli_insert_id($link);
                                if($exe){   
                                    $call_info = $this->getDetails(ADOC_REQUEST,"*"," id='".$data['adhoc_id']."' ");
                                    $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");
                                    $emp_info_app = $this->getDetails(EMPLOYEE,"*"," id='".$info['created_to']."' ");
                                    /*if ($emp_info_app['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info_app['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "Your call has been reassigned ",
                                            'subtitle'  => $call_info['call_id']." has been removed from your list",
                                            'type'      => 'reassign',
                                            'user_id'   => $info['created_to'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }
                                    if ($emp_info['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "New call assigned to you",
                                            'subtitle'  => $call_info['call_id']." assigned for you, click to view",
                                            'type'      => 'assign',
                                            'user_id'   => $data['employee_id'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }*/

                                    $options['ref_id']      = $data['adhoc_id'];
                                    $options['created_by']  = $user_id;
                                    $options['created_to']  = $data['employee_id'];
                                    $options['remarks']     = $this->cleanString($data['remarks']);
                                    $notification           = $this->adhocLogs("reassigned",$options);   
                                    unset($_SESSION['add_reassign_adhoc_key']);
                                    return 1;                                               
                                }else{
                                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                                }       
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again...";
                    }                           
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*--------------------------------------------- 
                Adhoc Logs
    ----------------------------------------------*/

    // Add Adhoc Logs

    function adhocLogs($type,$options)
    {   
        $curr           = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        switch ($type) {
            case 'created':
                $created_by  = $options['created_by'];              
                $ref_id      = $options['ref_id'];
                $query = "INSERT INTO ".ASSIGN_ADHOC_LOGS." SET
                    project_id  = '".$project_id."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    type        = 'created',
                    adhoc_type  = 'manual',
                    created_by  = '$created_by',
                    created_to  = '0',
                    ref_id      = '$ref_id',
                    status      = '1',
                    remarks     = '',  
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'assigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".ASSIGN_ADHOC_LOGS." SET
                    project_id  = '".$project_id."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    type        = 'assigned',
                    adhoc_type  = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',   
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'reassigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".ASSIGN_ADHOC_LOGS." SET
                    project_id  = '".$project_id."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    type        = 'reassigned',
                    adhoc_type  = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            default:
            break;
        }
        //return $query;
        $exe = $this->selectQuery($query);
        if ($exe) {
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }
    }


    function approverejectkyc($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        $status     = $data[1];
        $_SESSION['update_kyc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(CUSTOMER_TBL,"*","id='$id' ");
        $result['name'] = ucwords($info['name']).' ('.$info['uid'].')' ;
        $result['fkey'] = $_SESSION['update_kyc_key'] ;
        $result['kyc_status'] = $status ;
        $result['token'] = $this->encryptData($info['id']) ;
       /* $layout = "             
                    <input type='hidden' value='".$_SESSION['update_kyc_key']."' name='fkey' id='materal_group_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>
                    <input type='hidden' value='".$status."' name='kyc_status' id='kyc_status'>         
                    <input type='hidden' value='".$this->encryptData($info['id'])."' name='token' id='token'>
                    <div class='form-group'>
                        <label class='form-label' for='cf-full-name'>Remarks</label>
                        <textarea class='form-control form-control-sm' id='cf-default-textarea' placeholder='Write your message' name='remarks' ></textarea>
                    </div>
                    ";
        $result['layout'] = $layout;*/
        return $result;

    }

    // Update Agenda Files

    function updateapproverejectkyc($data="")
    {
        //$result =  array();
        if(isset($_SESSION['update_kyc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['update_kyc_key']){
                $id             = $this->decryptData($data['token']);  
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];
                $query = "UPDATE ".CUSTOMER_TBL." SET 
                        kyc_status    = '".$this->cleanString($data['kyc_status'])."',
                        approved_on   = '$curr',
                        approved_by   = '$admin_id',
                        kyc_remarks   = '".$this->cleanString($data['remarks'])."',
                        updated_at    = '$curr' WHERE id = '$id' ";
                $exe = $this->selectQuery($query);

                $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$id."' ");
                // TO Customer 
                if ($data['kyc_status']==1) {
                    $sender_mail    = NO_REPLY;
                    $subject        = "Your KYC documents has to been approved";
                    $receiver       = ADMIN_EMAIL;
                    $email_temp     = $this->KYCDocumnetApprovedNotification($cusinfo['name']);            
                    $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                }else{
                    $sender_mail    = NO_REPLY;
                    $subject        = "Your KYC documents has to been rejected";
                    $receiver       = ADMIN_EMAIL;
                    $email_temp     = $this->KYCDocumnetRejectNotification($cusinfo['name'],$data['remarks']);            
                    $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                }
                


                unset($_SESSION['update_kyc_key']);
                return 1; 
            }else{
                return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "0`Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function updateLeadToCustomerModel($id)
    {   
        $result =  array();
        $id = $this->decryptData($id);
        $_SESSION['update_completestatus_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['update_completestatus_key'];
        return $result;
    } 

    function updateCompletedStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(LEAD_TBL,"closed_status"," id ='$data' ");
        if($info['closed_status'] ==1){
            $query = "UPDATE ".LEAD_TBL." SET closed_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_TBL." SET closed_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    function moveLeadToCustomer($data)
    {
        //$lead_id = $this->decryptData($data['lead_id']);
        $info = $this -> getDetails(LEAD_TBL,"*"," id ='".$data['lead_id']."' ");
        $e_check = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$info['email']."' ");
        if($e_check == 0){  
            $m_check = $this -> check_query(CUSTOMER_TBL,"id"," mobile ='".$info['mobile']."' "); 
            if($m_check == 0){  
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $token          = $this->generateRandomString("30");
                $password       = $this->generateRandomStringGenerator("8");
                $psw            = $this->encryptPassword($password);
                $name           = $info['fname'].' '.$info['lname'];
                $project_id     = @$_SESSION['crm_project_id'];

                $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                    token       = '".$this->hyphenize($name)."',
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    project_id  = '".$project_id."',
                    priority    = 'low',
                    lead_id     = '".$info['id']."', 
                    type        = 'moved', 
                    name        = '".$this->cleanString($name)."',
                    gender      = '".$this->cleanString($info['gender'])."',
                    mobile      = '".$this->cleanString($info['mobile'])."',
                    email       = '".$this->cleanString($info['email'])."',                            
                    password    = '".$psw."',
                    has_psw     = '".$password."',
                    address     = '".$this->cleanString($info['address'])."',
                    city        = '".$this->cleanString($info['city'])."',
                    state       = '".$this->cleanString($info['state'])."',
                    pincode     = '".$this->cleanString($info['pincode'])."',
                    kyc_status  = '0',
                    assign_status = '0',
                    kyc_remarks = '',
                    status      = '1',
                    added_by    = '$admin_id',  
                    created_at  = '$curr',
                    updated_at  = '$curr' "; 

                //return  $query ;  
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){       
                    $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);

                    $qu =" UPDATE ".LEAD_TBL." SET moved_status='1', customer_id='".$last_id."' , move_remarks='".$data['moveremarks']."'  WHERE id='".$info['id']."' ";
                    $result_update = $this->selectQuery($qu);
                    

                    $sender_mail    = NO_REPLY;
                    $subject        = ucwords($name)." Account login details";
                    $receiver       = $info['email'];
                    $user_token     = $this->hyphenize($name);
                    $email_temp     = $this->memberLoginInfoTemp($name,$info['email'],$password,$user_token,$last_id,$name);
                    $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                    if($sendemail){
                        return 1;
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }
            }else{
            return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
        }       
        }else{
            return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
        }   
    }

    /*==============================================================
                        Add Invoice
    ================================================================*/

    function addInvoice($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['add_invoice_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_invoice_key']){
                $attachment     = $this->cleanString($files['pdf']);
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");

                $output         = str_replace("uploads/document/", "", $attachment);
                $doc_info       = explode(".", $output); 
                $document_type  = end($doc_info);
                $admin_id       =$_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];

                $inv_date       = $this->changeDateFormat($data['inv_date']);
                $query = "INSERT INTO ".INVOICE_TBL." SET
                            project_id     = '".$project_id."',
                            tenant_id      = '".$_SESSION["tenant_id"]."',
                            inv_number     = '".$this->cleanString($data['inv_number'])."',
                            customer_id    = '".$this->cleanString($data['customer_id'])."',
                            inv_date       = '".$inv_date."',
                            type           = '".$this->cleanString($data['type'])."',
                            raised_to      = '".$this->cleanString($data['raised_to'])."',
                            documents      = '".$attachment."',
                            document_type  = '".$document_type."',
                            remarks        = '".$this->cleanString($data['remarks'])."',
                            status         = '1',
                            deleted_status   = '0',
                            deleted_by       = '0',
                            deleted_on       = '',
                            restored_by      = '0',
                            restored_on      = '',
                            restored_remarks = '',
                            added_by       = '".$admin_id."',
                            created_at     = '$curr',
                            updated_at     = '$curr' ";
                //$exe = $this->selectQuery($query);
                $link   = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe    = mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    $inv_uid = "INV".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".INVOICE_TBL." SET inv_uid ='".$inv_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);
                    unset($_SESSION['add_invoice_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function editInvoice($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['edit_invoice_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_invoice_key']){
                $admin_id       =$_SESSION["crm_admin_id"];
                $attachment     = $this->cleanString($files['pdf']);
                $output         = str_replace("uploads/document/", "", $attachment);
                $doc_info       = explode(".", $output); 
                $document_type  = end($doc_info);

                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);
                $inv_date       = $this->changeDateFormat($data['inv_date']);
                $query = " UPDATE ".INVOICE_TBL." SET
                            inv_number     = '".$this->cleanString($data['inv_number'])."',
                            customer_id    = '".$this->cleanString($data['customer_id'])."',
                            inv_date       = '".$inv_date."',
                            type           = '".$this->cleanString($data['type'])."',
                            raised_to      = '".$this->cleanString($data['raised_to'])."',
                            remarks        = '".$this->cleanString($data['remarks'])."',";

                            if ($data['file_test']==0) {
                            $query .= "
                                documents      = '".$attachment."',
                                document_type  = '".$document_type."',";        
                            }
                     $query .= "                        
                            added_by         = '$admin_id',                            
                            updated_at       = '$curr' WHERE id='".$id."' ";
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['edit_invoice_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Remove Doc

    function removeInvoiceFile($id)
    {
        $q = "UPDATE ".INVOICE_TBL." SET documents ='', document_type ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    /*----------------------------
         Activity Type Master
    ------------------------------*/

    // Add Activity Type Master 

    function addActivityType($data)
    {
        $layout = "";
        if(isset($_SESSION['activity_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['activity_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(ACTIVITY_TYPE,"id"," token ='".$this->masterhyphenize($data['activity_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".ACTIVITY_TYPE." SET
                                project_id      = '".$project_id."',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                token           = '".$this->masterhyphenize($data['activity_type'])."',
                                activity_type   = '".$this->cleanString($data['activity_type'])."',
                                added_by        = '$admin_id',
                                status          = '1',
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['activity_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Activity Type Master is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editActivityType($id='')
    {   
        $result =  array();
        $_SESSION['edit_activitytype_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(ACTIVITY_TYPE,"id,activity_type","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_activitytype_key'];
        $result['activity_type'] = $info['activity_type'];
        return $result;
    }   

    // Update 

    function updateActivityType($data)
    {
        if(isset($_SESSION['edit_activitytype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_activitytype_key']){
                $check = $this->check_query(ACTIVITY_TYPE,"id"," token ='".$this->masterhyphenize($data['activity_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".ACTIVITY_TYPE." SET 
                               token          = '".$this->masterhyphenize($data['activity_type'])."',
                               activity_type  = '".$this->cleanString($data['activity_type'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['activity_type'];
                        unset($_SESSION['edit_activitytype_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Activity Type Master is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function activityTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(ACTIVITY_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".ACTIVITY_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".ACTIVITY_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
            Lead Status Message
    ------------------------------*/

    // Add Lead Status Message

    function addLeadType($data)
    {
        $layout = "";
        if(isset($_SESSION['lead_type_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['lead_type_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(LEAD_TYPE,"id"," token ='".$this->masterhyphenize($data['lead_type'])."'  ");
                if ($check==0) {
                    if (isset($data['default_settings'])) {
                        $default_settings =1 ;
                    }else{
                        $default_settings =0 ;
                    }
                    $query = "INSERT INTO ".LEAD_TYPE." SET
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        token           = '".$this->masterhyphenize($data['lead_type'])."',
                        lead_type       = '".$this->cleanString($data['lead_type'])."',
                        default_settings= '".$default_settings."', 
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last_id = mysqli_insert_id($link);
                    if($exe){       
                        $uid = "LT".str_pad($last_id, 4,0, STR_PAD_LEFT);
                        $q =" UPDATE ".LEAD_TYPE." SET uid='".$uid."' WHERE id='".$last_id."' ";
                        $result = $this->selectQuery($q);

                        unset($_SESSION['lead_type_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Lead Status Message is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editLeadType($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadtype_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TYPE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_leadtype_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='editemail'>Lead Status Message  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='lead_type' name='lead_type' value='".$info['lead_type']."' placeholder='Enter Lead Status Message'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLeadType($data)
    {
        if(isset($_SESSION['edit_leadtype_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadtype_key']){
                $check = $this->check_query(LEAD_TYPE,"id"," token ='".$this->masterhyphenize($data['lead_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".LEAD_TYPE." SET 
                               token           = '".$this->masterhyphenize($data['lead_type'])."',
                               lead_type       = '".$this->cleanString($data['lead_type'])."',
                               added_by        = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_leadtype_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Lead Status Message is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function leadTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(LEAD_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".LEAD_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
            Lead Type
    ------------------------------*/

    // Add Lead Type

    function addLeadSource($data)
    {
        $layout = "";
        if(isset($_SESSION['lead_source_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['lead_source_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(LEAD_SOURCE,"id"," token ='".$this->masterhyphenize($data['lead_source'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".LEAD_SOURCE." SET
                        token           = '".$this->masterhyphenize($data['lead_source'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        lead_source     = '".$this->cleanString($data['lead_source'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['lead_source_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Lead Source Master is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editLeadSource($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadsource_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_SOURCE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_leadsource_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='lead_source'>Lead Source  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='lead_source' name='lead_source' value='".$info['lead_source']."' placeholder='Enter Lead Source'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLeadSource($data)
    {
        if(isset($_SESSION['edit_leadsource_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadsource_key']){
                $check = $this->check_query(LEAD_SOURCE,"id"," token ='".$this->masterhyphenize($data['lead_source'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".LEAD_SOURCE." SET 
                               token          = '".$this->masterhyphenize($data['lead_source'])."',
                               lead_source    = '".$this->cleanString($data['lead_source'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_leadsource_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Lead Source Master is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function leadSourceStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(LEAD_SOURCE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".LEAD_SOURCE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".LEAD_SOURCE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    function addActivityModel($id='')
    {   
        $result =  array();
        $_SESSION['add_activity_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email","id='$id'");
        $result['id'] = $id;
        $result['name'] = ucwords($info['fname'].' '.$info['lname']);
        $result['mobile'] = $info['mobile'];
        $result['email'] = $info['email'];
        $result['fkey'] = $_SESSION['add_activity_key'];
        return $result;
    }   

    /*----------------------------
           Customer Profile
    ------------------------------*/

    // Add Customer Profile

    function addCustomerProfile($data)
    {
        $layout = "";
        if(isset($_SESSION['customer_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['customer_profile_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(CUSTOMER_PROFILE,"id"," customer_profile ='".$data['customer_profile']."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".CUSTOMER_PROFILE." SET
                        token           = '".$this->hyphenize($data['customer_profile'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        customer_profile= '".$this->cleanString($data['customer_profile'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['customer_profile_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Lead Profile is already exist!.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editCustomerProfile($id='')
    {   
        $result =  array();
        $_SESSION['edit_customer_profile_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(CUSTOMER_PROFILE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['edit_customer_profile_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='customer_profile'>Lead Profile Name  <en>*</en></label>
                                        <input type='text' class='form-control form-control-lg' id='customer_profile' name='customer_profile' value='".$info['customer_profile']."' placeholder='Enter Lead Profile Name'>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateCustomerProfile($data)
    {
        if(isset($_SESSION['edit_customer_profile_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_customer_profile_key']){
                $check = $this->check_query(CUSTOMER_PROFILE,"id"," customer_profile ='".$data['customer_profile']."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".CUSTOMER_PROFILE." SET 
                               token          = '".$this->hyphenize($data['customer_profile'])."',
                               customer_profile    = '".$this->cleanString($data['customer_profile'])."',
                               added_by       = '$admin_id',
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        unset($_SESSION['edit_customer_profile_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Lead Profile Name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Avtive & Inactive Status 

    function customerProfileStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this->getDetails(CUSTOMER_PROFILE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_PROFILE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_PROFILE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Members Lead
    ------------------------------*/

    // Add Lead 

    function addLead($data="")
    {
        if(isset($_SESSION['add_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_lead_key']){  
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $token          = $this->generateRandomString("30");
                $today          = date("Y-m-d");
                $project_id     = @$_SESSION['crm_project_id'];

                $echeck = $this->check_query(LEAD_TBL,"id"," email ='".$data['email']."'  ");
                if ($echeck==0) {
                    $m_check = $this -> check_query(LEAD_TBL,"id"," mobile ='".$data['mobile']."' "); 
                    if($m_check == 0){  
                        $leadstatus_info = $this -> getDetails(LEAD_TYPE,"id"," default_settings='1' ");
                        $query = "INSERT INTO ".LEAD_TBL." SET 
                            token           = '".$this->hyphenize($data['fname'].$data['flast'])."',
                            tenant_id       = '".$_SESSION["tenant_id"]."',
                            project_id      = '".$project_id."',
                            property_id     = '0',
                            employee_id     = '0',
                            customer_id     = '0',
                            lead_date       = '".$today."',
                            fname           = '".$this->cleanString($data['fname'])."',
                            lname           = '".$this->cleanString($data['flast'])."',
                            leadsource      = '".$this->cleanString($data['leadsource'])."',
                            flattype_id         = '".$this->cleanString($data['flat_type_id'])."',
                            site_visit_status   = '".$this->cleanString($data['site_visit'])."',
                            customer_profile_id = '".$this->cleanString($data['customerprofile_id'])."',
                            gender          = '".$this->cleanString($data['gender'])."',
                            mobile          = '".$this->cleanString($data['mobile'])."',
                            email           = '".$this->cleanString($data['email'])."',
                            secondary_mobile= '".$this->cleanString($data['smobile'])."',
                            secondary_email = '".$this->cleanString($data['semail'])."',
                            address         = '".$this->cleanString($data['address'])."',
                            city            = '".$this->cleanString($data['city'])."',
                            state           = '".$this->cleanString($data['state'])."',
                            pincode         = '".$this->cleanString($data['pincode'])."',
                            description     = '".$this->cleanString($data['description'])."',
                            lead_status_id  = '".$leadstatus_info['id']."',
                            assign_status   = '0',
                            closed_status   = '0',
                            moved_status    = '0',
                            status          = '1',
                            deleted_status   = '0',
                            deleted_by       = '0',
                            deleted_on       = '',
                            restored_by      = '0',
                            restored_on      = '',
                            restored_remarks = '',
                            added_by        = '$admin_id',  
                            created_at      = '$curr',
                            updated_at      = '$curr' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $uid = "LE".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".LEAD_TBL." SET uid='".$uid."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);
                            
                            $options['ref_id']      = $last_id;
                            $options['created_by']  = $admin_id;
                            $notification           = $this->leadLogs("created",$options);  

                            unset($_SESSION['add_lead_key']);
                            return 1;

                           /* $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                            if($sendemail){
                                
                            }*/
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                     return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
                }     
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Customer 

    function editLead($data="")
    {
        if(isset($_SESSION['edit_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_lead_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $e_check = $this -> check_query(LEAD_TBL,"id"," email ='".$data['email']."' AND id!='".$id."' ");
                if($e_check == 0){  
                    $m_check = $this -> check_query(LEAD_TBL,"id"," mobile ='".$data['mobile']."' AND id!='".$id."'  "); 
                    if($m_check == 0){  
                        $query = " UPDATE ".LEAD_TBL." SET 
                            token           = '".$this->hyphenize($data['fname'].$data['flast'])."',
                            fname           = '".$this->cleanString($data['fname'])."',
                            lname           = '".$this->cleanString($data['flast'])."',
                            leadsource      = '".$this->cleanString($data['leadsource'])."',
                            flattype_id         = '".$this->cleanString($data['flat_type_id'])."',
                            site_visit_status   = '".$this->cleanString($data['site_visit'])."',
                            customer_profile_id = '".$this->cleanString($data['customerprofile_id'])."',
                            gender          = '".$this->cleanString($data['gender'])."',
                            mobile          = '".$this->cleanString($data['mobile'])."',
                            email           = '".$this->cleanString($data['email'])."',
                            secondary_mobile= '".$this->cleanString($data['smobile'])."',
                            secondary_email = '".$this->cleanString($data['semail'])."',
                            address         = '".$this->cleanString($data['address'])."',
                            city            = '".$this->cleanString($data['city'])."',
                            state           = '".$this->cleanString($data['state'])."',
                            pincode         = '".$this->cleanString($data['pincode'])."',
                            description     = '".$this->cleanString($data['description'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_lead_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return $this->errorMsg("Entered Mobile number is already registered. Please Change the Mobile Number or Try Login.");
                    }       
                }else{
                    return $this->errorMsg("Entered Email Address is already registered. Please Change the Email Address or Try Login.");
            }               
                    
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function leadStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(CUSTOMER_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".CUSTOMER_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".CUSTOMER_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Assign Call 

    function assignLeadModel($id='')
    {   
        $result =  array();
        $_SESSION['add_assign_lead_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email","id='$id'");
        $result['id'] = $id;
        $result['name'] = ucwords($info['fname'].' '.$info['lname']);
        $result['mobile'] = $info['mobile'];
        $result['email'] = $info['email'];
        $result['fkey'] = $_SESSION['add_assign_lead_key'];
        return $result;
    }  


    function assignLead($data="")
    {
        if(isset($_SESSION['add_assign_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_assign_lead_key']){          
                    $admin_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $project_id     = @$_SESSION['crm_project_id'];

                    $check = $this->check_query(ASSIGN_LEAD,"id"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                            $info  = $this->getDetails(ASSIGN_LEAD,"id,created_to"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                            $update = "UPDATE ".ASSIGN_LEAD." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                    }   
                    $query = "INSERT INTO ".ASSIGN_LEAD." SET 
                                type            = 'assign',
                                tenant_id       = '".$_SESSION["tenant_id"]."',
                                project_id      = '".$project_id."',
                                created_by      = '$admin_id',
                                created_to      = '".$data['employee_id']."',
                                ref_id          = '".$data['lead_id']."',   
                                priority        = '".$this->cleanString($data['priority'])."',
                                remarks         = '".$this->cleanString($data['remarks'])."',
                                status          = '1',
                                remove_status   = '0',  
                                created_at      = '$curr',
                                updated_at      = '$curr' ";
                    //return $query; 
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last = mysqli_insert_id($link);
                    if($exe){
                        $q =" UPDATE ".LEAD_TBL." SET 
                            assign_status ='1',
                            employee_id = '".$data['employee_id']."',
                            closed_status = '0' WHERE id='".$data['lead_id']."' ";
                        $result = $this->selectQuery($q);
                        if ($result) {
                            $lead_info = $this->getDetails(LEAD_TBL,"*"," id='".$data['lead_id']."' ");
                            $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");

                            $options['ref_id']      = $data['lead_id'];
                            $options['created_by']  = $admin_id;
                            $options['created_to']  = $data['employee_id'];
                            $options['remarks']     = $this->cleanString($data['remarks']);
                            $notification           = $this->leadLogs("assigned",$options); 
                            unset($_SESSION['add_assign_lead_key']);
                            return 1;   
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }                       
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Reassign Call 

    function reassignLeadModel($id='')
    {   
        $result =  array();
        $_SESSION['add_reassign_lead_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"*","id='$id'");
        $assigninfo = $this->getDetails(ASSIGN_LEAD,"*","ref_id='$id'");
        $result['id']           = $id;
        $name = ucwords($info['fname'].' '.$info['lname']);
        $result['layout']       = "

            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['add_reassign_lead_key']."'>
                <input type='hidden' name='lead_id' id='token' value='".$id."'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Reassign Lead to Employee</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <p >
                                            <em class='icon ni ni-user'></em> <span style='margin-right: 10px;'>".$name."</span> 
                                            <em class='icon ni ni-mobile'></em> <span style='margin-right: 10px;'>".$info['mobile']."</span> 
                                            <em class='icon ni ni-mail'></em> <span >".$info['email']."</span>
                                        </p>
                                    </div>
                                </div>

                                
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for=''>Current Owner <en></en></label>
                                        <select class='form-control form-select ' id='' name='' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true' disabled>
                                            ".$this->getEmployeeList($assigninfo['created_to'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='employee_id'>Reassign Employee <en>*</en></label>
                                        <select class='form-control form-select ' id='employee_id' name='employee_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getEmployeeList()."
                                        </select>
                                    </div>
                                </div>   
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='priority'>Lead Priority <en>*</en></label>
                                        <select class='form-control ' name='priority' id='priority'>
                                           <option value='low'>Low</option>
                                           <option value='medium'>Medium</option>
                                           <option value='high'>High</option>
                                           <option value='urgent'>Urgent</option>
                                        </select> 
                                    </div>
                                </div> 
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='remarks' name='remarks' placeholder='Write your Remarks'></textarea>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    } 

    function reassignLead($data="")
    {
        if(isset($_SESSION['add_reassign_lead_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_reassign_lead_key']){            
                    $user_id        = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");  
                    $project_id     = @$_SESSION['crm_project_id'];
                    $check = $this->check_query(ASSIGN_LEAD,"id"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                    if($check==1) {
                        $info  = $this->getDetails(ASSIGN_LEAD,"id,created_to"," ref_id='".$data['lead_id']."' ORDER BY id DESC LIMIT 1 ");
                        $call_status = "UPDATE ".LEAD_TBL." SET 
                                assign_status='1',
                                closed_status = '0',
                                employee_id = '".$data['employee_id']."',
                                updated_at      = '$curr' WHERE id='".$data['lead_id']."' ";
                        $call_status = $this->selectQuery($call_status);
                        if ($call_status) {
                            $update = "UPDATE ".ASSIGN_LEAD." SET 
                                status          = '0',  
                                updated_at      = '$curr' WHERE id='".$info['id']."' ";
                            $update_status = $this->selectQuery($update);
                            if($update_status){
                                $query = "INSERT INTO ".ASSIGN_LEAD." SET 
                                    tenant_id       = '".$_SESSION["tenant_id"]."',
                                    project_id      = '".$project_id."',
                                    type            = 'reassign',
                                    created_by      = '$user_id',
                                    created_to      = '".$data['employee_id']."',
                                    ref_id          = '".$data['lead_id']."',
                                    priority        = '".$this->cleanString($data['priority'])."',
                                    remarks         = '".$this->cleanString($data['remarks'])."',
                                    status          = '1',  
                                    remove_status   = '0',
                                    created_at      = '$curr',
                                    updated_at      = '$curr' ";
                                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                                $exe =mysqli_query($link,$query);
                                $last = mysqli_insert_id($link);
                                if($exe){   
                                    $call_info = $this->getDetails(LEAD_TBL,"*"," id='".$data['lead_id']."' ");
                                    $emp_info  = $this->getDetails(EMPLOYEE,"*"," id='".$data['employee_id']."' ");
                                    $emp_info_app = $this->getDetails(EMPLOYEE,"*"," id='".$info['created_to']."' ");
                                    /*if ($emp_info_app['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info_app['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "Your call has been reassigned ",
                                            'subtitle'  => $call_info['call_id']." has been removed from your list",
                                            'type'      => 'reassign',
                                            'user_id'   => $info['created_to'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }
                                    if ($emp_info['android_device_token']!="") {
                                        $registrationIds = array();
                                        $registrationIds[] = $emp_info['android_device_token'];
                                        $msg_array = array(
                                            'title'     => "New call assigned to you",
                                            'subtitle'  => $call_info['call_id']." assigned for you, click to view",
                                            'type'      => 'assign',
                                            'user_id'   => $data['employee_id'],
                                            'call_id'   => $data['call_id']
                                        );
                                        $this->sendPushNotification($registrationIds,$msg_array);
                                    }*/

                                    $options['ref_id']      = $data['lead_id'];
                                    $options['created_by']  = $user_id;
                                    $options['created_to']  = $data['employee_id'];
                                    $options['remarks']     = $this->cleanString($data['remarks']);
                                    $notification           = $this->leadLogs("reassigned",$options);   
                                    unset($_SESSION['add_reassign_lead_key']);
                                    return 1;                                               
                                }else{
                                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                                }       
                            }else{
                                return "Sorry!! Unexpected Error Occurred. Please try again.";
                            }
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again...";
                    }                           
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }


    /*--------------------------------------------- 
                lead Logs
    ----------------------------------------------*/

    // Add Call Logs

    function leadLogs($type,$options)
    {   
        $curr  = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        switch ($type) {
            case 'created':
                $created_by  = $options['created_by'];              
                $ref_id      = $options['ref_id'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    project_id  = '".$project_id."',
                    type        = 'created',
                    lead_type   = 'manual',
                    created_by  = '$created_by',
                    created_to  = '0',
                    ref_id      = '$ref_id',
                    status      = '1',
                    remarks     = '',  
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'assigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    project_id  = '".$project_id."',
                    type        = 'assigned',
                    lead_type   = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',   
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            case 'reassigned':
                $created_by  = $options['created_by'];  
                $created_to  = $options['created_to'];              
                $ref_id      = $options['ref_id'];
                $remarks     = $options['remarks'];
                $query = "INSERT INTO ".LEAD_LOGS." SET
                    tenant_id   = '".$_SESSION["tenant_id"]."',
                    project_id  = '".$project_id."',
                    type        = 'reassigned',
                    lead_type    = 'manual',
                    created_by  = '$created_by',
                    created_to  = '$created_to',
                    ref_id      = '$ref_id',
                    remarks     = '$remarks',
                    status      = '1',
                    created_at  = '$curr',
                    updated_at  = '$curr' ";
            break;
            default:
            break;
        }
        //return $query;
        $exe = $this->selectQuery($query);
        if ($exe) {
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }
    }

    /*----------------------------
            Acitivity
    -----------------------------*/

    function addActivity($data)
    {
        $layout = "";
        if(isset($_SESSION['add_activity_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_activity_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $act_date = $this->changeDateFormat($data['act_date']);
                $project_id     = @$_SESSION['crm_project_id'];
                $query = "INSERT INTO ".LEAD_ACTIVITY." SET
                    tenant_id       = '".$_SESSION["tenant_id"]."',
                    project_id      = '".$project_id."',
                    lead_id         = '".$data['token']."',                    
                    date            = '".$act_date."',                   
                    activity_id     = '".$data['activity_id']."',
                    remarks         = '".$data['remarks']."',
                    employee_id     = '".$admin_id."',
                    status          = '1',
                    created_at      = '$curr',
                    updated_at      = '$curr' ";
                         //  return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    unset($_SESSION['add_activity_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function addleadActivity($data)
    {
        $layout = "";
        $token          = $this->generateRandomString("30");
        $admin_id       = $_SESSION["crm_admin_id"];
        $curr           = date("Y-m-d H:i:s");
        $act_date = $this->changeDateFormat($data['act_date']);
        $project_id     = @$_SESSION['crm_project_id'];
        $query = "INSERT INTO ".LEAD_ACTIVITY." SET
            tenant_id       = '".$_SESSION["tenant_id"]."',
            project_id      = '".$project_id."',
            lead_id         = '".$data['token']."',            
            date            = '".$act_date."',           
            activity_id     = '".$data['activity_id']."',
            remarks         = '".$data['remarks']."',
            employee_id     = '".$admin_id."',
            status          = '1',
            created_at      = '$curr',
            updated_at      = '$curr' ";
                 //  return $query;
        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
        $exe =mysqli_query($link,$query);
        $last_id = mysqli_insert_id($link);
        if($exe){
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }   
    }

     // Edit 

    function editleadactivity($id='')
    {   
        $result =  array();
        $_SESSION['edit_leadactivity_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_ACTIVITY,"id,date,lead_id,employee_id,activity_id,remarks"," id='".$id."' ");
        $leadinfo = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email"," id='".$info['lead_id']."' ");
        $name = $leadinfo['fname'].' '.$leadinfo['lname'] ;
        $result['id']           = $id;
        $result['layout']       = "
            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['edit_leadactivity_key']."'>
                <input type='hidden' name='token' id='token' value='".$id."'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Add Activity</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <p >
                                            <em class='icon ni ni-user'></em> <span style='margin-right: 10px;'>".$name."</span> 
                                            <em class='icon ni ni-mobile'></em> <span style='margin-right: 10px;'>".$leadinfo['mobile']."</span> 
                                            <em class='icon ni ni-mail'></em> <span >".$leadinfo['email']."</span>
                                        </p>
                                    </div>
                                </div>
                                
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Activity Type Master <en>*</en></label>
                                        <select class='form-control form-select ' id='activity_id' name='activity_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getActivityType($info['activity_id'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-lg-6'>
                                    <div class='form-group'>
                                        <label class='form-label'>Activity Date <en>*</en></label>
                                        <div class='form-control-wrap'>
                                            <input type='text' name='act_date' id='act_date' class='form-control date-picker' data-date-format='dd/mm/yyyy' placeholder='dd/mm/yyyy' value='".date("d/m/Y",strtotime($info['date']))."'>
                                        </div>
                                    </div>
                                </div> 
                                
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='company'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='remarks' name='remarks' placeholder='Write your Remarks'>".$info['remarks']."</textarea>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    }   

    // Update 

    function updateLeadActivity($data)
    {
        if(isset($_SESSION['edit_leadactivity_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_leadactivity_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $act_date       = $this->changeDateFormat($data['act_date']);
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".LEAD_ACTIVITY." SET 
                        date            = '".$act_date."',
                        activity_id     = '".$data['activity_id']."',
                        remarks         = '".$data['remarks']."',
                        updated_at      = '$curr'
                        WHERE id        = '".$id."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['edit_leadactivity_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

     /*----------------------------
         Project type
    ------------------------------*/

    // Add Project type 

    function addProjectType($data)
    {
        $layout = "";
        if(isset($_SESSION['add_project_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_project_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $project_id     = @$_SESSION['crm_project_id'];
                $check = $this->check_query(PROJECT_TYPE,"id"," token ='".$this->masterhyphenize($data['project_type'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".PROJECT_TYPE." SET
                        token           = '".$this->masterhyphenize($data['project_type'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        project_name    = '".$this->cleanString($data['project_type'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_project_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Project name is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editProjectType($id='')
    {   
        $result =  array();
        $_SESSION['edit_project_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROJECT_TYPE,"id,project_name","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_project_key'];
        $result['project_name'] = $info['project_name'];
        return $result;
    }   

    // Update 

    function updateProjectType($data)
    {
        if(isset($_SESSION['edit_project_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_project_key']){
                $check = $this->check_query(PROJECT_TYPE,"id"," token ='".$this->masterhyphenize($data['project_type'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".PROJECT_TYPE." SET 
                               token          = '".$this->masterhyphenize($data['project_type'])."',
                               project_name  = '".$this->cleanString($data['project_type'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['project_type'];
                        unset($_SESSION['edit_project_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Project name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function projectTypeStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROJECT_TYPE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROJECT_TYPE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROJECT_TYPE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Lead Status Model 


    function leadStatusModel($id='')
    {   
        $result =  array();
        $_SESSION['update_leadstatus_key'] = $this->generateRandomString("40");
        $info               = $this->getDetails(LEAD_TBL,"id,fname,lname,mobile,email,lead_status_id","id='$id'");
        $result['id']       = $id;
        $token              = $this->encryptData($id);
        $result['layout']       = "
            <input type='hidden' name='session_token' id='session_token' value='".$this->encryptData($id)."'>
                <input type='hidden' name='fkey' id='fkey' value='".$_SESSION['update_leadstatus_key']."'>
                <input type='hidden' name='session_token' id='session_token' value='".$token."'>
                <input type='hidden' name='token' id='token' value='0'>
                <div class='modal-body modal-body-sm'>
                    <h5 class='title'>Update Lead Status</h5>
                    <h5 class='title name'></h5>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>                                
                                <div class='col-lg-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='leadstatus_id'>Lead Status <en>*</en></label>
                                        <select class='form-control form-select ' id='leadstatus_id' name='leadstatus_id' data-placeholder='Select a Lead Status' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                            ".$this->getLeadStatus($info['lead_status_id'])."
                                        </select>
                                    </div>
                                </div>
                                <div class='col-12'>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right'>
                                        <li>
                                            <a href='#' data-dismiss='modal' class='btn btn-lg btn-danger'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        ";
        return $result;
    } 

    // Update 

    function updateLeadStatus($data)
    {
        if(isset($_SESSION['update_leadstatus_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['update_leadstatus_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $id             = $this->decryptData($data['session_token']);
                $query = "UPDATE ".LEAD_TBL." SET 
                           lead_status_id = '".$this->cleanString($data['leadstatus_id'])."',
                           updated_at     = '$curr'
                           WHERE id       = '".$id."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['update_leadstatus_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                } 
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    /*--------------------------------------------- 
                Import Alumni Data
    ----------------------------------------------*/ 

    // Save Excel Session

    function saveExcelSession($data)
    {
        $data = explode("`",$data);
        $sheet_name     = $this->cleanString($data[0]);
        $token          = $this->cleanString($data[1]);
        $curr = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        $query = "INSERT INTO ".DATASHEET_TBL." SET import_token='$token', tenant_id = '".$_SESSION["tenant_id"]."', project_id  = '".$project_id."', uploaded_sheet='$sheet_name', created_at='$curr', updated_at='$curr', status='0' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return $this->errorMsg ("Error Occured Please try Again");
        }
    }

    // Approve Import Data

    function approveImportData($token) {
        $project_id = @$_SESSION['crm_project_id'];
        $tenant_id  = @$_SESSION["tenant_id"];
        $q = "SELECT * FROM ".TEMPEXCEL_TBL." WHERE upload_token='$token' AND tenant_id='".$tenant_id."' AND project_id='".$project_id."' ";
        $temp_q = $this->selectQuery($q);
        if(mysqli_num_rows($temp_q) > 0){


            while ($data = mysqli_fetch_array($temp_q)) {
                //$user_token     = $this->generateRandomString("35").date("Ymdhis").$this->generateRandomString("30");
                //$mobile_token   = $this->mobileToken("8");
                //$email_token    = $this->generateRandomString("50");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];
                
                if(($data['primary_mobile']!=="")){
                    $check_user = $this -> check_query(LEAD_TBL,"id"," mobile='".$data['primary_mobile']."' ");
                }else{
                    $check_user = 1;
                }
                if($check_user == 0){
                    $leadcount = $this -> check_query(LEAD_TBL,"id"," 1 ");
                    $leadstatus_info = $this -> getDetails(LEAD_TYPE,"id"," default_settings='1' ");
                    $today          = date("Y-m-d");

                    $leadsource = $this->getCheckLeadSourceId($data['leadsource']);
                    $flattype_id = $this->getCheckFlatTypeId($data['flat_type']);
                    $customer_profile_id = $this->getCheckCustomerProfileId($data['profile']);
                    $project_id     = @$_SESSION['crm_project_id'];

                    $q = "INSERT INTO ".LEAD_TBL." SET
                        uid             = '',
                        import_token    = '".$token."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        auth_type       = 'excel',
                        token           = '".$this->hyphenize($data['fname'].$data['lname'])."',
                        property_id     = '0',
                        customer_id     = '0',
                        lead_date       = '".$today."',
                        fname           = '".$this->cleanString($data['fname'])."',
                        lname           = '".$this->cleanString($data['lname'])."',
                        leadsource          = '".$leadsource."',
                        flattype_id         = '".$flattype_id."',
                        site_visit_status   = '".$this->cleanString($data['site_visited'])."',
                        customer_profile_id = '".$customer_profile_id."',
                        gender          = '".$this->cleanString($data['gender'])."',
                        mobile          = '".$this->cleanString($data['primary_mobile'])."',
                        email           = '".$this->cleanString($data['primary_email'])."',
                        secondary_mobile= '".$this->cleanString($data['secondary_mobile'])."',
                        secondary_email = '".$this->cleanString($data['secondary_email'])."',
                        address         = '".$this->cleanString($data['address'])."',
                        city            = '".$this->cleanString($data['city'])."',
                        state           = '".$this->cleanString($data['state'])."',
                        pincode         = '".$this->cleanString($data['pincode'])."',
                        description     = '".$this->cleanString($data['description'])."',
                        lead_status_id  = '".$leadstatus_info['id']."',
                        employee_id     = '0',
                        assign_status   = '0',
                        closed_status   = '0',
                        moved_status    = '0',
                        move_remarks    = '',
                        status          = '1',
                        deleted_status   = '0',
                        deleted_by       = '0',
                        deleted_on       = '',
                        restored_by      = '0',
                        restored_on      = '',
                        restored_remarks = '',
                        added_by        = '$admin_id',  
                        created_at      = '$curr',
                        updated_at      = '$curr' "; 

                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$q);
                    $last_id = mysqli_insert_id($link);
                    if($exe){ 
                        $lead_uid = "LE".str_pad($last_id, 4,0, STR_PAD_LEFT);
                        $qu =" UPDATE ".LEAD_TBL." SET uid='".$lead_uid."' WHERE id='".$last_id."' ";
                        $result = $this->selectQuery($qu);
                    }
                }
            }
        }
        $curre  = date("Y-m-d H:i:s");
        $query = "UPDATE ".DATASHEET_TBL." SET status='1', updated_at='$curre' WHERE import_token='$token' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return "Error Occured Please try Again";
        }
    }

    // Deleted Imported Data

    function deleteImportData($data){
        $delete_temp = $this->deleteRow(TEMPEXCEL_TBL," upload_token='$data' ");
        if($delete_temp){
            $delete = $this->deleteRow(DATASHEET_TBL," import_token='$data' ");
            if ($delete) {
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    function deleteSingleRowImportData($data){
        $delete_temp = $this->deleteRow(TEMPEXCEL_TBL," id='$data' ");
        if($delete_temp){
                return 1;
        }else{
            return 0;
        }
    }

    function getCheckLeadSourceId($leadsource){
        $default = $this->masterhyphenize($leadsource);
        $check = $this -> check_query(LEAD_SOURCE,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $project_id     = @$_SESSION['crm_project_id'];
            $query = "INSERT INTO ".LEAD_SOURCE." SET
                token           = '".$default."',
                tenant_id       = '".$_SESSION["tenant_id"]."',
                project_id      = '".$project_id."',
                lead_source     = '".$leadsource."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(LEAD_SOURCE,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckFlatTypeId($flattype){
        $default = $this->masterhyphenize($flattype);
        $check = $this -> check_query(FLATTYPE_MASTER,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $project_id     = @$_SESSION['crm_project_id'];
            $query = "INSERT INTO ".FLATTYPE_MASTER." SET
                token           = '".$default."',
                tenant_id       = '".$_SESSION["tenant_id"]."',
                project_id      = '".$project_id."',
                flat_variant    = '".$flattype."',
                description     = '',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(FLATTYPE_MASTER,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

    function getCheckCustomerProfileId($customerprofile){
        $default = $this->masterhyphenize($customerprofile);
        $check = $this -> check_query(CUSTOMER_PROFILE,"token"," token ='".$default."' ");
        if($check==0){
            $admin_id       = $_SESSION["crm_admin_id"];
            $curr           = date("Y-m-d H:i:s");
            $project_id     = @$_SESSION['crm_project_id'];
            $query = "INSERT INTO ".CUSTOMER_PROFILE." SET
                token           = '".$default."',
                tenant_id       = '".$_SESSION["tenant_id"]."',
                project_id      = '".$project_id."',
                customer_profile= '".$customerprofile."',
                added_by        = '$admin_id',
                status          = '1',
                created_at      = '$curr',
                updated_at      = '$curr' ";
            //return $query;
            $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
            $exe =mysqli_query($link,$query);
            $get_id = mysqli_insert_id($link);
            return $get_id;
        }else{
            $get_id = $this->getDetails(CUSTOMER_PROFILE,"id"," token ='".$default."' ");
            return $get_id['id'];
        }
    }

     /*--------------------------------------------- 
                Customer Import Alumni Data
    ----------------------------------------------*/ 

    // Save Excel Session

    function saveCustomerExcelSession($data)
    {
        $data = explode("`",$data);
        $sheet_name     = $this->cleanString($data[0]);
        $token          = $this->cleanString($data[1]);
        $curr = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        $query = "INSERT INTO ".CUSTOMERDATASHEET_TBL." SET import_token='$token', tenant_id       = '".$_SESSION["tenant_id"]."',  project_id  = '".$project_id."', uploaded_sheet='$sheet_name', created_at='$curr', updated_at='$curr', status='0' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return $this->errorMsg ("Error Occured Please try Again");
        }
    }

    // Approve Import Data

    function approveCustomerImportData($token) {
        $project_id = @$_SESSION['crm_project_id'];
        $tenant_id  = @$_SESSION["tenant_id"];
        $q = "SELECT * FROM ".TEMPCUSTOMEREXCEL_TBL." WHERE upload_token='$token' AND tenant_id='".$tenant_id."' AND project_id='".$project_id."' ";
        $temp_q = $this->selectQuery($q);
        if(mysqli_num_rows($temp_q) > 0){
            while ($data = mysqli_fetch_array($temp_q)) {
                //$user_token     = $this->generateRandomString("35").date("Ymdhis").$this->generateRandomString("30");
                //$mobile_token   = $this->mobileToken("8");
                //$email_token    = $this->generateRandomString("50");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                
                /*if(($data['mobile']!="") && ($data['email']!="")){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email='".$data['email']."' OR mobile   ='".$data['mobile']."' ");
                }elseif($data['mobile'] ==""){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email='".$data['email']."' ");
                }elseif($data['email'] ==""){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," mobile='".$data['mobile']."' ");
                }else{
                    $check_user = 1;
                }*/

               
                if($data['email']!=''){
                    $check_user = $this -> check_query(CUSTOMER_TBL,"id"," email ='".$data['email']."' ");
                    if($check_user == 0){
                        $leadcount  = $this->check_query(CUSTOMER_TBL,"id"," 1 ");
                        $uid        = "LE".str_pad($leadcount, 4,0, STR_PAD_LEFT);
                        $today      = date("Y-m-d");

                        $token          = $this->generateRandomString("30");
                        $password       = $this->generateRandomStringGenerator("8");
                        $psw            = $this->encryptPassword($password);
                        $query = "INSERT INTO ".CUSTOMER_TBL." SET 
                                import_token = '".$this->cleanString($data['upload_token'])."',
                                tenant_id    = '".$_SESSION["tenant_id"]."',
                                token       = '".$this->hyphenize($data['name'])."',
                                project_id  = '".$project_id."',
                                priority    = 'low',
                                name        = '".$this->cleanString($data['name'])."',
                                gender      = '',
                                mobile      = '".$this->cleanString($data['mobile'])."',
                                email       = '".$this->cleanString($data['email'])."',                            
                                password    = '".$psw."',
                                has_psw     = '".$password."',
                                address     = '".$this->cleanString($data['addresss'])."',
                                city        = '',
                                state       = '',
                                pincode     = '',
                                kyc_status  = '0',
                                assign_status = '0',
                                kyc_remarks = '',
                                status      = '1',
                                added_by    = '$admin_id',  
                                created_at  = '$curr',
                                updated_at  = '$curr' "; 
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            $customer_id = "CU".str_pad($last_id, 4,0, STR_PAD_LEFT);
                            $q =" UPDATE ".CUSTOMER_TBL." SET uid='".$customer_id."' WHERE id='".$last_id."' ";
                            $result = $this->selectQuery($q);


                            $sender_mail    = NO_REPLY;
                            $subject        = ucwords($data['name'])." Account login details";
                            $receiver       = $data['email'];
                            $user_token     = $this->hyphenize($data['name']);
                            $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                            $sendemail      = $this->send_mail_import($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                        }
                    }
                }
            }
        }
        $curre  = date("Y-m-d H:i:s");
        $query = "UPDATE ".CUSTOMERDATASHEET_TBL." SET status='1', updated_at='$curre' WHERE import_token='$token' ";
        $execute = $this->selectQuery($query);
        if($execute){
            return 1;
        }else{
            return "Error Occured Please try Again";
        }
    }

    // Deleted Imported Data

    function deleteCustomerImportDatass($data){
        $delete_temp = $this->deleteRow(TEMPCUSTOMEREXCEL_TBL," upload_token='$data' ");
        if($delete_temp){
            $delete = $this->deleteRow(CUSTOMERDATASHEET_TBL," import_token='$data' ");
            if ($delete) {
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }

    function deleteCustomerSingleRowImportData($data){
        $delete_temp = $this->deleteRow(TEMPCUSTOMEREXCEL_TBL," id='$data' ");
        if($delete_temp){
                return 1;
        }else{
            return 0;
        }
    }

        
    /*-----------------------------------
                Delete
    ------------------------------------*/

    // Delete Status 

    function deletGeneralDocList($data)
    {    
        $info           = $this->getDetails(GENDRAL_DOCUMENTS,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".GENDRAL_DOCUMENTS." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".GENDRAL_DOCUMENTS." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     // Revert Gen Doc List 

    function undoGeneralDocList($id='')
    {   
        $result =  array();
        $_SESSION['undo_gendoc_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(GENDRAL_DOCUMENTS,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_gendoc_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateGeneralDocList($data)
    {
        if(isset($_SESSION['undo_gendoc_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_gendoc_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = "UPDATE ".GENDRAL_DOCUMENTS." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                   
                    unset($_SESSION['undo_gendoc_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Publish

    function publishCheckListDoc($data)
    {    
        $info           = $this->getDetails(FLATVILLA_DOC_TYPE,"publish_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['publish_status'] == 0){
            $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET
                 publish_status ='1',
                 updated_at     ='".$curr."'
                 WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Delete Status

    function deletCheckListDoc($data)
    {    
        $info           = $this->getDetails(FLATVILLA_DOC_TYPE,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Doc Check List 

    function undoDocCheckList($id='')
    {   
        $result =  array();
        $_SESSION['undo_docchecklist_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(FLATVILLA_DOC_TYPE,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_docchecklist_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div>


                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateDocCheckList($data)
    {
        if(isset($_SESSION['undo_docchecklist_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_docchecklist_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = "UPDATE ".FLATVILLA_DOC_TYPE." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                   
                    unset($_SESSION['undo_docchecklist_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Delete Status

    function deletLead($data)
    {    
        $info           = $this->getDetails(LEAD_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".LEAD_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".LEAD_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Lead 

    function undoLead($id='')
    {   
        $result =  array();
        $_SESSION['undo_leads_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(LEAD_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_leads_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateLead($data)
    {
        if(isset($_SESSION['undo_leads_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_leads_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".LEAD_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_leads_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Delete Status

    function deleteInvoice($data)
    {    
        $info           = $this->getDetails(INVOICE_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".INVOICE_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".INVOICE_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Invoice 

    function undoInvoice($id='')
    {   
        $result =  array();
        $_SESSION['undo_invoice_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(INVOICE_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_invoice_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateInvoice($data)
    {
        if(isset($_SESSION['undo_invoice_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_invoice_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".INVOICE_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_invoice_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Delete Status

    function deleteGallery($data)
    {    
        $info           = $this->getDetails(GALLERY_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".GALLERY_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".GALLERY_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Invoice 

    function undoGallery($id='')
    {   
        $result =  array();
        $_SESSION['undo_gallery_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(GALLERY_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_gallery_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updateGallery($data)
    {
        if(isset($_SESSION['undo_gallery_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_gallery_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".GALLERY_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_gallery_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

      // Delete Status

    function deletePropertyBrochure($data)
    {    
        $info           = $this->getDetails(PROPERTY_BROUCHERS,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".PROPERTY_BROUCHERS." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".PROPERTY_BROUCHERS." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Invoice 

    function undoPropertyBrochure($id='')
    {   
        $result =  array();
        $_SESSION['undo_propertybrochures_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_BROUCHERS,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_propertybrochures_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;

    } 

    // Update 

    function updatePropertyBrochure($data)
    {
        if(isset($_SESSION['undo_propertybrochures_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_propertybrochures_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".PROPERTY_BROUCHERS." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_propertybrochures_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }


    // Delete Status

    function deleteNews($data)
    {    
        $info           = $this->getDetails(NEWS_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".NEWS_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".NEWS_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert News 

    function undoNews($id='')
    {   
        $result =  array();
        $_SESSION['undo_news_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(NEWS_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_news_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;
    } 

    // Update 

    function updateNews($data)
    {
        if(isset($_SESSION['undo_news_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_news_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".NEWS_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_news_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    /*---------------------------
        Delete Property
    ---------------------------*/

    // Delete Status

    function deletProperty($data)
    {    
        $info           = $this->getDetails(PROPERTY_TBL,"deleted_status"," id ='$data' ");
        $admin_id       = $_SESSION["crm_admin_id"] ;
        $curr           = date("Y-m-d H:i:s");
        if($info['deleted_status'] == 0){
            $query = "UPDATE ".PROPERTY_TBL." SET
                 deleted_status ='1',
                 deleted_by     ='".$admin_id."',
                 deleted_on     ='".$curr."',
                 restored_by    ='0',
                 restored_on    ='',
                 restored_remarks = ''  WHERE id='$data' ";
        }else{
             $query = "UPDATE ".PROPERTY_TBL." SET
                 deleted_status  ='0',
                 deleted_by      ='0',
                 deleted_on      ='',
                 restored_by     ='".$admin_id."',
                 restored_on     ='".$curr."',
                 restored_remarks = '' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Revert Property 

    function undoProperty($id='')
    {   
        $result =  array();
        $_SESSION['undo_property_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROPERTY_TBL,"*"," id ='$id' ");
        $layout = "             
                <input type='hidden' value='".$_SESSION['undo_property_key']."' name='fkey' id='email_fkey'>
                    <input type='hidden' value='".$info['id']."' name='option' id='option'>         
                    <input type='hidden' value='".$info['id']."' name='token' id='token'>
                    <div class='tab-content'>
                        <div class='tab-pane active' id='personal'>
                            <div class='row gy-4'>
                                <div class='col-md-12'>
                                    <div class='form-group'>
                                        <label class='form-label' for='restored_remarks'>Remarks  <en>*</en></label>
                                        <textarea class='form-control' name='restored_remarks' id='restored_remarks'></textarea>
                                    </div>
                                </div>
                               
                                <div class='col-12 '>
                                    <ul class='align-center flex-wrap flex-sm-nowrap gx-2 float_right'>
                                        <li>
                                            <a href='javascript:void();' class='btn btn-lg btn-danger ' data-dismiss='modal'>Cancel</a>
                                        </li>
                                        <li>
                                            <button type='submit' class='btn btn-lg btn-primary'>Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        $result['layout'] = $layout;
        return $result;
    } 

    // Update 

    function updateProperty($data)
    {
        if(isset($_SESSION['undo_property_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['undo_property_key']){
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $today          = date("d/m/Y");
                $query = " UPDATE ".PROPERTY_TBL." SET 
                    deleted_status  = '0',
                    deleted_by      = '0',
                    deleted_on      = '',
                    restored_by     = '".$admin_id."',
                    restored_on     = '".$curr."',
                    restored_remarks= '".$data['restored_remarks']."',
                    updated_at      = '".$curr."'
                    WHERE id        = '".$data['token']."' ";
               // return $query;
               $exe = $this->selectQuery($query);
                if($exe){
                    $result =  array();
                    $result['status']   = "1";
                    unset($_SESSION['undo_property_key']);
                    return $result;
                }else{
                    $result =  array();
                    $result['status']   = "1";
                    $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                    return $result; 
                }
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }



    /*--------------------------------------------- 
                News
    ----------------------------------------------*/ 

    // Add 

    function addnews($data)
    {
        $layout = "";
        if(isset($_SESSION['add_news_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_news_key']){
                //$token            = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $curr           = date("Y-m-d H:i:s");
                $date           = $this->changeDateFormat($data['date']);
                if(isset($data['email_notification'])){
                    $email_notification = 1;
                }else{
                    $email_notification = 0;
                }
                $customer_ids = '';
                if (isset($data['assistants_selected'])) {
                    $assistants_selected = $data['assistants_selected'];
                    $count_related = count($assistants_selected);
                    if ($count_related>0) {
                        for ($i=0; $i < $count_related; $i++) {
                            $comma = (($i>0) ? "," : "");
                            $customer_ids .= $comma.$assistants_selected[$i];
                        }
                    }
                }else{
                    $customer_ids = '';
                }
                if(isset($data['show_popup'])){
                    $show_popup = 1;
                    $till_date      = $this->changeDateFormat($data['till_date']);

                    $q = "UPDATE ".NEWS_TBL. " SET show_popup='0' WHERE show_popup='1' ";
                    $update1 = $this->selectQuery($q); 

                }else{
                    $show_popup = 0;
                    $till_date      = '';
                }
                   

                $query = "INSERT INTO ".NEWS_TBL." SET
                    title               = '".$this->cleanString($data['title'])."',
                    tenant_id           = '".$_SESSION["tenant_id"]."',
                    project_id          = '".$project_id."',
                    date                = '".$date."',
                    description         = '".$this->cleanString($data['description'])."',
                    email_notification  = '".$email_notification."',
                    post_to             = '".$this->cleanString($data['post_to'])."',
                    customer_ids        = '".$customer_ids."',
                    image               = '".$this->cleanString($data['image'])."',
                    show_popup          = '".$show_popup."',
                    popup_till          = '".$till_date."',  
                    added_by            = '$admin_id',
                    status              = '1',
                    send_mail_status    = '0',
                    deleted_status      = '0',
                    deleted_by          = '0',
                    deleted_on          = '',
                    restored_by         = '0',
                    restored_on         = '',
                    restored_remarks    = '',
                    created_at          = '$curr',
                    updated_at          = '$curr' ";
                //return $query;
                //$exe = $this->selectQuery($query);
                $link    = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe     = mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    $token = $this->createNewsToken($data['title'],$last_id);
                    $update_q = "UPDATE ".NEWS_TBL. " SET token='$token' WHERE id='$last_id' ";
                    $update = $this->selectQuery($update_q);    
                    if ($update) {

                        /*
                        $sender_mail    = NO_REPLY;
                        $subject        = ucwords($data['name'])." Account login details";
                        $receiver       = $data['email'];
                        $user_token     = $this->hyphenize($data['name']);
                        $email_temp     = $this->memberLoginInfoTemp($data['name'],$data['email'],$password,$user_token,$last_id,$data['name']);
                        $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");
                        */

                        unset($_SESSION['add_news_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function createNewsToken($name,$id)
    {
        $default = $this->hyphenize($name);
        $check = $this -> check_query(NEWS_TBL,"token"," token ='".$default."' ");
        if($check==0){
            return $default;
        }else{
            return $default.".".$id;
        }
    }

    // Avtive & Inactive Status 

    function newsStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(NEWS_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".NEWS_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".NEWS_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    function removeNewsImgIcon($data)
    {
        $query = "UPDATE ".NEWS_TBL." SET image='' WHERE id='$data' ";
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }   
    }

    function editnews($data)
    {
        $layout = "";
        if(isset($_SESSION['edit_news_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_news_key']){
                //$token            = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $id             = $this->decryptData($data['session_token']);

                $date           = $this->changeDateFormat($data['date']);
                if(isset($data['email_notification'])){
                    $email_notification = 1;
                }else{
                    $email_notification = 0;
                }

                if ($data['post_to']=='all') {
                    $customer_ids = '';
                }else{
                    $customer_ids = '';
                    if (isset($data['assistants_selected'])) {
                        $assistants_selected = $data['assistants_selected'];
                        $count_related = count($assistants_selected);
                        if ($count_related>0) {
                            for ($i=0; $i < $count_related; $i++) {
                                $comma = (($i>0) ? "," : "");
                                $customer_ids .= $comma.$assistants_selected[$i];
                            }
                        }
                    }else{
                        $customer_ids = '';
                    }
                }

                
                if(isset($data['show_popup'])){
                    $show_popup = 1;
                    $till_date      = $this->changeDateFormat($data['till_date']);

                    $q = "UPDATE ".NEWS_TBL. " SET show_popup='0' WHERE show_popup='1' ";
                    $update1 = $this->selectQuery($q); 

                }else{
                    $show_popup = 0;
                    $till_date      = '';
                }
                   


                $query = "UPDATE ".NEWS_TBL." SET
                    title               = '".$this->cleanString($data['title'])."',
                    date                = '".$date."',
                    description         = '".$this->cleanString($data['description'])."',
                    email_notification  = '".$email_notification."',
                    post_to             = '".$this->cleanString($data['post_to'])."',
                    customer_ids        = '".$customer_ids."',
                    image               = '".$this->cleanString($data['image'])."',
                    show_popup          = '".$show_popup."',
                    popup_till          = '".$till_date."',
                    added_by            = '$admin_id',
                    updated_at          = '$curr' WHERE id='".$id."' ";
                    //return $query;
                //$exe = $this->selectQuery($query);
                $link    = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe     = mysqli_query($link,$query);
                if($exe){
                    $token = $this->createNewsToken($data['title'],$id);
                    $update_q = "UPDATE ".NEWS_TBL. " SET token='$token' WHERE id='$id' ";
                    $update = $this->selectQuery($update_q);    
                    if ($update) {
                        unset($_SESSION['edit_news_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    function sendcustomer_mail($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(NEWS_TBL,"send_mail_status"," id ='$data' ");
        if($info['send_mail_status'] ==1){
            $query = "UPDATE ".NEWS_TBL." SET send_mail_status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".NEWS_TBL." SET send_mail_status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }
    
     /*----------------------------
            Adhoc requests
    ------------------------------*/

    // Add 

    function addticket($data,$images)
    {
        $layout = "";
        if(isset($_SESSION['add_request_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_request_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $curr           = date("Y-m-d H:i:s");

                
               $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$data['customer_id']."' ");
               $admininfo = $this -> getDetails(EMPLOYEE,"*"," id ='".$admin_id."' ");
               $customer_id = $cusinfo['id'];
               $property_id = $cusinfo['property_id']=='' ? '0' : $cusinfo['property_id'] ;

                $query = "INSERT INTO ".ADOC_REQUEST." SET
                        project_id      = '".$project_id."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        property_id     = '".$this->cleanString($property_id)."',
                        customer_id     = '".$this->cleanString($customer_id)."',
                        employee_id     = '0',
                        ref_id          = '0',
                        request_type    = '".$data['raised_by']."',
                        type            = 'common',
                        requesttype_id  = '".$this->cleanString($data['request_id'])."',
                        subject         = '".$this->cleanString($data['subject'])."',
                        remarks         = '".$this->cleanString($data['remarks'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        assign_status   = '0',
                        closed_status   = '0',
                        assign_employee_id = '0',
                        request_status  = '0',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                //return $query;
                $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                $exe =mysqli_query($link,$query);
                $last_id = mysqli_insert_id($link);
                if($exe){
                    $ticket_uid = "T".str_pad($last_id, 4,0, STR_PAD_LEFT);
                    $q =" UPDATE ".ADOC_REQUEST." SET ticket_uid='".$ticket_uid."' WHERE id='".$last_id."' ";
                    $result = $this->selectQuery($q);
                    
                    if($result){
                        $cusinfo = $this -> getDetails(CUSTOMER_TBL,"*"," id ='".$customer_id."' ");
                        // To Admin
                            
                            $csender_mail    = NO_REPLY; 
                            $csubject        = COMPANY_NAME." - Adhoc request ".$ticket_uid." has been raised successfully ";
                            $creceiver       = ADMIN_EMAIL;
                            $cemail_temp     = $this->submitKYCSubmitNotification($ticket_uid,$customer_id,$property_id,$data);    
                            $csendemail      = $this->send_mail($csender="",$csender_mail,$creceiver,$csubject,$cemail_temp,$cbcc="");
                        if ($csendemail) {
                            // To Customer
                            $sender_mail    = NO_REPLY;
                            $subject        = COMPANY_NAME." - New Adhoc request ".$ticket_uid." has been raised by ".ucwords($admininfo['name']);
                            $receiver       = $cusinfo['email'];
                            $email_temp     = $this->newKYCSubmitNotification($ticket_uid,$customer_id,$property_id,$data);            
                            $sendemail      = $this->send_mail($sender="",$sender_mail,$receiver,$subject,$email_temp,$bcc="");

                            $options['ref_id']      = $last_id;
                            $options['created_by']  = $customer_id;
                            $options['customer_id'] = $customer_id;
                            $options['property_id'] = $property_id;
                            $notification           = $this->requestLogs("created",$options,$images,$data); 


                            $options['ref_id']      = $last_id;
                            $options['created_by']  = $admin_id;
                            $notification           = $this->adhocLogs("created",$options);  

                            unset($_SESSION['add_request_key']);
                            return 1;
                        }
                    }
                    
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    /*--------------------------------------------- 
                Request Logs
    ----------------------------------------------*/

    // Add Request Logs

    function requestLogs($type,$options,$images,$data)
    {   
        $curr  = date("Y-m-d H:i:s");
        $created_by  = $options['created_by'];              
        $ref_id      = $options['ref_id'];
        $property_id = $options['property_id'];
        $customer_id = $options['customer_id'];
        $project_id  = @$_SESSION['crm_project_id'];

        $query = "INSERT INTO ".REQUEST_LOGS." SET
            tenant_id       = '".$_SESSION["tenant_id"]."',
            project_id  = '".$project_id."',
            type        = 'created',
            request_type= '".$data['raised_by']."',
            created_by  = '$created_by',
            created_to  = '0',
            ref_id      = '$ref_id',
            remarks     = '".$this->cleanString($data['remarks'])."',
            status      = '1',
            created_at  = '$curr',
            updated_at  = '$curr' ";
        //$exe = $this->selectQuery($query);
        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
        $exe =mysqli_query($link,$query);
        $last_id = mysqli_insert_id($link);

        if ($exe) {
            if(count($images)>0){
                $content_array  = $data['adhoc_row'];
                $description    = array();
                foreach ($content_array as $key => $value) {
                    $description[]          = $this->cleanString($value['description']);        
                }
                $i = 0;
                foreach ($images as $value) {   
                    $output              = str_replace("uploads/srcimg/", "", $value);
                    $image_info          = explode(".", $output); 
                    $image_type          = end($image_info);
                    $insert_description  = $description[$i];    

                    // Upload to S3
                    $targetPath = "../resource/uploads/srcimg/".$output;
                    S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$output, S3::ACL_PUBLIC_READ);
                    unlink($targetPath);

                    $qu = "INSERT INTO ".ADOC_IMAGES." SET
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        adoc_id         = '$ref_id',
                        ref_id          = '$last_id',
                        property_id     = '".$this->cleanString($property_id)."',
                        customer_id     = '".$this->cleanString($customer_id)."',
                        image_name      = '$output',
                        file_type       = '$image_type',
                        description     = '".$insert_description."', 
                        status          = '1', 
                        created_at      = '$curr', 
                        updated_at      = '$curr'  ";
                    $exe2 = $this->selectQuery($qu);
                    $i++;
                }
                return 1;
            }else{
                return 1;
            }
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        }
    }

        /*=====================================
            General Documents
    =======================================*/

    function viewGenDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        //$property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(GENDRAL_DOCUMENTS,"*","id='$id' ");
        //$projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='$property_id' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $layout = " 
                <div class='modal-content'>
                    <div class='modal-header align-center'>
                        <div class='nk-file-title'>
                            <div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
                                        <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
                                        <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
                                        <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
                                        <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
                                    </svg>
                                </span>
                            </div>
                            <div class='nk-file-name'>
                                <div class='nk-file-name-text'><span class='title'> </span></div>
                                <div class='nk-file-name-sub'>Project</div>
                            </div>
                        </div>
                        <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
                    </div>
                    <div class='modal-body'>
                        <div class='nk-file-details'>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Title</div>
                                <div class='nk-file-details-col'>".$info['title']."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Type</div>
                                <div class='nk-file-details-col'>".$info['document_type']."</div>
                            </div>
                            
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Owner</div>
                                <div class='nk-file-details-col'>".ucwords($ownerinfo['name'])."</div>
                            </div>
                            
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Description</div>
                                <div class='nk-file-details-col'>".($info['description'])."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Created</div>
                                <div class='nk-file-details-col'>".date("M d, Y",strtotime($info['created_at']))."</div>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class='modal-footer modal-footer-stretch bg-light'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li><a href='".DOCUMENTS."".$info['documents']."' target='_blank' class='btn btn-primary file-dl-toast'>Download</a></li>
                                    <li><a href='".BASEPATH."documents/generalrequest/".$info['id']."' class='title btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            ";
            /* <li><a href='javascript:void();' data-option='".$info['id']."' class='title raiseAdhocRequest btn btn-warning file-dl-toast'> Raise Adhoc Request</a></li>*/
                
        $result['layout'] = $layout;
        return $result;
    }

    /*=====================================
            Private Documents
    =======================================*/

    function viewPrivateDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        //$property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(PROPERTY_DOCUMENTS_ITEMS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='".$info['property_id']."' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");


        $doc_file  = (($info['document_file']!='') ? "<span class='tb-status text-success'>Submitted</span>" : "<span class='tb-status text-danger'>Pending</span>"  );


        $doc_status = (($info['doc_status']!=0) ? "<div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li><a href='javascript:void();' class='btn btn-danger' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em> Close</a></li>
                                    <li><a href='javascript:void()' class='btn btn-blue uploadDocfile' data-value='".$info['id']."' data-property='".$info['property_id']."'  data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-plus'></em> Update Document</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>" : "<div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li><a href='javascript:void();' class='btn btn-danger' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em> Close</a></li>
                                    <li><a href='javascript:void()' class='btn btn-green uploadDocfile' data-value='".$info['id']."' data-property='".$info['property_id']."'  data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-plus'></em>Add Document</a></li>
                                    
                                    
                                </ul>
                            </div>
                        </div>
                    </div>");
        $layout = " 
                <div class='modal-content ContentDocInfo' style='width: 550px;'>
                    <div class='modal-header align-center'>
                        <div class='nk-file-title'>
                            <div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
                                        <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
                                        <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
                                        <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
                                        <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
                                    </svg>
                                </span>
                            </div>
                            <div class='nk-file-name'>
                                <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
                                <div class='nk-file-name-sub'>Private Documents</div>
                            </div>
                        </div>
                        <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
                    </div>
                    <div class='modal-body'>
                        <div class='nk-file-details'>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Title</div>
                                <div class='nk-file-details-col'>".$info['doc_name']."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Description</div>
                                <div class='nk-file-details-col'>".($info['description'])."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Status</div>
                                <div class='nk-file-details-col'>".$doc_file."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Type</div>
                                <div class='nk-file-details-col'>".($info['doc_type']!='' ? $info['doc_type'] :'-')."</div>
                            </div>
                        </div>
                    </div>
                    ".$doc_status."
                </div>
                 ";
        $result['layout'] = $layout;
        return $result;

    }

    /*=====================================
            Private Common Documents
    =======================================*/

    function viewPrivateCommonDocInfo($data='')
    {   
        $result =  array();
        $data   = explode("`",$data);
        $id     = $data[0];
        //$property_id = $_SESSION["crm_property_id"];
        $info = $this->getDetails(PROPERTY_GENDRAL_DOCUMENTS,"*","id='$id' ");
        $projectinfo = $this->getDetails(PROPERTY_TBL,"*","id='".$info['property_id']."' ");
        $ownerinfo = $this->getDetails(EMPLOYEE,"*","id='".$info['added_by']."' ");
        $doc_file  = (($info['documents']!='') ? "<span class='tb-status text-success'>Submitted</span>" : "<span class='tb-status text-danger'>Pending</span>"  ); 
        $doc_status = (($info['documents']=='') ? "<div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li><a href='javascript:void();' class='btn btn-danger' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em> Close</a></li>
                                    <li><a href='javascript:void()' class='btn btn-green uploadGenDocfile' data-value='".$info['id']."' data-property='".$info['property_id']."'  data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-plus'></em>Add Document</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>" : "<div class='modal-footer modal-footer-stretch bg-light float_right'>
                        <div class='modal-footer-between'>
                            <div class='g'>
                                <ul class='btn-toolbar g-3'>
                                    <li><a href='javascript:void();' class='btn btn-danger' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em> Close</a></li>
                                    <li><a href='javascript:void()' class='btn btn-blue uploadGenDocfile' data-value='".$info['id']."' data-property='".$info['property_id']."'  data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-plus'></em> Update Document</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>");

        $layout = " 
                <div class='modal-content ContentDocInfo' style='width: 550px;'>
                    <div class='modal-header align-center'>
                        <div class='nk-file-title'>
                            <div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 72 72'>
                                        <path fill='#6C87FE' d='M57.5,31h-23c-1.4,0-2.5-1.1-2.5-2.5v-10c0-1.4,1.1-2.5,2.5-2.5h23c1.4,0,2.5,1.1,2.5,2.5v10   C60,29.9,58.9,31,57.5,31z' />
                                        <path fill='#8AA3FF' d='M59.8,61H12.2C8.8,61,6,58,6,54.4V17.6C6,14,8.8,11,12.2,11h18.5c1.7,0,3.3,1,4.1,2.6L38,24h21.8   c3.4,0,6.2,2.4,6.2,6v24.4C66,58,63.2,61,59.8,61z' />
                                        <path display='none' fill='#8AA3FF' d='M62.1,61H9.9C7.8,61,6,59.2,6,57c0,0,0-31.5,0-42c0-2.2,1.8-4,3.9-4h19.3   c1.6,0,3.2,0.2,3.9,2.3l2.7,6.8c0.5,1.1,1.6,1.9,2.8,1.9h23.5c2.2,0,3.9,1.8,3.9,4v31C66,59.2,64.2,61,62.1,61z' />
                                        <path fill='#798BFF' d='M7.7,59c2.2,2.4,4.7,2,6.3,2h45c1.1,0,3.2,0.1,5.3-2H7.7z' />
                                    </svg>
                                </span>
                            </div>
                            <div class='nk-file-name'>
                                <div class='nk-file-name-text'><span class='title'>".$projectinfo['title']."</span></div>
                                <div class='nk-file-name-sub'>General Documents</div>
                            </div>
                        </div>
                        <a href='javascript:void();' class='close' data-dismiss='modal'><em class='icon ni ni-cross-sm'></em></a>
                    </div>
                    <div class='modal-body'>
                        <div class='nk-file-details'>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Title</div>
                                <div class='nk-file-details-col'>".$info['title']."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Description</div>
                                <div class='nk-file-details-col'>".($info['description'])."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Status</div>
                                <div class='nk-file-details-col'>".$doc_file."</div>
                            </div>
                            <div class='nk-file-details-row'>
                                <div class='nk-file-details-col'>Type</div>
                                <div class='nk-file-details-col'>".($info['document_type']!='' ? $info['document_type'] :'-')."</div>
                            </div>
                            
                        </div>
                    </div>
                    ".$doc_status."
                </div>
                 ";
        $result['layout'] = $layout;
        return $result;

    }

    /*----------------------------
         Property Area
    ------------------------------*/

    // Add Property Area 

    function addPropertyArea($data="")
    {
        if(isset($_SESSION['add_property_area_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_property_area_key']){  
                $e_check = $this -> check_query(PROPERTY_AREA,"id"," token ='".$this->masterhyphenize($data['title'])."' ");
                if($e_check == 0){  
                    $admin_id       =$_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $project_id     = @$_SESSION['crm_project_id'];
                    $query = "INSERT INTO ".PROPERTY_AREA." SET 
                        token       = '".$this->masterhyphenize($data['title'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id  = '".$project_id."',
                        title       = '".$this->cleanString($data['title'])."',
                        carpet_area = '".$this->cleanString($data['carpet_area'])."',
                        total_area  = '".$this->cleanString($data['total_area'])."',
                        usable_area = '".$this->cleanString($data['usable_area'])."',  
                        common_area = '".$this->cleanString($data['common_area'])."',
                        uds         = '".$this->cleanString($data['uds'])."',
                        uds_perc    = '".$this->cleanString($data['uds_perc'])."',
                        status      = '1',
                        added_by    = '$admin_id',  
                        created_at  = '$curr',
                        updated_at  = '$curr' ";    
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last_id = mysqli_insert_id($link);
                    if($exe){   
                        unset($_SESSION['add_property_area_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return $this->errorMsg("Entered property area title is already exist!");
                }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Property Area  

    function editPropertyArea($data="")
    {
        if(isset($_SESSION['edit_property_area_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_property_area_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $e_check = $this -> check_query(PROPERTY_AREA,"id"," token ='".$this->masterhyphenize($data['title'])."' AND id!='".$id."' ");
                if($e_check == 0){  
                        $query = " UPDATE ".PROPERTY_AREA." SET 
                            title       = '".$this->cleanString($data['title'])."',
                            carpet_area = '".$this->cleanString($data['carpet_area'])."',
                            total_area  = '".$this->cleanString($data['total_area'])."',
                            usable_area = '".$this->cleanString($data['usable_area'])."',  
                            common_area = '".$this->cleanString($data['common_area'])."',
                            uds         = '".$this->cleanString($data['uds'])."',
                            uds_perc    = '".$this->cleanString($data['uds_perc'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_property_area_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                         
                }else{
                    return $this->errorMsg("Entered property area title is already exist!");
                }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function propertyAreaStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROPERTY_AREA,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROPERTY_AREA." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROPERTY_AREA." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
            Department
    ------------------------------*/

    // Add Department

    function addDepartment($data="")
    {
        if(isset($_SESSION['add_department_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_department_key']){  
                $e_check = $this -> check_query(DEPARTMENT_TBL,"id"," token ='".$this->masterhyphenize($data['name'])."' ");
                if($e_check == 0){  
                    $admin_id       =$_SESSION["crm_admin_id"];
                    $project_id     = @$_SESSION['crm_project_id'];
                    $curr           = date("Y-m-d H:i:s");
                    $query = "INSERT INTO ".DEPARTMENT_TBL." SET 
                        token       = '".$this->masterhyphenize($data['name'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id  = '".$project_id."',
                        name        = '".$this->cleanString($data['name'])."',
                        short_description = '".$this->cleanString($data['sort_description'])."',
                        status      = '1',
                        added_by    = '$admin_id',  
                        created_at  = '$curr',
                        updated_at  = '$curr' ";    
                    $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                    $exe =mysqli_query($link,$query);
                    $last_id = mysqli_insert_id($link);
                    if($exe){   
                        unset($_SESSION['add_department_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    }
                }else{
                    return $this->errorMsg("Entered department is already exist!");
                }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit Department 

    function editDepartment($data="")
    {
        if(isset($_SESSION['edit_department_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_department_key']){  
                $id             = $this->decryptData($data['session_token']);
                $admin_id       =$_SESSION["crm_admin_id"];
                $curr           = date("Y-m-d H:i:s");
                $e_check = $this -> check_query(DEPARTMENT_TBL,"id"," token ='".$this->masterhyphenize($data['name'])."' AND id!='".$id."' ");
                if($e_check == 0){  
                        $query = " UPDATE ".DEPARTMENT_TBL." SET 
                            token       = '".$this->masterhyphenize($data['name'])."',
                            name        = '".$this->cleanString($data['name'])."',
                            short_description = '".$this->cleanString($data['sort_description'])."',
                            updated_at  = '$curr' WHERE id='$id' ";    
                        $link  = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
                        $exe =mysqli_query($link,$query);
                        $last_id = mysqli_insert_id($link);
                        if($exe){       
                            unset($_SESSION['edit_department_key']);
                            return 1;
                        }else{
                            return "Sorry!! Unexpected Error Occurred. Please try again.";
                        }
                         
                }else{
                    return $this->errorMsg("Entered department is already exist!");
                }               
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function departmentStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(DEPARTMENT_TBL,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".DEPARTMENT_TBL." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".DEPARTMENT_TBL." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Role
    ------------------------------*/

    // Add Role

    function addEmployeeRole($data)
    {
        $layout = "";
        if(isset($_SESSION['add_role_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_role_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(EMPLOYEE_ROLE,"id"," token ='".$this->masterhyphenize($data['employee_role'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".EMPLOYEE_ROLE." SET
                        token           = '".$this->masterhyphenize($data['employee_role'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        employee_role   = '".$this->cleanString($data['employee_role'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                        //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_role_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Employee role is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editEmployeeRole($id='')
    {   
        $result =  array();
        $_SESSION['edit_role_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(EMPLOYEE_ROLE,"id,employee_role","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_role_key'];
        $result['employee_role'] = $info['employee_role'];
        return $result;
    }   

    // Update 

    function updateEmployeeRole($data)
    {
        if(isset($_SESSION['edit_role_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_role_key']){
                $check = $this->check_query(EMPLOYEE_ROLE,"id"," token ='".$this->masterhyphenize($data['employee_role'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".EMPLOYEE_ROLE." SET 
                       token          = '".$this->masterhyphenize($data['employee_role'])."',
                       employee_role  = '".$this->cleanString($data['employee_role'])."',
                       added_by       = '$admin_id',  
                       updated_at     = '$curr'
                       WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['employee_role'];
                        unset($_SESSION['edit_role_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Employee role is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function empRoleStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(EMPLOYEE_ROLE,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".EMPLOYEE_ROLE." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".EMPLOYEE_ROLE." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    /*----------------------------
         Payment Stage Master
    ------------------------------*/

    // Add Payment Stage

    function addPaymentStage($data)
    {
        $layout = "";
        if(isset($_SESSION['add_stage_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['add_stage_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(STAGE_MASTER,"id"," token ='".$this->masterhyphenize($data['payment_stage'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".STAGE_MASTER." SET
                        token           = '".$this->masterhyphenize($data['payment_stage'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_id      = '".$project_id."',
                        stage_name      = '".$this->cleanString($data['payment_stage'])."',
                        payment_percentage = '".$this->cleanString($data['payment_percentage'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                        //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['add_role_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Payment Stage is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editPaymentStage($id='')
    {   
        $result =  array();
        $_SESSION['edit_stage_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(STAGE_MASTER,"id,stage_name,payment_percentage","id='$id'");
        $result['id'] = $id;
        $result['fkey'] = $_SESSION['edit_stage_key'];
        $result['payment_stage'] = $info['stage_name'];
        $result['payment_percentage'] = $info['payment_percentage'];
        return $result;
    }   

    // Update 

    function updateStageRole($data)
    {
        if(isset($_SESSION['edit_stage_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_stage_key']){
                $check = $this->check_query(STAGE_MASTER,"id"," token ='".$this->masterhyphenize($data['payment_stage'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".STAGE_MASTER." SET 
                       token          = '".$this->masterhyphenize($data['payment_stage'])."',
                       stage_name     = '".$this->cleanString($data['payment_stage'])."',
                       payment_percentage = '".$this->cleanString($data['payment_percentage'])."',
                       added_by       = '$admin_id',  
                       updated_at     = '$curr'
                       WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['payment_stage'];
                        unset($_SESSION['edit_stage_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Entered Payment Stage is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function stageStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(STAGE_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".STAGE_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".STAGE_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

     function uploadVideoDocuments($data,$files)
    {
        $layout = "";
        if(isset($_SESSION['edit_helpvideo_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_helpvideo_key']){
                $token          = $this->generateRandomString("30");
                $curr           = date("Y-m-d H:i:s");
                $admin_id       =$_SESSION["crm_admin_id"];
                $id             = $this->decryptData($data['session_token']);

                if ($data['file_test']==1) {
                    $attachment     = '';
                    $output         = '';
                    $doc_info       = ''; 
                    $document_type  = '';
                }else{
                    $attachment     = $this->cleanString($files['pdf']);
                    $output         = str_replace("uploads/videos/", "", $attachment);
                    $doc_info       = explode(".", $output); 
                    $document_type  = end($doc_info);
                }
                
                $query = "UPDATE ".UPLOAD_PAGE_VIDEO." SET
                    added_by         = '".$admin_id."', ";
                    if ($data['file_test']==0) {
                    $query .= "
                            page_video        = '".$attachment."',
                            video_type      = '".$document_type."',";        
                    }

                    $query .= "                    
                    updated_at       = '".$curr."' WHERE id='".$id."' ";
                //return $query;
                $exe = $this->selectQuery($query);
                if($exe){
                    unset($_SESSION['edit_helpvideo_key']);
                    return 1;
                }else{
                    return "Sorry!! Unexpected Error Occurred. Please try again.";
                }   
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Avtive & Inactive Status 

    function uploadVideoStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(UPLOAD_PAGE_VIDEO,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".UPLOAD_PAGE_VIDEO." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".UPLOAD_PAGE_VIDEO." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Remove Video Doc

    function removeVideoDocument($id)
    {
        $q = "UPDATE ".UPLOAD_PAGE_VIDEO." SET page_video ='' WHERE id='$id' ";
        $exe = $this->selectQuery($q);
        return 1;
    }

    // Video Preview 

    function videoPreview($id='')
    {   
        $result          =  array();
        $info            = $this->getDetails(UPLOAD_PAGE_VIDEO,"id,page_video,title","id='$id'");
        $result['id']    = $id;
        $result['video'] ="

            <div class='modal-body modal-body-sm'>
                <h5 class='title' >".$info['title']." video preview</h5>
                <div class='tab-content'>
                    <div class='tab-pane active' id='personal'>
                        <div class='row gy-4'>
                            <div class='col-md-12'>
                                <video width='320' height='200' controls autoplay='1' style='float: left;'>
                                   <source src='".VIDEO_PATH.$info['page_video']."' type=''>
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";

        return $result;
    }  

    // Video Show Preview 

    function videoShowPreview($id='')
    {   
        $result          =  array();
        $info            = $this->getDetails(UPLOAD_PAGE_VIDEO,"id,page_video,title","id='$id'");
        $result['id']    = $id;
        $result['video'] ="

            <div class='modal-body modal-body-sm'>
                <h5 class='title' >".$info['title']." video preview</h5>
                <div class='tab-content'>
                    <div class='tab-pane active' id='personal'>
                        <div class='row gy-4'>
                            <div class='col-md-12'>
                                <video width='' height='300' controls autoplay='1' style='float: left;'>
                                   <source src='".VIDEO_PATH.$info['page_video']."' type=''>
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ";

        return $result;
    }  

    /*----------------------------
         Project Master
    ------------------------------*/

    // Add Project Master 

    function addProjectMaster($data)
    {
        $layout = "";
        if(isset($_SESSION['project_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['project_master_key']){
                $token          = $this->generateRandomString("30");
                $admin_id       = $_SESSION["crm_admin_id"];
                $project_id     = @$_SESSION['crm_project_id'];
                $curr           = date("Y-m-d H:i:s");
                $check = $this->check_query(PROJECT_MASTER,"id"," token ='".$this->masterhyphenize($data['project_name'])."'  ");
                if ($check==0) {
                    $query = "INSERT INTO ".PROJECT_MASTER." SET
                        token           = '".$this->masterhyphenize($data['project_name'])."',
                        tenant_id       = '".$_SESSION["tenant_id"]."',
                        project_name    = '".$this->cleanString($data['project_name'])."',
                        added_by        = '$admin_id',
                        status          = '1',
                        created_at      = '$curr',
                        updated_at      = '$curr' ";
                                //return $query;
                    $exe = $this->selectQuery($query);
                    if($exe){
                        unset($_SESSION['project_master_key']);
                        return 1;
                    }else{
                        return "Sorry!! Unexpected Error Occurred. Please try again.";
                    } 
                }else{
                    return $this->errorMsg("Sorry Entered Project name is already exist!.");
                }        
            }else{
                return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            }
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }
    }

    // Edit 

    function editProjectMaster($id='')
    {   
        $result =  array();
        $_SESSION['edit_project_master_key'] = $this->generateRandomString("40");
        $info = $this->getDetails(PROJECT_MASTER,"id,project_name","id='$id'");
        $result['id']           = $id;
        $result['fkey']         = $_SESSION['edit_project_master_key'];
        $result['project_name'] = $info['project_name'];
        return $result;
    }   

    // Update 

    function updateProjectMaster($data)
    {
        if(isset($_SESSION['edit_project_master_key'])){
            if($this->cleanString($data['fkey']) == $_SESSION['edit_project_master_key']){
                $check = $this->check_query(PROJECT_MASTER,"id"," token ='".$this->masterhyphenize($data['project_name'])."' AND id!='".$data['token']."' ");
                if ($check==0) {
                    $admin_id       = $_SESSION["crm_admin_id"];
                    $curr           = date("Y-m-d H:i:s");
                    $today          = date("d/m/Y");
                    $query = "UPDATE ".PROJECT_MASTER." SET 
                               token          = '".$this->masterhyphenize($data['project_name'])."',
                               project_name   = '".$this->cleanString($data['project_name'])."',
                               added_by       = '$admin_id',  
                               updated_at     = '$curr'
                               WHERE id       = '".$data['token']."' ";
                   // return $query;
                   $exe = $this->selectQuery($query);
                    if($exe){
                        $result =  array();
                        $result['status']   = "1";
                        $result['id']       = $data['token'];
                        $result['date']     = $today;
                        $result['item']     = $data['project_name'];
                        unset($_SESSION['edit_project_master_key']);
                        return $result;
                    }else{
                        $result =  array();
                        $result['status']   = "1";
                        $result['msg']      = "Sorry!! Unexpected Error Occurred. Please try again.";
                        return $result; 
                    }
                }else{
                    $result =  array();
                    $result['status']       = "0";
                    $result['msg']          = $this->errorMsg("Sorry Entered Project name is already exist!.");
                    return $result;
                }       
            }else{
                $result =  array();
                $result['status']       = "0";
                $result['msg']          = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
                return $result;
            }
        }else{
            $result =  array();
            $result['status']           = "0";
            $result['msg']              = "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
            return $result;
        }
    }

    // Avtive & Inactive Status 

    function projectMasterStatus($data)
    {
        $data = $this->decryptData($data);
        $info = $this -> getDetails(PROJECT_MASTER,"status"," id ='$data' ");
        if($info['status'] ==1){
            $query = "UPDATE ".PROJECT_MASTER." SET status='0' WHERE id='$data' ";
        }else{
            $query = "UPDATE ".PROJECT_MASTER." SET status='1' WHERE id='$data' ";
        }
        $up_exe = $this->selectQuery($query);
        if($up_exe){
            return 1;
        }
    }

    // Select Property

    function selectProject($data)
    {
        
        if(isset($_SESSION['crm_admin_id'])){
            $_SESSION['crm_project_id'] = $data;
            return 1;
        }else{
            return "Sorry Invalid User Entry. Try Re login to the Panel to continue.";
        }

    }

    /*----------------------------
         Project Details
    ------------------------------*/

    // Add Project Details 

    function updateDueDate($data)
    {
        $layout = "";
        $curr           = date("Y-m-d H:i:s");
        $date           = $this->changeDateFormat($data['due_date']);
        $query = "UPDATE ".PAYMENT_SCHEDULE." SET
            due_date        = '".$date."',
            updated_at      = '$curr' WHERE id='".$data['payment_schedule_id']."' ";
        $exe = $this->selectQuery($query);
        if($exe){
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        } 
    }

    function addPaymentSchedulelist($data)
    {
        $layout = "";
        $curr           = date("Y-m-d H:i:s");
        $project_id     = @$_SESSION['crm_project_id'];
        $balance        = $data['balance_amt'] - $data['amt_paying'];
        $today          = date("Y-m-d");
        $payment_schedule_id = $data['payment_schedule_id'];
        $property_id    = $data['property_id'];
        $admin_id       =$_SESSION["crm_admin_id"];
        $query          = "INSERT INTO ".PAYMENT_SCHEDULE_LIST." SET
            tenant_id       = '".$_SESSION["tenant_id"]."',
            project_id      = '".$project_id."',
            property_id     = '".$property_id."' ,
            payment_schedule_id = '".$payment_schedule_id."',
            stage_id        = '".$this->cleanString($data['stage_id'])."',
            percentage      = '".$this->cleanString($data['per'])."',
            amt_payable     = '".$data['balance_amt']."', 
            due_date        = '".$data['due_date']."',
            amt_receive     = '".$data['amt_paying']."',
            payed_date      = '".$today."',
            balance         = '".$balance."',
            status          = '1',
            added_by        = '$admin_id' ,
            created_at      = '$curr' ,
            updated_at      = '$curr'  ";
        $exe = $this->selectQuery($query);
        if($exe){
            $paymentSchedule = $this->updatePaymentSchedule($data,$payment_schedule_id);  
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        } 
    }

    function updatePaymentSchedule($data,$payment_schedule_id)
    {
        $layout = "";
        $curr              = date("Y-m-d H:i:s");
        $paymentinfo       = $this ->getDetails(PAYMENT_SCHEDULE,"*"," id='$payment_schedule_id' ");
        $balance           = $paymentinfo['balance'] - $data['amt_paying'];
        $amt_receive       = $paymentinfo['amt_receive'] + $data['amt_paying'];
        $due_date          = date('Y-m-d',strtotime($data['due_date']." +30 days"));
        $query          = "UPDATE ".PAYMENT_SCHEDULE." SET
            due_date        = '".$due_date."',
            amt_receive     = '".$amt_receive."',
            balance         = '".$balance."',
            updated_at      = '$curr' WHERE id='".$payment_schedule_id."'  ";
        $exe = $this->selectQuery($query);
        if($exe){
            return 1;
        }else{
            return "Sorry!! Unexpected Error Occurred. Please try again.";
        } 
    }



//-----------------Dont'delete----------------
	
}


?>