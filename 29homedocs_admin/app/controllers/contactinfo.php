<?php

class Contactinfo extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("contactinfo");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Contact Information Master' ");
				$this->view('home/managecontactinfo', 
					[	
						'active_menu' 	=> 'contactinfo',
						'meta_title'  	=>  COLNAME.' | Manage Contact Information',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageContactInfo(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',		
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("contactinfo");
	        if ($check==1) {
				if(!isset($_SESSION['add_contactinfo_key'])){
					$_SESSION['add_contactinfo_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/addcontactinfo', 
					[	
						'active_menu' 	=> 'contactinfo',
						'meta_title'  	=>  COLNAME.' | Add Contact Information',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'addcontactinfo',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("contactinfo");
	        if ($check==1) {
				if(!isset($_SESSION['edit_contactinfo_key'])){
					$_SESSION['edit_contactinfo_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(CONTACT_INFO,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CONTACT_INFO,"*"," id='$token' ");
					$this->view('home/editcontactinfo', 
						[	
							'active_menu' 		=> 'contactinfo',
							'meta_title'  		=>  COLNAME.' | Edit Contact Information',
							'page_title'  		=>  'Edit Contact Information',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'scripts'			=> 'addcontactinfo',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function details($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("contactinfo");
	        if ($check==1) {
				$validate = $user->check_query(CONTACT_INFO,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CONTACT_INFO,"*"," id='$token' ");
					$this->view('home/viewcontactinfo', 
						[	
							'active_menu' 		=> 'contactinfo',
							'meta_title'  		=>  COLNAME.' | View Contact Information',
							'page_title'  		=>  'View Contact Information',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'info'				=>  $info,
							'scripts'			=> 'addcontactinfo',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}			
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>