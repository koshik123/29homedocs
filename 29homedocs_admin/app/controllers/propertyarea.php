<?php

class  Propertyarea  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("areamaster");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Property Area Master' ");
				$this->view('home/managepropertyarea', 
					[	
						'active_menu' 	=> 'area',
						'meta_title'  	=>  COLNAME.' | Manage Property Area',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->managePropertyArea(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("areamaster");
	        if ($check==1) {
				if(!isset($_SESSION['add_property_area_key'])){
					$_SESSION['add_property_area_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/addpropertyarea', 
					[	
						'active_menu' 	=> 'area',
						'meta_title'  	=>  COLNAME.' | Add Property Area',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("areamaster");
	        if ($check==1) {
				if(!isset($_SESSION['edit_property_area_key'])){
					$_SESSION['edit_property_area_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(PROPERTY_AREA,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(PROPERTY_AREA,"*"," id='$token' ");
					$this->view('home/editpropertyarea', 
						[	
							'active_menu' 	=> 'documents',
							'meta_title'  	=>  COLNAME.' | Edit Property Area',
							'page_title'  	=>  ' Edit Property Area',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'brochure',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>