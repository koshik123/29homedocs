<?php

class Trash extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$info_details = $user->getDetails(EMPLOYEE,"*"," id='".$_SESSION["crm_admin_id"]."' ");
			$info = $user->editPagePublish($info_details);
			$this->view('home/restoregeneraldocuments', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | General Documents',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'info'				=>  $info,
				'list' 			=> $user->trashGeneralDocuments(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function checklist()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restorechecklistdocuments', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Document Checklists',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashDocumentChecklist(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function news()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restorenews', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | News',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashNews(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function leads()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restoreleads', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Leads',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashLeads(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function invoice()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restoreinvoice', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Invoices/Receipts',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashInvoice(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function gallery()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restoregallery', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Gallery',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashGallery(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function brochure()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restorebrochure', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Property Brochure',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashBrochure(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function property()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restoreproperty', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Property ',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->trashProperty(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function canceled()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$this->view('home/restorecancelproperty', 
			[	
				'active_menu' 	=> 'trash',
				'meta_title'  	=>  COLNAME.' | Property ',
				'page_title'  	=>  COLNAME,
				'meta_keywords' =>  META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'list' 			=> $user->cancelProperty(),
				'scripts'		=> 'home',	
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>