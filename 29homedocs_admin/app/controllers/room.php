<?php

class Room extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/manageroom', 
				[	
					'active_menu' 	=> 'room',
					'meta_title'  	=>  COLNAME.' | Manage Rooms',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');		
			$this->view('home/addroom', 
				[	
					'active_menu' 	=> 'room',
					'meta_title'  	=>  COLNAME.' | Add Room',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>