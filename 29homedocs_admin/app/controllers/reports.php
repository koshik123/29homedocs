<?php

class Reports extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("adhocreport");
	        if ($check==1) {
				$this->view('home/customerreport', 
					[	
						'active_menu' 	=> 'customerreport',
						'meta_title'  	=>  COLNAME.' | Customer Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function adhoc()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("adhocreport");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Adhoc Reports' ");	
				$this->view('home/adhocreport', 
					[	
						'active_menu' 	=> 'adhocreport',
						'meta_title'  	=>  COLNAME.' | Adhoc Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'customer_list' => $user->getCustomerList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function adhocreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("adhocreport");
	        if ($check==1) {
				$cus_id = $_GET['customer_id'];
				$status = $_GET['status'];

				$cusinfo = $user->getDetails(CUSTOMER_TBL,"*"," id='$cus_id'  ");	
	        	$this->view('home/adhocreportinfo', 
	                [
	                    'active_menu' 	=> 'adhocreport',
						'meta_title'	=> 'Adhoc Reports - '.COMPANY_NAME,
						'page_title'  	=> 'Adhoc Reports',
						'scripts'		=> 'reports',
						'cusname' 		=>  $cusinfo['name'],
						'cus_id' 		=>  $cus_id,
						'status' 		=>  $status,
					//	'list'          =>  $user->manageCustomerReport($from,$to),
						'list'			=> $user->adhocRequestsReport($from,$to,$cus_id,$status),
						'customer_list' => $user->getCustomerList(),
						'from'			=> (($from!="") ? $from : ""),
						'to'			=> (($to!="") ? $to : ""),
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}

	public function lead()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadreport");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Lead Reports' ");	
				$this->view('home/leadreport', 
					[	
						'active_menu' 	=> 'leadreport',
						'meta_title'  	=>  COLNAME.' | Lead Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'lead_list' 	=> $user->getLeadStatus(),
						'employee_list' => $user->getEmployeeList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("leadreport");
	        if ($check==1) {
				$employee_id = $_GET['employee_id'];
				$leadstatus  = $_GET['leadstatus'];

				$employeeinfo 	  = $user->getDetails(EMPLOYEE,"*"," id='$employee_id'  ");	
				$leadstatusname   = $user->getLeadStatusName($leadstatus);
	        	$this->view('home/leadreportinfo', 
	                [
	                    'active_menu' 	=> 'leadreport',
						'meta_title'	=> 'Lead Reports - '.COMPANY_NAME,
						'page_title'  	=> 'Lead Reports',
						'scripts'		=> 'leadreports',
						'empname' 		=>  $employeeinfo['name'],
						'employee_id' 	=>  $employee_id,
						'status' 		=>  (($leadstatus!='') ? $leadstatusname  : '' ),
					//	'list'          =>  $user->manageCustomerReport($from,$to),
						'list'			=> $user->leadReport($from,$to,$employee_id,$leadstatus),
						'lead_list' 	=> $user->getLeadStatus(),
						'employee_list' => $user->getEmployeeList(),
						'from'			=> (($from!="") ? $from : ""),
						'to'			=> (($to!="") ? $to : ""),
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}

	public function customer()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("customerreport");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Customer Reports' ");	
				$this->view('home/customerreport', 
					[	
						'active_menu' 	=> 'customerreport',
						'meta_title'  	=>  COLNAME.' | Customer Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'cus_list' 		=> $user->getCustomerList(),
						'employee_list' => $user->getEmployeeList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function customerreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("customerreport");
	        if ($check==1) {
				$customer_id = $_GET['customer_id'];
				$priority  	 = $_GET['priority'];
				$kyc_status  = $_GET['kyc_status'];

				$custinfo = $user->getDetails(CUSTOMER_TBL,"*"," id='$customer_id'  ");	

				$kyc_status1 = (($kyc_status=='0') ? 'Not submitted' : ''  );
                $kyc_status2  = (($kyc_status=='1') ? 'Approved' : $kyc_status1 );
                $kyc_status3  = (($kyc_status=='2') ? 'Pending Approval' : $kyc_status2 );
                $kyc_status4  = (($kyc_status=='3') ? 'Rejected' : $kyc_status3 );

	        	$this->view('home/customerreportinfo', 
	                [
	                    'active_menu' 	=> 'customerreport',
						'meta_title'	=> 'Customer Reports - '.COMPANY_NAME,
						'page_title'  	=> 'Lead Reports',
						'scripts'		=> 'leadreports',
						'cusname' 		=>  (($customer_id!='') ? $custinfo['name']  : '' ),
						'priorityname' 	=>  (($priority!='') ? $priority  : '' ),
						'kyc_statusname'=>  (($kyc_status!='') ? $kyc_status4  : '' ),
						'customer_id' 	=>  $customer_id,
						'priority' 		=>  $priority,
						'kyc_status' 	=>  $kyc_status,
					//	'list'          =>  $user->manageCustomerReport($from,$to),
						'list'			=> $user->manageCustomerReport($from,$to,$customer_id,$priority,$kyc_status),
						'cus_list' 		=> $user->getCustomerList(),
						'from'			=> (($from!="") ? $from : ""),
						'to'			=> (($to!="") ? $to : ""),
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}

	public function kyc()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("kycreport");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='KYC Reports' ");	
				$this->view('home/kycreport', 
					[	
						'active_menu' 	=> 'kycreport',
						'meta_title'  	=>  COLNAME.' | Lead Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'cus_list' 		=> $user->getCustomerList(),
						'employee_list' => $user->getEmployeeList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',		
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function kycreport($from="",$to="")
	{		
		if(isset($_SESSION["crm_admin_id"])){		
			$user = $this->model('User'); 
			$check = $user->pagePermission("kycreport");
	        if ($check==1) {
				$customer_id = $_GET['customer_id'];
				$kyc_status  = $_GET['kyc_status'];

				$custinfo = $user->getDetails(CUSTOMER_TBL,"*"," id='$customer_id'  ");	

				$kyc_status1  = (($kyc_status=='0') ? 'Not submitted' : ''  );
                $kyc_status2  = (($kyc_status=='1') ? 'Approved' : $kyc_status1 );
                $kyc_status3  = (($kyc_status=='2') ? 'Pending Approval' : $kyc_status2 );
                $kyc_status4  = (($kyc_status=='3') ? 'Rejected' : $kyc_status3 );
	        	$this->view('home/kycreportinfo', 
	                [
	                    'active_menu' 	=> 'kycreport',
						'meta_title'	=> 'KYC Reports - '.COMPANY_NAME,
						'page_title'  	=> 'KYC Reports',
						'scripts'		=> 'leadreports',
						'cusname' 		=> (($customer_id!='') ? $custinfo['name']  : '' ),
						'kyc_statusname'=> (($kyc_status!='') ? $kyc_status4  : '' ),
						'customer_id' 	=> $customer_id,
						'kyc_status' 	=> $kyc_status,
						'list'			=> $user->manageKYCReport($from,$to,$customer_id,$kyc_status),
						'cus_list' 		=> $user->getCustomerList(),
						'from'			=> (($from!="") ? $from : ""),
						'to'			=> (($to!="") ? $to : ""),
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=> $user->userInfo($_SESSION["crm_admin_id"])   
	                ]);
	        }else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
        }else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}		
	    
	}

	public function document()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("adhocreport");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Document Reports' ");	
				$this->view('home/documentreport', 
					[	
						'active_menu' 	=> 'documentreport',
						'meta_title'  	=>  COLNAME.' | Document Reports',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
				$this->view('home/error', 
					[	
						'active_menu' 	=> 'report',
						'meta_title'  	=> '404 Error - Page Not Found',
						'scripts'		=> 'error',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>