<?php

class Profile extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['edit_profile_key'])){
				$_SESSION['edit_profile_key'] = $user->generateRandomString("40");
			}
			$info_details = $user->getDetails(EMPLOYEE,"*"," id='".$_SESSION["crm_admin_id"]."' ");
			$info = $user->editPagePublish($info_details);
			$this->view('home/profile', 
			[	
				'active_menu' 		=> 'profile',
				'meta_title'  		=>  COLNAME.' | Profile Settings',
				'page_title'  		=>  COLNAME,
				'meta_keywords' 	=> META_KEYWORDS,
				'meta_description'  => META_DESCRIPTION,
				'scripts'			=> 'home',	
				'info'				=>  $info,
				'token'				=>	$user->encryptData($info['id']),
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),		
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function changepassword()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['change_password_key'])){
				$_SESSION['change_password_key'] = $user->generateRandomString("40");
			}
			$this->view('home/changepassword', 
				[	
					'active_menu' 	=> 'profile',
					'meta_title'  	=>  COLNAME.' | Change Password',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',		
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),		
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function activity()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/accountactivity', 
				[	
					'active_menu' 	=> 'profile',
					'meta_title'  	=>  COLNAME.' | Account Activity',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->manageLoginActivity(),
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),			
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function notification()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/notifications', 
				[	
					'active_menu' 	=> 'profile',
					'meta_title'  	=>  COLNAME.' | Notifications',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),	
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),		
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'profile',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>