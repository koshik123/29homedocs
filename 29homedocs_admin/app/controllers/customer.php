<?php

class Customer extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Open Requests' ");	
				$this->view('home/managecustomer', 
					[	
						'active_menu' 	=> 'customer',
						'meta_title'  	=>  COLNAME.' | Manage Customer',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'list' 			=> $user->manageCustomer(),
						'property_list' => $user->getPropertyList(),
						'scripts'		=> 'managecustomer',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function assign($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {	
				if(!isset($_SESSION['assign_property_key'])){
					$_SESSION['assign_property_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CUSTOMER_TBL,"*"," id='$token' ");
					$this->view('home/assignproperty', 
						[	
							'active_menu' 	=> 'customer',
							'meta_title'  	=>  COLNAME.' | Assign Property',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'property_list' => $user->getAssignPropertyList(),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {
				if(!isset($_SESSION['add_customer_key'])){
					$_SESSION['add_customer_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage existing Leads' ");
				$this->view('home/addcustomer', 
					[	
						'active_menu' 	=> 'customer',
						'meta_title'  	=>  COLNAME.' | Add Customer',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("managecustomer");
	        if ($check==1) {
				if(!isset($_SESSION['edit_customer_key'])){
					$_SESSION['edit_customer_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CUSTOMER_TBL,"*"," id='$token' ");
					$this->view('home/editcustomer', 
						[	
							'active_menu' 	=> 'customer',
							'meta_title'  	=>  COLNAME.' | Edit Customer',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
						//	'property_list' => $user->getPropertyList(),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$pcheck = $user->pagePermission("managecustomer");
	        if ($pcheck==1) {
				if(!isset($_SESSION['edit_customer_key'])){
					$_SESSION['edit_customer_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(CUSTOMER_TBL,"*"," id='$token' ");
					$this->view('home/viewcustomer', 
						[	
							'active_menu' 	=> 'customer',
							'meta_title'  	=>  COLNAME.' | View Customer',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'kyc_list'		=>  $user->getkycList($info['id']),	
							'info'			=> $info,
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function kyc()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("kycdocuments");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='KYC Documents Management' ");	
				$this->view('home/managekyc', 
					[	
						'active_menu' 	=> 'kyc',
						'meta_title'  	=>  COLNAME.' | Customer KYC',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list'			=> $user->manageKyc(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function kycdetails($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("kycdocuments");
	        if ($pcheck==1) {
				$check = $user->check_query(CUSTOMER_TBL,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(CUSTOMER_TBL,"*"," id='$token'  ");	
					$this->view('home/kycdetails', 
						[	
							'active_menu' 	=> 'kyc',
							'meta_title'  	=>  COLNAME.' | KYC Info',
							'page_title'  	=>  'KYC Info',
							'info'			=>	$info,
							
							'kyc_list'		=>  $user->getkycList($info['id']),	
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'customer',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>