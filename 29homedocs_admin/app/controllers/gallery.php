<?php

class Gallery extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("gallery");
	        if ($check==1) {		
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Gallery' ");	
				$this->view('home/managegallery', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=>  COLNAME.' | Manage Gallery',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageGallery(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("gallery");
	        if ($check==1) {
				if(!isset($_SESSION['add_gallery_key'])){
					$_SESSION['add_gallery_key'] = $user->generateRandomString("40");
				}
				$this->view('home/addgallery', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=>  COLNAME.' | Add Gallery',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),				
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("gallery");
	        if ($check==1) {
				if(!isset($_SESSION['edit_gallery_key'])){
					$_SESSION['edit_gallery_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(GALLERY_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(GALLERY_TBL,"*"," id='$token' ");
					$this->view('home/editgallery', 
						[	
							'active_menu' 	=> 'gallery',
							'meta_title'  	=>  COLNAME.' | Edit Gallery',
							'page_title'  	=>  'Edit Gallery',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'addproperty',
							'project_items'	=>	$user->selectProjectItems(),	
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function images($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("gallery");
	        if ($pcheck==1) {
				if(!isset($_SESSION['add_galleryimg_key'])){
					$_SESSION['add_galleryimg_key'] = $user->generateRandomString("40");
				}
				$check = $user->check_query(GALLERY_TBL,"id"," token='$token' ");
		        if($check){
					 $info = $user->getDetails(GALLERY_TBL,"*"," token='$token'  ");
					$this->view('home/galleryimages', 
						[	
							'active_menu' 	  	=> 'gallery',
							'meta_title'	  	=> 'Add Images - '.COMPANY_NAME,
							'page_title'  	  	=> 'Add Images',
							'redirect'		  	=> '',
							'images'			=>	$user->galleryImages($info['id']),
							'token'			  	=>  $user->encryptData($info['id']),
							'info'			  	=>  $info,
							'scripts'		  	=> 'addimages',
							'project_items'		=>	$user->selectProjectItems(),
							'user'   		  	=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'active_menu' 	=> 'gallery',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}		
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>