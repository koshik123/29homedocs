<?php

class Newsinfo extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/managenewsinfo', 
				[	
					'active_menu' 	=> 'newsinfo',
					'meta_title'  	=>  COLNAME.' | Manage News Information',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/addnewsinfo', 
				[	
					'active_menu' 	=> 'newsinfo',
					'meta_title'  	=>  COLNAME.' | Add News Information',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),				
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function edit()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/editnewsinfo', 
				[	
					'active_menu' 	=> 'newsinfo',
					'meta_title'  	=>  COLNAME.' | Edit News Information',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),				
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>