<?php

require_once '../app/excel/PHPExcel.php';

class Exportdata extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'active_menu' 	=> 'error',
					'project_items'	=>	$user->selectProjectItems(),
					'user'   		=>  $user->userInfo(($_SESSION["crm_admin_id"]))
				]);
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
	    }
	}

	public function customer($from="",$to="")
	{
		if(isset($_SESSION["ecom_admin_id"])){
			$user = $this->model('Export');

			$today = date("d-m-y");
			$title = "customer_report_".$today;
			
			// PHPExcel Instance
			$sheet = new PHPExcel();

			//------------- Create Header ------------//
			
			// Set document properties
			// Set document properties
			$sheet->getProperties()->setCreator("Post CRM")
			               ->setLastModifiedBy(COMPANY_NAME)
			               ->setTitle("Customer Report")
			               ->setSubject("Customer Report")
			               ->setDescription("Customer Report")
			               ->setKeywords("Post CRM")
			               ->setCategory("Customer Report");
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$sheet->setActiveSheetIndex(0);
			$activeSheet = $sheet->getActiveSheet();
			
			// Create Header
			$sheet->setActiveSheetIndex(0)
						->setCellValue('A1', 'S.No')
			           	->setCellValue('B1', 'UID')
			            ->setCellValue('C1', 'Name')
			            ->setCellValue('D1', 'Flat')
			            ->setCellValue('E1', 'Address')
			            ->setCellValue('F1', 'Contact Number')
			            ->setCellValue('G1', 'Email')
			            ->setCellValue('H1', 'Priority')
			            ->setCellValue('I1', 'KYC Status')
			            ->setCellValue('J1', 'Flat Assign Status');

			$activeSheet->getColumnDimension('A')->setAutoSize(true);
			$activeSheet->getColumnDimension('B')->setAutoSize(true);
			$activeSheet->getColumnDimension('C')->setAutoSize(true);
			$activeSheet->getColumnDimension('D')->setAutoSize(true);
			$activeSheet->getColumnDimension('E')->setAutoSize(true);
			$activeSheet->getColumnDimension('F')->setAutoSize(true);
			$activeSheet->getColumnDimension('G')->setAutoSize(true);
			$activeSheet->getColumnDimension('H')->setAutoSize(true);
			$activeSheet->getColumnDimension('I')->setAutoSize(true);
			$activeSheet->getColumnDimension('J')->setAutoSize(true);
			
			//------------- Create Header Ends ------------//

			//------------- Create Report Body ------------//

			$users = $user->contactList($from,$to);

			$row = 2;
			foreach ($users as $key => $value) {
				$col = 0;
			    foreach ($value as $key=> $value) {
			        //echo $row." $col -- ".$key."=".$value."<br/>";
			        $sheet->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $value);
			        $col++;
			   }
			   $row++;
			}
			

			// Rename worksheet
			$sheet_name = "Customer Report";
	
			$activeSheet ->setTitle($sheet_name);
			$filename = $title.".csv";
			// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename='.$filename);
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
			//ob_end_clean();
			$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'CSV');
			$objWriter->save('php://output');

			
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
	    }
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("block");
	        if ($check==1) {	
				$this->view('home/addblock', 
					[	
						'active_menu' 	=> 'block',
						'meta_title'  	=>  COLNAME.' | Add Block',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>