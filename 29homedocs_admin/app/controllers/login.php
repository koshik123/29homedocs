<?php

class login extends Controller
{
	
	public function index()
	{
		$user = $this->model('User');
		$this->view('home/login', 
		[	
			'active_menu' 	=> 'home',
			'meta_title'  	=>  COLNAME.' | Home',
			'page_title'  	=>  COLNAME,
			'meta_keywords' => META_KEYWORDS,
			'meta_description' => META_DESCRIPTION,
			'scripts'		=> 'home',			
		]);
	}
	
	public function error()
	{	
		$user = $this->model('User');
		$this->view('home/error', 
		[	
			'active_menu' 	=> 'home',
			'meta_title'  	=> '404 Error - Page Not Found',
			'scripts'		=> 'error',
			'page_title'  	=>  COLNAME,
			'meta_keywords' => META_KEYWORDS,
			'meta_description' => META_DESCRIPTION,
			
		]);
	}
}


?>