<?php

class News extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managenews");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Published News' ");	
				$this->view('home/managenews', 
					[	
						'active_menu' 	=> 'news',
						'meta_title'  	=>  COLNAME.' | Manage News',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageNews(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'managecustomer',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managenews");
	        if ($check==1) {
				if(!isset($_SESSION['add_news_key'])){
					$_SESSION['add_news_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Add News' ");		
				$this->view('home/addnews', 
					[	
						'active_menu' 	=> 'news',
						'meta_title'  	=>  COLNAME.' | Add News',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'addnews',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managenews");
	        if ($check==1) {
				if(!isset($_SESSION['edit_news_key'])){
						$_SESSION['edit_news_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(NEWS_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(NEWS_TBL,"*"," id='$token' ");
					$this->view('home/editnews', 
						[	
							'active_menu' 		=> 'news',
							'meta_title'  		=>  COLNAME.' | Edit News',
							'page_title'  		=>  'Edit News',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'list' 				=>  $user->getSelectCustomerList($info['id']),
							'info'				=>  $info,
							'scripts'			=> 'addnews',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}					
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function details($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managenews");
	        if ($check==1) {
				$validate = $user->check_query(NEWS_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(NEWS_TBL,"*"," id='$token' ");
					$this->view('home/viewnews', 
						[	
							'active_menu' 		=> 'news',
							'meta_title'  		=>  COLNAME.' | View News',
							'page_title'  		=>  'View News',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'list' 				=>  $user->getSelectCustomerList($info['id']),
							'info'				=>  $info,
							'scripts'			=> 'employee',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>