<?php

class Property extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Property' ");		
				$this->view('home/manageproperty', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=>  COLNAME.' | Manage Property',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageProperty(),
						'newlist_old' 		=> $user->manageNewProperty_old(),
						'newlist' 		=> $user->manageNewProperty(),
						'floor_types'   => $user->getFloortypeList(),
						'block_types'   => $user->getBlocktypeList(),
						'villa_types'   => $user->getVillaypeList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function trash()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("trash");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Trash Property' ");		
				$this->view('home/managecancelproperty', 
					[	
						'active_menu' 	=> 'trash',
						'meta_title'  	=>  COLNAME.' | Trash Items',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageCancelProperty(),
						'newlist_old' 		=> $user->manageNewProperty_old(),
						'newlist' 		=> $user->manageNewProperty(),
						'floor_types'   => $user->getFloortypeList(),
						'block_types'   => $user->getBlocktypeList(),
						'villa_types'   => $user->getVillaypeList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function filter()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {	
	        	$facing = @$_GET['facing'];
				$block 	= @$_GET['block'];
				$floor 	= @$_GET['floor'];
				$room 	= @$_GET['room'];

				$block_name = $user->getBlockTypeName($block);
				$floor_name = $user->getFloorTypeName($floor);
				$room_name = $user->getRoomTypeName($room);

				$this->view('home/managepropertyfilter', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=>  COLNAME.' | Manage Property',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,

						'facing'	=> (($facing!="") ? $facing : ""),
						'block'		=> (($block!="") ? $block_name : ""),
						'floor' 	=> (($floor!="") ? $floor_name : ""),
						'room'		=> (($room!="") ? $room_name : ""),

						'list' 			=> $user->managePropertyFilter($facing,$block,$floor,$room),
						'newlist_old' 		=> $user->manageNewProperty_old(),
						'newlist' 		=> $user->manageNewProperty(),
						'floor_types'   => $user->getFloortypeList(),
						'block_types'   => $user->getBlocktypeList(),
						'villa_types'   => $user->getVillaypeList(),
						
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function chart()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("propertychart");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Property' ");	
				$this->view('home/propertychart', 
					[	
						'active_menu' 	=> 'propertychart',
						'meta_title'  	=>  COLNAME.' | Manage Property',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'newlist' 		=> $user->manageNewProperty(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function assign($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				if(!isset($_SESSION['assign_customer_key'])){
					$_SESSION['assign_customer_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(PROPERTY_TBL,"*"," id='$token' ");
					$this->view('home/assigncustomer', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=>  COLNAME.' | Assign Customer',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'customer_list' => $user->getAssignCustomerList(),
							'scripts'		=> 'home',
							'project_items'	=>	$user->selectProjectItems(),	
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				if(!isset($_SESSION['add_property_key'])){
					$_SESSION['add_property_key'] = $user->generateRandomString("40");
				}
				$this->view('home/addproperty', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=>  COLNAME.' | Add Property',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'floor_types'   => $user->getFloortypeList(),
						'block_types'   => $user->getBlocktypeList(),
						'villa_types'   => $user->getVillaypeList(),
						'document_list' => $user->getDocumentLists(),
						'area_type'   	=> $user->getAreaTypeList(),
						'scripts'		=> 'addproperty',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				if(!isset($_SESSION['edit_property_key'])){
					$_SESSION['edit_property_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(PROPERTY_TBL,"*"," id='$token' ");
					$this->view('home/editproperty', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=>  COLNAME.' | Edit Property',
							'page_title'  	=>  'Edit Property',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'floor_types'   => $user->getFloortypeList($info['floor_id']),
							'block_types'   => $user->getBlocktypeList($info['block_id']),
							'villa_types'   => $user->getVillaypeList($info['room_id']),
							'document_list' => $user->geteditDocumentLists($info['id']),
							'area_type'   	=> $user->getAreaTypeList($info['area_type_id']),
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'addproperty',
							'project_items'	=>	$user->selectProjectItems(),	
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function cancel($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				if(!isset($_SESSION['cancel_property_key'])){
					$_SESSION['cancel_property_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(PROPERTY_TBL,"*"," id='$token' ");
					$empinfo  = $user->getDetails(EMPLOYEE,"*"," id='".$_SESSION["crm_admin_id"]."' ");
					$blockinfo  = $user->getDetails(BLOCK_TBL,"*"," id='".$info['block_id']."' ");
					$floorinfo  = $user->getDetails(FLOOR_TBL,"*"," id='".$info['floor_id']."' ");
					$roominfo   = $user->getDetails(FLATTYPE_MASTER,"*"," id='".$info['room_id']."' ");
					$cusinfo 	= $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");
					
					$this->view('home/cancelproperty', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=>  COLNAME.' | Cancel Property',
							'page_title'  	=>  'Cancel Property',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'floor_types'   => $user->getFloortypeList($info['floor_id']),
							'block_types'   => $user->getBlocktypeList($info['block_id']),
							'villa_types'   => $user->getVillaypeList($info['room_id']),
							'document_list' => $user->geteditDocumentLists($info['id']),
							'area_type'   	=> $user->getAreaTypeList($info['area_type_id']),
							'info'			=> $info,
							'empinfo'		=> $empinfo,
							'blockinfo'		=> $blockinfo,
							'floorinfo'		=> $floorinfo,
							'roominfo'		=> $roominfo,
							'cusinfo' 		=> $cusinfo,
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'addproperty',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				$validate = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
				if(!isset($_SESSION['add_propertygendral_documents_key'])){
					$_SESSION['add_propertygendral_documents_key'] = $user->generateRandomString("40");
				}
				if($validate==1){
					$info  = $user->getDetails(PROPERTY_TBL,"*"," id='$token' ");
					$blockinfo  = $user->getDetails(BLOCK_TBL,"*"," id='".$info['block_id']."' ");
					$floorinfo  = $user->getDetails(FLOOR_TBL,"*"," id='".$info['floor_id']."' ");
					$roominfo   = $user->getDetails(FLATTYPE_MASTER,"*"," id='".$info['room_id']."' ");
					$cusinfo 	= $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");

					$payment_schedule 		= $user->getDetails(PAYMENT_SCHEDULE,"*"," property_id='$token' ");
					$payment_schedule_list 	= $user->getDetails(PAYMENT_SCHEDULE_LIST,"*"," payment_schedule_id='".$payment_schedule['id']."' ");
					$this->view('home/viewproperty', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=>  COLNAME.' | View Property',
							'page_title'  	=>  'View Property',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'floor_types'   => $user->getFloortypeList($info['floor_id']),
							'block_types'   => $user->getBlocktypeList($info['block_id']),
							'villa_types'   => $user->getVillaypeList($info['room_id']),
							'stage_list'    => $user->getStageList(),
							'document_list' => $user->geteditDocumentLists($info['id']),
							'payment_stats' => $user->getPaymentStats($payment_schedule['id']),
							'payment_schedule' => $payment_schedule,
							'info'			=> $info,
							'blockinfo'		=> $blockinfo,
							'floorinfo'		=> $floorinfo,
							'roominfo'		=> $roominfo,
							'cusinfo' 		=> $cusinfo,
							'room_list'		=> $user->getPropertyRoomList($info['room_id'],$info['id']),
							'images'		=> $user->getPropertyGalleryImageList($info['id']),
							'list' 			=> $user->getPropertyDocumentLists($info['id']),
							'doc_list' 		=> $user->getPropertyGeneralDocumentLists($info['id']),
							'payment_list'	=> $user->getPaymentScheduleList($payment_schedule['id'],$info['id']),
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'addproperty',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function rooms()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manageproperty");
	        if ($check==1) {
				if(!isset($_SESSION['add_propertyrooms_key'])){
					$_SESSION['add_propertyrooms_key'] = $user->generateRandomString("40");
				}
				$this->view('home/propertyrooms', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=>  COLNAME.' | Add Property',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'floor_types'   => $user->getFloortypeList(),
						'block_types'   => $user->getBlocktypeList(),
						'scripts'		=> 'addproperty',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function images($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("manageproperty");
	        if ($pcheck==1) {
				$check = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(PROPERTY_TBL,"*"," id='$token'  ");	
			    	$blockinfo  = $user->getDetails(BLOCK_TBL,"*"," id='".$info['block_id']."' ");
					$floorinfo  = $user->getDetails(FLOOR_TBL,"*"," id='".$info['floor_id']."' ");
					$roominfo   = $user->getDetails(FLATTYPE_MASTER,"*"," id='".$info['room_id']."' ");
					$cusinfo 	= $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");
					$this->view('home/propertyimages', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> COLNAME.' | Add Property Images',
							'page_title'  	=> ' Add Property Images',
							'info'			=> $info,
							'blockinfo'		=> $blockinfo,
							'floorinfo'		=> $floorinfo,
							'roominfo'		=> $roominfo,
							'cusinfo' 		=> $cusinfo,
							'token'			=> $user->encryptData($info['id']),
							'room_list'		=> $user->getPropertyRoomList($info['room_id'],$info['id']),
							'images'		=> $user->getPropertyGalleryImageList($info['id']),
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function addimages($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("manageproperty");
	        if ($pcheck==1) {
				if(!isset($_SESSION['add_propertygalleryimg_key'])){
					$_SESSION['add_propertygalleryimg_key'] = $user->generateRandomString("40");
				}
				$check = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
		        if($check){
					 $info = $user->getDetails(PROPERTY_TBL,"*"," id='$token'  ");
					$this->view('home/addpropertygalleryimages', 
						[	
							'active_menu' 	  	=> 'property',
							'meta_title'	  	=> 'Add Images - '.COMPANY_NAME,
							'page_title'  	  	=> 'Add Images',
							'redirect'		  	=> '',
							'images'			=>	$user->getPropertygalleryImages($info['id']),
							'token'			  	=>  $user->encryptData($info['id']),
							'info'			  	=>  $info,
							'scripts'		  	=> 'addimages',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		  	=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'active_menu' 	=> 'gallery',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}	

	public function addroomimg($token="",$property_id="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("manageproperty");
	        if ($pcheck==1) {
				if(!isset($_SESSION['add_propertyroomimg_key'])){
					$_SESSION['add_propertyroomimg_key'] = $user->generateRandomString("40");
				}
				$check = $user->check_query(FLATTYPE_MASTER_LIST,"id"," id='$token' ");
		        if($check){
					$roominfo = $user->getDetails(FLATTYPE_MASTER_LIST,"*"," id='$token'  ");
					$info = $user->getDetails(PROPERTY_TBL,"*"," id ='".$property_id."'  ");
					$this->view('home/addpropertyroomimages', 
						[	
							'active_menu' 	  	=> 'property',
							'meta_title'	  	=> 'Add Images - '.COMPANY_NAME,
							'page_title'  	  	=> 'Add Images',
							'redirect'		  	=> '',
							'images'			=>	$user->getPropertyRoomImages($info['id'],$roominfo['id']),
							'token'			  	=>  $user->encryptData($roominfo['id']),
							'info'			  	=>  $info,
							'roominfo'			=>  $roominfo,
							'scripts'		  	=> 'addimages',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		  	=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'active_menu' 	=> 'gallery',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}	
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}		


	public function document($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("manageproperty");
	        if ($pcheck==1) {
				$check = $user->check_query(PROPERTY_TBL,"id"," id='$token' ");
				if(!isset($_SESSION['add_propertygendral_documents_key'])){
					$_SESSION['add_propertygendral_documents_key'] = $user->generateRandomString("40");
				}
			    if($check){	
			    	$info = $user->getDetails(PROPERTY_TBL,"*"," id='$token'  ");	
			    	$blockinfo  = $user->getDetails(BLOCK_TBL,"*"," id='".$info['block_id']."' ");
					$floorinfo  = $user->getDetails(FLOOR_TBL,"*"," id='".$info['floor_id']."' ");
					$roominfo   = $user->getDetails(FLATTYPE_MASTER,"*"," id='".$info['room_id']."' ");
					$cusinfo 	= $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");
					$this->view('home/propertydocument', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> COLNAME.' | Add Property Document',
							'page_title'  	=> ' Add Property Document',
							'info'			=> $info,
							'blockinfo'		=> $blockinfo,
							'floorinfo'		=> $floorinfo,
							'roominfo'		=> $roominfo,
							'cusinfo' 		=> $cusinfo,
							'token'			=> $user->encryptData($info['id']),
							'list' 			=> $user->getPropertyDocumentLists($info['id']),
							'doc_list' 		=> $user->getPropertyGeneralDocumentLists($info['id']),
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}


	public function documentdetails($token="",$property_id="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("manageproperty");
	        if ($pcheck==1) {
				if(!isset($_SESSION['add_propertyroomimg_key'])){
					$_SESSION['add_propertyroomimg_key'] = $user->generateRandomString("40");
				}
				$check = $user->check_query(FLATVILLA_DOC_TYPE,"id"," id='$token' ");
		        if($check){
					$docinfo = $user->getDetails(FLATVILLA_DOC_TYPE,"*"," id='$token'  ");
					$info = $user->getDetails(PROPERTY_TBL,"*"," id ='".$property_id."'  ");
					$this->view('home/propertydocumentdetails', 
						[	
							'active_menu' 	  	=> 'property',
							'meta_title'	  	=> 'Add Images - '.COMPANY_NAME,
							'page_title'  	  	=> 'Add Images',
							'redirect'		  	=> '',
							'list'				=>	$user->getDocumentListItems($info['id'],$docinfo['id']),
							'token'			  	=>  $user->encryptData($docinfo['id']),
							'info'			  	=>  $info,
							'docinfo'			=>  $docinfo,
							'scripts'		  	=> 'addimages',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		  	=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'active_menu' 	=> 'gallery',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
				}	
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}	

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>