<?php

class Importdata extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("importlead");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Import Leads from excel sheet' ");
				$this->view('home/importdata', 
					[	
						'active_menu' 	=> 'import',
						'meta_title'  	=>  COLNAME.' | Import Data',
						'page_title'  	=>  'Import Data',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'layout'		=>	$user->getDataSheetList(),
						'videoinfo' 	=> $videoinfo,	
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function approve($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("importlead");
	        if ($check==1) {	
	        	$count = $user -> check_query(TEMPEXCEL_TBL,"id"," upload_token	='$token' ");
				if($count >0){
					$this->view('home/approveimport', 
						[	
							'active_menu' 	=> 'alumni_manage',
							'meta_title'  	=> 'Approve Imported Data - '.COLNAME,
							'page_title'  	=> 'Approve Imported Data',
							'scripts'		=> 'importdata',
							'layout'		=>	$user->dataImported($token),
							'count'			=>	$count,
							'token'			=>  $token,
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
				}

			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>