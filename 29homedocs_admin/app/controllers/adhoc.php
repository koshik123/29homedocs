<?php

class adhoc  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("openrequest");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage existing customer' ");	
				$this->view('home/manageadhoc', 
					[	
						'active_menu' 	=> 'adhoc',
						'meta_title'  	=>  COLNAME.' | Home',
						'page_title'  	=>  "Adhoc Open Requests",
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'request_status' => $request_status="0",
						'videoinfo' 	=> $videoinfo,
						'types'  		 => $user->getRequestTypeList(),
						'list'			=> $user->manageadhocrequests($request_status="0"),
						'emp_list' 		=> $user->getAdhocEmployeeList(),
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function all()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("allrequest");
	        if ($check==1) {
				$this->view('home/managealladhoc', 
					[	
						'active_menu' 	=> 'adhoc',
						'meta_title'  	=>  COLNAME.' | Home',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'request_status' => $request_status="0",
						'types'  		 => $user->getRequestTypeList(),
						'list'			=> $user->manageadhocAllrequests(),
						'emp_list' 		=> $user->getAdhocEmployeeList(),
						'department_list'	=>	$user->getDepartmentList(),
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function filter()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
        	$request_status 	= @$_GET['requeststatus'];
        	$requesttype 	= @$_GET['requesttype'];
			$priority 		= @$_GET['priority'];

			$assignstaus 		= @$_GET['assignstaus'];
			$assigned_to 		= @$_GET['assigned_to'];

			$requesttype_name = $user->getRequestTypeName($requesttype);

			$assignedto_name = $user->getEmployeesName($assigned_to);
			$this->view('home/manageadhocfilter', 
				[	
					'active_menu' 	=> 'adhoc',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'request_status' => $request_status,
					'requesttype'	=> (($requesttype!="") ? $requesttype_name : ""),
					'priority'		=> (($priority!="") ? $priority : ""),

					'assignstaus'		=> (($assignstaus!="") ? $assignstaus : ""),
					'assigned_to'		=> (($assigned_to!="") ? $assignedto_name : ""),


					'types'  		=> $user->getRequestTypeList(),
					'emp_list' 		=> $user->getAdhocEmployeeList(),
					'list'			=> $user->manageFilterAdhocrequests($request_status,$requesttype,$priority,$assignstaus,$assigned_to),
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
			
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function allfilter()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
        	$department_id 		= @$_GET['department_id'];
        	$status 			= @$_GET['status'];
			$assignstaus 		= @$_GET['assignstaus'];
			$assigned_to 		= @$_GET['assigned_to'];

			$department_name = $user->getDepartmentName($department_id);
			$assignedto_name = $user->getEmployeesName($assigned_to);
			$this->view('home/managealladhocfilter', 
				[	
					'active_menu' 	=> 'adhoc',
					'meta_title'  	=>  COLNAME.' | Home',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,

					'department_name'	=> (($department_id!="") ? $department_name : ""),
					'status'			=> (($status!="") ? $status : ""),
					'assignstaus'		=> (($assignstaus!="") ? $assignstaus : ""),
					'assigned_to'		=> (($assigned_to!="") ? $assignedto_name : ""),

					'types'  		 	=> $user->getRequestTypeList(),
					'list'				=> $user->manageadhocAllrequests(),
					'emp_list' 			=> $user->getAdhocEmployeeList(),
					'department_list'	=> $user->getDepartmentList(),

					'list'				=> $user->manageAllFilterAdhocrequests($department_id,$status,$assignstaus,$assigned_to),
					'scripts'			=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
			
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
        	if(!isset($_SESSION['add_request_key'])){
				$_SESSION['add_request_key'] = $user->generateRandomString("40");
			}
			$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Add a new Adhoc request' ");	
			$this->view('home/addadhocrequest', 
				[	
					'active_menu' 	=> 'adhoc',
					'meta_title'  	=>  COLNAME.' | Add Adhoc Request',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'customer_list'  => $user->getCustomerList(),
					'types'  		 => $user->getRequestTypeList(),
					'videoinfo' 	=> $videoinfo,
					'scripts'		=> 'addadhoc',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function closed()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("close_request");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Closed Requests' ");	
				$this->view('home/manageadhoc', 
					[	
						'active_menu' 	=> 'adhoc',
						'meta_title'  	=>  COLNAME.' | Home',
						'page_title'  	=>  "Adhoc Closed Requests",
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'request_status' => $request_status="1",
						'videoinfo' 	=> $videoinfo,
						'types'  		 => $user->getRequestTypeList(),
						'list'			=> $user->manageadhocrequests($request_status="1"),
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}


	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['reply_ticket_key'])){
				$_SESSION['reply_ticket_key'] = $user->generateRandomString("40");
			}	
			/*$pcheck = $user->pagePermission("adhocrequest");
	        if ($pcheck==1) {*/
				$check = $user->check_query(ADOC_REQUEST,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(ADOC_REQUEST,"*"," id='$token'  ");	

			    	$assign_info = $user->adhocAssignTo($info['id']);
                	$assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );

					$this->view('home/adhocdetails', 
						[	
							'active_menu' 	=> 'adhoc',
							'meta_title'  	=>  COLNAME.' | Adhoc requests',
							'page_title'  	=>  'Adhoc requests',
							'info'			=>	$info,
							'assigned_to'   =>  $assigned_to,
							'token'			=>	$user->encryptData($info['id']),
							'list'			=>  $user->getAdhocInfo($info['id']),	
							'replay_list'	=>  $user->getReplyAdhocInfo($info['id']),	
							'emp_list' 		=> $user->getAdhocEmployeeList(),
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'adhoc',
							'project_items'	=>	$user->selectProjectItems(),	
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
			/*}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}*/
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'adhoc',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>