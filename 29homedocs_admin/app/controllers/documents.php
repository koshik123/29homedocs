<?php

class documents extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("generaldocuments");
	        if ($check==1) {			
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='General Documents for all units Master' ");	
				$this->view('home/managedocuments', 
					[	
						'active_menu' 	=> 'documents',
						'meta_title'  	=>  COLNAME.' | General Documents for all units Master',
						'page_title'  	=>  'General Documents for all units Master',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'list' 			=> $user->manageGeneralDocuments(),
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("generaldocuments");
	        if ($check==1) {	
				if(!isset($_SESSION['add_gendral_documents_key'])){
					$_SESSION['add_gendral_documents_key'] = $user->generateRandomString("40");
				}	
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Property' ");	
				$this->view('home/adddocuments', 
					[	
						'active_menu' 	=> 'documents',
						'meta_title'  	=>  COLNAME.' | Add General Documents',
						'page_title'  	=>  'Add General Documents',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("generaldocuments");
	        if ($check==1) {	
				if(!isset($_SESSION['edit_gendral_documents_key'])){
					$_SESSION['edit_gendral_documents_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(GENDRAL_DOCUMENTS,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(GENDRAL_DOCUMENTS,"*"," id='$token' ");
					$this->view('home/editdocuments', 
						[	
							'active_menu' 	=> 'documents',
							'meta_title'  	=>  COLNAME.' | Edit General Documents',
							'page_title'  	=>  'Edit General Documents',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);

				}else{
				$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("generaldocuments");
	        if ($check==1) {	
				$validate = $user->check_query(GENDRAL_DOCUMENTS,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(GENDRAL_DOCUMENTS,"*"," id='$token' ");
					$this->view('home/viewgeneraldocuments', 
						[	
							'active_menu' 	=> 'documents',
							'meta_title'  	=>  COLNAME.' | View General Documents',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function project()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("documentchecklist");
	        if ($check==1) {	
	       		$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Private Document list for every unit Master' ");		
				$this->view('home/manageprojectdocuments', 
					[	
						'active_menu' 	=> 'flatdocuments',
						'meta_title'  	=>  COLNAME.' | Private Document list for every unit',
						'page_title'  	=>  'Private Document list for every unit',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageFlatVillaDocumentsType(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}


	public function addproject()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("documentchecklist");
	        if ($check==1) {	
				if(!isset($_SESSION['add_fatvilla_document_key'])){
					$_SESSION['add_fatvilla_document_key'] = $user->generateRandomString("40");
				}			
				$this->view('home/addprojectdocuments', 
					[	
						'active_menu' 	=> 'flatdocuments',
						'meta_title'  	=>  COLNAME.' | Add Private Documents ',
						'page_title'  	=>  'Add Private Documents ',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function editproject($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("documentchecklist");
	        if ($check==1) {	
				if(!isset($_SESSION['edit_fatvilla_document_key'])){
					$_SESSION['edit_fatvilla_document_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(FLATVILLA_DOC_TYPE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(FLATVILLA_DOC_TYPE,"*"," id='$token' ");
					$this->view('home/editprojectdocuments', 
						[	
							'active_menu' 	=> 'flatdocuments',
							'meta_title'  	=>  COLNAME.' | Edit Private Documents ',
							'page_title'  	=>  'Edit Private Document',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'documents' 	=> $user->getDocCheckList($info['id'],$info['publish_status']),
							'token'			=> $user->encryptData($info['id']),
							'scripts'		=> 'home',
							'project_items'	=>	$user->selectProjectItems(),	
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function projectdetails($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("documentchecklist");
	        if ($check==1) {	
				$validate = $user->check_query(FLATVILLA_DOC_TYPE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(FLATVILLA_DOC_TYPE,"*"," id='$token' ");
					$this->view('home/viewchecklistdocuments', 
						[	
							'active_menu' 	=> 'flatdocuments',
							'meta_title'  	=>  COLNAME.' | View Private Documents',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'documents' 	=> $user->DocCheckList($info['id']),
							'info'			=> $info,
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	/*public function addproject()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_fatvilla_document_key'])){
				$_SESSION['add_fatvilla_document_key'] = $user->generateRandomString("40");
			}	
			$this->view('home/addprojectdocuments', 
				[	
					'active_menu' 	=> 'flatdocuments',
					'meta_title'  	=>  COLNAME.' | Add Documents',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'scripts'		=> 'home',	
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}*/
	
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>