<?php

class Employee extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employee");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Employee' ");	
				$this->view('home/manageemployee', 
					[	
						'active_menu' 	=> 'employee',
						'meta_title'  	=>  COLNAME.' | Manage Employee',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'list' 			=> $user->manageEmployee(),
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employee");
	        if ($check==1) {
				if(!isset($_SESSION['add_employee_key'])){
					$_SESSION['add_employee_key'] = $user->generateRandomString("40");
				}	
				$this->view('home/addemployee', 
					[	
						'active_menu' 	=> 'addemployee',
						'meta_title'  	=>  COLNAME.' | Add Employee',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'emp_role'			=>	$user->getEmployeeRole(),
						'department_list'	=>	$user->getDepartmentList(),
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employee");
	        if ($check==1) {
				if(!isset($_SESSION['edit_employee_key'])){
						$_SESSION['edit_employee_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					$this->view('home/editemployee', 
						[	
							'active_menu' 		=> 'addemployee',
							'meta_title'  		=>  COLNAME.' | Edit Employee',
							'page_title'  		=>  'Edit Employee',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'emp_role'			=>	$user->getEmployeeRole($info['role_id']),
							'department_list'	=>	$user->getDepartmentList($info['department_id']),
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'scripts'			=> 'employee',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function details($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employee");
	        if ($check==1) {
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					$roleinfo  = $user->getDetails(EMPLOYEE_ROLE,"*"," id='".$info['role_id']."' ");
					$this->view('home/viewemployee', 
						[	
							'active_menu' 		=> 'addemployee',
							'meta_title'  		=>  COLNAME.' | View Employee',
							'page_title'  		=>  'View Employee',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'roleinfo' 			=>  $roleinfo,
							'scripts'			=> 'employee',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function permission($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("employee");
	        if ($check==1) {
				if(!isset($_SESSION['create_permission_key'])){
						$_SESSION['create_permission_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(EMPLOYEE,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(EMPLOYEE,"*"," id='$token' ");
					//$role  = $user->getDetails(EMPLOYEE_TYPE,"employee_type"," id='".$info['em_type']."' ");
					$employee_permission = $user->getDetails(PERMISSION,"*"," employee_id = '".$info['id']."'  ");
					$roleinfo  = $user->getDetails(EMPLOYEE_ROLE,"*"," id='".$info['role_id']."' ");
					$this->view('home/employeepermission', 
						[	
							'active_menu' 		=> 'employee',
							'meta_title'  		=> 'Manage Permission',
							'meta_keywords'  	=> 'Manage Permission',
							'meta_description'  => 'Manage Permission',
							'page_title'  		=> 'Manage Permission',
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'roleinfo' 			=>  $roleinfo,
							'emp_info'			=>  $employee_permission,
						//	'role' 				=>  $role['employee_type'],
							'scripts'			=> 'employee',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}
	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>