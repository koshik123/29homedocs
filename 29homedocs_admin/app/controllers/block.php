<?php

class Block extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			if(!isset($_SESSION['add_block_key'])){
				$_SESSION['add_block_key'] = $user->generateRandomString("40");
			}	
			$check = $user->pagePermission("block");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Block Settings Master' ");	
				$this->view('home/manageblock', 
					[	
						'active_menu' 	=> 'block',
						'meta_title'  	=>  COLNAME.' | Manage Block',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,
						'list' 			=> $user->manageBlock(),
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("block");
	        if ($check==1) {	
				$this->view('home/addblock', 
					[	
						'active_menu' 	=> 'block',
						'meta_title'  	=>  COLNAME.' | Add Block',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
	
}


?>