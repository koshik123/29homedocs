<?php

class  Settings  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("manage_department");
	        if ($check==1) {
				if(!isset($_SESSION['add_request_type_key'])){
					$_SESSION['add_request_type_key'] = $user->generateRandomString("40");
				}	
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Departments' ");	
				$this->view('home/managerequesttypemaster ', 
					[	
						'active_menu' 	=> 'requesttype',
						'meta_title'  	=>  COLNAME.' | Manage Request type Master',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageRequestType(),
						'department_list'=>	$user->getDepartmentList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function kyctype()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("kycdoctype");
		    if ($check==1) {
				if(!isset($_SESSION['add_kyc_type_key'])){
					$_SESSION['add_kyc_type_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='KYC Document Types Master' ");		
				$this->view('home/managekyctype', 
					[	
						'active_menu' 	=> 'kyctype',
						'meta_title'  	=>  COLNAME.' | Manage KYC type Master',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->managekycType(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function project()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['project_master_key'])){
				$_SESSION['project_master_key'] = $user->generateRandomString("40");
			}	
			$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Project Master' ");	
			$this->view('home/manageprojectmaster', 
				[	
					'active_menu' 	=> 'project',
					'meta_title'  	=>  COLNAME.' | Project Master',
					'page_title'  	=>  'Project Master',
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'list' 			=> $user->manageProjectType(),
					'videoinfo' 	=> $videoinfo,
					'scripts'		=> 'home',	
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function virtualtour()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("virtualtour");
	        if ($check==1) {
				if(!isset($_SESSION['add_virtual_key'])){
					$_SESSION['add_virtual_key'] = $user->generateRandomString("40");
				}
				$info_details = $user->getDetails(MENU_SETTINGS,"*"," 1 ");
				$info = $user->editPagePublish($info_details);	
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='360 Virtual Tour Settings' ");	
				$this->view('home/managevirtualtour', 
					[	
						'active_menu' 	=> 'virtual',
						'meta_title'  	=>  COLNAME.' | Manage KYC type Master',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->managekycType(),
						'videoinfo' 	=> $videoinfo,
						'info' 			=> $info,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}


	public function notification()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("notification");
	        if ($check==1) {
				if(!isset($_SESSION['notification_email_key'])){
					$_SESSION['notification_email_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Open Requests' ");		
				$this->view('home/notificationemail', 
					[	
						'active_menu' 	=> 'notification',
						'meta_title'  	=>  COLNAME.' | Add Notification Email list',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageNotificationEmail(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
	
	public function role()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("role_master");
	        if ($check==1) {
				if(!isset($_SESSION['add_role_key'])){
					$_SESSION['add_role_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Employee Role Master' ");		
				$this->view('home/managerolemaster ', 
					[	
						'active_menu' 	=> 'role',
						'meta_title'  	=>  COLNAME.' | Manage Employee Role',
						'page_title'  	=>  'Manage Employee Role',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageEmployeeRole(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}	

	public function stage()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("role_master");
	        if ($check==1) {
				if(!isset($_SESSION['add_stage_key'])){
					$_SESSION['add_stage_key'] = $user->generateRandomString("40");
				}	
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Payment Stage Master' ");	
				$this->view('home/managestagemaster ', 
					[	
						'active_menu' 	=> 'role',
						'meta_title'  	=>  COLNAME.' | Payment Stage Master',
						'page_title'  	=>  'Payment Stage Master',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->managePaymentStageMaster(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}	


	public function helpdocs()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("upload_video");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Upload Videos' ");	
				$this->view('home/managehelpdocs', 
					[	
						'active_menu' 	=> 'helpdocs',
						'meta_title'  	=>  COLNAME.' | Manage Videos',
						'page_title'  	=>  'Manage Videos',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->managePageVideos(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function upload($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("upload_video");
	        if ($pcheck==1) {
				if(!isset($_SESSION['edit_helpvideo_key'])){
					$_SESSION['edit_helpvideo_key'] = $user->generateRandomString("40");
				}	
				$check = $user->check_query(UPLOAD_PAGE_VIDEO,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," id='$token'  ");	
			    	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage Open Requests' ");	
					$this->view('home/uploadhelpdocsvideos', 
						[	
							'active_menu' 	=> 'upload',
							'meta_title'  	=>  COLNAME.' | Upload Videos',
							'page_title'  	=>  'Upload Videos',
							'info'			=>	$info,
							'token'			=>	$user->encryptData($info['id']),
							'videoinfo' 	=> $videoinfo,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}


	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
		$user = $this->model('User');
		$this->view('home/error', 
			[	
				'active_menu' 	=> 'home',
				'meta_title'  	=> '404 Error - Page Not Found',
				'scripts'		=> 'error',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>