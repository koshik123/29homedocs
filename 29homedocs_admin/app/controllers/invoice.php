<?php

class Invoice extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("invoice");
	        if ($check==1) {	
	       		$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage existing Invoices / Receipts' ");		
				$this->view('home/manageinvoice', 
					[	
						'active_menu' 	=> 'invoice',
						'meta_title'  	=>  COLNAME.' | manageinvoice',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageInvoice(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("invoice");
	        if ($check==1) {
				if(!isset($_SESSION['add_invoice_key'])){
					$_SESSION['add_invoice_key'] = $user->generateRandomString("40");
				}		
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='KYC Documents Management' ");		
				$this->view('home/addinvoice', 
					[	
						'active_menu' 	=> 'invoice',
						'meta_title'  	=>  COLNAME.' | Add Invoice',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'customer' 		=> $user->getCustomerList(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("invoice");
	        if ($check==1) {
				if(!isset($_SESSION['edit_invoice_key'])){
					$_SESSION['edit_invoice_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(INVOICE_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(INVOICE_TBL,"*"," id='$token' ");
					$this->view('home/editinvoice', 
						[	
							'active_menu' 	=> 'invoice',
							'meta_title'  	=>  COLNAME.' | Edit Invoice',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'customer' 		=> $user->getCustomerList($info['customer_id']),
						//	'property_list' => $user->getPropertyList(),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'invoice',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("invoice");
	        if ($pcheck==1) {
				$check = $user->check_query(INVOICE_TBL,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(INVOICE_TBL,"*"," id='$token'  ");	
			    	$cusinfo 	= $user->getDetails(CUSTOMER_TBL,"*"," id='".$info['customer_id']."'  ");
			    	$propertyinfo 	= $user->getDetails(PROPERTY_TBL,"*"," id='".$cusinfo['property_id']."'  ");
					$this->view('home/invoicedetails', 
					[	
						'active_menu' 	=> 'invoice',
						'active_submenu'=> '',
						'info' 			=>  $info,
						'cusinfo' 		=> $cusinfo,
						'propertyinfo' 		=> $propertyinfo,
						'meta_title'  	=>  COLNAME.' | Manage Bank Invoice',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
				}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'gallery',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"]),
					]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Member Login - '.COMPANY_NAME
				]);
		}
	}

	public function printinvoice()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("invoice");
	        if ($check==1) {	
				$this->view('home/printinvoice', 
					[	
						'active_menu' 	=> 'invoice',
						'meta_title'  	=>  COLNAME.' | manageinvoice',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
		
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'invoice',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>