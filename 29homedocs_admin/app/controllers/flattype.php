<?php

class Flattype extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("flattype");
	        if ($check==1) {	
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Flat Type settings Master' ");	
				$this->view('home/manageflattype', 
					[	
						'active_menu' 	=> 'flattype',
						'meta_title'  	=>  COLNAME.' | Flat Type Settings',
						'page_title'  	=>  'Flat Type Settings',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageFlatTypeMaster(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("flattype");
	        if ($check==1) {
				if(!isset($_SESSION['add_flattype_key'])){
					$_SESSION['add_flattype_key'] = $user->generateRandomString("40");
				}			
				$this->view('home/addflattype', 
					[	
						'active_menu' 	=> 'flattype',
						'meta_title'  	=>  COLNAME.' | Add Flat Type Settings',
						'page_title'  	=>  'Add Flat Type Settings',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'addflattype',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("flattype");
	        if ($pcheck==1) {
				if(!isset($_SESSION['edit_flattype_key'])){
					$_SESSION['edit_flattype_key'] = $user->generateRandomString("40");
				}	
				$check = $user->check_query(FLATTYPE_MASTER,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(FLATTYPE_MASTER,"*"," id='$token'  ");	
					$this->view('home/editflattype', 
						[	
							'active_menu' 	=> 'flattype',
							'meta_title'  	=>  COLNAME.' | Edit Flat Type Master',
							'page_title'  	=>  'Edit Flat Type Master',
							'info'			=>	$info,
							'token'			=>	$user->encryptData($info['id']),
							'type_list'		=>  $user->getFlatTypeMasterList($info['id']),	
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$pcheck = $user->pagePermission("flattype");
	        if ($pcheck==1) {
				$check = $user->check_query(FLATTYPE_MASTER,"id"," id='$token' ");
			    if($check){	
			    	$info = $user->getDetails(FLATTYPE_MASTER,"*"," id='$token'  ");	
					$this->view('home/viewflattype', 
						[	
							'active_menu' 	=> 'flattype',
							'meta_title'  	=>  COLNAME.' | View Flat Type Master',
							'page_title'  	=>  'View Flat Type Master',
							'info'			=>	$info,
							'type_list'		=>  $user->flatTypeMasterList($info['id']),	
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'scripts'		=> 'addflattype',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'active_menu' 	=> 'property',
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);	
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>