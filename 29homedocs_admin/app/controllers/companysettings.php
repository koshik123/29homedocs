<?php

class Companysettings extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("company");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Company Settings' ");
				$this->view('home/managecompanysettings', 
					[	
						'active_menu' 	=> 'companysettings',
						'meta_title'  	=>  COLNAME.' | Manage Company Settings',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageCompanySettings(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),		
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			if(!isset($_SESSION['add_contactinfo_key'])){
				$_SESSION['add_contactinfo_key'] = $user->generateRandomString("40");
			}
			$check = $user->pagePermission("company");
	        if ($check==1) {	
				$this->view('home/addcontactinfo', 
					[	
						'active_menu' 	=> 'companysettings',
						'meta_title'  	=>  COLNAME.' | Add Contact Information',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'scripts'		=> 'addcontactinfo',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("company");
	        if ($check==1) {
				if(!isset($_SESSION['edit_companysettings_key'])){
					$_SESSION['edit_companysettings_key'] = $user->generateRandomString("40");
				}
				$validate = $user->check_query(COMPANY_SETTINGS,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(COMPANY_SETTINGS,"*"," id='$token' ");
					$this->view('home/editcompanysettings', 
						[	
							'active_menu' 		=> 'companysettings',
							'meta_title'  		=>  COLNAME.' | Edit Company Settings',
							'page_title'  		=>  'Edit Company Settings',
							'meta_keywords' 	=>  META_KEYWORDS,
							'meta_description'  =>  META_DESCRIPTION,
							'token'				=>	$user->encryptData($info['id']),
							'info'				=>  $info,
							'scripts'			=> 'addcontactinfo',
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	public function details($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$validate = $user->check_query(CONTACT_INFO,"id"," id='$token' ");
			if($validate==1){
				$info  = $user->getDetails(CONTACT_INFO,"*"," id='$token' ");
				$this->view('home/viewcontactinfo', 
					[	
						'active_menu' 		=> 'companysettings',
						'meta_title'  		=>  COLNAME.' | View Contact Information',
						'page_title'  		=>  'View Contact Information',
						'meta_keywords' 	=>  META_KEYWORDS,
						'meta_description'  =>  META_DESCRIPTION,
						'info'				=>  $info,
						'scripts'			=> 'addcontactinfo',
						'project_items'		=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
					]);
			}else{
				$this->view('home/error', 
					[	
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);
				}
						
		}else{
			$this->view('home/login',
				array(
					'meta_title'=> 'User Login - '.COMPANY_NAME
				));
		}		
	}

	
	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$this->view('home/error', 
				[	
					'active_menu' 	=> 'home',
					'meta_title'  	=> '404 Error - Page Not Found',
					'scripts'		=> 'error',
					'page_title'  	=>  COLNAME,
					'meta_keywords' => META_KEYWORDS,
					'meta_description' => META_DESCRIPTION,
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					
				]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}
}


?>