<?php

class  Lead  extends Controller
{
	
	public function index()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
	        	$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Manage existing Leads' ");
				$this->view('home/managelead ', 
					[	
						'active_menu' 	=> 'lead',
						'meta_title'  	=>  COLNAME.' | Manage Lead',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageLead(),
						'activity_types' => $user->getActivityType(),
						'emp_list' 		=> $user->getEmployeeList(),
						'videoinfo' 	=> $videoinfo,	
						'source_list'  	=> $user->getLeadSource(),
						'leadstatus'	=> $user->getLeadStatus(),
						'scripts'		=> 'managelead',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function filter()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
	        	$leadsource 	= @$_GET['leadsource'];
				$movecustomer 	= @$_GET['movecustomer'];
				$assignedstatus = @$_GET['assignedstatus'];
				$assignedto 	= @$_GET['assignedto'];
				$leadstatus 	= @$_GET['leadstatus'];

				$leadstatus_name = $user->getLeadStatusName($leadstatus);
				$assignedto_name = $user->getEmployeesName($assignedto);

				$this->view('home/manageleadfilter ', 
					[	
						'active_menu' 	=> 'lead',
						'meta_title'  	=>  COLNAME.' | Manage Lead',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,

						'leadsource'	=> (($leadsource!="") ? $leadsource : ""),
						'movecustomer'	=> (($movecustomer!="") ? $movecustomer : ""),
						'assignedstatus'=> (($assignedstatus!="") ? $assignedstatus : ""),
						'assignedto'	=> (($assignedto!="") ? $assignedto_name : ""),
						'lead_status'	=> (($leadstatus!="") ? $leadstatus_name : ""),

						'list' 			=> $user->manageFilterLead($leadsource,$movecustomer,$assignedstatus,$assignedto,$leadstatus),
						'activity_types' => $user->getActivityType(),
						'emp_list' 		=> $user->getEmployeeList(),

						'source_list'  	=> $user->getLeadSource(),
						'leadstatus'	=> $user->getLeadStatus(),
						'scripts'		=> 'managelead',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function add()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
				if(!isset($_SESSION['add_lead_key'])){
					$_SESSION['add_lead_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Add a new Lead' ");
				$this->view('home/addlead', 
					[	
						'active_menu' 	=> 'lead',
						'meta_title'  	=>  COLNAME.' | Add Lead',
						'page_title'  	=>  COLNAME,
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'videoinfo' 	=> $videoinfo,	
						'project_list'  => $user->getProjectList(),
						'source_list'  	=> $user->getLeadSource(),
						'flat_type'  	=> $user->getVillaypeList(),
						'customerprofile_list' => $user->getCustomerProfileList(),
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
					]);
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function edit($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
				if(!isset($_SESSION['edit_lead_key'])){
					$_SESSION['edit_lead_key'] = $user->generateRandomString("40");
				}	
				$validate = $user->check_query(LEAD_TBL,"id"," id='$token' ");
				if($validate==1){
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					$this->view('home/editlead', 
						[	
							'active_menu' 	=> 'lead',
							'meta_title'  	=>  COLNAME.' | Edit Lead',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info'			=> $info,
							'token'			=> $user->encryptData($info['id']),
							'source_list'  	=> $user->getLeadSource($info['leadsource']),
							'flat_type'  	=> $user->getVillaypeList($info['flattype_id']),
							'customerprofile_list' => $user->getCustomerProfileList($info['customer_profile_id']),
						//	'project_list'  => $user->getProjectList($info['project_id']),
						//	'property_list' => $user->getPropertyList(),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function assignlead($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->check_query(LEAD_TBL,"id"," id='$token' ");
			if ($check==1) {
				$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
				//$assign_employee    = $user->getAssignedEmployeeInfo($token);
 				if(!isset($_SESSION['add_assign_lead_key'])){
					$_SESSION['add_assign_lead_key'] = $user->generateRandomString("40");
				}
				$this->view('home/assignlead',
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=> 'Assigned Lead',
						'page_title'  		=> 'Assigned Lead',
						'meta_keywords'  	=> 'Assigned Lead',
						'meta_description'  => 'Assigned Lead',
						'scripts'			=> 'assigncalls',
					//	'employee_list'		=>  $user->getEmployeeList(),
					//	'employee_list'		=>  $user->getEmployeeList($assign_employee['created_to']),
						'info'				=>  $info,
					//	'call_info'			=>  $call_info,
					//	'stats'				=>  $user->dashboardStats(),
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
					]);
			}else{
				$this->view('home/error', 
				[	
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),
				]);
			}			
		}else{
		$this->view('home/login',
			array(
				'meta_title'=> 'User Login - '.COMPANY_NAME
			));
		}
	}

	public function reassign($token="")
	{	
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->check_query(LEAD_TBL,"id"," id='$token' ");
			if ($check==1) {
				$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
				//$call_info = $user->getCallInfo($token);
				$assign_employee    = $user->getAssignedEmployeeInfo($token);				
 				if(!isset($_SESSION['add_reassign_lead_key'])){
					$_SESSION['add_reassign_lead_key'] = $user->generateRandomString("40");
				}
				$this->view('home/reassignlead',
					[	
						'active_menu' 		=> 'lead',
						'meta_title'  		=> 'Reassign Lead',
						'meta_keywords'  	=> 'Reassign Lead',
						'meta_description'  => 'Reassign Lead',
						'page_title'  		=> 'Reassign Lead',
						'scripts'			=> 'assigncalls',
					//	'employee_list'		=>  $user->getEmployeeList($assign_employee['created_to']),
						'info'				=>  $info,
					//	'call_info'			=>  $call_info,
						'assign_emp'		=>  $assign_employee,
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 			=> $user->userInfo($_SESSION["crm_admin_id"]),
					]);
			}else{
				$this->view('home/error', 
				[	
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),
				]);
			}			
		}else{
		$this->view('home/login',
			array(
				'meta_title'=> 'User Login - '.COMPANY_NAME
			));
		}
	}

	public function details($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');	
			$pcheck = $user->pagePermission("managelead");
	        if ($pcheck==1) {
				$validate = $user->check_query(LEAD_TBL,"id"," id='$token' ");
				if($validate==1){
					if(!isset($_SESSION['update_leadstatus_key'])){
					$_SESSION['update_leadstatus_key'] = $user->generateRandomString("40");
				}	
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					//$projectinfo  = $user->getDetails(PROJECT_TYPE,"*"," id='".$info['project_id']."' ");
					$leadstatusinfo  = $user->getDetails(LEAD_TYPE,"*"," id='".$info['lead_status_id']."' ");
					$leadSourceinfo  = $user->getDetails(LEAD_SOURCE,"*"," id='".$info['leadsource']."' ");

					$assign_btn = (($info['assign_status']=="0")? "
                	
                		<a href='#' class='btn btn-info assignLeadModel' data-value='".$info['id']."' data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-check'></em> Assign</a>" : "<a href='#' class='btn btn-blue reassignLeadModel' data-value='".$info['id']."' data-type='".$info['id']."' data-option='".$info['id']."'><em class='icon ni ni-check'></em> Reassign</a>" );

                	$assign_info = $user->leadAssignTo($info['id']);
                	$assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> " : "<span class='badge badge-warning'>Not Assigned</span>" );

					$this->view('home/viewlead', 
						[	
							'active_menu' 	=> 'lead',
							'meta_title'  	=>  COLNAME.' | View Lead',
							'page_title'  	=>  'View Lead',
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'leadstatus'	=> $user->getLeadStatus($info['lead_status_id']),	
							'activity_types' => $user->getActivityType(),
							'token'			=> $user->encryptData($info['id']),
							'info'			=> $info,
						//	'projectinfo' 	=> $projectinfo,
							'assign_btn'   => $assign_btn,
							'assigned_to'  => $assigned_to,
							'leadstatusinfo'=> $leadstatusinfo,
							'leadSourceinfo'=> $leadSourceinfo,
						//	'activity_logs'	=> $user->manageActivityLogs($token),
							'newactivity_logs'	=> $user->manageNewActivityLogs($token),
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
						]);
				}else{
					$this->view('home/error', 
						[	
							'meta_title'  	=> '404 Error - Page Not Found',
							'page_title'  	=> '404 Error - Page Not Found',
							'project_items'	=>	$user->selectProjectItems(),
							'user'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
						]);
					}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function addactivity($token="")
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("managelead");
	        if ($check==1) {
    			$checkl = $user->check_query(LEAD_TBL,"id"," id='$token' ");
				if ($checkl==1) {
					$info  = $user->getDetails(LEAD_TBL,"*"," id='$token' ");
					if(!isset($_SESSION['add_activity_key'])){
						$_SESSION['add_activity_key'] = $user->generateRandomString("40");
					}
					$this->view('home/addactivitylead', 
						[	
							'active_menu' 	=> 'lead',
							'meta_title'  	=>  COLNAME.' | Add Lead',
							'page_title'  	=>  COLNAME,
							'meta_keywords' => META_KEYWORDS,
							'meta_description' => META_DESCRIPTION,
							'info' 			 => $info,
							'activity_types' => $user->getActivityType(),
							
							'scripts'		=> 'home',	
							'project_items'	=>	$user->selectProjectItems(),
							'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),			
						]);
				}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
			}else{
				$this->view('home/error', 
				[	
					'active_menu' 	=> 'property',
					'meta_title'  	=> '404 Error - Page Not Found',
					'page_title'  	=> '404 Error - Page Not Found',
					'project_items'	=>	$user->selectProjectItems(),
					'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
				]);	
			}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}
	}

	public function activetype()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("activitytype");
		    if ($check==1) {
				if(!isset($_SESSION['activity_type_key'])){
					$_SESSION['activity_type_key'] = $user->generateRandomString("40");
				}	
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Activity Type Master' ");	
				$this->view('home/manageactivitytype', 
					[	
						'active_menu' 	=> 'activitytype',
						'meta_title'  	=>  COLNAME.' | Activity Type Master',
						'page_title'  	=>  'Activity Type Master',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageActivityType(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadtype()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadtype");
		    if ($check==1) {
				if(!isset($_SESSION['lead_type_key'])){
					$_SESSION['lead_type_key'] = $user->generateRandomString("40");
				}	
				$defaultcheck = $user->check_query(LEAD_TYPE,"id"," default_settings='1' ");
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Lead Status Master' ");	
				$this->view('home/manageleadtype', 
					[	
						'active_menu' 	=> 'leadtype',
						'meta_title'  	=>  COLNAME.' | Manage Lead Status Message',
						'page_title'  	=>  'Manage Lead Status Message',
						'meta_keywords' => META_KEYWORDS,
						'defaultcheck'  => $defaultcheck,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageLeadType(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function leadsource()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("leadsource");
		    if ($check==1) {
				if(!isset($_SESSION['lead_source_key'])){
					$_SESSION['lead_source_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Lead Source Master' ");		
				$this->view('home/manageleadsource', 
					[	
						'active_menu' 	=> 'leadsource',
						'meta_title'  	=>  COLNAME.' | Manage Lead Source Master',
						'page_title'  	=>  'Manage Lead Source Master',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageLeadSource(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',
						'project_items'	=>	$user->selectProjectItems(),	
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}

	public function customerprofile()
	{
		if(isset($_SESSION["crm_admin_id"])){
			$user = $this->model('User');
			$check = $user->pagePermission("profilemaster");
		    if ($check==1) {
				if(!isset($_SESSION['customer_profile_key'])){
					$_SESSION['customer_profile_key'] = $user->generateRandomString("40");
				}
				$videoinfo  = $user->getDetails(UPLOAD_PAGE_VIDEO,"*"," type='Lead Profile Master' ");		
				$this->view('home/managecustomerprofile', 
					[	
						'active_menu' 	=> 'customerprofile',
						'meta_title'  	=>  COLNAME.' | Manage Lead Profile',
						'page_title'  	=>  'Manage Lead Profile',
						'meta_keywords' => META_KEYWORDS,
						'meta_description' => META_DESCRIPTION,
						'list' 			=> $user->manageCustomerProfile(),
						'videoinfo' 	=> $videoinfo,
						'scripts'		=> 'home',	
						'project_items'	=>	$user->selectProjectItems(),
						'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
					]);
			}else{
					$this->view('home/error', 
					[	
						'active_menu' 	=> 'property',
						'meta_title'  	=> '404 Error - Page Not Found',
						'page_title'  	=> '404 Error - Page Not Found',
						'project_items'	=>	$user->selectProjectItems(),
						'member'   		=>  $user->userInfo($_SESSION["crm_admin_id"])
					]);	
				}
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}


	public function error()
	{	
		if(isset($_SESSION["crm_admin_id"])){
		$user = $this->model('User');
		$this->view('home/error', 
			[	
				'active_menu' 	=> 'home',
				'meta_title'  	=> '404 Error - Page Not Found',
				'scripts'		=> 'error',
				'page_title'  	=>  COLNAME,
				'meta_keywords' => META_KEYWORDS,
				'meta_description' => META_DESCRIPTION,
				'project_items'	=>	$user->selectProjectItems(),
				'user' 	 		=> $user->userInfo($_SESSION["crm_admin_id"]),	
			]);
		}else{
			$this->view('home/login',
				[	
					'meta_title'=> 'Admin Login - '.COMPANY_NAME
				]);
		}	
	}
}


?>