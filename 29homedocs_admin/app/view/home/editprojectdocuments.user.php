<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>documents/project">Private Document list for every unit</a></li>
                                <li class="breadcrumb-item active">Edit Private Document</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <form id="editFlatVillaDocumnt" name="editFlatVillaDocumnt" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['edit_fatvilla_document_key'] ?>" name="fkey" id="fkey">
                        <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                        <div class="nk-block nk-block-lg">
                            <div class="nk-block-head">
                                <div class="form-error"></div><br>
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Edit Private Document</h4>
                                </div>
                            </div>
                            <div class="row g-gs">
                                <div class="col-lg-5">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="form-group">
                                                <label class="form-label" for="cf-full-name">Document Title <en>*</en></label>
                                                <input type="text" class="form-control" id="" name="document_title" value="<?php echo $data['info']['doc_title'] ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="cf-full-name">Sort Order <en>*</en></label>
                                                <input type="number" class="form-control" id="" name="sort_order" value="<?php echo $data['info']['sort_order'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 h-100 minheight">
                                    <div class="form-group">
                                        <div class="card panel panel-default panel-table">
                                            <h5 class="table-header">
                                                Add Documents type
                                            </h5>
                                            <?php if ($data['info']['publish_status']==0): ?>
                                            <div class="add_btn">
                                                <button type="button" onclick="addDocumentsName();" title="Add" class="btn btn-success btn-sm">Add </button>
                                            </div>
                                            <?php endif ?>
                                            <div class="card-block">
                                                <table class="table table-bordered valign table_custom" id="documents">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%"> Document Name</th>
                                                            <th width="15%"> Description</th>
                                                            <?php if ($data['info']['publish_status']==0): ?>
                                                                <th width="8%"> Action</th>
                                                            <?php endif ?>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $data['documents'] ?>
                                                        <?php $documents_row  = 0; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>documents/project' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .nk-block -->
                    </form>
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<?php include 'includes/bottom.html'; ?>

<div class="modal fade editCheckLIstAttachment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="perror"></div>
                <form name="updateCheckListInfo" id="updateCheckListInfo" method="POST" action="#" accept-charset="utf-8">
                     <div class="editCheckListInfoModel">
                    </div>                     
                </form>
            </div>
        </div>
    </div>
</div>