<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item active">Private Document for <?php echo ucwords($data['info']['title']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">

                        <div class="nk-block">
                            <div class="row gy-5">
                                <div class="col-lg-6">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h5 class="nk-block-title title">Property Info</h5>
                                        </div>
                                    </div>
                                    <div class="card card-bordered">
                                        <ul class="data-list is-compact">
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Title</div>
                                                    <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>property/details/<?php echo $data['info']['id']  ?>"><?php echo ucwords($data['info']['title']) ?></a> </div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Type</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['type']) ?></div>
                                                </div>
                                            </li>
                                            
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Flat Variant</div>
                                                    <div class="data-value"><?php echo ucwords($data['roominfo']['flat_variant']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Blook</div>
                                                    <div class="data-value"><?php echo ucwords($data['blockinfo']['block']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Floor</div>
                                                    <div class="data-value"><?php echo ucwords($data['floorinfo']['floor']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Cost </div>
                                                    <div class="data-value">Rs. <?php echo ucwords($data['info']['cost']) ?></div>
                                                </div>
                                            </li>
                                            
                                            
                                        </ul>
                                    </div>
                                </div><!-- .col -->
                                <?php if ($data['info']['customer_id']=='0') { ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                                            
                                                <h5 class="nk-block-title title">Other Info</h5>
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Size</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectsize']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Area</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectarea']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">City</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">State</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['state']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Pincode</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['pincode']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Customer Status</div>
                                                    <div class="data-value"><span class="badge badge-info">Un Assign</span></div>
                                                </div>
                                            </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                        
                                                <h5 class="nk-block-title title">Customer Info</h5>  
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">ID</div>
                                                        <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>customer/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['cusinfo']['uid']) ?></a></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Name</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Email Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Mobile</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">City</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>                                
                            </div><!-- .row -->
                        </div>



                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Private Documents </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="#"  data-toggle="modal" data-target="#addGeneralDoc" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add General Document</span></a>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                       <div class="nk-block">
                            <div class="card card-bordered card-stretch">
                                <div class="card-inner-group">
                                    
                                    <div class="card-inner p-0">
                                        <div class="nk-tb-list nk-tb-ulist">
                                            <?php echo $data['list'] ?>
                                        </div>
                                    </div><!-- .card-inner -->
                                    <?php if ($data['doc_list']['count']>0){ ?>
                                    <div class="card-inner p-0">
                                        <div class="nk-tb-list nk-tb-ulist">
                                            <div class='card-inner'>
                                                <div class='card-title-group'>
                                                    <div class='card-title'>
                                                        <h6 class='title'>General Documents</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='card-inner card-inner-md'>
                                                <div class='user-card'>
                                                    <div class='nk-tb-list nk-tb-ulist'>
                                                        <div class='nk-tb-item nk-tb-head'>
                                                            <div class='nk-tb-col' style='width: 1%;'><span>#</span></div>
                                                            <div class='nk-tb-col tb-col-mb' style='width: 15%;'><span>Doc Name</span></div>
                                                            
                                                            <div class='nk-tb-col tb-col-md' style='width: 15%;'><span> Status</span></div>
                                                            <div class='nk-tb-col tb-col-md' style='width: 14%;'><span>Action</span></div>
                                                        </div>
                                                        <?php echo $data['doc_list']['layout'] ?>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .card-inner -->
                                    <?php } ?>
                                </div><!-- .card-inner-group -->
                            </div><!-- .card -->
                        </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal edituploadfile fade in" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateDocumentsFiles" name="updateDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add File</h5>
                
                   <!--  <input type="hidden" value="<?php echo $_SESSION['add_floor_key'] ?>" name="fkey" id="fkey"> -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="edituploadfileModel"></div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal editgenuploadfile fade in" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateGenDocumentsFiles" name="updateGenDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add File</h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="editgenuploadfileModel"></div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="addGeneralDoc">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="addPropertyGendralDocuments" name="addPropertyGendralDocuments" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add General Document</h5>
                
                    <input type="hidden" value="<?php echo $_SESSION['add_propertygendral_documents_key'] ?>" name="fkey" id="fkey">
                    <input type="hidden" name="property_id" id="property_id" value="<?php echo $data['info']['id'] ?>">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="title">Title <en>*</en></label>
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                     <div class="form-group">
                                        <label class="form-label" for="description">Description <en></en></label>
                                        <textarea class="form-control" name="description" id="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="attachment">Document <en>*</en></label>
                                        <input type="file" class="form-control" id="attachment" name="attachment">
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>


<?php include 'includes/bottom.html'; ?>



<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>General document added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>
