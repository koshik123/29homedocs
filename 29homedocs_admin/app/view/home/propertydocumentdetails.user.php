<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property/document/<?php echo $data['info']['id'] ?>">Manage Document List</a></li>
                                <li class="breadcrumb-item active">Document Check Lists </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Document Check Lists </h4>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                       <div class="nk-block">
                            <div class="card card-bordered card-stretch">
                                <div class="card-inner-group">
                                    
                                    <div class="card-inner p-0">
                                        <div class="nk-tb-list nk-tb-ulist">
                                            <div class="nk-tb-item nk-tb-head">
                                                
                                                <div class="nk-tb-col" style="width: 1%;"><span>#</span></div>
                                                <div class="nk-tb-col tb-col-mb" style="width: 10%;"><span>Doc Name</span></div>
                                                <div class="nk-tb-col tb-col-mb" style="width: 40%;"><span>Description</span></div>
                                                <div class="nk-tb-col tb-col-md" style="width: 15%;"><span>Documents Status</span></div>
                                                <div class="nk-tb-col tb-col-md" style="width: 14%;"><span>Status</span></div>
                                                
                                            </div><!-- .nk-tb-item -->
                                            <?php echo $data['list'] ?>
                                           
                                        </div>
                                    </div><!-- .card-inner -->
                                    
                                </div><!-- .card-inner-group -->
                            </div><!-- .card -->
                        </div><!-- .nk-block -->
                         <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>property/document/<?php echo $data['info']['id'] ?>' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                    </div> <!-- nk-block -->
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="" name="" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add File</h5>
                
                   <!--  <input type="hidden" value="<?php echo $_SESSION['add_floor_key'] ?>" name="fkey" id="fkey"> -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="block">File</label>
                                        <input type="file" class="form-control" id="file" name="file">
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>



