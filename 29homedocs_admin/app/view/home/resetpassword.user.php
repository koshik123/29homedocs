<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?php echo IMGPATH ?>favicon.png">
    <!-- Page Title  -->
    <title><?php echo $data['page_title'] ?></title>
    <!-- StyleSheets  -->
     <link rel="stylesheet" href="<?php echo CSSPATH ?>custom.css">
    <link rel="stylesheet" href="<?php echo CSSPATH ?>dashlite.css?ver=1.5.0">
    <link id="skin-default" rel="stylesheet" href="<?php echo CSSPATH ?>theme.css?ver=1.5.0">
    <script type="text/javascript">
        var base_path = "<?php echo BASEPATH ?>";
        var core_path = "<?php echo COREPATH ?>";
    </script>
</head>

<body class="nk-body bg-white npc-general pg-auth">

    <div class="page_loading">
        <div class="loading_image">
            <p class="load_img"><img  src="<?php echo IMGPATH?>loader.gif" width="80"></p>
            <p class="load_text">Please Wait...</p>
        </div>
    </div>

    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="javascript:void();" class="logo-link">
                                <img class="logo-light logo-img logo-img-lg" src="<?php echo IMGPATH ?>logo.png" srcset="<?php echo IMGPATH ?>logo2x.png 2x" alt="logo">
                                <img class="logo-dark logo-img logo-img-lg" src="<?php echo IMGPATH ?>logo-dark.png" srcset="<?php echo IMGPATH ?>logo-dark2x.png 2x" alt="logo-dark">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Dear <?php echo ucwords($data['info']['name']) ?> ,</h5>
                                        <div class="nk-block-des">
                                            <p>Create a new Password for your account.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-error"></div>
                                <form class="form-container" id="resetpass" method="POST">
                                    <input type="hidden" value="<?php echo $_SESSION['reset_password_key'] ?>" id="fkey"/>
                                    <input type="hidden" value="<?php echo $data['token']; ?>" id="token"/>
                                    <div class="form-group">
                                        <input class="form-control form-control-lg" type="password" name="new_pass" id="newpassword" placeholder="Enter New Password">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control form-control-lg" type="password" name="confirm_pass" id="confirm_pass" placeholder="Enter Confirm Password">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
                                    </div>
                                </form>
                                <div class="form-note-s2 text-center pt-4">
                                    Remember Password? <a href="<?php echo COREPATH ?>"><strong>Sign in?</strong></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nk-footer nk-auth-footer-full">
                        <div class="container wide-lg">
                            <div class="row g-3">
                                <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="javascript:void();">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="javascript:void();">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="javascript:void();">Help</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <div class="nk-block-content text-center text-lg-left">
                                        <p class="text-soft">&copy; <?php echo date("Y").' '.COMPANY_NAME ?>. All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="<?php echo JSPATH ?>bundle.js?ver=1.5.0"></script>
    <script src="<?php echo JSPATH ?>scripts.js?ver=1.5.0"></script>
    <script src="<?php echo JSPATH ?>validate.min.js"></script>
    <script src="<?php echo JSPATH ?>user.js"></script>

</html>