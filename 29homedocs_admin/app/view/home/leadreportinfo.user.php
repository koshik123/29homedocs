<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>reports/lead">Lead Reports</a></li>
                                <li class="breadcrumb-item active"><strong><?php echo date("d M'y ", strtotime($data['from'])).' to '.date("d M'y ", strtotime($data['to'])) ?></strong>     </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Filter</h4>
                            </div>
                        </div>
                        <div class="row g-gs ">
                            
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="leadReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fromdate">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="fromdate" name='from_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="todate">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="todate" name='to_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Employee </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-top" name="employee_id" data-placeholder="Select a Employee" >
                                                                <option value="">Select a Employee</option>
                                                                <?php echo $data['employee_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Lead Status <en></en></label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="leadstatus_id" data-placeholder="Select a Lead Status">  <option value="">Select a Lead Status</option>
                                                                <?php echo $data['lead_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php if ($data['from']!="" && $data['to']!="" ){ ?>
                                <div class="daybook_report-form">
                                    <h3 style="font-size: 21px;"> <i class="fa fa-file-o"></i> Showing Results From - <strong><?php echo date("d M'y ", strtotime($data['from'])) ?></strong>  to <strong> <?php echo date("d M'y ", strtotime($data['to']))?></strong>
                                        <?php if ($data['employee_id']!=""){ ?>
                                            , <strong>Assign Employee:</strong>  <?php echo ucwords($data['empname'])?>
                                        <?php } ?>
                                        <?php if ($data['status']!=""){ ?>
                                            , <strong>Status:</strong> <?php echo ucwords($data['status']) ?>
                                        <?php } ?>

                                      </h3>
                                </div>
                                <?php } ?>
                                <a class="btn btn-info btn-sm mt15" href="<?php echo COREPATH ?>resource/lead_excel_report.php?from=<?php echo $data['from'] ?>&to=<?php echo $data['to'] ?>&employee_id=<?php echo $data['employee_id'] ?>&status=<?php echo $data['status'] ?>" ><em class="icon ni ni-file-docs"></em>  Export Report</a>
                            </div>

                            <div class="col-lg-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-bordered ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Lead Source Master</th>
                                                <th>Lead Status</th>
                                                <th>Assigned To</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php echo $data['list'] ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>