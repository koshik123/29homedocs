<?php include 'includes/top.html'; ?>


<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm float_left">
                    <div class="nk-block-between g-3">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">KYC Documents</h3>
                            <!-- <div class="nk-block-des text-soft">
                                <p>You have total <span class="text-base">1,257</span> KYC documents.</p>
                            </div> -->
                        </div>
                       
                    </div>
                </div><!-- .nk-block-head -->
                <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                <?php endif ?>
                <div class="clearfix"></div>
                <div class="nk-block">
                    <div class="card card-bordered card-stretch">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Customer</th>
                                            <th>Submitted On</th>
                                            <th>Status</th>
                                            <th>Checked By</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list'] ?> 
                                    </tbody>
                                </table>
                            </div>
                    </div><!-- .card -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>



<div class="modal fade kycModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateapproverejectkyc"  id="updateapproverejectkyc">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">KYC Status</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <!-- <div class="kycInfo"></div> -->
                                    <input type='hidden' value='' name='fkey' id='fkey'>
                                    <input type='hidden' value='' name='kyc_status' id='kyc_status'>         
                                    <input type='hidden' value='' name='token' id='token'>
                                    <div class='form-group'>
                                        <label class='form-label' for='cf-full-name'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='cf-default-textarea' placeholder='Write your message' name='remarks' ></textarea>
                                    </div>


                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>


<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>kyc updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

