<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Document Reports </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Document Reports</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            
                            <div class="col-lg-10">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form action="javascript:void();">
                                            <div class="row gy-4">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">From Date</label>
                                                        <input type="text" class="form-control  date-picker" id="birth-day" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">To Date</label>
                                                        <input type="text" class="form-control date-picker" id="birth-day" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fv-topics">Select Document</label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select form-select-lg " id="
                                                            " name="fv-topics" data-placeholder="Select a Document" required>
                                                                <option label="empty" value=""></option>
                                                                <option value="fv-gq">Document 1</option>
                                                                <option value="fv-tq">Document 2</option>
                                                                <option value="fv-ab">Document 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>