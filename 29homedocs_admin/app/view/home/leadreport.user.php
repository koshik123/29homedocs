<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Adhoc Reports </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Lead Reports</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            
                            <div class="col-lg-7">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="leadReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fromdate">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="fromdate" name='from_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="todate">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="todate" name='to_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Employee </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-top" name="employee_id" data-placeholder="Select a Employee" >
                                                                <option value="">Select a Employee</option>
                                                                <?php echo $data['employee_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Lead Status<en></en></label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="leadstatus_id" data-placeholder="Select a Lead Status">  <option value="">Select a Lead Status</option>
                                                                <?php echo $data['lead_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>