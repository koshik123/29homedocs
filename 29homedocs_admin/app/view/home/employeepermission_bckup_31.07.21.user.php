<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active">Employee Permission</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="row g-gs">
                            
                            <div class="col-lg-6">
                                <div class="">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h4 class="title nk-block-title"> Employee Contact information</h4>
                                        </div>
                                    </div>      
                                    <div class="card panel panel-default panel-table">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Name </th>
                                                        <td> <?php echo ucwords($data['info']['name']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Role </th>
                                                        <td><?php echo ucwords($data['roleinfo']['employee_role']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Gender </th>
                                                        <td><?php echo ucwords($data['info']['gender']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Email </th>
                                                        <td><?php echo ($data['info']['email']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Mobile</th>
                                                        <td><?php echo ($data['info']['mobile']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Address</th>
                                                        <td><?php echo ($data['info']['address']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">City </th>
                                                        <td><?php echo ucwords($data['info']['city']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">State</th>
                                                        <td><?php echo ucwords($data['info']['state']) ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-6">
                                    <div class="form-error"></div>
                                    <form id="employeePermissions" method="POST" accept-charset="utf-8" action="javascript:void();">
                                        <input type="hidden" value="<?php echo $_SESSION['create_permission_key'] ?>" name="fkey" id="fkey">
                                        <input type="hidden" value="<?php echo $data['token'] ?>" id="session_token" name="session_token">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <h5 class="  pull-left">
                                               <span class="ks-icon la la-key"></span> Permissions 
                                                </h5>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="select_box"><label class='checkbox-inline checkbox-success'>
                                                <input class='styled' type='checkbox' id="selectAllPost"> Select All</label></p>                             
                                            </div>
                                        </div>

                                        <div class="card panel panel-default panel-table">
                                            <div class="table-responsive">
                                                
                                                <table class="table table-bordered permission_checkbox ">
                                                    <tbody>
                                                        <tr>
                                                            <th width="20%" scope="row"># </th>
                                                            <th> Allowed Permissions</th>
                                                        </tr> 
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">1 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='lead' class='post_permission' id="l1" type="checkbox" <?php echo (($data['emp_info']['lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l1" class="marginleft5">
                                                                       <strong>Lead Management </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manage_lead' class='post_permission lead_permission' id="l2" type="checkbox" <?php echo (($data['emp_info']['manage_lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l2" class="marginleft5">
                                                                        Manage Lead
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_type' class='post_permission lead_permission' id="l3" type="checkbox" <?php echo (($data['emp_info']['lead_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l3" class="marginleft5">
                                                                        Lead Status Message
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='activity_type' class='post_permission lead_permission' id="l4" type="checkbox" <?php echo (($data['emp_info']['activity_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l4" class="marginleft5">
                                                                        Activity Type Master
                                                                    </label>
                                                                </div> 

                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_source' class='post_permission lead_permission' id="l5" type="checkbox" <?php echo (($data['emp_info']['lead_source']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l5" class="marginleft5">
                                                                        Lead Source Master
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='profile_master' class='post_permission lead_permission' id="l6" type="checkbox" <?php echo (($data['emp_info']['profile_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l6" class="marginleft5">
                                                                        Lead Profile Master
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='import_lead' class='post_permission lead_permission' id="l7" type="checkbox" <?php echo (($data['emp_info']['import_lead']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="l7" class="marginleft5">
                                                                        Import Lead
                                                                    </label>
                                                                </div> 
                                                            </td>
                                                        </tr>



                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">2 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='customers' class='post_permission' id="c1" type="checkbox" <?php echo (($data['emp_info']['customers']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c1" class="marginleft5">
                                                                       <strong>Customer Management </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manage_customer' class='post_permission customer_permission' id="c2" type="checkbox" <?php echo (($data['emp_info']['manage_customer']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c2" class="marginleft5">
                                                                        Manage Customer
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='import_customer' class='post_permission customer_permission' id="c6" type="checkbox" <?php echo (($data['emp_info']['import_customer']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c6" class="marginleft5">
                                                                        Import Customer
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='kyc_documents' class='post_permission customer_permission' id="c3" type="checkbox" <?php echo (($data['emp_info']['kyc_documents']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c3" class="marginleft5">
                                                                        KYC documents
                                                                    </label>
                                                                </div>   
                                                               <!--  <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='adhoc_request' class='post_permission customer_permission' id="c4" type="checkbox" <?php echo (($data['emp_info']['adhoc_request']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c4" class="marginleft5">
                                                                        Adhoc Request
                                                                    </label>
                                                                </div>    -->
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='invoice' class='post_permission customer_permission' id="c5" type="checkbox" <?php echo (($data['emp_info']['invoice']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="c5" class="marginleft5">
                                                                        Invoices / Receipts
                                                                    </label>
                                                                </div>   
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">3 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='adhoc' class='post_permission' id="a1" type="checkbox" <?php echo (($data['emp_info']['adhoc']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="a1" class="marginleft5">
                                                                       <strong>Adhoc Management </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='open_request' class='post_permission adhoc_permission' id="a2" type="checkbox" <?php echo (($data['emp_info']['open_request']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="a2" class="marginleft5">
                                                                        Open Request
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='close_request' class='post_permission adhoc_permission' id="a3" type="checkbox" <?php echo (($data['emp_info']['close_request']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="a3" class="marginleft5">
                                                                        Closed Request
                                                                    </label>
                                                                </div>  
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='all_request' class='post_permission adhoc_permission' id="a4" type="checkbox" <?php echo (($data['emp_info']['all_request']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="a4" class="marginleft5">
                                                                        All Request
                                                                    </label>
                                                                </div>   
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">4 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='documents' class='post_permission' id="d1" type="checkbox" <?php echo (($data['emp_info']['documents']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="d1" class="marginleft5">
                                                                       <strong>Document Management  </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='general_documents' class='post_permission document_permission' id="d2" type="checkbox" <?php echo (($data['emp_info']['general_documents']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="d2" class="marginleft5">
                                                                        General Documents
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='document_checklist' class='post_permission document_permission' id="d3" type="checkbox" <?php echo (($data['emp_info']['document_checklist']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="d3" class="marginleft5">
                                                                        Document Checklists
                                                                    </label>
                                                                </div>  
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">5 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='property' class='post_permission' id="p1" type="checkbox" <?php echo (($data['emp_info']['property']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p1" class="marginleft5">
                                                                       <strong>Property Settings  </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='manage_property' class='post_permission property_permission' id="p2" type="checkbox" <?php echo (($data['emp_info']['manage_property']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p2" class="marginleft5">
                                                                        Manage Property 
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='property_chart' class='post_permission property_permission' id="p3" type="checkbox" <?php echo (($data['emp_info']['property_chart']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p3" class="marginleft5">
                                                                        Property Chart
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='gallery' class='post_permission property_permission' id="p4" type="checkbox" <?php echo (($data['emp_info']['gallery']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p4" class="marginleft5">
                                                                        Manage Gallery
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='block' class='post_permission property_permission' id="p5" type="checkbox" <?php echo (($data['emp_info']['block']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p5" class="marginleft5">
                                                                        Block Settings
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='floor' class='post_permission property_permission' id="p6" type="checkbox" <?php echo (($data['emp_info']['floor']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p6" class="marginleft5">
                                                                       Floor Settings
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='flat_type' class='post_permission property_permission' id="p7" type="checkbox" <?php echo (($data['emp_info']['flat_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p7" class="marginleft5">
                                                                        Flat Type settings
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='brochure' class='post_permission property_permission' id="p8" type="checkbox" <?php echo (($data['emp_info']['brochure']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p8" class="marginleft5">
                                                                        Property Brochure
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='contact_info' class='post_permission property_permission' id="p9" type="checkbox" <?php echo (($data['emp_info']['contact_info']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p9" class="marginleft5">
                                                                        Contact Information
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='payment_info' class='post_permission property_permission' id="p10" type="checkbox" <?php echo (($data['emp_info']['payment_info']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p10" class="marginleft5">
                                                                        Payment Information
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='virtual_tour' class='post_permission property_permission' id="p11" type="checkbox" <?php echo (($data['emp_info']['virtual_tour']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p11" class="marginleft5">
                                                                        360 Virtual Tour Settings
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='area_master' class='post_permission property_permission' id="p12" type="checkbox" <?php echo (($data['emp_info']['area_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p12" class="marginleft5">
                                                                        Property Area Master
                                                                    </label>
                                                                </div>
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='department' class='post_permission property_permission' id="p13" type="checkbox" <?php echo (($data['emp_info']['department']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="p13" class="marginleft5">
                                                                       Department
                                                                    </label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">5 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='settings' class='post_permission' id="m1" type="checkbox" <?php echo (($data['emp_info']['settings']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m1" class="marginleft5">
                                                                       <strong>Master settings  </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='employee' class='post_permission master_permission' id="m2" type="checkbox" <?php echo (($data['emp_info']['employee']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m2" class="marginleft5">
                                                                        Manage Employee
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='request_type' class='post_permission master_permission' id="m3" type="checkbox" <?php echo (($data['emp_info']['request_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m3" class="marginleft5">
                                                                         Request Type
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='kycdoc_type' class='post_permission master_permission' id="m4" type="checkbox" <?php echo (($data['emp_info']['kycdoc_type']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m4" class="marginleft5">
                                                                        KYC Document Types
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='notification' class='post_permission master_permission' id="m5" type="checkbox" <?php echo (($data['emp_info']['notification']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m5" class="marginleft5">
                                                                        Notification Emails
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='company' class='post_permission master_permission' id="m6" type="checkbox" <?php echo (($data['emp_info']['company']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m6" class="marginleft5">
                                                                        Company Settings
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='role_master' class='post_permission master_permission' id="m7" type="checkbox" <?php echo (($data['emp_info']['role_master']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m7" class="marginleft5">
                                                                        Employee Role Master 
                                                                    </label>
                                                                </div>   
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='news' class='post_permission master_permission' id="m8" type="checkbox" <?php echo (($data['emp_info']['news']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="m8" class="marginleft5">
                                                                        News
                                                                    </label>
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th width="" scope="row" class="verticalalign">6 </th>
                                                            <td width="">
                                                                <div class="checkbox">
                                                                    <input name='permission[]' value='reports' class='post_permission' id="r1" type="checkbox" <?php echo (($data['emp_info']['reports']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r1" class="marginleft5">
                                                                       <strong>Reports   </strong> 
                                                                    </label>
                                                                </div> 
                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='adhoc_report' class='post_permission report_permission' id="r2" type="checkbox" <?php echo (($data['emp_info']['adhoc_report']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r2" class="marginleft5">
                                                                        Adhoc Reports
                                                                    </label>
                                                                </div>   


                                                                <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='lead_report' class='post_permission report_permission' id="r3" type="checkbox" <?php echo (($data['emp_info']['lead_report']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r3" class="marginleft5">
                                                                        Lead Reports
                                                                    </label>
                                                                </div>   
                                                                <!-- <div class="checkbox marginleft30">
                                                                    <input name='permission[]' value='activity_report' class='post_permission report_permission' id="r4" type="checkbox" <?php echo (($data['emp_info']['activity_report']=="1") ? "checked" : "" ) ?>>
                                                                    <label for="r4" class="marginleft5">
                                                                        Activity Reports
                                                                    </label>
                                                                </div>  -->  
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="text-right m-b-0">
                                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>