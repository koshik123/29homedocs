<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>documents">General Documents</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['title']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-7">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Document Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Title</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['title']) ?></div>
                                            </div>
                                        </li>


                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Description</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['description']) ?></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            <div class="col-lg-5">
                                
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Uploaded Documents</h5>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                         <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-value"><a href="<?php echo DOCUMENTS ?><?php echo $data['info']['documents'] ?>" target="_blank"><img src='<?php echo IMGPATH ?>file.png' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style="width: 130px;"></a></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- .card -->
                            </div><!-- .col -->
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>




<?php include 'includes/bottom.html'; ?>




