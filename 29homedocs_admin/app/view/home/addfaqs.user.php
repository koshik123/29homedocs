<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>faqs">Manage FAQ's</a></li>
                                <li class="breadcrumb-item active">Add FAQ's</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Add FAQ's</h4>
                                
                            </div>
                        </div>
                        <form id="addContactInfo" name="addFaqs" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_faq_key'] ?>" name="fkey" id="fkey">
                            <div class="row g-gs">
                                <div class="col-lg-6">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Add FAQ's</h5>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="title">Title <en>*</en></label>
                                                <input type="text" class="form-control" id="title" name="title">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="form-label" for="message">Message <en>*</en></label>
                                                <div class="form-control-wrap">
                                                    <textarea class="form-control form-control-sm" id="message" name="message" placeholder="Write your message"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href='<?php echo COREPATH ?>faqs' class="btn btn-lg btn-danger">Cancel</a>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>