<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item active">Add Property Rooms</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->

                    <form  id="addPropertyRoomss" name="addPropertyRooms" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['add_propertyrooms_key'] ?>" name="fkey" id="fkey">
                        <div class="nk-block nk-block-lg minheight">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Add Property Rooms</h4>
                                </div>
                            </div>
                                <div class="row g-gs">                                    
                                     <div class="col-lg-12 h-100 minheight">
                                        <div class="form-group">
                                            <div class="card panel panel-default panel-table">
                                                <h5 class="table-header">
                                                    Add Rooms 
                                                </h5>
                                                <div class="add_btn">
                                                    <button type="button" onclick="addProperty();" title="Add" class="btn btn-success btn-sm">Add </button>
                                                </div>
                                                <div class="card-block">
                                                    <table class="table table-bordered valign table_custom" id="propert">
                                                        <thead>
                                                            <tr>
                                                                <th width="15%">Room Name</th>
                                                                <th width="15%">Type</th>
                                                                <th width="15%">Sqft</th>
                                                                <th width="15%">Images</th>
                                                                <th width="8%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        
                                                        <tbody>
                                                            <?php $property_row  = 0; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form_submit_footer">
                                        <div class="form_footer_contents">
                                            <div class="text-right m-b-0">
                                                <a href='<?php echo COREPATH ?>property' class="btn btn-lg btn-danger">Cancel</a>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div><!-- .nk-block -->
                    </form>
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>



<?php include 'includes/bottom.html'; ?>