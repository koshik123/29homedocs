<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>news">Manage News</a></li>
                                <li class="breadcrumb-item active">Edit News</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg ">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Edit News</h4>
                                
                            </div>
                        </div>
                        <form id="editnews" method="POST" action="#" name="editnews" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_news_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                            <div class="row g-gs">
                                <div class="col-lg-5 mb70">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="title">Title <en>*</en></label>
                                                        <input type="text" class="form-control" id="title" name="title" value="<?php echo $data['info']['title'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="date">Date <en>*</en></label>
                                                        <input type="text" name="date" id="date" class="form-control date-picker" data-date-format="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($data['info']['date'])) ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class=""> </label>
                                                        <ul class="custom-control-group g-3 align-center">
                                                            <li>
                                                                <div class="custom-control custom-control-md custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="com-email-1" name="email_notification"  <?php echo $data['info']['email_notification']=='1' ? 'checked' : ''  ?>>
                                                                    <label class="custom-control-label" for="com-email-1">Send Email Notification</label>
                                                                </div>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br>
                                                    <div class="form-group">
                                                        <ul class="custom-control-group g-3 align-center">
                                                            <li>
                                                                <div class="custom-control custom-control-md custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input show_popup" id="com-email-2" name="show_popup" <?php echo $data['info']['show_popup']=='1' ? 'checked' : ''  ?>>
                                                                    <label class="custom-control-label" for="com-email-2">Show in pop up banner</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <?php $show = $data['info']['show_popup']=='1' ? '' : 'no_display' ; ?>
                                                    <div class="form-group <?php echo $show ?>" id="show_popup_warp">
                                                        <label class="form-label" for="till_date">Show banner till (datepicker - greater than today) <en>*</en></label>
                                                        <input type="text" name="till_date" id="till_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" value="<?php echo $data['info']['popup_till']!='' ? date('d/m/Y',strtotime($data['info']['popup_till'])) : '' ; ?>" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group"></div>
                                                    <label class="form-label" for="album_title">Post To <en>*</en></label>
                                                    <ul class="custom-control-group g-3 align-center flex-wrap">
                                                        <li>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" class="custom-control-input post_to" checked="" name="post_to" value="all" id="reg-enable" <?php echo $data['info']['post_to']=='all' ? 'checked' : ''  ?>>
                                                                <label class="custom-control-label" for="reg-enable">All Customers</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" class="custom-control-input post_to" name="post_to" value="selected" id="reg-disable" <?php echo $data['info']['post_to']=='selected' ? 'checked' : ''  ?>>
                                                                <label class="custom-control-label" for="reg-disable">Select Customers</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <?php $show2 = $data['info']['post_to']=='all' ? 'no_display' : '' ; ?>
                                                <?php $value = $data['info']['post_to']=='all' ? '0' : $data['info']['customer_ids'] ; ?>
                                                <div class="col-lg-12 <?php echo $show2 ?>" id="selectcustomer_warp">
                                                    <div class="form-group"></div>
                                                    <div class="form-group">
                                                        <label>Select Customers <en></en> </label>
                                                        <input type="text" name="editcustomers" value="" placeholder="Select Customers"  class="form-control" />
                                                        <input type="hidden" name="customer_ids" id="customer_ids" value="<?php echo $value ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $show1 = $data['info']['post_to']=='all' ? 'no_display' : '' ; ?>

                                    <div class="form-group <?php echo $show1 ?>" id="selectcustomerdisplay_warp">
                                        <label></label>
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">
                                                <h4 class="title nk-block-title">Selected Customers</h4>
                                                
                                            </div>
                                        </div>
                                        <div class="related_product_container">
                                            <div id="selected-assistants" class="chapter_locations_wrap" style="">
                                            </div>
                                        </div>
                                        <?php echo $data['list'] ?>
                                    </div>

                                </div>
                                <div class="col-lg-7 mb70">
                                    <div class="card card-bordered">
                                        <div class="card-inner">
                                            <div class="form-group">
                                                <label class="form-label" for="description">Description</label>
                                                 <textarea placeholder="Enter Description" value="" class="form-control summernote" name="description"><?php echo $data['info']['description'] ?></textarea>
                                            </div>
                                            <?php if($data['info']['image']!="") { ?>
                                                 <div class="form-group">
                                                    <label class="form-label" for="album_title">Images <en></en></label>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <img src="<?php echo SRCIMG.$data['info']['image'] ?>" class="img-responsive img-thumbnail">
                                                            <input type="hidden" id="list_image_exists" value="2">
                                                            <input type="hidden" name="image" value="<?php echo $data['info']['image'] ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <button type="button" data-id="<?php echo $data['token'] ?>" class="btn btn-danger btn-sm ma_top_25 removeNewsImgIcon" data-type="image" id=""><i class="icon-remove2 "></i>Delete Image</button>
                                                        </div>
                                                    </div>
                                                </div> 
                                            <?php }else{ ?>
                                                <div class="form-group">
                                                    <label class="form-label" for="album_title">Images <en></en></label>
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                                        <div>
                                                            <span class="btn btn-info btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" id="cimage" name="cimage" />
                                                            <input type="hidden" name="image" id="newsImage"/>
                                                            </span>
                                                            <a href="#" class="btn btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                        </div>
                                                    </div>
                                                    <div class="imgerr"></div>
                                                </div> 
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                    
                                    <!--  <div class="form-group " id="selectcustomerdisplay_warp">
                                        <label></label>
                                        
                                    </div> -->

                                    
                                </div>
                                
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>news' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>