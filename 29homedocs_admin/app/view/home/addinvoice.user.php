<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>invoice">Invoice / Receipts</a></li>
                                <li class="breadcrumb-item active">Add Invoice / Receipt</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h4 class="title nk-block-title">Add Invoice / Receipt</h4>
                        </div>
                    </div>
                    <div class="nk-block nk-block-lg minheight">
                        <form id="addInvoice" name="addInvoice" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_invoice_key'] ?>" name="fkey" id="fkey">
                            <div class="row g-gs">
                                <div class="col-lg-7">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Add Invoice / Receipt</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="name">Invoice / Receipt Number <en>*</en></label>
                                                        <input type="text" class="form-control" id="inv_number" name="inv_number">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="form-label">Invoice / Receipt Date <en>*</en></label>
                                                            <div class="form-control-wrap">
                                                                <input type="text" name="inv_date" id="inv_date" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                            
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="row g-3 align-center">
                                                            <div class="col-lg-12">
                                                                <label class="form-label">Type <en>*</en></label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" checked="" name="type" id="reg-enable" value="invoice">
                                                                            <label class="custom-control-label" for="reg-enable">Invoice </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" name="type" id="reg-disable" value="receipt">
                                                                            <label class="custom-control-label" for="reg-disable">Receipt</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="row g-3 align-center">
                                                            <div class="col-lg-12">
                                                                <label class="form-label">Raised to <en>*</en></label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" checked="" name="raised_to" id="reg-enable1" value="bank">
                                                                            <label class="custom-control-label" for="reg-enable1">Bank  </label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" name="raised_to" id="reg-disable1" value="customer">
                                                                            <label class="custom-control-label" for="reg-disable1">Customer</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Select Customer <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-top" name="customer_id" data-search="on" data-placeholder="Select a Customer" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['customer'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="attachment">File <en>*</en></label>
                                                        <input type="file" class="form-control" id="attachment" name="attachment">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="email">Remarks <en>*</en></label>
                                                        <textarea class="form-control" id="remarks" name="remarks"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>invoice' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

