<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active">Employee Permission</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="row g-gs">
                            
                            <div class="col-lg-6">
                                <div class="">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h4 class="title nk-block-title"> Employee Contact information</h4>
                                        </div>
                                    </div>      
                                    <div class="card panel panel-default panel-table">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Name </th>
                                                        <td> <?php echo ucwords($data['info']['name']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Role </th>
                                                        <td><?php echo ucwords($data['roleinfo']['employee_role']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Gender </th>
                                                        <td><?php echo ucwords($data['info']['gender']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Email </th>
                                                        <td><?php echo ($data['info']['email']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Mobile</th>
                                                        <td><?php echo ($data['info']['mobile']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Address</th>
                                                        <td><?php echo ($data['info']['address']) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">City </th>
                                                        <td><?php echo ucwords($data['info']['city']) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">State</th>
                                                        <td><?php echo ucwords($data['info']['state']) ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-6">
                                    <div class="form-error"></div>
                                    <form id="employeePermissions" method="POST" accept-charset="utf-8" action="javascript:void();">
                                        <input type="hidden" value="<?php echo $_SESSION['create_permission_key'] ?>" name="fkey" id="fkey">
                                        <input type="hidden" value="<?php echo $data['token'] ?>" id="session_token" name="session_token">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <h5 class="  pull-left">
                                               <span class="ks-icon la la-key"></span> Permissions 
                                                </h5>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="select_box"><label class='checkbox-inline checkbox-success'>
                                                <input class='styled' type='checkbox' id="selectAllPost"> Select All</label></p>                             
                                            </div>
                                        </div>
                                            <div class="card panel panel-default panel-table">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered permission_checkbox ">
                                                        <tbody>
                                                            <tr>
                                                                <th width="20%" scope="row"># </th>
                                                                <th> Allowed Permissions</th>
                                                            </tr> 
                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">1 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='lead' class='post_permission' id="l1" type="checkbox" <?php echo (($data['emp_info']['lead']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="l1" class="marginleft5">
                                                                           <strong>Lead Management </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='manage_lead' class='post_permission lead_permission' id="l2" type="checkbox" <?php echo (($data['emp_info']['manage_lead']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="l2" class="marginleft5">
                                                                            Manage existing Lead
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='import_lead' class='post_permission lead_permission' id="l3" type="checkbox" <?php echo (($data['emp_info']['import_lead']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="l3" class="marginleft5">
                                                                            Import Leads from excel sheet
                                                                        </label>
                                                                    </div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">2 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='customers' class='post_permission' id="c1" type="checkbox" <?php echo (($data['emp_info']['customers']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c1" class="marginleft5">
                                                                           <strong>Customer Management </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='manage_customer' class='post_permission customer_permission' id="c2" type="checkbox" <?php echo (($data['emp_info']['manage_customer']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c2" class="marginleft5">
                                                                            Manage existing customer 
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='kyc_documents' class='post_permission customer_permission' id="c3" type="checkbox" <?php echo (($data['emp_info']['kyc_documents']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c3" class="marginleft5">
                                                                            KYC Documents Management
                                                                        </label>
                                                                    </div>  
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='invoice' class='post_permission customer_permission' id="c4" type="checkbox" <?php echo (($data['emp_info']['invoice']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c4" class="marginleft5">
                                                                            Manage existing Invoices / Receipts
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='import_customer' class='post_permission customer_permission' id="c5" type="checkbox" <?php echo (($data['emp_info']['import_customer']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c5" class="marginleft5">
                                                                            Import Customer data from excel sheet
                                                                        </label>
                                                                    </div>   
                                                                   <!--  <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='adhoc_request' class='post_permission customer_permission' id="c4" type="checkbox" <?php echo (($data['emp_info']['adhoc_request']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="c4" class="marginleft5">
                                                                            Adhoc Request
                                                                        </label>
                                                                    </div>    -->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">3 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='property' class='post_permission' id="p1" type="checkbox" <?php echo (($data['emp_info']['property']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="p1" class="marginleft5">
                                                                           <strong>Property Management  </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='manage_property' class='post_permission property_permission' id="p2" type="checkbox" <?php echo (($data['emp_info']['manage_property']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="p2" class="marginleft5">
                                                                            Manage Property 
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='property_chart' class='post_permission property_permission' id="p3" type="checkbox" <?php echo (($data['emp_info']['property_chart']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="p3" class="marginleft5">
                                                                            Property Chart
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='gallery' class='post_permission property_permission' id="p4" type="checkbox" <?php echo (($data['emp_info']['gallery']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="p4" class="marginleft5">
                                                                            Manage Gallery
                                                                        </label>
                                                                    </div>   
                                                                   
                                                                </td>
                                                            </tr>



                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">4 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='adhoc' class='post_permission' id="a1" type="checkbox" <?php echo (($data['emp_info']['adhoc']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="a1" class="marginleft5">
                                                                           <strong>Adhoc Management </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='open_request' class='post_permission adhoc_permission' id="a2" type="checkbox" <?php echo (($data['emp_info']['open_request']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="a2" class="marginleft5">
                                                                            Open Request
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='close_request' class='post_permission adhoc_permission' id="a3" type="checkbox" <?php echo (($data['emp_info']['close_request']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="a3" class="marginleft5">
                                                                            Closed Request
                                                                        </label>
                                                                    </div>  
                                                                </td>
                                                            </tr>


                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">5 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='news' class='post_permission' id="n1" type="checkbox" <?php echo (($data['emp_info']['documents']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="n1" class="marginleft5">
                                                                           <strong>News Management  </strong> 
                                                                        </label>
                                                                    </div> 
                                                                      
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='manage_news' class='post_permission news_permission' id="n2" type="checkbox" <?php echo (($data['emp_info']['manage_news']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="n2" class="marginleft5">
                                                                            Manage Published News
                                                                        </label>
                                                                    </div>  
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">6 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='settings' class='post_permission' id="s1" type="checkbox" <?php echo (($data['emp_info']['settings']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="s1" class="marginleft5">
                                                                           <strong> settings  </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='employee' class='post_permission setting_permission' id="s2" type="checkbox" <?php echo (($data['emp_info']['employee']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="s2" class="marginleft5">
                                                                            Manage Employee
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='manage_department' class='post_permission setting_permission' id="s3" type="checkbox" <?php echo (($data['emp_info']['manage_department']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="s3" class="marginleft5">
                                                                             Manage Departments
                                                                        </label>
                                                                    </div>  
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='virtual_tour' class='post_permission setting_permission' id="s4" type="checkbox" <?php echo (($data['emp_info']['virtual_tour']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="s4" class="marginleft5">
                                                                            360 Virtual Tour Settings
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='notification' class='post_permission setting_permission' id="s5" type="checkbox" <?php echo (($data['emp_info']['notification']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="s5" class="marginleft5">
                                                                            Notification Emails
                                                                        </label>
                                                                    </div> 
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">7 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='reports' class='post_permission' id="r1" type="checkbox" <?php echo (($data['emp_info']['reports']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r1" class="marginleft5">
                                                                           <strong>Reports   </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='adhoc_report' class='post_permission report_permission' id="r2" type="checkbox" <?php echo (($data['emp_info']['adhoc_report']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r2" class="marginleft5">
                                                                            Adhoc Reports
                                                                        </label>
                                                                    </div>   


                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='lead_report' class='post_permission report_permission' id="r3" type="checkbox" <?php echo (($data['emp_info']['lead_report']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r3" class="marginleft5">
                                                                            Lead Reports
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='customer_report' class='post_permission report_permission' id="r4" type="checkbox" <?php echo (($data['emp_info']['customer_report']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r4" class="marginleft5">
                                                                            Customer Reports
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='kyc_report' class='post_permission report_permission' id="r5" type="checkbox" <?php echo (($data['emp_info']['kyc_report']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r5" class="marginleft5">
                                                                            KYC Reports
                                                                        </label>
                                                                    </div>
                                                                    <!-- <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='activity_report' class='post_permission report_permission' id="r4" type="checkbox" <?php echo (($data['emp_info']['activity_report']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="r4" class="marginleft5">
                                                                            Activity Reports
                                                                        </label>
                                                                    </div>  -->  
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <th width="" scope="row" class="verticalalign">8 </th>
                                                                <td width="">
                                                                    <div class="checkbox">
                                                                        <input name='permission[]' value='master_settings' class='post_permission' id="m1" type="checkbox" <?php echo (($data['emp_info']['master_settings']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m1" class="marginleft5">
                                                                           <strong>Master settings  </strong> 
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='lead_type' class='post_permission master_permission' id="m2" type="checkbox" <?php echo (($data['emp_info']['lead_type']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m2" class="marginleft5">
                                                                            Lead Status Master
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='activity_type' class='post_permission master_permission' id="m3" type="checkbox" <?php echo (($data['emp_info']['activity_type']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m3" class="marginleft5">
                                                                            Activity Type Master
                                                                        </label>
                                                                    </div> 

                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='lead_source' class='post_permission master_permission' id="m4" type="checkbox" <?php echo (($data['emp_info']['lead_source']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m4" class="marginleft5">
                                                                            Lead Source Master
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='profile_master' class='post_permission master_permission' id="m5" type="checkbox" <?php echo (($data['emp_info']['profile_master']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m5" class="marginleft5">
                                                                            Lead Profile Master
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='general_documents' class='post_permission master_permission' id="m6" type="checkbox" <?php echo (($data['emp_info']['general_documents']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m6" class="marginleft5">
                                                                            General Documents for all units Master
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='document_checklist' class='post_permission master_permission' id="m7" type="checkbox" <?php echo (($data['emp_info']['document_checklist']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m7" class="marginleft5">
                                                                            Private Document list for every unit Master
                                                                        </label>
                                                                    </div>  
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='block' class='post_permission master_permission' id="m8" type="checkbox" <?php echo (($data['emp_info']['block']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m8" class="marginleft5">
                                                                            Block Settings Master
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='floor' class='post_permission master_permission' id="m9" type="checkbox" <?php echo (($data['emp_info']['floor']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m9" class="marginleft5">
                                                                           Floor Settings Master
                                                                        </label>
                                                                    </div>   
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='flat_type' class='post_permission master_permission' id="m10" type="checkbox" <?php echo (($data['emp_info']['flat_type']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m10" class="marginleft5">
                                                                            Flat Type settings Master
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='brochure' class='post_permission master_permission' id="m11" type="checkbox" <?php echo (($data['emp_info']['brochure']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m11" class="marginleft5">
                                                                            Property Brochure
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='contact_info' class='post_permission master_permission' id="m12" type="checkbox" <?php echo (($data['emp_info']['contact_info']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m12" class="marginleft5">
                                                                            Contact Information Master
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='payment_info' class='post_permission master_permission' id="m13" type="checkbox" <?php echo (($data['emp_info']['payment_info']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m13" class="marginleft5">
                                                                            Payment Information Master
                                                                        </label>
                                                                    </div> 

                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='area_master' class='post_permission master_permission' id="m14" type="checkbox" <?php echo (($data['emp_info']['area_master']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m14" class="marginleft5">
                                                                            Property Area Master
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='department' class='post_permission master_permission' id="m15" type="checkbox" <?php echo (($data['emp_info']['department']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m15" class="marginleft5">
                                                                           Department
                                                                        </label>
                                                                    </div>
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='role_master' class='post_permission master_permission' id="m20" type="checkbox" <?php echo (($data['emp_info']['role_master']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m20" class="marginleft5">
                                                                            Employee Role Master
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='kycdoc_type' class='post_permission master_permission' id="m16" type="checkbox" <?php echo (($data['emp_info']['kycdoc_type']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m16" class="marginleft5">
                                                                            KYC Document Types Master
                                                                        </label>
                                                                    </div>  
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='company' class='post_permission master_permission' id="m17" type="checkbox" <?php echo (($data['emp_info']['company']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m17" class="marginleft5">
                                                                             Company Settings
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='trash' class='post_permission master_permission' id="m18" type="checkbox" <?php echo (($data['emp_info']['trash']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m18" class="marginleft5">
                                                                            Trash Property
                                                                        </label>
                                                                    </div> 
                                                                    <div class="checkbox marginleft30">
                                                                        <input name='permission[]' value='upload_video' class='post_permission master_permission' id="m19" type="checkbox" <?php echo (($data['emp_info']['upload_video']=="1") ? "checked" : "" ) ?>>
                                                                        <label for="m19" class="marginleft5">
                                                                            Upload Videos
                                                                        </label>
                                                                    </div> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <div class="text-right m-b-0">
                                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<?php include 'includes/bottom.html'; ?>