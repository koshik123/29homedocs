<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?php echo IMGPATH ?>favicon.png">
    <!-- Page Title  -->
    <title><?php echo COMPANY_NAME ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?php echo CSSPATH ?>dashlite.css?ver=1.5.0">
    <link id="skin-default" rel="stylesheet" href="<?php echo CSSPATH ?>theme.css?ver=1.5.0">
    <script type="text/javascript">
        var base_path = "<?php echo BASEPATH  ?>";
        var core_path = "<?php echo COREPATH ?>";
    </script>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-block nk-block-middle nk-auth-body  wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="<?php echo COREPATH ?>" class="logo-link">
                                <img class="logo-light logo-img logo-img-lg" src="<?php echo IMGPATH ?>logo.png" srcset="<?php echo IMGPATH ?>logo2x.png 2x" alt="logo">
                                <img class="logo-dark logo-img logo-img-lg" src="<?php echo IMGPATH ?>logo.png" srcset="<?php echo IMGPATH ?>logo-dark2x.png 2x" alt="logo-dark">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title">Sign-In</h4>
                                        <div class="nk-block-des">
                                            <p>Access the Admin panel using your email and password.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-error"></div>
                                <form action="#" id="login_auth" method="POST">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email or Username</label>
                                        </div>
                                        <input type="text" class="form-control form-control-lg" id="email" name="email" placeholder="Enter your email address or username">
                                    </div>
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">Password</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch" data-target="Password">
                                               
                                            </a>
                                            <input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="Enter your Password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
                                    </div>
                                </form>
                               
                                <div class="form-note-s2 text-center pt-4">  <a href="<?php echo COREPATH ?>home/forgotpassword">Forgot Password?</a>
                                </div> 
                                
                            </div>
                        </div>
                    </div>
                    <div class="nk-footer nk-auth-footer-full">
                        <div class="container wide-lg">
                            <div class="row g-3">
                                <!-- <div class="col-lg-6 order-lg-last">
                                    <ul class="nav nav-sm justify-content-center justify-content-lg-end">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                        
                                    </ul>
                                </div> -->
                                <div class="col-lg-6 order-lg-last">
                                    <div class="nk-block-content text-center text-lg-left">
                                        <p class="text-soft">&copy; <?php echo date("Y").' '.COMPANY_NAME ?>. All Rights Reserved.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="<?php echo JSPATH ?>bundle.js?ver=1.5.0"></script>
    <script src="<?php echo JSPATH ?>scripts.js?ver=1.5.0"></script>
    <script src="<?php echo JSPATH ?>validate.min.js"></script>
    <script src="<?php echo JSPATH ?>user.js"></script>

</html>