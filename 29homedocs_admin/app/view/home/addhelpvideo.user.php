<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">>Upload Video</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Upload Video</h4>
                            </div>
                        </div>
                        <div class="form-error"></div><br>
                        <form id="addGendralDocuments" name="addGendralDocuments" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_gendral_documents_key'] ?>" name="fkey" id="fkey">
                            <div class="row g-gs">
                                <div class="col-lg-5">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="form-group">
                                                <label class="form-label" for="title">Page Title <en>*</en></label>
                                                <input type="text" class="form-control" id="title" name="title" value="<?php echo $data['info']['title'] ?>">
                                            </div>
                                             <?php if ($data['info']['documents']==""){ ?>
                                                    <div class="form-group">
                                                        <label class="form-label" for="attachment">Document <en>*</en></label>
                                                        <input type="file" class="form-control" id="attachment" name="attachment">
                                                        <input type='hidden' value='0' name='file_test'>
                                                    </div>
                                                <?php }else{ ?>
                                                    <div class='form-group row' id='hideSpeakerDefaultFiles'>
                                                        <div class='col-sm-12'>
                                                           <label class="form-label" for="attachment">Document <en>*</en></label>
                                                        </div>
                                                        <div class='col-sm-12'>
                                                        <input type='hidden' id='attachment' name='attachment' value='' />
                                                        <input type='hidden' id='customerImage' name='image' value='<?php echo $data['info']['documents']  ?>'>
                                                        <input type='hidden' value='1' name='file_test'>

                                                            <div class='customer_pic_edit'>
                                                               <img src='<?php echo IMGPATH ?>file.png' width='100px' class=''>
                                                                
                                                                <a target='blank' class='btn btn-success btn-sm' href='<?php echo DOCUMENTS.$data['info']['documents'] ?>'><i class=' icon ni ni-eye'></i></a>
                                                                <button type='button' class='btn btn-danger btn-sm removeGeneralDocument' data-type='image' data-token='<?php echo $data['token'] ?>'><i class='icon ni ni-trash'></i></button>
                                                            </div>
                                                        </div>
                                                </div>           
                                                <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>documents' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>