<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>lead">Manage Lead</a></li>
                                <li class="breadcrumb-item active">Edit Lead</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Edit Lead</h4>
                                
                            </div>
                        </div>
                       
                        <div class="form-error"></div><br>
                        <form id="editLead" name="editLead" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_lead_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                            <div class="row g-gs">
                                <div class="col-lg-4">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Basic Information</h5>
                                            </div>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="">First Name <en>*</en></label>
                                                            <input type="text" class="form-control" id="" name="fname" value="<?php echo $data['info']['fname'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="">Last Name <en></en></label>
                                                            <input type="text" class="form-control" id="" name="flast" value="<?php echo $data['info']['lname'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <label class="form-label" for="company">Lead Source <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="leadsource" name="leadsource" data-placeholder="Select a Lead Source" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                        <?php echo $data['source_list'] ?>
                                                    </select>
                                                </div> 
                                                <div class="form-group">
                                                    <label class="form-label" for="flat_type_id">Select Interested Flat type <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="flat_type_id" name="flat_type_id" data-placeholder="Select a Interested Flat type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                        <?php echo $data['flat_type'] ?>
                                                    </select>
                                                </div> 
                                                <div class="form-group">
                                                    <div class="row g-3 align-center">
                                                        <div class="col-lg-12">
                                                            <label class="form-label">Site Visit Status</label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="yes" checked="" name="site_visit" id="reg-enable1" <?php echo $data['info']['site_visit_status']=='yes' ? 'checked' : '' ?>>
                                                                        <label class="custom-control-label" for="reg-enable1">Yes</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="no"  name="site_visit" id="reg-disable1" <?php echo $data['info']['site_visit_status']=='no' ? 'checked' : '' ?>>
                                                                        <label class="custom-control-label" for="reg-disable1">No</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="customerprofile_id">Lead Profile <en>*</en></label>
                                                    <select class="form-control form-select select2-hidden-accessible" id="customerprofile_id" name="customerprofile_id" data-placeholder="Select a Lead Profile" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                        <?php echo $data['customerprofile_list'] ?>
                                                    </select>
                                                </div> 
                                                <div class="form-group">
                                                    <div class="row g-3 align-center">
                                                        <div class="col-lg-12">
                                                            <label class="form-label">Gender</label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="male"  name="gender" id="reg-enable" <?php echo $data['info']['gender']=='male' ? 'checked' : '' ?>>
                                                                        <label class="custom-control-label" for="reg-enable">Male</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="female"  name="gender" id="reg-disable" <?php echo $data['info']['gender']=='female' ? 'checked' : '' ?>>
                                                                        <label class="custom-control-label" for="reg-disable">Female</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 mb70">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Contact Information</h5>
                                            </div>
                                            
                                                <div class="row g-4">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="email">Primary Email <en>*</en></label>
                                                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $data['info']['email'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="mobile">Primary Phone <en>*</en></label>
                                                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $data['info']['mobile'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="semail">Secondary Email <en></en></label>
                                                            <input type="text" class="form-control" id="semail" name="semail" value="<?php echo $data['info']['secondary_email'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="smobile">Secondary Phone <en></en></label>
                                                            <input type="text" class="form-control" id="smobile" name="smobile" value="<?php echo $data['info']['secondary_mobile'] ?>" >
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="address">Address</label>
                                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $data['info']['address'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="city">City</label>
                                                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $data['info']['city'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="state">State</label>
                                                            <input type="text" class="form-control" id="state" name="state" value="<?php echo $data['info']['state'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="pincode">Pincode</label>
                                                            <input type="text" class="form-control" id="pincode" name="pincode" value="<?php echo $data['info']['pincode'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="pincode">Comments</label>
                                                            <textarea class="form-control" name="description" id="description"><?php echo $data['info']['description'] ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>lead' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>