<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Customer Reports </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Customer Reports</h4>
                            </div>
                        </div>
                        <div class="row g-gs">
                            
                            <div class="col-lg-10">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="customerReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="from_date" name="from_date" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="birth-day">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="to_date" name="to_date" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fv-topics">Select Customer</label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select form-select-lg " id="customer_id
                                                            " name="customer_id" data-placeholder="Select a Customer" >
                                                                <option value=""></option>
                                                                <?php echo $data['cus_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="priority">Priority </label>
                                                        <select class="form-control search" name="priority" id="priority">
                                                           <option value="">Select a Priority</option>
                                                           <option value="low">Low</option>
                                                           <option value="medium">Medium</option>
                                                           <option value="high">High</option>
                                                           <option value="urgent">Urgent</option>
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="form-label" for="priority">KYC Status </label>
                                                        <select class="form-control search" name="kyc_status" id="kyc_status">
                                                           <option value="">Select a KYC Status</option>
                                                           <option value="0">Not submitted</option>
                                                           <option value="1">Approved</option>
                                                           <option value="2">Pending Approval</option>
                                                           <option value="3">Rejected</option>
                                                        </select> 
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>