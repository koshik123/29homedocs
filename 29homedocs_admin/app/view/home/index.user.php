<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">Overview of the Admin Dashboard</h3>
                            <div class="nk-block-des text-soft">
                                <p> Welcome to the Admin Dashboard.</p>
                            </div>
                        </div><!-- .nk-block-head-content -->
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a href="javascript:void();" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-more-v"></em></a>
                                <!-- <div class="toggle-expand-content" data-content="pageMenu">
                                    <ul class="nk-block-tools g-3">
                                        
                                        <li class="nk-block-tools-opt"><a href="<?php echo COREPATH ?>reports" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Reports</span></a></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div><!-- .nk-block-head-content -->
                    </div><!-- .nk-block-between -->
                </div><!-- .nk-block-head -->

                <div class="nk-block">
                    <div class="row g-gs">
                        <div class="col-xxl-12">
                            <div class="row g-gs">
                                <div class="col-lg-12 col-xxl-12">
                                    <div class="row g-gs">
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start ">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Active Units</h6>
                                                        </div>
                                                        <div class="card-tools mr-n1">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="<?php echo COREPATH ?>property"><em class="icon ni ni-building"></em><span>Assign Property</span></a></li>
                                                                        <li><a href="<?php echo COREPATH ?>property/chart"><em class="icon fa fa-pie-chart"></em><span>Property Chart</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['property_count'] ?></span>
                                                            
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start ">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Customers</h6>
                                                        </div>
                                                        <div class="card-tools mr-n1">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="<?php echo COREPATH ?>lead"><em class="icon fa fa-tasks "></em><span>Manage Leads</span></a></li>
                                                                        <li><a href="<?php echo COREPATH ?>customer"><em class="icon ni ni-users"></em><span>Manage Customers</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['customer_count'] ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->
                                        <div class="col-sm-4 col-lg-4 col-xxl-4">
                                            <div class="card card-bordered">
                                                <div class="card-inner">
                                                    <div class="card-title-group align-start ">
                                                        <div class="card-title">
                                                            <h6 class="title">Total Employees</h6>
                                                        </div>
                                                        <div class="card-tools mr-n1">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="<?php echo COREPATH ?>employee"><em class="icon ni ni-users"></em><span>Manage Employees</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="align-end flex-sm-wrap g-4 flex-md-nowrap">
                                                        <div class="nk-sale-data">
                                                            <span class="amount"><?php echo $data['stats']['employee_count'] ?></span>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div><!-- .card -->
                                        </div><!-- .col -->
                                    </div><!-- .row -->
                                </div><!-- .col -->
                            </div><!-- .row -->
                        </div><!-- .col -->
                        <div class="col-md-6 col-xxl-6">
                            <?php if (1!=1): ?>
                            <div class="card card-bordered ">
                                <div class="card-inner-group">
                                    <div class="card-inner">
                                        <div class="card-title-group">
                                            <div class="card-title">
                                                <h6 class="title">New Customers</h6>
                                            </div>
                                            <div class="card-tools">
                                                <a href="<?php echo COREPATH ?>customer" class="link">View All</a>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <?php echo $data['customer'] ?>
                                </div>
                            </div>
                            <?php endif ?>
                            

                            <div class="card card-bordered ">
                                <div class="card-inner">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title"><span class="mr-2">Leads followup status</span> </h6>
                                        </div>
                                        <div class="card-tools">
                                            <a href="<?php echo COREPATH ?>lead" class="link">View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-inner p-0 border-top">
                                    <div class="nk-tb-list nk-tb-orders">
                                        <div class="nk-tb-item nk-tb-head">
                                            <div class="nk-tb-col"><span>UID</span></div>
                                            <div class="nk-tb-col tb-col-sm"><span>Customer</span></div>
                                            <div class="nk-tb-col tb-col-md"><span>Lead Status</span></div>
                                            <div class="nk-tb-col"><span>&nbsp;</span></div>
                                        </div>
                                        <?php echo $data['lead_followup'] ?>                                        
                                    </div>
                                </div>
                                
                            </div>




                            <div class="card card-bordered ">
                                <div class="card-inner border-bottom">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">Pending KYC to follow up</h6>
                                        </div>
                                        <div class="card-tools">
                                            <a href="<?php echo COREPATH ?>customer/kyc" class="link">View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <div class="timeline">
                                        <ul class="timeline-list">
                                            
                                           <?php echo $data['kyc'] ?>
                                        </ul>
                                    </div>
                                </div>
                             </div>
                        </div><!-- .col -->
                        <div class="col-lg-6 col-xxl-6">
                            <div class="card card-bordered h-100">
                                <div class="card-inner border-bottom">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">Open Adhoc Requests</h6>
                                        </div>
                                        <div class="card-tools">
                                            <a href="<?php echo COREPATH ?>adhoc" class="link">All Requests</a>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nk-support">
                                    
                                    <?php echo $data['adhoc'] ?>
                                </ul>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        
                        <div class="col-xxl-12">
                            <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title ">Property Chart </h5>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        
                    </div> <!-- nk-block -->

                    <div class="nk-block">
                        <div class="row g-gs">
                            <?php echo $data['newlist'] ?>
                        </div>
                    </div> 
                        </div>



                        <?php if (1!=1): ?>
                        <div class="col-xxl-12">
                            <div class="card card-bordered card-full">
                                <div class="card-inner">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title"><span class="mr-2">Invoice</span></h6>
                                        </div>
                                        <div class="card-tools">
                                            <ul class="card-tools-nav">
                                                
                                                <li class="active"><a href="<?php echo COREPATH ?>invoice"><span>View all</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="nk-block">
                                    <div class="card card-bordered card-stretch">
                                        <div class="card-inner">
                                            <table class="datatable-init table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Invoice Date</th>
                                                        <th>Invoice ID</th>
                                                        <th>Customer</th>
                                                        <th>Type</th>
                                                        <th>Raised To</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php echo $data['list'] ?> 
                                                </tbody>
                                            </table>
                                        </div> <!-- .card -->
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <?php endif ?>

                        <?php if (1!=1): ?>
                            
                        
                        <div class="col-md-6 col-xxl-4">
                            <div class="card card-bordered card-full">
                                <div class="card-inner border-bottom">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">Recent Activities</h6>
                                        </div>
                                        <div class="card-tools">
                                            <ul class="card-tools-nav">
                                                <li><a href="javascript:void();"><span>Cancel</span></a></li>
                                                <li class="active"><a href="javascript:void();"><span>All</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <ul class="nk-activity">
                                    <li class="nk-activity-item">
                                        <div class="nk-activity-media user-avatar bg-success"><img src="./images/avatar/c-sm.jpg" alt=""></div>
                                        <div class="nk-activity-data">
                                            <div class="label">Keith Jensen requested to Widthdrawl.</div>
                                            <span class="time">2 hours ago</span>
                                        </div>
                                    </li>
                                    <li class="nk-activity-item">
                                        <div class="nk-activity-media user-avatar bg-warning">HS</div>
                                        <div class="nk-activity-data">
                                            <div class="label">Harry Simpson placed a Order.</div>
                                            <span class="time">2 hours ago</span>
                                        </div>
                                    </li>
                                    <li class="nk-activity-item">
                                        <div class="nk-activity-media user-avatar bg-azure">SM</div>
                                        <div class="nk-activity-data">
                                            <div class="label">Stephanie Marshall got a huge bonus.</div>
                                            <span class="time">2 hours ago</span>
                                        </div>
                                    </li>
                                    <li class="nk-activity-item">
                                        <div class="nk-activity-media user-avatar bg-purple"><img src="./images/avatar/d-sm.jpg" alt=""></div>
                                        <div class="nk-activity-data">
                                            <div class="label">Nicholas Carr deposited funds.</div>
                                            <span class="time">2 hours ago</span>
                                        </div>
                                    </li>
                                    <li class="nk-activity-item">
                                        <div class="nk-activity-media user-avatar bg-pink">TM</div>
                                        <div class="nk-activity-data">
                                            <div class="label">Timothy Moreno placed a Order.</div>
                                            <span class="time">2 hours ago</span>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- .card -->
                        </div>
                        
                        
                        <div class="col-lg-6 col-xxl-4">
                            <div class="card card-bordered h-100">
                                <div class="card-inner border-bottom">
                                    <div class="card-title-group">
                                        <div class="card-title">
                                            <h6 class="title">Notifications</h6>
                                        </div>
                                        <div class="card-tools">
                                            <a href="javascript:void();" class="link">View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-inner">
                                    <div class="timeline">
                                        <h6 class="timeline-head">November, 2019</h6>
                                        <ul class="timeline-list">
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-primary is-outline"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-primary"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="timeline-item">
                                                <div class="timeline-status bg-pink"></div>
                                                <div class="timeline-date">13 Nov <em class="icon ni ni-alarm-alt"></em></div>
                                                <div class="timeline-data">
                                                    <h6 class="timeline-title">Submited KYC Application</h6>
                                                    <div class="timeline-des">
                                                        <p>Re-submitted KYC Application form.</p>
                                                        <span class="time">09:30am</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div>
                        <?php endif ?>
                    </div><!-- .row -->
                </div><!-- .nk-block -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>