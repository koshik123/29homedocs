    <?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property/images/<?php echo $data['info']['id'] ?>">Manage Property Gallery</a></li>
                                <li class="breadcrumb-item active">Add Gallery Images</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->

                    <div class="nk-block nk-block-lg">
                        <div class="g-gs">
                            <div class="submit-form minheight300">
                                <form id="addPropertyGalleryImages" method="POST" name="addPropertyGalleryImages" action="#">
                                    <input type="hidden" value="<?php echo $data['token'] ?>" name="token" id="token">
                                    <input type="hidden" value="<?php echo $data['info']['id'] ?>" name="property_id" id="property_id">
                                    <div class="row minheight">
                                        <div class="col-md-6 bgp20">
                                            <div class="">
                                                <h4 class="form-box-title"><?php echo $data['info']['title'] ?></h4>
                                                <div class="row">
                                                    <?php if ($data['images']['count']>0){ ?>
                                                        <ul class="image_list" id="masonry">
                                                           <?php echo $data['images']['layout'] ?>
                                                        </ul>
                                                    <?php }else{ ?>
                                                        <div class='col-md-12'>
                                                            <div class='no_event'>
                                                                 <h1><i class='icon ni ni-img'></i></h1>
                                                                <p>No images added yet</p>
                                                            </div> 
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 bgp20">
                                            <input type="file" multiple="multiple" data-jfiler-extensions="jpg, jpeg, png, gif" name="files[]" id="input2">
                                            <div class="tips_img_up">
                                                <h3>Note:</h3>
                                                <ul>
                                                    <li> Upload Images in a set of 10.</li>
                                                    <li> Upload Images in the form of JPEG, PNG or GIF Formats alone with a max Size of 3.5 MB.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="imgerr"></div>
                                        </div>
                                    </div>
                                    <div class="form_submit_footer">
                                        <div class="form_footer_contents">
                                            <div class="form-group text-right m-b-0">
                                                <a href="<?php echo COREPATH ?>property/images/<?php echo $data['info']['id'] ?>" class="btn btn-danger waves-effect waves-light m-l-5">
                                                    <i class="fa fa-close"></i> Cancel
                                                </a>
                                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                    <i class="fa fa-check"></i> Add Images
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                             
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>



<?php include 'includes/bottom.html'; ?>