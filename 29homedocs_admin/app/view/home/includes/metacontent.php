<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="<?php echo IMGPATH ?>favicon.png">
    <!-- Page Title  -->
    <title><?php echo $data['page_title'] ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?php echo CSSPATH ?>dashlite.css?ver=1.5.0">
    <link rel="stylesheet" href="<?php echo CSSPATH ?>custom.css">
    <link id="skin-default" rel="stylesheet" href="<?php echo CSSPATH ?>theme.css?ver=1.5.0">
    <link href="<?php echo PLUGINS ?>sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo PLUGINS ?>noty/noty.css">
    <link href="<?php echo PLUGINS ?>bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <script type="text/javascript" src="<?php echo PLUGINS ?>autocomplete/jquery-ui.css"></script>
    <link href="<?php echo CSSPATH ?>icons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo PLUGINS ?>summernote/dist/summernote.css" rel="stylesheet" />
    <script type="text/javascript">
        var base_path = "<?php echo BASEPATH ?>";
        var core_path = "<?php echo COREPATH ?>";
    </script>

    <?php if (isset($data['scripts'])) { ?>
        <?php if ($data['scripts']=="home" ) { ?>
           
        <?php } ?> 
        <?php if ($data['scripts']=="addimages"  ) { ?>
            <link href="<?php echo PLUGINS ?>multifile/jquery.filer.css" type="text/css" rel="stylesheet" />
            <link href="<?php echo PLUGINS ?>multifile/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" /> 


            <link href="<?php echo PLUGINS ?>multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
            <link href="<?php echo PLUGINS ?>select2/select2.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo PLUGINS ?>bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
            <link href="<?php echo PLUGINS ?>bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
            <link href="<?php echo PLUGINS ?>custombox/dist/custombox.min.css" rel="stylesheet">
            <link href="<?php echo PLUGINS ?>summernote/dist/summernote.css" rel="stylesheet" />
        <?php } ?> 

        <?php if ($data['scripts']=="managelead" || $data['scripts']=="leadreports" || $data['scripts']=="managecustomer" || $data['scripts']=="importdata") { ?>
            <link rel="stylesheet" href="<?php echo PLUGINS ?>datatables-net/media/css/dataTables.bootstrap4.min.css">

            <!-- Responsive datatable examples -->
            <link href="<?php echo PLUGINS ?>datatable/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
            <!-- DataTables -->
            <link href="<?php echo PLUGINS ?>datatable/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo PLUGINS ?>datatable/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

            <link rel="stylesheet" href="<?php echo PLUGINS ?>datatables-net/datatables-net/datatables.min.css">
        <?php } ?>

    <?php } ?>  
</head>
<!-- <body class="nk-body bg-lighter npc-general has-sidebar "> -->
    <body class="nk-body npc-invest bg-lighter ">