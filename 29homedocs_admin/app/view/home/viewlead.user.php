<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>lead">Manage Lead</a></li>
                                <li class="breadcrumb-item active">Lead Info </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-4">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Lead Info </h4>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="float_right">

                                        <?php if ($data['info']['closed_status']==0){ ?>  
                                            <a href="#"  class="btn btn-primary updateCompletedStatus" data-option='<?php echo $data['token'] ?>'><em class="icon ni ni-check"></em> <span>Mark as completed</span></a>
                                        <?php }else{ ?>    
                                            <!-- <a href="#"  class="btn btn-primary updateCompletedStatus" data-option='<?php echo $data['token'] ?>'><em class="icon ni ni-repeat"></em> <span>Undo Completed Status</span></a> -->
                                            <a href="#"  class="btn btn-primary moveLeadToCustomer" data-option='<?php echo $data['token'] ?>'><em class="icon ni ni-check"></em> <span>Move Lead to Customer</span></a>
                                        <?php } ?> 

                                        <?php echo $data['assign_btn'] ?>

                                        <a href="#"  class="btn btn-info " data-toggle="modal" data-target="#updateLeadStatusmodel"><em class="icon ni ni-check"></em> <span>Update Lead Status</span></a>
                                        <a href="#"  class="btn btn-warning " data-toggle="modal" data-target="#addactivity"><em class="icon ni ni-plus"></em> <span>Activity</span></a>

                                   <!--  <?php if ($data['info']['kyc_status']!=1){ ?>                                        
                                        
                                        <a href="#"  class="btn btn-danger approverejectkyc " data-value='3' data-option='<?php echo $data['info']['id'] ?>'><em class="icon ni ni-info"></em> <span>Reject</span></a>
                                    <?php }else{ ?>                                        
                                        <a href="#"  class="btn btn-danger approverejectkyc " data-value='3' data-option='<?php echo $data['info']['id'] ?>'><em class="icon ni ni-info"></em> <span>Reject</span></a>
                                    <?php } ?> -->
                                        <button class="btn btn-danger" onclick="window.close();" ><em class="icon ni ni-cross"></em> <span>Close</span></button>
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-4">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">General Info</h5>
                                        
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">ID</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['uid']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Lead Date</div>
                                                <div class="data-value"><?php echo $data['info']['lead_date']=="" ? '' : date("d M, Y",strtotime($data['info']['lead_date'])) ?></div>
                                            </div>
                                        </li>

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Name</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['fname'].' '.$data['info']['lname']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Gender</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['gender']) ?></div>
                                            </div>
                                        </li>
                                       <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Auth Type</div>
                                                <div class="data-value"><?php echo $data['info']['auth_type']=="" ? '-' : ucwords($data['info']['auth_type']) ?></div>
                                            </div>
                                        </li>
                                       
                                       
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Contact Details</h5>
                                        
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Primary Mobile</div>
                                                <div class="data-value"><?php echo ($data['info']['mobile']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Primary Email</div>
                                                <div class="data-value "><em><?php echo ($data['info']['email']) ?></em></div>
                                            </div>
                                        </li>

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Secondary Mobile</div>
                                                <div class="data-value"><?php echo ($data['info']['secondary_mobile']) ?></div>
                                            </div>
                                        </li>
                                       

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Secondary Email</div>
                                                <div class="data-value"><?php echo ($data['info']['secondary_email']) ?></div>
                                            </div>
                                        </li>


                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Address</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['address'].' '.$data['info']['city'].' '.$data['info']['state'].' '.$data['info']['pincode']) ?></div>
                                            </div>
                                        </li>
                                        
                                        
                                        
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Lead Info</h5>
                                        
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Lead Source</div>
                                                <div class="data-value"><?php echo ucwords($data['leadSourceinfo']['lead_source']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Lead Status</div>
                                                <div class="data-value"><?php echo ucwords($data['leadstatusinfo']['lead_type']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Assigned To</div>
                                                <div class="data-value"><?php echo ucwords($data['assigned_to']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Completed Status</div>
                                                <div class="data-value"><?php echo $data['info']['closed_status']=="1" ? '<span class="badge badge-success">Completed</span>' : '<span class="badge badge-warning">Processing</span>' ?>  </div>
                                            </div>
                                        </li>
                                        <?php if ($data['info']['closed_status']=="1"): ?>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Lead Moved Status</div>
                                                    <div class="data-value"><?php echo $data['info']['moved_status']=="1" ? '<span class="badge badge-success">Moved</span>' : '<span class="badge badge-warning">Not Moved</span>' ?></div>
                                                </div>
                                            </li>
                                        <?php endif ?>
                                        
                                    </ul>
                                </div>
                            </div>
                            <!-- .col -->
                            <div class="col-lg-8">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Activity Info</h5>
                                        
                                    </div>
                                </div>
                                <div class="card card-bordered card-preview">
                                    <div class="card-inner">
                                        <table class="datatable-init table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date </th>
                                                    <th>Activity Type Master</th>
                                                    <th>Remarks </th>
                                                    <th>Created By </th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php echo $data['newactivity_logs'] ?> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-lg-6">
                                <div class="title-box">
                                   <h5 class="table-header">
                                           Activity  
                                    </h5>
                                </div>
                                <?php if($data['activity_logs']!=""){ ?>
                                    <section id="cd-timeline" class="cd-container">
                                        <?php echo $data['activity_logs'] ?>    
                                    </section>
                                <?php }else{ ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h6>No activities found..!</h6>
                                        </div>                                        
                                    </div>
                                <?php } ?> 
                            </div> --><!-- .col -->
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade " tabindex="-1" role="dialog" id="updateLeadStatusmodel">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateLeadStatus"  id="updateLeadStatus">
                <input type='hidden' value='<?php echo $_SESSION['update_leadstatus_key'] ?>' name='fkey' id='fkey'>
                <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                <input type="hidden" name="token" id="token" value="<?php echo $data['info']['id'] ?>">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">Update Lead Status</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <!-- <div class="kycInfo"></div> -->
                                   <div class="form-group">
                                        <label class="form-label" for="company">Lead Status <en>*</en></label>
                                        <select class="form-control form-select select2-hidden-accessible" id="leadstatus_id" name="leadstatus_id" data-placeholder="Select a Lead Status" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                            <?php echo $data['leadstatus'] ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="addactivity">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="addleadActivity"  id="addleadActivity">
                <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                <input type="hidden" name="token" id="token" value="<?php echo $data['info']['id'] ?>">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">Add Activity</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p >
                                            <em class="icon ni ni-user"></em> <span id="activityName" style="margin-right: 10px;"><?php echo $data['info']['fname'].' '.$data['info']['lname'] ?></span> 
                                            <em class="icon ni ni-mobile"></em> <span id="activityMobile" style="margin-right: 10px;"><?php echo $data['info']['mobile'] ?></span> 
                                            <em class="icon ni ni-mail"></em> <span id="activityEmail"><?php echo $data['info']['email'] ?></span>
                                        </p>
                                    </div>
                                </div>
                               <!--  <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityName">Lead Name <en>*</en></label>
                                        <input type="text" class="form-control" id="activityName" name="lead_name" value="<?php echo $data['info']['fname'].' '.$data['info']['lname'] ?>" disabled="">
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityMobile">Mobile <en>*</en></label>
                                        <input type="text" class="form-control" id="mobactivityMobileile" name="mobile" disabled="" value="<?php echo $data['info']['mobile'] ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label" for="activityEmail">Email <en>*</en></label>
                                        <input type="email" class="form-control" id="activityEmail" name="email" disabled="" value="<?php echo $data['info']['email'] ?>">
                                    </div>
                                </div> -->


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="activity_id">Select Activity <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="activity_id" name="activity_id" data-placeholder="Select a Activity Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['activity_types'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Activity Date <en>*</en></label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="act_date" id="act_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade editactivity_model" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateLeadActivity"  id="updateLeadActivity">
               <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade addAssignLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Assign Lead to Employee</h5>
                <form  id="assignLead" name="assignLead" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="fkey1">
                    <input type="hidden" name="lead_id" id="token1">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <p >
                                            <em class="icon ni ni-user"></em> <span id="activityName1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mobile"></em> <span id="activityMobile1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mail"></em> <span id="activityEmail1"></span>
                                        </p>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="lead_name">Lead Name <en>*</en></label>
                                        <input type="text" class="form-control" id="lead_name" name="lead_name">
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="mobile">Mobile <en>*</en></label>
                                        <input type="text" class="form-control" id="mobile" name="mobile">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="email">Email <en>*</en></label>
                                        <input type="email" class="form-control" id="email" name="email">
                                    </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="employee_id">Select Employee <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['emp_list'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="priority">Lead Priority <en>*</en></label>
                                        <select class="form-control " name="priority" id="priority">
                                           <option value="low">Low</option>
                                           <option value="medium">Medium</option>
                                           <option value="high">High</option>
                                           <option value="urgent">Urgent</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade ReassignLeadModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form  id="reassignLead" name="reassignLead" method="POST" action="#" enctype="multipart/form-data">
                <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>




<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead Status updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['la'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Activity added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>