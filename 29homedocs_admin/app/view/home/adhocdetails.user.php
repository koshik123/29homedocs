<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>adhoc">Adhoc requests</a></li>
                                <li class="breadcrumb-item active"><?php echo $data['info']['ticket_uid'] ?></li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Adhoc requests</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                        <?php 
                                            echo $assign_btn = (($data['info']['assign_status']=="0")? "
                                            <div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-info assignAdhocModel' data-value='' data-type='".$data['info']['id']."' data-option='".$data['info']['id']."'> Assign</a></div>" : "
                                                Assign To: ".$data['assigned_to']."

                                            <div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignAdhocModel' data-value='' data-type='".$data['info']['id']."' data-option='".$data['info']['id']."'> Reassign</a></div>" );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        

                        <div class="nk-block nk-block-lg minheight">
                            <div class="card card-bordered">
                                <div class="card-inner">
                                    <div class="ticket-msgs">
                                        <?php if ($data['info']['assign_status']=="0"){ ?>
                                        <div class="nk-reply-body">
                                            <div class="nk-reply-entry entry note">
                                                <p>Assign to the Employee to reply or close this Adhoc...<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-info assignAdhocModel' data-value='' data-type='<?php echo $data['info']['id'] ?>' data-option='<?php echo $data['info']['id'] ?>'> Assign</a></div></p>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <?php  echo $data['list'] ?> 
                                        <?php  echo $data['replay_list'] ?>

                                        
                                        <?php if ($data['info']['assign_status']=="1"){ ?>

                                            <div class="ticket-msg-reply">
                                                <?php if ($data['info']['request_status']=='0'){ ?>
                                                    <h5 class="title">Reply</h5>
                                                    <form id="replayAdhocRequest" name="replayAdhocRequest" method="POST" action="#" enctype="multipart/form-data">
                                                        <input type="hidden" value="<?php echo $_SESSION['reply_ticket_key'] ?>" name="fkey" id="fkey">
                                                        <input type="hidden" value="<?php echo $data['info']['id'] ?>" name="ref_id" id="ref_id">
                                                        
                                                        <div class="form-group">
                                                            <div class="form-editor-custom">
                                                                <textarea class="form-control" name="remarks" id="remarks" placeholder="Write a message..." required=""></textarea>
                                                            </div>
                                                            <table class="table  " id="adhoccontent">
                                                                <tbody>
                                                                    <?php $adhoc_row  = 0; ?>
                                                                </tbody>
                                                            </table>
                                                            <div class="form-editor-action">
                                                                <ul class="form-editor-btn-group">
                                                                    <div class="post_box_footer">
                                                                        <div class="selector">
                                                                            <div class="attachment">
                                                                                <ul>
                                                                                    <li>
                                                                                        <button type="button" class="tooltips img_button" data-toggle="tooltip" data-placement="top"  onclick="addProperty();" data-original-title="Add Attachment" data-option="1" id="btn1"  style="background: #ffff;border: #fff;font-size: 12px;"><i class="icon ni ni-clip-h"></i> <span class="add_attachment">Add Attachment</span></button> 
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </ul>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-action">
                                                            <ul class="form-btn-group">
                                                                <li class="form-btn-primary"><button type="submit" class="btn btn-primary">Send</a></li>
                                                                <li class="form-btn-secondary "><a href="javascript:void();" class="btn btn-dim btn-outline-light closeTicket " data-option='<?php echo $data['token'] ?>'>Mark as close</a></li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </form>
                                                <?php }else{ ?>
                                                     <div class="form-action">
                                                        <ul class="form-btn-group">
                                                            <li class="form-btn-secondary"><a href="javascript:void();" class="btn btn-dim btn-outline-light"> Ticket Closed</a></li>
                                                            <li class="form-btn-primary "><a href="javascript:void();" class="btn btn-info   closeTicket " data-option='<?php echo $data['token'] ?>'><i class='icon ni ni-undo'></i>  Undo</a></li>
                                                        </ul>
                                                    </div>
                                                <?php } ?>
                                            </div><!-- .ticket-msg-reply -->
                                        <?php }else{ ?>                                      
                                                
                                        <?php } ?>
                                    </div><!-- .ticket-msgs -->
                                </div>
                            </div>
                        </div><!-- .nk-block -->
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade addAssignAdhocModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Assign Adhoc to Employee</h5>
                <form  id="assignAdhoc" name="assignAdhoc" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="fkey1">
                    <input type="hidden" name="adhoc_id" id="token1">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p >
                                            <em class="icon ni ni-user"></em> <span id="activityName1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mobile"></em> <span id="activityMobile1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mail"></em> <span id="activityEmail1"></span>
                                        </p>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="employee_id">Select Employee <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['emp_list'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="priority">Lead Priority <en>*</en></label>
                                        <select class="form-control " name="priority" id="priority">
                                           <option value="low">Low</option>
                                           <option value="medium">Medium</option>
                                           <option value="high">High</option>
                                           <option value="urgent">Urgent</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade ReassignAdhocModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form  id="reassignAdhoc" name="reassignAdhoc" method="POST" action="#" enctype="multipart/form-data">
                <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>



<?php include 'includes/bottom.html'; ?>