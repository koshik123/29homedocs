<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>invoice">Invoices / Receipts</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['inv_number']) ?></li>
                            </ul>
                            
                        </nav>
                        
                        
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                               
                                <div class="col-12">
                                    <div class="float_right">
                                        <a href="<?php echo COREPATH ?>invoice/add" class="btn btn-primary"  ><em class="icon ni ni-plus"></em> <span> Add</span></a>
                                    <a href="<?php echo COREPATH ?>invoice/edit/<?php echo $data['info']['id'] ?>" class="btn btn-success"  ><em class="icon ni ni-edit"></em> <span> Edit</span></a>
                                    <button class="btn btn-danger" onclick="window.close();" ><em class="icon ni ni-cross"></em> <span>Close</span></button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Invoice Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Invoice ID</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['inv_uid']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Invoice / Receipt Number</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['inv_number']) ?></div>
                                            </div>
                                        </li>
                                        
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Invoice Date</div>
                                                <div class="data-value"><?php echo date("d M, Y h:i a",strtotime($data['info']['inv_date'])) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Invoice Type</div>
                                                <div class="data-value">
                                                    <?php echo ucwords($data['info']['type']) ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Raised to</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['raised_to']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">File</div>
                                                <div class="data-value">
                                                    <a href="<?php echo DOCUMENTS ?><?php echo $data['info']['documents'] ?>" target="_blank" class="btn btn-info btn-sm" > File</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Uploaded <?php echo ucwords($data['info']['type']) ?> File</h5>
                                        <p>Here is user uploaded documents.</p>
                                    </div>
                                </div><!-- .nk-block-head -->
                                

                            </div><!-- .col -->
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                        
                                                <h5 class="nk-block-title title">Customer Info</h5>  
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">ID</div>
                                                        <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>customer/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['cusinfo']['uid']) ?></a></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Name</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Assign Property</div>
                                                        <?php if ($data['cusinfo']['assign_status']==1) { ?>
                                                           <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>property/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['propertyinfo']['title']) ?></a></div>
                                                        <?php }else{ ?>
                                                            <div class="data-value"><span class="badge badge-info">Un Assign</span></div>
                                                        <?php } ?>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Email Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Mobile</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                                    </div>
                                                </li>
                                                
                                            </ul>
                                        </div>
                            </div>
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>



<?php include 'includes/bottom.html'; ?>




