<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property </a></li>
                                <li class="breadcrumb-item active"> Private Gallery for <?php echo ucwords($data['info']['title']) ?></li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block">
                            <div class="row gy-5">
                                <div class="col-lg-6">
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">
                                            <h5 class="nk-block-title title">Property Info</h5>
                                        </div>
                                    </div>
                                    <div class="card card-bordered">
                                        <ul class="data-list is-compact">
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Title</div>
                                                    <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>property/details/<?php echo $data['info']['id']  ?>"><?php echo ucwords($data['info']['title']) ?></a> </div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Type</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['type']) ?></div>
                                                </div>
                                            </li>
                                            
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Flat Variant</div>
                                                    <div class="data-value"><?php echo ucwords($data['roominfo']['flat_variant']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Blook</div>
                                                    <div class="data-value"><?php echo ucwords($data['blockinfo']['block']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Floor</div>
                                                    <div class="data-value"><?php echo ucwords($data['floorinfo']['floor']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Cost </div>
                                                    <div class="data-value">Rs. <?php echo ucwords($data['info']['cost']) ?></div>
                                                </div>
                                            </li>
                                            
                                            
                                        </ul>
                                    </div>
                                </div><!-- .col -->
                                <?php if ($data['info']['customer_id']=='0') { ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                                            
                                                <h5 class="nk-block-title title">Other Info</h5>
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Size</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectsize']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Area</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectarea']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">City</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">State</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['state']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Pincode</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['pincode']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Customer Status</div>
                                                    <div class="data-value"><span class="badge badge-info">Un Assign</span></div>
                                                </div>
                                            </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                        
                                                <h5 class="nk-block-title title">Customer Info</h5>  
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">ID</div>
                                                        <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>customer/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['cusinfo']['uid']) ?></a></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Name</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Email Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Mobile</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">City</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?>                                
                            </div><!-- .row -->
                        </div>
                        <div class="row gy-5">
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content float_left">
                                        <h4 class="title nk-block-title">Private Images</h4>
                                    </div>
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>property/addimages/<?php echo $data['info']['id'] ?>" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Images</span></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-error"></div>
                                <div class="row g-gs">

                                    <div class="col-lg-12">

                                        <div class="row">
                                            <?php if ($data['images']['count']>0){ ?>
                                                <ul class="image_list" id="masonry">
                                                   <?php echo $data['images']['layout'] ?>
                                                </ul>
                                            <?php }else{ ?>
                                                <div class='col-md-12'>
                                                    <div class='no_event'>
                                                        <h1><i class='icon ni ni-img'></i></h1>
                                                        <p>No images added yet</p>
                                                    </div> 
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>                                   
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Room Images</h4>
                                    </div>
                                </div>
                                <div class="row g-gs">
                                    <div class="col-lg-12">
                                        <div class="card card-bordered card-preview">
                                            <div class="card-inner">
                                                <table class="datatable-init table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Room Name</th>
                                                            <th>Total Images</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $data['room_list'] ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div>
                                    
                                    
                                </div>

                            </div>
                        </div>
                        
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="file-upload">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <div class="nk-upload-form">
                        <h5 class="title mb-3">Upload File</h5>
                        <div class="upload-zone small bg-lighter">
                            <div class="dz-message" data-dz-message>
                                <span class="dz-message-text"><span>Drag and drop</span> file here or <span>browse</span></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="nk-modal-action justify-end">
                        <ul class="btn-toolbar g-4 align-center">
                            <li><a href="javascript:void();" class="link link-primary">Cancel</a></li>
                            <li><button class="btn btn-primary">Add Files</button></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['i'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Images added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['r'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Room images added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>