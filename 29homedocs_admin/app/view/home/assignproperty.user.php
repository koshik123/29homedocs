<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>customer">Manage Customer</a></li>
                                <li class="breadcrumb-item active">Assign Flat/Villa</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <form id="assignPropertyToCustomer" name="assignPropertyToCustomer" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['assign_property_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                            <div class="row g-gs">
                                <div class="col-lg-7">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Assign Flat/Villa</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="name">Select Flat/Villa <en>*</en></label>
                                                        <select class='form-control form-select ' id='property_id' name='property_id' data-placeholder='Select a Flat/Villa' required='' data-select2-id='fva-topics' tabindex='-1' aria-hidden='true'>
                                                            <?php echo $data['property_list'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label class="form-label">Date of Allotment <en>*</en></label>
                                                            <div class="form-control-wrap">
                                                                <input type="text" name="allotment_date" id="allotment_date" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="mobile">Remarks <en></en></label>
                                                        <textarea  class="form-control" id="assign_remarks" name="assign_remarks"></textarea> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>customer' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

