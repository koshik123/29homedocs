<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Adhoc Requests  </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Adhoc Requests</h4>
                                    </div>
                                </div>
                                 <div class="col-5">
                                    <div class="float_right">
                                        <div class="card card-bordered card-stretch" style="float: left;margin-right:10px ">
                                            <div class="card-inner-group">
                                                <div class="position-relative card-tools-toggle" data-select2-id="23">
                                                    <div class="card-title-group" data-select2-id="22">
                                                        <div class="card-tools mr-n1" data-select2-id="21">
                                                            <ul class="btn-toolbar gx-1" data-select2-id="20">
                                                                <li data-select2-id="19">
                                                                    <div class="toggle-wrap" data-select2-id="18">
                                                                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-menu-right"></em></a>
                                                                        <div class="toggle-content" >
                                                                            <form role="form" method="POST" action="#" id="adhocAllFilter">   
                                                                                <input type="hidden" name="requeststatus" id="requeststatus" value="<?php echo $data['request_status'] ?>">
    <ul class="btn-toolbar gg gx-1" data-select2-id="16">
        <li data-select2-id="15">
            <div class="dropdown" data-select2-id="14">
                <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    
                    <em class="icon ni ni-filter-alt"></em>  Filter
                </a> 
                <div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right" style="">
                    <div class="dropdown-head">
                        <span class="sub-title dropdown-title">Adhoc Request Filter </span>
                    </div>
                    <div class="dropdown-body dropdown-body-rg">
                        <div class="row gx-6 gy-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Department</label>
                                    <select class="form-select " name="department_id" id="department_id" data-placeholder='Select Department'>
                                        <option value="">Select Department</option>
                                        <?php echo $data['department_list'] ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Status</label>
                                    <select class="form-select " name="status" id="status" data-placeholder='Select Status'>
                                       <option value="">Select Status</option>
                                       <option value="0">Open</option>
                                       <option value="1">Closed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Assigned Status</label>
                                    <select class="form-select " name="assignstaus" id="assignstaus" data-placeholder='Select Assigned Status'>
                                       <option value="">Select Assigned Status</option>
                                       <option value="1">Assign</option>
                                       <option value="0">Unassign</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Assigned To</label>
                                    <select class="form-select " name="assigned_to" id="assigned_to" data-placeholder='Select Assigned To'>
                                        <option value="">Select Assigned To</option>
                                        <?php echo $data['emp_list'] ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group float_right">
                                    <button type="submit" class="btn btn-secondary">Filter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div><!-- .filter-wg -->
            </div><!-- .dropdown -->
        </li>
    </ul>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-search search-wrap" data-search="search"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo COREPATH ?>adhoc/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add a Request </span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Ticket</th>
                                            <th>Customer </th>
                                            <th>Subject</th>
                                            <th>Request Type</th>
                                            <th>Department</th>
                                            <th>Submitted</th>
                                            <th >Assigned To</th>
                                            <th>Raised by</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade addAssignAdhocModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Assign Adhoc to Employee</h5>
                <form  id="assignAdhoc" name="assignAdhoc" method="POST" action="#" enctype="multipart/form-data">
                    <input type="hidden" name="fkey" id="fkey1">
                    <input type="hidden" name="adhoc_id" id="token1">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p >
                                            <em class="icon ni ni-user"></em> <span id="activityName1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mobile"></em> <span id="activityMobile1" style="margin-right: 10px;"></span> 
                                            <em class="icon ni ni-mail"></em> <span id="activityEmail1"></span>
                                        </p>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="employee_id">Select Employee <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="employee_id" name="employee_id" data-placeholder="Select a Employee" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                <?php echo $data['emp_list'] ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="priority">Lead Priority <en>*</en></label>
                                        <select class="form-control " name="priority" id="priority">
                                           <option value="low">Low</option>
                                           <option value="medium">Medium</option>
                                           <option value="high">High</option>
                                           <option value="urgent">Urgent</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="remarks">Remarks
                                            <en>*</en></label>
                                         <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 float_right">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-danger " data-dismiss="modal">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </form>
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade ReassignAdhocModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form  id="reassignAdhoc" name="reassignAdhoc" method="POST" action="#" enctype="multipart/form-data">
                <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Submitted successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>


<?php if (isset($_GET['as'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Successfully Assigned! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['rs'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Successfully Re-Assigned! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>