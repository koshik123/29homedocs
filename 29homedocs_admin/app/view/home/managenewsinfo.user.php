<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage News Information </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage News Information</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>newsinfo/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add News Information</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Title</th>
                                            <th>Project</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>20/05/2020</td>
                                            <td>Sample Title</td>
                                            <td><a href="javascript:void();">Project 1</a></td>
                                            <td><div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck7" checked>
                                                    <label class="custom-control-label" for="customCheck7">Inactive</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-success"><em class="icon ni ni-edit"></em></a>
                                                </div>
                                                
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-warning"><em class="icon ni ni-eye"></em></a>
                                                </div>
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-danger"><em class="icon ni ni-trash"></em></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>



<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-lg">
                    <h5 class="title">Update New Password</h5>
                   
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="full-name">New Password</label>
                                        <input type="password" class="form-control form-control-lg" id="full-name" value="" placeholder="Enter New Password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="display-name">Confirm Password</label>
                                        <input type="password" class="form-control form-control-lg" id="display-name" value="" placeholder="Enter Confirm Password">
                                    </div>
                                </div>
                               
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-primary">Update Password</a>
                                        </li>
                                        <li>
                                            <a href="#" data-dismiss="modal" class="link link-light">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>