<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active">Add Employee</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <form id="addEmployee" name="addEmployee" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_employee_key'] ?>" name="fkey" id="fkey">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Add Employee</h4>
                                </div>
                            </div>
                            <div class="row g-gs">
                                <div class="col-lg-12">
                                    <div class="form-error"></div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Basic Information</h5>
                                            </div>
                                            
                                                <div class="form-group">
                                                    <label class="form-label" for="name">Employee name <en>*</en></label>
                                                    <input type="text" class="form-control" id="name" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Employee Role <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="role_id" name="role_id" data-placeholder="Select a Employee Role" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['emp_role'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Department <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="department_id" name="department_id" data-placeholder="Select a Department" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['department_list'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row g-3 align-center">
                                                        <div class="col-lg-12">
                                                            <label class="form-label">Is Manager</label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" checked="" name="is_manager" id="is_manager1" value="1">
                                                                        <label class="custom-control-label" for="is_manager1">Yes</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" name="is_manager" id="is_manager2" value="0">
                                                                        <label class="custom-control-label" for="is_manager2">No</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Contact Information</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <div class="row g-3 align-center">
                                                            <div class="col-lg-12">
                                                                <label class="form-label">Gender</label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" checked="" name="gender" id="reg-enable" value="male">
                                                                            <label class="custom-control-label" for="reg-enable">Male</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" name="gender" id="reg-disable" value="female">
                                                                            <label class="custom-control-label" for="reg-disable">Female</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Login Password <en>*</en></label>
                                                        <input type="password" class="form-control" id="password" name="password" >
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="email">Email <en>*</en></label>
                                                        <input type="text" class="form-control" id="email" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="mobile">Mobile <en>*</en></label>
                                                        <input type="text" class="form-control" id="mobile" name="mobile">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="address">Address</label>
                                                        <input type="text" class="form-control" id="address" name="address">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="city">City</label>
                                                        <input type="text" class="form-control" id="city" name="city">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="state">State</label>
                                                        <input type="text" class="form-control" id="state" name="state">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="pincode">Pincode</label>
                                                        <input type="text" class="form-control" id="pincode" name="pincode">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>employee' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>