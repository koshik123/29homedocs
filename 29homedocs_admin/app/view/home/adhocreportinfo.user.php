<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>reports/adhoc">Adhoc Reports</a></li>
                                <li class="breadcrumb-item active"><strong><?php echo date("d M'y ", strtotime($data['from'])).' to '.date("d M'y ", strtotime($data['to'])) ?></strong>     </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Filter</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs ">
                            
                            <div class="col-lg-12">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <form id="adhocReport" method="POST" action="#">
                                            <div class="row gy-4">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fromdate">From Date <en>*</en></label>
                                                        <input type="text" class="form-control  date-picker" id="fromdate" name='from_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="todate">To Date <en>*</en></label>
                                                        <input type="text" class="form-control date-picker" id="todate" name='to_date' placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Select Customer </label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-top" name="customer_id" data-placeholder="Select a Customer" >
                                                                <option value="">Select a Customer</option>
                                                                <?php echo $data['customer_list'] ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Status <en></en></label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="status" data-placeholder="Select a Status">
                                                                <option value="">Select a Status</option>
                                                                <option value="0" >Open</option>
                                                                <option value="1" >Close</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group float_right">
                                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php if ($data['from']!="" && $data['to']!="" ){ ?>
                                <div class="daybook_report-form">
                                    <h3 style="font-size: 21px;"> <i class="fa fa-file-o"></i> Showing Results From - <strong><?php echo date("d M'y ", strtotime($data['from'])) ?></strong>  to <strong> <?php echo date("d M'y ", strtotime($data['to']))?></strong>
                                        <?php if ($data['cus_id']!=""){ ?>
                                            , <strong>Customer:</strong>  <?php echo ucwords($data['cusname'])?>
                                        <?php } ?>
                                        <?php if ($data['status']!=""){ ?>
                                            , <strong>Status:</strong> <?php echo $data['status']=='1' ? "Closed" : "Open" ?>
                                        <?php } ?>

                                      </h3>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="col-lg-12">
                            <div class="card card-bordered card-preview">
                                <div class="card-inner">
                                    
                                    <table class="datatable-init table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Ticket</th>
                                                <th>Customer </th>
                                                <th>Subject</th>
                                                <th>Request Type</th>
                                                <th>Submitted</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $data['list'] ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- .nk-block -->
                        </div>
                        </div>

                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>