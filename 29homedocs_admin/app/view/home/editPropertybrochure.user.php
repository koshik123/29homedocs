<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>propertybrochure"> Property Brochure</a></li>
                                <li class="breadcrumb-item active">Edit Property Brochure</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                    <form id="editPropertyBroucher" name="editPropertyBroucher" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['edit_property_broucher_key'] ?>" name="fkey" id="fkey">
                        <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">   
                        <div class="row g-gs">
                            <div class="col-lg-8">
                                <div class="card card-bordered ">
                                    <div class="card-inner">
                                        <div class="card-head">
                                            <h5 class="card-title">Edit Property Brochure</h5>
                                        </div>
                                        <div class="row g-4">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="title">Title <en>*</en></label>
                                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $data['info']['title'] ?>">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Project <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="project_id" name="project_id" data-placeholder="Select a Project" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['property_list'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <?php if ($data['info']['brochures']==""){ ?>
                                                    <div class="form-group">
                                                    <label class="form-label" for="attachment">Upload Brochures <en>*</en></label>
                                                    <input type="file" class="form-control" id="attachment" name="attachment">
                                                    <input type='hidden' value='0' name='file_test'>
                                                </div>
                                                <?php }else{ ?>
                                                    <div class='form-group row' id='hideSpeakerDefaultFiles'>
                                                        <div class='col-sm-12'>
                                                           <label class="form-label" for="attachment">Document <en>*</en></label>
                                                        </div>
                                                        <div class='col-sm-12'>
                                                        <input type='hidden' id='attachment' name='attachment' value='' />
                                                        <input type='hidden' id='customerImage' name='image' value='<?php echo $data['info']['brochures']  ?>'>
                                                        <input type='hidden' value='1' name='file_test'>

                                                            <div class='customer_pic_edit'>
                                                               <img src='<?php echo IMGPATH ?>file.png' width='100px' class=''>
                                                                
                                                                <a target='blank' class='btn btn-success btn-sm' href='<?php echo DOCUMENTS.$data['info']['brochures'] ?>'><i class=' icon ni ni-eye'></i></a>
                                                                <button type='button' class='btn btn-danger btn-sm removeBroucher' data-type='image' data-token='<?php echo $data['token'] ?>'><i class='icon ni ni-trash'></i></button>
                                                            </div>
                                                        </div>
                                                </div>           
                                                <?php } ?>
                                                
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="discription">Description</label>
                                                    <textarea class="form-control form-control-sm" id="description" name="description" placeholder="Write your message"><?php echo $data['info']['description'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>propertybrochure' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="file-upload">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <div class="nk-upload-form">
                        <h5 class="title mb-3">Upload File</h5>
                        <div class="upload-zone small bg-lighter">
                            <div class="dz-message" data-dz-message>
                                <span class="dz-message-text"><span>Drag and drop</span> file here or <span>browse</span></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="nk-modal-action justify-end">
                        <ul class="btn-toolbar g-4 align-center">
                            <li><a href="javascript:void();" class="link link-primary">Cancel</a></li>
                            <li><button class="btn btn-primary">Add Files</button></li>
                        </ul>
                    </div>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

<?php include 'includes/bottom.html'; ?>