<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item active">Cancel Property</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="row g-gs">
                            
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Cancel Reports</h4>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <div class="card-inner">
                                        <form id="cancelProperty" method="POST" action="#">
                                            <input type="hidden" value="<?php echo $_SESSION['cancel_property_key'] ?>" name="fkey" id="fkey">
                                            <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                                            <div class="row gy-4">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="reason_cancellation">Reason for cancellation <en>*</en></label>
                                                        <input type="text" class="form-control" id="reason_cancellation" name="reason_cancellation">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cancel_remarks">Remarks <en></en></label>
                                                        <textarea class="form-control" name="cancel_remarks" id="cancel_remarks"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="">Cancelled by <en>*</en></label>
                                                        <input type="text" class="form-control" id="" name="" disabled="" value="<?php echo ucwords($data['empinfo']['name']) ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                     <div class="form-group ">
                                                        <button type="submit" class="btn btn-lg btn-primary">Confirm Cancellation</button>
                                                        <span class="clearfix"></span>
                                                        <span class="italic_content">By Confriming the cancellation the property will be un assigned from the customer and will not be available for customer when he/she login to the customer portal. Also all the documents and gallery images shared with the property will be trashed and shall not be viewed by the customer</span>
                                                    </div>

                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Property Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Title</div>
                                                <div class="data-value"><a href="<?php echo COREPATH ?>property/details/<?php echo $data['info']['id'] ?>"><?php echo ucwords($data['info']['title']) ?></a></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Type</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['type']) ?></div>
                                            </div>
                                        </li>
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Flat Variant</div>
                                                <div class="data-value"><?php echo ucwords($data['roominfo']['flat_variant']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Blook</div>
                                                <div class="data-value"><?php echo ucwords($data['blockinfo']['block']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Floor</div>
                                                <div class="data-value"><?php echo ucwords($data['floorinfo']['floor']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Cost </div>
                                                <div class="data-value">Rs. <?php echo ucwords($data['info']['cost']) ?></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>


                                <?php if ($data['info']['customer_id']=='0') { ?>
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">                                                            
                                            <h5 class="nk-block-title title">Other Info</h5>
                                        </div>
                                    </div>
                                    <div class="card card-bordered">
                                        <ul class="data-list is-compact">
                                            <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Project Size</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['projectsize']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Project Area</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['projectarea']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">City</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">State</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['state']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Pincode</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['pincode']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Customer Status</div>
                                                <div class="data-value"><span class="badge badge-info">Un Assign</span></div>
                                            </div>
                                        </li>
                                        </ul>
                                    </div>
                                <?php }else{ ?>
                                    <div class="nk-block-head">
                                        <div class="nk-block-head-content">                                        
                                            <h5 class="nk-block-title title">Customer Info</h5>  
                                        </div>
                                    </div>
                                    <div class="card card-bordered">
                                        <ul class="data-list is-compact">
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">ID</div>
                                                    <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>customer/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['cusinfo']['uid']) ?></a></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Name</div>
                                                    <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Email Address</div>
                                                    <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Mobile</div>
                                                    <div class="data-value"><?php echo ucwords($data['cusinfo']['mobile']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Address</div>
                                                    <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">City</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?> 

                            </div><!-- .col -->
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>