<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>employee">Manage Employee</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['name']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-7">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Employee Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Name</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['name']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Role</div>
                                                <div class="data-value"><?php echo ucwords($data['roleinfo']['employee_role']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Email</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['email']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Mobile</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['mobile']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Address</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['address']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">City</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">State</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['state']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Pincode</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['pincode']) ?></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>




<?php include 'includes/bottom.html'; ?>




