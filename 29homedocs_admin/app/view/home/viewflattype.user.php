<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>flattype">Flat Type Settings</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['flat_variant']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-7">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title"><?php echo ucwords($data['info']['flat_variant']) ?> Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Flat Variant</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['flat_variant']) ?></div>
                                            </div>
                                        </li>


                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Description</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['description']) ?></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            <div class="col-lg-12">
                                
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Rooms</h5>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="card card-bordered">
                                    <table class="table table-bordered valign table_custom" >
                                        <thead>
                                            <tr>
                                                <th width="15%"> Room</th>
                                                <th width="10%"> Sqft </th>
                                                <th width="75%"> Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $data['type_list'] ?>
                                        </tbody>
                                    </table>
                                </div><!-- .card -->
                            </div><!-- .col -->
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>




<?php include 'includes/bottom.html'; ?>




