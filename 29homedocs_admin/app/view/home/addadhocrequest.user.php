<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>adhoc">Adhoc Request</a></li>
                                <li class="breadcrumb-item active">Add Adhoc Request</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg minheight">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Add Adhoc Request</h4>                                
                            </div>
                        </div>
                        <form id="addAdhocRequests" class="post_form_wrap" name="addAdhocRequests" method="POST" action="#" >
                        <input type="hidden" value="<?php echo $_SESSION['add_request_key'] ?>" name="fkey" id="fkey">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="ticket-msgs">
                                                <div class="post_an_item_wrap">
                                                    
                                                       
                                                        <div class="form-group">
                                                            <label class="form-label" for="fva-topics">Request Type <en>*</en></label>
                                                            <div class="form-control-wrap ">
                                                                <select class="form-control form-select select2-hidden-accessible" id="request_id" name="request_id" data-placeholder="Select a Request Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                                    <?php echo $data['types'] ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row g-3 align-center">
                                                                <div class="col-lg-12">
                                                                    <label class="form-label">Request is Raised by <en>*</en></label>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                        <li>
                                                                            <div class="custom-control custom-radio">
                                                                                <input type="radio" class="custom-control-input raised_by" checked="" name="raised_by" id="reg-enable" value="admin">
                                                                                <label class="custom-control-label" for="reg-enable">Admin</label>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="custom-control custom-radio">
                                                                                <input type="radio" class="custom-control-input raised_by" name="raised_by" id="reg-disable" value="user">
                                                                                <label class="custom-control-label" for="reg-disable">Customer</label>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="" id="">
                                                            <div class="form-group">
                                                                <label class="form-label" for="fva-topics">Select Customer<en>*</en></label>
                                                                <div class="form-control-wrap ">
                                                                    <select class="form-control form-select select2-hidden-accessible" id="customer_id" name="customer_id" data-placeholder="Select a Customer" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                                        <?php echo $data['customer_list'] ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group"></div>
                                                       <div class="form-group">
                                                            <label class="form-label" for="email">Subject <en>*</en></label>
                                                            <input type="text" class="form-control" id="subject" name="subject" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="form-label" for="email">Message <en>*</en></label>
                                                            <textarea class="form-control form-control-lg" id="remarks" name="remarks" required></textarea>
                                                        </div>
                                                        
                                                </div>
                                            </div><!-- .ticket-msgs -->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card card-bordered">
                                    <table class="table table-ulogs" id="addadhoccontent">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="tb-col-os"><span class="overline-title">Description <span class="d-sm-none">/ IP</span></span></th>
                                                <th class="tb-col-ip"><span class="overline-title">File</span></th>
                                                <th class="tb-col-action"><span class="overline-title">&nbsp;</span></th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <?php $add_adhoc_row  = 0; ?>
                                        </tbody>
                                    </table>
                                </div>


                                    
                                    <div class="post_box_footer mt15">
                                        <div class="selector">
                                            <div class="attachment float_right">
                                                <ul>
                                                    <li>
                                                        <!-- <button type="button" class=" " data-toggle="tooltip" data-placement="top"  onclick="addProperty();" data-original-title="Add Attachment"><i class="icon ni ni-clip-h"></i> <span>Add Attachment</span></button> -->

                                                        <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top"  onclick="addPropertyAdhoc();" data-original-title="Add Attachment"><em class="icon ni ni-plus"></em> <span>Add Attachment</span></button>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>adhoc' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>       
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>