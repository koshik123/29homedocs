<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>room">Manage Rooms</a></li>
                                <li class="breadcrumb-item active">Add Rooms</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Add Rooms</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            
                            
                            <div class="col-lg-8">
                                <div class="card card-bordered ">
                                    <div class="card-inner">
                                        <div class="card-head">
                                            <h5 class="card-title">Room Information</h5>
                                        </div>
                                        <form action="javascript:void();">
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Room Name</label>
                                                        <input type="text" class="form-control" id="cf-full-name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Sqft</label>
                                                        <input type="text" class="form-control" id="cf-full-name">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>room' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>