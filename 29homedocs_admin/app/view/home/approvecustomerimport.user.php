<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>importdata">Import Lead Data</a></li>
                                <li class="breadcrumb-item active">Approve Imported Data</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="row">
                          <div class="errmsgs"></div>
                          <div class="col-md-3 col-md-offset-9">
                            <!-- Vertical bars -->
                            <div class="panel panel-default text-center mb15">
                              <div class="panel-body">
                                <h4 class="text-danger text-center">Total Data: <?php echo $data['count'] ?> </h4>
                                <input type="hidden" value="<?php echo $data['token'] ?>" id="token">
                                <a href="javascript:;" class="btn btn-info approveCustomerImportData" ><i class="icon-checkmark"></i>Approve Imported Data</a>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-12">
                              <div class="nk-block-head-content mb15">
                                  <h4 class="nk-block-title ftsiz20"><em class='icon ni ni-check'></em> <?php echo $data['page_title']." (".$data['count'].")" ?></h4>
                              </div>
                              

                              <div class="card-box table-responsive">
                                <table id="datatable" class="table table-bordered ">
                                    <thead>
                                        <tr>
                                            <th width="1%">#</th>
                                            <th width="10%">Name</th>
                                            <th width="20%">Address</th>
                                            <th width="10%">Mobile</th>
                                            <th width="10%">Email</th>
                                            <th width="40%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php echo $data['layout'] ?>
                                    </tbody>
                                </table>
                            </div>


                          </div>
                        </div>
                    </div><!-- .nk-block -->
                    
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['r'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>The Excelsheet has been imported sucessfully.  </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>