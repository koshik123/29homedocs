<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>companysettings">Company Settings</a></li>
                                <li class="breadcrumb-item active">Update Company Settings</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <form id="editCompanySettings" name="editCompanySettings" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_companysettings_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                            <div class="row g-gs">
                                <div class="col-lg-7">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Update Company Settings</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="phone">Phone <en>*</en></label>
                                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $data['info']['phone'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="email">Email <en>*</en></label>
                                                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $data['info']['email'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="website_address">Website Address <en>*</en></label>
                                                        <input type="text" class="form-control" id="website_address" name="website_address" value="<?php echo $data['info']['website_address'] ?>">
                                                    </div>
                                                </div>

                                                
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="address">Address <en>*</en></label>
                                                        <input type="text" class="form-control" id="address" name="address" value="<?php echo $data['info']['address'] ?>">
                                                    </div>
                                                </div>
                                              
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               

                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>companysettings' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

