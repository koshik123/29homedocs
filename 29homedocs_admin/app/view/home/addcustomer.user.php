<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>customer">Manage Customer</a></li>
                                <li class="breadcrumb-item active">Add Customer</li>
                            </ul>
                        </nav>
                    </div>
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Add Customer</h4>
                            </div>
                        </div>
                        <div class="form-error"></div><br>
                        <form id="addCustomer" name="addCustomer" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_customer_key'] ?>" name="fkey" id="fkey">
                            <div class="row g-gs">
                                <div class="col-lg-4">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Basic Information</h5>
                                            </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="">Customer Name <en>*</en></label>
                                                    <input type="text" class="form-control" id="" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="priority">Priority <en>*</en></label>
                                                    <select class="form-control search" name="priority" id="priority">
                                                       <option value="low">Low</option>
                                                       <option value="medium">Medium</option>
                                                       <option value="high">High</option>
                                                       <option value="urgent">Urgent</option>
                                                    </select> 
                                                </div> 
                                                <div class="form-group">
                                                    <div class="row g-3 align-center">
                                                        <div class="col-lg-12">
                                                            <label class="form-label">Gender</label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <ul class="custom-control-group g-3 align-center flex-wrap">
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="male" checked="" name="gender" id="reg-enable">
                                                                        <label class="custom-control-label" for="reg-enable">Male</label>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="custom-control custom-radio">
                                                                        <input type="radio" class="custom-control-input" value="female"  name="gender" id="reg-disable">
                                                                        <label class="custom-control-label" for="reg-disable">Female</label>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Contact Information</h5>
                                            </div>
                                            
                                                <div class="row g-4">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="email">Email <en>*</en></label>
                                                            <input type="text" class="form-control" id="email" name="email">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="mobile">Mobile <en>*</en></label>
                                                            <input type="text" class="form-control" id="mobile" name="mobile" >
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="address">Address</label>
                                                            <input type="text" class="form-control" id="address" name="address">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="city">City</label>
                                                            <input type="text" class="form-control" id="city" name="city">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="state">State</label>
                                                            <input type="text" class="form-control" id="state" name="state">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="pincode">Pincode</label>
                                                            <input type="text" class="form-control" id="pincode" name="pincode">
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>customer' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>