<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>department">Department</a></li>
                                <li class="breadcrumb-item active">Edit Department</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="form-error"></div>
                        <br>
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Edit Department</h4>
                            </div>
                        </div>
                        <form id="editDepartment" name="editDepartment" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_department_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                            <div class="row g-gs">
                                <div class="col-lg-6">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Edit Department</h5>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="name">Department Name <en>*</en></label>
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $data['info']['name'] ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="sort_description">Short Description <en></en></label>
                                                <div class="form-control-wrap">
                                                    <textarea class="form-control form-control-sm" id="sort_description" name="sort_description" placeholder="Write your message"><?php echo $data['info']['short_description'] ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>department' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>