<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage Property </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Property </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">

                                        <div class="card card-bordered card-stretch" style="float: left;margin-right:10px ">
                                            <div class="card-inner-group">
                                                <div class="position-relative card-tools-toggle" data-select2-id="23">
                                                    <div class="card-title-group" data-select2-id="22">
                                                        <div class="card-tools mr-n1" data-select2-id="21">
                                                            <ul class="btn-toolbar gx-1" data-select2-id="20">
                                                                <li data-select2-id="19">
                                                                    <div class="toggle-wrap" data-select2-id="18">
                                                                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-menu-right"></em></a>
                                                                        <div class="toggle-content" >
                                                                            <form role="form" method="POST" action="#" id="propertyFilter">   
                                                                                <ul class="btn-toolbar gg gx-1" data-select2-id="16">
                                                                                    <li data-select2-id="15">
                                                                                        <div class="dropdown" data-select2-id="14">
                                                                                            <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                                                
                                                                                                <em class="icon ni ni-filter-alt"></em>  Filter
                                                                                            </a> 
                                                                                            <div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right" style="">
                                                                                                <div class="dropdown-head">
                                                                                                    <span class="sub-title dropdown-title">Property Filter </span>
                                                                                                </div>
                                                                                                <div class="dropdown-body dropdown-body-rg">
                                                                                                    <div class="row gx-6 gy-3">
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">House Facing</label>
                                                                                                                <select class="form-select " name="facing" id="facing" data-placeholder='Select House Facing'>
                                                                                                                    <option value="">Select House Facing</option>
                                                                                                                    <option value="east">East</option>
                                                                                                                    <option value="west">West</option>
                                                                                                                    <option value="north">North</option>
                                                                                                                    <option value="south">South</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Block</label>
                                                                                                                <select class="form-select " name="block" id="block" data-placeholder='Select Block'>
                                                                                                                    <option value="">Select Block</option>
                                                                                                                    <?php echo $data['block_types'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Floor</label>
                                                                                                                <select class="form-select " name="floor" id="floor" data-placeholder='Select Floor'>
                                                                                                                    <option value="">Select Floor</option>
                                                                                                                    <?php echo $data['floor_types'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="overline-title overline-title-alt">Room</label>
                                                                                                                <select class="form-select " name="room" id="room" data-placeholder='Select Room'>
                                                                                                                    <option value="">Select Room</option>
                                                                                                                    <?php echo $data['villa_types'] ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-12">
                                                                                                            <div class="form-group float_right">
                                                                                                                <button type="submit" class="btn btn-secondary">Filter</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                
                                                                                            </div><!-- .filter-wg -->
                                                                                        </div><!-- .dropdown -->
                                                                                    </li>
                                                                                </ul>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-search search-wrap" data-search="search"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <a href="<?php echo COREPATH ?>property/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Property</span></a>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php if (1==1): ?>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            
                                            <th>Title</th>
                                            <th>House Facing</th>
                                            <th>Block</th>
                                            <th>Floor </th>
                                            <th>Room </th>
                                            <th>Customer</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                        <?php endif ?>
                    </div> <!-- nk-block -->

                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>




<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Property added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['e'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Property updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['as'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Customer assigned successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['c'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong> Cancelled property successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>


