<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>gallery">Manage Gallery</a></li>
                                <li class="breadcrumb-item active">Edit Gallery</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Edit Gallery</h4>
                                
                            </div>
                        </div>
                        <form id="editGallery" method="POST" action="#" name="editGallery" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_gallery_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                            <div class="row g-gs">
                                <div class="col-lg-5">
                                    <div class="card card-bordered h-100">
                                        <div class="card-inner">
                                            <div class="form-group">
                                                <label class="form-label" for="album_title">Album Title <en>*</en></label>
                                                <input type="text" class="form-control" id="album_title" name="album_title" value="<?php echo $data['info']['album_title'] ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="sort_order">Sort Order <en>*</en></label>
                                                <input type="number" class="form-control" id="sort_order" name="sort_order" value="<?php echo $data['info']['sort_order'] ?>">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="sort_order">Description</label>
                                                <textarea  class="form-control" id="description" name="description"><?php echo $data['info']['description'] ?></textarea>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7">
                                    <div class="card card-bordered">
                                        <div class="card-inner">
                                            <?php if($data['info']['image']!="") { ?>
                                                 <div class="form-group">
                                                    <label class="form-label" for="album_title">Banner Images <en>*</en></label>
                                                    
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <img src="<?php echo SRCIMG.$data['info']['image'] ?>" class="img-responsive img-thumbnail">
                                                            <input type="hidden" id="list_image_exists" value="2">
                                                            <input type="hidden" name="image" value="<?php echo $data['info']['image'] ?>">
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <button type="button" data-id="<?php echo $data['token'] ?>" class="btn btn-danger btn-sm ma_top_25 removeGalleryImgIcon" data-type="image" id=""><i class="icon-remove2 "></i>Delete Image</button>
                                                        </div>
                                                    </div>
                                                </div> 
                                            <?php }else{ ?>
                                                <div class="form-group">
                                                    <label class="form-label" for="album_title">Banner Images <en>*</en></label>
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                                        <div>
                                                            <span class="btn btn-info btn-file">
                                                            <span class="fileupload-new">Select image</span>
                                                            <span class="fileupload-exists">Change</span>
                                                            <input type="file" id="cimage" name="cimage" />
                                                            <input type="hidden" name="image" id="newsImage"/>
                                                            </span>
                                                            <a href="#" class="btn btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                        </div>
                                                    </div>
                                                    <div class="imgerr"></div>
                                                </div> 
                                            <?php } ?> 
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>gallery' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>