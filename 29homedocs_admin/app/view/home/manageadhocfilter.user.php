<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Adhoc Requests  </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Adhoc Requests</h4>
                                    </div>
                                </div>
                                 <div class="col-5">
                                    <div class="float_right">
                                        <div class="card card-bordered card-stretch" style="float: left;margin-right:10px ">
                                            <div class="card-inner-group">
                                                <div class="position-relative card-tools-toggle" data-select2-id="23">
                                                    <div class="card-title-group" data-select2-id="22">
                                                        <div class="card-tools mr-n1" data-select2-id="21">
                                                            <ul class="btn-toolbar gx-1" data-select2-id="20">
                                                                <li data-select2-id="19">
                                                                    <div class="toggle-wrap" data-select2-id="18">
                                                                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="cardTools"><em class="icon ni ni-menu-right"></em></a>
                                                                        <div class="toggle-content" >
                                                                            <form role="form" method="POST" action="#" id="adhocFilter">   
                                                                                <input type="hidden" name="requeststatus" id="requeststatus" value="0">
<ul class="btn-toolbar gg gx-1" data-select2-id="16">
    <li data-select2-id="15">
        <div class="dropdown" data-select2-id="14">
            <a href="#" class="btn btn-trigger btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                
                <em class="icon ni ni-filter-alt"></em>  Filter
            </a> 
            <div class="filter-wg dropdown-menu dropdown-menu-xl dropdown-menu-right" style="">
                <div class="dropdown-head">
                    <span class="sub-title dropdown-title">Adhoc Request Filter </span>
                </div>
                <div class="dropdown-body dropdown-body-rg">
                    <div class="row gx-6 gy-3">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="overline-title overline-title-alt">Request Type</label>
                                <select class="form-select " name="request_id" id="request_id" data-placeholder='Select Request Type'>
                                    <option value="">Select Request Type</option>
                                    <?php echo $data['types'] ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="overline-title overline-title-alt">Priority</label>
                                <select class="form-select " name="priority" id="priority" data-placeholder='Select Priority'>
                                   <option value="">Select Priority</option>
                                   <option value="low">Low</option>
                                   <option value="medium">Medium</option>
                                   <option value="high">High</option>
                                   <option value="urgent">Urgent</option>
                                </select>
                            </div>
                        </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Assigned Status</label>
                                    <select class="form-select " name="assignstaus" id="assignstaus" data-placeholder='Select Assigned Status'>
                                       <option value="">Select Assigned Status</option>
                                       <option value="1">Assign</option>
                                       <option value="0">Unassign</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label class="overline-title overline-title-alt">Assigned To</label>
                                    <select class="form-select " name="assigned_to" id="assigned_to" data-placeholder='Select Assigned To'>
                                        <option value="">Select Assigned To</option>
                                        <?php echo $data['emp_list'] ?>
                                    </select>
                                </div>
                            </div>
                            <
                        <div class="col-12">
                            <div class="form-group float_right">
                                <button type="submit" class="btn btn-secondary">Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div><!-- .filter-wg -->
        </div><!-- .dropdown -->
    </li>
</ul>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-search search-wrap" data-search="search"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="<?php echo COREPATH ?>adhoc/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add a Request </span></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="filter_tag_wrap">
                            <span class='tag_element'>
                                <h5 class="type_name"><i class="la la-file-o"></i> Showing Results: 
                                    <?php if ($data['requesttype']!="") { ?>
                                    Lead Source - <strong> <?php echo ucwords($data['requesttype']) ?> </strong>
                                    <?php } ?>
                                    <?php if ($data['priority']!="") { ?>
                                        <?php if ($data['requesttype']!="") { ?> ,<?php } ?>
                                     Lead Status - <strong> <?php echo ucwords($data['priority']) ?> </strong>

                                    <?php } ?>
                                </h5>  
                                <span class="clearfix"></span>
                                <p class="result_count"> About <?php echo $data['list']['count'] ?> results </p>
                            </span>
                            <?php $url = (($data['request_status']=='0') ? 'adhoc' : 'adhoc/closed' ) ?>
                            <span class="float_right remove_filters"><a href="<?php echo COREPATH.$url ?>" class="btn btn-danger btn-sm"><em class="icon ni ni-cross"></em> Remove Filter</a></span>
                        </div>


                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Ticket</th>
                                            <th>Customer </th>
                                            <th>Subject</th>
                                            <th>Request Type</th>
                                            <th>Department</th>
                                            <th>Submitted</th>
                                            <th >Assigned To</th>
                                            <th>Raised by</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list']['layout'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Submitted successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>