<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>newsinfo">Manage News Information</a></li>
                                <li class="breadcrumb-item active">Add News Information</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                       
                        <div class="row g-gs">
                            <div class="col-lg-8">
                                <div class="card card-bordered ">
                                    <div class="card-inner">
                                        <div class="card-head">
                                            <h5 class="card-title">Add News Information</h5>
                                        </div>
                                        <form action="javascript:void();">
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Title</label>
                                                        <input type="text" class="form-control" id="cf-full-name">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Date</label>
                                                        <input type="text" class="form-control  date-picker" id="birth-day" placeholder="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="fva-topics">Project</label>
                                                        <div class="form-control-wrap ">
                                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="fva-topics" data-placeholder="Select a Project" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                                <option label="empty" value="" data-select2-id="4"></option>
                                                                <option value="fva-gq" data-select2-id="22">Project 1</option>
                                                                <option value="fva-tq" data-select2-id="23">Project 2 </option>
                                                                <option value="fva-ab" data-select2-id="24">Project 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Content</label>
                                                        <textarea class="form-control form-control-sm" id="cf-default-textarea" placeholder="Write your message"></textarea>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                          

                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>newsinfo' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>