<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>faqs">Manage FAQ's</a></li>
                                <li class="breadcrumb-item active">Edit FAQ's</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Edit FAQ's</h4>
                                
                            </div>
                        </div>
                        <div class="row g-gs">
                            
                            <div class="col-lg-6">
                                <div class="card card-bordered h-100">
                                    <div class="card-inner">
                                        <div class="card-head">
                                            <h5 class="card-title">Edit FAQ's</h5>
                                        </div>
                                        <form action="javascript:void();">
                                            <div class="form-group">
                                                <label class="form-label" for="cf-full-name">Title</label>
                                                <input type="text" class="form-control" id="cf-full-name">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="form-label" for="cf-default-textarea">Message</label>
                                                <div class="form-control-wrap">
                                                    <textarea class="form-control form-control-sm" id="cf-default-textarea" placeholder="Write your message"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href='<?php echo COREPATH ?>faqs' class="btn btn-lg btn-danger">Cancel</a>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>