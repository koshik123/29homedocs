<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>news">Manage News</a></li>
                                <li class="breadcrumb-item active">Add News</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg ">
                        <div class="nk-block-head">
                            <div class="nk-block-head-content">
                                <h4 class="title nk-block-title">Add News</h4>
                                
                            </div>
                        </div>
                        <form id="addnews" method="POST" action="#" name="addnews" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_news_key'] ?>" name="fkey" id="fkey">
                            <div class="row g-gs">
                                <div class="col-lg-5 mb70">
                                    <div class="card card-bordered">
                                        <div class="card-inner">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="title">Title <en>*</en></label>
                                                        <input type="text" class="form-control" id="title" name="title">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="date">Date <en>*</en></label>
                                                        <input type="text" name="date" id="date" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class=""> </label>
                                                        <ul class="custom-control-group g-3 align-center">
                                                            <li>
                                                                <div class="custom-control custom-control-md custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="com-email-1" name="email_notification">
                                                                    <label class="custom-control-label" for="com-email-1">Send Email Notification</label>
                                                                </div>
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <br>
                                                    <div class="form-group">
                                                        <ul class="custom-control-group g-3 align-center">
                                                            <li>
                                                                <div class="custom-control custom-control-md custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input show_popup" id="com-email-2" name="show_popup">
                                                                    <label class="custom-control-label" for="com-email-2">Show in pop up banner</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="form-group no_display" id="show_popup_warp">
                                                        <label class="form-label" for="till_date">Show banner till date (select the date greater than today) <en>*</en></label>
                                                        <input type="text" name="till_date" id="till_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" required=""  >
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group"></div>
                                                    <label class="form-label" for="album_title">Post To <en>*</en></label>
                                                    <ul class="custom-control-group g-3 align-center flex-wrap">
                                                        <li>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" class="custom-control-input post_to" checked="" name="post_to" value="all" id="reg-enable">
                                                                <label class="custom-control-label" for="reg-enable">All Customers</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-radio">
                                                                <input type="radio" class="custom-control-input post_to" name="post_to" value="selected" id="reg-disable">
                                                                <label class="custom-control-label" for="reg-disable">Select Customers</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="col-lg-12 no_display" id="selectcustomer_warp">
                                                    <div class="form-group"></div>
                                                     <div class="form-group">
                                                        <label>Select Customers <en></en> </label>
                                                        <input type="text" name="customers" value="" placeholder="Select Customers"  class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group no_display" id="selectcustomerdisplay_warp">
                                        <label></label>
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">
                                                <h4 class="title nk-block-title">Selected Customers</h4>
                                                
                                            </div>
                                        </div>
                                        <div class="related_product_container">
                                            <div id="selected-assistants" class="chapter_locations_wrap" style="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 mb70">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="form-group">
                                                <label class="form-label" for="description">Description</label>
                                                 <textarea placeholder="Enter Description" value="" class="form-control summernote" name="description"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="album_title">Image <en></en></label>
                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                                    <div>
                                                        <span class="btn btn-info btn-file">
                                                        <span class="fileupload-new">Select image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" id="cimage" name="cimage" />
                                                        <input type="hidden" name="image" id="newsImage"/>
                                                        </span>
                                                        <a href="#" class="btn btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                    </div>
                                                </div>
                                                <div class="imgerr"></div>
                                            </div>  
                                        </div>
                                    </div>

                                    
                                    
                                </div>
                                
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>news' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>