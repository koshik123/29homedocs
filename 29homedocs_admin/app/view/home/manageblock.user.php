<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Block Settings</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Block Settings</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="#" data-toggle="modal" data-target="#profile-edit" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add a new Block</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Block</th>
                                            <th>Added By </th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php echo $data['list'] ?>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
            <form id="addBlock" name="addBlock" method="POST" action="#" enctype="multipart/form-data">

            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>

                <div class="modal-body modal-body-sm">
                    <div class="form-error"></div><br>
                    <h5 class="title">Add Block Name</h5>
                    
                        <input type="hidden" value="<?php echo $_SESSION['add_block_key'] ?>" name="fkey" id="fkey">
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <div class="row gy-4">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label" for="block">Block Name <en>*</en></label>
                                            <input type="text" class="form-control" id="block" name="block">
                                        </div>
                                    </div>
                                   
                                    <div class="col-12">
                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                            <li>
                                                <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                            </li>
                                            <li>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                           
                        </div><!-- .tab-content -->
                   
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
            </form>
        </div><!-- .modal-dialog -->
    </div>

<div class="modal fade blockModel" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateBlock" name="updateBlock" method="POST" action="#" enctype="multipart/form-data">

        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>

            <div class="modal-body modal-body-sm">
                <div class="form-error"></div><br>
                <h5 class="title">Update Block Name</h5>
                    <input type="hidden" value="" name="fkey" id="editfkey">
                    <input type="hidden" value="" name="token" id="token">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="block_info">Block Name <en>*</en></label>
                                        <input type="text" class="form-control" id="block_info" name="block_info">
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>



<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Block added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['e'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Block updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>