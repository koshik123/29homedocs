<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>news">Manage News</a></li>
                                <li class="breadcrumb-item active">News Info </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-4">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage News Info </h4>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="float_right">
                                            <a href="<?php echo COREPATH ?>news/edit/<?php echo $data['info']['id'] ?>"  class="btn btn-info " ><em class="icon ni ni-edit"></em> <span> Edit</span></a>
                                        <?php if ($data['info']['send_mail_status']==0){ ?>  
                                            <a href="#"  class="btn btn-primary sendcustomer_mail" data-option='<?php echo $data['token'] ?>'><em class="icon ni ni-mail"></em> <span> Send Email</span></a>
                                        <?php }else{ ?>    
                                            <a href="#"  class="btn btn-success " data-option='<?php echo $data['token'] ?>'><em class="icon ni ni-check"></em> <span> Email Sent</span></a>
                                        <?php } ?>
                                        <?php if ($data['info']['deleted_status']==0){ ?>  
                                            <a href='javascript:void()' class='btn btn-warning deleteNews' data-value='<?php echo $data['info']['id'] ?>' data-type='<?php echo $data['info']['id'] ?>' data-option='<?php echo $data['info']['id'] ?>'> <span class='icon ni ni-trash'> </span> Delete</a>  
                                        <?php }else{ ?>    
                                            
                                        <?php } ?>
                                        <button class="btn btn-danger" onclick="window.close();" ><em class="icon ni ni-cross"></em> <span>Close</span></button>
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-5">
                                
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Title</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['title']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Date</div>
                                                <div class="data-value"><?php echo $data['info']['date']=="" ? '' : date("d M, Y",strtotime($data['info']['date'])) ?></div>
                                            </div>
                                        </li>

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Description</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['description']) ?></div>
                                            </div>
                                        </li>
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Email Notification</div>
                                                <div class="data-value"><?php echo ($data['info']['email_notification']==0 ? 'No' : 'Yes' ) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Post To</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['post_to']) ?></div>
                                            </div>
                                        </li>
                                      

                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Show Popup</div>
                                                <div class="data-value"><?php echo ($data['info']['show_popup']==0 ? 'No' : 'Yes' ) ?></div>
                                            </div>
                                        </li>
                                        <?php if ($data['info']['show_popup']=="1"): ?>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Popup Till</div>
                                                <div class="data-value"><?php echo $data['info']['popup_till']=="" ? '' : date("d M, Y",strtotime($data['info']['popup_till'])) ?></div>
                                            </div>
                                        </li>
                                        <?php endif ?>

                                        
                                           
                                        
                                        
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            <div class="col-lg-6">
                                <?php $show1 = $data['info']['post_to']=='all' ? 'no_display' : '' ; ?>
                                <div class="<?php echo $show1 ?>">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title " style="margin-bottom: 15px;">List of Customers</h4>
                                    </div>
                                    <div class="FixedHeightContainer ">
                                        <div class="Content_box">
                                           <?php echo $data['list'] ?>
                                        </div>
                                    </div>
                                </div>
                               
                            </div><!-- .col -->
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade " tabindex="-1" role="dialog" id="updateLeadStatusmodel">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateLeadStatus"  id="updateLeadStatus">
                <input type='hidden' value='<?php echo $_SESSION['update_leadstatus_key'] ?>' name='fkey' id='fkey'>
                <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                <input type="hidden" name="token" id="token" value="<?php echo $data['info']['id'] ?>">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">Update Lead Status</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <!-- <div class="kycInfo"></div> -->
                                   <div class="form-group">
                                        <label class="form-label" for="company">Lead Status <en>*</en></label>
                                        <select class="form-control form-select select2-hidden-accessible" id="leadstatus_id" name="leadstatus_id" data-placeholder="Select a Lead Status" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                            <?php echo $data['leadstatus'] ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="addactivity">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="addleadActivity"  id="addleadActivity">
                <input type="hidden" name="session_token" id="session_token" value="<?php echo $data['token'] ?>">
                <input type="hidden" name="token" id="token" value="<?php echo $data['info']['id'] ?>">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">Add Activity</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label" for="company">Activity Type <en>*</en></label>
                                        <select class="form-control form-select select2-hidden-accessible" id="activity_id" name="activity_id" data-placeholder="Select a Activity Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                            <?php echo $data['activity_types'] ?>
                                        </select>
                                    </div>
                                </div>
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Activity Date <en>*</en></label>
                                        <div class="form-control-wrap">
                                            <input type="text" name="act_date" id="act_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div> 
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label" for="company">Remarks <en>*</en></label>
                                        <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your Remarks"></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade editactivity_model" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateLeadActivity"  id="updateLeadActivity">
               <div class="layout"></div>
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>




<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Lead Status updated successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['la'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Activity added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>