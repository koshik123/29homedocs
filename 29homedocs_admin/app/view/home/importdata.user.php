<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-lg float_left">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Import Lead Data</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <?php if ($data['videoinfo']['page_video']!=""): ?>
                    <div class="help_warp float_right">
                        <span>
                            <a href='javascript:void();' class='videoShowPreview' data-value='<?php echo $data['videoinfo']['title']  ?>' data-option='<?php echo $data['videoinfo']['id']  ?>' data-video='<?php echo VIDEO_PATH.$data['videoinfo']['page_video']  ?>' style="color: #ff0b0b;"><em class='icon ni ni-help-alt' style='font-size: 30px;'></em> Help </a> 
                        </span>
                    </div>
                    <?php endif ?>
                    <div class="clearfix"></div>
                    <div class="nk-block nk-block-lg">
                        <div class="row">
                          <div class="col-md-4">
                            <form class="form-horizontal" action="#"  enctype="multipart/form-data" role="form" method="POST" name="uploadExcel" id="uploadExcel">
                              <div class="panel panel-default">
                                <div class="panel-heading">
                                  <h6 class="panel-title"><i class="icon-file-upload"></i> <?php echo $data['page_title'] ?></h6>
                                </div>
                                <div class="panel-body">
                                   <div class="form-group">
                                    <label class="col-sm-4 "> </label>
                                    <div class="col-sm-8">
                                      <input type="file" name="excelsheet"  class="styled" required>
                                    </div>
                                  </div>
                                  <div class="form-group">

                                  </div>
                                  <div class="form-actions text-right">
                                    <input value="Import Excel Data" class="btn btn-success" type="submit">
                                  </div>
                                  <div class="errmsgs"></div>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="col-md-8">
                              <div class="panel panel-default">
                                <div class="panel-heading mb15">
                                  <h6 class="panel-title"><i class="icon-file-excel"></i> Sample Excel Sheet</h6>
                                </div>
                                <div class="panel-body ">
                                  <p><a href="<?php echo UPLOADS ?>datasheet/sample_excel_import_format.xlsx" class="btn btn-info"> Download Sample Excel</a></p>
                                  <p><strong>Note:</strong> </p>
                                  <p class="text-danger"> - Please Upload the Excel sheet with the fields provided in the sample Excel sheet alone. </p>
                                  <p> - The uploaded data will be displayed in the Excel import History Section Below. Validate the Data imported once again and then approve to publish the Imported data. Until then the data will not be published. </p>
                                </div>
                              </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                      <div class="row">
                          <div class="col-md-12">
                            <!-- Vertical bars -->
                              <div class="nk-block-head-content mb15">
                                  <h4 class="nk-block-title ftsiz20">Excel Import History</h4>
                              </div>
                              <div class="nk-block">
                                <div class="card card-bordered card-stretch">
                                    <div class="card-inner">
                                        <table class=" table">
                                            <thead>
                                                <tr>
                                                    <th width="1%">#</th>
                                                    <th width="15%">Last Import</th>
                                                    <th width="10%">Data Imported</th>
                                                    <th width="10%">Approved Data</th>
                                                    <th width="10%">Duplicate Data</th>
                                                    <th width="10%">Excel Sheet</th>
                                                    <th width="10%">Data Status</th>
                                                    <th width="10%">Approved On</th>
                                                    <th width="15%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php echo $data['layout'] ?> 
                                            </tbody>
                                        </table>
                                    </div> <!-- .card -->
                                </div>
                            </div>



                            <!-- /vertical bars -->
                          </div>
                        </div>
                    </div>
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

