<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>lead">Manage Lead</a></li>
                                <li class="breadcrumb-item active">Add Activity</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <form id="addActivity" name="addActivity" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['add_activity_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['info']['id'] ?>" name="lead_id" id="lead_id">
                            <div class="row g-gs">
                                <div class="col-lg-7">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Add Activity</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label" for="company">Activity Type <en>*</en></label>
                                                        <select class="form-control form-select select2-hidden-accessible" id="activity_id" name="activity_id" data-placeholder="Select a Activity Type" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['activity_types'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-label">Activity Date <en>*</en></label>
                                                        <div class="form-control-wrap">
                                                            <input type="text" name="act_date" id="act_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                                        </div>
                                                    </div>
                                                </div> 
                                                
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="company">Remarks <en>*</en></label>
                                                        <textarea class="form-control form-control-sm" id="remarks" name="remarks" placeholder="Write your Remarks"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>lead' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>

