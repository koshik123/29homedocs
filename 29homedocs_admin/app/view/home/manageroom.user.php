<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage Rooms</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage Rooms</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>room/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Room</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Project </th>
                                            <th>Room Info</th>
                                            <th>Sqft </th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Project 1</td>
                                            <td>Kitchen </td>
                                            <td>100sqft</td>
                                            
                                            <td><div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck7" checked>
                                                    <label class="custom-control-label" for="customCheck7">Active</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-success"><em class="icon ni ni-edit"></em></a>
                                                </div>
                                                
                                               
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-warning"><em class="icon ni ni-eye"></em></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Project 1</td>
                                            <td>Bath </td>
                                            <td>60sqft</td>
                                            
                                            <td><div class="custom-control custom-control-sm custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck7" checked>
                                                    <label class="custom-control-label" for="customCheck7">Active</label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-success"><em class="icon ni ni-edit"></em></a>
                                                </div>
                                                
                                               
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-warning"><em class="icon ni ni-eye"></em></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>



<div class="modal fade" tabindex="-1" role="dialog" id="profile-edit">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-sm">
                    <h5 class="title">Assign Property</h5>
                   
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="fva-topics">Select Property</label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="fva-topics" data-placeholder="Select a Property" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                <option label="empty" value="" data-select2-id="4"></option>
                                                <option value="fva-gq" data-select2-id="22">Property 1</option>
                                                <option value="fva-tq" data-select2-id="23">Property 2</option>
                                                <option value="fva-ab" data-select2-id="24">Property 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void();" class="btn btn-lg btn-primary">Assign</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                       
                    </div><!-- .tab-content -->
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div>