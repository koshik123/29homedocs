<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['title']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title "><?php echo ucwords($data['info']['title']) ?> Info </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                        <?php if($data['info']['cancel_status']=='0' && $data['info']['deleted_status']=='0' ){ ?>


                                            <?php if($data['info']['cancel_status']=='0'){ ?>
                                                <a href="<?php echo COREPATH ?>property/edit/<?php echo $data['info']['id'] ?>" class="btn btn-success"  ><em class="icon ni ni-edit"></em> <span> Edit</span></a>
                                            <?php  } ?>

                                            <?php if(isset($_SESSION["is_manager"])){ ?>
                                                 <a href='<?php echo COREPATH ?>property/cancel/<?php echo $data['info']['id'] ?>' class='btn btn-warning'><em class='icon ni ni-cross'></em> Cancel</a>
                                            <?php  } ?>

                                            <?php if($data['info']['assign_status']=='1'){ ?>
                                                <!-- <a href="<?php echo COREPATH ?>property/images/<?php echo $data['info']['id'] ?>" class="btn btn-info"  ><em class="icon ni ni-img"></em> <span> Upload Images</span></a>
                                                <a href="<?php echo COREPATH ?>property/document/<?php echo $data['info']['id'] ?>" class="btn btn-blue"  ><em class="icon ni ni-file"></em> <span> Upload Documents</span></a> -->
                                            <?php  } ?>

                                        <?php  } ?>
                                        <button class="btn btn-danger" onclick="window.close();" ><em class="icon ni ni-cross"></em> <span>Close</span></button>
                                    </div> 
                                </div>                               
                            </div>
                        </div>
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Property Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Title</div>
                                                <div class="data-value"><a href="<?php echo COREPATH ?>property/details/<?php echo $data['info']['id'] ?>"><?php echo ucwords($data['info']['title']) ?></a></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Type</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['type']) ?></div>
                                            </div>
                                        </li>
                                        
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Flat Variant</div>
                                                <div class="data-value"><?php echo ucwords($data['roominfo']['flat_variant']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Blook</div>
                                                <div class="data-value"><?php echo ucwords($data['blockinfo']['block']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Floor</div>
                                                <div class="data-value"><?php echo ucwords($data['floorinfo']['floor']) ?></div>
                                            </div>
                                        </li>
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Cost </div>
                                                <div class="data-value">Rs. <?php echo ucwords($data['info']['cost']) ?></div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div><!-- .col -->
                                <?php if ($data['info']['customer_id']=='0') { ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                                            
                                                <h5 class="nk-block-title title">Other Info</h5>
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Size</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectsize']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Project Area</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['projectarea']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">City</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">State</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['state']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Pincode</div>
                                                    <div class="data-value"><?php echo ucwords($data['info']['pincode']) ?></div>
                                                </div>
                                            </li>
                                            <li class="data-item">
                                                <div class="data-col">
                                                    <div class="data-label">Customer Status</div>
                                                    <div class="data-value"><span class="badge badge-info">Un Assign</span></div>
                                                </div>
                                            </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">                                        
                                                <h5 class="nk-block-title title">Customer Info</h5>  
                                            </div>
                                        </div>
                                        <div class="card card-bordered">
                                            <ul class="data-list is-compact">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">ID</div>
                                                        <div class="data-value"><a target="_blank" href="<?php echo COREPATH ?>customer/details/<?php echo $data['cusinfo']['id']  ?>"><?php echo ucwords($data['cusinfo']['uid']) ?></a></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Name</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['name']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Email Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['email']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Mobile</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['mobile']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Address</div>
                                                        <div class="data-value"><?php echo ucwords($data['cusinfo']['address']) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">City</div>
                                                        <div class="data-value"><?php echo ucwords($data['info']['city']) ?></div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } ?> 
                            </div><!-- .row -->
                        </div><!-- .nk-block -->
                        <?php if($data['info']['assign_status']=='1'){ ?>
                        <div class="row gy-5">
                            
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="nk-block-head-content">
                                                <h4 class="nk-block-title title">Private Documents </h4>
                                            </div>
                                        </div>
                                        <?php if($data['info']['cancel_status']=='0' && $data['info']['deleted_status']=='0' ){ ?>
                                        <div class="col-5">
                                            <div class="float_right">
                                            <a href="#"  data-toggle="modal" data-target="#addGeneralDoc" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add General Document</span></a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="nk-block">
                                    <div class="card card-bordered card-stretch">
                                        <div class="card-inner-group">
                                            
                                            <div class="card-inner p-0">
                                                <div class="nk-tb-list nk-tb-ulist">
                                                    <?php echo $data['list'] ?>
                                                </div>
                                            </div><!-- .card-inner -->
                                            <?php if ($data['doc_list']['count']>0){ ?>
                                            <div class="card-inner p-0">
                                                <div class="nk-tb-list nk-tb-ulist">
                                                    <div class='card-inner'>
                                                        <div class='card-title-group'>
                                                            <div class='card-title'>
                                                                <h6 class='title'>General Documents</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class='card-inner card-inner-md'>
                                                        <div class='user-card'>
                                                            <div class='nk-tb-list nk-tb-ulist'>
                                                                <div class='nk-tb-item nk-tb-head'>
                                                                    <div class='nk-tb-col' style='width: 1%;'><span>#</span></div>
                                                                    <div class='nk-tb-col tb-col-mb' style='width: 15%;'><span>Doc Name</span></div>
                                                                    <div class='nk-tb-col tb-col-md' style='width: 10%;'><span>Status</span></div>
                                                                    <div class='nk-tb-col tb-col-md' style='width: 14%;'><span>Action</span></div>
                                                                </div>
                                                                <?php echo $data['doc_list']['layout'] ?>   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .card-inner -->
                                            <?php } ?>
                                        </div><!-- .card-inner-group -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div>
                            <div class="col-lg-6">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content float_left">
                                        <h4 class="title nk-block-title">Private Images</h4>
                                    </div>
                                    <?php if($data['info']['cancel_status']=='0' && $data['info']['deleted_status']=='0'){ ?>
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>property/addimages/<?php echo $data['info']['id'] ?>" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Images</span></a>
                                    </div>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-error"></div>
                                <div class="row g-gs">

                                    <div class="col-lg-12">

                                        <div class="row">
                                            <?php if ($data['images']['count']>0){ ?>
                                                <ul class="image_list" id="masonry">
                                                   <?php echo $data['images']['layout'] ?>
                                                </ul>
                                            <?php }else{ ?>
                                                <div class='col-md-12'>
                                                    <div class='no_event'>
                                                        <h1><i class='icon ni ni-img'></i></h1>
                                                        <p>No images added yet</p>
                                                    </div> 
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>                                   
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h4 class="title nk-block-title">Room Images</h4>
                                    </div>
                                </div>
                                <div class="row g-gs">
                                    <div class="col-lg-12">
                                        <div class="card card-bordered card-preview">
                                            <div class="card-inner">
                                                <table class="datatable-init table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Room Name</th>
                                                            <th>Total Images</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $data['room_list'] ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div>                            
                                </div>
                                <br>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="nk-block-head">
                                    <div class="row">
                                        <div class="col-7">
                                            <div class="nk-block-head-content">
                                                <h4 class="nk-block-title title">Payment Schedule </h4>
                                            </div>
                                        </div>
                                        <?php if ($data['payment_schedule']['amt_payable']!=$data['payment_schedule']['amt_receive'] && $data['payment_schedule']['balance']!='0' ) { ?>
                                        <div class="col-5">
                                            <div class="float_right">
                                            <a href="#"  data-toggle="modal" data-target="#addPaymentSchedule" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add Payment Schedule</span></a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <div class="row">
                                            <div class="col-md-6">
                                                
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Amount Payable</div>
                                                        <div class="data-value">Rs.<?php echo number_format($data['payment_schedule']['amt_payable'],2) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item duedate">
                                                    <div class="data-col">
                                                        <div class="data-label">Next Due Date</div>
                                                        <div class="data-value"><?php echo date("d/m/Y",strtotime($data['payment_schedule']['due_date'])) ?>
                                                        <?php if ($data['payment_schedule']['amt_payable']!=$data['payment_schedule']['amt_receive'] && $data['payment_schedule']['balance']!='0' ) { ?>
                                                            <a href="#" data-toggle="modal" data-target="#editDueDate" class="btn btn-success btn-sm dueedit"><em class="icon ni ni-edit"></em> Edit </a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </li>
                                            </div>
                                            <div class="col-md-6">
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label">Amount Receive</div>
                                                        <div class="data-value">Rs.<?php echo number_format($data['payment_schedule']['amt_receive'],2) ?></div>
                                                    </div>
                                                </li>
                                                <li class="data-item">
                                                    <div class="data-col">
                                                        <div class="data-label"> Balance Amount</div>
                                                        <div class="data-value">Rs.<?php echo number_format($data['payment_schedule']['balance'],2) ?></div>
                                                    </div>
                                                </li>
                                            </div>
                                        </div>  
                                    </ul>
                                </div>
                                <?php if ($data['payment_list']['count']>0 ) { ?>
                                <div class="row g-gs">
                                    <div class="col-lg-12">
                                        <div class="card card-bordered card-preview">
                                            <div class="card-inner">
                                                <table class=" table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Stages</th>
                                                            <th>%</th>
                                                            <th>Amount Payable</th>
                                                            <th>Amount Received</th>
                                                            <th>Balance</th>
                                                            <th>Due Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <?php echo $data['payment_list']['layout'] ?> 
                                                         <tr>
                                                             <td colspan="2" class="text_center"><strong>Total</strong></td>
                                                             <td><?php echo $data['payment_stats']['percentage_count'] ?></td>
                                                             <td>Rs. <?php echo number_format($data['payment_schedule']['amt_payable'],2) ?></td>
                                                             <td>Rs. <?php echo number_format($data['payment_stats']['amt_receive_count'],2) ?></td>
                                                             <td>Rs. <?php echo number_format($data['payment_schedule']['amt_payable'] - $data['payment_stats']['amt_receive_count'],2) ?></td>
                                                             <td></td>
                                                         </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div>                            
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php  } ?>
                        <div class="row gy-5">
                            
                        </div>
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="editDueDate">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateDueDate" name="updateDueDate" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Due Date Update</h5>
                    <input type="hidden" name="property_id" id="property_id" value="<?php echo $data['info']['id'] ?>">
                    <input type="hidden" name="payment_schedule_id" id="payment_schedule_id" value="<?php echo $data['payment_schedule']['id'] ?>">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="date">Due Date <en>*</en></label>
                                        <input type="text" name="due_date" id="due_date" class="form-control date-picker" data-date-format="dd/mm/yyyy" value="<?php echo date("d/m/Y",strtotime($data['payment_schedule']['due_date'])) ?>">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="addPaymentSchedule">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="addPaymentSchedulelist" name="addPaymentSchedulelist" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add Payment Schedule</h5>
                    <input type="hidden" name="payment_schedule_id" id="payment_schedule_id" value="<?php echo $data['payment_schedule']['id'] ?>">
                    <input type="hidden" name="property_id" id="property_id" value="<?php echo $data['info']['id'] ?>">
                    <input type="hidden" name="amt_payable" id="amt_payable" value="<?php echo $data['payment_schedule']['amt_payable'] ?>">
                    <input type="hidden" name="balance_amt" id="balance_amt" value="<?php echo $data['payment_schedule']['balance'] ?>">
                    <input type="hidden" name="due_date" id="due_date" value="<?php echo $data['payment_schedule']['due_date'] ?>">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="">Amount Payable <en></en></label>
                                        <input type="text" class="form-control" id="" name="" value="<?php echo number_format($data['payment_schedule']['amt_payable'],2) ?>" disabled="">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="fva-topics">Select Stage <en>*</en></label>
                                        <div class="form-control-wrap ">
                                            <select class="form-control form-select valid" id="fva-topics" name="stage_id" data-placeholder="Select a Stage" required="" tabindex="-1" aria-hidden="false">
                                                <?php echo $data['stage_list'] ?>
                                            </select>
                                        </div>
                                    </div>                              
                                    <div class="form-group">
                                        <label class="form-label" for="per">Pay Percentage % <en>*</en></label>
                                        <input type="text" class="form-control" id="per" name="per">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="title">Amount Paying <en>*</en></label>
                                        <input type="text" class="form-control" id="amt_paying" name="amt_paying">
                                    </div>
                                    
                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="addGeneralDoc">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="addPropertyGendralDocuments" name="addPropertyGendralDocuments" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add General Document</h5>
                
                    <input type="hidden" value="<?php echo $_SESSION['add_propertygendral_documents_key'] ?>" name="fkey" id="fkey">
                    <input type="hidden" name="property_id" id="property_id" value="<?php echo $data['info']['id'] ?>">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label" for="title">Title <en>*</en></label>
                                        <input type="text" class="form-control" id="title" name="title">
                                    </div>
                                     <div class="form-group">
                                        <label class="form-label" for="description">Description <en></en></label>
                                        <textarea class="form-control" name="description" id="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="attachment">Document <en>*</en></label>
                                        <input type="file" class="form-control" id="attachment" name="attachment">
                                    </div>
                                </div>
                               
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>

<div class="modal fade edituploadfile" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-md " role="document">
        <form id="updateDocumentsFiles" name="updateDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
            <div class="edituploadfileModel"></div>
        </form>
    </div><!-- .modla-dialog -->
</div>

<div class="modal editgenuploadfile fade" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-md " role="document">
        <form id="updateGenDocumentsFiles" name="updateGenDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
            <div class="editgenuploadfileModel"></div>
        </form>
    </div><!-- .modal-dialog -->
</div>

<?php if (1!=1): ?>
  <div class="modal edituploadfile fade in" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateDocumentsFiles" name="updateDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add File</h5>
                
                   <!--  <input type="hidden" value="<?php echo $_SESSION['add_floor_key'] ?>" name="fkey" id="fkey"> -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="edituploadfileModel"></div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>  
<div class="modal editgenuploadfile fade in" tabindex="-1" role="dialog" id="uploadfile">
    <div class="modal-dialog modal-dialog-sm modal-dialog-centered  " role="document">
        <form id="updateGenDocumentsFiles" name="updateGenDocumentsFiles" method="POST" action="#" enctype="multipart/form-data">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
                <h5 class="title">Add File</h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="editgenuploadfileModel"></div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-md btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-md btn-primary">Submit</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       
                    </div><!-- .tab-content -->
               
            </div><!-- .modal-body -->
        </div><!-- .modal-content -->
        </form>
    </div><!-- .modal-dialog -->
</div>
<?php endif ?>





    <div class="modal fade genDocModel" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-md " role="document">
            <div class="genDocInfo"></div>
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

     <div class="modal fade privateDocModel" tabindex="-1" role="dialog" id="">
        <div class="modal-dialog modal-md " role="document">
            <div class="privateDocInfo"></div>
        </div><!-- .modla-dialog -->
    </div><!-- .modal -->

<?php include 'includes/bottom.html'; ?>




<?php if (isset($_GET['a'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>General document added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['i'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Images added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['r'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Room images added successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>


<?php if (isset($_GET['du'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Due date update successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>

<?php if (isset($_GET['pa'])): ?>
<script type="text/javascript" charset="utf-8" async defer>
setTimeout(function() {
    new Noty({
        text: '<strong>Payment update successfully! </strong>!',
        type: 'success',
        theme: 'relax',
        layout: 'topRight',
        timeout: 3000
    }).show();
}, 400);
history.pushState(null, "", location.href.split("?")[0]);
</script>
<?php endif ?>