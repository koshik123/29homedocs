<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>flattype">Manage Flat Type Master</a></li>
                                <li class="breadcrumb-item active"><?php echo $data['page_title'] ?></li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                   
                    <form  id="editFlattypemaster" name="editFlattypemaster" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['edit_flattype_key'] ?>" name="fkey" id="fkey">
                        <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                        <div class="nk-block nk-block-lg">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title"><?php echo $data['page_title'] ?></h4>
                                    
                                </div>
                            </div>  
                            <div class="form-error"></div>
                            <div class="row g-gs ">
                                <div class="col-lg-6">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Room Information</h5>
                                            </div>
                                            <div class="row g-4">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Flat Variant <en>*</en></label>
                                                        <input type="text" class="form-control" id="flat_variant" value="<?php echo $data['info']['flat_variant'] ?>" name="flat_variant">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="cf-full-name">Description</label>
                                                        
                                                        <textarea class="form-control" id="description" name="description"><?php echo $data['info']['description'] ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 h-100 minheight">
                                    <div class="form-group">
                                        <div class="card panel panel-default panel-table">
                                            <h5 class="table-header">
                                                Add Rooms 
                                            </h5>
                                            <div class="add_btn">
                                                <button type="button" onclick="addProperty();" title="Add" class="btn btn-success btn-sm">Add </button>
                                            </div>
                                            <div class="card-block">
                                                <table class="table table-bordered valign table_custom" id="flattype">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%">Room Type</th>
                                                            <th width="15%">Sqft</th>
                                                            <th width="15%">Description</th>
                                                            <th width="8%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <?php echo $data['type_list'] ?>
                                                        <?php $flattype_row  = 0; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>flattype' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .nk-block -->
                    </form>
                  
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>


<div class="modal fade editFlatTypeAttachment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Information</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="perror"></div>
                <form name="updateFlatTypeInfoDetails" id="updateFlatTypeInfoDetails" method="POST" action="#" accept-charset="utf-8">
                     <div class="editFlatTypeInfoModel">
                    </div>                     
                </form>
            </div>
        </div>
    </div>
</div>