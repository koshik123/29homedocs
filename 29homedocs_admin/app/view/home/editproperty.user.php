<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>property">Manage Property</a></li>
                                <li class="breadcrumb-item active">Edit Property</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <!-- <div class="form-error"></div>
                    <br> -->
                    <form  id="editFlatVilla" name="editFlatVilla" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['edit_property_key'] ?>" name="fkey" id="fkey">
                        <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                        <div class="nk-block nk-block-lg minheight">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h4 class="title nk-block-title">Edit Property</h4>
                                </div>
                            </div>
                                <div class="row g-gs ">
                                    <div class="col-lg-4">
                                        <div class="card card-bordered h-100">
                                            <div class="card-inner">
                                                <div class="card-head">
                                                    <h5 class="card-title">Basic Information</h5>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="title">Title <en>*</en></label>
                                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo $data['info']['title'] ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Type <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="type" data-placeholder="Select a Typee" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <option value="flat" data-select2-id="22" <?php echo $data['info']['type']=="flat" ? 'checked' : '' ?>>Flat</option>
                                                            <option value="villa" data-select2-id="23" <?php echo $data['info']['type']=="villa" ? 'checked' : '' ?>>Villa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Select Rooms <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-top" name="room_id" data-placeholder="Select a Room" required="" data-select2-id="fva-top" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['villa_types'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Select Block <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="block_id" data-placeholder="Select a Block" required="" data-select2-id="fva-topicss" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['block_types'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">Select Floor <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="floor_id" data-placeholder="Select a Floor" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['floor_types'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="fva-topics">House Facing <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="fva-topics" name="house_facing" data-placeholder="Select House Facing" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <option value="east" <?php echo $data['info']['house_facing']=='east' ? 'selected' : '' ?>>East</option>
                                                            <option value="west" <?php echo $data['info']['house_facing']=='west' ? 'selected' : '' ?>>West</option>
                                                            <option value="north" <?php echo $data['info']['house_facing']=='north' ? 'selected' : '' ?>>North</option>
                                                            <option value="south" <?php echo $data['info']['house_facing']=='south' ? 'selected' : '' ?>>South</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="area_type_id">Area Type <en>*</en></label>
                                                    <div class="form-control-wrap ">
                                                        <select class="form-control form-select select2-hidden-accessible" id="area_type_id" name="area_type_id" data-placeholder="Select a Area Type" required="" data-select2-id="fva-topics" tabindex="-1" aria-hidden="true">
                                                            <?php echo $data['area_type'] ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="form-label" for="projectsize">Project Size <en>*</en></label>
                                                    <input type="text" class="form-control" id="projectsize" name="projectsize" value="<?php echo $data['info']['projectsize'] ?>">
                                                </div>
                                                 <div class="form-group">
                                                    <label class="form-label" for="projectarea">Project Area <en>*</en></label>
                                                    <input type="text" class="form-control" id="projectarea" name="projectarea" value="<?php echo $data['info']['projectarea'] ?>">
                                                </div>
                                                 <div class="form-group">
                                                    <label class="form-label" for="cost">Cost <en>*</en></label>
                                                    <input type="number" class="form-control" id="cost" name="cost" value="<?php echo $data['info']['cost'] ?>">
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="card card-bordered h-100">
                                            <div class="card-inner">
                                                <div class="card-head">
                                                    <h5 class="card-title">Contact Information</h5>
                                                </div>
                                                <div class="row g-4">
                                                    
                                                   
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="address">Address</label>
                                                            <input type="text" class="form-control" id="address" name="address"  value="<?php echo $data['info']['address'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="city">City</label>
                                                            <input type="text" class="form-control" id="city" name="city"  value="<?php echo $data['info']['city'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="state">State</label>
                                                            <input type="text" class="form-control" id="state" name="state"  value="<?php echo $data['info']['state'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="pincode">Pincode</label>
                                                            <input type="text" class="form-control" id="pincode" name="pincode"  value="<?php echo $data['info']['pincode'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <?php if($data['info']['image']!="") { ?>
                                                            <div class="form-group">
                                                                <label class=" control-label ">Image :
                                                                    <en></en> </label>
                                                                <div class="row">
                                                                    <div class="col-sm-4">
                                                                        <img src="<?php echo SRCIMG.$data['info']['image'] ?>" class="img-responsive img-thumbnail">
                                                                        <input type="hidden" id="list_image_exists" value="2">
                                                                        <input type="hidden" name="image" value="<?php echo $data['info']['image'] ?>">
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <button type="button" data-id="<?php echo $data['token'] ?>" class="btn btn-danger btn-sm ma_top_25 removePropertyImgIcon" data-type="image" id=""><i class="icon-remove2 "></i>Delete Image</button>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        <?php }else{ ?>
                                                            <div class="form-group">
                                                                <label class=" control-label ">Image :
                                                                    <en></en> </label>
                                                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-preview fileupload-large thumbnail"><img src="<?php echo IMGPATH ?>file_upload_icon.jpg"></div>
                                                                    <div>
                                                                        <span class="btn btn-default btn-file">
                                                                        <span class="fileupload-new btn btn-sm btn-info">Select image</span>
                                                                        <span class="fileupload-exists btn btn-sm btn-info">Change</span>
                                                                        <input type="file" id="cimage" name="cimage" />
                                                                        <input type="hidden" name="image" id="newsImage"/>
                                                                        </span>
                                                                        <a href="#" class="btn btn-sm btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                                </div>
                                                                <div class="imgerr"></div>
                                                            </div> 
                                                        <?php } ?> 
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label class="form-label" for="content">Content</label>
                                                            <textarea class="form-control form-control-sm" id="content" name="content" placeholder="Write your message"><?php echo $data['info']['content'] ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (1==1): ?>
                                    <div class="col-lg-6">
                                        <div class="nk-block-head">
                                            <div class="nk-block-head-content">
                                                <h4 class="title nk-block-title">Add Documents</h4>
                                            </div>
                                        </div>

                                            
                                        
                                        <div class="nk-block">
                                            <div class="card card-bordered card-stretch">
                                                <div class="card-inner-group">
                                                    <div class="card-inner p-0">
                                                        <div class="nk-tb-list nk-tb-ulist">
                                                            <div class="nk-tb-item nk-tb-head">
                                                                <div class="nk-tb-col" style="width: 1%;"><span>#</span></div>
                                                                <div class="nk-tb-col tb-col-mb" style="width: 10%;"><span>Doc Name</span></div>
                                                            </div><!-- .nk-tb-item -->
                                                            <?php echo $data['document_list'] ?> 
                                                           
                                                        </div>
                                                    </div><!-- .card-inner -->
                                                   
                                                </div><!-- .card-inner-group -->
                                            </div><!-- .card -->
                                        </div><!-- .nk-block -->
                                        
                                    </div>
                                    <?php endif ?>
                                    <div class="col-lg-12">
                                        <div class="form-error"></div>
                                    </div>
                                    <div class="form_submit_footer">
                                        <div class="form_footer_contents">
                                            <div class="text-right m-b-0">
                                                <a href='<?php echo COREPATH ?>property' class="btn btn-lg btn-danger">Cancel</a>
                                                <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                        </div><!-- .nk-block -->
                    </form>
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>



<?php include 'includes/bottom.html'; ?>