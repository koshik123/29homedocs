<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>customer/kyc">Manage KYC</a></li>
                                <li class="breadcrumb-item active">KYC Info </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage KYC Info </h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <?php if ($data['info']['kyc_status']!=1){ ?>
                                        <div class="float_right">
                                            <a href="#"  class="btn btn-info approverejectkyc" data-value='1' data-option='<?php echo $data['info']['id'] ?>' ><em class="icon ni ni-check"></em> <span>Approve</span></a>
                                            <a href="#"  class="btn btn-danger approverejectkyc " data-value='3' data-option='<?php echo $data['info']['id'] ?>'><em class="icon ni ni-cross"></em> <span>Reject</span></a>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="float_right">
                                            <a href="#"  class="btn btn-danger approverejectkyc " data-value='3' data-option='<?php echo $data['info']['id'] ?>'><em class="icon ni ni-cross"></em> <span>Reject</span></a>
                                        </div>
                                    <?php } ?>
                                    
                                </div>
                               
                            </div>
                        </div>
                        <div class="nk-block">
                    <div class="row gy-5">
                        <div class="col-lg-5">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Application Info</h5>
                                    <p>Submission date, approve date, status etc.</p>
                                </div>
                            </div><!-- .nk-block-head -->
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Submitted By</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['uid']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Submitted At</div>
                                            <div class="data-value">
                                            <?php echo $data['info']['kyc_submittedon']=="" ? '-' : date("d M, Y h:i a",strtotime($data['info']['kyc_submittedon'])) ?>

                                            </div>
                                        </div>
                                    </li>
                                    <?php 
                                    


                                    $kyc_status1 = (($data['info']['kyc_status']=='0') ? '<span class="badge badge-dim badge-sm badge-outline-warning">Not submitted</span>' : $data['info']['kyc_status']  );
                                    $kyc_status2  = (($data['info']['kyc_status']=='1') ? '<span class="badge badge-dim badge-sm badge-outline-success">Approved</span>' : $kyc_status1 );
                                    $kyc_status3  = (($data['info']['kyc_status']=='2') ? '<span class="badge badge-dim badge-sm badge-outline-info">Pending Approval</span>' : $kyc_status2 );
                                    $kyc_status  = (($data['info']['kyc_status']=='3') ? '<span class="badge badge-dim badge-sm badge-outline-danger">Rejected </span>' : $kyc_status3 );

                                     ?>
                                    
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Status</div>
                                            <div class="data-value"><?php echo $kyc_status ?></div>
                                        </div>
                                    </li>
                                    <!-- <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Last Checked</div>
                                            <div class="data-value">
                                                <div class="user-card">
                                                    <div class="user-name">
                                                        <span class="tb-lead"><a href="javascript:void();">-</a></span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </li> -->
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Last Checked At</div>
                                            <div class="data-value"> <?php echo $data['info']['approved_on']=="" ? '-' : date("d M, Y h:i a",strtotime($data['info']['approved_on'])) ?></div>
                                        </div>
                                    </li>
                                </ul>
                            </div><!-- .card -->
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Uploaded Documents</h5>
                                    <p>Here is user uploaded documents.</p>
                                </div>
                            </div><!-- .nk-block-head -->
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <?php echo $data['kyc_list'] ?>
                                </ul>
                            </div><!-- .card -->
                        </div><!-- .col -->
                        <div class="col-lg-7">
                            <div class="nk-block-head">
                                <div class="nk-block-head-content">
                                    <h5 class="nk-block-title title">Applicant Information</h5>
                                    <p>Basic info, like name, phone, address, country etc.</p>
                                </div>
                            </div>
                            <div class="card card-bordered">
                                <ul class="data-list is-compact">
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Name</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['name']) ?></div>
                                        </div>
                                    </li>
                                    
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Email Address</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['email']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Phone Number</div>
                                            <div class="data-value text-soft"><em><?php echo ucwords($data['info']['mobile']) ?></em></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Gender</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['gender']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Date of Birth</div>
                                            <div class="data-value"><?php echo $data['info']['dob']=="" ? '' : date("d M, Y",strtotime($data['info']['dob'])) ?></div>
                                        </div>
                                    </li>

                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Full Address</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['address']) ?></div>
                                        </div>
                                    </li>
                                    <li class="data-item">
                                        <div class="data-col">
                                            <div class="data-label">Country of Residence</div>
                                            <div class="data-value"><?php echo ucwords($data['info']['nationality']) ?></div>
                                        </div>
                                    </li>
                                   
                                    
                                </ul>
                            </div>
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade kycModel" tabindex="-1" role="dialog" id="">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <form method="POST" action="#" name="updateapproverejectkyc"  id="updateapproverejectkyc">
                <div class="modal-body modal-body-sm">
                    <h5 class="title">KYC Status</h5>
                    <h5 class="title name"></h5>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal">
                            <div class="row gy-4">
                                <div class="col-md-12">
                                    <!-- <div class="kycInfo"></div> -->
                                    <input type='hidden' value='' name='fkey' id='fkey'>
                                    <input type='hidden' value='' name='kyc_status' id='kyc_status'>         
                                    <input type='hidden' value='' name='token' id='token'>
                                    <div class='form-group'>
                                        <label class='form-label' for='cf-full-name'>Remarks <en>*</en></label>
                                        <textarea class='form-control form-control-sm' id='cf-default-textarea' placeholder='Write your message' name='remarks' ></textarea>
                                    </div>


                                </div>
                                <div class="col-12">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-2 gy-2 float_right">
                                        <li>
                                            <a href="#" data-dismiss="modal" class="btn btn-lg btn-danger">Cancel</a>
                                        </li>
                                        <li>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div><!-- .tab-pane -->
                    </div>
                </div><!-- .modal-body -->
            </form>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div>

<?php include 'includes/bottom.html'; ?>




