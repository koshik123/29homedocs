<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item active">Manage  FAQ's </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        <div class="nk-block-head">
                            <div class="row">
                                <div class="col-7">
                                    <div class="nk-block-head-content">
                                        <h4 class="nk-block-title ">Manage FAQ's</h4>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="float_right">
                                    <a href="<?php echo COREPATH ?>faqs/add" class="btn btn-primary "><em class="icon ni ni-plus"></em> <span>Add FAQ's</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-bordered card-preview">
                            <div class="card-inner">
                                <table class="datatable-init table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Content</th>
                                           
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>FAQ's</td>
                                            <td>See your profile data and manage your Account to choose what is saved in our system.</td>
                                            <td><div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="<?php echo COREPATH ?>faqs/edit" class="btn btn-sm btn-primary">Edit</a>
                                                </div>
                                                <div class="tb-odr-btns d-none d-md-inline">
                                                    <a href="javascript:void();" class="btn btn-sm btn-info">View</a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>