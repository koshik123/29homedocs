<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>propertyarea"> Property Area</a></li>
                                <li class="breadcrumb-item active">Add Property Area</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="form-error"></div>
                        <br>
                    <div class="nk-block nk-block-lg">
                    <form id="addPropertyArea" name="addPropertyArea" method="POST" action="#" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $_SESSION['add_property_area_key'] ?>" name="fkey" id="fkey">   
                        <div class="row g-gs">
                            <div class="col-lg-7 minheight">
                                <div class="card card-bordered ">
                                    <div class="card-inner">
                                        <div class="card-head">
                                            <h5 class="card-title">Add Property Area</h5>
                                        </div>
                                        <div class="row g-4">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-label" for="title">Title <en>*</en></label>
                                                    <input type="text" class="form-control" id="title" name="title">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="carpet_area">Carpet Area <en>*</en></label>
                                                    <input type="text" class="form-control" id="carpet_area" name="carpet_area">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="total_area">Total Area <en>*</en></label>
                                                    <input type="text" class="form-control" id="total_area" name="total_area">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="usable_area">Usable Area % <en>*</en></label>
                                                    <input type="text" class="form-control" id="usable_area" name="usable_area">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="common_area">Common Area % <en>*</en></label>
                                                    <input type="text" class="form-control" id="common_area" name="common_area">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="uds">UDS <en>*</en></label>
                                                    <input type="text" class="form-control" id="uds" name="uds">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="uds_perc">UDS % <en>*</en></label>
                                                    <input type="text" class="form-control" id="uds_perc" name="uds_perc">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form_submit_footer">
                                <div class="form_footer_contents">
                                    <div class="text-right m-b-0">
                                        <a href='<?php echo COREPATH ?>propertyarea' class="btn btn-lg btn-danger">Cancel</a>
                                        <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>



<?php include 'includes/bottom.html'; ?>