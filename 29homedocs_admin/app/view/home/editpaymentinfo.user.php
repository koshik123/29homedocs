<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>paymentinfo">Payment Informations</a></li>
                                <li class="breadcrumb-item active">Edit Payment Information</li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                       <form id="editPaymentInfo" name="editPaymentInfo" method="POST" action="#" enctype="multipart/form-data">
                            <input type="hidden" value="<?php echo $_SESSION['edit_paymentinfo_key'] ?>" name="fkey" id="fkey">
                            <input type="hidden" value="<?php echo $data['token'] ?>" name="session_token" id="session_token">
                            <div class="row g-gs">
                                <div class="col-lg-8">
                                    <div class="card card-bordered ">
                                        <div class="card-inner">
                                            <div class="card-head">
                                                <h5 class="card-title">Edit Payment Information</h5>
                                            </div>
                                           
                                                <div class="row g-4">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="name">Name <en>*</en></label>
                                                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $data['info']['name'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="account_number">Account Number <en>*</en></label>
                                                            <input type="text" class="form-control" id="account_number" name="account_number" value="<?php echo $data['info']['account_number'] ?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="bank">Bank <en>*</en></label>
                                                            <input type="text" class="form-control" id="bank" name="bank" value="<?php echo $data['info']['bank'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="ifcs_code">IFSC Code <en>*</en></label>
                                                            <input type="text" class="form-control" id="ifcs_code" name="ifcs_code" value="<?php echo $data['info']['ifcs_code'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="fva-topics">Branch <en>*</en></label>
                                                            <input type="text" class="form-control" id="branch" name="branch" value="<?php echo $data['info']['branch'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="city">City</label>
                                                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $data['info']['city'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="state">State</label>
                                                            <input type="text" class="form-control" id="state" name="state" value="<?php echo $data['info']['state'] ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="form-label" for="pincode">Pincode</label>
                                                            <input type="text" class="form-control" id="pincode" name="pincode" value="<?php echo $data['info']['pincode'] ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                              

                                <div class="form_submit_footer">
                                    <div class="form_footer_contents">
                                        <div class="text-right m-b-0">
                                            <a href='<?php echo COREPATH ?>paymentinfo' class="btn btn-lg btn-danger">Cancel</a>
                                            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div><!-- .nk-block -->
                   
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>


<?php include 'includes/bottom.html'; ?>