<?php include 'includes/top.html'; ?>

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="components-preview ">
                    <div class="nk-block-head nk-block-head-lg wide-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo COREPATH ?>documents/project">Document Checklists</a></li>
                                <li class="breadcrumb-item active"><?php echo ucwords($data['info']['doc_title']) ?> </li>
                            </ul>
                        </nav>
                    </div><!-- .nk-block -->
                    <div class="nk-block nk-block-lg">
                        
                        <div class="nk-block">
                        <div class="row gy-5">
                            <div class="col-lg-7">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title"><?php echo ucwords($data['info']['doc_title']) ?> Info</h5>
                                    </div>
                                </div>
                                <div class="card card-bordered">
                                    <ul class="data-list is-compact">
                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Title</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['doc_title']) ?></div>
                                            </div>
                                        </li>


                                        <li class="data-item">
                                            <div class="data-col">
                                                <div class="data-label">Sort Order</div>
                                                <div class="data-value"><?php echo ucwords($data['info']['sort_order']) ?></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- .col -->
                            <div class="col-lg-12">
                                
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title title">Document Checklists</h5>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="card card-bordered">
                                    <table class="table table-bordered valign table_custom" >
                                        <thead>
                                            <tr>
                                                <th width="15%"> Document Name</th>
                                                <th width="80%"> Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php echo $data['documents'] ?>
                                        </tbody>
                                    </table>
                                </div><!-- .card -->
                            </div><!-- .col -->
                            
                        </div><!-- .row -->
                    </div><!-- .nk-block -->
                    </div> <!-- nk-block -->
                    
                </div><!-- .components-preview -->
            </div>
        </div>
    </div>
</div>




<?php include 'includes/bottom.html'; ?>




