<?php
	require_once 'Model.php';
	require_once 'config/config.php';
	require_once 'app/core/classes/PHPMailerAutoload.php';

	class User extends Model
	{

	/*--------------------------------------------- 
					User Info
	----------------------------------------------*/

	// General User info for all pages

	function userInfo($id)
	{
		$today = date("Y-m-d");
		$project_id = @$_SESSION['crm_project_id'];
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT E.id,(E.name) as username, E.status,E.email FROM ".EMPLOYEE." E  WHERE E.id ='".$id."' AND E.tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	/*--------------------------------------------- 
					Login Activity
	----------------------------------------------*/

	function manageLoginActivity()
  	{
  		$layout = "";
  		$id=$_SESSION["crm_admin_id"];
  		$project_id = @$_SESSION['crm_project_id'];
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT  id,logged_id,user_type,portal_type,auth_referer,auth_medium,auth_user_agent,auth_ip_address,session_in  FROM ".SESSION_TBL." M WHERE user_type='admin' AND logged_id='$id' AND tenant_id='".$tenant_id."' ORDER BY id DESC LIMIT 20  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);
	    		$layout .= "
	    			<tr>
                        <td>".($list['auth_user_agent'])."</td>
                        <td>".$list['auth_ip_address']."</td>
                        <td>".date("M D, y h:i a")."</td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			 	General Settings
	----------------------------------------------*/

	
	function getFloortypeList($current="")
	{
		$layout ="";
		$project_id = @$_SESSION['crm_project_id'];
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,floor,status  FROM ".FLOOR_TBL." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['floor'])."</option>";
			}
		}
		return $layout;
	}

	function getBlocktypeList($current="")
	{
		$layout ="";
		$project_id = @$_SESSION['crm_project_id'];
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,block,status  FROM ".BLOCK_TBL." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['block'])."</option>";
			}
		}
		return $layout;
	}

	function getVillaypeList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,flat_variant,status  FROM ".FLATTYPE_MASTER." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['flat_variant'])."</option>";
			}
		}
		return $layout;
	}

	function getPropertyList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,title,status  FROM ".PROPERTY_TBL." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['title'])."</option>";
			}
		}
		return $layout;
	}


	function getCustomerList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getAssignPropertyList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,title,status  FROM ".PROPERTY_TBL." WHERE status='1' AND assign_status!='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['title'])."</option>";
			}
		}
		return $layout;
	}

	function getAssignCustomerList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,name,status  FROM ".CUSTOMER_TBL." WHERE status='1' AND assign_status!='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getActivityType($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,activity_type,status  FROM ".ACTIVITY_TYPE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['activity_type'])."</option>";
			}
		}
		return $layout;
	}

	function getProjectList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,project_name,status  FROM ".PROJECT_TYPE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['project_name'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadStatus($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,lead_type,status  FROM ".LEAD_TYPE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['lead_type'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadStatusName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,lead_type,status  FROM ".LEAD_TYPE." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['lead_type']);
			}
		}
		return $layout;
	}

	function getBlockTypeName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,block,status  FROM ".BLOCK_TBL." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['block']);
			}
		}
		return $layout;
	}

	function getFloorTypeName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,floor,status  FROM ".FLOOR_TBL." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['floor']);
			}
		}
		return $layout;
	}

	function getRoomTypeName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,flat_variant,status  FROM ".FLATTYPE_MASTER." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['flat_variant']);
			}
		}
		return $layout;
	}


	function getEmployeesName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,name,status  FROM ".EMPLOYEE." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['name']);
			}
		}
		return $layout;
	}

	function getRequestTypeName($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,request_type,status  FROM ".REQUEST_TYPE_TBL." WHERE id='".$current."' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$layout.= ucwords($list['request_type']);
			}
		}
		return $layout;
	}

	function getEmployeeList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,name,status  FROM ".EMPLOYEE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['name'])."</option>";
			}
		}
		return $layout;
	}

	function getLeadSource($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,lead_source,status  FROM ".LEAD_SOURCE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['lead_source'])."</option>";
			}
		}
		return $layout;
	}

	function getCustomerProfileList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,customer_profile,status  FROM ".CUSTOMER_PROFILE." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['customer_profile'])."</option>";
			}
		}
		return $layout;
	}

	function getRequestTypeList($current="")
	{
		$layout ="";
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,token,request_type,status  FROM ".REQUEST_TYPE_TBL." WHERE status='1' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);	
		if(mysqli_num_rows($exe) > 0){
			while ($list = mysqli_fetch_array($exe)) {
				$selected = (($list['id']==$current) ? 'selected' : '');
				$layout.= "<option value='".$list['id']."' $selected>".ucwords($list['request_type'])."</option>";
			}
		}
		return $layout;
	}

  	/*--------------------------------------------- 
				Customer List
	----------------------------------------------*/

	function manageCustomer()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,name,uid,gender,mobile,email,address,city,state ,pincode,kyc_status,status,assign_status,priority  FROM ".CUSTOMER_TBL." WHERE 1 AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $kyc_status1 = (($list['kyc_status']=='0') ? '<span class="badge badge-warning">Not submitted</span>' : $list['kyc_status']  );
                $kyc_status2  = (($list['kyc_status']=='1') ? '<span class="badge badge-success">Approved</span>' : $kyc_status1 );
                $kyc_status3  = (($list['kyc_status']=='2') ? '<span class="badge badge-info">Pending Approval</span>' : $kyc_status2 );
                $kyc_status  = (($list['kyc_status']=='3') ? '<span class="badge badge-danger">Rejected </span>' : $kyc_status3 );

                $assign_status = (($list['assign_status']!=1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/assign/".$list['id']."' class='btn btn-sm btn-info' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-account-setting'></em> Assign</a></div>" : '' );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."customer/details/".$list['id']."' target='_blank'>".$list['uid']."</a></td>
                        <td><a href='".COREPATH."customer/details/".$list['id']."' target='_blank'>".ucwords($list['name'])."</a></td>
                        <td><i class='la la-phone'></i> ".$list['mobile']." </td>
                        <td><i class='la la-envelope'></i> ".$list['email']."</td>
                        <td>".ucwords($list['priority'])."</td>
                        <td>".$kyc_status."</td>
                        <td>
		                    
		                    <div class='custom-control custom-control-sm custom-checkbox CustomerStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."customer/details/".$list['id']."' target='_blank' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <br>
                            ".$assign_status."
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Block List
	----------------------------------------------*/

	function manageBlock()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT B.id,B.token,B.block,B.status,B.added_by,B.created_at,B.added_by,E.name FROM ".BLOCK_TBL." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE 1 AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['block'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox BlockStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editBlock' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Floor List
	----------------------------------------------*/

	function manageFloors()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT F.id,F.token,F.floor,F.status,F.added_by,F.created_at,F.added_by,E.name FROM ".FLOOR_TBL." F LEFT JOIN ".EMPLOYEE." E ON (E.id=F.added_by) WHERE 1 AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['floor'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox FloorStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                 <a href='javascript:void();' class='btn btn-sm btn-success editFloor' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Flat / Villa Document Types
	----------------------------------------------*/

	function manageFlatVillaDocumentsType()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,doc_title,sort_order,status,publish_status   FROM ".FLATVILLA_DOC_TYPE." WHERE deleted_status='0' AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $publish_status = (($list['publish_status']==1) ? "<span class='badge badge-success'>Published</span>" : "<span class='badge badge-warning'>Not Publish</span>");
                $btnpublish_status = (($list['publish_status']==1) ? "" : "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletCheckListDoc' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm publishCheckListDoc' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-check'></span>  Publish</a>	
                            </div>");

	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td><a href='".COREPATH."documents/projectdetails/".$list['id']."' >".ucwords($list['doc_title'])."</a></td>
                        <td> ".$list['sort_order']." </td>
                        <td> ".$publish_status." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox faltVillaTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."documents/editproject/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."documents/projectdetails/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            ".$btnpublish_status."
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Edit page additional 

  	function getDocCheckList($document_id="",$publish_status="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,document_id,doc_name,description,status FROM ".FLATVILLA_DOCTYPE_LIST."  WHERE document_id='$document_id' AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);	 
	    		$btnstatus = (($publish_status=='0') ? "<td width='10%'> 	
	    				<a href='javascript:void()' class='btn btn-success btn-sm editCheckListInformation' data-value='".$i."'  data-type='".$list['id']."' data-option='".$list['id']."' > <span class='icon ni ni-edit'></span></a>
	    				<a href='javascript:void()' class='btn btn-danger btn-sm deletCheckListInformation' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
	    			</td>" : ""  );   		
	    		$layout .= "
	    		<tr id='attacjmentrow_".$list['id']."'>
	    			<td width='15%'>".($list['doc_name'])."</td>
                    <td width='15%'>".($list['description'])."</td>
                    ".$btnstatus."
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;	
  	}

  	function DocCheckList($document_id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,document_id,doc_name,description,status FROM ".FLATVILLA_DOCTYPE_LIST."  WHERE document_id='$document_id' AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);	    		
	    		$layout .= "
	    		<tr>
	    			<td width='15%'>".($list['doc_name'])."</td>
                    <td width='15%'>".($list['description'])."</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;	
  	}

  	/*--------------------------------------------- 
				Contact Info
	----------------------------------------------*/

	function manageContactInfo()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,name,role,email,mobile,status FROM ".CONTACT_INFO." WHERE 1  AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."contactinfo/details/".$list['id']."'>".ucwords($list['name'])."</a></td>
                        <td><i class='la la-phone'></i> ".$list['mobile']." </td>
                        <td><i class='la la-envelope'></i> ".$list['email']."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox ContactInfoStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."contactinfo/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."contactinfo/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-danger deleteContactInfo' data-token='".$this->encryptData($list['id'])."'><em class='icon ni ni-trash'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Payment Info
	----------------------------------------------*/

	function managePaymentInfo()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,name ,account_number,bank ,ifcs_code,branch ,city,state,pincode,status FROM ".PAYMENT_INFO." WHERE 1 AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."paymentinfo/details/".$list['id']."'>".ucwords($list['bank'])."</a></td>
                        <td>".$list['account_number']."</td>
                        <td>".$list['ifcs_code']."</td>
                        <td>".ucwords($list['branch'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox PaymentInfoStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."paymentinfo/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."paymentinfo/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-danger deletePaymentInfo' data-token='".$this->encryptData($list['id'])."'><em class='icon ni ni-trash'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Employees
	----------------------------------------------*/

	function manageEmployee()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,name,role ,gender  ,email ,mobile ,city,state,pincode,status FROM ".EMPLOYEE." WHERE 1 AND super_admin='0' AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."employee/details/".$list['id']."'>".ucwords($list['name'])."</a></td>
                        <td>".ucwords($list['role'])."</td>
                        <td>".$list['mobile']."</td>
                        <td>".$list['email']."</td>
                        
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox employeeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='#' class='btn btn-sm btn-primary changepasswordEmployee'  data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-lock-alt'></em></a>

	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/permission/".$list['id']."' class='btn btn-sm btn-info'><em class='icon ni ni-account-setting'></em></a>
	                        </div>
	                        <div class='tb-odr-btns d-none d-md-inline'>
	                            <a href='".COREPATH."employee/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
	                        </div>
	                    </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage Property 
	----------------------------------------------*/

	function manageProperty()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.customer_id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at,P.house_facing,P.assign_status,C.name,P.deleted_status  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=P.customer_id) WHERE  P.deleted_status='0' AND P.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger"); 
                $other_btn  = (($list['assign_status']=='1') ? "<div class='tb-odr-btns d-none d-md-inline'>
                                  <a href='".COREPATH."property/images/".$list['id']."' class='btn btn-sm btn-info' ><em class='icon ni ni-img'></em> </a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/document/".$list['id']."' class='btn btn-sm btn-blue' ><em class='icon ni ni-file'></em> </a>
                            </div>" : "" );    
                $assign_btn = (($list['assign_status']=='0') ? " <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/assign/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'> Assign Customer</em></a>
                            </div>" : "" );
                /*<td><span class='badge badge-dot badge-dot-xs badge-success'>Open</span></td>*/
                $customerinfo = (($list['assign_status']=='0') ? $assign_btn : "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' >".ucwords($list['name'])." </a>" );      
                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletProperty' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td><a href='".COREPATH."property/details/".$list['id']."' target='_blank'>".ucwords($list['title'])."</a></td>
                        <td>".ucwords($list['house_facing'])."</td>
                        <td>".ucwords($list['block'])."</td>
                        <td>".ucwords($list['floor'])."</td>
                        <td>".ucwords($list['flat_variant'])."</td>
                        <td>$customerinfo</td>
		    			<td>
		                    <div class='custom-control custom-control-sm custom-checkbox propertyStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/details/".$list['id']."' target='_blank' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            ".$deletedbtn."
                        </td>
                    </tr>
	    		";
	    		/*<a href='#' data-toggle='modal' data-target='#imageupload' class='btn btn-sm btn-info '><em class='icon ni ni-img'></em> </a>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function managePropertyFilter($facing,$block,$floor,$room)
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.customer_id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at,P.house_facing,P.assign_status,C.name  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=P.customer_id) WHERE 1 AND P.tenant_id='".$tenant_id."'  " ;
		if ($facing!="") {
			$q .= " AND P.house_facing='$facing' ";
		}
		if ($block!="") {
			$q .= " AND P.block_id='$block' ";
		}
		if ($floor!="") {
			$q .= " AND P.floor_id='$floor' ";
		}
		if ($room!="") {
			$q .= " AND P.room_id='$room' ";
		}
		$q .= " ORDER BY P.id  ";
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger"); 
                $other_btn  = (($list['assign_status']=='1') ? "<div class='tb-odr-btns d-none d-md-inline'>
                                  <a href='".COREPATH."property/images/".$list['id']."' class='btn btn-sm btn-info' ><em class='icon ni ni-img'></em> </a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/document/".$list['id']."' class='btn btn-sm btn-blue' ><em class='icon ni ni-file'></em> </a>
                            </div>" : "" );    
                $assign_btn = (($list['assign_status']=='0') ? " <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/assign/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'> Assign Customer</em></a>
                            </div>" : "" );
                /*<td><span class='badge badge-dot badge-dot-xs badge-success'>Open</span></td>*/
                $customerinfo = (($list['assign_status']=='0') ? $assign_btn : "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' >".ucwords($list['name'])." </a>" );         
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['title'])."</td>
                        <td>".ucwords($list['house_facing'])."</td>
                        <td>".ucwords($list['block'])."</td>
                        <td>".ucwords($list['floor'])."</td>
                        <td>".ucwords($list['flat_variant'])."</td>
                        <td>$customerinfo</td>
		    			<td>
		                    <div class='custom-control custom-control-sm custom-checkbox propertyStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            ".$other_btn."
                            
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."property/details/".$list['id']."' target='_blank' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>

                        </td>
                    </tr>
	    		";
	    		/*<a href='#' data-toggle='modal' data-target='#imageupload' class='btn btn-sm btn-info '><em class='icon ni ni-img'></em> </a>*/
	    		$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count;
	    return $result;
  	}

  	function manageNewProperty_old()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) WHERE 1 AND P.tenant_id='".$tenant_id."' GROUP BY P.block_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/             
	    		$layout .= "
	    		<div class='col-lg-12'>
		    		<div class='card card-bordered '>
	                    <div class='card-inner'>
	                        <div class='card-title-group align-start '>
	                           <div class='card-title'>
	                               <h6 class='title'>".ucwords($list['block'])."</h6>
	                            </div>
	                        </div>
	                    </div>
	                </div>
		    		<div class='nk-order-ovwg mt15'>
	                    <div class='row g-4 align-end'>
	                        <div class='col-xxl-4'>
	                            <div class='row' style='margin-right: 7%;'>
					    			<div class='col-sm-12 col-xxl-12 offset-md-1 pull-md-1 '>
					    				".$this->getFloorName($list['block_id'])."
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>

	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getFloorName_old($block_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) WHERE P.block_id='$block_id' AND P.tenant_id='".$tenant_id."' GROUP BY P.floor_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/             
	    		$layout .= "
	    		
    			<div class='card-title'>
                   <h6 class='title'>".ucwords($list['floor'])."</h6>
                </div>
                <div class='row'>
                    ".$this->getRoomName($block_id,$list['floor_id'])."
                </div>

	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getRoomName_old($block_id,$floor_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.customer_id,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at,P.assign_status,C.name,C.uid  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=P.customer_id) WHERE P.block_id='$block_id' AND P.floor_id='$floor_id' AND P.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/  
	    		
	    		$cusinfo = (($list['assign_status']=='1') ? "<p style='font-size: 15px;margin-top: 3px;'><a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank'>".ucwords($list['name'])."</p></a>" : "" );
	    		$assign_color = (($list['assign_status']=='1') ? "assigned" : "unassigned" );

	    		$other_btn  = (($list['assign_status']=='1') ? "<li><a href='".COREPATH."property/images/".$list['id']."'>Add Gallery</a></li><li><a href='".COREPATH."property/document/".$list['id']."'>Add Document </a></li>" : "" );
	    		$assign_btn = (($list['assign_status']=='0') ? "<li><a href='".COREPATH."property/assign/".$list['id']."'>Assign Customer </a></li>" : "" );
	    		$layout .= "
	    		
    			<div class='col-sm-3'>
                    <div class='card card-bordered ".$assign_color."'>
                        <div class='nk-wgw room_no_warp'>
                            <div class='nk-wgw-inner'>
                                <a class='nk-wgw-name ' href='javascript:void();'>
                                    <h5 class='nk-wgw-title title'>".ucwords($list['title'])."</h5>
                                </a>
                                ".$cusinfo."
                            </div>
                            <div class='nk-wgw-more dropdown room_no_dropdown'>
                                <a href='javascript:void();' class='btn btn-icon btn-trigger' data-toggle='dropdown' aria-expanded='false'><em class='icon ni ni-more-h'></em></a>
                                <div class='dropdown-menu dropdown-menu-xs dropdown-menu-right' style=''>
                                    <ul class='link-list-plain sm'>
                                    	<li><a href='".COREPATH."property/details/".$list['id']."' target='_blank'>View</a></li>
                                        <li><a href='".COREPATH."property/edit/".$list['id']."'>Edit</a></li>
                                        ".$other_btn."
                                        ".$assign_btn."
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageNewProperty()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) WHERE 1 AND P.tenant_id='".$tenant_id."' GROUP BY P.block_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/             
	    		$layout .= "
	    		<div class='col-lg-12'>
		    		<div class='card card-bordered '>
	                    <div class='card-inner'>
	                        <div class='card-title-group align-start '>
	                           <div class='card-title'>
	                               <h6 class='title'>".ucwords($list['block'])."</h6>
	                            </div>
	                        </div>
	                    </div>
	                </div>
		    		<div class='nk-order-ovwg mt15'>
	                    <div class='row g-4 align-end'>
	                        <div class='col-xxl-12'>
	                            <div class='row' >
	                            	
					    			".$this->getFloorName($list['block_id'])."
					    			
				                </div>
				            </div>
				        </div>
				    </div>
				</div>

	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getFloorName($block_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) WHERE P.block_id='$block_id' AND P.tenant_id='".$tenant_id."' GROUP BY P.floor_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/             
	    		$layout .= "
	    		
    			
	    		<div class='col-sm-1 col-xxl-1  '>
                    <div class='card card-bordered h-100 minheight110'>
                        <div class='nk-wgw '>
                            <div class='nk-wgw-inner floor_warp'>
                                <a class='nk-wgw-name floor_title' href='javascript:void();'>
                                    <h5 class='nk-wgw-title title'>".ucwords($list['floor'])."</h5>
                                </a>
                            </div>
                        </div>
                    </div>                                                  
                </div>
                <div class='col-sm-11 col-xxl-11  property_chart_warp'>
                    ".$this->getRoomName($block_id,$list['floor_id'])."
                </div>

	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getRoomName($block_id,$floor_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.title,P.type,P.customer_id,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.house_facing,P.created_at,P.assign_status,C.name,C.uid,FM.flat_variant  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=P.customer_id) LEFT JOIN ".FLATTYPE_MASTER." FM ON (FM.id=P.room_id) WHERE P.block_id='$block_id' AND P.floor_id='$floor_id' AND P.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		/*assigned, unassigned*/  
	    		
	    		$cusinfo = (($list['assign_status']=='1') ? "<p style='font-size: 15px;margin-top: 3px;'><a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank'>".ucwords($list['name'])."</p></a>" : "" );
	    		$assign_color = (($list['assign_status']=='1') ? "assigned" : "unassigned" );

	    		$other_btn  = (($list['assign_status']=='1') ? "<li><a href='".COREPATH."property/images/".$list['id']."'>Add Gallery</a></li><li><a href='".COREPATH."property/document/".$list['id']."'>Add Document </a></li>" : "" );
	    		$assign_btn = (($list['assign_status']=='0') ? "<li><a href='".COREPATH."property/assign/".$list['id']."'>Assign Customer </a></li>" : "" );
	    		$layout .= "

                <div class='card card-bordered small_box_warp ".$assign_color."'>
                    <div class='nk-wgw room_no_warp'>
                        <div class='text_center'>
                            <h5 class=' title title_warp'>".ucwords($list['title'])."</h5>
                            <h5 class=' title title_warp'>".$list['flat_variant']." (".ucwords($list['house_facing']).")</h5>
                            <h5 class=' title title_warp'> ".$list['projectsize']." sq.ft</h5>
                        </div>
                        <div class='nk-wgw-more dropdown room_no_dropdown'>
                            <a href='javascript:void();' class='btn btn-icon btn-trigger' data-toggle='dropdown' aria-expanded='false'><em class='icon ni ni-more-h'></em></a>
                            <div class='dropdown-menu dropdown-menu-xs dropdown-menu-right' style=''>
                                <ul class='link-list-plain sm'>
                                	<li><a href='".COREPATH."property/details/".$list['id']."' target='_blank'>View</a></li>
                                    <li><a href='".COREPATH."property/edit/".$list['id']."'>Edit</a></li>
                                    ".$other_btn."
                                    ".$assign_btn."
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
	    		";
	    	
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				General Documents
	----------------------------------------------*/

	function manageGeneralDocuments()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,title,documents,status,created_at FROM ".GENDRAL_DOCUMENTS." WHERE 1 AND deleted_status='0' AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."documents/details/".$list['id']."' >".ucwords($list['title'])."</a></td>
                        <td><a href='".DOCUMENTS.$list['documents']."' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file'></em></a></td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox generalDocumentStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."documents/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."documents/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                            <a href='javascript:void()' class='btn btn-danger btn-sm deletGeneralDocList' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage Property Brochure
	----------------------------------------------*/

	function managePropertyBrouchers()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT B.id,B.token,B.title ,B.property_id ,B.description ,B.brochures ,B.status,P.title as pro_title,B.deleted_status  FROM ".PROPERTY_BROUCHERS." B LEFT JOIN ".PROPERTY_TBL." P ON (P.id=B.property_id) WHERE B.deleted_status='0' AND B.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
               	$deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletePropertyBrochure' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."propertybrochure/details/".$list['id']."'>".ucwords($list['title'])."</a></td>
                        <td>".ucwords($list['pro_title'])."</td>
                        <td><a href='".DOCUMENTS.$list['brochures']."' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file-pdf'></em></a></td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox propertyBroucherStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."propertybrochure/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."propertybrochure/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></em></a>
                            </div>
                            $deletedbtn
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage Request type
	----------------------------------------------*/

	function manageRequestType()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT B.id,B.token,B.request_type,B.status,B.added_by,B.created_at,B.added_by,E.name FROM ".REQUEST_TYPE_TBL." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE 1 AND B.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['request_type'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox requestTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editRequestType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage kyc type
	----------------------------------------------*/

	function managekycType()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT B.id,B.token,B.kyc_type,B.status,B.added_by,B.created_at,B.added_by,E.name FROM ".KYC_TYPE." B LEFT JOIN ".EMPLOYEE." E ON (E.id=B.added_by) WHERE 1 AND B.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['kyc_type'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox kycTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editkycType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage Notification Email
	----------------------------------------------*/ 

	function manageNotificationEmail()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT N.id,N.token,N.email,N.adhoc_permission,N.kyc_permission,N.status,N.added_by,N.created_at,N.added_by,E.name FROM ".NOTIFICATION_EMAIL_TBL." N LEFT JOIN ".EMPLOYEE." E ON (E.id=N.added_by) WHERE 1 AND N.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");


                $adhoc_permission = (($list['adhoc_permission']==1) ? 'Adhoc Requests & ' : '' );
                $kyc_permission = (($list['kyc_permission']==1) ? 'KYC Notification' : '' );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['email'])."</td>
                        <td>".$adhoc_permission."".$kyc_permission."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox notificationEmailStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editNotification' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                            
                        </td>
                    </tr>
	    		";
	    		/*<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-danger'><em class='icon ni ni-trash'></em></a>
                            </div>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage Gallery
	----------------------------------------------*/ 

	function manageGallery()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT G.id,G.token,G.album_title,G.image,G.status,G.added_by,G.created_at,E.name,G.deleted_status FROM ".GALLERY_TBL." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.deleted_status='0' AND G.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deleteGallery' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );
                
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><img src='".SRCIMG.$list['image']."' class='img-thumbnail' width='80'></td>
                        <td>".ucwords($list['album_title'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox galleryStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."gallery/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."gallery/images/".$list['token']."' class='btn btn-sm btn-primary'><em class='icon ni ni-img'></em></a>
                            </div>
                            $deletedbtn 
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Manage Gallery Images

  	function galleryImages($gallery_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$result = array();
  		$q = "SELECT id,image FROM ".GALLERY_IMAGE." WHERE gallery_id='$gallery_id' AND tenant_id='".$tenant_id."' ORDER BY id ASC" ;
  		$query = $this->selectQuery($q);
  		$count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
    			<li id='pro_$i' class='col-masonry'>
    				<div class='product_image_item'>
    					<img src='".SRCIMG.$list['image']."' class='img-responsive' />
    					<p><a href='javascript:void();' class='removePropertyImage' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='icon ni ni-trash' style='color: red;'></i></a> </p>
    					<span class='clearfix'></span>
    				</div>
    			</li>";
    			$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count ;
	    return $result;

  	}

  	/*--------------------------------------------- 
				Manage Flat Type Master
	----------------------------------------------*/

	function manageFlatTypeMaster()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT F.id,F.token,F.flat_variant,F.status,F.added_by,F.created_at,F.added_by,E.name FROM ".FLATTYPE_MASTER." F LEFT JOIN ".EMPLOYEE." E ON (E.id=F.added_by) WHERE 1 AND F.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."flattype/details/".$list['id']."'>".ucwords($list['token'])."</a></td>
                        <td>".ucwords($list['flat_variant'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox flattypemasterStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."flattype/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."flattype/details/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Edit page additional Contatc List

  	function getFlatTypeMasterList($id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,flattype_id,room_type,sqft ,description,status FROM ".FLATTYPE_MASTER_LIST."  WHERE flattype_id='$id' AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);	    		
	    		$layout .= "
	    		<tr id='attacjmentrow_".$list['id']."'>
	    			<td width='15%'>".($list['room_type'])."</td>
                    <td width='15%'>".($list['sqft'])."</td>
                    <td width='15%'>".($list['description'])."</td>
	    			<td width='10%'> 	
	    				<a href='javascript:void()' class='btn btn-success btn-sm editFlatTypeMasterInformation' data-value='".$i."'  data-type='".$list['id']."' data-option='".$list['id']."' > <span class='icon ni ni-edit'></span></a>
	    				<a href='javascript:void()' class='btn btn-danger btn-sm deletFlatTypeInformation' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;	
  	}

  	function flatTypeMasterList($id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,flattype_id,room_type,sqft ,description,status FROM ".FLATTYPE_MASTER_LIST."  WHERE flattype_id='$id' AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);	    		
	    		$layout .= "
	    		<tr >
	    			<td width='15%'>".($list['room_type'])."</td>
                    <td width='15%'>".($list['sqft'])."</td>
                    <td width='15%'>".($list['description'])."</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;	
  	}


  	// Edit page additional Contatc List

  	function getPropertyRoomList($flattype_id="",$property_id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT F.id,F.flattype_id,F.room_type,F.sqft,F.description,F.status,(SELECT count(id) as tot FROM ".PROPERTY_ROOMS_LIST." WHERE property_id=$property_id AND roomlist_id=F.id ) as img_count FROM ".FLATTYPE_MASTER_LIST." F  WHERE F.flattype_id='$flattype_id' AND F.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);	    

	    		$layout .= "
	    		<tr id='attacjmentrow_".$list['id']."'>
	    		<td width='1%'>".$i."</td>
	    			<td width='15%'>".($list['room_type'])."</td>
	    			<td width='15%'>".($list['img_count'])."</td>
	    			<td width='10%'> 	
	    				<a href='".COREPATH."property/addroomimg/".$list['id']."/".$property_id."' class='btn btn-success btn-sm ' ><em class='icon ni ni-plus'></em> Add Images</a>
	    			</td>
	    		</tr>";
	    		$i++;
	    	}
	    }
	    return $layout;	
  	}

  	// Manage Property Gallery Images

  	function getPropertyGalleryImageList($property_id)
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$q = "SELECT id,images,property_id FROM ".PROPERTY_GALLERY." WHERE property_id='$property_id' AND tenant_id='".$tenant_id."' ORDER BY id ASC" ;
  		$query = $this->selectQuery($q);
  		$count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
    			<li id='pro_$i' class='col-masonry'>
    				<div class='product_image_item'>
    					<img src='".SRCIMG.$list['images']."' class='img-responsive' />
    					<p><a href='javascript:void();' class='removePropertyGalleryImages' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='icon ni ni-trash' style='color: red;'></i></a> </p>
    					<span class='clearfix'></span>
    				</div>
    			</li>";
    			$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count ;
	    return $result;

  	}

  	// Manage Add Property Gallery Images

  	function getPropertygalleryImages($property_id)
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$q = "SELECT id,images FROM ".PROPERTY_GALLERY." WHERE property_id='$property_id' AND tenant_id='".$tenant_id."' ORDER BY id ASC" ;
  		$query = $this->selectQuery($q);
  		$count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
    			<li id='pro_$i' class='col-masonry'>
    				<div class='product_image_item'>
    					<img src='".SRCIMG.$list['images']."' class='img-responsive' />
    					<p><a href='javascript:void();' class='removePropertyGalleryImages' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='icon ni ni-trash' style='color: red;'></i></a> </p>
    					<span class='clearfix'></span>
    				</div>
    			</li>";
    			$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count ;
	    return $result;

  	}

  	// Manage Add Property Room Images

  	function getPropertyRoomImages($property_id="",$room_id="")
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$q = "SELECT id,image FROM ".PROPERTY_ROOMS_LIST." WHERE property_id='$property_id' AND roomlist_id='$room_id' AND tenant_id='".$tenant_id."' ORDER BY id ASC" ;
  		$query = $this->selectQuery($q);
  		$count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query)>0){
	    	$i=1;
	    	while($list = mysqli_fetch_array($query)){
	    		$layout .= "
    			<li id='pro_$i' class='col-masonry'>
    				<div class='product_image_item'>
    					<img src='".SRCIMG.$list['image']."' class='img-responsive' />
    					<p><a href='javascript:void();' class='removePropertyRoomImages' data-option='$i' data-token='".$this->encryptData($list['id'])."' ><i class='icon ni ni-trash' style='color: red;'></i></a> </p>
    					<span class='clearfix'></span>
    				</div>
    			</li>";
    			$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count'] = $count ;
	    return $result;

  	}

  	/*--------------------------------------------- 
				Flat/Villa Property Documents
	----------------------------------------------*/

	function getDocumentLists()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,doc_title ,sort_order,status,created_at FROM ".FLATVILLA_DOC_TYPE." WHERE 1 AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		
	    		$layout .= "
	    			<div class='nk-tb-item'>
                        <div class='nk-tb-col nk-tb-col-check'>
                            <div class='custom-control custom-control-sm custom-checkbox notext'>
                                <input type='checkbox' class='custom-control-input' id='uid".$i."' name='document[]' value=".$list['id']." >
                                <label class='custom-control-label' for='uid".$i."'></label>
                            </div>
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>
                                <div class='user-info'>
                                    <span class='tb-lead'>".ucwords($list['doc_title'])." <span class='dot dot-success d-md-none ml-1'></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function geteditDocumentLists($property_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,token,doc_title ,sort_order,status,created_at FROM ".FLATVILLA_DOC_TYPE." WHERE 1 AND tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$checked_status  = $this->checkedDocuments($property_id,$list['id']);
	    		$checked    = (($list['id']==$checked_status['id']) ? 'checked' : '' );
 	    		$layout .= "
	    			<div class='nk-tb-item'>
                        <div class='nk-tb-col nk-tb-col-check'>
                            <div class='custom-control custom-control-sm custom-checkbox notext'>
                                <input type='checkbox' class='custom-control-input' id='uid".$i."' name='document[]' 
                                value=".$list['id']." ".$checked.">
                                <label class='custom-control-label' for='uid".$i."'></label>
                            </div>
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>
                                <div class='user-info'>
                                    <span class='tb-lead'>".ucwords($list['doc_title'])." <span class='dot dot-success d-md-none ml-1'></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  
  	function checkedDocuments($property_id,$documents_id)
	{
		$today = date("Y-m-d");
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,property_id,documents_id FROM ".PROPERTY_DOCUMENTS." WHERE property_id='$property_id' AND documents_id='$documents_id' AND tenant_id='".$tenant_id."' ";
		$exe = $this->selectQuery($query);
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}


  	function getPropertyDocumentLists($property_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT PD.id,PD.property_id,PD.documents_id,PD.status,PD.created_at,PD.type,FD.doc_title,FD.sort_order FROM ".PROPERTY_DOCUMENTS." PD LEFT JOIN ".FLATVILLA_DOC_TYPE." FD ON (FD.id=PD.documents_id) WHERE PD.property_id='$property_id' AND PD.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		
	    		$layout .= "

	    			<div class='card-inner'>
                        <div class='card-title-group'>
                            <div class='card-title'>
                                <h6 class='title'>".ucwords($list['doc_title'])."</h6>
                            </div>
                        </div>
                    </div>
                    <div class='card-inner card-inner-md'>
                        <div class='user-card'>
                            <div class='nk-tb-list nk-tb-ulist'>
                                <div class='nk-tb-item nk-tb-head'>
                                    <div class='nk-tb-col' style='width: 1%;'><span>#</span></div>
                                    <div class='nk-tb-col tb-col-mb' style='width: 15%;'><span>Doc Name</span></div>
                                    <div class='nk-tb-col tb-col-md' style='width: 15%;'><span>Status</span></div>
                                    <div class='nk-tb-col tb-col-md' style='width: 14%;'><span>Action</span></div>
                                </div>
                                ".$this->getDocumentListItems($property_id,$list['documents_id'])."     
                            </div>
                        </div>
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }else{
	    	$layout .="
	    		
	    		<div class='no_result'>
                    No documents added!
                </div>
                
	    	";
	    }
	    return $layout;
  	}

  	function getDocumentListItems($property_id="",$document_id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,document_id,document_list_id,property_id,doc_name ,description ,document_file,status,doc_status FROM ".PROPERTY_DOCUMENTS_ITEMS." WHERE property_id='$property_id' AND document_id='$document_id' AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 	   = $this->editPagePublish($details);
	    		$doc_file  = (($list['document_file']!='') ? "<span class='tb-status text-success'>Submitted</span>" : "<span class='tb-status text-danger'>Pending</span>"  );

	    		$addfile_btn  = (($list['document_file']=='') ? "<a href='javascript:void()' class='btn btn-sm btn-green uploadDocfile' data-value='".$i."' data-property='".$property_id."'  data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em>Add Document</a>" : "<a href='javascript:void()' class='btn btn-sm btn-blue uploadDocfile' data-value='".$i."' data-property='".$property_id."'  data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em>Update Document</a>"  ); 
	    		$layout .= "
	    			<div class='nk-tb-item'>
                        <div class='nk-tb-col nk-tb-col-check'>
                            ".$i."
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>
                                <div class='user-info'>
                                    <span class='tb-lead'> <a href='javascript:void();' class='title viewPrivateDocInfo' data-value='".$i."' data-option='".$list['id']."'>".ucwords($list['doc_name'])."</a> <span class='dot dot-success d-md-none ml-1'></span></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class='nk-tb-col tb-col-lg'>
                            ".$doc_file."
                        </div>
                        <div class='nk-tb-col tb-col-mb'>
                            <div class='tb-odr-btns d-none d-md-inline'>
	                            ".$addfile_btn."
	                        </div>
                        </div>
                    </div>
	    		";
	    		/*<a href='<?php echo IMGPATH ?>sample.pdf' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file-pdf'></em></a>*/
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*------------------------------
  		Property General Doc List
  	--------------------------------*/

  	function getPropertyGeneralDocumentLists($property_id="")
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,property_id ,title ,description,documents,document_type,status FROM ".PROPERTY_GENDRAL_DOCUMENTS." WHERE property_id='$property_id' AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 	   = $this->editPagePublish($details);
	    		$doc_file  = (($list['documents']!='') ? "<span class='tb-status text-success'>Submitted</span>" : "<span class='tb-status text-danger'>Pending</span>"  ); 
	    		$doc_preview  = (($list['documents']!='') ? "<a target='blank' class='' href='".DOCUMENTS.$list['documents']."''><img src='".IMGPATH."file.png' style='width: 30px;margin-bottom: 10px;margin-left: -15px;''></a>" : "-"  ); 

	    		$addfile_btn  = (($list['documents']=='') ? "<a href='javascript:void()' class='btn btn-sm btn-green uploadGenDocfile' data-value='".$i."' data-property='".$property_id."'  data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em>Add Document</a>" : "<a href='javascript:void()' class='btn btn-sm btn-blue uploadGenDocfile' data-value='".$i."' data-property='".$property_id."'  data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em>Update Document</a>"  ); 

	    		$layout .= "
	    			<div class='nk-tb-item'>
                        <div class='nk-tb-col nk-tb-col-check'>
                            ".$i."
                        </div>
                        <div class='nk-tb-col'>
                            <div class='user-card'>
                                <div class='user-info'>
                                    <span class='tb-lead'><a href='javascript:void();' class='title viewPrivateCommonDocInfo' data-value='".$i."' data-option='".$list['id']."'>".ucwords($list['title'])."</a> <span class='dot dot-success d-md-none ml-1'></span></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class='nk-tb-col tb-col-lg'>
                            ".$doc_file."
                        </div>
                        <div class='nk-tb-col tb-col-mb'>
                           ".$addfile_btn."
                        </div>
                    </div>
	    		";
	    		/*<a href='<?php echo IMGPATH ?>sample.pdf' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file-pdf'></em></a>*/
	    		
	    		$i++;
	    	}
	    }
	    $result['count']  = $count ;
	    $result['layout'] = $layout;

	    return $result;
  	}

  	/*--------------------------------------------- 
			Manage Adhoc Requests
	----------------------------------------------*/

	function manageadhocrequests($request_status="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name,C.priority  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE A.request_status='$request_status'AND A.tenant_id='".$tenant_id."'  ORDER BY A.created_at DESC " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );

	    		$priority1 = (($list['priority']=='low') ? '<span class="badge badge-success">Low</span>' : '-'  );
                $priority2  = (($list['priority']=='medium') ? '<span class="badge badge-info">Medium</span>' : $priority1 );
                $priority3  = (($list['priority']=='high') ? '<span class="badge badge-warning">High</span>' : $priority2 );
                $priority  = (($list['priority']=='urgent') ? '<span class="badge badge-danger">Urgent </span>' : $priority3 );

                $raisedby = (($list['request_type']=='user') ? '<span class="badge badge-info">Customer</span>' : '<span class="badge badge-warning">Admin</span>'  );
	    		$layout .= "
	    			<tr >
	    				<td>".$i."</td>
                        <td ><a href='".COREPATH."adhoc/details/".$list['id']."'>".$list['ticket_uid']."</a></td>
                        <td><a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank'> ".ucwords($list['name'])."</a></td>
                        <td '>
                            ".ucwords($list['subject'])."
                        </td>
                        <td '>
                            ".ucwords($list['requestname'])."<br>
                           
                        </td>
                        <td >
                           ".date("d M Y",strtotime($list['created_at']))."
                        </td>
                        <td '>
                            ".$raisedby."
                        </td>
                        <td '>
                            ".$priority."
                        </td>
                        <td >
                            ".$request_status."
                        </td>
                        <td >
                            <a href='".COREPATH."adhoc/details/".$list['id']."' class='btn btn-icon btn-trigger'>
                                <em class='icon ni ni-chevron-right'></em>
                            </a>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageFilterAdhocrequests($request_status="",$requesttype="",$priority="")
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name,C.priority  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE 1 AND A.tenant_id='".$tenant_id."'  " ;
		if ($request_status!="") {
			$q .= " AND A.request_status='$request_status' ";
		}
		if ($requesttype!="") {
			$q .= " AND A.requesttype_id='$requesttype' ";
		}
		if ($priority!="") {
			$q .= " AND C.priority='$priority' ";
		}
		$q .= " ORDER BY A.created_at DESC ";
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );

	    		$priority1 = (($list['priority']=='low') ? '<span class="badge badge-success">Low</span>' : '-'  );
                $priority2  = (($list['priority']=='medium') ? '<span class="badge badge-info">Medium</span>' : $priority1 );
                $priority3  = (($list['priority']=='high') ? '<span class="badge badge-warning">High</span>' : $priority2 );
                $priority  = (($list['priority']=='urgent') ? '<span class="badge badge-danger">Urgent </span>' : $priority3 );

                $raisedby = (($list['request_type']=='user') ? '<span class="badge badge-info">Customer</span>' : '<span class="badge badge-warning">Admin</span>'  );

	    		$layout .= "
	    			<tr >
	    				<td>".$i."</td>
                        <td ><a href='".COREPATH."adhoc/details/".$list['id']."'>".$list['ticket_uid']."</a></td>
                        <td><a href='javascript:void();'> ".ucwords($list['name'])."</a></td>
                        <td '>
                            <a href='".COREPATH."adhoc/details/".$list['id']."'><span class='title'>".ucwords($list['subject'])."</span></a>
                        </td>
                        <td '>
                            ".ucwords($list['requestname'])."
                        </td>
                        <td >
                           ".date("d M Y",strtotime($list['created_at']))."
                        </td>
                        <td '>
                            ".$raisedby."
                        </td>
                        <td '>
                            ".$priority."
                        </td>
                        <td >
                            ".$request_status."
                        </td>
                        <td >
                            <a href='".COREPATH."adhoc/details/".$list['id']."' class='btn btn-icon btn-trigger'>
                                <em class='icon ni ni-chevron-right'></em>
                            </a>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    $result['layout'] = $layout;	
	    $result['count'] = $count;	
	    return $result;
  	}

  	function getAdhocInfo($adhoc_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.ref_id,A.type,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name, (SELECT id as tots FROM ".REQUEST_LOGS." WHERE ref_id=A.id AND type='created' ) AS log_id  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE A.id='$adhoc_id' AND A.tenant_id='".$tenant_id."' ORDER BY A.id ASC  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );

	    		$words = explode(" ", $list['name']);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
            	}

                $requesttype = (($list['request_type']=="user" ? "bg-primary" : "bg-warning" ));
                $requesttype1 = (($list['request_type']=="user" ? "Customer" : "Support Team" ));	

                $attachment = $this->getTicketAttachment($list['log_id'],$list['id']);

               /* if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<ul class='ticket-msg-attach-list'>
                        		".$attachment['layout']."
                        	</ul>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }*/

                if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<div class='row'>
                        		".$attachment['layout']."
                        	</div>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }
                $raisedby = (($list['request_type']=='user') ? '<span class="badge badge-info">Customer</span>' : '<span class="badge badge-warning">Admin</span>'  );
                $type = $this->getAdhocReferenceInformation($list['ref_id'],$list['type']);
	    		$layout .= "
	    			<div class='ticket-msg-item'>
                        <div class='ticket-msg-from'>
                            <div class='ticket-msg-user user-card'>
                                <div class='user-avatar ".$requesttype."'>
                                    <span>".$acronym."</span>
                                </div>
                                <div class='user-info'>
                                    <span class='lead-text'>".ucwords($list['name'])."</span>
                                    <span class='text-soft'>".$requesttype1."</span>
                                </div>
                            </div>
                            <div class='ticket-msg-date'>
                                <span class='sub-text'>".date("M d, Y h:i A",strtotime($list['created_at']))."</span>
                                <span class='sub-text'>Raised by: ".$raisedby."</span>
                            </div>
                        </div>
                        <div class='ticket-msg-comment'>
                        	<p>".$type."</p>
                            <p>".$list['remarks']."</p>
                        </div>
                        	".$attachmentinfo."
                    </div>
	    		";
	    		/*<p><strong>".ucwords($list['subject'])."</strong></p>*/
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getAdhocReferenceInformation($ref_id,$type)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
  		if ($type=="general document") {
  			$q = "SELECT G.id,G.token,G.title,G.description,G.documents,G.document_type,G.added_by,G.status,G.created_at  FROM ".GENDRAL_DOCUMENTS." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.added_by) WHERE G.id='$ref_id' AND G.tenant_id='".$tenant_id."' " ;
		    $query = $this->selectQuery($q);	
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['document_type']);

		    		$documents_file = (($list['documents']!="") ? DOCUMENTS.$list['documents'] : 'javascript:void();' );
		    		$target_blank   = (($list['documents']!="") ? "target='_blank'"  : "" );

		    		$layout .= "<a href='".$documents_file."' $target_blank>
				    				<div class='nk-file-item nk-file'>
			                            <div class='nk-file-info'>
			                                <div class='nk-file-title'>
			                                    ".$type_icon."
			                                    <div class='nk-file-name'>
			                                        <div class='nk-file-name-text'>
			                                            <a href='".$documents_file."' $target_blank class='title ' >".ucwords($list['title'])."</a>
			                                        </div>
			                                    </div>
			                                </div>
			                                
			                            </div>
			                        </div>
	                        	</a>
		    		";
		    		
		    		$i++;
		    	}
		    }
  		}elseif($type=="private document"){
  			$q = "SELECT D.id,D.property_id ,D.document_id ,D.document_list_id,D.doc_name,D.description,D.document_file,D.doc_type,D.doc_status ,D.status,D.created_at  FROM ".PROPERTY_DOCUMENTS_ITEMS." D LEFT JOIN ".EMPLOYEE." E ON (E.id=D.added_by)  WHERE D.id='$ref_id' AND D.tenant_id='".$tenant_id."' " ;
		    $query = $this->selectQuery($q);	
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['doc_type']);
		    		$documents_file = (($list['document_file']!="") ? DOCUMENTS.$list['document_file'] : 'javascript:void();' );
		    		$target_blank = (($list['document_file']!="") ? "target='_blank'"  : "" );
		    		$layout .= "
		    				<a href='".$documents_file."' $target_blank>            
				                <div class='nk-file-item nk-file'>
				                    <div class='nk-file-info'>
				                        <div class='nk-file-title'>
				                            ".$type_icon."
				                            <div class='nk-file-name'>
				                                <div class='nk-file-name-text'>
				                                    <a class='title' href='".$documents_file."' $target_blank>".ucwords($list['doc_name'])."</a>
				                                    
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
			                </a>
							 ";
		    		
		    		$i++;
		    	}
		    }
  		}elseif($type=="private general document"){
			$q = "SELECT D.id,D.property_id ,D.title ,D.description,D.documents,D.document_type,D.status,D.created_at  FROM ".PROPERTY_GENDRAL_DOCUMENTS." D WHERE D.id='$ref_id' AND D.tenant_id='".$tenant_id."' " ;
		    $query = $this->selectQuery($q);	
		    $count = mysqli_num_rows($query);
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
		    		$list 		 = $this->editPagePublish($details);
		    		$type_icon   = $this->getFileIcon($list['document_type']);

		    		$documents_file = (($list['documents']!="") ? DOCUMENTS.$list['documents'] : 'javascript:void();' );
		    		$target_blank   = (($list['documents']!="") ? "target='_blank'"  : "" );

		    		$layout .= "
		    				<a href='javascript:void();' href='".$documents_file."' $target_blank>            
				                <div class='nk-file-item nk-file'>
				                    <div class='nk-file-info'>
				                        <div class='nk-file-title'>
				                            ".$type_icon."
				                            <div class='nk-file-name'>
				                                <div class='nk-file-name-text'>
				                                    <a href='".$documents_file."' $target_blank class='title'>".ucwords($list['title'])."</a>
				                                </div>
				                            </div>
				                        </div>
				                    </div>
				                </div>
			                </a>
							 ";
		    		
		    		$i++;
		    	}
		    }

  		}elseif($type=="community gallery"){
  			$q = "SELECT * FROM ".GALLERY_IMAGE." WHERE id='$ref_id' AND tenant_id='".$tenant_id."' ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='' href='".SRCIMG.$list['image']."' target='_blank' title=''>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade img-responsive img-thumbnail'  style='width: 200px;height: auto;'>
						</a>
						
						
						";	
		            $i++;
		      	}
		    }

  		}elseif($type=="private common gallery"){
  			$q = "SELECT * FROM ".PROPERTY_GALLERY." WHERE id='$ref_id' AND tenant_id='".$tenant_id."' ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='zooming' href='".SRCIMG.$list['images']."' title='' target='_blank'>
							<img src='".SRCIMG.$list['images']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style='width: 200px;height: auto;'>
						</a>
						";	
		            $i++;
		      	}
		    }
  			
  		}elseif($type=="private gallery"){
  			$q = "SELECT * FROM ".PROPERTY_ROOMS_LIST." WHERE id='$ref_id' AND tenant_id='".$tenant_id."'  ";
			$query = $this->selectQuery($q);	
			if(mysqli_num_rows($query) > 0){
				$i=1;
		      	while($list = mysqli_fetch_array($query)){
		      		$layout .= "
						<a class='zooming' href='".SRCIMG.$list['image']."' title='' target='_blank'>
							<img src='".SRCIMG.$list['image']."' alt='Image' class='mfp-fade item-gallery img-responsive img-thumbnail' style='width: 200px;height: auto;'>
						</a>
						";	
		            $i++;
		      	}
		    }
  		}
	    return $layout;
  	}

  	function getReplyAdhocInfo($adhoc_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.type,L.request_type,L.created_by,L.created_to,L.ref_id,L.remarks,L.status,L.created_at,L.updated_at,E.name,C.name as cus_name FROM ".REQUEST_LOGS." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.created_to) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.created_by) WHERE L.ref_id='$adhoc_id' AND L.type!='created' AND L.tenant_id='".$tenant_id."' ORDER BY L.id ASC  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		if ($list['request_type']=="user" ) {
	    			$words = explode(" ", $list['cus_name']);
	                $acronym = "";
	                foreach ($words as $w) {
	                $acronym .= $w[0];
	            	}

	            	$name = ucwords($list['cus_name']);
	    		}else{
	    			$words = explode(" ", $list['name']);
	                $acronym = "";
	                foreach ($words as $w) {
	                $acronym .= $w[0];
	            	}

	            	$name = ucwords($list['name']);
	    		}

                $requesttype = (($list['request_type']=="user" ? "bg-primary" : "bg-warning" ));
                $requesttype1 = (($list['request_type']=="user" ? "Customer" : "Support Team" ));

                 $attachment = $this->getTicketAttachment($list['id'],$list['ref_id']);

                /*if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<ul class='ticket-msg-attach-list'>
                        		".$attachment['layout']."
                        	</ul>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }*/

                if ($attachment['count']>0) {
                	$attachmentinfo = " <div class='ticket-msg-attach'>
                        	<h6 class='title'>Attachment</h6>
                        	<div class='row'>
                        		".$attachment['layout']."
                        	</div>
                   		 </div>";
                }else{
                	$attachmentinfo = "";
                }

	    		$layout .= "
	    			<div class='ticket-msg-item'>
                        <div class='ticket-msg-from'>
                            <div class='ticket-msg-user user-card'>
                                <div class='user-avatar ".$requesttype."'>
                                    <span>".$acronym."</span>
                                </div>
                                <div class='user-info'>
                                    <span class='lead-text'>".$name."</span>
                                    <span class='text-soft'>".$requesttype1."</span>
                                </div>
                            </div>
                            <div class='ticket-msg-date'>
                                <span class='sub-text'>".date("M d, Y h:i A",strtotime($list['created_at']))."</span>
                            </div>
                        </div>
                        <div class='ticket-msg-comment'>
                            <p>".$list['remarks']."</p>
                        </div>
                        ".$attachmentinfo."
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getTicketAttachment($ref_id,$adhoc_id)
  	{
  		$layout = "";
  		$result = array();
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT I.id,I.ref_id,I.adoc_id,I.property_id,I.customer_id,I.image_name,I.file_type,I.description,I.status,I.created_at,L.request_type FROM ".ADOC_IMAGES." I LEFT JOIN ".REQUEST_LOGS." L ON (L.id=I.ref_id) WHERE I.ref_id='$ref_id' AND I.adoc_id='$adhoc_id' AND I.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		
	    		$type = (($list['request_type']=='user') ? FRONTSRCIMG.$list['image_name'] : SRCIMG.$list['image_name'] );
	    		
	    		//$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? "<img src='".$type."' alt=''>" : "<img src='".IMGPATH."file.png' alt=''>" );

	    		$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? $type : IMGPATH."file.png" );

	    		$layout .= "
	    				<div class='col-md-4 col-sm-12 col-xs-12 '>
							<a href='".$type."' target='_blank'>
								<div class='event_item_list' >
									<div class='event_image' style='background-image: url($image); background-repeat: no-repeat; background-position: center top; background-size: cover;'>
									</div>
									<div class='event_info'>
										<div class='info_wrap'>
											<h5> ".$list['description']." </h5>
										</div>
									</div>
								</div>
							</a>
						</div>
                       
	    		";
	    		/* <li>
                        <a href='".$type."' target='_blank'>
                            ".$image."
                            <em class='icon ni ni-download'></em>
                        </a>
                    </li>*/
	    		$i++;
	    	}
	    }
	    $result['count'] = $count ;
	    $result['layout'] = $layout;
	    return $result;
  	}

  	function getFileIcon($value)
  	{
  		$type_icon = "";
  		if ($value=="doc" || $value=="doc") {
	    			$type_icon .= "<div class='nk-file-icon'>
                                    <span class='nk-file-icon-type'>
                                        <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                            <g>
                                                <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#599def'></path>
                                                <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#c2e1ff'></path>
                                                <rect x='27' y='31' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='36' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='41' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                                <rect x='27' y='46' width='12' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                            </g>
                                        </svg>
                                    </span>
                                </div>";
		}elseif($value=="dot"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                    <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#7e95c4'></path>
                                    <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#b7ccea'></path>
                                    <rect x='27' y='31' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='35' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='39' width='18' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='43' width='14' height='2' rx='1' ry='1' style='fill:#fff'></rect>
                                    <rect x='27' y='47' width='8' height='2' rx='1' ry='1' style='fill:#fff'></rect></svg>
                            </span>
                        </div>";
		}elseif($value=="pdf"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 90 90'><rect x='15' y='5' width='56' height='70' rx='6' ry='6' fill='#e3e7fe' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><path d='M69.88,85H30.12A6.06,6.06,0,0,1,24,79V21a6.06,6.06,0,0,1,6.12-6H59.66L76,30.47V79A6.06,6.06,0,0,1,69.88,85Z' fill='#fff' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><polyline points='60 16 60 31 75 31.07' fill='none' stroke='#6576ff' stroke-linecap='round' stroke-linejoin='round' stroke-width='2'/><path d='M57.16,60.13c-.77,0-1.53,0-2.28.08l-.82.07c-.28-.31-.55-.63-.81-1a32.06,32.06,0,0,1-4.11-6.94,28.83,28.83,0,0,0,.67-3.72,16.59,16.59,0,0,0-.49-7.29c-.29-.78-1-1.72-1.94-1.25S46.08,42.2,46,43.28a11,11,0,0,0,.12,2.63,20.88,20.88,0,0,0,.61,2.51c.23.76.49,1.51.77,2.25-.18.59-.37,1.16-.56,1.72-.46,1.28-1,2.49-1.43,3.65l-.74,1.7C44,59.52,43.18,61.26,42.25,63a27.25,27.25,0,0,0-5.72,2.85,12.36,12.36,0,0,0-2.26,2A4.33,4.33,0,0,0,33,70.24a1.62,1.62,0,0,0,.59,1.39,2.36,2.36,0,0,0,2,.27c2.19-.48,3.86-2.48,5.29-4.15a46.09,46.09,0,0,0,3.27-4.41h0a47.26,47.26,0,0,1,6.51-1.63c1.06-.18,2.15-.34,3.26-.44a15.74,15.74,0,0,0,2.54,2.07,11.65,11.65,0,0,0,2.28,1.16,15.78,15.78,0,0,0,2.45.65,7,7,0,0,0,1.3.07c1,0,2.4-.44,2.49-1.71a1.93,1.93,0,0,0-.2-1C64,61,61.33,60.55,60.1,60.34A17,17,0,0,0,57.16,60.13Zm-16,4.68c-.47.75-.91,1.44-1.33,2-1,1.48-2.2,3.25-3.9,3.91a3,3,0,0,1-1.2.22c-.4,0-.79-.21-.77-.69a2,2,0,0,1,.3-.89,5,5,0,0,1,.7-1,11.3,11.3,0,0,1,2.08-1.79,24.17,24.17,0,0,1,4.4-2.33C41.36,64.49,41.27,64.65,41.18,64.81ZM47,45.76a9.07,9.07,0,0,1-.07-2.38,6.73,6.73,0,0,1,.22-1.12c.1-.3.29-1,.61-1.13.51-.15.67,1,.73,1.36a15.91,15.91,0,0,1-.36,5.87c-.06.3-.13.59-.21.88-.12-.36-.24-.73-.35-1.09A19.24,19.24,0,0,1,47,45.76Zm3.55,15A46.66,46.66,0,0,0,45,62a14.87,14.87,0,0,0,1.38-2.39,29.68,29.68,0,0,0,2.42-5.92,28.69,28.69,0,0,0,3.87,6.15l.43.51C52.22,60.48,51.36,60.6,50.52,60.74Zm13.15,2.64c-.07.41-.89.65-1.27.71A6.84,6.84,0,0,1,59,63.74a10,10,0,0,1-2.15-1.06,12.35,12.35,0,0,1-1.9-1.51c.73,0,1.47-.07,2.21-.06a18.42,18.42,0,0,1,2.23.15,7.6,7.6,0,0,1,4,1.63C63.63,63.07,63.7,63.24,63.67,63.38Z' fill='#6576ff'/></svg>
                            </span>
                        </div>";
		}elseif($value=="jpeg" || $value=="jpg" || $value=="png" || $value=="JPG" || $value=="JPEG" || $value=="PNG"){
			$type_icon .= "<div class='nk-file-icon'>
                            <span class='nk-file-icon-type'>
                                <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                    <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#755de0'></path>
                                    <path d='M27.2223,43H44.7086s2.325-.2815.7357-1.897l-5.6034-5.4985s-1.5115-1.7913-3.3357.7933L33.56,40.4707a.6887.6887,0,0,1-1.0186.0486l-1.9-1.6393s-1.3291-1.5866-2.4758,0c-.6561.9079-2.0261,2.8489-2.0261,2.8489S25.4268,43,27.2223,43Z' style='fill:#fff'></path>
                                    <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#b5b3ff'></path></svg>
                            </span>
                        </div>";
		}elseif($value=="xlsx"){
			$type_icon  .= "<div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                        <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#36c684' />
                                        <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#95e5bd' />
                                        <path d='M42,31H30a3.0033,3.0033,0,0,0-3,3V45a3.0033,3.0033,0,0,0,3,3H42a3.0033,3.0033,0,0,0,3-3V34A3.0033,3.0033,0,0,0,42,31ZM29,38h6v3H29Zm8,0h6v3H37Zm6-4v2H37V33h5A1.001,1.001,0,0,1,43,34ZM30,33h5v3H29V34A1.001,1.001,0,0,1,30,33ZM29,45V43h6v3H30A1.001,1.001,0,0,1,29,45Zm13,1H37V43h6v2A1.001,1.001,0,0,1,42,46Z' style='fill:#fff' /></svg>
                                </span>
                            </div>";
        }else{
        	$type_icon   .= "<div class='nk-file-icon'>
                                <span class='nk-file-icon-type'>
                                    <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 72 72'>
                                        <path d='M50,61H22a6,6,0,0,1-6-6V22l9-11H50a6,6,0,0,1,6,6V55A6,6,0,0,1,50,61Z' style='fill:#36c684' />
                                        <path d='M25,20.556A1.444,1.444,0,0,1,23.556,22H16l9-11h0Z' style='fill:#95e5bd' />
                                        <path d='M42,31H30a3.0033,3.0033,0,0,0-3,3V45a3.0033,3.0033,0,0,0,3,3H42a3.0033,3.0033,0,0,0,3-3V34A3.0033,3.0033,0,0,0,42,31ZM29,38h6v3H29Zm8,0h6v3H37Zm6-4v2H37V33h5A1.001,1.001,0,0,1,43,34ZM30,33h5v3H29V34A1.001,1.001,0,0,1,30,33ZM29,45V43h6v3H30A1.001,1.001,0,0,1,29,45Zm13,1H37V43h6v2A1.001,1.001,0,0,1,42,46Z' style='fill:#fff' /></svg>
                                </span>
                            </div>";
        }

        return $type_icon;
	}

	function adhocRequestsReport($from="",$to="",$cus_id="",$status="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE	DATE_FORMAT(A.created_at,'%Y-%m-%d') BETWEEN '$from' AND '$to' AND A.tenant_id='".$tenant_id."'   " ;
		if ($cus_id!="") {
			$q .=" AND A.customer_id='$cus_id'  ";
		}
		if ($status!="") {
			$q .=" AND A.request_status='$status' ";
		}
			$q .=" ORDER BY A.created_at DESC ";
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-light'>Close</span>" : "<span class='badge badge-success'>Open</span>"  );
	    		$readcolor = (($list['request_status']=='1') ? "is-read" : "is-unread"  );
	    		$layout .= "
	    			<tr >
	    				<td>".$i."</td>
                        <td ><a href='".COREPATH."adhoc/details/".$list['id']."'>".$list['ticket_uid']."</a></td>
                        <td><a href='javascript:void();'> ".ucwords($list['name'])."</a></td>
                        <td '>
                            <a href='".COREPATH."adhoc/details/".$list['id']."'><span class='title'>".ucwords($list['subject'])."</span></a>
                        </td>
                        <td '>
                            ".ucwords($list['requestname'])."
                        </td>
                        <td >
                           ".date("d M Y",strtotime($list['created_at']))."
                        </td>
                        <td >
                            ".$request_status."
                        </td>
                        <td >
                            <a href='".COREPATH."adhoc/details/".$list['id']."' class='btn btn-icon btn-trigger'>
                                <em class='icon ni ni-chevron-right'></em>
                            </a>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function leadReport($from="",$to="",$emp_id="",$lead_status="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.token,L.import_token,L.uid,L.auth_type,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,
		L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.closed_status,L.status,L.added_by,L.created_at,E.name,T.lead_type  FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id)  WHERE L.lead_date BETWEEN '$from' AND '$to' AND L.tenant_id='".$tenant_id."'  " ;
		if ($emp_id!="") {
			$q .=" AND L.employee_id='$emp_id'  ";
		}
		if ($lead_status!="") {
			$q .=" AND L.lead_status_id='$lead_status' ";
		}
			$q .=" ORDER BY L.lead_date DESC ";
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$assign_btn = (($list['assign_status']=="0")? "
                    <div class='tb-odr-btns d-none d-md-inline'>
                    <a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
                    </div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-info btn-sm'> Reassign</a></div>" );
                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a>" : "-" );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['lead_date']))."</td>
                        <td><em class='icon ni ni-user'></em> ".ucwords($list['fname'].' '.$list['lname'])." <br><em class='icon ni ni-mobile'></em> ".($list['mobile'])." <br><em class='icon ni ni-mail'></em>  ".($list['email'])."</td>
                        <td>".ucwords($list['leadsource'])."</td>
                        <td>".ucwords($list['lead_type'])."</td>
                        <td>".ucwords($assigned_to)."</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            $assign_btn 
                            <br>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

	/*--------------------------------------------- 
				Manage KYC
	----------------------------------------------*/

	function manageKyc()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type,K.status,K.approval_status,K.created_at,C.name,C.uid,C.kyc_status,C.approved_on,C.approved_by,E.name as emp_name  FROM ".KYC_LIST." K LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=K.customer_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=C.approved_by) WHERE 1 AND K.tenant_id='".$tenant_id."' GROUP BY K.customer_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
                $kyc_status1 = (($list['kyc_status']=='0') ? '<span class="badge badge-warning">Not submitted</span>' : $list['kyc_status']  );
                $kyc_status2  = (($list['kyc_status']=='1') ? '<span class="badge badge-success">Approved</span>' : $kyc_status1 );
                $kyc_status3  = (($list['kyc_status']=='2') ? '<span class="badge badge-info">Pending Approval</span>' : $kyc_status2 );
                $kyc_status  = (($list['kyc_status']=='3') ? '<span class="badge badge-danger">Rejected </span>' : $kyc_status3 );

                
                $employee_name = (($list['emp_name']=='') ? '-' : "<a href='".COREPATH."employee/details/".$list['approved_by']."' target='_blank'>".ucwords($list['emp_name'])."</a>" );

                if ($list['kyc_status']=='1') {
                	$ap_btn = "";
                }else{
                	$ap_btn = "<li class=''>
                                    <a href='javascript:void();'  class='btn btn-trigger btn-icon approverejectkyc' data-toggle='tooltip' data-placement='top' title='Approved' data-value='1' data-option='".$list['customer_id']."'>
                                        <em class='icon ni ni-check-fill-c'></em>
                                    </a>
                                </li>
                                <li class=''>
                                    <a href='javascript:void();' class='btn btn-trigger btn-icon approverejectkyc'  data-toggle='tooltip' data-placement='top' title='Rejected' data-value='3' data-option='".$list['customer_id']."'>
                                        <em class='icon ni ni-cross-fill-c'></em>
                                    </a>
                                </li>
                                ";
                }

	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank'>".ucwords($list['name'])."</a>
                        	<p>".($list['uid'])."</p>
                        </td>
                        <td>".date("d M, Y h:i a",strtotime($list['created_at']))."</td>
                        <td>".$kyc_status."</td>
                        <td>".$employee_name."</td>
                        <td>
                        	<ul class='nk-tb-actions gx-1'>
                                <li class=''>
                                    <a href='".COREPATH."customer/kycdetails/".$list['customer_id']."' class='btn btn-trigger btn-icon' data-toggle='tooltip' data-placement='top' title='Quick View'>
                                        <em class='icon ni ni-eye-fill'></em>
                                    </a>
                                </li>
                                ".$ap_btn."
                                
                                
                            </ul>
                        </td>
                        
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getkycList($customer_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT  K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type,K.status,K.approval_status,K.created_at,K.updated_at,T.kyc_type  FROM ".KYC_LIST." K LEFT JOIN ".KYC_TYPE." T ON (T.id=K.kyctype_id) WHERE K.customer_id='$customer_id' AND K.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$image 		 =  (($list['file_type']=='jpeg' || $list['file_type']=='jpg' || $list['file_type']=='png' || $list['file_type']=='JPG' || $list['file_type']=='JPEG' || $list['file_type']=='PNG' ) ? FRONTSRCIMG.$list['image_name'] : IMGPATH."file.png" );
	    		$layout .= "
    				<li class='data-item'>
                        <div class='data-col'>
                            <div class='data-label'>".ucwords($list['kyc_type'])."</div>
                            <div class='data-value'><a href='".FRONTSRCIMG."".$list['image_name']."' target='_blank'><img src='".$image."' alt='Image' class='mfp-fade img-responsive img-thumbnail' style='width: 70px;'></a></div>
                        </div>
                    </li>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Invoice
	----------------------------------------------*/

	function manageInvoice()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT I.id,I.inv_uid,I.inv_number,I.inv_date,I.type,I.raised_to,I.documents,I.document_type,I.remarks,I.status,I.added_by,I.created_at,I.updated_at,C.name,I.deleted_status   FROM ".INVOICE_TBL." I LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=I.customer_id) WHERE I.deleted_status='0' AND I.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deleteInvoice' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."invoice/details/".$list['id']."' target='_blank' >".($list['inv_number'])."</a></td>
                        <td>".date("d/m/Y",strtotime($list['inv_date']))."</a></td>
                        <td><a href='".COREPATH."customer/details/".$list['id']."' target='_blank' > ".ucwords($list['name'])." </td>
                        <td> ".ucwords($list['type'])." </td>
                        <td>".ucwords($list['raised_to'])."</td>
                        <td>
                            <a href='".COREPATH."invoice/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            <a href='".COREPATH."invoice/details/".$list['id']."' target='_blank'  class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            $deletedbtn
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*-----------------------------------------
					 Dashboard
	------------------------------------------*/

  	function dashboardStats()
	{	
		$today = date("Y-m-d");
		$id = $_SESSION["crm_admin_id"];
		$tenant_id  = @$_SESSION["tenant_id"];
		$query = "SELECT id,
				(SELECT COUNT(id) as tots FROM ".PROPERTY_TBL." WHERE 1 AND tenant_id='".$tenant_id."') as property_count,
				(SELECT COUNT(id) as tots FROM ".CUSTOMER_TBL." WHERE 1 AND tenant_id='".$tenant_id."') as customer_count,
				(SELECT COUNT(id) as tots FROM ".EMPLOYEE." WHERE 1) as employee_count				
				FROM ".EMPLOYEE." WHERE id ='".$id."' ";
		$exe = $this->selectQuery($query);	
		$list = mysqli_fetch_assoc($exe);
		return $list;
	}

	function dashboardCustomer()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,name,uid,gender,mobile,email FROM  ".CUSTOMER_TBL." WHERE 1 AND tenant_id='".$tenant_id."' ORDER BY id DESC LIMIT 5 " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$words = explode(" ", $list['name']);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
            	}

	    		$layout .= "
	    			 <div class='card-inner card-inner-md'>
                        <div class='user-card'>
                            <div class='user-avatar bg-primary-dim'>
                                <span>".$acronym."</span>
                            </div>
                            <div class='user-info'>
                                <span class='lead-text'>".ucwords($list['name'])."</span>
                                <span class='sub-text'>".$list['email']."</span>
                            </div>
                            <div class='user-action'>
                                <div class='drodown'>
                                    <a href='javascript:void();' class='dropdown-toggle btn btn-icon btn-trigger mr-n1' data-toggle='dropdown' aria-expanded='false'><em class='icon ni ni-more-h'></em></a>
                                    <div class='dropdown-menu dropdown-menu-right'>
                                        <ul class='link-list-opt no-bdr'>
                                            <li><a href='javascript:void();'><em class='icon ni ni-eye'></em><span>View</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}


  	function dashboardAdhocRequest()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.ticket_uid,A.property_id,A.customer_id,A.employee_id,A.request_type,A.requesttype_id,A.subject,A.remarks,A.request_status,A.status,A.added_by,A.created_at,A.updated_at,R.request_type AS requestname,C.name  FROM ".ADOC_REQUEST." A LEFT JOIN ".REQUEST_TYPE_TBL." R ON (R.id=A.requesttype_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=A.customer_id) WHERE 1 AND A.tenant_id='".$tenant_id."'  ORDER BY A.created_at DESC LIMIT 5 " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$request_status = (($list['request_status']=='1') ? "<span class='badge badge-dot badge-dot-xs badge-success ml-1'>Close</span>" : "<span class='badge badge-dot badge-dot-xs badge-warning ml-1'>Open</span>"  );
				$words = explode(" ", $list['name']);
                $acronym = "";
                foreach ($words as $w) {
                $acronym .= $w[0];
            	}

            	$ticket_uid = $list['ticket_uid'];
	    		$layout .= "

                    <li class='nk-support-item'>
                        <div class='user-avatar bg-purple-dim'>
                            <span>".$acronym."</span>
                        </div>
                        <div class='nk-support-content'>
                            
                            <div class='title'>
                                <span><a href='".COREPATH."adhoc/details/".$list['id']."'>".$ticket_uid."</a>  </span>
                            </div>
                            <div class='title'>
                                <span>".ucwords($list['name'])."  </span>".$request_status."
                            </div>
                            <p>".ucwords($list['subject'])."...</p>
                            <span class='time'>".date("d M Y h:i a",strtotime($list['created_at']))."</span>
                        </div>
                    </li>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function dashboardInvoice()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT I.id,I.inv_uid,I.inv_number,I.inv_date,I.type,I.raised_to,I.documents,I.document_type,I.remarks,I.status,I.added_by,I.created_at,I.updated_at,C.name   FROM ".INVOICE_TBL." I LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=I.customer_id) WHERE 1 AND I.tenant_id='".$tenant_id."' ORDER BY I.id DESC LIMIT 5 " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><a href='javascript:void();'>".date("d/m/Y",strtotime($list['inv_date']))."</a></td>
                        <td>".($list['inv_uid'])."</td>
                        <td><a href='javascript:void();'> ".ucwords($list['name'])." </a></td>
                        <td> ".ucwords($list['type'])." </td>
                        <td>".ucwords($list['raised_to'])."</td>
                        <td>
                        	
                            <a href='javascript:void();' class='btn btn-dim btn-sm btn-primary'>View</a>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function dashboardKyc()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type,K.status,K.approval_status,K.created_at,C.name,C.uid,C.kyc_status,C.approved_on,C.approved_by,E.name as emp_name  FROM ".KYC_LIST." K LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=K.customer_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=C.approved_by) WHERE 1 AND K.tenant_id='".$tenant_id."' GROUP BY K.customer_id  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
                $kyc_status1 = (($list['kyc_status']=='0') ? '<span class="badge badge-warning">Not submitted</span>' : $list['kyc_status']  );
                $kyc_status2  = (($list['kyc_status']=='1') ? '<span class="badge badge-success">Approved</span>' : $kyc_status1 );
                $kyc_status3  = (($list['kyc_status']=='2') ? '<span class="badge badge-info">Pending Approval</span>' : $kyc_status2 );
                $kyc_status  = (($list['kyc_status']=='3') ? '<span class="badge badge-danger">Rejected </span>' : $kyc_status3 );

                
                $employee_name = (($list['emp_name']=='') ? '-' : ucwords($list['emp_name']) );

                if ($list['kyc_status']=='1') {
                	$ap_btn = "";
                }else{
                	$ap_btn = "<li class=''>
                                    <a href='javascript:void();'  class='btn btn-trigger btn-icon approverejectkyc' data-toggle='tooltip' data-placement='top' title='Approved' data-value='1' data-option='".$list['customer_id']."'>
                                        <em class='icon ni ni-check-fill-c'></em>
                                    </a>
                                </li>
                                <li class=''>
                                    <a href='javascript:void();' class='btn btn-trigger btn-icon approverejectkyc'  data-toggle='tooltip' data-placement='top' title='Rejected' data-value='3' data-option='".$list['customer_id']."'>
                                        <em class='icon ni ni-cross-fill-c'></em>
                                    </a>
                                </li>
                                ";
                }

	    		$layout .= "
	    			
                    <li class='timeline-item'>
                        <div class='timeline-status bg-primary is-outline'></div>
                        <div class='timeline-date'>".date("d M",strtotime($list['created_at']))." <em class='icon ni ni-alarm-alt'></em></div>
                        <div class='timeline-data'>
                            <h6 class='timeline-title'>".ucwords($list['name'])."</h6>
                            <div class='timeline-des'>
                                <p>".$kyc_status."</p>
                                <span class='time'>".date("h:i a",strtotime($list['created_at']))."</span>
                                
	                               
                            </div>
                        </div>
                    </li>

	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				COMPANY SETTINGS
	----------------------------------------------*/

	function manageCompanySettings()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT id,phone ,email ,website_address,address ,status ,created_at, updated_at  FROM ".COMPANY_SETTINGS." WHERE 1 AND tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td><i class='la la-phone'></i> ".$list['phone']." </td>
                        <td><i class='la la-envelope'></i> ".$list['email']."</td>
                        <td> ".$list['website_address']."</td>
                        <td> ".$list['address']."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox CompanySettingsInfoStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."companysettings/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Activity Type
	----------------------------------------------*/

	function manageActivityType()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT A.id,A.token,A.activity_type,A.status,A.added_by,A.created_at,A.added_by,E.name FROM ".ACTIVITY_TYPE." A LEFT JOIN ".EMPLOYEE." E ON (E.id=A.added_by) WHERE 1 AND A.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".ucwords($list['activity_type'])."</td>
                        <td>".ucwords($list['name'])."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox activityTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editActivityType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Lead Status Message
	----------------------------------------------*/

	function manageLeadType()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.uid,L.token,L.lead_type,L.default_settings,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".LEAD_TYPE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1 AND L.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $default_settings = (($list['default_settings']==1) ? '(default Lead)' : '' );
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".$list['id']."</td>
                        <td>".ucwords($list['lead_type'])." $default_settings</td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox leadTypeStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editLeadType' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                           
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Lead Source
	----------------------------------------------*/

	function manageLeadSource()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.token,L.lead_source,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".LEAD_SOURCE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1 AND L.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".($list['id'])." </td>
                        <td>".ucwords($list['lead_source'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox leadSourceStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editLeadSource' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Manage Lead
	----------------------------------------------*/

	function manageLead()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.added_by,L.created_at,L.updated_at ,E.name,T.lead_type,L.closed_status,L.moved_status,L.deleted_status,L.customer_id,C.name as cus_name,LS.lead_source FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.customer_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource) WHERE L.deleted_status='0' AND L.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");


                $assign_btn = (($list['assign_status']=="0")? "
                	<div class='tb-odr-btns d-none d-md-inline'>
                		<a href='#' class='btn btn-sm btn-info assignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Assign</a>
                	</div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Reassign</a></div>" );

                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );
                $completedbtn = (($list['closed_status']==1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='#' class='btn btn-sm btn-info moveLeadToCustomer' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> Move Lead</a>
                            </div>" : "<a href='#'  class='btn btn-sm btn-primary updateCompletedStatus' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> <span>Mark as completed</span></a>" );
                $movebtn = (($list['moved_status']==1) ? "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' ><em class='icon ni ni-user'></em> ".ucwords($list['cus_name'])."</a> " : $completedbtn );

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );

	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''>".($list['uid'])."</a></td>
                        <td><em class='icon ni ni-user'></em><a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class=''> ".ucwords($list['fname'].' '.$list['lname'])."</a> <br><em class='icon ni ni-mobile'></em> ".($list['mobile'])." <br><em class='icon ni ni-mail'></em>  ".($list['email'])."</td>
                        <td>".ucwords($list['lead_source'])."</td>

                        <td>".ucwords($list['lead_type'])." <br>
                        	<a href='javascript:void();'  class='btn btn-success btn-sm leadStatusModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-check'></em> <span>Update Status</span></a>
                        </td>

                        <td>".ucwords($assigned_to)." $assign_btn</td>
                        <td>".$movebtn."</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                
                                <a href='#' class='btn btn-sm btn-warning addActivityModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em> Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <br>
                            
                            ".$deletedbtn."
                        </td>
                    </tr>
	    		";
	    		/*
				<a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
				<a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-blue btn-sm'> Reassign</a>
	    		<a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a> */
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageFilterLead($leadsource="",$movecustomer="",$assignedstatus="",$assignedto="",$leadstatus="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$result = array();
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.customer_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.employee_id,L.added_by,L.created_at,L.updated_at ,E.name,T.lead_type,L.closed_status,L.moved_status,L.deleted_status,L.customer_id,C.name as cus_name FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=L.customer_id) WHERE 1 AND L.tenant_id='".$tenant_id."' " ;
		if($leadsource!=""){
			$q .=" AND L.leadsource='$leadsource' ";
		}
		if($movecustomer!=""){
			$q .=" AND  L.moved_status='$movecustomer' ";
		}
		if($assignedstatus!=""){
			$q .=" AND  L.assign_status='$assignedstatus' ";
		}
		if($assignedto!=""){
			$q .=" AND  L.employee_id='$assignedto' ";
		}
		if($leadstatus!=""){
			$q .=" AND  L.lead_status_id='$leadstatus' ";
		}
		$q .=" AND L.deleted_status='0' ";
	    $query = $this->selectQuery($q);	
	    $count = mysqli_num_rows($query);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");


                $assign_btn = (($list['assign_status']=="0")? "
                	<div class='tb-odr-btns d-none d-md-inline'>
                		<a href='#' class='btn btn-sm btn-info assignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Assign</a>
                	</div>" : "<div class='tb-odr-btns d-none d-md-inline'><a href='#' class='btn btn-sm btn-blue reassignLeadModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> Reassign</a></div>" );

                $assign_info = $this->leadAssignTo($list['id']);
                $assigned_to = (($assign_info!="") ? "<a href='".COREPATH."employee/details/".$assign_info['created_to']."' target='_blank' >".ucwords($assign_info['name'])."</a> <br> " : "" );
                $completedbtn = (($list['closed_status']==1) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='#' class='btn btn-sm btn-info moveLeadToCustomer' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> Move Lead</a>
                            </div>" : "<a href='#'  class='btn btn-sm btn-primary updateCompletedStatus' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-check'></em> <span>Mark as completed</span></a>" );
                $movebtn = (($list['moved_status']==1) ? "<a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank' ><em class='icon ni ni-user'></em> ".ucwords($list['cus_name'])."</a> " : $completedbtn );

                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deletLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );

	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['lead_date']))."</td>
                        <td><em class='icon ni ni-user'></em> ".ucwords($list['fname'].' '.$list['lname'])." <br><em class='icon ni ni-mobile'></em> ".($list['mobile'])." <br><em class='icon ni ni-mail'></em>  ".($list['email'])."</td>
                        <td>".ucwords($list['leadsource'])."</td>
                        <td>".ucwords($list['lead_type'])."</td>
                        <td>".ucwords($assigned_to)." $assign_btn</td>
                        <td>".$movebtn."</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                
                                <a href='#' class='btn btn-sm btn-warning addActivityModel' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'><em class='icon ni ni-plus'></em> Activity</a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."lead/details/".$list['id']."' target='_blank' class='btn btn-sm btn-primary'><em class='icon ni ni-eye'></em></a>
                            </div>
                            <br>
                            
                            ".$deletedbtn."
                        </td>
                    </tr>
	    		";
	    		/*
				<a href='".COREPATH."lead/assignlead/".$list['id']."' class='btn btn-info btn-sm'> Assign</a>
				<a href='".COREPATH."lead/reassign/".$list['id']."' class='btn btn-blue btn-sm'> Reassign</a>
	    		<a href='".COREPATH."lead/addactivity/".$list['id']."' class='btn btn-sm btn-warning'><em class='icon ni ni-plus'></em> Add Activity</a> */
	    		$i++;
	    	}
	    }
	    $result['layout'] = $layout;
	    $result['count']  = $count;
	    return $result;
  	}

  	function leadAssignTo($lead_id)
  	{
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$q=" SELECT A.id,A.type,A.created_by,A.created_to,A.ref_id,A.status,A.updated_at,E.name  FROM ".ASSIGN_LEAD." A LEFT JOIN ".EMPLOYEE." E ON (E.id=A.created_to) WHERE A.ref_id='$lead_id' AND A.status='1' AND A.tenant_id='".$tenant_id."' ";
  		$exe = $this->selectQuery($q);
  		$list = mysqli_fetch_array($exe);
  		return $list;
  	}

  	function getAssignedEmployeeInfo($lead_id)
  	{
  		$tenant_id  = @$_SESSION["tenant_id"];
  		$q = "SELECT C.id,C.type,C.created_by,C.created_to,C.priority,C.ref_id,C.remarks,C.status,E.id as emp_id,E.name,
  			(SELECT COUNT(A.id) FROM ".ASSIGN_LEAD." A LEFT JOIN ".LEAD_TBL." CA ON (CA.id=A.ref_id) WHERE A.created_to=emp_id AND A.status='1' AND CA.closed_status='0') as assign_count
  			FROM ".ASSIGN_LEAD." C  LEFT JOIN ".EMPLOYEE." E ON (E.id=C.created_to) WHERE C.ref_id='$lead_id' AND C.status='1' AND A.tenant_id='".$tenant_id."'  " ;
  		$exe = $this->selectQuery($q);
  		$list = mysqli_fetch_array($exe);
  		return $list;
  	}


  	// Manage Activity Logs
  	
	function manageActivityLogs($lead_id)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT  L.id,L.date,L.lead_id,L.employee_id,L.activity_id,L.lead_name,L.mobile,L.email,L.remarks,L.status,L.created_at,L.updated_at,E.name,A.activity_type FROM ".LEAD_ACTIVITY." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.employee_id) LEFT JOIN ".ACTIVITY_TYPE." A ON (A.id=L.activity_id) WHERE L.lead_id='$lead_id' AND L.tenant_id='".$tenant_id."' ORDER BY L.id ASC " ;
	    $query = $this->selectQuery($q);
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);               
                
	    		$layout .= "
	    					<div class='cd-timeline-block'>
		                        <div class='cd-timeline-img cd-primary'>
		                            <em class='icon ni ni-check'></em>
		                        </div>
		                        <div class='cd-timeline-content editactivity_warp'>
		                            <h3>".ucwords($list['activity_type'])."</h3>
		                          <p> <span>Created By : </span><a href='".COREPATH."employee/details/".$list['employee_id']."' class=''  >".ucwords($list['name'])."</a></p>

		                          <p> <span>Lead Name : </span>".ucwords($list['lead_name'])."</p>
		                          <p> <span>Mobile : </span>".ucwords($list['mobile'])."</p>
		                          <p> <span>Email : </span>".ucwords($list['email'])."</p>

		                          <p><em class='icon ni ni-calendar'></em> ".date("d/m/Y - g:i A",strtotime($list['updated_at']))."</p> 
		                          <p>".$list['remarks']."</p>
		                          <div class='editwarp'>
		                          		<a href='javascript:void();' class='editleadactivity' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
		                          </div>
		                        </div>
		                    </div>
	    				";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function manageNewActivityLogs($lead_id)
	{
		$layout = "";
		$tenant_id  = @$_SESSION["tenant_id"];
	    $q = "SELECT  L.id,L.date,L.lead_id,L.employee_id,L.activity_id,L.remarks,L.status,L.created_at,L.updated_at,E.name,A.activity_type FROM ".LEAD_ACTIVITY." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.employee_id) LEFT JOIN ".ACTIVITY_TYPE." A ON (A.id=L.activity_id) WHERE L.lead_id='$lead_id' AND L.tenant_id='".$tenant_id."' ORDER BY L.id DESC " ;
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
      		
      		
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td><em class='icon ni ni-calendar'></em> ".date("d/m/Y", strtotime($list['updated_at']))." <br><em class='icon ni ni-clock'></em> ".date("g:i A", strtotime($list['updated_at']))."</td>
	                <td>".ucwords($list['activity_type'])."</td>
	               
	                <td>".$list['remarks']."</td>
	                <td><a href='".COREPATH."employee/details/".$list['employee_id']."' class=''  >".ucwords($list['name'])."</a></td>
	                <td>
	                  <a href='javascript:void();' class='editleadactivity btn btn-info' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>

	                  
	                </td>               
	              </tr>"; 
	              /*<a href='javascript:;' name='".$list['import_token']."' class='deleteImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>*/          
	        $i++;     
	      }
	    }
	    return $layout;
	}

	/*--------------------------------------------- 
				Customer Profile
	----------------------------------------------*/

	function manageCustomerProfile()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.token,L.customer_profile,L.status,L.added_by,L.created_at,L.added_by,E.name FROM ".CUSTOMER_PROFILE." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) WHERE 1 AND L.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr>
                        <td>".$i."</td>
                        <td>".($list['id'])." </td>
                        <td>".ucwords($list['customer_profile'])." </td>
                        <td>".ucwords($list['name'])." </td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox customerProfileStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-success editCustomerProfile' data-value='".$i."' data-option='".$list['id']."'><em class='icon ni ni-edit'></em></a>
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
				Import Alumni Data
	----------------------------------------------*/

	// Data sheets import history

	function getDataSheetList()
	{
		$layout = "";
		$tenant_id  = @$_SESSION["tenant_id"];
	    $q = "SELECT * FROM ".DATASHEET_TBL." WHERE 1 AND tenant_id='".$tenant_id."' ORDER BY id ASC ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
      		$status = (($list['status']==1) ? "<label class='label label-success'>Migrated</label>" : "<label class='label label-danger'>Pending Approval</label>");
      		$display  = (($list['status']==1) ? "no_display" : " ");
      		$updated_at = (($list['status']==1) ? date("d M Y - H:i:s", strtotime($list['updated_at'])) : "NA "); 
      		$count_import = $this -> check_query(TEMPEXCEL_TBL,"id"," upload_token	='".$list['import_token']."' ");
      		$count_approve = $this -> check_query(LEAD_TBL,"id"," import_token	='".$list['import_token']."' ");
      		$duplicate = (($list['status']==1) ? ($count_import - $count_approve) : "NA ");
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".date("d M Y - H:i:s", strtotime($list['created_at']))."</td>
	                <td>".$count_import."</td>
	                <td>".$count_approve."</td>
	                <td>".$duplicate."</td>
	                <td><a  class='btn btn-primary' href='".UPLOADS."datasheet/".$list['uploaded_sheet']."'><span class='icon-file-excel'></span>  Download</a></td> 
	                <td>$status </td>
	                <td>".$updated_at."</td>
	                <td>
	                  <a href='".COREPATH."importdata/approve/".$list['import_token']."' class='btn btn-info $display'  ><span class='icon-checkmark'></span> Approve</a>
	                   <a href='javascript:;' name='".$list['import_token']."' class='deleteImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	// List of Data imported

	function dataImported($token)
	{
		$layout = "";
		$tenant_id  = @$_SESSION["tenant_id"];
	    $q = "SELECT * FROM ".TEMPEXCEL_TBL." WHERE upload_token='$token' AND tenant_id='".$tenant_id."' ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".$list['fname']." ".$list['lname']." </td>
	                <td><strong>Lead Source:</strong> ".ucwords($list['leadsource'])."<br>
	                	<strong>Flat Type:</strong> ".ucwords($list['flat_type'])." <br>
	                	<strong>Site Visited:</strong> ".ucwords($list['site_visited'])." <br>
	                	<strong>Lead Profile:</strong> ".ucwords($list['profile'])."
	                </td>
	                <td>".ucwords($list['gender'])."</td>
	                <td>".$list['primary_mobile']." </td>
	                <td>".$list['primary_email']."</td>
	                <td>".$list['address'].", ".$list['city'].", ".$list['state']." - ".$list['pincode']."</td>
	                <td>
	                  <a href='javascript:;' name='".$list['id']."' class='deleteSingleRowImportData btn btn-danger' ><em class='icon ni ni-trash'></em></a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}


	/*--------------------------------------------- 
				Customer Import Alumni Data
	----------------------------------------------*/

	// Data sheets import history

	function getCustomerDataSheetList()
	{
		$layout = "";
		$tenant_id  = @$_SESSION["tenant_id"];
	    $q = "SELECT * FROM ".CUSTOMERDATASHEET_TBL." WHERE 1 AND tenant_id='".$tenant_id."' ORDER BY id ASC ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
      		$status = (($list['status']==1) ? "<label class='label label-success'>Migrated</label>" : "<label class='label label-danger'>Pending Approval</label>");
      		$display  = (($list['status']==1) ? "no_display" : " ");
      		$updated_at = (($list['status']==1) ? date("d M Y - H:i:s", strtotime($list['updated_at'])) : "NA "); 
      		$count_import = $this -> check_query(TEMPCUSTOMEREXCEL_TBL,"id"," upload_token	='".$list['import_token']."' ");
      		$count_approve = $this -> check_query(CUSTOMER_TBL,"id"," import_token	='".$list['import_token']."' ");
      		$duplicate = (($list['status']==1) ? ($count_import - $count_approve) : "NA ");
	        $layout .= "<tr>
	                <td>".$i."</td>
	                <td>".date("d M Y - H:i:s", strtotime($list['created_at']))."</td>
	                <td>".$count_import."</td>
	                <td>".$count_approve."</td>
	                <td>".$duplicate."</td>
	                <td><a  class='btn btn-primary' href='".UPLOADS."datasheet/".$list['uploaded_sheet']."'><span class='icon-file-excel'></span>  Download</a></td> 
	                <td>$status </td>
	                <td>".$updated_at."</td>
	                <td>
	                  <a href='".COREPATH."importcustomerdata/approve/".$list['import_token']."' class='btn btn-info $display'  ><span class='icon-checkmark'></span> Approve</a>
	                   <a href='javascript:;' name='".$list['import_token']."' class='deleteCustomerImportDatass btn btn-danger' ><em class='icon ni ni-trash'></em> Delete Data</a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	// List of Data imported

	function dataCustomerImported($token)
	{
		$layout = "";
		$tenant_id  = @$_SESSION["tenant_id"];
	    $q = "SELECT * FROM ".TEMPCUSTOMEREXCEL_TBL." WHERE  upload_token='$token' AND tenant_id='".$tenant_id."' ";
	    $query = $this->selectQuery($q);
    	if(mysqli_num_rows($query) > 0){
      	$i=1;
      	while($list = mysqli_fetch_array($query)){
	        $layout .= "<tr>
	                <td width='1%'>".$i."</td>
	                <td width='10%'>".$list['name']."  </td>
	                <td width='20%'>".$list['addresss']."</td>
	                <td width='10%'>".$list['mobile']."</td>
	                <td width='10%'>".$list['email']." </td>
	                <td width='40%'>
	                  <a href='javascript:;' name='".$list['id']."' class='deleteCustomerSingleRowImportData btn btn-danger' ><em class='icon ni ni-trash'></em></a>
	                </td>               
	              </tr>";           
	        $i++;     
	      }
	    }
	    return $layout;
	}

	/*--------------------------------------------- 
				Trash Management
	----------------------------------------------*/

	// General Doc

	function trashGeneralDocuments()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT G.id,G.token,G.title,G.documents,G.status,G.created_at,G.deleted_status,G.deleted_by,G.deleted_on,G.restored_by,G.restored_on,G.restored_remarks,E.name FROM ".GENDRAL_DOCUMENTS." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.deleted_by) WHERE G.deleted_status='1' AND G.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td>".ucwords($list['title'])."</td>
                        <td><a href='".DOCUMENTS.$list['documents']."' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file'></em></a></td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                            <div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoGeneralDocList' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// Doc Checklist

  	function trashDocumentChecklist()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT F.id,F.token,F.doc_title,F.sort_order,F.status,F.publish_status,E.name,F.deleted_by FROM ".FLATVILLA_DOC_TYPE." F LEFT JOIN ".EMPLOYEE." E ON (E.id=F.deleted_by) WHERE F.deleted_status='1'  AND F.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");
                $publish_status = (($list['publish_status']==1) ? "<span class='badge badge-success'>Published</span>" : "<span class='badge badge-warning'>Not Publish</span>");
                
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td>".ucwords($list['doc_title'])."</td>
                        <td> ".$publish_status." </td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoDocCheckList' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashLeads()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT L.id,L.token,L.uid,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.status,L.added_by,L.created_at,L.updated_at ,E.name,L.deleted_by FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.deleted_by) WHERE L.deleted_status='1' AND L.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td><em class='icon ni ni-user'></em> ".ucwords($list['fname'].' '.$list['lname'])." <br><em class='icon ni ni-mobile'></em> ".($list['mobile'])." <br><em class='icon ni ni-mail'></em>  ".($list['email'])."</td>
                        <td>".ucwords($list['leadsource'])."</td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoLead' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashInvoice()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT I.id,I.inv_uid,I.inv_number,I.inv_date,I.type,I.raised_to,I.documents,I.document_type,I.remarks,I.status,I.added_by,I.created_at,I.updated_at,C.name,I.deleted_status,I.deleted_by,I.customer_id,E.name as empname   FROM ".INVOICE_TBL." I LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=I.customer_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=I.deleted_by) WHERE I.deleted_status='1' AND I.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['inv_date']))."</td>
                        <td>".($list['inv_number'])."</td>
                        <td><a href='".COREPATH."customer/details/".$list['customer_id']."' target='_blank'> ".ucwords($list['name'])." </a></td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['empname'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoInvoice' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashGallery()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT G.id,G.token,G.album_title,G.image,G.status,G.added_by,G.created_at,E.name,G.deleted_by FROM ".GALLERY_TBL." G LEFT JOIN ".EMPLOYEE." E ON (E.id=G.deleted_by) WHERE G.deleted_status='1' AND G.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td><img src='".SRCIMG.$list['image']."' class='img-thumbnail' width='80'></td>
                        <td>".ucwords($list['album_title'])."</td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoGallery' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashBrochure()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT B.id,B.token,B.title ,B.property_id ,B.description ,B.brochures ,B.status,P.title as pro_title,B.deleted_status,B.deleted_by,E.name  FROM ".PROPERTY_BROUCHERS." B LEFT JOIN ".PROPERTY_TBL." P ON (P.id=B.property_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=B.deleted_by) WHERE B.deleted_status='1' AND B.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td>".ucwords($list['title'])."</td>
                        <td><a href='".DOCUMENTS.$list['brochures']."' target='_blank' style='font-size: 30px;color: red;'><em class='icon ni ni-file-pdf'></em></a></td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoPropertyBrochure' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashNews()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT N.id,N.token,N.title,N.date,N.description,N.email_notification,N.post_to,N.customer_ids,N.image,N.show_popup,N.popup_till,N.deleted_status,N.deleted_by,E.name  FROM ".NEWS_TBL." N LEFT JOIN ".EMPLOYEE." E ON (E.id=N.deleted_by) WHERE N.deleted_status='1' AND N.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$image  = (($list['image']!="") ? SRCIMG.$list['image']  : IMGPATH.'file_upload_icon.jpg' );
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['date']))."</td>
                        <td>".ucwords($list['title'])."</td>
                        <td><img src='".$image."' class='img-thumbnail' width='80'></td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoNews' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function trashProperty()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT P.id,P.customer_id,P.title,P.type,P.projectsize,P.projectarea,P.cost,P.block_id,P.floor_id,P.status,P.room_id,B.block,F.floor ,FL.flat_variant,P.created_at,P.house_facing,P.assign_status,C.name,P.deleted_status,P.deleted_status,P.deleted_by  FROM ".PROPERTY_TBL." P LEFT JOIN ".BLOCK_TBL." B ON (B.id=P.block_id) LEFT JOIN ".FLOOR_TBL." F ON (F.id=P.floor_id) LEFT JOIN ".FLATTYPE_MASTER." FL ON (FL.id=P.room_id) LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=P.customer_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=P.deleted_by)  WHERE  P.deleted_status='1' AND P.tenant_id='".$tenant_id."'  " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td >".$i."</td>
                        <td>".ucwords($list['title'])."</td>
                        <td>".ucwords($list['house_facing'])."</td>
                        <td>".ucwords($list['block'])."</td>
                        <td>".ucwords($list['floor'])."</td>
                        <td>
		                   <a href='".COREPATH."employee/details/".$list['deleted_by']."' target='_blank' >".ucwords($list['name'])."</a>
		    			</td>
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-info btn-sm undoProperty' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-repeat'></span> Undo</a>	
                            </div>
                        </td>
                    </tr>
	    		";
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	/*--------------------------------------------- 
			Manage News
	----------------------------------------------*/ 

	function manageNews()
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT N.id,N.token,N.title,N.date,N.description,N.email_notification,N.post_to,N.customer_ids,N.image,N.show_popup,N.popup_till,N.status,N.added_by,N.deleted_status,N.send_mail_status,(SELECT (CHAR_LENGTH(customer_ids) - CHAR_LENGTH(REPLACE(customer_ids, ',', '')) + 1) as total FROM ".NEWS_TBL." WHERE id=N.id ) as count
		  FROM ".NEWS_TBL." N  WHERE N.deleted_status ='0' AND N.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		 = $this->editPagePublish($details);
	    		$status 	 = (($list['status']==1) ? "Active" : "Inactive"); 
                $status_c 	 = (($list['status']==1) ? "checked" : " ");
                $status_class = (($list['status']==1) ? "text-success" : "text-danger");

                $customer_array 	= explode(",", $list['post_to']);
                $arr = array($list['post_to']);


                $post_to = $list['post_to']=='all' ? 'All customers' : $list['count'] ;
                $tilldate = (($list['show_popup']==1) ? '<br><i>Displayed in pop up banner till: '.date("d/m/Y",strtotime($list['popup_till'])).'</i>' : '' );
                $deletedbtn = (($list['deleted_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                            	<a href='javascript:void()' class='btn btn-danger btn-sm deleteNews' data-value='".$i."' data-type='".$list['id']."' data-option='".$list['id']."'> <span class='icon ni ni-trash'></span></a>	
                            </div>" : "" );
               	$sendmailbtn = (($list['send_mail_status']==0) ? "<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='javascript:void();' class='btn btn-sm btn-primary sendcustomer_mail' data-value='".$i."' data-type='".$list['id']."' data-option='".$this->encryptData($list['id'])."'><em class='icon ni ni-mail'></em> Send Email</a>
                            </div>" : "" );               
               	$image  = (($list['image']!="") ? SRCIMG.$list['image']  : IMGPATH.'file_upload_icon.jpg' );
	    		$layout .= "
	    			<tr id='attacjmentrow_".$list['id']."'>
                        <td>".$i."</td>
                        <td>".date("d/m/Y",strtotime($list['date']))."</td>
                        <td><img src='".$image."' class='img-thumbnail' width='80'></td>
                        <td>".ucwords($list['title'])." 
                        	".$tilldate."
                        </td>
                        <td>".$post_to."</td>
                        <td>
		                    <div class='custom-control custom-control-sm custom-checkbox newsStatus' data-option='".$this->encryptData($list['id'])."'>
                                <input type='checkbox' class='custom-control-input' id='check_$i' $status_c>
                                <label class='custom-control-label $status_class' for='check_$i'>$status</label>
                            </div>
		    			</td>
                       
                        <td>
                        	<div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."news/edit/".$list['id']."' class='btn btn-sm btn-success'><em class='icon ni ni-edit'></em></a>
                            </div>
                            <div class='tb-odr-btns d-none d-md-inline'>
                                <a href='".COREPATH."news/details/".$list['id']."' target='_blank' class='btn btn-sm btn-warning'><em class='icon ni ni-eye'></em></a>
                            </div>
                            $sendmailbtn
                            $deletedbtn 
                        </td>
                    </tr>
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	// News Customer List

	function getSelectCustomerList($id="")
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$q = "SELECT N.id,N.token,N.title,N.date,N.description,N.email_notification,N.post_to,N.customer_ids,N.image,N.show_popup,N.popup_till,N.status,N.added_by,N.deleted_status FROM ".NEWS_TBL." N  WHERE N.id='$id' AND N.tenant_id='".$tenant_id."' " ;
	    $query = $this->selectQuery($q);	
	    if(mysqli_num_rows($query) > 0){
	    	$i=1;
	    	while($details = mysqli_fetch_array($query)){
	    		$list 		= $this->editPagePublish($details);
	    		$customerlist = $this->getcustomer($list['customer_ids']);
	    		$layout .= "
	    			$customerlist
	    		";
	    		
	    		$i++;
	    	}
	    }
	    return $layout;
  	}

  	function getcustomer($customer_ids)
  	{
  		$layout = "";
  		$tenant_id  = @$_SESSION["tenant_id"];
		$dates_array = explode(",", $customer_ids);
		foreach ($dates_array as $value) {
			$q = "SELECT  id,uid,name,gender,dob,mobile,email FROM ".CUSTOMER_TBL." WHERE id='$value' AND tenant_id='".$tenant_id."' ";
			$query = $this->selectQuery($q);
		    if(mysqli_num_rows($query) > 0){
		    	$i=1;
		    	while($details = mysqli_fetch_array($query)){
				$list 			= $this->editPagePublish($details);
	            $layout .= "
		    		<div class='related_product_container'>
                        <div id='selected-assistants' class='chapter_locations_wrap' style=''>
                        <div id='selected-assistants1' class='selected_locations '><em class='icon ni ni-user remove_country' style='color: red;'></em> ".ucwords($list['name'])." (".$list['mobile'].")  - ".$list['email']."<input type='hidden' name='assistants_selected[]' value='".$list['id']."'></div></div>
                    </div>
		    		";
		    		$i++;
		    	}
		    }
		}
	    return $layout;
  	}
  	

	/*-----------Dont'delete---------*/

	}


	?>




