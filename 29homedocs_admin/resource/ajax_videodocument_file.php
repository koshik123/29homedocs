<?php
session_start();
require_once '../app/core/user_ajaxcontroller.php';
$route 		= new Ajaxcontroller();

// S3 Bucket
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

// Add Job Document 

$errors = array();
$msg = array();
$content 	= $_POST;
//$url = COREPATH."resource/";
if(!empty($_FILES["attachment"]["type"]))  
{
	$validextensions = array( "webm", "mp4", "ogv", "mpg","avi", "flv", "wmv", "mov");
    $temporary = explode(".", $_FILES["attachment"]["name"]); 
    $file_extension = end($temporary);
    /*if(($_FILES["attachment"]["type"] == "video/webm") || ($_FILES["attachment"]["type"] == "video/mp4") || ($_FILES["attachment"]["type"] == "video/ogv") || ($_FILES["attachment"]["type"] == "video/mpg") || ($_FILES["attachment"]["type"] == "video/avi") || ($_FILES["attachment"]["type"] == "video/flv") || ($_FILES["attachment"]["type"] == "video/wmv") || ($_FILES["attachment"]["type"] == "video/mov") )  && in_array($file_extension, $validextensions) ) 
	{*/
		if(($_FILES["attachment"]["size"] < 800000000000000000000000000000))
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");
	        if ($_FILES["attachment"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["attachment"]["error"] . "<br/><br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/videos/".$_FILES["attachment"]["name"])) {
	            	$err =  $_FILES["attachment"]["name"]." <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				}
				else 
				{					
					$sourcePath = $_FILES["attachment"]['tmp_name'];
					$document_name = "attachment_".$date."_".$rands.".".$file_extension;
					$targetPath = "uploads/videos/".$document_name;
					move_uploaded_file($sourcePath,$targetPath);

					// Upload to S3
                    S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'videos/'.$document_name, S3::ACL_PUBLIC_READ);
                    unlink($targetPath);

					$msg['pdf'] = $document_name;
				}				       
	        }
		}
		else{
			$errmsg =  "Video Size Exceed . Please upload proper Video with proper file size.<br/>";
        	$errors[] = $errmsg;
		}		    
    /*}   
	else 
	{
        $errmsg =  $_FILES["attachment"]["type"]." Invalid Video type. Please upload Video formats alone.";
        $errors[] = $errmsg;
    }*/
}else{
	//$errmsg =  "Please Attach the PDF.";
    //$errors[] = $errmsg;
    $msg['pdf'] = "";
}
if(count($errors)==0){
	//echo "1`".$route->encryptData($msg[0]);

	$success = "";
	foreach ($msg as $key =>  $value) {
		$success .= "`".$value;
	}
	//echo $route->addJournal($content,$msg);

	$page = @$_REQUEST["page"];
        switch($page){
            case 'uploadVideoDocuments':
                echo $route->uploadVideoDocuments($content,$msg);
            break;     
        }
}else{
	$op = "";
	foreach ($errors as $value) {
		$op .= $value;
	}
	echo "0`".$op;
}



?>