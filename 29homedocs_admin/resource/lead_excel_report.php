<?php 
session_start();
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();
//$client_id 	=  @$_REQUEST["client_id"];

$from	 	= @$_GET['from'];
$to	 		= @$_GET['to'];
$emp_id 	= @$_GET['employee_id'];
$lead_status= @$_GET['status'];

$today = date("d-m-y");


$title = "lead_report_".$today;



	//------------- Create Header ------------//

	// PHPExcel Instance
	$sheet = new PHPExcel();
	// Set document properties
	$sheet->getProperties()->setCreator("Post CRM")
	               ->setLastModifiedBy(COMPANY_NAME)
	               ->setTitle("Lead Report")
	               ->setSubject("Lead Report")
	               ->setDescription("Lead Report")
	               ->setKeywords("Post CRM")
	               ->setCategory("Excel Report");
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$sheet->setActiveSheetIndex(0);
	$activeSheet = $sheet->getActiveSheet();
	
	// Create Header
	$sheet->setActiveSheetIndex(0)
				->setCellValue('A1', 'S.No')
	           	->setCellValue('B1', 'Date')
	            ->setCellValue('C1', 'First Name')
	            ->setCellValue('D1', 'Last Name')
	            ->setCellValue('E1', 'Primary Phone')
	            ->setCellValue('F1', 'Primary Email')
	            ->setCellValue('G1', 'Assigned To')
	            ->setCellValue('H1', 'Lead Source')
	            ->setCellValue('I1', 'Lead Status')
	            ->setCellValue('J1', 'City')
	            ->setCellValue('K1', 'State')
	            ->setCellValue('L1', 'Description');

	$activeSheet->getColumnDimension('A')->setAutoSize(true);
	$activeSheet->getColumnDimension('B')->setAutoSize(true);
	$activeSheet->getColumnDimension('C')->setAutoSize(true);
	$activeSheet->getColumnDimension('D')->setAutoSize(true);
	$activeSheet->getColumnDimension('E')->setAutoSize(true);
	$activeSheet->getColumnDimension('F')->setAutoSize(true);
	$activeSheet->getColumnDimension('G')->setAutoSize(true);
	$activeSheet->getColumnDimension('H')->setAutoSize(true);
	$activeSheet->getColumnDimension('I')->setAutoSize(true);
	$activeSheet->getColumnDimension('J')->setAutoSize(true);
	$activeSheet->getColumnDimension('K')->setAutoSize(true);
	$activeSheet->getColumnDimension('L')->setAutoSize(true);

	//------------- Create Header Ends ------------//

	//------------- Create Report Body ------------//

  	$today = date("Y-m-d");
	$users = array();
	$project_id = @$_SESSION['crm_project_id'];
	$tenant_id  = @$_SESSION["tenant_id"];
	$q = "SELECT L.id,L.token,L.import_token,L.uid,L.auth_type,L.property_id,L.lead_date,L.fname,L.lname,L.leadsource,
		L.gender,L.mobile,L.email,L.secondary_mobile,L.secondary_email,L.address,L.city,L.state,L.pincode,L.description,L.lead_status_id,L.assign_status,L.closed_status,L.status,L.added_by,L.created_at,E.name,T.lead_type,LS.lead_source  FROM ".LEAD_TBL." L LEFT JOIN ".EMPLOYEE." E ON (E.id=L.added_by) LEFT JOIN ".LEAD_TYPE." T ON (T.id=L.lead_status_id) LEFT JOIN ".LEAD_SOURCE." LS ON (LS.id=L.leadsource)  WHERE L.lead_date BETWEEN '$from' AND '$to'   " ;
		if ($emp_id!="") {
			$q .=" AND L.employee_id='$emp_id'  ";
		}
		if ($lead_status!="") {
			$q .=" AND L.lead_status_id='$lead_status' ";
		}
	$q .=" AND L.project_id='".$project_id."' AND L.tenant_id='".$tenant_id."' ORDER BY L.lead_date DESC ";
	$exe = $route->selectQuery($q);	
    $row_count = mysqli_num_rows($exe);
    if(mysqli_num_rows($exe) > 0){
    	$i=1;
    	while($list = mysqli_fetch_array($exe)){
    			$assign_info = $route->leadAssignToReport($list['id']);
                $assigned_to = (($assign_info!="") ? ucwords($assign_info['name']) : "-" );	

    			$element 	=  array();
				$element[] 	=  $i;
				$element[] 	=  date("d/m/Y",strtotime($list['lead_date']));
				$element[] 	=  ucwords($list['fname']);
				$element[] 	=  ucwords($list['lname']);
				$element[] 	=  ($list['mobile']);
				$element[] 	=  ($list['email']);
				$element[] 	=  ucwords($assigned_to);
				$element[] 	=  ucwords($list['lead_source']);
				$element[] 	=  ucwords($list['lead_type']);
				$element[] 	=  ucwords($list['city']);
				$element[] 	=  ucwords($list['state']);
				$element[] 	=  ($list['description']);
				$users[] 	=  $element;
			  	$i++;
    	}
    }

    $row = 2;
	foreach ($users as $key => $value) {
		$col = 0;
	    foreach ($value as $key=> $value) {
	        //echo $row." $col -- ".$key."=".$value."<br/>";
	        $sheet->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $value);
	        $col++;
	   }
	   $row++;
	}

	//------------- Create Report Body Ends------------//

	// Rename worksheet
	
	$sheet_name = "Lead Report";
	
	$activeSheet ->setTitle($sheet_name);
	$filename = $title.".csv";
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.$filename);
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	//ob_end_clean();
	$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'CSV');
	$objWriter->save('php://output');
?>