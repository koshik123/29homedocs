<?php 

session_start();
/*require_once '../config/config.php';*/
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();


// Require AWS
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

$page = @$_REQUEST["page"];
$data = @$_REQUEST["element"];
$term = @$_REQUEST["term"];
$post = @$_POST["result"];

// Get Client IP Address

switch($page){

	/*=====================================
	        General Documents
	=======================================*/

	case 'viewGenDocInfo':
		$array 	=  $route -> viewGenDocInfo($post);
		echo json_encode($array);
	break;

	/*=====================================
	        Private Documents
	=======================================*/

	case 'viewPrivateDocInfo':
		$array 	=  $route -> viewPrivateDocInfo($post);
		echo json_encode($array);
	break;
	case 'viewPrivateCommonDocInfo':
		$array 	=  $route -> viewPrivateCommonDocInfo($post);
		echo json_encode($array);
	break;

	/*-------------------------------------------------------------------------------------
								User Management
	---------------------------------------------------------------------------------------*/

	case 'userLogin':		
		echo $route -> userLogin($_POST);
	break;	
	case 'changePassword':		
		echo $route -> changePassword($_POST);
	break;
	case 'editProfile':		
		echo $route -> editProfile($_POST);
	break;

	case 'forgot_password':
		echo $route -> forgot_password($_POST);
	break;

	case 'resetPassword':
		echo $route -> resetPassword($post);
	break;

	/*-------------------------------------------------------------------------------------
								Customer Management
	---------------------------------------------------------------------------------------*/

	case 'addCustomer':		
		echo $route -> addCustomer($_POST);
	break;	
	case 'editCustomer':		
		echo $route -> editCustomer($_POST);
	break;	
	case 'CustomerStatus':	
		echo $route->CustomerStatus($post);
	break;	
	case 'assignproperty':
		$array 	=  $route -> assignproperty($post);
		echo json_encode($array);
	break;
	case 'updateAssignProperty':
		echo $route -> updateAssignProperty($_POST);
	break;
	case 'assignPropertyToCustomer':
		echo $route -> assignPropertyToCustomer($_POST);
	break;
	case 'assignCustomerToProperty':
		echo $route -> assignCustomerToProperty($_POST);
	break;

	/*---------------------
			Block
	-----------------------*/

	case 'addBlock':		
		echo $route -> addBlock($_POST);
	break;
	case 'BlockStatus':	
		echo $route->BlockStatus($post);
	break;	
	case 'editBlock':
		$array 	=  $route -> editBlock($post);
		echo json_encode($array);
	break;
	case 'updateBlock':
		$array 	=  $route -> updateBlock($_POST);
		echo json_encode($array);
	break;

	/*---------------------
			Floor
	-----------------------*/

	case 'addFloor':		
		echo $route -> addFloor($_POST);
	break;
	case 'FloorStatus':	
		echo $route->FloorStatus($post);
	break;	
	case 'editFloor':
		$array 	=  $route -> editFloor($post);
		echo json_encode($array);
	break;
	case 'updateFloor':
		$array 	=  $route -> updateFloor($_POST);
		echo json_encode($array);
	break;	

	/*--------------------------------------------
			Flat / Villa Documents Type
	--------------------------------------------*/

	case 'addFlatVillaDocumnt':		
		echo $route -> addFlatVillaDocumnt($_POST);
	break;	
	case 'editFlatVillaDocumnt':		
		echo $route -> editFlatVillaDocumnt($_POST);
	break;	
	case 'faltVillaTypeStatus':	
		echo $route->faltVillaTypeStatus($post);
	break;	
	case 'editCheckListInformation':
		$array 	=  $route -> editCheckListInformation($post);
		echo json_encode($array);
	break;
	case 'updateCheckListInfo':
		echo $route -> updateCheckListInfo($_POST);
	break;	
	case 'deletCheckListInformation':
		echo $route -> deletCheckListInformation($post);
	break;	

	/*============================
			  Property
	==============================*/	

	case 'addFlatVilla':
		echo $route -> addFlatVilla($_POST);
	break;
	case 'editFlatVilla':
		echo $route -> editFlatVilla($_POST);
	break;
	case 'removePropertyImgIcon':
		$result 	= explode("`",$post);
		$id 		= $route->decryptData($result[0]);
		$info = $route->getDetails(PROPERTY_TBL,"image", " id= '$id' ");
		unlink("uploads/srcimg/".$info['image']);
		echo $route ->  removePropertyImgIcon($id);
	break;
	case 'cancelProperty':
		echo $route -> cancelProperty($_POST);
	break;

	/*============================
			  Contact Info
	==============================*/	

	case 'addContactInfo':
		echo $route -> addContactInfo($_POST);
	break;
	case 'editContactInfo':
		echo $route -> editContactInfo($_POST);
	break;
	case 'ContactInfoStatus':	
		echo $route->ContactInfoStatus($post);
	break;	
	case 'deleteContactInfo':
		$result 	= explode("`",$post);
		$id 		= $route->decryptData($result[0]);
		echo $route -> deleteContactInfo($id);
	break;

	case 'CompanySettingsInfoStatus':	
		echo $route->CompanySettingsInfoStatus($post);
	break;	
	case 'editCompanySettings':
		echo $route -> editCompanySettings($_POST);
	break;

	/*============================
			  Payement Info
	==============================*/	

	case 'addPaymentInfo':
		echo $route -> addPaymentInfo($_POST);
	break;
	case 'editPaymentInfo':
		echo $route -> editPaymentInfo($_POST);
	break;
	case 'PaymentInfoStatus':	
		echo $route->PaymentInfoStatus($post);
	break;	
	case 'deletePaymentInfo':
		$result 	= explode("`",$post);
		$id 		= $route->decryptData($result[0]);
		echo $route -> deletePaymentInfo($id);
	break;

	/*============================
			  Employee Info
	==============================*/	

	case 'addEmployee':
		echo $route -> addEmployee($_POST);
	break;
	case 'editEmployee':
		echo $route -> editEmployee($_POST);
	break;
	case 'employeeStatus':	
		echo $route->employeeStatus($post);
	break;	
	case 'employeePermissions': 
		echo $route->employeePermissions($_POST);		
	break;
	case 'changepasswordEmployee':
		$array 	=  $route -> changepasswordEmployee($post);
		echo json_encode($array);
	break;
	case 'empChangePassword':
		$array 	=  $route -> empChangePassword($_POST);
		echo json_encode($array);
	break;

	case 'propertyStatus':	
		echo $route->propertyStatus($post);
	break;	

	/*============================
			General Documents
	==============================*/	

	case 'generalDocumentStatus':	
		echo $route->generalDocumentStatus($post);
	break;	
	case 'removeGeneralDocument':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(GENDRAL_DOCUMENTS,"documents", " id= '$id' ");
		if ($info['documents']!="") {
			unlink("uploads/document/".$info['documents']);
		}
		echo $route -> removeGeneralDocument($id);
	break;

	/*============================
			 Property Broucher
	==============================*/	

	case 'propertyBroucherStatus':	
		echo $route->propertyBroucherStatus($post);
	break;	
	case 'removeBroucher':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(PROPERTY_BROUCHERS,"brochures", " id= '$id' ");
		if ($info['brochures']!="") {
			unlink("uploads/document/".$info['brochures']);
		}
		echo $route -> removeBroucher($id);
	break;

	case 'removeInvoiceFile':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(INVOICE_TBL,"documents", " id= '$id' ");
		if ($info['documents']!="") {
			unlink("uploads/document/".$info['documents']);
		}
		echo $route -> removeInvoiceFile($id);
	break;

	/*============================
			Request type
	==============================*/	

	case 'addRequestType':
		echo $route -> addRequestType($_POST);
	break;
	case 'requestTypeStatus':	
		echo $route->requestTypeStatus($post);
	break;	
	case 'editRequestType':
		$array 	=  $route -> editRequestType($post);
		echo json_encode($array);
	break;
	case 'updateRequestType':
		$array 	=  $route -> updateRequestType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			KYC type
	==============================*/	

	case 'addKYCType':
		echo $route -> addKYCType($_POST);
	break;
	case 'kycTypeStatus':	
		echo $route->kycTypeStatus($post);
	break;	
	case 'editkycType':
		$array 	=  $route -> editkycType($post);
		echo json_encode($array);
	break;
	case 'updatekycType':
		$array 	=  $route -> updatekycType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Notification Email
	==============================*/	

	case 'addNotificationEmail':
		echo $route -> addNotificationEmail($_POST);
	break;
	case 'notificationEmailStatus':	
		echo $route->notificationEmailStatus($post);
	break;	
	case 'editNotification':
		$array 	=  $route -> editNotification($post);
		echo json_encode($array);
	break;
	case 'updateNotification':
		$array 	=  $route -> updateNotification($_POST);
		echo json_encode($array);
	break;	

	/*--------------------------------------------
				Gallery 
	--------------------------------------------*/

	case 'addGallery':		
		echo $route -> addGallery($_POST);
	break;	
	case 'editGallery':		
		echo $route -> editGallery($_POST);
	break;	
	case 'galleryStatus':	
		echo $route->galleryStatus($post);
	break;	
	case 'removeGalleryImgIcon':
		$result 	= explode("`",$post);
		$id 		= $route->decryptData($result[0]);
		$info = $route->getDetails(GALLERY_TBL,"image", " id= '$id' ");
		unlink("uploads/srcimg/".$info['image']);
		echo $route ->  removeGalleryImgIcon($id);
	break;

	/*---------------------
		Flat Type Master
	-----------------------*/

	case 'addFlattypemaster':		
		echo $route -> addFlattypemaster($_POST);
	break;
	case 'editFlattypemaster':		
		echo $route -> editFlattypemaster($_POST);
	break;
	case 'flattypemasterStatus':	
		echo $route->flattypemasterStatus($post);
	break;	

	case 'editFlatTypeMasterInformation':
		$array 	=  $route -> editFlatTypeMasterInformation($post);
		echo json_encode($array);
	break;
	case 'updateFlatTypeInfoDetails':
		echo $route -> updateFlatTypeInfoDetails($_POST);
	break;	
	case 'deletFlatTypeInformation':
		echo $route -> deletFlatTypeInformation($post);
	break;	

	/*---------------------
			Gallery
	-----------------------*/

	case 'removePropertyImage':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(GALLERY_IMAGE,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removePropertyImage($id);
	break;

	case 'setmasterSession':
		echo $route -> setmasterSession($post);
	break;

	/*---------------------
		Property Gallery
	-----------------------*/

	case 'removePropertyGalleryImages':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(GALLERY_IMAGE,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removePropertyGalleryImages($id);
	break;

	case 'removePropertyRoomImages':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(PROPERTY_ROOMS_LIST,"image", " id= '$id' ");
		if ($info['image']!="") {
			unlink("uploads/srcimg/".$info['image']);
		}
		echo $route -> removePropertyRoomImages($id);
	break;
	


	case 'uploadDocfile':
		$array 	=  $route -> uploadDocfile($post);
		echo json_encode($array);
	break;
	case 'updateDocumentsFiles':
		echo $route -> updateDocumentsFiles($_POST);
	break;
	case 'deletPropertyDocInformation':
		$id 	= $post;
		$info 	= $route->getDetails(PROPERTY_DOCUMENTS_ITEMS,"document_file", " id= '$id' ");
		if ($info['document_file']!="") {
			unlink("uploads/document/".$info['document_file']);
		}
		echo $route -> deletPropertyDocInformation($id);
	break;


	case 'uploadGenDocfile':
		$array 	=  $route -> uploadGenDocfile($post);
		echo json_encode($array);
	break;
	case 'updateGenDocumentsFiles':
		echo $route -> updateGenDocumentsFiles($_POST);
	break;
	case 'deletPropertyGenDocInformations':
		$id 	= $post;
		$info 	= $route->getDetails(PROPERTY_DOCUMENTS_ITEMS,"document_file", " id= '$id' ");
		if ($info['document_file']!="") {
			unlink("uploads/document/".$info['document_file']);
		}
		echo $route -> deletPropertyGenDocInformations($id);
	break;


	/*============================
			Activity type
	==============================*/	

	case 'addActivityType':
		echo $route -> addActivityType($_POST);
	break;
	case 'activityTypeStatus':	
		echo $route->activityTypeStatus($post);
	break;	
	case 'editActivityType':
		$array 	=  $route -> editActivityType($post);
		echo json_encode($array);
	break;
	case 'updateActivityType':
		$array 	=  $route -> updateActivityType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Lead Status Message
	==============================*/	

	case 'addLeadType':
		echo $route -> addLeadType($_POST);
	break;
	case 'leadTypeStatus':	
		echo $route->leadTypeStatus($post);
	break;	
	case 'editLeadType':
		$array 	=  $route->editLeadType($post);
		echo json_encode($array);
	break;
	case 'updateLeadType':
		$array 	=  $route->updateLeadType($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Lead Source
	==============================*/	

	case 'addLeadSource':
		echo $route -> addLeadSource($_POST);
	break;
	case 'leadSourceStatus':	
		echo $route->leadSourceStatus($post);
	break;	
	case 'editLeadSource':
		$array 	=  $route->editLeadSource($post);
		echo json_encode($array);
	break;
	case 'updateLeadSource':
		$array 	=  $route->updateLeadSource($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Customer Profile
	==============================*/	

	case 'addCustomerProfile':
		echo $route -> addCustomerProfile($_POST);
	break;
	case 'customerProfileStatus':	
		echo $route->customerProfileStatus($post);
	break;	
	case 'editCustomerProfile':
		$array 	=  $route->editCustomerProfile($post);
		echo json_encode($array);
	break;
	case 'updateCustomerProfile':
		$array 	=  $route->updateCustomerProfile($_POST);
		echo json_encode($array);
	break;	




	/*-------------------------------------------------------------------------------------
							Menu Settings
	---------------------------------------------------------------------------------------*/

	case 'virtualtoursettings':		
		echo $route -> virtualtoursettings($_POST);
	break;	

	/*-------------------------------------------------------------------------------------
							Delet Option
	---------------------------------------------------------------------------------------*/


	case 'deletGeneralDocList':
		echo $route -> deletGeneralDocList($post);
	break;	
	case 'undoGeneralDocList':
		$array 	=  $route->undoGeneralDocList($post);
		echo json_encode($array);
	break;
	case 'updateGeneralDocList':
		$array 	=  $route->updateGeneralDocList($_POST);
		echo json_encode($array);
	break;	

	case 'publishCheckListDoc':
		echo $route -> publishCheckListDoc($post);
	break;	
	case 'deletCheckListDoc':
		echo $route -> deletCheckListDoc($post);
	break;	
	case 'undoDocCheckList':
		$array 	=  $route->undoDocCheckList($post);
		echo json_encode($array);
	break;
	case 'updateDocCheckList':
		$array 	=  $route->updateDocCheckList($_POST);
		echo json_encode($array);
	break;	

	case 'deletLead':
		echo $route -> deletLead($post);
	break;
	case 'undoLead':
		$array 	=  $route->undoLead($post);
		echo json_encode($array);
	break;
	case 'updateLead':
		$array 	=  $route->updateLead($_POST);
		echo json_encode($array);
	break;	

	case 'deleteInvoice':
		echo $route -> deleteInvoice($post);
	break;
	case 'undoInvoice':
		$array 	=  $route->undoInvoice($post);
		echo json_encode($array);
	break;
	case 'updateInvoice':
		$array 	=  $route->updateInvoice($_POST);
		echo json_encode($array);
	break;	

	case 'deleteGallery':
		echo $route -> deleteGallery($post);
	break;
	case 'undoGallery':
		$array 	=  $route->undoGallery($post);
		echo json_encode($array);
	break;
	case 'updateGallery':
		$array 	=  $route->updateGallery($_POST);
		echo json_encode($array);
	break;

	case 'deletePropertyBrochure':
		echo $route -> deletePropertyBrochure($post);
	break;
	case 'undoPropertyBrochure':
		$array 	=  $route->undoPropertyBrochure($post);
		echo json_encode($array);
	break;
	case 'updatePropertyBrochure':
		$array 	=  $route->updatePropertyBrochure($_POST);
		echo json_encode($array);
	break;	

	case 'deleteNews':
		echo $route -> deleteNews($post);
	break;
	case 'undoNews':
		$array 	=  $route->undoNews($post);
		echo json_encode($array);
	break;
	case 'updateNews':
		$array 	=  $route->updateNews($_POST);
		echo json_encode($array);
	break;	

	case 'deletProperty':
		echo $route -> deletProperty($post);
	break;
	case 'undoProperty':
		$array 	=  $route->undoProperty($post);
		echo json_encode($array);
	break;
	case 'updateProperty':
		$array 	=  $route->updateProperty($_POST);
		echo json_encode($array);
	break;

	/*-------------------------------------------------------------------------------------
							Adhoc requests
	---------------------------------------------------------------------------------------*/

	case 'replayAdhocRequest':		
		echo $route -> replayAdhocRequest($_POST);
	break;	
	case 'closeTicket':	
		echo $route->closeTicket($post);
	break;	

	case 'assignAdhocModel':
		$array 	=  $route->assignAdhocModel($post);
		echo json_encode($array);
	break;
	case 'assignAdhoc':
		echo $route -> assignAdhoc($_POST);
	break;
	case 'reassignAdhocModel':
		$array 	=  $route->reassignAdhocModel($post);
		echo json_encode($array);
	break;
	case 'reassignAdhoc':		
		echo $route -> reassignAdhoc($_POST);
	break;

	

	case 'approverejectkyc':
		$array 	=  $route -> approverejectkyc($post);
		echo json_encode($array);
	break;
	case 'updateapproverejectkyc':
		echo $route -> updateapproverejectkyc($_POST);
	break;



	case 'adhocReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");

    $cus_id  		 = $_POST['customer_id'];
    $status          = $_POST['status'];
    echo $new_from."`".$new_to."`".$cus_id."`".$status;
	break;

	case 'leadReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");

    $employee_id  	 = $_POST['employee_id'];
    $leadstatus_id   = $_POST['leadstatus_id'];
    echo $new_from."`".$new_to."`".$employee_id."`".$leadstatus_id;
	break;

	case 'customerReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");

    $customer_id  	 = $_POST['customer_id'];
    $priority  	 	 = $_POST['priority'];
    $kyc_status  	 = $_POST['kyc_status'];
    echo $new_from."`".$new_to."`".$customer_id."`".$priority."`".$kyc_status;
	break;

	case 'kycReport':
    $format_date1    = $route->changeDateFormat($_POST['from_date']);
    $date1           = date_create($format_date1);
    $new_from        = date_format($date1,"Y-m-d");
    $format_date2    = $route->changeDateFormat($_POST['to_date']);
    $date2           = date_create($format_date2);
    $new_to          = date_format($date2,"Y-m-d");

    $customer_id  	 = $_POST['customer_id'];
    $kyc_status  	 = $_POST['kyc_status'];
    echo $new_from."`".$new_to."`".$customer_id."`".$kyc_status;
	break;

	/*-------------------------------------------------------------------------------------
								Lead Management
	---------------------------------------------------------------------------------------*/

	case 'addLead':		
		echo $route -> addLead($_POST);
	break;	
	case 'editLead':		
		echo $route -> editLead($_POST);
	break;	
	case 'leadStatus':	
		echo $route->leadStatus($post);
	break;	
	case 'leadStatusModel':
		$array 	=  $route->leadStatusModel($post);
		echo json_encode($array);
	break;
	case 'assignLead':
		echo $route -> assignLead($_POST);
	break;
	case 'assignLeadModel':
		$array 	=  $route->assignLeadModel($post);
		echo json_encode($array);
	break;
	case 'reassignLeadModel':
		$array 	=  $route->reassignLeadModel($post);
		echo json_encode($array);
	break;
	case 'reassignLead':		
		echo $route -> reassignLead($_POST);
	break;

	case 'updateLeadToCustomerModel':
		$array 	=  $route->updateLeadToCustomerModel($post);
		echo json_encode($array);
	break;
	case 'updateCompletedStatus':	
		echo $route->updateCompletedStatus($post);
	break;	
	/*case 'moveLeadToCustomer':	
		echo $route->moveLeadToCustomer($post);
	break;	*/
	case 'moveLeadToCustomer':
		echo $route -> moveLeadToCustomer($_POST);
	break;


	//Lead Activity
	case 'addActivityModel':
		$array 	=  $route->addActivityModel($post);
		echo json_encode($array);
	break;
	case 'addActivity':		
		echo $route -> addActivity($_POST);
	break;
	case 'addleadActivity':		
		echo $route -> addleadActivity($_POST);
	break;	
	case 'editActivity':		
		echo $route -> editActivity($_POST);
	break;	



	/*============================
			Project type
	==============================*/	

	case 'addProjectType':
		echo $route -> addProjectType($_POST);
	break;
	case 'projectTypeStatus':	
		echo $route->projectTypeStatus($post);
	break;	
	case 'editProjectType':
		$array 	=  $route -> editProjectType($post);
		echo json_encode($array);
	break;
	case 'updateProjectType':
		$array 	=  $route -> updateProjectType($_POST);
		echo json_encode($array);
	break;	
	case 'updateLeadStatus':
		$array 	=  $route -> updateLeadStatus($_POST);
		echo json_encode($array);
	break;	

	case 'editleadactivity':
		$array 	=  $route -> editleadactivity($post);
		echo json_encode($array);
	break;
	case 'updateLeadActivity':
		$array 	=  $route -> updateLeadActivity($_POST);
		echo json_encode($array);
	break;	


	/*--------------------------------------------
				News 
	--------------------------------------------*/

	case 'addnews':		
		echo $route -> addnews($_POST);
	break;	
	case 'newsStatus':	
		echo $route->newsStatus($post);
	break;	
	case 'removeNewsImgIcon':
		$result 	= explode("`",$post);
		$id 		= $route->decryptData($result[0]);
		$info = $route->getDetails(NEWS_TBL,"image", " id= '$id' ");
		unlink("uploads/srcimg/".$info['image']);
		echo $route ->  removeNewsImgIcon($id);
	break;
	case 'editnews':		
		echo $route -> editnews($_POST);
	break;	
	case 'sendcustomer_mail':	
		echo $route->sendcustomer_mail($post);
	break;	

	/*-----------------------
		Property Area
	-------------------------*/

	case 'addPropertyArea':		
		echo $route -> addPropertyArea($_POST);
	break;	
	case 'editPropertyArea':		
		echo $route -> editPropertyArea($_POST);
	break;	
	case 'propertyAreaStatus':	
		echo $route->propertyAreaStatus($post);
	break;	

	/*-----------------------
			Department
	-------------------------*/

	case 'addDepartment':		
		echo $route -> addDepartment($_POST);
	break;	
	case 'editDepartment':		
		echo $route -> editDepartment($_POST);
	break;	
	case 'departmentStatus':	
		echo $route->departmentStatus($post);
	break;	

	/*============================
			Role
	==============================*/	

	case 'addEmployeeRole':
		echo $route -> addEmployeeRole($_POST);
	break;
	case 'empRoleStatus':	
		echo $route->empRoleStatus($post);
	break;	
	case 'editEmployeeRole':
		$array 	=  $route -> editEmployeeRole($post);
		echo json_encode($array);
	break;
	case 'updateEmployeeRole':
		$array 	=  $route -> updateEmployeeRole($_POST);
		echo json_encode($array);
	break;	

	/*============================
			Payment Stage
	==============================*/	

	case 'addPaymentStage':
		echo $route -> addPaymentStage($_POST);
	break;
	case 'stageStatus':	
		echo $route->stageStatus($post);
	break;	
	case 'editPaymentStage':
		$array 	=  $route -> editPaymentStage($post);
		echo json_encode($array);
	break;
	case 'updateStageRole':
		$array 	=  $route -> updateStageRole($_POST);
		echo json_encode($array);
	break;	

	case 'uploadVideoStatus':	
		echo $route->uploadVideoStatus($post);
	break;	
	case 'removeVideoDocument':
		$id 	= $route->decryptData($post);
		$info 	= $route->getDetails(UPLOAD_PAGE_VIDEO,"page_video", " id= '$id' ");
		if ($info['page_video']!="") {
			unlink("uploads/videos/".$info['page_video']);
		}
		echo $route -> removeVideoDocument($id);
	break;
	case 'videoPreview':
		$array 	=  $route -> videoPreview($post);
		echo json_encode($array);
	break;
	case 'videoShowPreview':
		$array 	=  $route -> videoShowPreview($post);
		echo json_encode($array);
	break;
	
	/*-----------------------*/

	case 'employeeAutoComplete':
		$data = $route -> employeeAutoComplete($_REQUEST['filter_name']);
		echo json_encode($data);
	break;

	case 'reassignEmployeeAutoComplete':
		$data = $route -> reassignEmployeeAutoComplete($_REQUEST['filter_name'],$_REQUEST['emp_id']);
		echo json_encode($data);
	break;

	case 'uploadExcel':
		
		if(!empty($_FILES["excelsheet"]["type"])) 
		{
			$errors = array();
			$msg = array();
			$validextensions = array("xls", "xlsx");
		    $temporary = explode(".", $_FILES["excelsheet"]["name"]); 
		    $file_extension = end($temporary);

		   /* if ((($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel" || $_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) && ($_FILES["excelsheet"]["size"] < 9000000) && in_array($file_extension, $validextensions))
			{*/
				if ($_FILES["excelsheet"]["error"] > 0)
				{
		           $errors[] = "Return Code: " . $_FILES["excelsheet"]["error"] . "<br/><br/>";
		        } 
				else 
				{ 
					if (file_exists("uploads/datasheet/". $_FILES["excelsheet"]["name"])) {
		            	$err =  $_FILES["excelsheet"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
		            	$errors[] = $err;
					} 
					else 
					{	
						$rands = $route -> generateRandomString("8");
						$token = $route -> generateRandomString("15");
						$curr 	= date("Y-m-d H:i:s");
						$project_id     = @$_SESSION['crm_project_id'];
						$date = date("d-m-Y");
						$sourcePath = $_FILES["excelsheet"]['tmp_name'];
						$excel_name = $date."_".$rands."_".$_FILES["excelsheet"]['name'];
						$targetPath = "uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name'];
						move_uploaded_file($sourcePath,$targetPath) ;

						// Upload to S3
						S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'datasheet/'.$excel_name, S3::ACL_PUBLIC_READ);
  						//unlink($targetPath);

						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						$objReader->setReadDataOnly(true);
						$objPHPExcel 	= $objReader->load("uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name']);
						$worksheet 		= $objPHPExcel->getActiveSheet();
						$worksheetTitle     = $worksheet->getTitle();
					    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
					    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
					    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					    $nrColumns = ord($highestColumn) - 64;
					    for ($row = 2; $row <= $highestRow; $row++ ) {
						    $val=array();
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
							   $cell = $worksheet->getCellByColumnAndRow($col, $row);
							   $val[] = $route->cleanString($cell->getValue());
							}
							//$address = strtolower($val[7]).", ".strtolower($val[8]);
							//$date_formate = explode("-", $val[4]);

							//$phpexcepDate = $val[2]-25569;
							//$dob      = date("Y-m-d", strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970)) );
							//$mobile  = explode(",", $val[13]);
							//$mobile_1 = ((isset($mobile[1])) ? $mobile[1] : "" );

							//$profile_pic = (($val[1]!="") ? strtolower($val[1]).".png" : "");

							$q = "INSERT INTO ".TEMPEXCEL_TBL."  SET 
									upload_token 	= '$token',
									tenant_id       = '".$_SESSION["tenant_id"]."',
									project_id      = '".$project_id."',
									fname 			= '".$route->cleanString($val[1])."',
									lname 			= '".$route->cleanString($val[2])."',
									leadsource 		= '".$route->cleanString($val[3])."',
									flat_type 		= '".$route->cleanString($val[4])."',
									site_visited 	= '".$route->cleanString($val[5])."',
									profile 		= '".$route->cleanString($val[6])."',
									gender 			= '".$route->cleanString($val[7])."',
									primary_mobile 	= '".$route->cleanString($val[8])."',
									primary_email 	= '".$route->cleanString($val[9])."',
									secondary_mobile= '".$route->cleanString($val[10])."',
									secondary_email = '".$route->cleanString($val[11])."',
									address 		= '".$route->cleanString($val[12])."',
									city 			= '".$route->cleanString($val[13])."',
									state 			= '".$route->cleanString($val[14])."',
									pincode  		= '".$route->cleanString($val[15])."',
									description 	= '".$route->cleanString($val[16])."',
									created_at 		= '$curr',	 
									updated_at 		= '$curr',
									status 			= '0' ";
							$exe = $route->selectQuery($q);
						}
						$output =  "1`".$excel_name."`".$token;
					}				       
		        }        
		   /* }else{
		        $errmsg =  "Invaild File. Please Enter Only Excel Sheet with the Fields provided in the sample Excel.";
		        $errors[] = $errmsg;
		    }*/

			// Output

		    if(count($errors)==0){
				echo $output;
			}else{
				$op = "";
				foreach ($errors as $value) {
					$op .= $value;
				}
				echo "0`".$op;
			}
		}	
	break;
	case 'saveExcelSession':
		echo $route -> saveExcelSession($data);
	break;
	case 'deleteImportData':
		$info = $route->getDetails(DATASHEET_TBL,"uploaded_sheet", " import_token= '$data' ");
		unlink("uploads/datasheet/".$info['uploaded_sheet']);
		echo $route -> deleteImportData($data);
	break;
	case 'approveImportData':
		echo $route -> approveImportData($data);
	break;
	case 'deleteSingleRowImportData':
		echo $route -> deleteSingleRowImportData($data);
	break;

	/*============================
			Project Mater
	==============================*/	

	case 'addProjectMaster':
		echo $route -> addProjectMaster($_POST);
	break;
	case 'projectMasterStatus':	
		echo $route->projectMasterStatus($post);
	break;	
	case 'editProjectMaster':
		$array 	=  $route -> editProjectMaster($post);
		echo json_encode($array);
	break;
	case 'updateProjectMaster':
		$array 	=  $route -> updateProjectMaster($_POST);
		echo json_encode($array);
	break;

	case 'selectProject':
		echo $route -> selectProject($post);
	break;

	case 'updateDueDate':
		echo $route -> updateDueDate($_POST);
	break;

	case 'addPaymentSchedulelist':
		echo $route -> addPaymentSchedulelist($_POST);
	break;


	case 'uploadCustomerExcel':
		
		if(!empty($_FILES["excelsheet"]["type"])) 
		{
			$errors = array();
			$msg = array();
			$validextensions = array("xls", "xlsx");
		    $temporary = explode(".", $_FILES["excelsheet"]["name"]); 
		    $file_extension = end($temporary);

		   /* if ((($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel" || $_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) && ($_FILES["excelsheet"]["size"] < 9000000) && in_array($file_extension, $validextensions))
			{*/
				if ($_FILES["excelsheet"]["error"] > 0)
				{
		           $errors[] = "Return Code: " . $_FILES["excelsheet"]["error"] . "<br/><br/>";
		        } 
				else 
				{ 
					if (file_exists("uploads/datasheet/". $_FILES["excelsheet"]["name"])) {
		            	$err =  $_FILES["excelsheet"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
		            	$errors[] = $err;
					} 
					else 
					{	
						$rands = $route -> generateRandomString("8");
						$token = $route -> generateRandomString("15");
						$curr 	= date("Y-m-d H:i:s");
						$date = date("d-m-Y");
						$sourcePath = $_FILES["excelsheet"]['tmp_name'];
						$excel_name = $date."_".$rands."_".$_FILES["excelsheet"]['name'];
						$targetPath = "uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name'];
						move_uploaded_file($sourcePath,$targetPath) ;

						// Upload to S3
						S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'datasheet/'.$excel_name, S3::ACL_PUBLIC_READ);
  						//unlink($targetPath);
												
						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						$objReader->setReadDataOnly(true);
						$objPHPExcel 	= $objReader->load("uploads/datasheet/".$date."_".$rands."_".$_FILES["excelsheet"]['name']);
						$worksheet 		= $objPHPExcel->getActiveSheet();
						$worksheetTitle     = $worksheet->getTitle();
					    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
					    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
					    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
					    $nrColumns = ord($highestColumn) - 64;

					    $project_id     = @$_SESSION['crm_project_id'];

					    for ($row = 2; $row <= $highestRow; $row++ ) {
						    $val=array();
							for ($col = 0; $col < $highestColumnIndex; ++ $col) {
							   $cell = $worksheet->getCellByColumnAndRow($col, $row);
							   $val[] = $route->cleanString($cell->getValue());
							}
							//$address = strtolower($val[7]).", ".strtolower($val[8]);
							//$date_formate = explode("-", $val[4]);

							//$phpexcepDate = $val[2]-25569;
							//$dob      = date("Y-m-d", strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970)) );
							//$mobile  = explode(",", $val[13]);
							//$mobile_1 = ((isset($mobile[1])) ? $mobile[1] : "" );

							//$profile_pic = (($val[1]!="") ? strtolower($val[1]).".png" : "");

							$q = "INSERT INTO ".TEMPCUSTOMEREXCEL_TBL."  SET 
									upload_token 	= '$token',
									tenant_id       = '".$_SESSION["tenant_id"]."',
									project_id      = '".$project_id."',
									name 			= '".$route->cleanString($val[1])."',
									addresss 		= '".$route->cleanString($val[2])."',
									mobile 			= '".$route->cleanString($val[3])."',
									email 			= '".$route->cleanString($val[4])."',
									created_at 		= '$curr',	 
									updated_at 		= '$curr',
									status 			= '0' ";
							$exe = $route->selectQuery($q);
						}
						$output =  "1`".$excel_name."`".$token;
					}				       
		        }        
		   /* }else{
		        $errmsg =  "Invaild File. Please Enter Only Excel Sheet with the Fields provided in the sample Excel.";
		        $errors[] = $errmsg;
		    }*/

			// Output

		    if(count($errors)==0){
				echo $output;
			}else{
				$op = "";
				foreach ($errors as $value) {
					$op .= $value;
				}
				echo "0`".$op;
			}
		}	
	break;
	case 'saveCustomerExcelSession':
		echo $route -> saveCustomerExcelSession($data);
	break;
	case 'deleteCustomerImportDatass':
		$info = $route->getDetails(CUSTOMERDATASHEET_TBL,"uploaded_sheet", " import_token= '$data' ");
		unlink("uploads/datasheet/".$info['uploaded_sheet']);
		echo $route -> deleteCustomerImportDatass($data);
	break;
	case 'approveCustomerImportData':
		echo $route -> approveCustomerImportData($data);
	break;
	case 'deleteCustomerSingleRowImportData':
		echo $route -> deleteCustomerSingleRowImportData($data);
	break;


	case 'searchCustomerJson':
		$data = $route -> searchCustomerJson($_REQUEST['filter_name']); 
		echo json_encode($data);
	break;

	case 'searchEditCustomerJson':
		$data = $route -> searchEditCustomerJson($_REQUEST['filter_name'],$_REQUEST['cust_ids']); 
		echo json_encode($data);
	break;

	// Lead Filter

	case 'leadFilter':
    $escapedPost 	= $_POST;
    $leadsource 	= $escapedPost['leadsource'];
    $movecustomer 	= $escapedPost['movecustomer'];
    $assignedstatus = $escapedPost['assignedstatus'];
    $assignedto     = $escapedPost['assignedto'];
    $leadstatus     = $escapedPost['leadstatus'];
    
    echo $leadsource."`".$movecustomer."`".$assignedstatus."`".$assignedto."`".$leadstatus;
	break;

	// Adhoc Filter

	case 'adhocFilter':
    $escapedPost 	= $_POST;
    $requeststatus 	= $escapedPost['requeststatus'];
    $request_id 	= $escapedPost['request_id'];
    $priority 	= $escapedPost['priority'];

    $assignstaus 	= $escapedPost['assignstaus'];
    $assigned_to 	= $escapedPost['assigned_to'];
    
    echo $requeststatus."`".$request_id."`".$priority."`".$assignstaus."`".$assigned_to;
	break;

	case 'adhocAllFilter':
    $escapedPost 	= $_POST;
    $department_id 	= $escapedPost['department_id'];
    $status 		= $escapedPost['status'];
    $assignstaus 	= $escapedPost['assignstaus'];
    $assigned_to 	= $escapedPost['assigned_to'];
    
    echo $department_id."`".$status."`".$assignstaus."`".$assigned_to;
	break;

	// Property Filter

	case 'propertyFilter':
    $escapedPost 	= $_POST;
    $facing = $escapedPost['facing'];
    $block 	= $escapedPost['block'];
    $floor 	= $escapedPost['floor'];
    $room 	= $escapedPost['room'];
    
    echo $facing."`".$block."`".$floor."`".$room;
	break;


	/*-------------------------------------------------------------------------------------
								Test Mail
	---------------------------------------------------------------------------------------*/

	case 'sendMail':
		$sender = COMPANY_NAME;
		$sender_mail = NO_REPLY;
		$receiver = "webykart.com@gmail.com";
		$subject = "Reset Password Mail";
		$email_temp = "test1234567879";
		echo $route->send_mail($sender,$sender_mail,$receiver,$subject,$email_temp,$bcc="");
	break;

}	
	

?>