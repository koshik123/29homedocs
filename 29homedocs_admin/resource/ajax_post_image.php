<?php
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
$route 		= new Ajaxcontroller();


// S3 Bucket
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

// Add New Product productimage

$errors = array();
$msg = array();

if(!empty($_FILES["cimage"]["type"]))
{
    $validextensions = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
    $temporary = explode(".", $_FILES["cimage"]["name"]); 
    $file_extension = end($temporary);
		if(($_FILES["cimage"]["size"] < 900000000) && in_array($file_extension, $validextensions))
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");

	        if ($_FILES["cimage"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["cimage"]["error"] . "<br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/srcimg/".$_FILES["cimage"]["name"])) {
	            	$err =  $_FILES["cimage"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				} 
				else 
				{					
					$sourcePath = $_FILES["cimage"]['tmp_name'];
					$post_image_name = $rands.$date.$route->hyphenize($_FILES["cimage"]['name']);
					$targetPath = "uploads/srcimg/".$post_image_name;
					move_uploaded_file($sourcePath,$targetPath) ;


					// Upload to S3
					S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'srcimg/'.$post_image_name, S3::ACL_PUBLIC_READ);
  					unlink($targetPath);
					
					//$source_url = $targetPath;
					//$destination_url = $targetPath;
					//$process_img = $route->compress_image($source_url, $destination_url, 60);
					//$output = str_replace("uploads/customers/", "", $process_img);
					$msg[] = $post_image_name;
				}				       
	        }
		}
		else{
			$errmsg =  "Customer Image Size Exceed 800 KB. Please upload proper Image with proper file size.<br/>";
        	$errors[] = $errmsg;
		}
   
}else{
	$msg[] = "";
}

if(count($errors)==0){
	$success = "";
	foreach ($msg as $key =>  $value) {
		$success .= "`".$value;
	}
	echo "1".$success;
}else{
	$op = "";
	foreach ($errors as $value) {
		$op .= $value;
	}
	echo "0`".$op;
}



?>