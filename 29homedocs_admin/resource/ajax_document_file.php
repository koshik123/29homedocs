<?php
session_start();
require_once '../app/core/user_ajaxcontroller.php';
$route 		= new Ajaxcontroller();


// S3 Bucket
require_once '../app/models/S3.php';
S3::setAuth(AWS_S3_KEY, AWS_S3_SECRET);
S3::setRegion(AWS_S3_REGION);
S3::setSignatureVersion('v4');

// Add Document 

$errors = array();
$msg = array();
$content 	= $_POST;
//$url = COREPATH."resource/";
if(!empty($_FILES["attachment"]["type"]))  
{
	$validextensions = array( "doc", "docx", "dot", "pdf","jpeg", "jpg", "png", "JPG", "JPEG", "PNG", "xls", "xlsx");
    $temporary = explode(".", $_FILES["attachment"]["name"]); 
    $file_extension = end($temporary);
    if(($_FILES["attachment"]["type"] == "application/msword") || ($_FILES["attachment"]["type"] == "application/pdf") || ($_FILES["attachment"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") || ($_FILES["attachment"]["type"] == "application/octet-stream") || ($_FILES["attachment"]["type"] == "application/stream") || ($_FILES["attachment"]["type"] == "image/png") || ($_FILES["attachment"]["type"] == "image/jpg") || ($_FILES["attachment"]["type"] == "image/jpeg") || ($_FILES["excelsheet"]["type"] == "application/vnd.ms-excel") || ($_FILES["excelsheet"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")  && in_array($file_extension, $validextensions) ) 
	{
		if(($_FILES["attachment"]["size"] < 800000000))
		{
			$rands = $route -> generateRandomString("10");
			$date = date("dmYhis");
	        if ($_FILES["attachment"]["error"] > 0)
			{
	           $errors[] = "Return Code: " . $_FILES["attachment"]["error"] . "<br/><br/>";
	        } 
			else 
			{ 
				if (file_exists("uploads/document/".$_FILES["attachment"]["name"])) {
	            	$err =  $_FILES["attachment"]["name"]." <span id='invalid'><b>already exists.</b></span> ";
	            	$errors[] = $err;
				}
				else 
				{					
					$sourcePath = $_FILES["attachment"]['tmp_name'];
					$document_name = "attachment_".$date."_".$rands.".".$file_extension;
					$targetPath = "uploads/document/".$document_name;
					move_uploaded_file($sourcePath,$targetPath);

					// Upload to S3
					S3::putObject(S3::inputFile($targetPath), AWS_S3_BUCKET, 'document/'.$document_name, S3::ACL_PUBLIC_READ);
  					unlink($targetPath);
					
					$msg['pdf'] = $document_name;
				}				       
	        }
		}
		else{
			$errmsg =  "Document Size Exceed 800 KB. Please upload proper Document with proper file size.<br/>";
        	$errors[] = $errmsg;
		}		    
    }   
	else 
	{
        $errmsg =  $_FILES["attachment"]["type"]." Invalid Document type. Please upload documents in  PDF formats alone.";
        $errors[] = $errmsg;
    }
}else{
	//$errmsg =  "Please Attach the PDF.";
    //$errors[] = $errmsg;
    $msg['pdf'] = "";
}
if(count($errors)==0){
	//echo "1`".$route->encryptData($msg[0]);

	$success = "";
	foreach ($msg as $key =>  $value) {
		$success .= "`".$value;
	}
	//echo $route->addJournal($content,$msg);

	$page = @$_REQUEST["page"];
        switch($page){
            case 'addGendralDocuments':
                echo $route->addGendralDocuments($content,$msg);
            break;
            case 'editGendralDocuments':
                echo $route->editGendralDocuments($content,$msg);
            break;
            
            case 'addPropertyBroucher':
                echo $route->addPropertyBroucher($content,$msg);
            break;  
            case 'editPropertyBroucher':
                echo $route->editPropertyBroucher($content,$msg);
            break;  

            case 'addPropertyGendralDocuments':
                echo $route->addPropertyGendralDocuments($content,$msg);
            break;  

            case 'addInvoice':
                echo $route->addInvoice($content,$msg);
            break; 

            case 'editInvoice':
                echo $route->editInvoice($content,$msg);
            break;  
                       
        }
}else{
	$op = "";
	foreach ($errors as $value) {
		$op .= $value;
	}
	echo "0`".$op;
}



?>