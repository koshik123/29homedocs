<?php 
session_start();
//require_once '../config/config.php';
require_once '../app/core/user_ajaxcontroller.php';
require_once '../app/excel/PHPExcel.php';
$route = new Ajaxcontroller();
//$client_id 	=  @$_REQUEST["client_id"];

$from	 	= @$_GET['from'];
$to	 		= @$_GET['to'];
$customer_id= @$_GET['customer_id'];
$kyc_status = @$_GET['kyc_status'];

$today = date("d-m-y");


$title = "kyc_report_".$today;



	//------------- Create Header ------------//

	// PHPExcel Instance
	$sheet = new PHPExcel();
	// Set document properties
	$sheet->getProperties()->setCreator("29homesdocs")
	               ->setLastModifiedBy(COMPANY_NAME)
	               ->setTitle("KYC Report")
	               ->setSubject("KYC Report")
	               ->setDescription("KYC Report")
	               ->setKeywords("29homesdocs")
	               ->setCategory("Excel Report");
	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$sheet->setActiveSheetIndex(0);
	$activeSheet = $sheet->getActiveSheet();
	
	// Create Header
	$sheet->setActiveSheetIndex(0)
				->setCellValue('A1', 'S.No')
	           	->setCellValue('B1', 'Submitted On')
	           	->setCellValue('C1', 'Customer Name')
	            ->setCellValue('D1', 'Customer Name')
	            ->setCellValue('E1', 'Mobile')
	            ->setCellValue('F1', 'Email')
	            ->setCellValue('G1', 'KYC Status')
	            ->setCellValue('H1', 'Checked By');

	$activeSheet->getColumnDimension('A')->setAutoSize(true);
	$activeSheet->getColumnDimension('B')->setAutoSize(true);
	$activeSheet->getColumnDimension('C')->setAutoSize(true);
	$activeSheet->getColumnDimension('D')->setAutoSize(true);
	$activeSheet->getColumnDimension('E')->setAutoSize(true);
	$activeSheet->getColumnDimension('F')->setAutoSize(true);
	$activeSheet->getColumnDimension('G')->setAutoSize(true);
	$activeSheet->getColumnDimension('H')->setAutoSize(true);

	//------------- Create Header Ends ------------//

	//------------- Create Report Body ------------//

  	$today = date("Y-m-d");
	$users = array();
	$project_id = @$_SESSION['crm_project_id'];
	$tenant_id  = @$_SESSION["tenant_id"];
	$q = "SELECT K.id,K.customer_id,K.kyctype_id,K.image_name,K.file_type,K.status,K.approval_status,K.created_at,C.name,C.uid,C.kyc_status,C.approved_on,C.approved_by,C.mobile,C.email,E.name as emp_name  FROM ".KYC_LIST." K LEFT JOIN ".CUSTOMER_TBL." C ON (C.id=K.customer_id) LEFT JOIN ".EMPLOYEE." E ON (E.id=C.approved_by) WHERE K.created_at BETWEEN '$from' AND '$to'   " ;
		if ($customer_id!="") {
			$q .=" AND K.customer_id='$customer_id'  ";
		}
		if ($kyc_status!="") {
			$q .=" AND C.kyc_status='$kyc_status' ";
		}
			$q .=" AND K.project_id='".$project_id."' AND K.tenant_id='".$tenant_id."' GROUP BY K.customer_id  ";
	$exe = $route->selectQuery($q);	
    $row_count = mysqli_num_rows($exe);
    if(mysqli_num_rows($exe) > 0){
    	$i=1;
    	while($list = mysqli_fetch_array($exe)){
                $kyc_status1 = (($list['kyc_status']=='0') ? 'Not submitted' : $list['kyc_status']  );
                $kyc_status2  = (($list['kyc_status']=='1') ? 'Approved' : $kyc_status1 );
                $kyc_status3  = (($list['kyc_status']=='2') ? 'Pending Approval' : $kyc_status2 );
                $kyc_status  = (($list['kyc_status']=='3') ? 'Rejected' : $kyc_status3 );

                
                $employee_name = (($list['emp_name']=='') ? '-' : ucwords($list['emp_name']));

    			$element 	=  array();
				$element[] 	=  $i;
				$element[] 	=  date("d M, Y h:i a",strtotime($list['created_at']));
				$element[] 	=  ($list['uid']);
				$element[] 	=  ucwords($list['name']);
				$element[] 	=  ($list['mobile']);
				$element[] 	=  ($list['email']);
				$element[] 	=  $kyc_status;
				$element[] 	=  $employee_name;
				$users[] 	=  $element;
			  	$i++;
    	}
    }

    $row = 2;
	foreach ($users as $key => $value) {
		$col = 0;
	    foreach ($value as $key=> $value) {
	        //echo $row." $col -- ".$key."=".$value."<br/>";
	        $sheet->getActiveSheet()->setCellValueByColumnAndRow($key, $row, $value);
	        $col++;
	   }
	   $row++;
	}

	//------------- Create Report Body Ends------------//

	// Rename worksheet
	
	$sheet_name = "KYC Report";
	
	$activeSheet ->setTitle($sheet_name);
	$filename = $title.".csv";
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.$filename);
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	//ob_end_clean();
	$objWriter = PHPExcel_IOFactory::createWriter($sheet, 'CSV');
	$objWriter->save('php://output');
?>