<?php
	

	// Website Base Path
	define('BASEPATH', HOST_NAME.'/');
	// Website Admin Path
	define('COREPATH', HOST_NAME.ADMIN_PATH.'/');

	// Define image folder path
	define('IMGPATH', HOST_NAME.ADMIN_PATH.'/lib/images/');
	// Define css folder path
	define('CSSPATH', HOST_NAME.ADMIN_PATH.'/lib/css/');
	// Define js folder path
	define('JSPATH', HOST_NAME.ADMIN_PATH.'/lib/js/');
	// Define js folder path
	define('FONTPATH', HOST_NAME.ADMIN_PATH.'/lib/fonts/');
	// Define Plugin folder path
	define('PLUGINS', HOST_NAME.ADMIN_PATH.'/lib/plugins/');
	// Define Uploads Image path
	define('UPLOADS', HOST_NAME.ADMIN_PATH.'/resource/uploads/');
	// Define Plugin folder path
	define('SRCIMG', HOST_NAME.ADMIN_PATH.'/resource/uploads/srcimg/');
	// Define Uploads Image path
	define('DOCUMENTS', HOST_NAME.ADMIN_PATH.'/resource/uploads/document/');

	// Define Plugin folder path
	define('FRONTSRCIMG', HOST_NAME.'/resource/uploads/srcimg/');
	///// Define No Video Image path
	define('VIDEO_PATH', HOST_NAME.ADMIN_PATH.'/resource/uploads/videos/');
	

	//--------------------- DATABASE CONNECTION ---------------------//

	// Connect Mysql DB
	$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);

	
?>