
function closeWin() {
    myWindow.close();   
}

$(document).on('click','.close',function() {
  $(".alert").fadeOut(500); 
   return false;   
 });


// Select Post permissions  

$('#selectAllPost').click(function() {
    $('.post_permission').prop('checked', this.checked);
});




/*$(document).ready(function() {*/


$(document).on('click', '.mastermenu_click', function() {
    var value = $(this).data("value");
    if (value=="mastersettings") {
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=setmasterSession",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                location.reload();
            }
        });
      /*$("#master_settings_display").toggle();
      $('#master_settings_display').toggleClass("nk-sidebar");*/
    }else{
      $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=setmasterSession",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
               // location.reload();
            }
        });
    }

});




$(".galleryType").click(function() {
    var value = $(this).val();
    if (value == "property") {
        $("#selectproperty_warp").show();
    } else {
        $("#selectproperty_warp").hide();
    }
});

$(".post_to").click(function() {
    var value = $(this).val();
    if (value == "selected") {
        $("#selectcustomer_warp").show();
        $("#selectcustomerdisplay_warp").show();
    } else {
        $("#selectcustomer_warp").hide();
        $("#selectcustomerdisplay_warp").hide();
    }
});

$(".show_popup").click(function() {
    if($('#com-email-2').prop('checked')) {
        $("#show_popup_warp").show();
    } else {
        $("#show_popup_warp").hide();
    }
});

$(".propertyimg").click(function() {
    var value = $(this).val();

    if (value == "images") {
        window.location = core_path + "property/";
    } else {
         window.location = core_path + "property/";
    }
});

$(".raised_by").click(function() {
    var value = $(this).val();
    if (value == "user") {
        $("#customer_warp_id").show();
    } else {
        $("#customer_warp_id").hide();
    }
});

/*-------------------------------------------------------
                Gallery
--------------------------------------------------------*/

 // upload multiple inmages

$("form[name='addGalleryImages']").submit(function() {
    var flag = true;
    var redirect = $("#redirect").val();
    if (flag) {
        $.ajax({
            url: core_path + "resource/ajax_multiple_images.php?page=addGalleryImages",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "gallery?i=success";
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
    }
    return false;
});

// Remove hallery images

$(".removePropertyImage").click(function() {
    var id = $(this).data("token");
        var option = $(this).data("option");
    swal({
        title: "Are you sure to delete this image?",
        text: "Once deleted the image will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removePropertyImage",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    $("#pro_" + option).remove();
                    masonryInt();
                    //window.location = core_path + "product?c=success";
                    //location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});

/*-------------------------------------------------------
                Property Gallery
--------------------------------------------------------*/

 // upload multiple inmages

$("form[name='addPropertyGalleryImages']").submit(function() {
    var flag = true;
    var redirect = $("#redirect").val();
    var property_id = $("#property_id").val();
    if (flag) {
        $.ajax({
            url: core_path + "resource/ajax_multiple_images.php?page=addPropertyGalleryImages",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "property/details/"+ property_id +"?i=success";
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
    }
    return false;
});

// Remove hallery images

$(".removePropertyGalleryImages").click(function() {
    var id = $(this).data("token");
        var option = $(this).data("option");
    swal({
        title: "Are you sure to delete this image?",
        text: "Once deleted the image will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removePropertyGalleryImages",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    $("#pro_" + option).remove();
                    masonryInt();
                    //window.location = core_path + "product?c=success";
                    //location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});

/*-------------------------------------------------------
                Property Room Gallery
--------------------------------------------------------*/

 // upload multiple inmages

$("form[name='addPropertyRoomImages']").submit(function() {
    var flag = true;
    var redirect = $("#redirect").val();
    var property_id = $("#property_id").val();
    if (flag) {
        $.ajax({
            url: core_path + "resource/ajax_multiple_images.php?page=addPropertyRoomImages",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "property/details/"+ property_id +"?r=success";
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
    }
    return false;
});

// Remove hallery images

$(".removePropertyRoomImages").click(function() {
    var id = $(this).data("token");
        var option = $(this).data("option");
    swal({
        title: "Are you sure to delete this image?",
        text: "Once deleted the image will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removePropertyRoomImages",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    $("#pro_" + option).remove();
                    masonryInt();
                    //window.location = core_path + "product?c=success";
                    //location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});

/*----------------------------
        Login Functions
 ------------------------------*/

// User Login

$("#login_auth").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
        }
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
        password: {
            required: "Please Enter your Password",
        }
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=userLogin",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path;
                    //location.reload();
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

$("#changePassword").validate({
    rules: {
        password: {
            required: true
        },
        new_password: {
            required: true,
        },
        re_password: {
            required: true,
            equalTo: "#new_password"
        }
    },
    messages: {
        password: {
            required: "Please Enter your Password",
        },
        new_password: {
            required: "Please Enter New Password",
        },
        re_password: {
            required: "Please Enter your New Password Again",
        }
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=changePassword",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "profile/changepassword?a=success";
                    //location.reload();
                } else {
                    $(".form-error").html(data);
                    $(".form-error").show();
                }
            }
        });
        return false;
    }
});


// Edit Profile 

$("#editProfile").validate({
    rules: {
        name: {
            required: true

        },        
        mobile: {
            required: true,
            digits: true
        },
        email: {
            required: true
        },   
    },
    messages: {
        name: {
            required: "Please Enter Admin Name",
        },       
        mobile: {
            required: "Please Enter Mobile Number",
        },
        email: {
            required: "Please Enter Email Address",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editProfile",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "profile?p=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Forgot Password

$("#forgot_password").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=forgot_password",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                $(".form-error").html(data);
            }
        });
        return false;
    }
});

// Reset Password

$("#resetpass").validate({
    rules: {
        new_pass: {
            required: true,
        },
        confirm_pass: {
            required: true,
            equalTo: "#newpassword"
        }
    },
    messages: {
        new_pass: {
            required: "Please Enter Your New Password",
        },
        confirm_pass: {
            required: "Please Enter Your Confirm Password",
        }
    },
    submitHandler: function(form) {
        var newpassword = $("#newpassword").val();
        var token = $("#token").val();
        var value = token + "`" + newpassword;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=resetPassword",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "?reset=success";
                } else {
                    $(".form-error").html(data);
                }

            }
        });
        return false;
    }
});


/*----------------------------
        Customer
 ------------------------------*/

// Customer

$("#addCustomer").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            digits: true
        },
        name: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
        name: {
            required: "Please Enter your Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addCustomer",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "customer?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Customer

$("#editCustomer").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            digits: true
        },
        name: {
            required: true,
        },
        gender: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter your Email Address",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
        name: {
            required: "Please Enter your Name",
        },
        gender: {
            required: "Please Selecr your Gender",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editCustomer",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               // alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "customer?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Active & Inactive Status

$(".CustomerStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=CustomerStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

 // Assign Property 

    $(document).on('click', '.assignproperty', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=assignproperty",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".assignpropertyInfo").html(response['layout']);
                $('.assignpropertyModel').modal('show');
            }
        })
        return false;
    });

    // update Multiple files Attachment

    $("#updateAssignProperty").validate({
        rules: {
            property_id: {
                required: true
            }
        },
        messages: {
            property_id: {
                required: "Please Select Property",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateAssignProperty",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    data = data.split('`');
                    if (data['0'] == 1) {
                        $('.assignpropertyModel').modal('hide');
                        location.reload();
                        return true;
                    } else {
                        $(".perror").html(data['1']);
                    }
                    return false;
                }
            });

            return false;
        }
    });


    // Assign Property 

    $("#assignPropertyToCustomer").validate({
        rules: {
            property_id: {
                required: true
            },
            allotment_date: {
                required: true
            },
        },
        messages: {
            property_id: {
                required: "Please Select Flat/Villa",
            },
            allotment_date: {
                required: "Please Select >Date of Allotment",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);

            swal({
                title: "Are you sure to assign this Flat/Villa?",
                text: "Once assign the flat/villa will not be reassign!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Yes, assign it!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: core_path + "resource/ajax_redirect.php?page=assignPropertyToCustomer",
                        dataType: "html",
                        data: content,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location = core_path + "customer?as=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "not saved yet", "error");
                }
             });
            return false;
        }
    });


    // Assign Customer 

    $("#assignCustomerToProperty").validate({
        rules: {
            customer_id: {
                required: true
            },
            allotment_date: {
                required: true
            },
        },
        messages: {
            customer_id: {
                required: "Please Select Customer",
            },
            allotment_date: {
                required: "Please Select >Date of Allotment",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);

            swal({
                title: "Are you sure to assign this Customer?",
                text: "Once assign the customer will not be reassign!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Yes, assign it!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: core_path + "resource/ajax_redirect.php?page=assignCustomerToProperty",
                        dataType: "html",
                        data: content,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location = core_path + "property?as=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "not saved yet", "error");
                }
             });
            return false;
        }
    });



/*----------------------------
       Manage Block
 ------------------------------*/

// Add Block

$("#addBlock").validate({
    rules: {
        block: {
            required: true,
        },
    },
    messages: {
        block: {
            required: "Please Enter your Block Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addBlock",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "block?a=success";
                } else {
                    //swal.close();
                    $(".form-error").html(data);
                    
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".BlockStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=BlockStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});


// Edit 

$('.editBlock').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editBlock",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#editfkey").val(response['fkey']);
            $("#token").val(response['id']);
            $("#block_info").val(response['block']);
            $('.blockModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updateBlock").validate({
    rules: {
        block_info: {
            required: true,
        },
    },
    messages: {
        block_info: {
            required: "Please Enter your Block Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateBlock",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    $('.blockModel').modal('hide');
                     window.location = core_path + "block?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});


/*----------------------------
       Manage Floors
 ------------------------------*/

// Add Floor

$("#addFloor").validate({
    rules: {
        floor: {
            required: true,
        },
    },
    messages: {
        floor: {
            required: "Please Enter your Floor Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addFloor",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "floor?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Edit 

$('.editFloor').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editFloor",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#editfkey").val(response['fkey']);
            $("#token").val(response['id']);
            $("#floor_info").val(response['floor']);
            $('.floorModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updateFloor").validate({
    rules: {
        floor_info: {
            required: true,
        },
    },
    messages: {
        floor_info: {
            required: "Please Enter your Floor Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateFloor",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    $('.floorModel').modal('hide');
                     window.location = core_path + "floor?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});



// Active & Inactive Status

$(".FloorStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=FloorStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

/*----------------------------
    Manage Flat/Villa Douments
 ------------------------------*/

// Add 

$("#addFlatVillaDocumnt").validate({
    rules: {       
        document_title: {
            required: true,
        },
        sort_order: {
            required: true,
        },
    },
    messages: {
        document_title: {
            required: "Please Enter your Flat/Villa Documents Title",
        },
        sort_order: {
            required: "Please Enter your Sort Order",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addFlatVillaDocumnt",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "documents/project?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// edit 

$("#editFlatVillaDocumnt").validate({
    rules: {       
        document_title: {
            required: true,
        },
        sort_order: {
            required: true,
        },
    },
    messages: {
        document_title: {
            required: "Please Enter your Flat/Villa Documents Title",
        },
        sort_order: {
            required: "Please Enter your Sort Order",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editFlatVillaDocumnt",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "documents/project?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".faltVillaTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=faltVillaTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            //alert(data);
            if (data == 1) {
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Edit Check List

$(document).on('click', '.editCheckListInformation', function() {
    var id = $(this).data("option");
    var type = $(this).data("type");
    var sno = $(this).data("value");
    var value = id + "`" + sno;
    //alert(id);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editCheckListInformation",
        dataType: "html",
        //data: { result: id, option: value },
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editCheckListInfoModel").html(response['layout']);
            $('.editCheckLIstAttachment').modal('show');
        }
    })
    return false;
});

// update Check List

$("#updateCheckListInfo").validate({
    rules: {
        doc_name: {
            required: true
        }
    },
    messages: {
        doc_name: {
            required: "Please Enter Document Name",
        }
    },
    submitHandler: function(form) {
        var option = $("#option").val();
        $(".page_loading").show();
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateCheckListInfo",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                data = data.split('`');
                if (data['0'] == 1) {
                    $('.editCheckLIstAttachment').modal('hide');
                    $("#attacjmentrow_" + option).replaceWith(data['1']);
                    return true;
                } else {
                    $(".perror").html(data['1']);
                }
            }
        });

        return false;
    }
});

// Delete Check List

$(document).on("click", ".deletCheckListInformation", function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    var type = $(this).data("type");
    swal({
        title: "Are sure you to delete this Documnet type ?",
        text: "You will not be able to recover this Details !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        var value = id;
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deletCheckListInformation",
                dataType: "html",
                data: { result: value },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data[0] == 1) {
                        $("#attacjmentrow_" + id).remove();
                        swal("Deleted!", "Documnet type has been deleted.", "success");
                        //location.reload();
                    } else {
                        swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                    }
                }
            });
        } else {
            swal("Cancelled", "Documnet type retained", "error");
        }
    });
    return false;
});
    

/*============================
      Property  
==============================*/

// Add 

$("#addFlatVilla").validate({
    rules: {
        title: {
            required: true
        },
        projectsize: {
            required: true
        },
        projectarea: {
            required: true
        },
        cost: {
            required: true
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        projectsize: {
            required: "Please Enter Project Size",
        },
        projectarea: {
            required: "Please Enter Project Area",
        },
        cost: {
            required: "Please Enter Cost",
        },
    },
    submitHandler: function(form) {
        var photo = $("#cimage").val();
        var formname = document.getElementById('addFlatVilla');
        var formData = new FormData(formname);
        if (photo != "") {
            swal({
                title: "Are you sure to submit this Property?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55 ",
                confirmButtonText: "Yes, I am Sure !",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_post_image.php",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                           // alert(data);
                            data = data.split('`');
                            if (data[0] == 1) {
                                $("#newsImage").val(data[1]);
                                if (data[0] == 1) {
                                    var content = $(form).serialize();
                                    $.ajax({
                                        type: "POST",
                                        url: core_path + "resource/ajax_redirect.php?page=addFlatVilla",
                                        dataType: "html",
                                        data: content,
                                        beforeSend: function() {
                                            $(".page_loading").show();
                                        },
                                        success: function(data) {
                                           // alert(data);
                                            $(".page_loading").hide();
                                            if (data == 1) {
                                                window.location.href = core_path + "property?a=success";
                                                return true;
                                            } else {
                                                $(".form-error").html(data);
                                                swal.close();
                                            }
                                            return false;
                                        }
                                    });
                                }
                            } else {
                                $(".imgerr").html(data[1]);
                                return false;
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Not Saved Yet", "error");
                }
            });

        } else {
            swal("Please Add Image", "Image not found, Kindly upload the image", "error");
        }
        return false;
    }
});

// Edit 

$("#editFlatVilla").validate({
    rules: {
        title: {
            required: true
        },
        projectsize: {
            required: true
        },
        projectarea: {
            required: true
        },
        cost: {
            required: true
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        projectsize: {
            required: "Please Enter Project Size",
        },
        projectarea: {
            required: "Please Enter Project Area",
        },
        cost: {
            required: "Please Enter Cost",
        },
    },
    submitHandler: function(form) {
        var photo = $("#cimage").val();
        var formname = document.getElementById('editFlatVilla');
        var formData = new FormData(formname);
        if (photo != "") {
            swal({
                title: "Are you sure to submit this Property?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55 ",
                confirmButtonText: "Yes, I am Sure !",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_post_image.php",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            //alert(data);
                            data = data.split('`');
                            if (data[0] == 1) {
                                $("#newsImage").val(data[1]);
                                if (data[0] == 1) {
                                    var content = $(form).serialize();
                                    $.ajax({
                                        type: "POST",
                                        url: core_path + "resource/ajax_redirect.php?page=editFlatVilla",
                                        dataType: "html",
                                        data: content,
                                        beforeSend: function() {
                                            $(".page_loading").show();
                                        },
                                        success: function(data) {
                                            //alert(data);
                                            $(".page_loading").hide();
                                            if (data == 1) {
                                                window.location.href = core_path + "property?e=success";
                                                return true;
                                            } else {
                                                $(".form-error").html(data);
                                                swal.close();
                                            }
                                            return false;
                                        }
                                    });
                                }
                            } else {
                                $(".imgerr").html(data[1]);
                                return false;
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Not Saved Yet", "error");
                }
            });

        } else {
            swal("Please Add Image", "Image not found, Kindly upload the image", "error");
        }
        return false;
    }
});

// Remove Property Image

$(".removePropertyImgIcon").click(function() {
    var id = $(this).data("id");
    var type = $(this).data("type");
    swal({
        title: "Are sure you to delete this Image ?",
        text: "You will not be able to recover this Details !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        var value = id + "`" + type;
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=removePropertyImgIcon",
                dataType: "html",
                data: { result: value },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {

                    $(".page_loading").hide();
                    //alert(data);
                    if (data[0] == 1) {
                        swal("Deleted!", "Image has been deleted.", "success");
                        location.reload();
                    } else {
                        swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                    }
                }
            });
        } else {
            swal("Cancelled", "Image retained", "error");
        }
    });
    return false;
});


// add agenda items

    $("form[name='addPropertyRooms']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_multiple_property_file.php?page=addPropertyRooms",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "property?r=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });

    // Cancel Property

    $("#cancelProperty").validate({
        rules: {
            reason_cancellation: {
                required: true,
            },
            cancel_remarks: {
                required: false,
            },
        },
        messages: {
            reason_cancellation: {
                required: "Please Enter Reason for cancellation",
            },
            cancel_remarks: {
                required: "Please Select Activity Date",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            swal({
                title: "Are sure you to confirm cancellation ?",
                text: "You will not be able to recover this Details !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, cancel it!",
                closeOnConfirm: true,
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: core_path + "resource/ajax_redirect.php?page=cancelProperty",
                        dataType: "html",
                        data: content,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location = core_path + "property?c=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Image retained", "error");
                }
            });
            return false;
        }
    });

/*----------------------------
       Manage Contact Info
 ------------------------------*/

// Add Contact Info

$("#addContactInfo").validate({
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
        },
        mobile: {
            required: true,
            digits: true
        },
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        email: {
            required: "Please Enter your Email",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addContactInfo",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "contactinfo?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Edit Contact Info

$("#editContactInfo").validate({
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
        },
        mobile: {
            required: true,
            digits: true
        },
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        email: {
            required: "Please Enter your Email",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editContactInfo",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "contactinfo?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".ContactInfoStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=ContactInfoStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Edit Company Settings Info

$("#editCompanySettings").validate({
    rules: {
        website_address: {
            required: true,
        },
        email: {
            required: true,
        },
        phone: {
            required: true,
            digits: true
        },
        address: {
            required: true,
        },
    },
    messages: {
        website_address: {
            required: "Please Enter your Website Address",
        },
        email: {
            required: "Please Enter your Email",
        },
        phone: {
            required: "Please Enter your Mobile Number",
        },
        address: {
            required: "Please Enter your Address",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editCompanySettings",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "companysettings?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".CompanySettingsInfoStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=CompanySettingsInfoStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});


// Remove 

$(".deleteContactInfo").click(function() {
    var id = $(this).data("token");
    swal({
        title: "Are you sure to delete this contact?",
        text: "Once deleted info will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=deleteContactInfo",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});


/*----------------------------
       Manage Payment Info
 ------------------------------*/

// Add Payment Info

$("#addPaymentInfo").validate({
    rules: {
        name: {
            required: true,
        },
        account_number: {
            required: true,
        },
        bank: {
            required: true,
        },
        ifcs_code: {
            required: true,
        },
        branch: {
            required: true,
        },
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        account_number: {
            required: "Please Enter your Account Number",
        },
        bank: {
            required: "Please Enter your Bank Name",
        },
        ifcs_code: {
            required: "Please Enter your bank IFSC Code",
        },
        branch: {
            required: "Please Enter your bank branch",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addPaymentInfo",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "paymentinfo?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Edit Payment Info

$("#editPaymentInfo").validate({
    rules: {
        name: {
            required: true,
        },
        account_number: {
            required: true,
        },
        bank: {
            required: true,
        },
        ifcs_code: {
            required: true,
        },
        branch: {
            required: true,
        },
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        account_number: {
            required: "Please Enter your Account Number",
        },
        bank: {
            required: "Please Enter your Bank Name",
        },
        ifcs_code: {
            required: "Please Enter your bank IFSC Code",
        },
        branch: {
            required: "Please Enter your bank branch",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editPaymentInfo",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "paymentinfo?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".PaymentInfoStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=PaymentInfoStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Remove 
$(".deletePaymentInfo").click(function() {
    var id = $(this).data("token");
    swal({
        title: "Are you sure to delete this payment info?",
        text: "Once deleted info will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=deletePaymentInfo",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});


/*----------------------------
       Manage Employee Info
 ------------------------------*/

// Add Employee Info

$("#addEmployee").validate({
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
        },
        mobile: {
            required: true,
            digits: true
        },
        password: {
            required: true,
        },
       
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        email: {
            required: "Please Enter your Email",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
        password: {
            required: "Please Enter password",
        },
       
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addEmployee",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "employee?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Edit Employee Info

$("#editEmployee").validate({
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
        },
        mobile: {
            required: true,
            digits: true
        },
        password: {
            required: true,
        },
       
    },
    messages: {
        name: {
            required: "Please Enter your Name",
        },
        email: {
            required: "Please Enter your Email",
        },
        mobile: {
            required: "Please Enter your Mobile Number",
        },
        password: {
            required: "Please Enter password",
        },
       
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editEmployee",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "employee?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".employeeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=employeeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Active & Inactive Status

$(".propertyStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=propertyStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

/*------------------------------
   Permission
--------------------------------*/

// Employee Permission 

$("#employeePermissions").validate({
    rules: {
        emp_name: {
            required: true
        },
    },
    messages: {
        emp_name: {
            required: "Please Enter Employee Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=employeePermissions",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "employee?u=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


//---------------------------- Permission --------------------------------//

    // Customer Permission

    $('#l1').click(function() {
        $('.lead_permission').prop('checked', this.checked);
    });

    $('#c1').click(function() {
        $('.customer_permission').prop('checked', this.checked);
    });

    $('#p1').click(function() {
        $('.property_permission').prop('checked', this.checked);
    });

    $('#a1').click(function() {
        $('.adhoc_permission').prop('checked', this.checked);
    });

    $('#n1').click(function() {
        $('.news_permission').prop('checked', this.checked);
    });

    $('#s1').click(function() {
        $('.setting_permission').prop('checked', this.checked);
    });

    $('#r1').click(function() {
        $('.report_permission').prop('checked', this.checked);
    });   

    $('#m1').click(function() {
        $('.master_permission').prop('checked', this.checked);
    });

    


/*--------------------------------------
        Employee ChangePassword
----------------------------------------*/

// Change password Employee

$('.changepasswordEmployee').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=changepasswordEmployee",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#employee_fkey").val(response['fkey']);
            $("#employee_token").val(response['id']);
            $('.employeeChangePasswordShow').modal('show');
        }
    })
    return false;
});


// Update Employee Change Password

$("#empChangePassword").validate({
    rules: {
        password: {
            required: true,
        },
        confirm_pass: {
            required: true,
            equalTo: "#password"
        }
    },
    messages: {
        password: {
            required: "Please Enter Your New Password",
        },
        confirm_pass: {
            required: "Please Enter Your Confirm Password",
        }
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=empChangePassword",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    $('.employeeChangePasswordShow').modal('hide');
                    $('#empChangePassword').find('input:password').val('');
                    setTimeout(function() {
                        new Noty({
                            text: '<strong>Password Change Successfully</strong>!',
                            type: 'success',
                            theme: 'relax',
                            layout: 'topRight',
                            timeout: 3000
                        }).show();
                    }, 300);
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});


/*=======================================================
                     General Documents
==========================================================*/

// add 

$("#addGendralDocuments").validate({
    rules: {
        title: {
            required: true
        },
        attachment: {
            required: false,
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        attachment: {
            required: "Please Select Document",
        },
    },
    submitHandler: function(form) {
        var attachment = $("#attachment").val();
        var flag = true;
        var formname = document.getElementById('addGendralDocuments');
        var formData = new FormData(formname);            
        //if (attachment != "") {
            if (flag) {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_document_file.php?page=addGendralDocuments",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  core_path + "documents?a=success";
                        } else {
                            $(".form-error").html(data);
                        }
                    }
                });
            }
            return false;
        /*} else {
            swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
        }*/
    }
});

// add 

$("#editGendralDocuments").validate({
    rules: {
        title: {
            required: true
        },
        attachment: {
            required: true,
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        attachment: {
            required: "Please Select Document",
        },
    },
    submitHandler: function(form) {
        var attachment = $("#attachment").val();
        var flag = true;
        var formname = document.getElementById('editGendralDocuments');
        var formData = new FormData(formname);            
        //if (attachment != "") {
            if (flag) {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_document_file.php?page=editGendralDocuments",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  core_path + "documents?e=success";
                        } else {
                            $(".form-error").html(data);
                        }
                    }
                });
            }
            return false;
        /*} else {
            swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
        }*/
    }
});

// Active & Inactive Status

$(".generalDocumentStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=generalDocumentStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

$(".removeGeneralDocument").click(function() {
    var id = $(this).data("token");
    var option = $(this).data("option");
    swal({
        title: "Are you sure want to delete this documnet?",
        text: "Once deleted the document will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removeGeneralDocument",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //$("#pro_" + option).remove();
                    //masonryInt();
                    //window.location = core_path + "product?c=success";
                    location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});


/*----------------------------
       Manage Faqs
 ------------------------------*/

// Add Faqs

$("#addFaqs").validate({
    rules: {
        title: {
            required: true,
        },
        message: {
            required: true,
        },
       
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        message: {
            required: "Please Enter your Message",
        },
              
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addEmployee",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "employee?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".faqStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=faqStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});


/*=======================================================
            Property Broucher
==========================================================*/

// add 

$("#addPropertyBroucher").validate({
    rules: {
        title: {
            required: true
        },
        attachment: {
            required: true,
        },
        project_id: {
            required: true,
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        attachment: {
            required: "Please Select Document",
        },
        project_id: {
            required: "Please Select Project",
        },
    },
    submitHandler: function(form) {
        var attachment = $("#attachment").val();
        var flag = true;
        var formname = document.getElementById('addPropertyBroucher');
        var formData = new FormData(formname);            
        //if (attachment != "") {
            if (flag) {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_document_file.php?page=addPropertyBroucher",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  core_path + "propertybrochure?a=success";
                        } else {
                            $(".form-error").html(data);
                        }
                    }
                });
            }
            return false;
        /*} else {
            swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
        }*/
    }
});

$("#editPropertyBroucher").validate({
    rules: {
        title: {
            required: true
        },
        attachment: {
            required: true,
        },
        project_id: {
            required: true,
        },
    },
    messages: {
        title: {
            required: "Please Enter Title",
        },
        attachment: {
            required: "Please Select Document",
        },
        project_id: {
            required: "Please Select Project",
        },
    },
    submitHandler: function(form) {
        var attachment = $("#attachment").val();
        var flag = true;
        var formname = document.getElementById('editPropertyBroucher');
        var formData = new FormData(formname);            
        //if (attachment != "") {
            if (flag) {
                var content = $(form).serialize();
                $.ajax({
                    url: core_path + "resource/ajax_document_file.php?page=editPropertyBroucher",
                    type: "POST",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data == 1) {
                            window.location =  core_path + "propertybrochure?e=success";
                        } else {
                            $(".form-error").html(data);
                        }
                    }
                });
            }
            return false;
        /*} else {
            swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
        }*/
    }
});


$(".removeBroucher").click(function() {
    var id = $(this).data("token");
    var option = $(this).data("option");
    swal({
        title: "Are you sure to delete this documnet?",
        text: "Once deleted the documnet will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removeBroucher",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //$("#pro_" + option).remove();
                    //masonryInt();
                    //window.location = core_path + "product?c=success";
                    location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});

// Active & Inactive Status

$(".propertyBroucherStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=propertyBroucherStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

/*----------------------------
       Manage Request type
 ------------------------------*/

// Add Request type

$("#addRequestType").validate({
    rules: {
        request_type: {
            required: true,
        },
    },
    messages: {
        request_type: {
            required: "Please Enter Request type",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addRequestType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "settings?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".requestTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=requestTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Edit 

$('.editRequestType').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editRequestType",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".layoutinfo").html(response['layout']);
            $('.requestTypeModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updateRequestType").validate({
    rules: {
        request_type: {
            required: true,
        },
    },
    messages: {
        request_type: {
            required: "Please Enter Request type",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateRequestType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "settings?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});


/*----------------------------
       Manage KYC type
 ------------------------------*/

// Add KYC type

$("#addKYCType").validate({
    rules: {
        kyc_type: {
            required: true,
        },
    },
    messages: {
        kyc_type: {
            required: "Please Enter KYC type",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addKYCType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "settings/kyctype?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Edit 

$('.editkycType').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editkycType",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#editfkey").val(response['fkey']);
            $("#token").val(response['id']);
            $("#kycType_info").val(response['kyc_type']);
            $('.kycTypeModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updatekycType").validate({
    rules: {
        kyc_type: {
            required: true,
        },
    },
    messages: {
        kyc_type: {
            required: "Please Enter KYC type",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updatekycType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    window.location = core_path + "settings/kyctype?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".kycTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=kycTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});


/*----------------------------
    Notification Email
 ------------------------------*/

// Add 

$("#addNotificationEmail").validate({
    rules: {
        email: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter Email",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addNotificationEmail",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "settings/notification?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Edit 

$('.editNotification').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editNotification",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editNotificationInfo").html(response['layout']);
            $('.editNotificationModel').modal('show');

        }
    })
    return false;
});

// Update 

$("#updateNotification").validate({
    rules: {
        email: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter Email",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateNotification",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "settings/notification?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".notificationEmailStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=notificationEmailStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

 /*============================
          Gallery 
==============================*/

// Add 

$("#addGallery").validate({
    rules: {
        album_title: {
            required: true
        },
        sort_order: {
            required: true
        },
        image: {
            required: true
        },
    },
    messages: {
        album_title: {
            required: "Please Enter Title",
        },
        sort_order: {
            required: "Please Enter Sort Order",
        },
        image: {
            required: "Please Select Image",
        },
    },
    submitHandler: function(form) {
        var photo = $("#cimage").val();
        var formname = document.getElementById('addGallery');
        var formData = new FormData(formname);
        if (photo != "") {
            swal({
                title: "Are you sure to add this Gallery?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55 ",
                confirmButtonText: "Yes, I am Sure !",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_post_image.php",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            //alert(data);
                            data = data.split('`');
                            if (data[0] == 1) {
                                $("#newsImage").val(data[1]);
                                if (data[0] == 1) {
                                    var content = $(form).serialize();
                                    $.ajax({
                                        type: "POST",
                                        url: core_path + "resource/ajax_redirect.php?page=addGallery",
                                        dataType: "html",
                                        data: content,
                                        beforeSend: function() {
                                            $(".page_loading").show();
                                        },
                                        success: function(data) {
                                            //alert(data);
                                            $(".page_loading").hide();
                                            if (data == 1) {
                                                window.location.href = core_path + "gallery?a=success";
                                                return true;
                                            } else {
                                                $(".form-error").html(data);
                                                swal.close();
                                            }
                                            return false;
                                        }
                                    });
                                }
                            } else {
                                $(".imgerr").html(data[1]);
                                return false;
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Not Saved Yet", "error");
                }
            });

        } else {
            swal("Please Add Banner image", "Banner image not found, Kindly upload the banner image", "error");
        }
        return false;
    }
});

// Edit 

$("#editGallery").validate({
    rules: {
        album_title: {
            required: true
        },
        sort_order: {
            required: true
        },
        image: {
            required: true
        },
    },
    messages: {
        album_title: {
            required: "Please Enter Title",
        },
        sort_order: {
            required: "Please Enter Sort Order",
        },
        image: {
            required: "Please Select Image",
        },
    },
    submitHandler: function(form) {
        var photo = $("#cimage").val();
        var formname = document.getElementById('editGallery');
        var formData = new FormData(formname);
        if (photo != "") {
            swal({
                title: "Are you sure to add this Gallery?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55 ",
                confirmButtonText: "Yes, I am Sure !",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_post_image.php",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function(data) {
                            //alert(data);
                            data = data.split('`');
                            if (data[0] == 1) {
                                $("#newsImage").val(data[1]);
                                if (data[0] == 1) {
                                    var content = $(form).serialize();
                                    $.ajax({
                                        type: "POST",
                                        url: core_path + "resource/ajax_redirect.php?page=editGallery",
                                        dataType: "html",
                                        data: content,
                                        beforeSend: function() {
                                            $(".page_loading").show();
                                        },
                                        success: function(data) {
                                            //alert(data);
                                            $(".page_loading").hide();
                                            if (data == 1) {
                                                window.location.href = core_path + "gallery?e=success";
                                                return true;
                                            } else {
                                                $(".form-error").html(data);
                                                swal.close();
                                            }
                                            return false;
                                        }
                                    });
                                }
                            } else {
                                $(".imgerr").html(data[1]);
                                return false;
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Not Saved Yet", "error");
                }
            });

        } else {
            swal("Please Add Banner image", "Banner image not found, Kindly upload the banner image", "error");
        }
        return false;
    }
});

// Active & Inactive Status

$(".galleryStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=galleryStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Remove Property Image

$(".removeGalleryImgIcon").click(function() {
    var id = $(this).data("id");
    var type = $(this).data("type");
    swal({
        title: "Are sure you to delete this Image ?",
        text: "You will not be able to recover this Details !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        var value = id + "`" + type;
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=removeGalleryImgIcon",
                dataType: "html",
                data: { result: value },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {

                    $(".page_loading").hide();
                    //alert(data);
                    if (data[0] == 1) {
                        swal("Deleted!", "Image has been deleted.", "success");
                        location.reload();
                    } else {
                        swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                    }
                }
            });
        } else {
            swal("Cancelled", "Image retained", "error");
        }
    });
    return false;
});


/*----------------------------
       Manage Flat Type Master
 ------------------------------*/

// Add Flat Type Master

$("#addFlattypemaster").validate({
    rules: {
        flat_variant: {
            required: true,
        },
    },
    messages: {
        flat_variant: {
            required: "Please Enter Flat Variant",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addFlattypemaster",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "flattype?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Edit Flat Type Master

$("#editFlattypemaster").validate({
    rules: {
        flat_variant: {
            required: true,
        },
    },
    messages: {
        flat_variant: {
            required: "Please Enter Flat Variant",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editFlattypemaster",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "flattype?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".flattypemasterStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=flattypemasterStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});



// Edit Flat Type Master

$(document).on('click', '.editFlatTypeMasterInformation', function() {
    var id = $(this).data("option");
    var type = $(this).data("type");
    var sno = $(this).data("value");
    var value = id + "`" + sno;
    //alert(id);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editFlatTypeMasterInformation",
        dataType: "html",
        //data: { result: id, option: value },
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editFlatTypeInfoModel").html(response['layout']);
            $('.editFlatTypeAttachment').modal('show');
        }
    })
    return false;
});

// update Flat Type Master

$("#updateFlatTypeInfoDetails").validate({
    rules: {
        image: {
            required: true
        }
    },
    messages: {
        image: {
            required: "Please Select Image",
        }
    },
    submitHandler: function(form) {
        var option = $("#option").val();
        $(".page_loading").show();
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateFlatTypeInfoDetails",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                data = data.split('`');
                if (data['0'] == 1) {
                    $('.editFlatTypeAttachment').modal('hide');
                    $("#attacjmentrow_" + option).replaceWith(data['1']);
                    return true;
                } else {
                    $(".perror").html(data['1']);
                }
            }
        });

        return false;
    }
});

// Delete Flat Type Master

$(document).on("click", ".deletFlatTypeInformation", function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    var type = $(this).data("type");
    swal({
        title: "Are sure you to delete this Information ?",
        text: "You will not be able to recover this Details !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        var value = id;
        if (isConfirm) {
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deletFlatTypeInformation",
                dataType: "html",
                data: { result: value },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data[0] == 1) {
                        $("#attacjmentrow_" + id).remove();
                        swal("Deleted!", "Infomation has been deleted.", "success");
                        //location.reload();
                    } else {
                        swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                    }
                }
            });
        } else {
            swal("Cancelled", "Information retained", "error");
        }
    });
    return false;
});


    /*-------------------------------
        Property Private Documents  
    ---------------------------------*/

    // Remove News Files

    $(document).on('click', '.removeAgendaImage', function() {
        $("#hideAgendaDefaultFiles").remove();
        $("#newAgendaUpload").removeClass("display_none");
    });

    // Edit Property Private Documents  

    $(document).on('click', '.uploadDocfile', function() {
        var id = $(this).data("option");
        var type = $(this).data("type");
        var sno = $(this).data("value");
        $('.privateDocModel').modal('hide');
        var property = $(this).data("value");
        var value = id + "`" + sno + "`" + type + "`" + property;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=uploadDocfile",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".edituploadfileModel").html(response['layout']);
                $('.edituploadfile').modal('show');
            }
        })
        return false;
    });

    // update Property Private Documents 

    $("#updateDocumentsFiles").validate({
        rules: {
            image: {
                required: true
            }
        },
        messages: {
            image: {
                required: "Please Select File",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('updateDocumentsFiles');
            var formData = new FormData(formname);
            var option = $("#option").val();
            $(".page_loading").show();
            var content = $(form).serialize();
            $.ajax({
                url: core_path + "resource/ajax_propertydocument_file.php",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    //alert(data);
                    data = data.split('`');
                    if (data[0] == 1) {
                        $("#customerImage").val(data[1]);
                        if (data[0] == 1) {
                            var content = $(form).serialize();
                            $.ajax({
                                type: "POST",
                                url: core_path + "resource/ajax_redirect.php?page=updateDocumentsFiles",
                                dataType: "html",
                                data: content,
                                beforeSend: function() {
                                    $(".page_loading").show();
                                },
                                success: function(data) {
                                    //alert(data);
                                    $(".page_loading").hide();
                                    data = data.split('`');
                                    if (data['0'] == 1) {
                                        $('.edituploadfile').modal('hide');
                                        setTimeout(function() {
                                            new Noty({
                                                text: '<strong>Document updated successfully</strong>!',
                                                type: 'success',
                                                theme: 'relax',
                                                layout: 'topRight',
                                                timeout: 3000
                                            }).show();
                                        }, 300);
                                        setTimeout(function(){// wait for 5 secs(2)
                                               location.reload(); // then reload the page.(3)
                                        }, 800); 

                                        // $("#agendamentrow_" + option).replaceWith(data['1']);
                                        //location.reload();
                                        return true;
                                    } else {
                                        $(".perror").html(data['1']);
                                    }
                                    return false;
                                }
                            });
                        }
                    } else {
                        $(".imgerr").html(data[1]);
                        return false;
                    }
                }
            });

            return false;
        }
    });

    // Delete Property Private Documents 

    $(document).on("click", ".deletPropertyDocInformation", function() {
        var id = $(this).data("option");
        swal({
            title: "Are sure you to delete this File ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletPropertyDocInformation",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            //$("#attacjmentrow_" + id).remove();
                            //swal("Deleted!", "Infomation has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "File retained", "error");
            }
        });
        return false;
    });

    /*-------------------------------
        Property Genderal Private Documents  
    ---------------------------------*/

    // Edit Property Genderal Private Documents  

    $(document).on('click', '.uploadGenDocfile', function() {
        var id = $(this).data("option");
        var type = $(this).data("type");
        var sno = $(this).data("value");
        var property = $(this).data("value");
        $('.privateDocModel').modal('hide');
        var value = id + "`" + sno + "`" + type + "`" + property;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=uploadGenDocfile",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".editgenuploadfileModel").html(response['layout']);
                $('.editgenuploadfile').modal('show');
            }
        })
        return false;
    });

    // update Property Genderal Private Documents 

    $("#updateGenDocumentsFiles").validate({
        rules: {
            image: {
                required: true
            }
        },
        messages: {
            image: {
                required: "Please Select File",
            }
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            var formname = document.getElementById('updateGenDocumentsFiles');
            var formData = new FormData(formname);
            var option = $("#option").val();
            $(".page_loading").show();
            var content = $(form).serialize();
            $.ajax({
                url: core_path + "resource/ajax_propertydocument_file.php",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    //alert(data);
                    data = data.split('`');
                    if (data[0] == 1) {
                        $("#customerImage").val(data[1]);
                        if (data[0] == 1) {
                            var content = $(form).serialize();
                            $.ajax({
                                type: "POST",
                                url: core_path + "resource/ajax_redirect.php?page=updateGenDocumentsFiles",
                                dataType: "html",
                                data: content,
                                beforeSend: function() {
                                    $(".page_loading").show();
                                },
                                success: function(data) {
                                    //alert(data);
                                    $(".page_loading").hide();
                                    data = data.split('`');
                                    if (data['0'] == 1) {
                                        $('.editgenuploadfile').modal('hide');
                                        setTimeout(function() {
                                            new Noty({
                                                text: '<strong>Document updated successfully</strong>!',
                                                type: 'success',
                                                theme: 'relax',
                                                layout: 'topRight',
                                                timeout: 3000
                                            }).show();
                                        }, 300);
                                        setTimeout(function(){// wait for 5 secs(2)
                                               location.reload(); // then reload the page.(3)
                                        }, 800); 
                                        // $("#agendamentrow_" + option).replaceWith(data['1']);
                                        //location.reload();
                                        return true;
                                    } else {
                                        $(".perror").html(data['1']);
                                    }
                                    return false;
                                }
                            });
                        }
                    } else {
                        $(".imgerr").html(data[1]);
                        return false;
                    }
                }
            });

            return false;
        }
    });

    // Delete Property Genderal Private Documents 

    $(document).on("click", ".deletPropertyGenDocInformations", function() {
        var id = $(this).data("option");
        swal({
            title: "Are sure you to delete this File ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletPropertyGenDocInformations",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            //$("#attacjmentrow_" + id).remove();
                            //swal("Deleted!", "Infomation has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "File retained", "error");
            }
        });
        return false;
    });

    /*------------------------------------------------
            Add Gendral Property Documnets
    -----------------------------------------------*/

    // add 

    $("#addPropertyGendralDocuments").validate({
        rules: {
            title: {
                required: true
            },
            attachment: {
                required: false,
            },
        },
        messages: {
            title: {
                required: "Please Enter Title",
            },
            attachment: {
                required: "Please Select Document",
            },
        },
        submitHandler: function(form) {
            var attachment = $("#attachment").val();
            var property_id = $("#property_id").val();
            var flag = true;
            var formname = document.getElementById('addPropertyGendralDocuments');
            var formData = new FormData(formname);            
            //if (attachment != "") {
                if (flag) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_document_file.php?page=addPropertyGendralDocuments",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location =  core_path + "property/details/"+ property_id +"?a=success";
                                //location.reload();
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                }
                return false;
            /*} else {
                swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
            }*/
        }
    });

    /*=====================================
            Adhoc requests
    =======================================*/

    // Adhoc Request

    // Add Adhoc Request

    $("form[name='replayAdhocRequest']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_adhocreply_multiple_file.php?page=replayAdhocRequest",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "adhoc?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });


    $("#replayAdhocRequest_old").validate({
        rules: {
            remarks: {
                required: true,
            },
            
        },
        messages: {
            remarks: {
                required: "Please Enter your message",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=replayAdhocRequest",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "adhoc?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Close Ticket

    $(".closeTicket").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=closeTicket",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });


    /*----------------------------
       Menu Settings
    ------------------------------*/

    // User Login

    $("#virtualtoursettings").validate({
        rules: {
            virtual_settings: {
                required: true,
            }
        },
        messages: {
            virtual_settings: {
                required: "Please Select ",
            },
           
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=virtualtoursettings",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                         window.location = core_path + "settings/virtualtour?a=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    /*=====================================
        kyc
    =======================================*/

    // View kyc 

    $(document).on('click', '.approverejectkyc', function() {
        var id = $(this).data("option");
        var status = $(this).data("value");
        var value = id + '`' + status ;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=approverejectkyc",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);  
                $('#kycModel').modal('show');                 
                $(".name").html(response['name']);
                $("#fkey").val(response['fkey']);
                $("#kyc_status").val(response['kyc_status']);
                $("#token").val(response['token']);
                $('.kycModel').modal('show'); 
                
            }
        })
        return false;
    });

    $("#updateapproverejectkyc").validate({
        rules: {
            remarks: {
                required: true,
            }
        },
        messages: {
            remarks: {
                required: "Please Enter Remarks ",
            },
           
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
           // alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateapproverejectkyc",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $('.kycModel').modal('hide');
                    $(".page_loading").hide();
                    if (data == 1) {
                         window.location = core_path + "customer/kyc?a=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });


    // Update Complete Status 

    

    $(".updateCompletedStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateCompletedStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    $(".moveLeadToCustomer_old").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=moveLeadToCustomer",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                   window.location = core_path + "lead?ca=success";
                } else {
                   $(".form-error").html(data);
                }
            }
        });
        return false;
    });

    $('.moveLeadToCustomer').click(function() {
        var id = $(this).data("option");
        // alert(id);
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateLeadToCustomerModel",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#movetoken").val(response['id']);
                $("#movefkey").val(response['fkey']);
                $('.moveLeadToCustomerModel').modal('show');

            }
        })
        return false;
    });

    $("#moveLeadToCustomer").validate({
        rules: {
            moveremarks: {
                required: true,
            },
        },
        messages: {
            moveremarks: {
                required: "Please Enter Remarks",
            },
        },
        submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=moveLeadToCustomer",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?ca=success";
                } else {
                    $(".form-errors").html(data);
                }
            }
        });
        return false;
    }
    });

    /*=======================================================
                         Add Invoice
    ==========================================================*/

    // add Invoice

    $("#addInvoice").validate({
        rules: {
            inv_number: {
                required: true
            },
            inv_date: {
                required: true,
            },
            type: {
                required: true,
            },
            raised_to: {
                required: true,
            },
            attachment: {
                required: true,
            },
            remarks: {
                required: true,
            },
        },
        messages: {
            inv_number: {
                required: "Please Enter Invoice / Receipt No",
            },
            inv_date: {
                required: "Please Enter Invoice / Receipt Date",
            },
            type: {
                required: "Please Enter Title",
            },
            attachment: {
                required: "Please Select Document",
            },
            raised_to: {
                required: "Please Enter Title",
            },
            remarks: {
                required: "Please Enter Remarks",
            },
        },
        submitHandler: function(form) {
            var attachment = $("#attachment").val();
            var flag = true;
            var formname = document.getElementById('addInvoice');
            var formData = new FormData(formname);            
            //if (attachment != "") {
                if (flag) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_document_file.php?page=addInvoice",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location =  core_path + "invoice?a=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                }
                return false;
            /*} else {
                swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
            }*/
        }
    });

    // add Invoice

    $("#editInvoice").validate({
        rules: {
            inv_number: {
                required: true
            },
            inv_date: {
                required: true,
            },
            type: {
                required: true,
            },
            raised_to: {
                required: true,
            },
            attachment: {
                required: true,
            },
            remarks: {
                required: true,
            },
        },
        messages: {
            inv_number: {
                required: "Please Enter Invoice / Receipt No",
            },
            inv_date: {
                required: "Please Enter Invoice / Receipt Date",
            },
            type: {
                required: "Please Enter Title",
            },
            attachment: {
                required: "Please Select Document",
            },
            raised_to: {
                required: "Please Enter Title",
            },
            remarks: {
                required: "Please Enter Remarks",
            },
        },
        submitHandler: function(form) {
            var attachment = $("#attachment").val();
            var flag = true;
            var formname = document.getElementById('editInvoice');
            var formData = new FormData(formname);            
            //if (attachment != "") {
                if (flag) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_document_file.php?page=editInvoice",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location =  core_path + "invoice?e=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                }
                return false;
            /*} else {
                swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
            }*/
        }
    });

    $(".removeInvoiceFile").click(function() {
    var id = $(this).data("token");
    var option = $(this).data("option");
    swal({
        title: "Are you sure to delete this file?",
        text: "Once deleted the file will not be recovered!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4caf50",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=removeInvoiceFile",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //$("#pro_" + option).remove();
                    //masonryInt();
                    //window.location = core_path + "product?c=success";
                    location.reload();
                } else {
                    $(".form-error").show();
                    $(".form-error").html(data);
                }
            }
        });
        } else {
            swal("Cancelled", "not saved yet", "error");
        }
    });
    return false;
});

    /*--------------------
          Adhoc Report
    --------------------*/

    $("#adhocReport").validate({
        rules: {
            from_date: {
                required: true
            },
            to_date: {
                required: true
            },
        },
        messages: {
            from_date: {
                required: "Please Enter From Date",
            },
            to_date: {
                required: "Please Enter To Date",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=adhocReport",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "reports/adhocreport/" + response[0] + "/" + response[1] + "&customer_id=" + response[2] + "?status=" + response[3];
                }
            });
            return false;
        }
    });

    /*--------------------
          Lead Report
    --------------------*/

    $("#leadReport").validate({
        rules: {
            from_date: {
                required: true
            },
            to_date: {
                required: true
            },
        },
        messages: {
            from_date: {
                required: "Please Enter From Date",
            },
            to_date: {
                required: "Please Enter To Date",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=leadReport",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "reports/leadreport/" + response[0] + "/" + response[1] + "&employee_id=" + response[2] + "?leadstatus=" + response[3];
                }
            });
            return false;
        }
    });

     /*--------------------
          Customer Report
    --------------------*/

    $("#customerReport").validate({
        rules: {
            from_date: {
                required: true
            },
            to_date: {
                required: true
            },
        },
        messages: {
            from_date: {
                required: "Please Enter From Date",
            },
            to_date: {
                required: "Please Enter To Date",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=customerReport",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "reports/customerreport/" + response[0] + "/" + response[1] + "&customer_id=" + response[2] + "&priority=" + response[3] + "&kyc_status=" + response[4];
                }
            });
            return false;
        }
    });

     /*--------------------
          KYC Report
    --------------------*/

    $("#kycReport").validate({
        rules: {
            from_date: {
                required: true
            },
            to_date: {
                required: true
            },
        },
        messages: {
            from_date: {
                required: "Please Enter From Date",
            },
            to_date: {
                required: "Please Enter To Date",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=kycReport",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "reports/kycreport/" + response[0] + "/" + response[1] + "&customer_id=" + response[2] + "&kyc_status=" + response[3];
                }
            });
            return false;
        }
    });


/*----------------------------
       Manage Activity type
 ------------------------------*/

// Add Activity type

$("#addActivityType").validate({
    rules: {
        activity_type: {
            required: true,
        },
        act_date: {
            required: true,
        },
    },
    messages: {
        activity_type: {
            required: "Please Enter Activity type",
        },
        act_date: {
            required: "Please Select Activity Date",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addActivityType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead/activetype?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".activityTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=activityTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Edit 

$('.editActivityType').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editActivityType",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#editfkey").val(response['fkey']);
            $("#token").val(response['id']);
            $("#activitytype_info").val(response['activity_type']);
            $('.activityTypeModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updateActivityType").validate({
    rules: {
        activity_type: {
            required: true,
        },
    },
    messages: {
        activity_type: {
            required: "Please Enter Activity type",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateActivityType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "lead/activetype?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

/*----------------------------
    Lead Status Message
 ------------------------------*/

// Add 

$("#addLeadType").validate({
    rules: {
        lead_type: {
            required: true,
        },
    },
    messages: {
        lead_type: {
            required: "Please Enter Lead Status Message",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addLeadType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead/leadtype?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Edit 

$('.editLeadType').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editLeadType",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editLeadInfo").html(response['layout']);
            $('.editLeadModel').modal('show');

        }
    })
    return false;
});

// Update 

$("#updateLeadType").validate({
    rules: {
        lead_type: {
            required: true,
        },
    },
    messages: {
        lead_type: {
            required: "Please Enter Lead Status Message",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateLeadType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "lead/leadtype?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".leadTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=leadTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

/*----------------------------
    Lead Source
 ------------------------------*/

// Add 

$("#addLeadSource").validate({
    rules: {
        lead_source: {
            required: true,
        },
    },
    messages: {
        lead_source: {
            required: "Please Enter Lead Source",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addLeadSource",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead/leadsource?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Edit 

$('.editLeadSource').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editLeadSource",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editLeadInfo").html(response['layout']);
            $('.editLeadModel').modal('show');

        }
    })
    return false;
});

// Update 

$("#updateLeadSource").validate({
    rules: {
        lead_source: {
            required: true,
        },
    },
    messages: {
        lead_source: {
            required: "Please Enter Lead Source",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateLeadSource",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "lead/leadsource?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".leadSourceStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=leadSourceStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

/*----------------------------
   Customer Profile
 ------------------------------*/

// Add 

$("#addCustomerProfile").validate({
    rules: {
        customer_profile: {
            required: true,
        },
    },
    messages: {
        customer_profile: {
            required: "Please Enter Lead Profile Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addCustomerProfile",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead/customerprofile?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Edit 

$('.editCustomerProfile').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editCustomerProfile",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".editCustomerProfileInfo").html(response['layout']);
            $('.editCustomerProfileModel').modal('show');

        }
    })
    return false;
});

// Update 

$("#updateCustomerProfile").validate({
    rules: {
        customer_profile: {
            required: true,
        },
    },
    messages: {
        customer_profile: {
            required: "Please Enter Lead Profile Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateCustomerProfile",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "lead/customerprofile?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".customerProfileStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=customerProfileStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});



/*----------------------------
        Lead
 ------------------------------*/

// Lead

$("#addLead").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            digits: true
        },
        fname: {
            required: true,
        },
        leadsource: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter your Primary  Email Address",
        },
        mobile: {
            required: "Please Enter your Primary  Mobile Number",
        },
        fname: {
            required: "Please Enter your First Name",
        },
        leadsource: {
            required: "Please Enter your Lead Source",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addLead",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Customer

$("#editLead").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        mobile: {
            required: true,
            digits: true
        },
        fname: {
            required: true,
        },
        leadsource: {
            required: true,
        },
    },
    messages: {
        email: {
            required: "Please Enter your Primary  Email Address",
        },
        mobile: {
            required: "Please Enter your Primary  Mobile Number",
        },
        fname: {
            required: "Please Enter your First Name",
        },
        leadsource: {
            required: "Please Enter your Lead Source",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editLead",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               // alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?e=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


// Active & Inactive Status

$(".leadStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=leadStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Lead Status Model 

$('.leadStatusModel').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=leadStatusModel",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".layoutModel").html(response['layout']);
            $('.updateLeadStatusmodel').modal('show');

        }
    })
    return false;
});

// Assign Lead 

$('.assignLeadModel').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=assignLeadModel",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#token1").val(response['id']);
            $("#fkey1").val(response['fkey']);
            $("#activityName1").html(response['name']);
            $("#activityMobile1").html(response['mobile']);
            $("#activityEmail1").html(response['email']);
            $('.addAssignLeadModel').modal('show');

        }
    })
    return false;
});

$("#assignLead").validate({
    rules: {
        employee_id: {
            required: true
        },
        remarks: {
            required: true
        },
        lead_name: {
            required: false
        },
        mobile: {
            required: false
        },
        email: {
            required: false
        },
    },
    messages: {
        employee_id: {
            required: "Select a Employee",
        },
        remarks: {
            required: "Enter Assign Lead Remarks",
        },
        lead_name: {
            required: "Enter Lead Name",
        },
        mobile: {
            required: "Enter Mobile",
        },
        email: {
            required: "Enter Email",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=assignLead",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?as=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Reassign Call 

$('.reassignLeadModel').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=reassignLeadModel",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $(".layout").html(response['layout']);
            $('.ReassignLeadModel').modal('show');
        }
    })
    return false;
});

$("#reassignLead").validate({
    rules: {
        employee_id: {
            required: true
        },
        remarks: {
            required: true
        },
        lead_name: {
            required: true
        },
        mobile: {
            required: true
        },
        email: {
            required: true
        },
    },
    messages: {
        employee_id: {
            required: "Select a Employee",
        },
        remarks: {
            required: "Enter Assign Lead Remarks",
        },
        lead_name: {
            required: "Enter Lead Name",
        },
        mobile: {
            required: "Enter Mobile",
        },
        email: {
            required: "Enter Email",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=reassignLead",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?rs=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

/*-----------------------------------------
            Lead Activity
-------------------------------------------*/

$('.addActivityModel').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=addActivityModel",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#token").val(response['id']);
            $("#fkey").val(response['fkey']);
            $("#activityName").html(response['name']);
            $("#activityMobile").html(response['mobile']);
            $("#activityEmail").html(response['email']);
            $('.editLeadModel').modal('show');

        }
    })
    return false;
});

// Add Activity 

$("#addActivity").validate({
    rules: {
        lead_name: {
            required: true
        },
        act_date: {
            required: true
        },
        mobile: {
            required: true
        },
        email: {
            required: true
        },
        activity_id: {
            required: true
        },
        remarks: {
            required: true
        },
    },
    messages: {
        lead_name: {
            required: "Enter Lead Name",
        },
        act_date: {
            required: "Enter Activity Date",
        },
        mobile: {
            required: "Enter Mobile",
        },
        email: {
            required: "Enter Email",
        },
        activity_id: {
            required: "Select Activity",
        },
        remarks: {
            required: "Enter Remarks",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addActivity",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               // alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead?aa=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

$("#addleadActivity").validate({
    rules: {
        lead_name: {
            required: true
        },
        act_date: {
            required: true
        },
        mobile: {
            required: true
        },
        email: {
            required: true
        },
        activity_id: {
            required: true
        },
        remarks: {
            required: true
        },
    },
    messages: {
        lead_name: {
            required: "Enter Lead Name",
        },
        act_date: {
            required: "Enter Activity Date",
        },
        mobile: {
            required: "Enter Mobile",
        },
        email: {
            required: "Enter Email",
        },
        activity_id: {
            required: "Select Activity",
        },
        remarks: {
            required: "Enter Remarks",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        var id = $("#token").val();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addleadActivity",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
               // alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "lead/details/" + id + "?la=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});


/*-------------------------------------
       Assign Management
--------------------------------------*/

// Change Employee

$(document).on('click', '.change_customer_reassign', function() {
    $("#dropdown_employee").attr("disabled", false);
    $("input[type=text], text").val("");
    //$("#selected-assistants").remove();
    $('#selected-assistants').addClass("display_none");
    $("#dropdown_employee").show().focus();
});


$(document).on('click', '.change_employee', function() {
    $("#employee_info").addClass("display_none");
    $("input[type=text], text").val("");
    $("#dropdown_employee").show().focus();
});


/*----------------------------
       Manage Project type
 ------------------------------*/

// Add Project type

$("#addProjectType").validate({
    rules: {
        project_type: {
            required: true,
        },
       
    },
    messages: {
        project_type: {
            required: "Please Enter Project Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        //alert(content);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=addProjectType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                    window.location = core_path + "settings/project?a=success";
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    }
});

// Active & Inactive Status

$(".projectTypeStatus").click(function() {
    var value = $(this).data("option");
    //alert(value);
    $.ajax({
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=projectTypeStatus",
        dataType: "html",
        data: { result: value },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            $(".page_loading").hide();
            if (data == 1) {
                //alert(data);
                location.reload();
            } else {
                bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
            }
        }
    });
    return false;
});

// Edit 

$('.editProjectType').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editProjectType",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#editfkey").val(response['fkey']);
            $("#token").val(response['id']);
            $("#projectname_info").val(response['project_name']);
            $('.projectTypeModel').modal('show');
        }
    })
    return false;
});

// Update 

$("#updateProjectType").validate({
    rules: {
        project_type: {
            required: true,
        },
       
    },
    messages: {
        project_type: {
            required: "Please Enter Project Name",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateProjectType",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                   window.location = core_path + "settings/project?e=success";
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

/*=====================================
        Update Lead Status
=======================================*/
//7065970659
// Update 

$("#updateLeadStatus").validate({
    rules: {
        leadstatus_id: {
            required: true,
        },
       
    },
    messages: {
        leadstatus_id: {
            required: "Please Select Lead Status",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        var id = $("#token").val();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateLeadStatus",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    if (id=='') {
                        location.reload();
                    }else{
                        window.location = core_path + "lead/details/" + id  +  "?a=success";
                    }
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

// Edit 

$('.editleadactivity').click(function() {
    var id = $(this).data("option");
    var option = $(this).data("value");
    $.ajax({                  
        type: "POST",
        url: core_path + "resource/ajax_redirect.php?page=editleadactivity",
        dataType: "html",
        data: { result: id },
        beforeSend: function() {
            $(".page_loading").show();
        },
        success: function(data) {
            //alert(data);
            $(".page_loading").hide();
            var response = $.parseJSON(data);
            $("#token").val(response['id']);
            $(".layout").html(response['layout']);
            $('.editactivity_model').modal('show');
            reintdatepicker();
        }
    })
    return false;
});


function reintdatepicker() {
   NioApp.Picker.date('.date-picker');
}

// Update 

$("#updateLeadActivity").validate({
    rules: {
        lead_name: {
            required: true
        },
        act_date: {
            required: true
        },
        mobile: {
            required: true
        },
        email: {
            required: true
        },
        activity_id: {
            required: true
        },
        remarks: {
            required: true
        },
    },
    messages: {
        lead_name: {
            required: "Enter Lead Name",
        },
        act_date: {
            required: "Enter Activity Date",
        },
        mobile: {
            required: "Enter Mobile",
        },
        email: {
            required: "Enter Email",
        },
        activity_id: {
            required: "Select Activity",
        },
        remarks: {
            required: "Enter Remarks",
        },
    },
    submitHandler: function(form) {
        var content = $(form).serialize();
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=updateLeadActivity",
            dataType: "html",
            data: content,
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var responce = $.parseJSON(data);
                if (responce['status'] == 1) {
                    //window.location = core_path + "lead/details?e=success";
                    location.reload();
                } else {
                    $(".form-error").html(responce['msg']);
                }
            }
        });
        return false;
    }
});

    /*--------------------------------------------- 
                Data Import Management
    ----------------------------------------------*/

    // Import Excel Sheet

    $("#uploadExcel").on('submit', (function(e) {
        e.preventDefault(); 
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_redirect.php?page=uploadExcel",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    data = data.split('`');
                    if (data[0] == 1) {
                        $(".page_loading").hide();
                        var value = data[1] + "`" + data[2];
                        $.ajax({
                            type: "POST",
                            url: core_path + "resource/ajax_redirect.php?page=saveExcelSession",
                            dataType: "html",
                            data: "element=" + value,
                            success: function(data) {
                                $(".page_loading").hide();
                                //alert(data);
                                if (data == 1) {
                                    window.location.href = core_path + "importdata?r=success";
                                    return true;
                                } else {
                                    $(".errmsgs").html(data);
                                }
                                return false;
                            }
                        });
                    } else {
                        $(".page_loading").hide();
                        $(".errmsgs").html(data[1]);
                        return false;
                    }
                }
            });
        }
        return false;
    }));


    // Delete Import data

    $(".deleteImportDatass").click(function() {
        input = $(this).attr("name");
        swal({
            title: "Are you sure to remove the uploaded data and the Excel File ?",
            text: "Once Removed cannot be replaced!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deleteImportData",
                dataType: "html",
                data: "element=" + input,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                       swal("Deleted!", "Imported Info Removed Successfully !!", "success");
                       location.reload();
                    } else {
                        swal("Cancelled", "Error Occured Please try Again.", "error");
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    // Approve Import Excell Sheet Data

    $(".approveImportData").click(function() {
        var token = $("#token").val();
        swal({
            title: "Are you sure to Approve the Imported Data Present Below ?",
            text: "Once Approved the Data will be published.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Approve it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=approveImportData",
                dataType: "html",
                 data: "element=" + token,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location.href = core_path + "importdata?a=success";
                       return true;
                    } else {
                        $(".errmsgs").html(data);
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    // Delete Single Row Import data

    $(".deleteSingleRowImportData").click(function() {
        input = $(this).attr("name");
        swal({
            title: "Are you sure to remove the Selected Row ?",
            text: "Once Removed cannot be replaced!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deleteSingleRowImportData",
                dataType: "html",
                data: "element=" + input,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                       location.reload();
                    } else {
                        swal("Cancelled", "Error Occured Please try Again.", "error");
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    /*--------------------------------------------- 
             Customer Data Import Management
    ----------------------------------------------*/

    // Import Excel Sheet

    $("#uploadCustomerExcel").on('submit', (function(e) {
        e.preventDefault(); 
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_redirect.php?page=uploadCustomerExcel",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    data = data.split('`');
                    if (data[0] == 1) {
                        $(".page_loading").hide();
                        var value = data[1] + "`" + data[2];
                        $.ajax({
                            type: "POST",
                            url: core_path + "resource/ajax_redirect.php?page=saveCustomerExcelSession",
                            dataType: "html",
                            data: "element=" + value,
                            success: function(data) {
                                $(".page_loading").hide();
                                //alert(data);
                                if (data == 1) {
                                    window.location.href = core_path + "importcustomerdata?r=success";
                                    return true;
                                } else {
                                    $(".errmsgs").html(data);
                                }
                                return false;
                            }
                        });
                    } else {
                        $(".page_loading").hide();
                        $(".errmsgs").html(data[1]);
                        return false;
                    }
                }
            });
        }
        return false;
    }));


    // Delete Import data

    $(".deleteCustomerImportDatass").click(function() {
        input = $(this).attr("name");
        swal({
            title: "Are you sure to remove the uploaded data and the Excel File ?",
            text: "Once Removed cannot be replaced!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deleteCustomerImportDatass",
                dataType: "html",
                data: "element=" + input,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                       swal("Deleted!", "Imported Info Removed Successfully !!", "success");
                       location.reload();
                    } else {
                        swal("Cancelled", "Error Occured Please try Again.", "error");
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    // Approve Import Excell Sheet Data

    $(".approveCustomerImportData").click(function() {
        var token = $("#token").val();
        swal({
            title: "Are you sure to Approve the Imported Data Present Below ?",
            text: "Once Approved the Data will be published.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Approve it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=approveCustomerImportData",
                dataType: "html",
                 data: "element=" + token,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location.href = core_path + "importcustomerdata?a=success";
                       return true;
                    } else {
                        $(".errmsgs").html(data);
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    // Delete Single Row Import data

    $(".deleteCustomerSingleRowImportData").click(function() {
        input = $(this).attr("name");
        swal({
            title: "Are you sure to remove the Selected Row ?",
            text: "Once Removed cannot be replaced!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=deleteCustomerSingleRowImportData",
                dataType: "html",
                data: "element=" + input,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                       location.reload();
                    } else {
                        swal("Cancelled", "Error Occured Please try Again.", "error");
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });


    /*------------------------------
            Delete
    --------------------------------*/

    // Delete 

    $(document).on("click", ".deletGeneralDocList", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletGeneralDocList",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

    // Edit 

    $('.undoGeneralDocList').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoGeneralDocList",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateGeneralDocList").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateGeneralDocList",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });


    /*-----------------------
        Check List
    ------------------------*/

    // Publish 

    $(document).on("click", ".publishCheckListDoc", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to publish this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, publish it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=publishCheckListDoc",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            window.location.href = core_path + "documents/project?p=success";
                            return true;
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

    // Delete 

    $(document).on("click", ".deletCheckListDoc", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletCheckListDoc",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoDocCheckList').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoDocCheckList",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateDocCheckList").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateDocCheckList",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/checklist?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*-----------------------------
            LEAD
    -------------------------------*/

     // Delete 

    $(document).on("click", ".deletLead", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletLead",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoLead').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoLead",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateLead").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateLead",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/leads?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*------------------------------
            iNVOICE
    --------------------------------*/

    // Delete 

    $(document).on("click", ".deleteInvoice", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deleteInvoice",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoInvoice').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoInvoice",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateInvoice").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateInvoice",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/invoice?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*------------------------------
            Gallery
    --------------------------------*/

    // Delete 

    $(document).on("click", ".deleteGallery", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deleteGallery",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoGallery').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoGallery",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateGallery").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateGallery",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/gallery?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*------------------------------
            Property Brochure
    --------------------------------*/

    // Delete 

    $(document).on("click", ".deletePropertyBrochure", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletePropertyBrochure",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoPropertyBrochure').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoPropertyBrochure",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updatePropertyBrochure").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updatePropertyBrochure",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/brochure?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*-----------------------------
            PROPERTY
    -------------------------------*/

     // Delete 

    $(document).on("click", ".deletProperty", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deletProperty",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            //location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoProperty').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoProperty",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateProperty").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateProperty",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/property?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });



    /*------------------------------
            News
    --------------------------------*/

    // Delete 

    $(document).on("click", ".deleteNews", function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Information ?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=deleteNews",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        //alert(data);
                        $(".page_loading").hide();
                        if (data[0] == 1) {
                            $("#attacjmentrow_" + id).remove();
                            swal("Deleted!", "Infomation has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Information retained", "error");
            }
        });
        return false;
    });

     // Edit 

    $('.undoNews').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=undoNews",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".editRestoredInfo").html(response['layout']);
                $('.editRestoredModel').modal('show');

            }
        })
        return false;
    });

    // Update 

    $("#updateNews").validate({
        rules: {
            restored_remarks: {
                required: true,
            },
        },
        messages: {
            restored_remarks: {
                required: "Please Enter Remaks",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateNews",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "trash/news?a=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*============================
              News 
    ==============================*/

    // Add 

    $("#addnews").validate({
        rules: {
            title: {
                required: true
            },
            date: {
                required: true
            },
            
        },
        messages: {
            title: {
                required: "Please Enter Title",
            },
            date: {
                required: "Please Enter Date",
            },
            
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
            if($('#com-email-1').prop('checked')) {
                var title_text = "Are you sure to post the news and notifiy the customers?";
                var text_content = "Once confirmed the news will be sent via email to the customer and cannot be edited further";
            } else {
                var title_text = "Are you sure to post the news ?";
                var text_content = "";
            }


            var formname = document.getElementById('addnews');
            var formData = new FormData(formname);
            /*if (photo != "") {*/
                swal({
                    title: title_text,
                    text: text_content,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55 ",
                    confirmButtonText: "Yes, I am Sure !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        var content = $(form).serialize();
                       // alert(content);
                        $.ajax({
                            url: core_path + "resource/ajax_post_image.php",
                            type: "POST",
                            data: formData,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function(data) {
                                //alert(data);
                                data = data.split('`');
                                if (data[0] == 1) {
                                    $("#newsImage").val(data[1]);
                                    if (data[0] == 1) {
                                        var content = $(form).serialize();
                                        $.ajax({
                                            type: "POST",
                                            url: core_path + "resource/ajax_redirect.php?page=addnews",
                                            dataType: "html",
                                            data: content,
                                            beforeSend: function() {
                                                $(".page_loading").show();
                                            },
                                            success: function(data) {
                                                //alert(data);
                                                $(".page_loading").hide();
                                                if (data == 1) {
                                                    window.location.href = core_path + "news?a=success";
                                                    return true;
                                                } else {
                                                    $(".form-error").html(data);
                                                    swal.close();
                                                }
                                                return false;
                                            }
                                        });
                                    }
                                } else {
                                    $(".imgerr").html(data[1]);
                                    return false;
                                }
                            }
                        });
                    } else {
                        swal("Cancelled", "Not Saved Yet", "error");
                    }
                });

            /*} else {
                swal("Please Add image", "Banner image not found, Kindly upload the banner image", "error");
            }*/
            return false;
        }
    });


    // Active & Inactive Status

    $(".newsStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=newsStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    // Remove Property Image

    $(".removeNewsImgIcon").click(function() {
        var id = $(this).data("id");
        var type = $(this).data("type");
        swal({
            title: "Are sure you to delete this Image ?",
            text: "You will not be able to recover this Details !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            var value = id + "`" + type;
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=removeNewsImgIcon",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {

                        $(".page_loading").hide();
                        //alert(data);
                        if (data[0] == 1) {
                            swal("Deleted!", "Image has been deleted.", "success");
                            location.reload();
                        } else {
                            swal("Error Occurred", "Sorry Unexpected Error Occurred. Please Try Again.", "error");
                        }
                    }
                });
            } else {
                swal("Cancelled", "Image retained", "error");
            }
        });
        return false;
    });

    // Edit 

    $("#editnews").validate({
        rules: {
            title: {
                required: true
            },
            date: {
                required: true
            },
            
        },
        messages: {
            title: {
                required: "Please Enter Title",
            },
            date: {
                required: "Please Enter Date",
            },
            
        },
        submitHandler: function(form) {
            var photo = $("#cimage").val();
             if($('#com-email-1').prop('checked')) {
                var title_text = "Are you sure to update the news and notifiy the customers?";
                var text_content = "Once confirmed the news will be sent via email to the customer and cannot be edited further";
            } else {
                var title_text = "Are you sure to update the news ?";
                var text_content = "";
            }

            var formname = document.getElementById('editnews');
            var formData = new FormData(formname);
           /* if (photo != "") {*/
                swal({
                    title: title_text,
                    text: text_content,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55 ",
                    confirmButtonText: "Yes, I am Sure !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        var content = $(form).serialize();
                        $.ajax({
                            url: core_path + "resource/ajax_post_image.php",
                            type: "POST",
                            data: formData,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function(data) {
                                //alert(data);
                                data = data.split('`');
                                if (data[0] == 1) {
                                    $("#newsImage").val(data[1]);
                                    if (data[0] == 1) {
                                        var content = $(form).serialize();
                                        $.ajax({
                                            type: "POST",
                                            url: core_path + "resource/ajax_redirect.php?page=editnews",
                                            dataType: "html",
                                            data: content,
                                            beforeSend: function() {
                                                $(".page_loading").show();
                                            },
                                            success: function(data) {
                                                //alert(data);
                                                $(".page_loading").hide();
                                                if (data == 1) {
                                                    window.location.href = core_path + "news?e=success";
                                                    return true;
                                                } else {
                                                    $(".form-error").html(data);
                                                    swal.close();
                                                }
                                                return false;
                                            }
                                        });
                                    }
                                } else {
                                    $(".imgerr").html(data[1]);
                                    return false;
                                }
                            }
                        });
                    } else {
                        swal("Cancelled", "Not Saved Yet", "error");
                    }
                });

            /*} else {
                swal("Please Add image", "Banner image not found, Kindly upload the banner image", "error");
            }*/
            return false;
        }
    });


    $(".test").click(function() {
        var value = $(this).data("option");
        alert("sdfsdf");
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=newsStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    $(".sendcustomer_mail").click(function() {
        var value = $(this).data("option");
        swal({
            title: "Are you sure to post the news and notifiy the customers?",
            text: "Once confirmed the news will be sent via email to the customer and cannot be edited further",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, Notifiy it!",
            closeOnConfirm: true,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: core_path + "resource/ajax_redirect.php?page=sendcustomer_mail",
                    dataType: "html",
                    data: { result: value },
                    beforeSend: function() {
                        $(".page_loading").show();
                    },
                    success: function(data) {
                        $(".page_loading").hide();
                        if (data == 1) {
                            //alert(data);
                            //location.reload();
                            window.location = core_path + "news?s=success";
                        } else {
                            bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                        }
                    }
                });
            } else {
                swal("Cancelled", "not sended yet", "error");
            }
        });
        return false;
    });


    /*-------------------------------------
                Lead Filter
    ---------------------------------------*/

    // Lead Filter

    $("#leadFilter").validate({
        rules: {
            leadsource: {
                required: false
            },
            assignedstatus: {
                required: false
            },
        },
        messages: {
            leadsource: {
                required: "Please Select Lead Source",
            },
            assignedstatus: {
                required: "Please Select Assigned Status",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=leadFilter",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "lead/filter?leadsource=" + response[0] + "&movecustomer=" + response[1] + "&assignedstatus=" + response[2] + "&assignedto=" + response[3] + "&leadstatus=" + response[4];
                }
            });
            return false;
        }
    });

    /*-------------------------------------
                Adhoc Filter
    ---------------------------------------*/

    // Adhoc Filter

    $("#adhocFilter").validate({
        rules: {
            request_id: {
                required: false
            },
            priority: {
                required: false
            },
        },
        messages: {
            request_id: {
                required: "Please Select Request",
            },
            priority: {
                required: "Please Select priority",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=adhocFilter",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "adhoc/filter?requeststatus=" + response[0] + "&requesttype=" + response[1] + "&priority=" + response[2] + "&assignstaus=" + response[3] + "&assigned_to=" + response[4] ;
                }
            });
            return false;
        }
    });

    $("#adhocAllFilter").validate({
        rules: {
            request_id: {
                required: false
            },
            priority: {
                required: false
            },
        },
        messages: {
            request_id: {
                required: "Please Select Request",
            },
            priority: {
                required: "Please Select priority",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=adhocAllFilter",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "adhoc/allfilter?department_id=" + response[0] + "&status=" + response[1] + "&assignstaus=" + response[2] + "&assigned_to=" + response[3] ;
                }
            });
            return false;
        }
    });


    // Assign Adhoc 

    $('.assignAdhocModel').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=assignAdhocModel",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#token1").val(response['id']);
                $("#fkey1").val(response['fkey']);
                $("#activityName1").html(response['name']);
                $("#activityMobile1").html(response['mobile']);
                $("#activityEmail1").html(response['email']);
                $('.addAssignAdhocModel').modal('show');

            }
        })
        return false;
    });

    $("#assignAdhoc").validate({
        rules: {
            employee_id: {
                required: true
            },
            remarks: {
                required: true
            },
            lead_name: {
                required: false
            },
            mobile: {
                required: false
            },
            email: {
                required: false
            },
        },
        messages: {
            employee_id: {
                required: "Select a Employee",
            },
            remarks: {
                required: "Enter Assign adhoc remarks",
            },
            lead_name: {
                required: "Enter Lead Name",
            },
            mobile: {
                required: "Enter Mobile",
            },
            email: {
                required: "Enter Email",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=assignAdhoc",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "adhoc?as=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Reassign Call 

    $('.reassignAdhocModel').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=reassignAdhocModel",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $(".layout").html(response['layout']);
                $('.ReassignAdhocModel').modal('show');
            }
        })
        return false;
    });

    $("#reassignAdhoc").validate({
        rules: {
            employee_id: {
                required: true
            },
            remarks: {
                required: true
            },
            lead_name: {
                required: true
            },
            mobile: {
                required: true
            },
            email: {
                required: true
            },
        },
        messages: {
            employee_id: {
                required: "Select a Employee",
            },
            remarks: {
                required: "Enter Assign Lead Remarks",
            },
            lead_name: {
                required: "Enter Lead Name",
            },
            mobile: {
                required: "Enter Mobile",
            },
            email: {
                required: "Enter Email",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=reassignAdhoc",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "adhoc?rs=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    /*-------------------------------------
                Property Filter
    ---------------------------------------*/

    // Property Filter

    $("#propertyFilter").validate({
        rules: {
            leadsource: {
                required: false
            },
            assignedstatus: {
                required: false
            },
        },
        messages: {
            leadsource: {
                required: "Please Select Lead Source",
            },
            assignedstatus: {
                required: "Please Select Assigned Status",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=propertyFilter",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    var response = data.split("`");
                    window.location = core_path + "property/filter?facing=" + response[0] + "&block=" + response[1] + "&floor=" + response[2] + "&room=" + response[3] ;
                }
            });
            return false;
        }
    });


    /*=====================================
            Adhoc requests
    =======================================*/

    $("form[name='addAdhocRequests']").submit(function() {
        var flag = true;
        if (flag) {
            $.ajax({
                url: core_path + "resource/ajax_adhoc_multiple_file.php?page=addticket",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "adhoc?a=success";
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
        }
        return false;
    });


    /*=====================================
        General Documents
=======================================*/

    // View Info 

    $(document).on('click', '.viewGenDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=viewGenDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".genDocInfo").html(response['layout']);
                $('.genDocModel').modal('show');
            }
        })
        return false;
    });


     /*=====================================
        Private Documents
    =======================================*/

    // View Info 

    $(document).on('click', '.viewPrivateDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=viewPrivateDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".privateDocInfo").html(response['layout']);
                $('.privateDocModel').modal('show');
            }
        })
        return false;
    });

    /*=====================================
        Private Common Documents
    =======================================*/

    // View Info 

    $(document).on('click', '.viewPrivateCommonDocInfo', function() {
        var id = $(this).data("option");
        var value = id ;
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=viewPrivateCommonDocInfo",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);                   
                $(".privateDocInfo").html(response['layout']);
                $('.privateDocModel').modal('show');
            }
        })
        return false;
    });

    /*----------------------------
       Manage Property Area
     ------------------------------*/

    // Add Property Area

    $("#addPropertyArea").validate({
        rules: {
            title: {
                required: true,
            },
            carpet_area: {
                required: true,
            },
            total_area: {
                required: true,
            },
            usable_area: {
                required: true,
            },
            common_area: {
                required: true,
            },
            uds: {
                required: true,
            },
            uds_perc: {
                required: true,
            },
           
        },
        messages: {
            title: {
                required: "Please Enter your Title",
            },
            carpet_area: {
                required: "Please Enter your Carpet Area",
            },
            total_area: {
                required: "Please Enter your Total Area",
            },
            usable_area: {
                required: "Please Enter your Usable Area %",
            },
            common_area: {
                required: "Please Enter your Common Area % ",
            },
            uds: {
                required: "Please Enter your UDS",
            },
            uds_perc: {
                required: "Please Enter your UDS %",
            },    
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addPropertyArea",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "propertyarea?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Edit Property Area

    $("#editPropertyArea").validate({
        rules: {
            title: {
                required: true,
            },
            carpet_area: {
                required: true,
            },
            total_area: {
                required: true,
            },
            usable_area: {
                required: true,
            },
            common_area: {
                required: true,
            },
            uds: {
                required: true,
            },
            uds_perc: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please Enter your Title",
            },
            carpet_area: {
                required: "Please Enter your Carpet Area",
            },
            total_area: {
                required: "Please Enter your Total Area",
            },
            usable_area: {
                required: "Please Enter your Usable Area %",
            },
            common_area: {
                required: "Please Enter your Common Area % ",
            },
            uds: {
                required: "Please Enter your UDS",
            },
            uds_perc: {
                required: "Please Enter your UDS %",
            },    
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=editPropertyArea",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "propertyarea?e=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Active & Inactive Status

    $(".propertyAreaStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=propertyAreaStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    /*----------------------------
       Manage Department
     ------------------------------*/

    // Add Department

    $("#addDepartment").validate({
        rules: {
            name: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Please Enter Department Name",
            },   
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addDepartment",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "department?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Edit Department

    $("#editDepartment").validate({
        rules: {
            name: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Please Enter Department Name",
            },   
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=editDepartment",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "department?e=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Active & Inactive Status

    $(".departmentStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=departmentStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    /*----------------------------
       Manage Employee Role
     ------------------------------*/

    // Add Employee Role

    $("#addEmployeeRole").validate({
        rules: {
            employee_role: {
                required: true,
            },
        },
        messages: {
            employee_role: {
                required: "Please Enter Employee Role",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addEmployeeRole",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "settings/role?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Active & Inactive Status

    $(".empRoleStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=empRoleStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    // Edit 

    $('.editEmployeeRole').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editEmployeeRole",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#editfkey").val(response['fkey']);
                $("#token").val(response['id']);
                $("#employee_role_info").val(response['employee_role']);
                $('.EmpRoleModel').modal('show');
            }
        })
        return false;
    });

    // Update 

    $("#updateEmployeeRole").validate({
        rules: {
            employee_role: {
                required: true,
            },
        },
        messages: {
            employee_role: {
                required: "Please Enter Employee Role",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateEmployeeRole",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "settings/role?e=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    /*----------------------------
       Manage Payment Stage
     ------------------------------*/

    // Add Payment Stage

    $("#addPaymentStage").validate({
        rules: {
            payment_stage: {
                required: true,
            },
            payment_percentage: {
                required: true,
            }
        },
        messages: {
            payment_stage: {
                required: "Please Enter Payment Stage",
            },
            payment_percentage: {
                required: "Please Enter Payment Percentage",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addPaymentStage",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "settings/stage?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Active & Inactive Status

    $(".stageStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=stageStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    // Edit 

    $('.editPaymentStage').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editPaymentStage",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#editfkey").val(response['fkey']);
                $("#token").val(response['id']);
                $("#stage_info").val(response['payment_stage']);
                $("#perc_info").val(response['payment_percentage']);
                $('.stageModel').modal('show');
            }
        })
        return false;
    });

    // Update 

    $("#updateStageRole").validate({
        rules: {
            payment_stage: {
                required: true,
            },
            payment_percentage: {
                required: true,
            }
        },
        messages: {
            payment_stage: {
                required: "Please Enter Payment Stage",
            },
            payment_percentage: {
                required: "Please Enter Payment Percentage",
            }
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateStageRole",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "settings/stage?e=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

    // add 

    $("#uploadPageVideos").validate({
        rules: {
            title: {
                required: false
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please Enter Title",
            },
            attachment: {
                required: "Please Upload Videos",
            },
        },
        submitHandler: function(form) {
            var attachment = $("#attachment").val();
            var flag = true;
            var formname = document.getElementById('uploadPageVideos');
            var formData = new FormData(formname);            
            //if (attachment != "") {
                if (flag) {
                    var content = $(form).serialize();
                    $.ajax({
                        url: core_path + "resource/ajax_videodocument_file.php?page=uploadVideoDocuments",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $(".page_loading").show();
                        },
                        success: function(data) {
                            //alert(data);
                            $(".page_loading").hide();
                            if (data == 1) {
                                window.location =  core_path + "settings/helpdocs?e=success";
                            } else {
                                $(".form-error").html(data);
                            }
                        }
                    });
                }
                return false;
            /*} else {
                swal("Upload PDF File not found!!", "Kindly upload the file", "warning");
            }*/
        }
    });

    // Active & Inactive video Status

    $(".uploadVideoStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=uploadVideoStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    $(".removeVideoDocument").click(function() {
        var id = $(this).data("item");
        swal({
            title: "Are you sure want to delete this Video?",
            text: "Once deleted the video will not be recovered!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#4caf50",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=removeVideoDocument",
                dataType: "html",
                data: { result: id },
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    $(".page_loading").hide();
                    if (data == 1) {
                        //$("#pro_" + option).remove();
                        //masonryInt();
                        //window.location = core_path + "product?c=success";
                        location.reload();
                    } else {
                        $(".form-error").show();
                        $(".form-error").html(data);
                    }
                }
            });
            } else {
                swal("Cancelled", "not saved yet", "error");
            }
        });
        return false;
    });

    // Edit 

    $('.videoPreview').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=videoPreview",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#videoid").val(response['id']);
                $("#video").html(response['video']);
                $('.videoModel').modal('show');
            }
        })
        return false;
    });

    $('.videoShowPreview').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=videoShowPreview",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#videoid").val(response['id']);
                $("#video").html(response['video']);
                $('.videoShowModel').modal('show');
            }
        })
        return false;
    });


    /*----------------------------
           Manage Project Master
     ------------------------------*/

    // Add Project Master

    $("#addProjectMaster").validate({
        rules: {
            project_name: {
                required: true,
            },
           
        },
        messages: {
            project_name: {
                required: "Please Enter Project Name",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            //alert(content);
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=addProjectMaster",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location = core_path + "settings/project?a=success";
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // Active & Inactive Status

    $(".projectMasterStatus").click(function() {
        var value = $(this).data("option");
        //alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=projectMasterStatus",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                $(".page_loading").hide();
                if (data == 1) {
                    //alert(data);
                    location.reload();
                } else {
                    bootbox.alert("<h4 class='text-danger'>Sorry Unexpected Error Occurred. Please Try Again.</h4>");
                }
            }
        });
        return false;
    });

    // Edit 

    $('.editProjectMaster').click(function() {
        var id = $(this).data("option");
        var option = $(this).data("value");
        $.ajax({                  
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=editProjectMaster",
            dataType: "html",
            data: { result: id },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
                //alert(data);
                $(".page_loading").hide();
                var response = $.parseJSON(data);
                $("#editfkey").val(response['fkey']);
                $("#token").val(response['id']);
                $("#projectname_info").val(response['project_name']);
                $('.projectTypeModel').modal('show');
            }
        })
        return false;
    });

    // Update 

    $("#updateProjectMaster").validate({
        rules: {
            project_name: {
                required: true,
            },
           
        },
        messages: {
            project_name: {
                required: "Please Enter Project Name",
            },
        },
        submitHandler: function(form) {
            var content = $(form).serialize();
            $.ajax({
                type: "POST",
                url: core_path + "resource/ajax_redirect.php?page=updateProjectMaster",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    var responce = $.parseJSON(data);
                    if (responce['status'] == 1) {
                       window.location = core_path + "settings/project?e=success";
                    } else {
                        $(".form-error").html(responce['msg']);
                    }
                }
            });
            return false;
        }
    });

     $(".selectProject").click(function () {
        var value = $(this).data("option");
       // alert(value);
        $.ajax({
            type: "POST",
            url: core_path + "resource/ajax_redirect.php?page=selectProject",
            dataType: "html",
            data: { result: value },
            beforeSend: function() {
                $(".page_loading").show();
            },
            success: function(data) {
              //  alert(data);
                $(".page_loading").hide();
                if (data == 1) {
                     //window.location = core_path ;
                     location.reload();
                } else {
                    $(".form-error").html(data);
                }
            }
        });
        return false;
    });
    

    /*------------------------------------------------
            Add Gendral Property Documnets
    -----------------------------------------------*/

    // add 

    $("#updateDueDate").validate({
        rules: {
            due_date: {
                required: true
            },
        },
        messages: {
            due_date: {
                required: "Please Enter Due Date",
            },
        },
        submitHandler: function(form) {
            var property_id = $("#property_id").val();
            var content = $(form).serialize();
            $.ajax({
                url: core_path + "resource/ajax_redirect.php?page=updateDueDate",
                type: "POST",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location =  core_path + "property/details/"+ property_id +"?du=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });

    // add 

    $("#addPaymentSchedulelist").validate({
        rules: {
            stage_id: {
                required: true
            },
            per: {
                required: true
            },
            amt_paying: {
                required: true
            },
        },
        messages: {
            stage_id: {
                required: "Please Select Stage",
            },
            per: {
                required: "Please Enter Payed Percentage",
            },
            amt_paying: {
                required: "Please Enter Amount",
            },
        },
        submitHandler: function(form) {
            var property_id = $("#property_id").val();
            var content = $(form).serialize();
            $.ajax({
                url: core_path + "resource/ajax_redirect.php?page=addPaymentSchedulelist",
                type: "POST",
                dataType: "html",
                data: content,
                beforeSend: function() {
                    $(".page_loading").show();
                },
                success: function(data) {
                    //alert(data);
                    $(".page_loading").hide();
                    if (data == 1) {
                        window.location =  core_path + "property/details/"+ property_id +"?pa=success";
                        //location.reload();
                    } else {
                        $(".form-error").html(data);
                    }
                }
            });
            return false;
        }
    });


//--- don't delete--//

/*});
*/

/*$(document).ready(function () {
    $(".popup").hide();
    $(".openpop").click(function (e) {
        e.preventDefault();
        $("iframe").attr("src", $(this).attr('href'));
        $(".links").fadeOut('slow');
        $(".popup").fadeIn('slow');
    });

    $(".close").click(function () {
        $(this).parent().fadeOut("slow");
        $(".links").fadeIn("slow");
    });
});*/