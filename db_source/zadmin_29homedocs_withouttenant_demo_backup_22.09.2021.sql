-- phpMyAdmin SQL Dump
-- version 4.0.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2021 at 11:27 AM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zadmin_29homedocs`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type_tbl`
--

CREATE TABLE IF NOT EXISTS `activity_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `activity_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `activity_type_tbl`
--

INSERT INTO `activity_type_tbl` (`id`, `token`, `project_id`, `activity_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'follow-up', 1, 'follow up', 1, 1, '2020-08-12 22:00:16', '2020-08-17 19:39:54'),
(2, 'follow-up-1', 1, 'Follow Up 1', 1, 1, '2020-08-13 09:50:59', '2020-08-17 19:40:01'),
(3, 'follow-up-2', 1, 'Follow Up 2', 1, 1, '2020-08-17 18:41:51', '2020-08-17 19:40:06'),
(4, 'processing', 1, 'Processing', 1, 1, '2020-08-17 18:41:57', '2020-08-17 19:40:41'),
(5, 'completed', 1, 'completed', 1, 1, '2020-08-17 18:42:02', '2020-08-17 19:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `adhocassign_logs_tbl`
--

CREATE TABLE IF NOT EXISTS `adhocassign_logs_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL,
  `adhoc_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `adhocassign_logs_tbl`
--

INSERT INTO `adhocassign_logs_tbl` (`id`, `project_id`, `type`, `adhoc_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(13, 1, 'created', 'manual', 1, 0, 2, '', 1, '2021-07-28 11:02:36', '2021-07-28 11:02:36'),
(14, 1, 'created', 'manual', 1, 0, 4, '', 1, '2021-08-16 18:36:21', '2021-08-16 18:36:21'),
(15, 1, 'assigned', 'manual', 1, 1, 1, 'Test', 1, '2021-08-16 18:36:45', '2021-08-16 18:36:45'),
(16, 1, 'assigned', 'manual', 1, 1, 4, 'Do the necessary', 1, '2021-08-24 18:55:26', '2021-08-24 18:55:26'),
(17, 1, 'created', 'manual', 1, 0, 6, '', 1, '2021-08-24 18:56:35', '2021-08-24 18:56:35'),
(18, 1, 'assigned', 'manual', 1, 1, 6, 'do it ASAP', 1, '2021-09-02 16:57:56', '2021-09-02 16:57:56'),
(19, 1, 'created', 'manual', 1, 0, 8, '', 1, '2021-09-02 17:02:05', '2021-09-02 17:02:05'),
(20, 1, 'assigned', 'manual', 1, 1, 2, 'Ok', 1, '2021-09-03 09:31:59', '2021-09-03 09:31:59');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_image_tbl`
--

CREATE TABLE IF NOT EXISTS `adhoc_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `ref_id` int(11) NOT NULL,
  `adoc_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `image_name` varchar(250) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `adhoc_image_tbl`
--

INSERT INTO `adhoc_image_tbl` (`id`, `project_id`, `ref_id`, `adoc_id`, `property_id`, `customer_id`, `image_name`, `file_type`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 5, 5, 5, 1, 'fHrtuXUxRTS1W9J2bzIgGaZQAOLlnp.jpg', 'jpg', 'Details', 1, '2021-08-24 18:54:44', '2021-08-24 18:54:44'),
(2, 1, 6, 6, 5, 1, 'tqEBVFN3baUn9S7_2YfmdzHecAP04s.jpg', 'jpg', 'Details', 1, '2021-08-24 18:56:35', '2021-08-24 18:56:35'),
(3, 0, 9, 9, 5, 1, '2yE7qBwVrc1lNHj6oQX_WpUu9z4ThG.png', 'png', 'Details', 1, '2021-09-03 09:27:26', '2021-09-03 09:27:26');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_log_tbl`
--

CREATE TABLE IF NOT EXISTS `adhoc_log_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `request_type` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `adhoc_log_tbl`
--

INSERT INTO `adhoc_log_tbl` (`id`, `project_id`, `type`, `request_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'created', 'user', 1, 0, 1, 'This room is going to take over my newborn baby. so need to change an anti-bacteria paint and smell free.', 1, '2021-07-27 18:44:35', '2021-07-27 18:44:35'),
(2, 1, 'created', 'user', 4, 0, 2, 'Have to change the pantry cupboard.', 1, '2021-07-28 11:02:36', '2021-07-28 11:02:36'),
(3, 0, 'created', 'user', 1, 0, 3, 'Need to change window facing', 1, '2021-08-14 11:38:48', '2021-08-14 11:38:48'),
(4, 1, 'created', 'user', 3, 0, 4, 'Test', 1, '2021-08-16 18:36:21', '2021-08-16 18:36:21'),
(5, 0, 'created', 'user', 1, 0, 5, 'Light colour needed', 1, '2021-08-24 18:54:44', '2021-08-24 18:54:44'),
(6, 1, 'created', 'admin', 21, 0, 6, 'Price attached', 1, '2021-08-24 18:56:35', '2021-08-24 18:56:35'),
(7, 0, 'created', 'user', 1, 0, 7, 'Colour change', 1, '2021-09-02 16:53:36', '2021-09-02 16:53:36'),
(8, 1, 'created', 'admin', 1, 0, 8, 'Processed', 1, '2021-09-02 17:02:05', '2021-09-02 17:02:05'),
(9, 0, 'created', 'user', 1, 0, 9, 'Painting receipt required', 1, '2021-09-03 09:27:26', '2021-09-03 09:27:26');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_request_tbl`
--

CREATE TABLE IF NOT EXISTS `adhoc_request_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_uid` varchar(255) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `request_type` varchar(250) NOT NULL,
  `requesttype_id` int(11) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `request_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `assign_status` int(11) NOT NULL,
  `closed_status` int(11) NOT NULL,
  `assign_employee_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `adhoc_request_tbl`
--

INSERT INTO `adhoc_request_tbl` (`id`, `ticket_uid`, `project_id`, `property_id`, `customer_id`, `employee_id`, `ref_id`, `type`, `request_type`, `requesttype_id`, `subject`, `remarks`, `request_status`, `status`, `assign_status`, `closed_status`, `assign_employee_id`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'T0001', 1, 5, 1, 0, 16, 'private gallery', 'user', 1, 'Need Change the Room Paint.', 'This room is going to take over my newborn baby. so need to change an anti-bacteria paint and smell free.', 1, 1, 1, 0, 1, 1, '2021-07-27 18:44:29', '2021-07-27 18:44:29'),
(2, 'T0002', 1, 0, 4, 0, 0, 'common', 'user', 2, 'Alteration in kitchen', 'Have to change the pantry cupboard.', 0, 1, 1, 0, 1, 1, '2021-07-28 11:02:29', '2021-07-28 11:02:29'),
(3, 'T0003', 0, 5, 1, 0, 0, 'common', 'user', 2, 'Need to change window facing', 'Need to change window facing', 0, 1, 0, 0, 0, 1, '2021-08-14 11:38:42', '2021-08-14 11:38:42'),
(4, 'T0004', 1, 0, 3, 0, 0, 'common', 'user', 1, 'Need to change tile colour', 'Test', 1, 1, 1, 0, 1, 1, '2021-08-16 18:36:14', '2021-08-16 18:36:14'),
(5, 'T0005', 0, 5, 1, 0, 0, 'common', 'user', 2, 'Tile colour', 'Light colour needed', 0, 1, 0, 0, 0, 1, '2021-08-24 18:54:39', '2021-08-24 18:54:39'),
(6, 'T0006', 1, 0, 21, 0, 0, 'common', 'admin', 2, 'Tile colour - Proceeded', 'Price attached', 1, 1, 1, 0, 1, 1, '2021-08-24 18:56:29', '2021-08-24 18:56:29'),
(7, 'T0007', 0, 5, 1, 0, 0, 'common', 'user', 2, 'Paint', 'Colour change', 0, 1, 0, 0, 0, 1, '2021-09-02 16:53:30', '2021-09-02 16:53:30'),
(8, 'T0008', 1, 0, 1, 0, 0, 'common', 'admin', 3, 'Paint', 'Processed', 0, 1, 0, 0, 0, 1, '2021-09-02 17:01:59', '2021-09-02 17:01:59'),
(9, 'T0009', 0, 5, 1, 0, 0, 'common', 'user', 2, 'Bill wanted', 'Painting receipt required', 0, 1, 0, 0, 0, 1, '2021-09-03 09:27:20', '2021-09-03 09:27:20');

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

CREATE TABLE IF NOT EXISTS `admin_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `ad_name` varchar(250) NOT NULL,
  `ad_mobile` varchar(50) NOT NULL,
  `ad_email` varchar(250) NOT NULL,
  `ad_dob` varchar(250) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `ad_city` varchar(250) NOT NULL,
  `ad_pincode` varchar(250) NOT NULL,
  `ad_password` varchar(250) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `ad_super_admin` int(1) NOT NULL,
  `ad_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `ad_token`, `project_id`, `ad_name`, `ad_mobile`, `ad_email`, `ad_dob`, `address1`, `address2`, `ad_city`, `ad_pincode`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'Admin', '987654321', 'admin@postcrm.com', '2020-05-30', 'xxxxxx', 'yyyyy', 'cbe', 'tamil nadu', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2020-05-28 09:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `assignadhoc_tbl`
--

CREATE TABLE IF NOT EXISTS `assignadhoc_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `remove_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `assignadhoc_tbl`
--

INSERT INTO `assignadhoc_tbl` (`id`, `project_id`, `type`, `created_by`, `created_to`, `ref_id`, `priority`, `remarks`, `status`, `remove_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'assign', 1, 1, 1, 'medium', 'Test', 1, 0, '2021-08-16 18:36:45', '2021-08-16 18:36:45'),
(2, 1, 'assign', 1, 1, 4, 'medium', 'Do the necessary', 1, 0, '2021-08-24 18:55:26', '2021-08-24 18:55:26'),
(3, 1, 'assign', 1, 1, 6, 'medium', 'do it ASAP', 1, 0, '2021-09-02 16:57:56', '2021-09-02 16:57:56'),
(4, 1, 'assign', 1, 1, 2, 'medium', 'Ok', 1, 0, '2021-09-03 09:31:59', '2021-09-03 09:31:59');

-- --------------------------------------------------------

--
-- Table structure for table `assignlead_tbl`
--

CREATE TABLE IF NOT EXISTS `assignlead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `remove_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `assignlead_tbl`
--

INSERT INTO `assignlead_tbl` (`id`, `project_id`, `type`, `created_by`, `created_to`, `ref_id`, `priority`, `remarks`, `status`, `remove_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'assign', 1, 2, 1, 'low', 'follow up', 0, 0, '2021-08-14 10:48:06', '2021-09-03 09:09:10'),
(2, 1, 'assign', 1, 3, 2, 'low', 'Followup', 1, 0, '2021-08-14 10:48:26', '2021-08-14 10:48:26'),
(3, 1, 'assign', 1, 2, 3, 'low', 'Followup', 1, 0, '2021-08-14 10:48:36', '2021-08-14 10:48:36'),
(4, 1, 'assign', 1, 4, 5, 'low', 'Followup', 1, 0, '2021-08-14 10:48:44', '2021-08-14 10:48:44'),
(5, 1, 'assign', 1, 4, 7, 'high', 'Followup', 1, 0, '2021-08-14 10:48:56', '2021-08-14 10:48:56'),
(6, 1, 'assign', 1, 2, 8, 'high', 'Followup', 1, 0, '2021-08-14 10:49:08', '2021-08-14 10:49:08'),
(7, 1, 'assign', 1, 3, 9, 'urgent', 'Wall painting', 1, 0, '2021-08-24 17:13:13', '2021-08-24 17:13:13'),
(8, 1, 'reassign', 1, 3, 1, 'medium', 'Processed', 1, 0, '2021-09-03 09:09:10', '2021-09-03 09:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `block_tbl`
--

CREATE TABLE IF NOT EXISTS `block_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `block` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `block_tbl`
--

INSERT INTO `block_tbl` (`id`, `token`, `project_id`, `block`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'blockc', 1, 'Block c', 1, 1, '2020-05-28 16:09:22', '2020-09-26 11:07:00'),
(2, 'blockb', 1, 'Block B', 1, 1, '2020-06-03 09:06:06', '2020-09-26 11:05:45'),
(3, 'blockd', 1, 'Block D', 1, 1, '2020-09-26 10:58:35', '2020-09-26 11:05:50'),
(4, 'blocka', 1, 'Block A', 1, 1, '2020-09-26 11:03:06', '2020-09-26 11:06:27'),
(5, 'blocke', 1, 'Block e', 1, 1, '2020-09-26 11:06:16', '2020-09-26 11:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `company_settings_tbl`
--

CREATE TABLE IF NOT EXISTS `company_settings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `website_address` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_settings_tbl`
--

INSERT INTO `company_settings_tbl` (`id`, `project_id`, `phone`, `email`, `website_address`, `address`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, '9942387100', 'admin@postcrm.com', 'admin@postcrm.com', '99A vyasuva building', 1, 1, '2020-08-05 20:10:45', '2020-08-05 20:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_list_tbl`
--

CREATE TABLE IF NOT EXISTS `contactinfo_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contactinfo_list_tbl`
--

INSERT INTO `contactinfo_list_tbl` (`id`, `contact_id`, `project_id`, `name`, `designation`, `mobile`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 'name 1', 'role 1', '994238100', 'koshik1@webykart.com', 1, '2020-06-06 10:39:58', '2020-06-06 10:39:58'),
(2, 3, 1, 'name 2', 'role 2', '9942381001', 'koshik2@webykart.com', 1, '2020-06-06 10:39:58', '2020-06-06 10:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_tbl`
--

CREATE TABLE IF NOT EXISTS `contactinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `contactinfo_tbl`
--

INSERT INTO `contactinfo_tbl` (`id`, `token`, `project_id`, `name`, `role`, `email`, `mobile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(3, 'alex', 1, 'Alex', 'Admin', 'admin@postcrm.com', '9942387100', 1, 1, '2020-06-06 10:39:58', '2021-08-20 12:09:51');

-- --------------------------------------------------------

--
-- Table structure for table `customerdatasheet_tbl`
--

CREATE TABLE IF NOT EXISTS `customerdatasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `customerdatasheet_tbl`
--

INSERT INTO `customerdatasheet_tbl` (`id`, `project_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(2, 1, 'We7WOWsJtNKP87V', '05-09-2020_uKh8SdY9_samplecustomerdata1.xlsx', '2020-09-05 12:41:39', '2020-09-05 12:41:39', 0),
(3, 1, 'T57EtxcJYa7Q07r', '08-09-2020_8vqPlDrf_emailcustomerdata (1).xlsx', '2020-09-08 00:24:10', '2020-09-08 00:24:10', 0),
(4, 1, 'L2EdeftmKRRcRmx', '02-09-2021_RciZEUof_Leads.xlsx', '2021-09-02 15:15:22', '2021-09-02 15:15:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile_tbl`
--

CREATE TABLE IF NOT EXISTS `customer_profile_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `customer_profile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `customer_profile_tbl`
--

INSERT INTO `customer_profile_tbl` (`id`, `token`, `project_id`, `customer_profile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'teacher', 1, 'Teacher', 1, 1, '2020-09-22 15:38:24', '2021-08-20 12:08:16'),
(2, 'police', 1, 'Police', 1, 1, '2020-09-22 16:12:47', '2021-08-20 12:08:36'),
(4, 'mnc', 1, 'MNC', 1, 1, '2020-09-27 17:09:23', '2021-08-20 12:09:04'),
(5, 'government-service', 1, 'Government Service', 1, 1, '2020-09-27 17:09:24', '2021-08-20 12:09:17'),
(6, 'business', 1, 'Business', 1, 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(7, 'doctor', 1, 'Doctor', 1, 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(8, 'white-collar-', 1, 'White collar', 1, 1, '2021-08-20 12:08:02', '2021-08-20 12:08:02'),
(9, 'private-organisation', 1, 'Private organisation', 1, 1, '2021-08-24 17:01:11', '2021-08-24 17:01:11');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE IF NOT EXISTS `customer_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_token` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `priority` varchar(250) NOT NULL,
  `allotment_date` varchar(250) DEFAULT NULL,
  `assign_remarks` longtext,
  `name` varchar(250) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `has_psw` varchar(250) DEFAULT NULL,
  `address` text,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `nationality` varchar(250) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `ad_contact_person` varchar(250) DEFAULT NULL,
  `ad_relationship` varchar(250) DEFAULT NULL,
  `ad_mobile` varchar(250) DEFAULT NULL,
  `ad_email` varchar(250) DEFAULT NULL,
  `ad_address` varchar(250) DEFAULT NULL,
  `ad_city` varchar(250) DEFAULT NULL,
  `ad_state` varchar(250) DEFAULT NULL,
  `ad_pincode` varchar(250) DEFAULT NULL,
  `description` longtext,
  `status` int(1) NOT NULL,
  `kyc_status` int(11) NOT NULL,
  `approved_on` varchar(250) DEFAULT NULL,
  `kyc_submittedon` varchar(250) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `kyc_remarks` longtext NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `resetpassword` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`id`, `import_token`, `token`, `project_id`, `type`, `lead_id`, `uid`, `priority`, `allotment_date`, `assign_remarks`, `name`, `gender`, `dob`, `mobile`, `email`, `password`, `has_psw`, `address`, `city`, `state`, `nationality`, `pincode`, `ad_contact_person`, `ad_relationship`, `ad_mobile`, `ad_email`, `ad_address`, `ad_city`, `ad_state`, `ad_pincode`, `description`, `status`, `kyc_status`, `approved_on`, `kyc_submittedon`, `approved_by`, `kyc_remarks`, `assign_status`, `resetpassword`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'tQkM9JiwxSm3Ruj', 'lars', 1, NULL, NULL, 'CU0001', 'low', '', '', 'Lars', 'female', NULL, '9942387100', 'koshikprabhur@venpep.net', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TBfjCKOg', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-10-01 10:01:16', NULL, 1, 'approved', 1, NULL, 1, '2020-09-05 13:00:27', '2021-07-27 17:22:51'),
(2, 'tQkM9JiwxSm3Ruj', 'driss', 1, NULL, NULL, 'CU0002', 'low', '', '', 'Driss', 'female', NULL, '9585523822', 'dhileepan62@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'xlBKeDTU', '85,kalyani nagar,udumalaipettai,gandhi nagar,tirupur-642154', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 1, NULL, 1, '2020-09-05 13:00:30', '2021-09-15 11:00:52'),
(3, 'tQkM9JiwxSm3Ruj', 'ethan', 1, NULL, NULL, 'CU0003', 'low', NULL, NULL, 'Ethan', 'female', NULL, '9818071456', 'rharirajan@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'yqvQByKe', '22/A, D.D.A MIG-Flats,Pocket-2,Sector-7,Dwarka(west) West Delhi-110075', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:33', '2021-07-26 11:52:07'),
(4, 'tQkM9JiwxSm3Ruj', 'derrick', 1, NULL, NULL, 'CU0004', 'high', '', '', 'Derrick', 'male', NULL, '8754589225', 'maddyvishnoo@gmail.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'IpXZsyqn', 'PLOT NO.13,4Th Cross Street, Siva Sakthi Nagar, Madhavaram-Chennai-600060', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 1, NULL, 1, '2020-09-05 13:00:36', '2021-07-26 12:05:46'),
(21, NULL, 'satiel', 1, NULL, NULL, 'CU0021', 'high', '', '', 'Satiel', 'male', NULL, '012345678', 'cass.supernaturals@gmail.com', 'c6471ef3fdfd54235397d79530f7278364a0665e', 'XptuqvcB', '12 b', '', 'Chiacgo', NULL, '012345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 1, NULL, 1, '2021-08-24 18:07:25', '2021-08-24 18:23:07'),
(22, NULL, 'ramoorthy-', 1, 'moved', 7, 'CU0022', 'low', '', '', 'Ramoorthy', 'Male', NULL, '9737356233', '', '3ba9eb3d6d0cf18e42c5395fbb7d94b300be7c84', 'yIlgPDFK', '', 'Cbe', 'TamilNadu', NULL, '641030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 1, NULL, 1, '2021-09-02 11:20:24', '2021-09-02 18:50:01'),
(23, NULL, 'alba-', 1, 'moved', 1, 'CU0023', 'low', NULL, NULL, 'Alba', 'female', NULL, '9988878', 'Kalyancardio@yahoo.com', '64fd043c72e8e3d8ca12c0a615d75b7c7cf3b375', 'AGWdeyEZ', '100 Elm', 'Ridge Center Dr,', 'Greece NY', NULL, '14626', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-09-03 09:08:17', '2021-09-03 09:08:17'),
(24, NULL, 'derrick-', 1, 'moved', 2, 'CU0024', 'low', NULL, NULL, 'Derrick', 'female', NULL, '994004123022', 'sanath@camdew.com', '1b0ba9b6ef899e498100f1b776cde681c034b707', 'dcQhIHdP', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', NULL, '614408', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-09-06 18:43:54', '2021-09-06 18:43:54'),
(25, NULL, 'driss-', 1, 'moved', 3, 'CU0025', 'low', NULL, NULL, 'Driss', 'male', NULL, '9942009450', 'admin@postcrm.com', '696bde3111c84ad89951f4f84e310d96b13e8942', 'MzlTZZCQ', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2021-09-10 16:20:48', '2021-09-10 16:20:48');

-- --------------------------------------------------------

--
-- Table structure for table `datasheet_tbl`
--

CREATE TABLE IF NOT EXISTS `datasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `datasheet_tbl`
--

INSERT INTO `datasheet_tbl` (`id`, `project_id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(2, 1, 'SyDxz7NhpBL0Y7L', '01-10-2020_qWrggt0w_01-10-2020.xlsx', '2020-10-01 16:10:42', '2020-10-01 16:10:49', 1),
(4, 1, 'RQPhNbKBq3MJwRs', '05-10-2020_cvGSsgcQ_27.xlsx', '2020-10-05 16:04:01', '2020-10-05 16:04:09', 1),
(5, 1, '72lovaei9GJZrm0', '05-10-2020_VjhWc5zs_27.xlsx', '2020-10-05 16:04:39', '2020-10-05 16:04:44', 1),
(6, 1, 'itTPxA8CnXiYBog', '02-09-2021_nL4BNcHh_Leads.xlsx', '2021-09-02 10:57:35', '2021-09-02 10:57:35', 0),
(7, 1, 'CVUL6BEXfWwRHAU', '02-09-2021_BXQRSm91_Leads.xlsx', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(8, 1, 'kVuhPArG4WeeRm7', '02-09-2021_roan8waM_Leads.xlsx', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(9, 1, 'BuxZcz8QQzQ7C5r', '02-09-2021_SXyNRKRN_Leads.xlsx', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(10, 1, '0QLNMqdHXoJlKr3', '03-09-2021_gvFiiUOW_05-09-2020_uKh8SdY9_samplecustomerdata1 (2).xlsx', '2021-09-03 09:06:49', '2021-09-03 09:07:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department_tbl`
--

CREATE TABLE IF NOT EXISTS `department_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `short_description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `department_tbl`
--

INSERT INTO `department_tbl` (`id`, `token`, `project_id`, `name`, `short_description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'purchase', 1, 'Purchase', 'Inward raw material purchase dept.', 1, 1, '2020-10-12 20:41:22', '2021-07-26 14:56:30'),
(2, 'projectmanagement', 1, 'Project Management', '', 1, 1, '2020-10-15 09:41:25', '2021-07-26 14:59:13'),
(3, 'documentation', 1, 'Documentation', '', 1, 1, '2021-07-26 15:00:08', '2021-07-26 15:00:08'),
(4, 'sales', 1, 'Sales', '', 1, 1, '2021-07-26 15:00:25', '2021-07-26 15:00:25'),
(5, 'accounts', 1, 'Accounts', '', 1, 1, '2021-07-26 15:01:30', '2021-07-26 15:01:30'),
(6, 'hr', 1, 'HR', '', 1, 1, '2021-08-24 17:03:34', '2021-08-24 17:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `employee_permission_tbl`
--

CREATE TABLE IF NOT EXISTS `employee_permission_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `employee_id` varchar(250) DEFAULT NULL,
  `lead` int(11) NOT NULL,
  `manage_lead` int(11) NOT NULL,
  `import_lead` int(11) NOT NULL,
  `customers` varchar(250) DEFAULT NULL,
  `manage_customer` int(11) NOT NULL,
  `kyc_documents` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `import_customer` int(11) NOT NULL,
  `property` int(11) NOT NULL,
  `manage_property` int(11) NOT NULL,
  `property_chart` int(11) NOT NULL,
  `gallery` int(11) NOT NULL,
  `adhoc` int(11) NOT NULL,
  `open_request` int(11) NOT NULL,
  `close_request` int(11) NOT NULL,
  `news` int(11) NOT NULL,
  `manage_news` int(11) NOT NULL,
  `settings` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `manage_department` int(11) NOT NULL,
  `virtual_tour` int(11) NOT NULL,
  `notification` int(11) NOT NULL,
  `reports` int(11) NOT NULL,
  `adhoc_report` int(11) NOT NULL,
  `lead_report` int(11) NOT NULL,
  `customer_report` int(11) NOT NULL,
  `kyc_report` int(11) NOT NULL,
  `master_settings` int(11) NOT NULL,
  `lead_type` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `lead_source` int(11) NOT NULL,
  `profile_master` int(11) NOT NULL,
  `general_documents` int(11) NOT NULL,
  `document_checklist` int(11) NOT NULL,
  `block` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `flat_type` int(11) NOT NULL,
  `brochure` int(11) NOT NULL,
  `contact_info` int(11) NOT NULL,
  `payment_info` int(11) NOT NULL,
  `area_master` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `role_master` int(11) NOT NULL,
  `kycdoc_type` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `trash` int(11) NOT NULL,
  `upload_video` int(11) NOT NULL,
  `adhoc_request` int(11) NOT NULL,
  `all_request` int(11) NOT NULL,
  `documents` int(11) NOT NULL,
  `request_type` int(11) NOT NULL,
  `activity_report` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `employee_permission_tbl`
--

INSERT INTO `employee_permission_tbl` (`id`, `project_id`, `employee_id`, `lead`, `manage_lead`, `import_lead`, `customers`, `manage_customer`, `kyc_documents`, `invoice`, `import_customer`, `property`, `manage_property`, `property_chart`, `gallery`, `adhoc`, `open_request`, `close_request`, `news`, `manage_news`, `settings`, `employee`, `manage_department`, `virtual_tour`, `notification`, `reports`, `adhoc_report`, `lead_report`, `customer_report`, `kyc_report`, `master_settings`, `lead_type`, `activity_type`, `lead_source`, `profile_master`, `general_documents`, `document_checklist`, `block`, `floor`, `flat_type`, `brochure`, `contact_info`, `payment_info`, `area_master`, `department`, `role_master`, `kycdoc_type`, `company`, `trash`, `upload_video`, `adhoc_request`, `all_request`, `documents`, `request_type`, `activity_report`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 1, 1, 1, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2020-06-06 15:06:09', '2020-06-06 15:12:37'),
(2, 1, '2', 1, 1, 0, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, '2020-06-13 19:03:11', '2020-11-09 14:12:40'),
(3, 1, '3', 0, 0, 0, '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-08-18 08:27:42', '2020-08-18 08:27:42'),
(4, 1, '4', 1, 1, 1, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, '2020-10-15 10:20:38', '2021-07-31 17:34:49');

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_tbl`
--

CREATE TABLE IF NOT EXISTS `employee_role_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `employee_role` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employee_role_tbl`
--

INSERT INTO `employee_role_tbl` (`id`, `token`, `project_id`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'salesexecutive', 1, 'Sales Executive', 1, 1, '2020-10-13 00:37:28', '2021-08-20 12:02:30'),
(2, 'operationmanager', 1, 'Operation Manager', 1, 1, '2020-10-13 10:22:00', '2021-08-20 12:03:01'),
(3, 'contractors', 1, 'Contractors', 1, 1, '2020-10-13 10:22:10', '2021-08-20 12:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `employee_tbl`
--

CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `is_manager` int(11) NOT NULL,
  `gender` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `super_admin` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `employee_tbl`
--

INSERT INTO `employee_tbl` (`id`, `employee_uid`, `token`, `project_id`, `name`, `role_id`, `department_id`, `is_manager`, `gender`, `password`, `email`, `mobile`, `address`, `city`, `state`, `pincode`, `status`, `super_admin`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'EMP0001', 'admin', 1, 'Admin', 3, 0, 1, 'female', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin@29homedocs.com', '9942387100', '', 'coimbatore', '', '6441103', 1, 1, 1, '2020-06-06 15:06:09', '2020-10-08 10:25:31'),
(2, 'EMP0002', 'nick', 1, 'Nick', 3, 1, 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Koshik@webykart.com', '99423871001', '99A vyasuva building', 'coimbatore', 'tamil nadu', '2222', 1, 0, 1, '2020-06-13 19:03:11', '2021-08-14 10:49:41'),
(3, 'EMP0003', 'thomas', 1, 'Thomas', 1, 1, 0, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koshik1@webykart.com', '9942009450', '', '', '', '', 1, 0, 1, '2020-08-18 08:27:42', '2021-08-14 10:49:51'),
(4, 'EMP0004', 'charles', 1, 'Charles', 2, 2, 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koshikprabhur@venpep.com', '99423871012', '', '', '', '', 1, 0, 1, '2020-10-15 10:20:38', '2021-08-14 10:50:01');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_list_tbl`
--

CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `flatvilla_documenttype_list_tbl`
--

INSERT INTO `flatvilla_documenttype_list_tbl` (`id`, `project_id`, `document_id`, `doc_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'EB Form', 'test', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 1, 1, 'EB Bill', 'test', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(3, 1, 2, 'Building  Ouside ', 'Building Ouside View ', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(4, 1, 2, 'Building inner View', 'Building inner View', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(6, 1, 0, 'Building Ouside View', 'Form', 1, '2020-07-07 14:21:23', '2020-07-07 14:21:23'),
(7, 1, 0, 'EB Form', 'wdsadas', 1, '2020-07-07 14:22:44', '2020-07-07 14:22:44'),
(10, 1, 5, 'Building Ouside View', 'eee', 1, '2020-07-07 14:23:32', '2020-07-07 14:23:32'),
(11, 1, 6, 'te', 'tes', 1, '2020-09-12 14:28:05', '2020-09-12 14:28:05'),
(12, 1, 7, 'Construction Agreement', 'Construction Agreement', 1, '2021-08-14 11:12:35', '2021-08-14 11:12:35'),
(13, 1, 8, 'Tri', 'Agree', 1, '2021-08-14 11:13:07', '2021-08-14 11:13:07'),
(14, 1, 9, 'Sale', 'Deed', 1, '2021-08-14 11:13:30', '2021-08-14 11:13:30'),
(15, 1, 10, 'Bul', 'De', 1, '2021-08-14 11:14:03', '2021-08-14 11:14:03'),
(16, 1, 11, 'Tax', 'book', 1, '2021-08-14 11:14:28', '2021-08-14 11:14:28');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_tbl`
--

CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `doc_title` varchar(250) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `publish_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `flatvilla_documenttype_tbl`
--

INSERT INTO `flatvilla_documenttype_tbl` (`id`, `token`, `project_id`, `doc_title`, `sort_order`, `status`, `publish_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'eb-documents', 1, 'EB Documents', 1, 1, 1, 0, 0, '', 0, '', '', 1, '2020-06-16 09:54:06', '2020-09-12 15:24:39'),
(2, 'bookingform', 1, 'Booking Form', 2, 1, 1, 0, 0, '', 1, '2020-09-12 16:25:06', 'hjk', 1, '2020-06-16 09:55:52', '2021-08-24 17:01:56'),
(5, 'allotmentletter', 1, 'Allotment Letter', 3, 1, 1, 0, 0, '', 1, '2020-09-12 16:24:59', 'yuhyu', 1, '2020-07-07 13:49:39', '2021-09-15 10:54:36'),
(6, 'saleagreement', 1, 'Sale Agreement', 4, 1, 1, 0, 0, '', 0, '', '', 1, '2020-09-12 14:28:05', '2021-08-14 11:11:22'),
(7, 'constructionagreement', 1, 'Construction Agreement', 5, 1, 1, 0, 0, '', 0, '', '', 1, '2021-08-14 11:12:35', '2021-09-15 10:54:41'),
(8, 'tripartiteagreement', 1, 'Tripartite Agreement', 6, 1, 1, 0, 0, '', 0, '', '', 1, '2021-08-14 11:13:07', '2021-09-15 10:54:45'),
(9, 'saledeed', 1, 'Sale Deed', 7, 1, 1, 0, 0, '', 0, '', '', 1, '2021-08-14 11:13:30', '2021-09-15 10:54:49'),
(10, 'builderdemand', 1, 'Builder Demand', 8, 1, 1, 0, 0, '', 0, '', '', 1, '2021-08-14 11:14:03', '2021-09-15 10:54:52'),
(11, 'propertytaxbook', 1, 'Property Tax Book', 9, 1, 1, 0, 0, '', 0, '', '', 1, '2021-08-14 11:14:28', '2021-09-15 10:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_master`
--

CREATE TABLE IF NOT EXISTS `flat_type_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `flat_variant` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `flat_type_master`
--

INSERT INTO `flat_type_master` (`id`, `token`, `project_id`, `flat_variant`, `description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '1bhk', 1, '1BHK', 'Sample', 1, 1, '2020-06-13 14:03:37', '2020-07-06 18:39:02'),
(2, '2bhk', 1, '2BHK', 'sample', 1, 1, '2020-06-13 14:04:06', '2020-07-06 18:39:16'),
(3, '3bhk', 1, '3BHK', 'sample', 1, 1, '2020-06-13 14:04:21', '2021-07-27 17:47:20'),
(7, '7bhk', 1, '7bhk', '', 1, 1, '2020-09-27 17:09:24', '2020-09-27 17:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_masterlist`
--

CREATE TABLE IF NOT EXISTS `flat_type_masterlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `flattype_id` int(11) NOT NULL,
  `room_type` varchar(250) NOT NULL,
  `sqft` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `flat_type_masterlist`
--

INSERT INTO `flat_type_masterlist` (`id`, `project_id`, `flattype_id`, `room_type`, `sqft`, `description`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'Kitchen d', '100', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(3, 1, 2, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(4, 1, 2, 'Kitchen', '100', 'sampl 1', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(5, 1, 3, 'Living Room', '123', 'sample', 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(7, 1, 2, 'kitcheneeee', '55', '666', 1, '2020-07-06 18:39:16', '2020-07-06 18:39:16'),
(8, 1, 3, 'Kitchen ', '240', '', 1, '2021-07-27 17:43:17', '2021-07-27 17:43:17'),
(9, 1, 3, 'Dining Hall ', '150', '', 1, '2021-07-27 17:45:22', '2021-07-27 17:45:22'),
(10, 1, 3, 'Bed Room 1', '80', '', 1, '2021-07-27 17:46:10', '2021-07-27 17:46:10'),
(11, 1, 3, 'Bed Room 2', '120', '', 1, '2021-07-27 17:46:47', '2021-07-27 17:46:47'),
(12, 1, 3, 'Bed Room 3', '150', '', 1, '2021-07-27 17:47:20', '2021-07-27 17:47:20');

-- --------------------------------------------------------

--
-- Table structure for table `floor_tbl`
--

CREATE TABLE IF NOT EXISTS `floor_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `floor` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `floor_tbl`
--

INSERT INTO `floor_tbl` (`id`, `token`, `project_id`, `floor`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, '1-floor', 1, '1 Floor', 1, 1, '2020-05-28 16:42:59', '2020-07-08 00:10:15'),
(3, '2-floor', 1, '2 Floor', 1, 1, '2020-06-03 09:07:41', '2020-06-03 09:07:41'),
(4, '3-floor', 1, '3 Floor', 1, 1, '2020-06-13 17:29:44', '2020-07-08 00:10:32'),
(5, '4-floor', 1, '4 Floor', 1, 1, '2020-06-16 23:23:02', '2020-07-08 00:10:39'),
(6, '5-floor', 1, '5 floor', 1, 1, '2020-09-26 10:51:29', '2020-09-26 10:51:29'),
(7, '6floor', 1, '6 floor', 1, 1, '2020-09-26 10:52:06', '2020-09-26 10:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

CREATE TABLE IF NOT EXISTS `gallery_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `gallery_id` varchar(250) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `gallery_image_tbl`
--

INSERT INTO `gallery_image_tbl` (`id`, `project_id`, `gallery_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, '1', 'ydz0sHUKNl5boOZpP967uVWMvcfEa3.jpg', 1, '2020-06-18 11:54:17', '2020-06-18 11:54:17'),
(4, 1, '1', 'DEXUHVW419TamP3wMCcn6gdRv2Q_zl.jpg', 1, '2020-06-18 11:54:17', '2020-06-18 11:54:17'),
(5, 1, '2', 'jGCc5E9HgImkLMqJoi1DlhA73ryeWd.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(6, 1, '2', 'NlU4q9bOpQB2PF0RfCcexSIt1YHGMK.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(7, 1, '2', 'OZgbdswf4NPo1j5aYHuIEDrK0lnt3L.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(8, 1, '2', 'GcqnorLmlPew92utC7XkF1j_OyJ3Vd.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(9, 1, '8', 'RdryKFh0HT6Jtu19UkWzSaIpBMbQDv.jpg', 1, '2021-07-27 16:42:09', '2021-07-27 16:42:09'),
(10, 1, '9', 'MQjYyCsmtZzqS2vlGILxToN5BdEiaX.jpeg', 1, '2021-07-27 16:45:00', '2021-07-27 16:45:00'),
(11, 1, '9', 'HafVC4XM0nqgYkloRxJ31yibN6SP9d.jpg', 1, '2021-07-27 16:45:00', '2021-07-27 16:45:00'),
(12, 1, '10', 'upNTnMlWBbK2JLFk4qzv1ZIfrxAQGy.jpg', 1, '2021-07-27 17:06:36', '2021-07-27 17:06:36'),
(13, 1, '10', 'Hywf6F3c40UmVP_JN1XKgSEAosqbhx.jpg', 1, '2021-07-27 17:06:51', '2021-07-27 17:06:51'),
(14, 1, '10', 'wNvuhTHo9FRaItby7_132O5kzpWP06.jpg', 1, '2021-07-27 17:07:07', '2021-07-27 17:07:07');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tbl`
--

CREATE TABLE IF NOT EXISTS `gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `album_title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `gallery_tbl`
--

INSERT INTO `gallery_tbl` (`id`, `token`, `project_id`, `album_title`, `description`, `image`, `sort_order`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'sample', 1, 'Sample', 'sample', 'EIwq2zkBZH07072020113512pexels-photo-3785927.jpeg', 7, 1, 1, 1, '2021-07-27 12:35:43', 0, '', '', 1, '2020-06-10 16:58:04', '2020-09-13 00:50:17'),
(2, 'over-view.2', 1, 'Over View', '', 'NuOQgDjQ4E27072021043823over-view-of-apartment-1.jpg', 1, 1, 0, 0, '', 0, '', '', 1, '2020-06-13 17:32:09', '2021-07-27 16:38:23'),
(3, 'sample.3', 1, 'Sample', '', '09MxPCblRz06072020010153whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 2, 1, 1, 1, '2021-07-27 12:35:58', 0, '', '', 1, '2020-07-06 13:01:53', '2020-09-13 00:50:04'),
(4, 'sample.4', 1, 'Sample', 'dfgdfg', 'GSE2ZTgUpf13092020010701whatsapp-image-2020-09-04-at-4.53.40-pm.jpeg', 6, 1, 1, 1, '2021-07-27 12:36:00', 0, '', '', 1, '2020-09-13 01:07:01', '2020-09-13 01:07:01'),
(5, 'sample.5', 1, 'Sample', '', 'pELZA5wFZp30092020045040loan.png', 10, 1, 1, 1, '2021-07-27 12:36:02', 0, '', '', 1, '2020-09-30 16:50:40', '2020-09-30 16:50:40'),
(6, 'bed-room', 1, 'bed room', '', 'JEegVqlFap27072021105758img20200903080230.jpg', 1, 0, 1, 1, '2021-07-27 11:01:45', 0, '', '', 1, '2021-07-27 10:57:58', '2021-07-27 10:57:58'),
(7, 'lift-', 1, 'Lift', '', 'B6A9M4mBTo27072021041457lift.jpg', 2, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 12:37:04', '2021-07-27 16:14:57'),
(8, 'security-', 1, 'Security', '', 'MxTbpNrVAu27072021041658security.jpg', 3, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 15:04:09', '2021-07-27 16:16:59'),
(9, 'car-parking-', 1, 'Car Parking', '', 'F73cY2f6qG27072021041752car-parking.jpg', 4, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 16:17:52', '2021-07-27 16:17:52'),
(10, 'gym', 1, 'Gym', '', 'JpVPjxpMP627072021042856gym.jpg', 5, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 16:28:56', '2021-07-27 16:28:56'),
(11, 'common-space', 1, 'Common Space', '', 'VJAYlqET0U27072021043407walking-area.jpeg', 6, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 16:34:07', '2021-07-27 16:34:07'),
(12, 'swimming-', 1, 'Swimming', '', 'hjIorCOpuy27072021043506swimming-pool.jpg', 8, 1, 0, 0, '', 0, '', '', 1, '2021-07-27 16:35:06', '2021-07-27 16:35:06'),
(13, 'swimming-pool', 1, 'Swimming pool', '', 'aIlp5MuSBc02092021031915unnamed.jpg', 10, 1, 0, 0, '', 0, '', '', 1, '2021-09-02 15:19:15', '2021-09-02 15:19:15'),
(14, 'over-view-', 1, 'Over View', '', 'uwnYKRV2Bi02092021065135looking-to-buy-1bhk-flats-in-pune-consider-these.jpg', 11, 1, 0, 0, '', 0, '', '', 1, '2021-09-02 18:51:35', '2021-09-02 18:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `gendral_documents_tbl`
--

CREATE TABLE IF NOT EXISTS `gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `gendral_documents_tbl`
--

INSERT INTO `gendral_documents_tbl` (`id`, `token`, `project_id`, `title`, `description`, `documents`, `document_type`, `added_by`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `created_at`, `updated_at`) VALUES
(1, 'zROQEkV0Jalp3rFbnyCmmrUzMFAp5w', 1, 'Legal Opinion', 'sampleee', 'attachment_07072020011931_ugNXTB2NP2.jpeg', 'jpeg', 1, 1, 0, 0, '', 0, '', '', '2020-06-18 09:35:33', '2021-08-14 11:04:12'),
(2, 'v6VuH3VJHdd8ONOs8iwRQVOkrnMr4X', 1, 'RERA Certificate', 'sasa', 'attachment_18062020111948_CBQEMMBYc9.pdf', 'pdf', 1, 1, 0, 0, '', 1, '2020-09-12 16:23:33', 'sample', '2020-06-18 11:19:48', '2021-08-14 11:04:27'),
(3, 'J86qUoJfvRkiJvYnRPXdOtBGpHNox0', 1, 'new test', 'sampples', 'attachment_11092020111245_nxOTJJcLYt.pdf', 'pdf', 1, 1, 1, 1, '2020-09-12 15:45:44', 0, '', '', '2020-09-11 11:12:45', '2020-09-12 15:45:37'),
(4, '9TEB0uq49yCQHCGsfzi68aYdzon0hQ', 1, 'APF Letter', '76567', 'attachment_26092020123925_wLUEFjzIcm.xls', 'xls', 1, 1, 0, 0, '', 0, '', '', '2020-09-26 00:39:25', '2021-08-14 11:04:39'),
(5, 'mYf75tlKk5N7v5iopJMou7iSyYNOns', 1, 'Floor Plans', 'sample', 'attachment_14082021110640_Hl6vMg04ce.pdf', 'pdf', 1, 1, 0, 0, '', 0, '', '', '2021-08-14 11:06:40', '2021-08-14 11:06:40'),
(6, '4EaEtS0INhYn7AU0nvGEHzubvBCVzI', 1, 'Electrical Drawing', 'Sample', 'attachment_14082021110702_HuNZCAPFO1.pdf', 'pdf', 1, 1, 0, 0, '', 0, '', '', '2021-08-14 11:07:02', '2021-08-14 11:07:02'),
(7, '0LV1MJrqX8dvbwc1xPG7nuTJeBU7mc', 1, 'Plumbing Drawings', 'Sample', 'attachment_14082021110750_5VZytDvE5u.pdf', 'pdf', 1, 1, 0, 0, '', 0, '', '', '2021-08-14 11:07:50', '2021-08-14 11:07:50'),
(8, 'jdYqliZ5jyeBHEUSiPLxrn3fFLRM5b', 1, 'Car parking plans', 'Sample', 'attachment_14082021110815_pUhMUnGn0M.pdf', 'pdf', 1, 1, 0, 0, '', 0, '', '', '2021-08-14 11:08:15', '2021-08-14 11:08:15'),
(9, 'Ijip71lTB2Mn1mD4pYG6NAaaGqWAgd', 1, 'Test', '', 'attachment_18082021043044_bhYi1fMRdK.pdf', 'pdf', 1, 1, 1, 1, '2021-08-20 12:05:30', 0, '', '', '2021-08-18 16:30:44', '2021-08-18 16:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

CREATE TABLE IF NOT EXISTS `invoice_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `inv_uid` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `inv_number` varchar(255) DEFAULT NULL,
  `inv_date` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `raised_to` varchar(250) DEFAULT NULL,
  `documents` varchar(250) DEFAULT NULL,
  `document_type` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `invoice_tbl`
--

INSERT INTO `invoice_tbl` (`id`, `project_id`, `inv_uid`, `customer_id`, `inv_number`, `inv_date`, `type`, `raised_to`, `documents`, `document_type`, `remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'INV0001', 1, '0232', '1970-01-01', 'invoice', 'bank', 'attachment_30092020104541_Co9jYUQaMs.pdf', 'pdf', 'sample', 1, 0, 0, '', 0, '', '', 1, '2020-07-04 11:32:44', '2020-09-30 10:45:41'),
(2, 1, 'INV0002', 4, '00111', '2020-08-23', 'invoice', 'bank', 'attachment_23082020101814_Rgk24ItSyP.pdf', 'pdf', 'sample', 1, 1, 1, '2020-09-13 00:43:49', 0, '', '', 1, '2020-08-23 22:18:14', '2020-09-13 00:37:28'),
(3, 1, 'INV0003', 1, '444444444', '2020-09-14', 'invoice', 'bank', 'attachment_13092020010558_gc5iC80QyM.pdf', 'pdf', 'ghjghj', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:05:58', '2020-09-30 10:45:31'),
(4, 1, 'INV0004', 1, '8799998', '2020-09-12', 'invoice', 'bank', 'attachment_13092020010621_ZhDnaghxQs.pdf', 'pdf', 'ghfgh', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:06:21', '2020-09-13 01:06:21'),
(5, 1, 'INV0005', 1, '123', '2021-08-30', 'invoice', 'bank', 'attachment_02092021032944_pL7iNoiQGv.png', 'png', 'Kindly save', 1, 0, 0, '', 0, '', '', 1, '2021-09-02 15:29:44', '2021-09-02 15:29:44'),
(6, 1, 'INV0006', 1, '123', '2021-08-31', 'invoice', 'customer', 'attachment_02092021044710_LF0ffRyUY1.png', 'png', 'Done', 1, 1, 1, '2021-09-02 16:47:46', 0, '', '', 1, '2021-09-02 16:47:10', '2021-09-02 16:47:10'),
(7, 1, 'INV0007', 2, '123', '2021-09-07', 'invoice', 'bank', 'attachment_03092021092252_5GKZvJyAVW.png', 'png', 'Processed', 1, 1, 1, '2021-09-03 09:23:31', 0, '', '', 1, '2021-09-03 09:22:52', '2021-09-03 09:22:52');

-- --------------------------------------------------------

--
-- Table structure for table `kyc_list_tbl`
--

CREATE TABLE IF NOT EXISTS `kyc_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) NOT NULL,
  `kyctype_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `file_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `kyc_list_tbl`
--

INSERT INTO `kyc_list_tbl` (`id`, `project_id`, `customer_id`, `kyctype_id`, `image_name`, `file_type`, `status`, `approval_status`, `created_at`, `updated_at`) VALUES
(6, 1, 1, 1, '4uD7NEU2wOF0P9fWHJMVzgst6Spn8T.jpeg', 'jpeg', 1, 0, '2020-07-03 18:07:56', '2020-07-03 18:07:56'),
(10, 1, 1, 2, 'mOeN5QXVBPct1CMfUy4SbzY3dnhZkG.pdf', 'pdf', 1, 0, '2020-07-06 17:28:06', '2020-07-06 17:28:06'),
(12, 1, 1, 2, 'LtigKmZqvb0Gl4eYETrUsaNAf7k_IM.pdf', 'pdf', 1, 0, '2020-07-06 18:04:22', '2020-07-06 18:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `kyc_tbl`
--

CREATE TABLE IF NOT EXISTS `kyc_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `kyc_uid` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `id_type` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `dob` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `nationiality` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_type_tbl`
--

CREATE TABLE IF NOT EXISTS `kyc_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL,
  `kyc_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kyc_type_tbl`
--

INSERT INTO `kyc_type_tbl` (`id`, `token`, `project_id`, `kyc_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'aadhaar-card', 1, 'Aadhaar Card', 1, 1, '2020-07-02 22:58:39', '2020-08-19 20:11:48'),
(2, 'pan-card', 1, 'PAN Card', 1, 1, '2020-07-02 22:59:04', '2020-07-08 00:28:49'),
(3, 'ration-card', 1, 'Ration Card', 1, 1, '2020-07-02 22:59:12', '2020-07-02 22:59:12'),
(4, 'election-commission-id-card', 1, 'Election Commission ID Card', 1, 1, '2020-07-02 22:59:20', '2020-07-02 22:59:20'),
(5, 'driving-license', 1, 'Driving License', 1, 1, '2020-07-02 22:59:27', '2020-07-02 22:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity_tbl`
--

CREATE TABLE IF NOT EXISTS `lead_activity_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `date` varchar(250) NOT NULL,
  `lead_id` varchar(250) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `lead_activity_tbl`
--

INSERT INTO `lead_activity_tbl` (`id`, `project_id`, `date`, `lead_id`, `employee_id`, `activity_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-08-14', '1', 1, 1, 'call back on 15th Aug', 1, '2021-08-14 10:47:04', '2021-08-14 10:47:04'),
(2, 1, '2021-08-14', '1', 1, 4, 'followup', 1, '2021-08-14 11:32:49', '2021-08-14 11:32:49'),
(3, 1, '2021-08-23', '2', 1, 5, 'Purchased', 1, '2021-08-24 17:16:29', '2021-08-24 17:16:29'),
(4, 1, '2021-09-01', '1', 1, 4, 'Confirmed', 1, '2021-09-03 09:08:46', '2021-09-03 09:08:46'),
(5, 1, '2021-09-06', '1', 1, 1, 'Test', 1, '2021-09-06 18:42:43', '2021-09-06 18:42:43'),
(6, 1, '2021-09-10', '3', 1, 1, 'call me after 3 days', 1, '2021-09-10 16:20:02', '2021-09-10 16:20:02');

-- --------------------------------------------------------

--
-- Table structure for table `lead_logs_tbl`
--

CREATE TABLE IF NOT EXISTS `lead_logs_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(250) NOT NULL,
  `lead_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `lead_logs_tbl`
--

INSERT INTO `lead_logs_tbl` (`id`, `project_id`, `type`, `lead_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'created', 'manual', 1, 0, 3, '', 1, '2020-09-27 17:12:21', '2020-09-27 17:12:21'),
(2, 1, 'assigned', 'manual', 1, 2, 1, 'follow up', 1, '2021-08-14 10:48:06', '2021-08-14 10:48:06'),
(3, 1, 'assigned', 'manual', 1, 3, 2, 'Followup', 1, '2021-08-14 10:48:26', '2021-08-14 10:48:26'),
(4, 1, 'assigned', 'manual', 1, 2, 3, 'Followup', 1, '2021-08-14 10:48:36', '2021-08-14 10:48:36'),
(5, 1, 'assigned', 'manual', 1, 4, 5, 'Followup', 1, '2021-08-14 10:48:44', '2021-08-14 10:48:44'),
(6, 1, 'assigned', 'manual', 1, 4, 7, 'Followup', 1, '2021-08-14 10:48:56', '2021-08-14 10:48:56'),
(7, 1, 'assigned', 'manual', 1, 2, 8, 'Followup', 1, '2021-08-14 10:49:08', '2021-08-14 10:49:08'),
(8, 1, 'assigned', 'manual', 1, 3, 9, 'Wall painting', 1, '2021-08-24 17:13:13', '2021-08-24 17:13:13'),
(9, 1, 'reassigned', 'manual', 1, 3, 1, 'Processed', 1, '2021-09-03 09:09:10', '2021-09-03 09:09:10');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_tbl`
--

CREATE TABLE IF NOT EXISTS `lead_source_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `lead_source` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `lead_source_tbl`
--

INSERT INTO `lead_source_tbl` (`id`, `token`, `project_id`, `lead_source`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'twitter', 1, 'Twitter', 1, 1, '2020-09-18 10:17:38', '2021-08-03 22:46:48'),
(2, 'google', 1, 'Google', 1, 1, '2020-09-21 11:53:45', '2020-09-21 11:53:45'),
(3, 'instagram', 1, 'Instagram', 1, 1, '2020-09-21 11:53:55', '2020-09-21 11:53:55'),
(4, 'youtubeads', 1, 'Youtube ads', 1, 1, '2020-09-22 11:42:37', '2021-08-20 12:07:38'),
(6, 'internet', 1, 'internet', 1, 1, '2020-09-27 17:09:23', '2020-09-27 17:09:23'),
(7, 'facebook', 1, 'Facebook', 1, 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47');

-- --------------------------------------------------------

--
-- Table structure for table `lead_tbl`
--

CREATE TABLE IF NOT EXISTS `lead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `import_token` varchar(255) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `auth_type` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `lead_date` varchar(250) DEFAULT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `leadsource` varchar(250) NOT NULL,
  `flattype_id` int(11) DEFAULT NULL,
  `site_visit_status` varchar(250) DEFAULT NULL,
  `customer_profile_id` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `secondary_mobile` varchar(255) DEFAULT NULL,
  `secondary_email` varchar(255) DEFAULT NULL,
  `address` text,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `description` longtext,
  `lead_status_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `closed_status` int(11) DEFAULT NULL,
  `moved_status` int(1) NOT NULL,
  `move_remarks` longtext,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `lead_tbl`
--

INSERT INTO `lead_tbl` (`id`, `token`, `import_token`, `uid`, `project_id`, `auth_type`, `property_id`, `customer_id`, `lead_date`, `fname`, `lname`, `leadsource`, `flattype_id`, `site_visit_status`, `customer_profile_id`, `gender`, `mobile`, `email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `pincode`, `description`, `lead_status_id`, `employee_id`, `assign_status`, `closed_status`, `moved_status`, `move_remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'alba', '4ulsFbuBeO17tp5', 'LE0000', 1, 'excel', 0, 23, '2020-09-27', 'Alba', '', '7', 3, 'no', 1, 'female', '9988878', 'Kalyancardio@yahoo.com', '', '', '100 Elm', 'Ridge Center Dr,', 'Greece NY', '14626', 'Intrest to visit the site.', 5, 3, 1, 1, 1, 'done', 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:09:23', '2021-09-10 16:19:23'),
(2, 'derrick', '4ulsFbuBeO17tp5', 'LE0001', 1, 'excel', 0, 24, '2020-09-27', 'Derrick', '', '3', 3, 'yes', 2, 'female', '994004123022', 'sanath@camdew.com', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', '614408', 'sample 2', 1, 3, 1, 1, 1, 'Closed', 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:09:23', '2021-07-26 11:54:03'),
(3, 'driss', NULL, 'LE0003', 1, NULL, 0, 25, '2020-09-27', 'Driss', '', '2', 7, 'yes', 1, 'male', '9942009450', 'admin@postcrm.com', '', '', '', '', '', '', '', 4, 2, 1, 1, 1, 'advance amount done', 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:12:21', '2021-09-02 10:57:52'),
(4, 'ethan', 'SyDxz7NhpBL0Y7L', 'LE0003', 1, 'excel', 0, 0, '2020-10-01', 'Ethan', '', '6', 7, 'yes', 6, '', '7373021332', 'digital@29homedocs.com', '', '', '', 'Cbe', 'TamilNadu', '641027', 'Did not ans call', 4, 0, 0, 0, 0, '', 1, 1, 1, '2021-07-26 16:12:13', 0, '', '', 1, '2020-10-01 16:10:46', '2021-07-24 15:43:51'),
(5, 'rachael-adams', 'SyDxz7NhpBL0Y7L', 'LE0004', 1, 'excel', 0, 0, '2020-10-01', 'Rachael Adams', '', '2', 3, 'yes', 6, '', '8825642733', 'masteradmin@venpep.com', '', '', '', 'Madurai', 'TamilNadu', '641028', 'Did not ans call', 2, 4, 1, 1, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2021-07-26 11:59:10'),
(6, 'vetrivel', 'SyDxz7NhpBL0Y7L', 'LE0005', 1, 'excel', 0, 0, '2020-10-01', 'Vetrivel', '', '7', 2, 'Yes', 6, 'Male', '9841051590', '', '', '', '', 'Chennai', 'TamilNadu', '641029', 'has in mind, will call back', 2, 0, 0, 0, 0, '', 1, 1, 1, '2021-07-26 15:24:14', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(7, 'ramoorthy', 'SyDxz7NhpBL0Y7L', 'LE0006', 1, 'excel', 0, 22, '2020-10-01', 'Ramoorthy', '', '7', 2, 'No', 6, 'Male', '9737356233', '', '', '', '', 'Cbe', 'TamilNadu', '641030', 'thinking over multiple choices in chennai and cbe', 2, 4, 1, 1, 1, 'Done\r\n', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(8, 'akshay', 'SyDxz7NhpBL0Y7L', 'LE0007', 1, 'excel', 0, 0, '2020-10-01', 'Akshay', '', '2', 3, 'Yes', 6, 'Male', '8610739811', '', '', '', '', 'Madurai', 'TamilNadu', '641031', 'Did not ans call', 2, 2, 1, 1, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(9, 'ramkumar', 'SyDxz7NhpBL0Y7L', 'LE0008', 1, 'excel', 0, 0, '2020-10-01', 'Ramkumar', '', '2', 2, 'No', 6, 'Male', '9894013341', '', '', '', '', 'Chennai', 'TamilNadu', '641032', 'Room sizes are small, thinking off larger option', 2, 3, 1, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(10, 'balasundaram', 'SyDxz7NhpBL0Y7L', 'LE0009', 1, 'excel', 0, 0, '2020-10-01', 'Balasundaram', '', '7', 2, 'Yes', 6, 'Male', '9677622669', '', '', '', '', 'Cbe', 'TamilNadu', '641033', 'Wants L Type, Ready to buy with full payment', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(11, 'prabha', 'SyDxz7NhpBL0Y7L', 'LE0010', 1, 'excel', 0, 0, '2020-10-01', 'Prabha', '', '7', 2, 'No', 7, 'Male', '6382735660', '', '', '', '', 'Madurai', 'TamilNadu', '641034', 'share details again, not replying', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(12, 'raji', 'SyDxz7NhpBL0Y7L', 'LE0011', 1, 'excel', 0, 0, '2020-10-01', 'Raji', '', '7', 2, 'Yes', 7, 'female', '9113282820', '', '', '', '', 'Chennai', 'TamilNadu', '641035', 'Did not ans call', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(13, 'antojeba', 'SyDxz7NhpBL0Y7L', 'LE0012', 1, 'excel', 0, 0, '2020-10-01', 'Antojeba', '', '2', 2, 'No', 7, 'Male', '8098600910', '', '', '', '', 'Cbe', 'TamilNadu', '641036', 'His brother enquired it', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(14, 'murali', 'SyDxz7NhpBL0Y7L', 'LE0013', 1, 'excel', 0, 0, '2020-10-01', 'Murali', '', '2', 2, 'Yes', 7, 'Male', '9486922011', '', '', '', '', 'Madurai', 'TamilNadu', '641037', 'Will visit again with family', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(15, 'naveen', 'SyDxz7NhpBL0Y7L', 'LE0014', 1, 'excel', 0, 0, '2020-10-01', 'Naveen', '', '2', 2, 'No', 7, 'Male', '9894290431', '', '', '', '', 'Chennai', 'TamilNadu', '641038', 'Did not ans call, no response', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(16, 'gugan', 'SyDxz7NhpBL0Y7L', 'LE0015', 1, 'excel', 0, 0, '2020-10-01', 'Gugan', '', '2', 2, 'Yes', 7, 'Male', '9980148996', '', '', '', '', 'Cbe', 'TamilNadu', '641039', 'not planning to buy now', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(17, 'keerthana', 'SyDxz7NhpBL0Y7L', 'LE0016', 1, 'excel', 0, 0, '2020-10-01', 'Keerthana', '', '7', 2, 'No', 7, 'female', '9962009596', '', '', '', '', 'Madurai', 'TamilNadu', '641040', 'Did not ans call, no response', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(18, 'madhan', 'SyDxz7NhpBL0Y7L', 'LE0017', 1, 'excel', 0, 0, '2020-10-01', 'Madhan', '', '7', 2, 'yes', 7, 'Male', '8122658371', '', '', '', '', 'Chennai', 'TamilNadu', '641041', 'out of their budget at present', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(19, 'arunkumar', '72lovaei9GJZrm0', 'LE0019', 1, 'excel', 0, 0, '2020-10-05', 'Arun', 'Kumar', '1', 1, 'yes', 4, 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', '61104', 'sample 1', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-05 16:04:44', '2020-10-05 16:04:44'),
(20, 'vengetrajan', '72lovaei9GJZrm0', 'LE0020', 1, 'excel', 0, 0, '2020-10-05', 'Venget', 'Rajan', '6', 7, 'no', 5, 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', '614408', 'sample 2', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-05 16:04:44', '2020-10-05 16:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `lead_type_tbl`
--

CREATE TABLE IF NOT EXISTS `lead_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `lead_type` varchar(250) NOT NULL,
  `default_settings` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `lead_type_tbl`
--

INSERT INTO `lead_type_tbl` (`id`, `project_id`, `uid`, `token`, `lead_type`, `default_settings`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'LT0001', 'will-think-and-get-back', 'Will think and get back', 0, 1, 1, '2020-08-16 22:22:58', '2020-08-17 19:35:34'),
(2, 1, 'LT0002', 'hot-lead', 'Hot Lead', 1, 1, 1, '2020-08-16 22:23:09', '2020-08-17 19:35:22'),
(3, 1, 'LT0003', 'will-discuss-with-family', 'Will discuss with family', 0, 1, 1, '2020-08-16 22:26:03', '2020-08-17 19:35:41'),
(4, 1, 'LT0004', 'just-for-collecting-info', 'Just for collecting info', 0, 1, 1, '2020-08-17 19:35:53', '2020-08-17 19:35:53'),
(5, 1, 'LT0005', 'do-not-follow-up', 'Do not follow up', 0, 1, 1, '2020-08-17 19:35:59', '2020-08-17 19:35:59'),
(7, 1, 'LT0007', 'warm', 'Warm', 0, 1, 1, '2020-09-20 22:00:01', '2021-08-20 12:06:41'),
(8, 1, 'LT0008', 'followup', 'Follow up', 0, 1, 1, '2020-09-20 22:00:32', '2021-08-20 12:06:53'),
(9, 1, 'LT0009', 'cold', 'Cold', 0, 1, 1, '2021-08-18 16:29:44', '2021-08-18 16:29:44'),
(10, 1, 'LT0010', 'emaillead', 'Email lead', 0, 1, 1, '2021-08-24 17:00:19', '2021-08-24 17:00:19');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

CREATE TABLE IF NOT EXISTS `news_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `email_notification` int(11) NOT NULL,
  `post_to` varchar(250) NOT NULL,
  `customer_ids` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `show_popup` int(11) NOT NULL,
  `popup_till` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `send_mail_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news_tbl`
--

INSERT INTO `news_tbl` (`id`, `project_id`, `token`, `title`, `date`, `description`, `email_notification`, `post_to`, `customer_ids`, `image`, `show_popup`, `popup_till`, `status`, `send_mail_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'nirala-appartment.1', 'Nirala Appartment', '2020-09-21', 'dasdasd', 0, 'selected', '1', '', 0, '', 1, 1, 1, 1, '2020-09-25 12:07:20', 0, '', '', 1, '2020-09-21 17:03:53', '2020-09-25 11:54:11'),
(2, 1, 'nirala-appartment', 'Nirala Appartment', '2020-09-24', '<p>The Department of Mechanical Engineering, SNSCE organized the Alumni \r\nGuest lecture on 11.03.2020 in the topic Opportunities for Design \r\nEngineers in the field of Mechanical Engineering" for the third-year \r\nstudents. Our esteemed Alumni Mr.B.Jagadeeshwaran, Design Engineer, \r\nEngineer Design and Development, M/s Enpro Engineering, Ambattur \r\nIndustrial Estate, Chennai addressed the students. He shared his \r\nexpertise in the area of Designing and Drafting.</p>', 0, 'selected', '1', 'AihHUczs5D30092020083929screenshot2020-07-18-sreevatsa-google-drive2.png', 1, '2020-10-01', 1, 0, 0, 0, '', 1, '2020-09-30 08:47:13', 'a', 1, '2020-09-24 18:39:36', '2020-09-30 09:34:51'),
(3, 1, 'sample-test.3', 'sample test', '2020-09-24', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industryâ€™s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br><span style="color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: common-ligatures; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(248, 248, 248); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;"></span></p>', 0, 'all', '', '', 0, '', 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-24 19:02:32', '2020-09-30 16:24:35'),
(4, 1, 'sample-test', 'sample test', '2020-09-30', '<p>sadasdasasd<br></p>', 0, 'all', '', '', 0, '', 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-30 16:36:05', '2020-09-30 16:36:05');

-- --------------------------------------------------------

--
-- Table structure for table `notification_email_tbl`
--

CREATE TABLE IF NOT EXISTS `notification_email_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `adhoc_permission` int(1) NOT NULL,
  `kyc_permission` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `notification_email_tbl`
--

INSERT INTO `notification_email_tbl` (`id`, `project_id`, `token`, `email`, `adhoc_permission`, `kyc_permission`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'adminposyutyutytcrm.com', 'admin@posyutyutytcrm.com', 1, 1, 1, 1, '2020-06-10 11:55:26', '2020-08-05 20:10:36'),
(2, 1, 'koshikwasasebykart.com', 'koshik@wasasebykart.com', 1, 0, 1, 1, '2020-07-06 13:05:49', '2020-07-08 00:50:38'),
(3, 1, 'adminpostcrm.com', 'admin@postcrm.com', 0, 1, 1, 1, '2020-07-06 14:43:16', '2020-07-08 00:51:18'),
(4, 1, 'koshikwebykart.com', 'koshik@webykart.com', 1, 1, 1, 1, '2020-07-06 17:06:45', '2020-07-06 17:06:45'),
(5, 1, 'koshikwebykart.com', 'koshik@webykart.com', 0, 1, 1, 1, '2020-07-06 17:06:53', '2020-07-06 17:06:53'),
(6, 1, 'koshikwebyukart.com', 'koshik@webyukart.com', 1, 1, 1, 1, '2020-07-08 00:45:48', '2020-07-08 00:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo_tbl`
--

CREATE TABLE IF NOT EXISTS `paymentinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `account_number` varchar(250) NOT NULL,
  `bank` varchar(250) NOT NULL,
  `ifcs_code` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `paymentinfo_tbl`
--

INSERT INTO `paymentinfo_tbl` (`id`, `project_id`, `name`, `account_number`, `bank`, `ifcs_code`, `branch`, `city`, `state`, `pincode`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'sample', '1245', 'AXIS', 'AX002', 'CIMBATORE', 'COIMBATIORE', 'tamilnadu', 641103, 1, 1, '2020-06-06 12:06:56', '2020-07-15 16:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `project_master_tbl`
--

CREATE TABLE IF NOT EXISTS `project_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_name` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `project_master_tbl`
--

INSERT INTO `project_master_tbl` (`id`, `token`, `project_name`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'project1', 'Project 1', 1, 1, '2021-08-05 23:00:36', '2021-08-05 23:21:33'),
(2, 'project2', 'Project 2', 1, 1, '2021-08-05 23:00:57', '2021-08-05 23:21:36'),
(3, 'project3', 'Project 3', 1, 1, '2021-08-05 23:01:03', '2021-08-05 23:21:51'),
(4, 'project4', 'Project 4', 0, 1, '2021-08-18 16:29:21', '2021-08-18 16:29:21');

-- --------------------------------------------------------

--
-- Table structure for table `project_type_tbl`
--

CREATE TABLE IF NOT EXISTS `project_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(250) NOT NULL,
  `project_name` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `project_type_tbl`
--

INSERT INTO `project_type_tbl` (`id`, `project_id`, `token`, `project_name`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'royal-palm', 'Royal Palm', 1, 1, '2020-08-19 08:33:01', '2020-08-19 09:07:34');

-- --------------------------------------------------------

--
-- Table structure for table `property_area_tbl`
--

CREATE TABLE IF NOT EXISTS `property_area_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL,
  `carpet_area` varchar(250) NOT NULL,
  `total_area` varchar(250) NOT NULL,
  `usable_area` varchar(250) NOT NULL,
  `common_area` varchar(250) NOT NULL,
  `uds` varchar(250) NOT NULL,
  `uds_perc` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `property_area_tbl`
--

INSERT INTO `property_area_tbl` (`id`, `token`, `project_id`, `title`, `carpet_area`, `total_area`, `usable_area`, `common_area`, `uds`, `uds_perc`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'testsmaplearea', 1, 'Classic Apartment', '1230', '1454 sqft', '10', '13', '897', '31', 1, 1, '2020-10-12 14:49:14', '2021-08-20 12:10:59'),
(2, 'test1', 1, 'Studio homes', '456', '456', '456', '456', '456', '546', 1, 1, '2020-10-13 10:29:21', '2021-08-20 12:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `property_broucher_tbl`
--

CREATE TABLE IF NOT EXISTS `property_broucher_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL,
  `property_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `brochures` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `property_broucher_tbl`
--

INSERT INTO `property_broucher_tbl` (`id`, `token`, `project_id`, `title`, `property_id`, `description`, `brochures`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '2TFkc4FNM14oqCMCuhhcnkZctnf6Du', 1, 'Brochure 1', 5, 'You can download all the application that available in your plan.', 'attachment_08072020013117_CJRAIVTwHm.jpg', 1, 0, 0, '', 0, '', '', 1, '2020-06-17 20:58:36', '2020-09-29 12:38:07'),
(2, 'Q9KIAS9yh1Nr3bfYk4fiN6AHInvO0p', 1, 'new test yrty222', 5, 'ryrtyrtyrty222', 'attachment_08072020013442_gHqriSeync.jpg', 1, 0, 0, '', 0, '', '', 1, '2020-07-06 13:03:13', '2020-09-29 12:38:12'),
(3, 'tDoJdkdHsqTlADSHesWHQqSgNYhVMl', 1, 'Nirala Appartment', 2, '', 'attachment_06072020010334_MjkeKpHX70.pdf', 1, 0, 0, '', 1, '2020-09-13 01:04:39', 'HFGHFGH', 1, '2020-07-06 13:03:34', '2020-09-13 01:04:39'),
(4, 'GW13yLUVvCRjuSmdpsENyL1dkLPRIZ', 1, 'fsdf', 5, 'dfsdf', 'attachment_13092020010734_IgJZoZqXcz.pdf', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:07:34', '2020-09-29 12:38:16'),
(5, 'XXKYOZIaC2lGWoQrM9JDcxbbf3sje1', 1, 'Nirala Appartment', 1, '', 'attachment_22092020121117_9HdSa74OCg.pdf', 1, 0, 0, '', 0, '', '', 1, '2020-09-22 12:11:17', '2020-09-22 12:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_items`
--

CREATE TABLE IF NOT EXISTS `property_document_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `document_id` int(11) NOT NULL,
  `document_list_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `doc_type` varchar(250) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `doc_status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `property_document_list_items`
--

INSERT INTO `property_document_list_items` (`id`, `project_id`, `document_id`, `document_list_id`, `property_id`, `doc_name`, `description`, `document_file`, `doc_type`, `status`, `doc_status`, `added_by`, `created_at`, `updated_at`) VALUES
(17, 1, 1, 1, 1, 'EB Form', 'test', 'UZrLDwsSET28092020091658sample.pdf', 'pdf', 1, 1, 1, '2020-07-07 21:40:02', '2020-09-28 21:19:23'),
(18, 1, 1, 2, 1, 'EB Bill', 'test', '', '', 1, 0, 1, '2020-07-07 21:40:02', '2020-09-25 10:34:29'),
(19, 1, 1, 1, 5, 'EB Form', 'test', '', '', 1, 1, 1, '2020-07-17 00:24:08', '2020-09-26 01:58:45'),
(20, 1, 1, 2, 5, 'EB Bill', 'test', '', '', 1, 1, 1, '2020-07-17 00:24:08', '2020-09-26 01:59:37'),
(21, 1, 2, 3, 5, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(22, 1, 2, 4, 5, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(23, 1, 5, 10, 5, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(24, 1, 1, 1, 10, 'EB Form', 'test', '', NULL, 1, 0, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32'),
(25, 1, 1, 2, 10, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32'),
(26, 1, 1, 1, 14, 'EB Form', 'test', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(27, 1, 1, 2, 14, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(28, 1, 2, 3, 14, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(29, 1, 2, 4, 14, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(30, 1, 5, 10, 14, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(31, 1, 6, 11, 14, 'te', 'tes', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(32, 1, 7, 12, 14, 'Construction Agreement', 'Construction Agreement', '', NULL, 1, 0, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(33, 1, 1, 1, 15, 'EB Form', 'test', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(34, 1, 1, 2, 15, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(35, 1, 2, 3, 15, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(36, 1, 2, 4, 15, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(37, 1, 5, 10, 15, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(38, 1, 6, 11, 15, 'te', 'tes', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(39, 1, 7, 12, 15, 'Construction Agreement', 'Construction Agreement', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(40, 1, 8, 13, 15, 'Tri', 'Agree', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(41, 1, 9, 14, 15, 'Sale', 'Deed', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(42, 1, 10, 15, 15, 'Bul', 'De', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(43, 1, 11, 16, 15, 'Tax', 'book', '', NULL, 1, 0, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(44, 1, 1, 1, 16, 'EB Form', 'test', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(45, 1, 1, 2, 16, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(46, 1, 2, 3, 16, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(47, 1, 2, 4, 16, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(48, 1, 5, 10, 16, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(49, 1, 6, 11, 16, 'te', 'tes', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(50, 1, 7, 12, 16, 'Construction Agreement', 'Construction Agreement', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(51, 1, 8, 13, 16, 'Tri', 'Agree', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(52, 1, 9, 14, 16, 'Sale', 'Deed', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(53, 1, 10, 15, 16, 'Bul', 'De', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(54, 1, 11, 16, 16, 'Tax', 'book', '', NULL, 1, 0, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(55, 1, 1, 1, 17, 'EB Form', 'test', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(56, 1, 1, 2, 17, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(57, 1, 2, 3, 17, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(58, 1, 2, 4, 17, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(59, 1, 5, 10, 17, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(60, 1, 6, 11, 17, 'te', 'tes', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(61, 1, 7, 12, 17, 'Construction Agreement', 'Construction Agreement', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(62, 1, 8, 13, 17, 'Tri', 'Agree', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(63, 1, 9, 14, 17, 'Sale', 'Deed', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(64, 1, 10, 15, 17, 'Bul', 'De', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(65, 1, 11, 16, 17, 'Tax', 'book', '', NULL, 1, 0, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(66, 1, 1, 1, 19, 'EB Form', 'test', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(67, 1, 1, 2, 19, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(68, 1, 2, 3, 19, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(69, 1, 2, 4, 19, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(70, 1, 5, 10, 19, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(71, 1, 6, 11, 19, 'te', 'tes', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(72, 1, 7, 12, 19, 'Construction Agreement', 'Construction Agreement', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(73, 1, 8, 13, 19, 'Tri', 'Agree', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(74, 1, 9, 14, 19, 'Sale', 'Deed', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(75, 1, 10, 15, 19, 'Bul', 'De', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(76, 1, 11, 16, 19, 'Tax', 'book', '', NULL, 1, 0, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(77, 1, 6, 11, 5, 'te', 'tes', '', NULL, 1, 0, 1, '2021-09-15 13:27:48', '2021-09-15 13:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_tbl`
--

CREATE TABLE IF NOT EXISTS `property_document_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL,
  `documents_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `property_document_list_tbl`
--

INSERT INTO `property_document_list_tbl` (`id`, `project_id`, `property_id`, `documents_id`, `type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(10, 1, 1, 1, 'common', 1, 1, '2020-07-07 21:40:02', '2020-07-07 21:40:02'),
(11, 1, 5, 1, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(12, 1, 5, 2, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(13, 1, 5, 5, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(14, 1, 10, 1, 'common', 1, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32'),
(15, 1, 14, 1, 'common', 1, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(16, 1, 14, 2, 'common', 1, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(17, 1, 14, 5, 'common', 1, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(18, 1, 14, 6, 'common', 1, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(19, 1, 14, 7, 'common', 1, 1, '2021-08-24 18:18:04', '2021-08-24 18:18:04'),
(20, 1, 15, 1, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(21, 1, 15, 2, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(22, 1, 15, 5, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(23, 1, 15, 6, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(24, 1, 15, 7, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(25, 1, 15, 8, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(26, 1, 15, 9, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(27, 1, 15, 10, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(28, 1, 15, 11, 'common', 1, 1, '2021-09-02 11:17:40', '2021-09-02 11:17:40'),
(29, 1, 16, 1, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(30, 1, 16, 2, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(31, 1, 16, 5, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(32, 1, 16, 6, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(33, 1, 16, 7, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(34, 1, 16, 8, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(35, 1, 16, 9, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(36, 1, 16, 10, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(37, 1, 16, 11, 'common', 1, 1, '2021-09-02 11:20:02', '2021-09-02 11:20:02'),
(38, 1, 17, 1, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(39, 1, 17, 2, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(40, 1, 17, 5, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(41, 1, 17, 6, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(42, 1, 17, 7, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(43, 1, 17, 8, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(44, 1, 17, 9, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(45, 1, 17, 10, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(46, 1, 17, 11, 'common', 1, 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(47, 1, 19, 1, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(48, 1, 19, 2, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(49, 1, 19, 5, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(50, 1, 19, 6, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(51, 1, 19, 7, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(52, 1, 19, 8, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(53, 1, 19, 9, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(54, 1, 19, 10, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(55, 1, 19, 11, 'common', 1, 1, '2021-09-15 11:00:31', '2021-09-15 11:00:31'),
(56, 1, 5, 6, 'common', 1, 1, '2021-09-15 13:27:48', '2021-09-15 13:27:48');

-- --------------------------------------------------------

--
-- Table structure for table `property_gallery_tbl`
--

CREATE TABLE IF NOT EXISTS `property_gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `property_gallery_tbl`
--

INSERT INTO `property_gallery_tbl` (`id`, `project_id`, `property_id`, `images`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(4, 1, 5, 'CoceRBuy8pOE427SFkhf9xaNLPQsrY.jpg', 1, 1, '2021-07-27 17:26:10', '2021-07-27 17:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `property_gendral_documents_tbl`
--

CREATE TABLE IF NOT EXISTS `property_gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `property_gendral_documents_tbl`
--

INSERT INTO `property_gendral_documents_tbl` (`id`, `token`, `project_id`, `property_id`, `title`, `description`, `documents`, `document_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'fSwPEal3CLVVJvj8Cjut94JIyhRlVV', 1, 1, 'Nirala Appartment', 'FGHFHFGH', '', 'pdf', 1, 1, '2020-06-16 23:35:09', '2020-09-29 00:55:40'),
(2, '2g1uY36GbFiHJOTk2BrF24NGDfs0HO', 1, 1, 'sample test', '', '', '', 1, 1, '2020-06-16 23:37:18', '2020-09-29 00:50:59'),
(3, 'lPP546Bmrs3ekNzEvvDYB9SlkfKyJd', 1, 1, 'owner', 'test', '', 'jpg', 1, 1, '2020-06-29 11:27:50', '2020-09-29 00:55:46'),
(4, 'hnAw2pvMtSSsUL2wjePDHV595h64Xl', 1, 1, 'Nirala Appartment', 'asdasd', '01IapURg6025092020103523image-1.png', 'png', 1, 1, '2020-09-24 19:37:06', '2020-09-25 10:35:23'),
(5, 'plBVBGQ4VjCYbxuxWm3NhWrnGaK5XO', 1, 1, 'saS', 'ASA', 'W0mOgh0OH329092020010110availability.pdf', 'pdf', 1, 1, '2020-09-24 19:38:50', '2020-09-29 01:01:14'),
(6, 't7IpGGnMNlGn4wv5wI3V44IC79j5SE', 1, 1, 'wwwwwwwwwwww', 'wwwwwwww', '', '', 1, 1, '2020-09-24 19:38:58', '2020-09-29 00:57:44'),
(7, 'FNqJmKmOYgX5Z9nelbafzaDF0YVnM1', 1, 1, 'yu', 'iyuiyui', '', 'pdf', 1, 1, '2020-09-25 10:32:45', '2020-09-29 00:57:12'),
(8, 'U3w1qEIAjUeXGgsPxyb8nlhVdlfuqE', 1, 1, 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'yyyyyyyyyy', '', '', 1, 1, '2020-09-25 10:32:54', '2020-09-28 09:37:45'),
(9, 'HVqQ1L7uG5gUg6uXbBkPLm2XMwpgPK', 1, 1, 'qqqqqqqqqqqqqqqqq', 'qqqqqqqqqqqq', '', '', 1, 1, '2020-09-25 10:33:19', '2020-09-28 09:37:27'),
(10, 'DMAML629ugaLDgHzqeoLpl9gk6A8IL', 1, 1, 'aaaaaaaaaaa', 'aaaaaaaaaaa', '', '', 1, 1, '2020-09-25 10:34:43', '2020-09-28 09:26:56'),
(11, '7lyw3bnNmbxY1Sd40SmOTGxqrWyriv', 1, 1, 'sdfs', 'fsdfsd', '', '', 1, 1, '2020-09-25 10:43:37', '2020-09-25 10:43:37'),
(12, 'GuF6MHmE5tNx3Puq49bD3l7u0bf19z', 1, 5, 'sample', 'sa', '7hnDhBghoE26092020022039fileexamplexls10.xls', 'xls', 1, 1, '2020-09-25 10:46:50', '2020-09-26 02:20:39'),
(13, 'Uo4e5zKCV8WOut4keYBtU10LIRbgJ5', 1, 1, 'sdf', 'sfsdf', '', '', 1, 1, '2020-09-25 10:49:33', '2020-09-25 10:49:33'),
(14, 'NwnLRlnzmR7ScgnzH8ZLv0cR6kutSe', 1, 1, 'sdf', 'sfsdf', '', '', 1, 1, '2020-09-25 10:49:40', '2020-09-25 10:49:40'),
(15, '1fyizjEqPt2UKCKBTimclUoCaRi40E', 1, 1, 'Nirala Appartment', 'tyutu', 'attachment_26092020123830_y7eQGziY4B.docx', 'docx', 1, 1, '2020-09-26 00:38:30', '2020-09-26 00:38:30'),
(16, 'jHEVRLK1IMczhGHEgJey9diFWI09A2', 1, 5, 'Stop mode', 's', '', '', 1, 1, '2020-09-26 01:16:42', '2020-09-26 02:18:49'),
(17, 'O3ywyvWaV18hRlzSkvV2s2ZSw6JxE2', 1, 5, 'ty', 'u', '', '', 1, 1, '2020-09-26 01:17:32', '2020-09-26 02:20:31'),
(18, 'hk0DORGEmqoKeTit6OHmf7Nqrjw9Pr', 1, 1, 'Nirala Appartment', '', '', '', 1, 1, '2020-09-27 17:26:51', '2020-09-27 17:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `property_payment_schedule_list_tbl`
--

CREATE TABLE IF NOT EXISTS `property_payment_schedule_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL DEFAULT '0',
  `payment_schedule_id` int(11) NOT NULL DEFAULT '0',
  `payed_date` date DEFAULT NULL,
  `stage_id` int(11) DEFAULT '0',
  `percentage` float DEFAULT '0',
  `amt_payable` float DEFAULT '0',
  `amt_receive` float DEFAULT '0',
  `balance` float DEFAULT '0',
  `due_date` date DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `property_payment_schedule_list_tbl`
--

INSERT INTO `property_payment_schedule_list_tbl` (`id`, `tenant_id`, `project_id`, `property_id`, `payment_schedule_id`, `payed_date`, `stage_id`, `percentage`, `amt_payable`, `amt_receive`, `balance`, `due_date`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 16, 1, '2021-09-21', 1, 10, 6000000, 600000, 5400000, '2021-10-21', 1, 1, '2021-09-21 09:23:38', '2021-09-21 09:23:38'),
(2, 1, 1, 16, 1, '2021-09-21', 2, 10, 5400000, 600000, 4800000, '2021-10-21', 1, 1, '2021-09-21 09:40:29', '2021-09-21 09:40:29'),
(3, 1, 1, 16, 1, '2021-09-21', 3, 10, 4800000, 600000, 4200000, '2021-10-21', 1, 1, '2021-09-21 09:46:23', '2021-09-21 09:46:23'),
(4, 1, 1, 16, 1, '2021-09-21', 4, 10, 4200000, 600000, 3600000, '2021-10-21', 1, 1, '2021-09-21 09:46:40', '2021-09-21 09:46:40'),
(5, 1, 1, 16, 1, '2021-09-21', 5, 10, 3600000, 600000, 3000000, '2021-09-22', 1, 1, '2021-09-21 09:49:08', '2021-09-21 09:49:08'),
(6, 1, 1, 16, 1, '2021-09-21', 6, 10, 3000000, 600000, 2400000, '2021-09-22', 1, 1, '2021-09-21 09:51:24', '2021-09-21 09:51:24'),
(7, 1, 1, 16, 1, '2021-09-21', 7, 10, 2400000, 600000, 1800000, '2021-09-22', 1, 1, '2021-09-21 09:52:41', '2021-09-21 09:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `property_payment_schedule_tbl`
--

CREATE TABLE IF NOT EXISTS `property_payment_schedule_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL DEFAULT '0',
  `amt_payable` float DEFAULT '0',
  `amt_receive` float DEFAULT '0',
  `balance` float DEFAULT '0',
  `due_date` date DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `property_payment_schedule_tbl`
--

INSERT INTO `property_payment_schedule_tbl` (`id`, `tenant_id`, `project_id`, `property_id`, `amt_payable`, `amt_receive`, `balance`, `due_date`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 16, 6000000, 4200000, 1800000, '2021-10-22', 1, 1, '2021-09-21 09:22:14', '2021-09-21 09:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `property_rooms_list_tbl`
--

CREATE TABLE IF NOT EXISTS `property_rooms_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL,
  `roomlist_id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `property_rooms_list_tbl`
--

INSERT INTO `property_rooms_list_tbl` (`id`, `project_id`, `property_id`, `roomlist_id`, `image`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 2, 'mv8LG_Hxwzja4kCV1XZWlpOQfMiPSh.jpg', 1, 1, '2020-06-18 12:19:56', '2020-06-18 12:19:56'),
(4, 1, 1, 2, 'GTE3sJU9au7lrFcV8WBXCdZQLY0i5h.jpg', 1, 1, '2020-06-18 12:19:56', '2020-06-18 12:19:56'),
(5, 1, 1, 1, 'wHysBS79_CA43MoEmWIpNPQDvajF2X.jpeg', 1, 1, '2020-06-18 17:51:31', '2020-06-18 17:51:31'),
(6, 1, 1, 1, 'QITkYrEsSfWqMBOjJalR124nzgN7cK.jpeg', 1, 1, '2020-06-18 17:51:31', '2020-06-18 17:51:31'),
(9, 1, 1, 1, '_sZY5FWGTn2gz7rRykDcx0fqM4Uw1O.jpg', 1, 1, '2020-09-27 17:26:31', '2020-09-27 17:26:31'),
(11, 1, 5, 5, 'zdopGt4jRVweFicm0HQTWnJ5v1DkZC.jpg', 1, 1, '2021-07-27 17:48:40', '2021-07-27 17:48:40'),
(13, 1, 5, 9, 'Ri3Gwcgvo_pHmkr2V17KXW5qNxujdf.jpeg', 1, 1, '2021-07-27 17:53:21', '2021-07-27 17:53:21'),
(16, 1, 5, 12, 'I2rjd6sBtQ4of7pHy5ig1DmMYRhNvA.jpg', 1, 1, '2021-07-27 17:57:41', '2021-07-27 17:57:41'),
(17, 1, 5, 11, 'ioQrkgEYy8M162XK_nATSB57hx3zwq.jpg', 1, 1, '2021-07-27 18:02:11', '2021-07-27 18:02:11'),
(18, 1, 5, 10, 'TnrkmK1Ax0URGs5XNtf8J6oZSuHMl4.jpg', 1, 1, '2021-07-27 18:03:49', '2021-07-27 18:03:49'),
(19, 1, 5, 8, 'IlJ_RLSQMihe9PEs8q6DpoUkKcmVGN.jpeg', 1, 1, '2021-07-27 18:06:21', '2021-07-27 18:06:21');

-- --------------------------------------------------------

--
-- Table structure for table `property_tbl`
--

CREATE TABLE IF NOT EXISTS `property_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(11) DEFAULT NULL,
  `allotment_date` varchar(250) NOT NULL,
  `assign_remarks` longtext NOT NULL,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `house_facing` varchar(250) NOT NULL,
  `projectsize` varchar(250) NOT NULL,
  `projectarea` varchar(250) NOT NULL,
  `cost` varchar(250) NOT NULL,
  `room_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `area_type_id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `assign_status` int(11) NOT NULL,
  `cancel_status` int(11) NOT NULL,
  `reason_cancellation` varchar(250) NOT NULL,
  `cancel_remarks` longtext NOT NULL,
  `cancel_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `property_tbl`
--

INSERT INTO `property_tbl` (`id`, `project_id`, `property_id`, `customer_id`, `allotment_date`, `assign_remarks`, `token`, `title`, `type`, `house_facing`, `projectsize`, `projectarea`, `cost`, `room_id`, `block_id`, `floor_id`, `area_type_id`, `address`, `city`, `state`, `pincode`, `content`, `image`, `assign_status`, `cancel_status`, `reason_cancellation`, `cancel_remarks`, `cancel_by`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 2, '', '', 'nirala-appartment', 'Flat-1', 'flat', 'east', '123', '2 acr', '1400000', 1, 1, 2, 0, '99A vyasuva building', 'coimbatore', 'tamil nadu', '641108', 'sa', 'Y4pQTeZuZq07072020085135converting-auto-cad-2d-files-to-3d-cad-modeling-files-500x500.jpg', 1, 1, 'test', 'test', 1, 1, 0, 0, '', 1, '2020-09-24 12:01:30', 'sample', 1, '2020-06-16 10:42:01', '2020-10-16 11:06:18'),
(2, 1, 0, 4, '', '', 'nirala-appartment.2', 'Flat-2', 'flat', 'north', '123', '2 acr', '50000', 1, 1, 2, 0, '99A vyasuva building', 'COIMBATIORE', 'tamil nadu', '641108', '', '7pW5B7fbda22062020081135whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 1, 1, '2020-10-16 11:10:43', 0, '', '', 1, '2020-06-22 20:11:35', '2020-09-24 12:01:34'),
(5, 1, 0, 1, '', '', 'a1', 'Flat-1a', 'flat', 'east', '122', '2 acr', '11313', 3, 2, 3, 1, '', '', '', '', '', 'TzSdhXmKST27072021053009logo29docs150721v1-01.png', 1, 0, '', '', 0, 1, 0, 0, '', 1, '2020-09-29 12:32:43', 's', 1, '2020-07-17 00:24:08', '2021-09-15 13:27:48'),
(6, 1, 0, 1, '', '', 'a4', 'Flat-4', 'flat', 'north', '1234', '2 acr', '65464', 2, 1, 2, 1, '', '', '', '', '', 'sDlRnfcdHt17072020110021whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 1, 1, '2021-07-24 15:49:29', 0, '', '', 1, '2020-07-17 11:00:21', '2021-07-24 15:47:43'),
(7, 1, 0, 1, '', '', 'a5', 'Flat-5', 'flat', 'east', '123', '2 acr', '666546', 2, 1, 2, 0, '', '', '', '', '', 'gLPVRsdM3z17072020110047whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 1, 1, '2021-07-24 15:54:05', 0, '', '', 1, '2020-07-17 11:00:47', '2020-10-08 13:50:01'),
(8, 1, 0, 1, '2020-10-08', 'adasasd', 'nirala-appartment-fsdfs', 'Flat-6', 'flat', 'east', 'sdfsdf', 'sdfsd', '654', 1, 1, 2, 0, '', '', '', '', '', '4YIjV4p8aF22072020080319whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 1, 1, '2021-07-24 15:54:10', 0, '', '', 1, '2020-07-22 20:03:19', '2020-10-08 14:03:58'),
(11, 1, 0, 4, '2021-07-25', '', 'flat-1', 'Flat-2c', 'flat', 'west', '123', '2 acr', '1400000', 3, 1, 2, 1, '99A vyasuva building', 'coimbatore', 'tamil nadu', '641108', 'sa', 'Y4pQTeZuZq07072020085135converting-auto-cad-2d-files-to-3d-cad-modeling-files-500x500.jpg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2020-10-16 11:06:18', '2021-07-26 15:20:02'),
(12, 1, 0, 2, '2021-07-27', 'sample', 'flat-1b', 'Flat-1b', 'flat', 'north', '2 acr', 'city outer', '1', 3, 4, 4, 1, '', '', '', '', '', 'h8i6zx43GC24072021035120living-room.jpg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-07-24 15:51:20', '2021-07-27 19:06:50'),
(13, 1, 0, 21, '2021-08-24', 'Done', 'flat-2d', 'Flat-2d', 'flat', 'south', '1acr', 'city outer', '50', 7, 3, 2, 1, '', '', '', '', '', 'bMrgUQsxTq26072021120310144x105-01-29docs-logo.png', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-07-26 12:03:10', '2021-08-24 18:10:38'),
(14, 1, 0, 21, '2021-08-24', 'Payment done', '212', '212', 'flat', 'south', '500*500', '1500', '1200000', 2, 4, 3, 2, '', '', '', '', '', 'BZKuX6edFX240820210618041528397457-4687.jpg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-08-24 18:18:04', '2021-08-24 18:23:07'),
(15, 1, 0, 2, '2021-09-02', 'Payment done', '211', '211', 'flat', 'east', '500*500', '1500', '5000000', 3, 1, 2, 1, '12 b', '', 'Chiacgo', '012345', '', 'PTkA1lbAth020920211117401528397457-4687.jpg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-09-02 11:17:40', '2021-09-02 15:18:04'),
(16, 1, 0, 22, '2021-09-07', 'payment done', '210', '210', 'flat', 'west', '500*500', '1500', '40000000', 2, 1, 4, 2, '12 b', '', 'Chiacgo', '012345', '', 'MyjhNZv5Xz02092021112002unnamed.jpg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-09-02 11:20:02', '2021-09-02 18:50:01'),
(17, 1, 0, 0, '', '', '209', '209', 'flat', 'west', '500*500', '1500', '4500000', 2, 2, 4, 1, 'Coimbatore', '', '', '012345', '', 'jFOoISL4QK130920210915241528397457-4687.jpg', 0, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-09-13 09:15:24', '2021-09-13 09:15:24'),
(18, 1, 0, 0, '', '', '208', '208', 'flat', 'east', '500*500', '1500', '5000000', 3, 2, 3, 1, '', '', '', '', '', 'aw0lozKr9B13092021091738looking-to-buy-1bhk-flats-in-pune-consider-these.jpg', 0, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-09-13 09:17:38', '2021-09-13 09:17:38'),
(19, 1, 0, 2, '2021-09-01', 'Test', 'test-flat-', 'Test Flat', 'flat', 'east', '1200', '5000', '5000000', 2, 1, 2, 1, '', '', '', '', '', 'FJPPJylHo215092021110031whatsapp-image-2021-09-07-at-1.14.39-pm.jpeg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2021-09-15 11:00:31', '2021-09-15 11:00:52');

-- --------------------------------------------------------

--
-- Table structure for table `request_type_tbl`
--

CREATE TABLE IF NOT EXISTS `request_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `request_type` varchar(250) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `request_type_tbl`
--

INSERT INTO `request_type_tbl` (`id`, `token`, `project_id`, `request_type`, `department_id`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'request1', 1, 'request 1', 1, 1, 1, '2020-06-10 11:34:39', '2020-10-23 09:56:15'),
(2, 'request2', 1, 'Request 2', 2, 1, 1, '2020-06-19 11:21:25', '2020-10-23 09:56:22'),
(3, 'request3', 1, 'Request 3', 2, 1, 1, '2020-06-19 11:21:33', '2020-10-23 09:56:27'),
(4, 'request4', 1, 'Request 4', 1, 1, 1, '2020-10-23 09:42:20', '2020-10-23 09:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

CREATE TABLE IF NOT EXISTS `session_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logged_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `portal_type` varchar(100) NOT NULL,
  `auth_referer` varchar(50) NOT NULL,
  `auth_medium` varchar(50) NOT NULL,
  `auth_user_agent` text NOT NULL,
  `auth_ip_address` varchar(250) NOT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=485 ;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(75, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 18:56:34', NULL),
(76, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 19:07:56', NULL),
(77, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 19:09:23', NULL),
(78, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-27 15:58:14', NULL),
(79, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-27 15:58:29', NULL),
(80, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-28 06:31:35', NULL),
(81, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-28 06:31:36', NULL),
(82, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '::1', '2020-06-01 12:38:22', NULL),
(83, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '::1', '2020-06-01 12:38:23', NULL),
(84, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-06-02 09:49:55', NULL),
(85, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-06-02 09:49:57', NULL),
(86, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-03 08:06:54', NULL),
(87, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-03 08:06:55', NULL),
(88, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-04 13:41:15', NULL),
(89, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-04 13:41:15', NULL),
(90, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-05 11:10:46', NULL),
(91, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-05 11:10:48', NULL),
(92, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-06 07:57:00', NULL),
(93, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-08 10:03:37', NULL),
(94, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-08 10:03:38', NULL),
(95, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-09 21:16:28', NULL),
(96, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-10 11:22:05', NULL),
(97, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-10 11:22:07', NULL),
(98, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 10:08:20', NULL),
(99, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 10:08:22', NULL),
(100, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 12:15:14', NULL),
(101, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 19:03:22', NULL),
(102, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-15 10:05:08', NULL),
(103, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-16 09:48:47', NULL),
(104, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-17 09:36:11', NULL),
(105, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 11:24:11', NULL),
(106, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 19:39:46', NULL),
(107, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 20:48:54', NULL),
(108, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:32:36', NULL),
(109, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:35:13', NULL),
(110, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:35:15', NULL),
(111, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-19 10:02:53', NULL),
(112, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-19 11:21:11', NULL),
(113, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-20 10:03:18', NULL),
(114, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-20 11:24:36', NULL),
(115, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-22 08:58:22', NULL),
(116, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-22 08:59:04', NULL),
(117, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-25 15:51:01', NULL),
(118, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-26 10:37:36', NULL),
(119, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-27 10:06:09', NULL),
(120, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:42:55', NULL),
(121, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:57:57', NULL),
(122, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:57:58', NULL),
(123, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-30 09:02:53', NULL),
(124, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-30 09:07:32', NULL),
(125, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-07-01 19:39:52', NULL),
(126, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-02 13:40:49', NULL),
(127, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-02 19:03:59', NULL),
(128, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 10:18:26', NULL),
(129, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 10:18:27', NULL),
(130, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 11:53:00', NULL),
(131, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 11:53:01', NULL),
(132, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 14:08:35', NULL),
(133, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 14:08:57', NULL),
(134, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 09:20:44', NULL),
(135, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 09:20:45', NULL),
(136, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 11:42:57', NULL),
(137, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-06 10:18:18', NULL),
(138, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-06 10:18:47', NULL),
(139, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-07 10:08:53', NULL),
(140, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-07 11:03:43', NULL),
(141, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 15:31:56', NULL),
(142, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:40:11', NULL),
(143, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:40:12', NULL),
(144, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:42:47', NULL),
(145, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-09 23:05:57', NULL),
(146, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-09 23:05:58', NULL),
(147, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-13 11:55:29', NULL),
(148, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 10:12:03', NULL),
(149, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 10:12:04', NULL),
(150, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 19:41:20', NULL),
(151, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:39:33', NULL),
(152, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:56:36', NULL),
(153, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:56:40', NULL),
(154, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-16 10:05:29', NULL),
(155, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-16 21:29:03', NULL),
(156, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-17 09:47:41', NULL),
(157, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-18 15:07:56', NULL),
(158, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-22 17:55:58', NULL),
(159, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-24 20:45:18', NULL),
(160, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-25 11:10:29', NULL),
(161, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-25 12:39:10', NULL),
(162, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:09', NULL),
(163, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:28', NULL),
(164, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:29', NULL),
(165, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', '::1', '2020-07-28 12:42:00', NULL),
(166, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 13:40:14', NULL),
(167, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:14:38', NULL),
(168, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:15:57', NULL),
(169, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:30:05', NULL),
(170, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-03 09:05:05', NULL),
(171, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 15:02:33', NULL),
(172, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:15:37', NULL),
(173, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:01', NULL),
(174, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:40', NULL),
(175, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:56', NULL),
(176, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 10:59:58', NULL),
(177, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:11:20', NULL),
(178, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:12:06', NULL),
(179, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:12:15', NULL),
(180, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-06 16:26:00', NULL),
(181, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-06 16:26:01', NULL),
(182, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-08 12:44:26', NULL),
(183, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-08 12:44:36', NULL),
(184, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-12 10:34:54', NULL),
(185, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-13 09:46:19', NULL),
(186, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-14 10:43:11', NULL),
(187, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-14 10:43:12', NULL),
(188, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-16 20:59:32', NULL),
(189, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-17 18:35:14', NULL),
(190, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-18 07:33:14', NULL),
(191, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-18 09:58:50', NULL),
(192, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-19 08:24:21', NULL),
(193, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-20 16:14:06', NULL),
(194, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-22 10:31:20', NULL),
(195, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-22 10:31:21', NULL),
(196, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-23 18:11:00', NULL),
(197, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-23 22:17:22', NULL),
(198, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-27 09:27:36', NULL),
(199, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '127.0.0.1', '2020-08-28 10:48:44', NULL),
(200, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-01 10:11:21', NULL),
(201, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-02 10:24:30', NULL),
(202, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-02 10:24:33', NULL),
(203, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-05 11:59:05', NULL),
(204, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-05 11:59:06', NULL),
(205, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-07 23:52:04', NULL),
(206, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-07 23:52:05', NULL),
(207, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-08 11:29:02', NULL),
(208, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-10 12:05:01', NULL),
(209, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-10 22:07:55', NULL),
(210, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-10 22:53:49', NULL),
(211, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-11 10:12:41', NULL),
(212, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-12 11:24:15', NULL),
(213, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-14 10:43:03', NULL),
(214, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-14 10:43:04', NULL),
(215, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-15 10:50:15', NULL),
(216, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-15 10:50:16', NULL),
(217, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 10:45:10', NULL),
(218, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 12:29:33', NULL),
(219, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 12:30:15', NULL),
(220, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-18 09:45:16', NULL),
(221, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-20 21:59:46', NULL),
(222, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-21 08:59:02', NULL),
(223, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-22 09:27:48', NULL),
(224, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-22 11:55:06', NULL),
(225, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-23 11:28:52', NULL),
(226, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-23 12:03:34', NULL),
(227, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-24 09:42:17', NULL),
(228, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-24 09:42:19', NULL),
(229, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-25 10:32:36', NULL),
(230, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-25 11:44:25', NULL),
(231, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-25 23:14:08', NULL),
(232, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-26 10:22:08', NULL),
(233, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-28 09:02:11', NULL),
(234, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 10:32:08', NULL),
(235, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:03:21', NULL),
(236, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:34:34', NULL),
(237, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:35:48', NULL),
(238, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:37:08', NULL),
(239, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:37:34', NULL),
(240, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 15:25:21', NULL),
(241, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 15:28:24', NULL),
(242, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-30 08:31:43', NULL),
(243, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-30 08:36:44', NULL),
(244, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-01 09:58:26', NULL),
(245, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-01 09:58:31', NULL),
(246, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-02 12:26:29', NULL),
(247, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 12:37:58', NULL),
(248, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 13:26:32', NULL),
(249, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 13:26:32', NULL),
(250, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-06 09:11:17', NULL),
(251, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-08 10:25:23', NULL),
(252, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-08 11:08:04', NULL),
(253, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:08:20', NULL),
(254, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:46:40', NULL),
(255, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 00:54:22', NULL),
(256, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:56:19', NULL),
(257, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:56:36', NULL),
(258, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:57:15', NULL),
(259, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 10:11:26', NULL),
(260, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:08:22', NULL),
(261, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:10:31', NULL),
(262, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:10:55', NULL),
(263, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:13:01', NULL),
(264, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:17:26', NULL),
(265, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:17:47', NULL),
(266, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:51:44', NULL),
(267, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-10 11:06:01', NULL),
(268, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:51', NULL),
(269, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:56', NULL),
(270, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:57', NULL),
(271, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-13 10:18:46', NULL),
(272, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 10:34:28', NULL),
(273, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:40:11', NULL),
(274, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:44:16', NULL),
(275, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:44:55', NULL),
(276, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:09:52', NULL),
(277, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:09:53', NULL),
(278, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:28:42', NULL),
(279, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 09:06:59', NULL),
(280, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:07:28', NULL),
(281, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:07:38', NULL),
(282, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:01', NULL),
(283, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:12', NULL),
(284, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:45', NULL),
(285, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 11:50:53', NULL),
(286, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-24 09:34:03', NULL),
(287, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-27 10:22:14', NULL),
(288, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-10-28 13:36:40', NULL),
(289, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-10-28 13:36:42', NULL),
(290, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-03 11:00:54', NULL),
(291, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-03 11:00:56', NULL),
(292, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-04 11:01:55', NULL),
(293, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-09 11:18:30', NULL),
(294, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-11-12 23:20:50', NULL),
(295, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-13 12:10:53', NULL),
(296, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '49.37.217.170', '2020-11-20 11:40:44', NULL),
(297, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '183.82.173.114', '2020-11-24 12:22:45', NULL),
(298, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '183.82.173.114', '2020-11-24 12:23:07', NULL),
(299, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '183.82.173.114', '2020-12-02 11:17:04', NULL),
(300, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', '183.82.173.114', '2020-12-02 11:17:30', NULL),
(301, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '106.222.121.190', '2021-03-03 11:37:57', NULL),
(302, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', '106.222.121.190', '2021-03-03 11:38:46', NULL),
(303, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.121.176', '2021-07-24 12:56:15', NULL),
(304, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.121.176', '2021-07-24 13:05:01', NULL),
(305, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.121.176', '2021-07-24 13:05:51', NULL),
(306, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.121.176', '2021-07-24 13:18:32', NULL),
(307, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', '49.206.117.131', '2021-07-24 13:57:35', NULL),
(308, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36', '49.206.117.131', '2021-07-24 14:14:00', NULL),
(309, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/90.0', '157.51.145.200', '2021-07-24 14:14:40', NULL),
(310, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.39.32', '2021-07-24 14:30:37', NULL),
(311, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', '43.225.165.36', '2021-07-26 10:36:49', NULL),
(312, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36', '49.206.112.67', '2021-07-26 11:23:04', NULL),
(313, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.39.32', '2021-07-26 12:39:03', NULL),
(314, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.113.63', '2021-07-27 10:49:24', NULL),
(315, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.39.32', '2021-07-27 11:30:44', NULL),
(316, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.113.177', '2021-07-27 11:38:10', NULL),
(317, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.113.63', '2021-07-27 11:41:07', NULL),
(318, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.113.177', '2021-07-27 12:33:32', NULL),
(319, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.113.63', '2021-07-27 15:09:23', NULL),
(320, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.113.63', '2021-07-27 15:10:33', NULL),
(321, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.34.241', '2021-07-27 18:49:55', NULL),
(322, 2, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.34.241', '2021-07-27 18:50:22', NULL),
(323, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.34.241', '2021-07-27 18:52:51', NULL),
(324, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:04:25', NULL),
(325, 2, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:04:35', NULL),
(326, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.34.241', '2021-07-27 19:06:10', NULL),
(327, 2, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:07:20', NULL),
(328, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:08:16', NULL),
(329, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.34.241', '2021-07-27 19:08:47', NULL),
(330, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:11:22', NULL),
(331, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.118', '2021-07-27 19:13:23', NULL),
(332, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.112.182', '2021-07-28 10:38:57', NULL),
(333, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.39.32', '2021-07-28 17:24:37', NULL),
(334, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.39.32', '2021-07-29 09:11:41', NULL),
(335, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.96.44.220', '2021-07-29 11:26:12', NULL),
(336, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.96.44.220', '2021-07-30 08:57:05', NULL),
(337, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.96.44.220', '2021-07-31 09:29:53', NULL),
(338, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.96.44.220', '2021-07-31 16:39:53', NULL),
(339, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.37.126', '2021-08-03 12:06:06', NULL),
(340, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.33.95', '2021-08-03 13:45:00', NULL),
(341, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.33.95', '2021-08-03 13:47:41', NULL),
(342, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '106.222.99.97', '2021-08-03 16:23:15', NULL),
(343, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '223.182.209.27', '2021-08-03 22:39:10', NULL),
(344, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.220.37.126', '2021-08-04 13:39:05', NULL),
(345, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.222.121.190', '2021-08-10 12:45:35', NULL),
(346, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.115.199', '2021-08-10 12:46:19', NULL),
(347, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '157.46.71.123', '2021-08-10 12:56:06', NULL),
(348, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.222.113.182', '2021-08-10 17:20:32', NULL),
(349, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.197.120.56', '2021-08-10 17:34:05', NULL),
(350, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.171', '2021-08-11 09:08:39', NULL),
(351, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.171', '2021-08-11 09:09:37', NULL),
(352, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.222.99.126', '2021-08-11 12:21:01', NULL),
(353, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '49.206.114.214', '2021-08-11 12:33:15', NULL),
(354, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.197.120.36', '2021-08-11 13:03:36', NULL),
(355, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-12 08:51:47', NULL),
(356, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-12 09:26:22', NULL);
INSERT INTO `session_tbl` (`id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(357, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-13 18:44:00', NULL),
(358, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', '122.174.33.21', '2021-08-13 21:55:09', NULL),
(359, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.222.99.97', '2021-08-13 21:59:02', NULL),
(360, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '106.222.99.97', '2021-08-13 22:04:31', NULL),
(361, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36', '122.174.33.21', '2021-08-13 22:05:45', NULL),
(362, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '157.49.207.92', '2021-08-13 22:08:46', NULL),
(363, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.32.253', '2021-08-14 10:40:01', NULL),
(364, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', '103.224.32.253', '2021-08-14 10:40:52', NULL),
(365, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '49.206.112.6', '2021-08-14 10:43:39', NULL),
(366, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '49.206.112.6', '2021-08-14 10:45:35', NULL),
(367, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-14 12:23:05', NULL),
(368, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-14 18:21:22', NULL),
(369, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-16 12:45:58', NULL),
(370, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-16 18:34:46', NULL),
(371, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-17 10:07:32', NULL),
(372, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-17 10:15:24', NULL),
(373, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-17 10:17:52', NULL),
(374, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.21.79.11', '2021-08-17 11:37:00', NULL),
(375, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 08:54:22', NULL),
(376, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 15:21:44', NULL),
(377, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 15:48:51', NULL),
(378, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 16:12:15', NULL),
(379, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 16:15:44', NULL),
(380, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36', '103.220.37.64', '2021-08-18 16:28:30', NULL),
(381, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-19 17:33:57', NULL),
(382, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-20 10:18:45', NULL),
(383, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-23 12:00:43', NULL),
(384, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-23 12:01:01', NULL),
(385, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '106.222.99.111', '2021-08-23 16:04:14', NULL),
(386, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 10:53:26', NULL),
(387, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 11:35:03', NULL),
(388, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '106.222.113.176', '2021-08-24 12:00:28', NULL),
(389, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '157.46.120.89', '2021-08-24 12:00:59', NULL),
(390, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '49.206.117.13', '2021-08-24 14:50:57', NULL),
(391, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 16:59:06', NULL),
(392, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '106.222.99.101', '2021-08-24 17:13:07', NULL),
(393, 21, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 18:09:16', NULL),
(394, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 18:52:04', NULL),
(395, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 18:53:42', NULL),
(396, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 18:58:47', NULL),
(397, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.220.37.64', '2021-08-24 18:59:06', NULL),
(398, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-08-30 12:23:39', NULL),
(399, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 10:54:46', NULL),
(400, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 14:35:33', NULL),
(401, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 15:13:47', NULL),
(402, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 15:27:04', NULL),
(403, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 16:49:50', NULL),
(404, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 16:50:57', NULL),
(405, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '106.197.120.48', '2021-09-02 16:52:38', NULL),
(406, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 16:56:51', NULL),
(407, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 16:57:21', NULL),
(408, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 17:00:24', NULL),
(409, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 17:05:16', NULL),
(410, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 17:05:38', NULL),
(411, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 18:32:20', NULL),
(412, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 18:44:22', NULL),
(413, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.96.46.155', '2021-09-02 18:48:17', NULL),
(414, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 09:05:35', NULL),
(415, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 09:25:57', NULL),
(416, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 09:30:58', NULL),
(417, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 09:31:27', NULL),
(418, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 09:35:56', NULL),
(419, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.46.155', '2021-09-03 15:45:41', NULL),
(420, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.224.33.75', '2021-09-06 11:08:09', NULL),
(421, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.224.33.75', '2021-09-06 11:20:13', NULL),
(422, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.224.33.75', '2021-09-06 16:11:19', NULL),
(423, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', '103.224.33.75', '2021-09-06 16:12:03', NULL),
(424, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.46.67.183', '2021-09-06 16:19:04', NULL),
(425, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-10 11:45:25', NULL),
(426, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-10 12:15:32', NULL),
(427, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-10 16:15:03', NULL),
(428, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-10 16:15:32', NULL),
(429, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-10 16:21:38', NULL),
(430, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.51.81.5', '2021-09-10 17:04:21', NULL),
(431, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 10:54:26', NULL),
(432, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 10:58:52', NULL),
(433, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:01:52', NULL),
(434, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:10:23', NULL),
(435, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:11:10', NULL),
(436, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:11:48', NULL),
(437, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:12:20', NULL),
(438, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:16:20', NULL),
(439, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:17:44', NULL),
(440, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:18:06', NULL),
(441, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.92.102', '2021-09-11 11:18:49', NULL),
(442, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.212.62', '2021-09-11 14:11:50', NULL),
(443, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.44.36', '2021-09-12 18:47:20', NULL),
(444, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.44.36', '2021-09-13 09:12:34', NULL),
(445, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.49.223.237', '2021-09-14 10:27:51', NULL),
(446, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.46.102.63', '2021-09-14 14:20:23', NULL),
(447, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.46.92.19', '2021-09-15 09:10:16', NULL),
(448, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.1.2 Safari/605.1.15', '106.198.125.140', '2021-09-15 10:33:30', NULL),
(449, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '42.106.84.48', '2021-09-15 10:39:35', NULL),
(450, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '42.106.84.48', '2021-09-15 10:40:11', NULL),
(451, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.197.120.62', '2021-09-15 10:50:37', NULL),
(452, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-15 10:51:30', NULL),
(453, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.222.121.183', '2021-09-15 10:52:01', NULL),
(454, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.197.120.62', '2021-09-15 10:52:07', NULL),
(455, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-15 10:56:02', NULL),
(456, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-15 11:05:50', NULL),
(457, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-15 11:13:25', NULL),
(458, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.222.121.183', '2021-09-15 11:13:32', NULL),
(459, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.224.33.75', '2021-09-15 12:12:44', NULL),
(460, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '157.51.147.235', '2021-09-15 13:34:24', NULL),
(461, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-15 17:34:27', NULL),
(462, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 08:44:22', NULL),
(463, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 08:46:29', NULL),
(464, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:03:14', NULL),
(465, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:04:50', NULL),
(466, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:12:38', NULL),
(467, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:14:45', NULL),
(468, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:15:53', NULL),
(469, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:18:04', NULL),
(470, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 09:25:42', NULL),
(471, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.197.120.47', '2021-09-16 10:16:04', NULL),
(472, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 10:45:55', NULL),
(473, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 10:47:19', NULL),
(474, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 10:48:42', NULL),
(475, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 11:13:57', NULL),
(476, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 11:57:16', NULL),
(477, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 12:01:51', NULL),
(478, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 12:02:43', NULL),
(479, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '103.96.47.195', '2021-09-16 12:03:34', NULL),
(480, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.197.120.47', '2021-09-16 12:07:56', NULL),
(481, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36', '106.197.120.47', '2021-09-16 12:09:25', NULL),
(482, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '103.96.47.195', '2021-09-17 10:24:21', NULL),
(483, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '49.206.115.160', '2021-09-20 11:32:05', NULL),
(484, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.82 Safari/537.36', '103.96.47.61', '2021-09-21 09:52:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stage_master_tbl`
--

CREATE TABLE IF NOT EXISTS `stage_master_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `token` varchar(200) DEFAULT NULL,
  `stage_name` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `stage_master_tbl`
--

INSERT INTO `stage_master_tbl` (`id`, `tenant_id`, `project_id`, `token`, `stage_name`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'booking', 'Booking', 1, 1, '2021-09-16 11:20:17', '2021-09-16 11:31:52'),
(2, 1, 1, 'basement', 'Basement', 1, 1, '2021-09-16 11:32:01', '2021-09-16 11:32:01'),
(3, 1, 1, 'stiltfloor', 'Stilt Floor', 1, 1, '2021-09-16 11:32:30', '2021-09-16 11:32:30'),
(4, 1, 1, '2ndfloorslab', '2nd Floor Slab', 1, 1, '2021-09-16 11:32:44', '2021-09-16 11:32:44'),
(5, 1, 1, '4thfloorslab', '4th Floor Slab', 1, 1, '2021-09-16 11:32:52', '2021-09-16 11:32:52'),
(6, 1, 1, '5thfloorslab', '5th Floor Slab', 1, 1, '2021-09-16 11:33:00', '2021-09-16 11:33:00'),
(7, 1, 1, '6thfloorslab', '6th Floor Slab', 1, 1, '2021-09-16 11:33:12', '2021-09-16 11:33:12'),
(8, 1, 1, '7thfloorslab', '7th Floor Slab', 1, 1, '2021-09-16 11:33:35', '2021-09-16 11:33:35'),
(9, 1, 1, '8thfloorslab', '8th Floor Slab', 1, 1, '2021-09-16 11:33:44', '2021-09-16 11:33:44'),
(10, 1, 1, 'brickwork', 'Brickwork', 1, 1, '2021-09-16 11:33:51', '2021-09-16 11:33:51'),
(11, 1, 1, 'tileswork', 'Tiles work', 1, 1, '2021-09-16 11:33:59', '2021-09-16 11:33:59'),
(12, 1, 1, 'finishing', 'Finishing', 1, 1, '2021-09-16 11:34:10', '2021-09-16 11:34:10');

-- --------------------------------------------------------

--
-- Table structure for table `temp_customer_excel_tbl`
--

CREATE TABLE IF NOT EXISTS `temp_customer_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_token` varchar(50) NOT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(250) NOT NULL,
  `addresss` varchar(255) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `temp_customer_excel_tbl`
--

INSERT INTO `temp_customer_excel_tbl` (`id`, `upload_token`, `project_id`, `name`, `addresss`, `mobile`, `email`, `created_at`, `updated_at`, `status`) VALUES
(1, 'We7WOWsJtNKP87V', 1, 'GOWARTHINI LC', '3/829 E, Sri Vigneshwara Nagar, 6th street , Palladam Road, Veerapandi post, Veerapandi, Tiruppur- 641605', '8870856666', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(2, 'We7WOWsJtNKP87V', 1, 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(3, 'We7WOWsJtNKP87V', 1, 'R.ARUNKUMAR', '1C,Golden Castle ,Kavetti Naidu Layout, Sowripalayam,Coimbatore-641028', '9944113440', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(4, 'We7WOWsJtNKP87V', 1, 'SUGUMAR MUTHUSAMY and RESHMI R', 'No.56,Karunanithi Nagar, Sowripalayam,Cbe-641028', '8197159292', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(5, 'We7WOWsJtNKP87V', 1, 'MRS.POORANI.K.T', '85,kalyani nagar,udumalaipettai,gandhi nagar,tirupur-642154', '9585523822', 'dhileepan62@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(6, 'We7WOWsJtNKP87V', 1, 'SADHASIVAM', 'B3, Lakshimipuram, Peelamedu, Coimbatore 641004', '9442367722', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(7, 'We7WOWsJtNKP87V', 1, 'DR RADHIKA', 'D21, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9363101831', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(8, 'We7WOWsJtNKP87V', 1, 'R.HARIRAJAN', '22/A, D.D.A MIG-Flats,Pocket-2,Sector-7,Dwarka(west) West Delhi-110075', '9818071456', 'rharirajan@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(9, 'We7WOWsJtNKP87V', 1, 'P.S VISHNOO VARDHAN', 'PLOT NO.13,4Th Cross Street, Siva Sakthi Nagar, Madhavaram-Chennai-600060', '8754589225', 'maddyvishnoo@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(10, 'We7WOWsJtNKP87V', 1, 'THIRUNAVAKARASU and T.ROOPESH', '2/54, Arasu paniyalar nagar, Vialnkuruchi post, Coimbatore- 641035', '9443914955', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(11, 'We7WOWsJtNKP87V', 1, 'RAJA KALYANA SUBRAMANIAN SAMBANTHAM', '701,Shanmuga Nagar, 7TH Cross,Uyakondan Thiramalai,Somasarasam Pettai, Trichy,-620102', '9842444466', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(12, 'We7WOWsJtNKP87V', 1, 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(13, 'We7WOWsJtNKP87V', 1, 'SOMAKUMAR and PREETHA', '4/404,Kottilingal Manniah House,Pazhambalacode,Palakkad,Tarur,Kerala-678544', '9894769561', 'rathina.sami@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(14, 'We7WOWsJtNKP87V', 1, 'MR. ZOHAIR M. KAGALWALA AND MRS. NISRIN ZOHAIR', '14,Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(15, 'We7WOWsJtNKP87V', 1, 'TN PRABHAKARAN', 'B3, Srinivasa Residency, 45, Sankaran Pillai Road,Opp to Aadhi Kottai Mariamman Kovil, Tiruchirappalli, Tamilnadu- 620002', '9952422004', 'trichyprabhaharan@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(16, 'We7WOWsJtNKP87V', 1, 'A.NACHYAR', 'Sri Ganapathy,Subhase Nagar,Poonithvra,Cochin-682038', '9495538182', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(17, 'We7WOWsJtNKP87V', 1, 'SATHISH KUMAR', '2/102, Evadugapalayam Elavanthi,Palladam,Tiruppur-641664', '9486071974', 'rsathishkumarr@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(18, 'We7WOWsJtNKP87V', 1, 'AMBETHKAR', 'Plot No-29, Bhagyalakshmi Nagar, Madampakkam, Selaiyur, Kanchipuram, Selaiyur-600073', '7498167947', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(19, 'We7WOWsJtNKP87V', 1, 'RAMESH & MRS.BHANU REKHA', '147,Rajivi Nagar,Manapparai,Trichy-621306', '9894769561', 'rathina.sami@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(20, 'We7WOWsJtNKP87V', 1, 'Mr. MUFADDAL T MARHABA AND Mrs. MUNIRA M MARHABA', '12/E,Burhani Colony,Avarampalayam Road,Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(21, 'We7WOWsJtNKP87V', 1, 'MUTHUSUBBIYAN', '38,Kandasamy street, Bazar post, T.C.Market, Tiruppur 641604', '9944235074', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(22, 'We7WOWsJtNKP87V', 1, 'MANICKAVASAGAM', '100, Sengaliyappa nagar , Peelamedu, Coimbatore â€“ 641004', '9363045457', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(23, 'We7WOWsJtNKP87V', 1, 'MR.RANJITH & G.MANJU', 'B2-4J -Gowtham ABC Avenue,Peelamedu', '9940975500', 'ranjith@futurerea.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(24, 'We7WOWsJtNKP87V', 1, 'SREE VIDYA BHUPESE', 'B4-4G, Gowtham ABC Avenue Apartment, Avarampalayam Road, Coimbatore- 641004', '9994924984', 'vidhujan87@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(25, 'We7WOWsJtNKP87V', 1, 'ANITHA', '1B,3rd Block-,Coral Castle Apartment,Peelamedu,Cbe-4', '9585523599', 'nithi_anii@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(26, 'We7WOWsJtNKP87V', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(27, 'We7WOWsJtNKP87V', 1, 'MR.K.AYYAVU', 'A-11 Brindavan Nagar, Kalapatti , Coimbatore Aerodrome,Coimbatore-641014', '9443910804', 'ayyavus@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(28, 'We7WOWsJtNKP87V', 1, 'SARASWATHI JEYARAJU', '6/236A,Muthu Gopal Nagar, Anuppurpalayam puthur,Periyar Colony,Tiruppur-641652', '9123589373', 'kamikki@hotmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(29, 'We7WOWsJtNKP87V', 1, 'THIRUPATHI', 'G3,Gowtham Enclave 164-A Sarojini Street,Ram Nagar,Cbe-9', '9842232324', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(30, 'We7WOWsJtNKP87V', 1, 'UMAMAHESHWARAN and PRIYADARSINI.U', '15-G Natesa Gounder Layout,Rathinapuri ,Coimbatore-27', '9894471544', 'mahesh71554@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(31, 'We7WOWsJtNKP87V', 1, 'G.ANANDAKUMAR', 'SFS C-14, Housing unit, HUDCO Colony, VK Road, Peelamedu, Coimbatore-641004', '9843113160', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(32, 'We7WOWsJtNKP87V', 1, 'ALEX ROBIN', '3B, 5th block, Sree Rose Appartments, near GV residency, Coimbatore- 641028', '9790696941', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(33, 'We7WOWsJtNKP87V', 1, 'SRI VIGNESH and S.R.JANANI', '14/1, Kumaran street, Surampatti, Erode- 638009', '9629231445', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(34, 'We7WOWsJtNKP87V', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(35, 'We7WOWsJtNKP87V', 1, 'KAVITHA RAJENDRAN and SUMITHA RAJESNDRAN. POA HOLDER RAJENDRAN', '269, 3rd cross, Ellai Thottam road, Coimbatore- 641004', '9894282292', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(36, 'We7WOWsJtNKP87V', 1, 'Mr. MURTUZA Z VADNAGARWALA AND Mrs. FATEMA M VADNAGARWALA', '14,1st Floor Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(37, 'We7WOWsJtNKP87V', 1, 'U.MITHUN and R.Thilagavathi', '1-1/58 Ponnagar, GanapathyPalayam, Salem-636030', '9884598226', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(38, 'We7WOWsJtNKP87V', 1, 'TS SIVAPRIYA', 'A6, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9600919888', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(39, 'We7WOWsJtNKP87V', 1, 'JAISUNDER', '32/6, Hudco Colony, Peelamedu, Coimbatore-641004', '9842257160', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(40, 'We7WOWsJtNKP87V', 1, 'SUBBURAJU', '104, Prime Enclave, Avinashi road, Gandhinagar, Tiruppur- 641603', '8754009505', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(41, 'We7WOWsJtNKP87V', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(42, 'We7WOWsJtNKP87V', 1, 'M.S.GOWTHAM', 'F0A,Block-2,Jains sri Saanvi Apartment,Ramasamy Nagar,Uppilipalayam(po) Coimbatore-641015', '9842535570', 'msgowtham6@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(43, 'We7WOWsJtNKP87V', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(44, 'We7WOWsJtNKP87V', 1, 'SINDHUJA', 'No.8/1254-G Pappan Thottam Pooluvapatti(POST) Tiruppur -641602', '8220765295', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(45, 'We7WOWsJtNKP87V', 1, 'SHARANYA', '4/20,Naicken Palayam SRKV-Via,cbe-641020', '9894337199', 'nilavan@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(46, 'We7WOWsJtNKP87V', 1, 'SUMATHA', 'A3, TWAD Board Quarters, Vilankurichi Road, Coimbatore 641004', '9842223698', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(47, 'We7WOWsJtNKP87V', 1, 'Kaussal Kannan Rathinamani and Priyanka Sudarshan', '18, Kumarasamy Nagar First Street, Civil Aerodrome Road, Coimbatore South, Coimbatore Aerodrome, Coimbatore, Tamilnadu- 641014', '9600644411', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(48, 'We7WOWsJtNKP87V', 1, 'ILANDURAIYAN', 'E-1, Block-1, Parijatha Apartment, Green Glen Layout, Bellandur, Bangalore 560103', '9916386964', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(49, 'We7WOWsJtNKP87V', 1, 'KRISHNALATHA S', '6/149, KULATHUPALAYAM, SULTHANPET, SULUR TALUK, VARAPATTI, COIMBATORE- 641609', '9843266236', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(50, 'We7WOWsJtNKP87V', 1, 'NIRMALA R', 'B 49, ANNA KUDIYIRUPPU, UDUMALAIPETTAI, TIRUPPUR- 642128', '9865853613', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(51, 'We7WOWsJtNKP87V', 1, 'SARANYA', 'A1c, Isha Armonia Apartments, Vishweswara Nagar, Vilankuruchi road, Coimbatore- 641035', '9620611644', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(52, 'We7WOWsJtNKP87V', 1, 'INDU', 'A4c, Isha Armonia apartment, Visweswara nagar, Vilankuruchi, Coimbatore- 641035', '9842789276', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(53, 'T57EtxcJYa7Q07r', 1, 'GOWARTHINI LC', '3/829 E, Sri Vigneshwara Nagar, 6th street , Palladam Road, Veerapandi post, Veerapandi, Tiruppur- 641605', '88708 56666', 'dr.vivekarumugham@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(54, 'T57EtxcJYa7Q07r', 1, 'R.ARUNKUMAR', '1C,Golden Castle ,Kavetti Naidu Layout, Sowripalayam,Coimbatore-641028', '9944113440', 'shivaaniarun76@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(55, 'T57EtxcJYa7Q07r', 1, 'SUGUMAR MUTHUSAMY and RESHMI R', 'No.56,Karunanithi Nagar, Sowripalayam,Cbe-641028', '81971 59292', 'sugu1030@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(56, 'T57EtxcJYa7Q07r', 1, 'SADHASIVAM', 'B3, Lakshimipuram, Peelamedu, Coimbatore 641004', '94423 67722', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(57, 'T57EtxcJYa7Q07r', 1, 'DR RADHIKA', 'D21, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '93631 01831', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(58, 'T57EtxcJYa7Q07r', 1, 'THIRUNAVAKARASU and T.ROOPESH', '2/54, Arasu paniyalar nagar, Vialnkuruchi post, Coimbatore- 641035', '94439 14955', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(59, 'T57EtxcJYa7Q07r', 1, 'RAJA KALYANA SUBRAMANIAN SAMBANTHAM', '701,Shanmuga Nagar, 7TH Cross,Uyakondan Thiramalai,Somasarasam Pettai, Trichy,-620102', '9842444466', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(60, 'T57EtxcJYa7Q07r', 1, 'A.NACHYAR', 'Sri Ganapathy,Subhase Nagar,Poonithvra,Cochin-682038', '9495538182', 'kanathan@hotmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(61, 'T57EtxcJYa7Q07r', 1, 'AMBETHKAR', 'Plot No-29, Bhagyalakshmi Nagar, Madampakkam, Selaiyur, Kanchipuram, Selaiyur-600073', '74981 67947', 'ambethkar_abus@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(62, 'T57EtxcJYa7Q07r', 1, 'RAMESH & MRS.BHANU REKHA', '147,Rajivi Nagar,Manapparai,Trichy-621306', '9894769561', 'rathina.sami@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(63, 'T57EtxcJYa7Q07r', 1, 'Mr. MUFADDAL T MARHABA AND Mrs. MUNIRA M MARHABA', '12/E,Burhani Colony,Avarampalayam Road,Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(64, 'T57EtxcJYa7Q07r', 1, 'MUTHUSUBBIYAN', '38,Kandasamy street, Bazar post, T.C.Market, Tiruppur 641604', '99442 35074', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(65, 'T57EtxcJYa7Q07r', 1, 'MANICKAVASAGAM', '100, Sengaliyappa nagar , Peelamedu, Coimbatore â€“ 641004', '93630 45457', 'kgmanickm2000@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(66, 'T57EtxcJYa7Q07r', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(67, 'T57EtxcJYa7Q07r', 1, 'THIRUPATHI', 'G3,Gowtham Enclave 164-A Sarojini Street,Ram Nagar,Cbe-9', '9842232324', 'srts121213@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(68, 'T57EtxcJYa7Q07r', 1, 'G.ANANDAKUMAR', 'SFS C-14, Housing unit, HUDCO Colony, VK Road, Peelamedu, Coimbatore-641004', '9843113160', 'anandkalpana10@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(69, 'T57EtxcJYa7Q07r', 1, 'ALEX ROBIN', '3B, 5th block, Sree Rose Appartments, near GV residency, Coimbatore- 641028', '97906 96941', 'aruldossbsnl@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(70, 'T57EtxcJYa7Q07r', 1, 'SRI VIGNESH and S.R.JANANI', '14/1, Kumaran street, Surampatti, Erode- 638009', '96292 31445', 'Sriviky@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(71, 'T57EtxcJYa7Q07r', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(72, 'T57EtxcJYa7Q07r', 1, 'Kavitha Rajendran and Sumithaa Rajendran . POA Holder Rajendran', '269, 3rd cross, Ellai Thottam road, Coimbatore- 641004', '98942 82292', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(73, 'T57EtxcJYa7Q07r', 1, 'Mr. MURTUZA Z VADNAGARWALA AND Mrs. FATEMA M VADNAGARWALA', '14,1st Floor Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(74, 'T57EtxcJYa7Q07r', 1, 'U.MITHUN and R.Thilagavathi', '1-1/58 Ponnagar, GanapathyPalayam, Salem-636030', '98845-98226', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(75, 'T57EtxcJYa7Q07r', 1, 'TS SIVAPRIYA', 'A6, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '96009 19888', 'cpramaesh@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(76, 'T57EtxcJYa7Q07r', 1, 'JAISUNDER', '32/6, Hudco Colony, Peelamedu, Coimbatore-641004', '9842257160', 'Sundarpeelamedu@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(77, 'T57EtxcJYa7Q07r', 1, 'SUBBURAJU', '104, Prime Enclave, Avinashi road, Gandhinagar, Tiruppur- 641603', '87540 09505', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(78, 'T57EtxcJYa7Q07r', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(79, 'T57EtxcJYa7Q07r', 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(80, 'T57EtxcJYa7Q07r', 1, 'SINDHUJA', 'No.8/1254-G Pappan Thottam Pooluvapatti(POST) Tiruppur -641602', '82207 65295', 'vigneshp.ceg@gmail.com , sindhutpr@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(81, 'T57EtxcJYa7Q07r', 1, 'SUMATHA', 'A3, TWAD Board Quarters, Vilankurichi Road, Coimbatore 641004', '98422 23698', 'jaikumarsuma@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(82, 'T57EtxcJYa7Q07r', 1, 'Kaussal Kannan Rathinamani and Priyanka Sudarshan', '18, Kumarasamy Nagar First Street, Civil Aerodrome Road, Coimbatore South, Coimbatore Aerodrome, Coimbatore, Tamilnadu- 641014', '96006 44411 & 96000 03575', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(83, 'T57EtxcJYa7Q07r', 1, 'ILANDURAIYAN', 'E-1, Block-1, Parijatha Apartment, Green Glen Layout, Bellandur, Bangalore 560103', '99163 86964', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(84, 'T57EtxcJYa7Q07r', 1, 'KRISHNALATHA S', '6/149, KULATHUPALAYAM, SULTHANPET, SULUR TALUK, VARAPATTI, COIMBATORE- 641609', '98432 66236', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(85, 'T57EtxcJYa7Q07r', 1, 'NIRMALA R', 'B 49, ANNA KUDIYIRUPPU, UDUMALAIPETTAI, TIRUPPUR- 642128', '98658 53613', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(86, 'T57EtxcJYa7Q07r', 1, 'SARANYA', 'A1c, Isha Armonia Apartments, Vishweswara Nagar, Vilankuruchi road, Coimbatore- 641035', '9620611644', 'saranyaharidass@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(87, 'T57EtxcJYa7Q07r', 1, 'INDU', 'A4c, Isha Armonia apartment, Visweswara nagar, Vilankuruchi, Coimbatore- 641035', '9842789276', 'indu.haridass@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(88, 'T57EtxcJYa7Q07r', 1, 'PRAVEEN', '', '9976215566', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(89, 'T57EtxcJYa7Q07r', 1, 'DR SUBBAKANI', '', '8870944822', 'subbukani@yahoo.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(90, 'L2EdeftmKRRcRmx', 1, 'Harish Kumar', '8310385535', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(91, 'L2EdeftmKRRcRmx', 1, 'Gaurav Kapoor', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(92, 'L2EdeftmKRRcRmx', 1, 'Venu Bade', '9708229999', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(93, 'L2EdeftmKRRcRmx', 1, 'Ar Akash Dixit', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(94, 'L2EdeftmKRRcRmx', 1, 'Bablu Rajbar Rajbar', '7288910515', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(95, 'L2EdeftmKRRcRmx', 1, 'Krish chowdary', '', 'info@landswealth', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(96, 'L2EdeftmKRRcRmx', 1, 'Jawahar Korku K', '', '', 'Hbjd Korku Travels & Real Estate', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(97, 'L2EdeftmKRRcRmx', 1, 'Kuldeep Kumar', '6387864346', '', 'Rolex Infra', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(98, 'L2EdeftmKRRcRmx', 1, 'Rajesh Bhave', '8652477797', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(99, 'L2EdeftmKRRcRmx', 1, 'Rajat Singh Manchanda', '9999777664', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(100, 'L2EdeftmKRRcRmx', 1, 'Vikas Gulati', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(101, 'L2EdeftmKRRcRmx', 1, 'Venkat Venki', '9502219788', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(102, 'L2EdeftmKRRcRmx', 1, 'Khusrau Ahmed', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(103, 'L2EdeftmKRRcRmx', 1, 'Sudheer Bose', '7034618741', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(104, 'L2EdeftmKRRcRmx', 1, 'Zaid Malik', '9930360661', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(105, 'L2EdeftmKRRcRmx', 1, 'T A Davidson', '7300399762', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(106, 'L2EdeftmKRRcRmx', 1, 'Arun Gupta', '8428709067', '', 'Sslf City Housing', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(107, 'L2EdeftmKRRcRmx', 1, 'Divyansinh Rajput', '7600800955', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(108, 'L2EdeftmKRRcRmx', 1, '', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0),
(109, 'L2EdeftmKRRcRmx', 1, '', '', '', '', '2021-09-02 15:15:21', '2021-09-02 15:15:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp_excel_tbl`
--

CREATE TABLE IF NOT EXISTS `temp_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_token` varchar(50) NOT NULL,
  `uid` varchar(80) DEFAULT NULL,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `leadsource` varchar(200) NOT NULL,
  `flat_type` varchar(250) NOT NULL,
  `site_visited` varchar(250) NOT NULL,
  `profile` varchar(250) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `primary_mobile` varchar(200) NOT NULL,
  `primary_email` varchar(200) NOT NULL,
  `secondary_mobile` varchar(250) NOT NULL,
  `secondary_email` varchar(250) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `pincode` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=154 ;

--
-- Dumping data for table `temp_excel_tbl`
--

INSERT INTO `temp_excel_tbl` (`id`, `upload_token`, `uid`, `project_id`, `fname`, `lname`, `leadsource`, `flat_type`, `site_visited`, `profile`, `gender`, `primary_mobile`, `primary_email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(3, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Swasthik', '', 'Facebook', '2 BHK', 'Yes', 'Business', 'Female', '7373021332', '', '', '', '', 'Cbe', 'TamilNadu', 'Did not ans call', '641027', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(4, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Duraiswamy', '', 'Google', '3BHK', 'No', 'Business', 'Male', '8825642733', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call', '641028', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(5, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Vetrivel', '', 'facebook', '2 BHK', 'Yes', 'Business', 'Male', '9841051590', '', '', '', '', 'Chennai', 'TamilNadu', 'has in mind, will call back', '641029', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(6, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Ramoorthy', '', 'facebook', '2 BHK', 'No', 'Business', 'Male', '9737356233', '', '', '', '', 'Cbe', 'TamilNadu', 'thinking over multiple choices in chennai and cbe', '641030', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(7, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Akshay', '', 'google', '3BHK', 'Yes', 'Business', 'Male', '8610739811', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call', '641031', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(8, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Ramkumar', '', 'google', '2 BHK', 'No', 'Business', 'Male', '9894013341', '', '', '', '', 'Chennai', 'TamilNadu', 'Room sizes are small, thinking off larger option', '641032', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(9, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Balasundaram', '', 'facebook', '2 BHK', 'Yes', 'Business', 'Male', '9677622669', '', '', '', '', 'Cbe', 'TamilNadu', 'Wants L Type, Ready to buy with full payment', '641033', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(10, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Prabha', '', 'facebook', '2 BHK', 'No', 'Doctor', 'Male', '6382735660', '', '', '', '', 'Madurai', 'TamilNadu', 'share details again, not replying', '641034', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(11, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Raji', '', 'facebook', '2 BHK', 'Yes', 'Doctor', 'female', '9113282820', '', '', '', '', 'Chennai', 'TamilNadu', 'Did not ans call', '641035', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(12, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Antojeba', '', 'google', '2 BHK', 'No', 'Doctor', 'Male', '8098600910', '', '', '', '', 'Cbe', 'TamilNadu', 'His brother enquired it', '641036', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(13, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Murali', '', 'google', '2 BHK', 'Yes', 'Doctor', 'Male', '9486922011', '', '', '', '', 'Madurai', 'TamilNadu', 'Will visit again with family', '641037', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(14, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Naveen', '', 'google', '2 BHK', 'No', 'Doctor', 'Male', '9894290431', '', '', '', '', 'Chennai', 'TamilNadu', 'Did not ans call, no response', '641038', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(15, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Gugan', '', 'google', '2 BHK', 'Yes', 'Doctor', 'Male', '9980148996', '', '', '', '', 'Cbe', 'TamilNadu', 'not planning to buy now', '641039', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(16, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Keerthana', '', 'facebook', '2 BHK', 'No', 'Doctor', 'female', '9962009596', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call, no response', '641040', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(17, 'SyDxz7NhpBL0Y7L', NULL, 1, 'Madhan', '', 'facebook', '2 BHK', 'yes', 'Doctor', 'Male', '8122658371', '', '', '', '', 'Chennai', 'TamilNadu', 'out of their budget at present', '641041', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(18, 'RQPhNbKBq3MJwRs', NULL, 1, 'Arun', 'Kumar', 'fb', '1 bhk', 'yes', 'profile 1', 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', 'sample 1', '61104', '2020-10-05 16:04:01', '2020-10-05 16:04:01', 0),
(19, 'RQPhNbKBq3MJwRs', NULL, 1, 'Venget', 'Rajan', 'internet', '7bhk', 'no', 'profile 3', 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', 'sample 2', '614408', '2020-10-05 16:04:01', '2020-10-05 16:04:01', 0),
(20, '72lovaei9GJZrm0', NULL, 1, 'Arun', 'Kumar', 'fb', '1 bhk', 'yes', 'profile 1', 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', 'sample 1', '61104', '2020-10-05 16:04:39', '2020-10-05 16:04:39', 0),
(21, '72lovaei9GJZrm0', NULL, 1, 'Venget', 'Rajan', 'internet', '7bhk', 'no', 'profile 3', 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', 'sample 2', '614408', '2020-10-05 16:04:39', '2020-10-05 16:04:39', 0),
(22, 'itTPxA8CnXiYBog', NULL, 1, 'Harish Kumar', '8310385535', '', '', '', 'Warm', 'Call after 6PM', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(23, 'itTPxA8CnXiYBog', NULL, 1, 'Gaurav Kapoor', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(24, 'itTPxA8CnXiYBog', NULL, 1, 'Venu Bade', '9708229999', '', '', '', '', 'Telugu. He couldnâ€™', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(25, 'itTPxA8CnXiYBog', NULL, 1, 'Ar Akash Dixit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(26, 'itTPxA8CnXiYBog', NULL, 1, 'Bablu Rajbar Rajbar', '7288910515', '', '', '', '', 'Only hindi.. He didn', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(27, 'itTPxA8CnXiYBog', NULL, 1, 'Krish chowdary', '', 'info@landswealth', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(28, 'itTPxA8CnXiYBog', NULL, 1, 'Jawahar Korku K', '', '', 'Hbjd Korku Travels & Real Estate', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(29, 'itTPxA8CnXiYBog', NULL, 1, 'Kuldeep Kumar', '6387864346', '', 'Rolex Infra', 'Lucknow', '', 'Hindi.. He asked to ', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(30, 'itTPxA8CnXiYBog', NULL, 1, 'Rajesh Bhave', '8652477797', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(31, 'itTPxA8CnXiYBog', NULL, 1, 'Rajat Singh Manchanda', '9999777664', '', '', '', '', 'He will call me.. Bu', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(32, 'itTPxA8CnXiYBog', NULL, 1, 'Vikas Gulati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(33, 'itTPxA8CnXiYBog', NULL, 1, 'Venkat Venki', '9502219788', '', '', 'Tirupati', '', 'Invalid number', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(34, 'itTPxA8CnXiYBog', NULL, 1, 'Khusrau Ahmed', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(35, 'itTPxA8CnXiYBog', NULL, 1, 'Sudheer Bose', '7034618741', '', '', '', '', 'He already purchased', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(36, 'itTPxA8CnXiYBog', NULL, 1, 'Zaid Malik', '9930360661', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(37, 'itTPxA8CnXiYBog', NULL, 1, 'T A Davidson', '7300399762', '', '', '', '', 'Didnâ€™t pick', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(38, 'itTPxA8CnXiYBog', NULL, 1, 'Arun Gupta', '8428709067', '', 'Sslf City Housing', 'Chennai', 'Warm', '9894665132. Wants De', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(39, 'itTPxA8CnXiYBog', NULL, 1, 'Divyansinh Rajput', '7600800955', '', '', '', '', 'Didnt Pick', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(40, 'itTPxA8CnXiYBog', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(41, 'itTPxA8CnXiYBog', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 10:57:34', '2021-09-02 10:57:34', 0),
(42, 'CVUL6BEXfWwRHAU', NULL, 1, 'Harish Kumar', '8310385535', '', '', '', 'Warm', 'Call after 6PM', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(43, 'CVUL6BEXfWwRHAU', NULL, 1, 'Gaurav Kapoor', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(44, 'CVUL6BEXfWwRHAU', NULL, 1, 'Venu Bade', '9708229999', '', '', '', '', 'Telugu. He couldnâ€™', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(45, 'CVUL6BEXfWwRHAU', NULL, 1, 'Ar Akash Dixit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(46, 'CVUL6BEXfWwRHAU', NULL, 1, 'Bablu Rajbar Rajbar', '7288910515', '', '', '', '', 'Only hindi.. He didn', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(47, 'CVUL6BEXfWwRHAU', NULL, 1, 'Krish chowdary', '', 'info@landswealth', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(48, 'CVUL6BEXfWwRHAU', NULL, 1, 'Jawahar Korku K', '', '', 'Hbjd Korku Travels & Real Estate', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(49, 'CVUL6BEXfWwRHAU', NULL, 1, 'Kuldeep Kumar', '6387864346', '', 'Rolex Infra', 'Lucknow', '', 'Hindi.. He asked to ', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(50, 'CVUL6BEXfWwRHAU', NULL, 1, 'Rajesh Bhave', '8652477797', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(51, 'CVUL6BEXfWwRHAU', NULL, 1, 'Rajat Singh Manchanda', '9999777664', '', '', '', '', 'He will call me.. Bu', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(52, 'CVUL6BEXfWwRHAU', NULL, 1, 'Vikas Gulati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(53, 'CVUL6BEXfWwRHAU', NULL, 1, 'Venkat Venki', '9502219788', '', '', 'Tirupati', '', 'Invalid number', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(54, 'CVUL6BEXfWwRHAU', NULL, 1, 'Khusrau Ahmed', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(55, 'CVUL6BEXfWwRHAU', NULL, 1, 'Sudheer Bose', '7034618741', '', '', '', '', 'He already purchased', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(56, 'CVUL6BEXfWwRHAU', NULL, 1, 'Zaid Malik', '9930360661', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(57, 'CVUL6BEXfWwRHAU', NULL, 1, 'T A Davidson', '7300399762', '', '', '', '', 'Didnâ€™t pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(58, 'CVUL6BEXfWwRHAU', NULL, 1, 'Arun Gupta', '8428709067', '', 'Sslf City Housing', 'Chennai', 'Warm', '9894665132. Wants De', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(59, 'CVUL6BEXfWwRHAU', NULL, 1, 'Divyansinh Rajput', '7600800955', '', '', '', '', 'Didnt Pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(60, 'CVUL6BEXfWwRHAU', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(61, 'CVUL6BEXfWwRHAU', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:06:15', '2021-09-02 11:06:15', 0),
(62, 'kVuhPArG4WeeRm7', NULL, 1, 'Harish Kumar', '8310385535', '', '', '', 'Warm', 'Call after 6PM', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(63, 'kVuhPArG4WeeRm7', NULL, 1, 'Gaurav Kapoor', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(64, 'kVuhPArG4WeeRm7', NULL, 1, 'Venu Bade', '9708229999', '', '', '', '', 'Telugu. He couldnâ€™', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(65, 'kVuhPArG4WeeRm7', NULL, 1, 'Ar Akash Dixit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(66, 'kVuhPArG4WeeRm7', NULL, 1, 'Bablu Rajbar Rajbar', '7288910515', '', '', '', '', 'Only hindi.. He didn', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(67, 'kVuhPArG4WeeRm7', NULL, 1, 'Krish chowdary', '', 'info@landswealth', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(68, 'kVuhPArG4WeeRm7', NULL, 1, 'Jawahar Korku K', '', '', 'Hbjd Korku Travels & Real Estate', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(69, 'kVuhPArG4WeeRm7', NULL, 1, 'Kuldeep Kumar', '6387864346', '', 'Rolex Infra', 'Lucknow', '', 'Hindi.. He asked to ', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(70, 'kVuhPArG4WeeRm7', NULL, 1, 'Rajesh Bhave', '8652477797', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(71, 'kVuhPArG4WeeRm7', NULL, 1, 'Rajat Singh Manchanda', '9999777664', '', '', '', '', 'He will call me.. Bu', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(72, 'kVuhPArG4WeeRm7', NULL, 1, 'Vikas Gulati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(73, 'kVuhPArG4WeeRm7', NULL, 1, 'Venkat Venki', '9502219788', '', '', 'Tirupati', '', 'Invalid number', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(74, 'kVuhPArG4WeeRm7', NULL, 1, 'Khusrau Ahmed', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(75, 'kVuhPArG4WeeRm7', NULL, 1, 'Sudheer Bose', '7034618741', '', '', '', '', 'He already purchased', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(76, 'kVuhPArG4WeeRm7', NULL, 1, 'Zaid Malik', '9930360661', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(77, 'kVuhPArG4WeeRm7', NULL, 1, 'T A Davidson', '7300399762', '', '', '', '', 'Didnâ€™t pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(78, 'kVuhPArG4WeeRm7', NULL, 1, 'Arun Gupta', '8428709067', '', 'Sslf City Housing', 'Chennai', 'Warm', '9894665132. Wants De', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(79, 'kVuhPArG4WeeRm7', NULL, 1, 'Divyansinh Rajput', '7600800955', '', '', '', '', 'Didnt Pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(80, 'kVuhPArG4WeeRm7', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(81, 'kVuhPArG4WeeRm7', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:07:31', '2021-09-02 11:07:31', 0),
(82, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Harish Kumar', '8310385535', '', '', '', 'Warm', 'Call after 6PM', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(83, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Gaurav Kapoor', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(84, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Venu Bade', '9708229999', '', '', '', '', 'Telugu. He couldnâ€™', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(85, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Ar Akash Dixit', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(86, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Bablu Rajbar Rajbar', '7288910515', '', '', '', '', 'Only hindi.. He didn', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(87, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Krish chowdary', '', 'info@landswealth', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(88, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Jawahar Korku K', '', '', 'Hbjd Korku Travels & Real Estate', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(89, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Kuldeep Kumar', '6387864346', '', 'Rolex Infra', 'Lucknow', '', 'Hindi.. He asked to ', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(90, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Rajesh Bhave', '8652477797', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(91, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Rajat Singh Manchanda', '9999777664', '', '', '', '', 'He will call me.. Bu', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(92, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Vikas Gulati', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(93, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Venkat Venki', '9502219788', '', '', 'Tirupati', '', 'Invalid number', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(94, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Khusrau Ahmed', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(95, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Sudheer Bose', '7034618741', '', '', '', '', 'He already purchased', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(96, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Zaid Malik', '9930360661', '', '', '', '', 'Busy', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(97, 'BuxZcz8QQzQ7C5r', NULL, 1, 'T A Davidson', '7300399762', '', '', '', '', 'Didnâ€™t pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(98, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Arun Gupta', '8428709067', '', 'Sslf City Housing', 'Chennai', 'Warm', '9894665132. Wants De', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(99, 'BuxZcz8QQzQ7C5r', NULL, 1, 'Divyansinh Rajput', '7600800955', '', '', '', '', 'Didnt Pick', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(100, 'BuxZcz8QQzQ7C5r', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(101, 'BuxZcz8QQzQ7C5r', NULL, 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-02 11:09:26', '2021-09-02 11:09:26', 0),
(102, '0QLNMqdHXoJlKr3', NULL, 1, 'GOWARTHINI LC', '3/829 E, Sri Vigneshwara Nagar, 6th street , Palladam Road, Veerapandi post, Veerapandi, Tiruppur- 641605', '8870856666', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(103, '0QLNMqdHXoJlKr3', NULL, 1, 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(104, '0QLNMqdHXoJlKr3', NULL, 1, 'R.ARUNKUMAR', '1C,Golden Castle ,Kavetti Naidu Layout, Sowripalayam,Coimbatore-641028', '9944113440', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(105, '0QLNMqdHXoJlKr3', NULL, 1, 'SUGUMAR MUTHUSAMY and RESHMI R', 'No.56,Karunanithi Nagar, Sowripalayam,Cbe-641028', '8197159292', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(106, '0QLNMqdHXoJlKr3', NULL, 1, 'MRS.POORANI.K.T', '85,kalyani nagar,udumalaipettai,gandhi nagar,tirupur-642154', '9585523822', 'dhileepan62@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(107, '0QLNMqdHXoJlKr3', NULL, 1, 'SADHASIVAM', 'B3, Lakshimipuram, Peelamedu, Coimbatore 641004', '9442367722', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(108, '0QLNMqdHXoJlKr3', NULL, 1, 'DR RADHIKA', 'D21, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9363101831', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(109, '0QLNMqdHXoJlKr3', NULL, 1, 'R.HARIRAJAN', '22/A, D.D.A MIG-Flats,Pocket-2,Sector-7,Dwarka(west) West Delhi-110075', '9818071456', 'rharirajan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(110, '0QLNMqdHXoJlKr3', NULL, 1, 'P.S VISHNOO VARDHAN', 'PLOT NO.13,4Th Cross Street, Siva Sakthi Nagar, Madhavaram-Chennai-600060', '8754589225', 'maddyvishnoo@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(111, '0QLNMqdHXoJlKr3', NULL, 1, 'THIRUNAVAKARASU and T.ROOPESH', '2/54, Arasu paniyalar nagar, Vialnkuruchi post, Coimbatore- 641035', '9443914955', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(112, '0QLNMqdHXoJlKr3', NULL, 1, 'RAJA KALYANA SUBRAMANIAN SAMBANTHAM', '701,Shanmuga Nagar, 7TH Cross,Uyakondan Thiramalai,Somasarasam Pettai, Trichy,-620102', '9842444466', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(113, '0QLNMqdHXoJlKr3', NULL, 1, 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(114, '0QLNMqdHXoJlKr3', NULL, 1, 'SOMAKUMAR and PREETHA', '4/404,Kottilingal Manniah House,Pazhambalacode,Palakkad,Tarur,Kerala-678544', '9894769561', 'rathina.sami@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(115, '0QLNMqdHXoJlKr3', NULL, 1, 'MR. ZOHAIR M. KAGALWALA AND MRS. NISRIN ZOHAIR', '14,Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(116, '0QLNMqdHXoJlKr3', NULL, 1, 'TN PRABHAKARAN', 'B3, Srinivasa Residency, 45, Sankaran Pillai Road,Opp to Aadhi Kottai Mariamman Kovil, Tiruchirappalli, Tamilnadu- 620002', '9952422004', 'trichyprabhaharan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(117, '0QLNMqdHXoJlKr3', NULL, 1, 'A.NACHYAR', 'Sri Ganapathy,Subhase Nagar,Poonithvra,Cochin-682038', '9495538182', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(118, '0QLNMqdHXoJlKr3', NULL, 1, 'SATHISH KUMAR', '2/102, Evadugapalayam Elavanthi,Palladam,Tiruppur-641664', '9486071974', 'rsathishkumarr@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(119, '0QLNMqdHXoJlKr3', NULL, 1, 'AMBETHKAR', 'Plot No-29, Bhagyalakshmi Nagar, Madampakkam, Selaiyur, Kanchipuram, Selaiyur-600073', '7498167947', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(120, '0QLNMqdHXoJlKr3', NULL, 1, 'RAMESH & MRS.BHANU REKHA', '147,Rajivi Nagar,Manapparai,Trichy-621306', '9894769561', 'rathina.sami@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(121, '0QLNMqdHXoJlKr3', NULL, 1, 'Mr. MUFADDAL T MARHABA AND Mrs. MUNIRA M MARHABA', '12/E,Burhani Colony,Avarampalayam Road,Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(122, '0QLNMqdHXoJlKr3', NULL, 1, 'MUTHUSUBBIYAN', '38,Kandasamy street, Bazar post, T.C.Market, Tiruppur 641604', '9944235074', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(123, '0QLNMqdHXoJlKr3', NULL, 1, 'MANICKAVASAGAM', '100, Sengaliyappa nagar , Peelamedu, Coimbatore â€“ 641004', '9363045457', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(124, '0QLNMqdHXoJlKr3', NULL, 1, 'MR.RANJITH & G.MANJU', 'B2-4J -Gowtham ABC Avenue,Peelamedu', '9940975500', 'ranjith@futurerea.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(125, '0QLNMqdHXoJlKr3', NULL, 1, 'SREE VIDYA BHUPESE', 'B4-4G, Gowtham ABC Avenue Apartment, Avarampalayam Road, Coimbatore- 641004', '9994924984', 'vidhujan87@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(126, '0QLNMqdHXoJlKr3', NULL, 1, 'ANITHA', '1B,3rd Block-,Coral Castle Apartment,Peelamedu,Cbe-4', '9585523599', 'nithi_anii@yahoo.co.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(127, '0QLNMqdHXoJlKr3', NULL, 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(128, '0QLNMqdHXoJlKr3', NULL, 1, 'MR.K.AYYAVU', 'A-11 Brindavan Nagar, Kalapatti , Coimbatore Aerodrome,Coimbatore-641014', '9443910804', 'ayyavus@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(129, '0QLNMqdHXoJlKr3', NULL, 1, 'SARASWATHI JEYARAJU', '6/236A,Muthu Gopal Nagar, Anuppurpalayam puthur,Periyar Colony,Tiruppur-641652', '9123589373', 'kamikki@hotmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(130, '0QLNMqdHXoJlKr3', NULL, 1, 'THIRUPATHI', 'G3,Gowtham Enclave 164-A Sarojini Street,Ram Nagar,Cbe-9', '9842232324', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(131, '0QLNMqdHXoJlKr3', NULL, 1, 'UMAMAHESHWARAN and PRIYADARSINI.U', '15-G Natesa Gounder Layout,Rathinapuri ,Coimbatore-27', '9894471544', 'mahesh71554@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(132, '0QLNMqdHXoJlKr3', NULL, 1, 'G.ANANDAKUMAR', 'SFS C-14, Housing unit, HUDCO Colony, VK Road, Peelamedu, Coimbatore-641004', '9843113160', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(133, '0QLNMqdHXoJlKr3', NULL, 1, 'ALEX ROBIN', '3B, 5th block, Sree Rose Appartments, near GV residency, Coimbatore- 641028', '9790696941', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(134, '0QLNMqdHXoJlKr3', NULL, 1, 'SRI VIGNESH and S.R.JANANI', '14/1, Kumaran street, Surampatti, Erode- 638009', '9629231445', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(135, '0QLNMqdHXoJlKr3', NULL, 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(136, '0QLNMqdHXoJlKr3', NULL, 1, 'KAVITHA RAJENDRAN and SUMITHA RAJESNDRAN. POA HOLDER RAJENDRAN', '269, 3rd cross, Ellai Thottam road, Coimbatore- 641004', '9894282292', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(137, '0QLNMqdHXoJlKr3', NULL, 1, 'Mr. MURTUZA Z VADNAGARWALA AND Mrs. FATEMA M VADNAGARWALA', '14,1st Floor Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(138, '0QLNMqdHXoJlKr3', NULL, 1, 'U.MITHUN and R.Thilagavathi', '1-1/58 Ponnagar, GanapathyPalayam, Salem-636030', '9884598226', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(139, '0QLNMqdHXoJlKr3', NULL, 1, 'TS SIVAPRIYA', 'A6, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9600919888', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(140, '0QLNMqdHXoJlKr3', NULL, 1, 'JAISUNDER', '32/6, Hudco Colony, Peelamedu, Coimbatore-641004', '9842257160', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(141, '0QLNMqdHXoJlKr3', NULL, 1, 'SUBBURAJU', '104, Prime Enclave, Avinashi road, Gandhinagar, Tiruppur- 641603', '8754009505', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(142, '0QLNMqdHXoJlKr3', NULL, 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(143, '0QLNMqdHXoJlKr3', NULL, 1, 'M.S.GOWTHAM', 'F0A,Block-2,Jains sri Saanvi Apartment,Ramasamy Nagar,Uppilipalayam(po) Coimbatore-641015', '9842535570', 'msgowtham6@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(144, '0QLNMqdHXoJlKr3', NULL, 1, 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(145, '0QLNMqdHXoJlKr3', NULL, 1, 'SINDHUJA', 'No.8/1254-G Pappan Thottam Pooluvapatti(POST) Tiruppur -641602', '8220765295', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(146, '0QLNMqdHXoJlKr3', NULL, 1, 'SHARANYA', '4/20,Naicken Palayam SRKV-Via,cbe-641020', '9894337199', 'nilavan@yahoo.co.in', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(147, '0QLNMqdHXoJlKr3', NULL, 1, 'SUMATHA', 'A3, TWAD Board Quarters, Vilankurichi Road, Coimbatore 641004', '9842223698', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(148, '0QLNMqdHXoJlKr3', NULL, 1, 'Kaussal Kannan Rathinamani and Priyanka Sudarshan', '18, Kumarasamy Nagar First Street, Civil Aerodrome Road, Coimbatore South, Coimbatore Aerodrome, Coimbatore, Tamilnadu- 641014', '9600644411', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(149, '0QLNMqdHXoJlKr3', NULL, 1, 'ILANDURAIYAN', 'E-1, Block-1, Parijatha Apartment, Green Glen Layout, Bellandur, Bangalore 560103', '9916386964', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(150, '0QLNMqdHXoJlKr3', NULL, 1, 'KRISHNALATHA S', '6/149, KULATHUPALAYAM, SULTHANPET, SULUR TALUK, VARAPATTI, COIMBATORE- 641609', '9843266236', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(151, '0QLNMqdHXoJlKr3', NULL, 1, 'NIRMALA R', 'B 49, ANNA KUDIYIRUPPU, UDUMALAIPETTAI, TIRUPPUR- 642128', '9865853613', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(152, '0QLNMqdHXoJlKr3', NULL, 1, 'SARANYA', 'A1c, Isha Armonia Apartments, Vishweswara Nagar, Vilankuruchi road, Coimbatore- 641035', '9620611644', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0),
(153, '0QLNMqdHXoJlKr3', NULL, 1, 'INDU', 'A4c, Isha Armonia apartment, Visweswara nagar, Vilankuruchi, Coimbatore- 641035', '9842789276', '', '', '', '', '', '', '', '', '', '', '', '', '', '2021-09-03 09:06:48', '2021-09-03 09:06:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tenant_tbl`
--

CREATE TABLE IF NOT EXISTS `tenant_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT '0',
  `reg_date` varchar(150) DEFAULT NULL,
  `tenant_token` varchar(200) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `company` varchar(150) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address` text,
  `pincode` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tenant_tbl`
--

INSERT INTO `tenant_tbl` (`id`, `plan_id`, `reg_date`, `tenant_token`, `first_name`, `last_name`, `company`, `mobile`, `email`, `address`, `pincode`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-08-18', 'venpepsolution', 'Venpep', 'Solution', 'Venpep Solution', '+91 422 4379440', 'hello@venpep.com', '95A, Vyshnav Building,\r\nRace Course,\r\nCoimbatore', '641018', 1, '2020-08-12 22:00:16', '2020-08-12 22:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `upload_video_tbl`
--

CREATE TABLE IF NOT EXISTS `upload_video_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(200) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `page_video` varchar(250) NOT NULL,
  `video_type` varchar(150) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `upload_video_tbl`
--

INSERT INTO `upload_video_tbl` (`id`, `project_id`, `type`, `title`, `page_video`, `video_type`, `added_by`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Add a new Lead', 'Add a new Lead', 'attachment_10082021015707_Q4tJsS8fYI.mp4', 'mp4', 1, 1, '0000-00-00 00:00:00', '2021-08-10 13:57:07'),
(2, 1, 'Manage existing Leads', 'Manage existing Leads', 'attachment_10082021015955_nRUBrdQL8E.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-10 13:59:55'),
(3, 1, 'Import Leads from excel sheet', 'Import Leads from excel sheet', 'attachment_11082021010935_OgJ5cZdBw4.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:09:35'),
(4, 1, 'Add a new customer', 'Add a new customer', 'attachment_11082021103542_uKGvkLbFZg.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 10:35:42'),
(5, 1, 'Manage existing customer', 'Manage existing customer', 'attachment_11082021110846_ONZjr853uf.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 11:08:46'),
(6, 1, 'KYC Documents Management', 'KYC Documents Management', 'attachment_11082021111119_dL1l7NurvJ.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 11:11:19'),
(7, 1, 'Add new Invoice / Receipt', 'Add new Invoice / Receipt', 'attachment_11082021114507_9UwXt8jP7t.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 11:45:07'),
(8, 1, 'Manage existing Invoices / Receipts', 'Manage existing Invoices / Receipts', 'attachment_11082021114649_QjifHtLb3Q.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 11:46:49'),
(9, 1, 'Import Customer data from excel sheet', 'Import Customer data from excel sheet', 'attachment_11082021114725_gICgvDdx8b.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 11:47:25'),
(10, 1, 'Manage Property', 'Manage Property', 'attachment_11082021011701_AytP3oiOXc.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:17:01'),
(11, 1, 'Property Chart', ' Property Chart', 'attachment_11082021012852_ztIldkTHpr.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:28:52'),
(12, 1, 'Add a new document to unit', 'Add a new document to unit', 'attachment_11082021013043_sTbScvSb9D.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:30:43'),
(13, 1, 'Manage Gallery ', 'Manage Gallery ', 'attachment_11082021013010_vaoOe8jMd6.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:30:10'),
(14, 1, 'Add a new Adhoc request', 'Add a new Adhoc request', 'attachment_11082021013913_HyrCMgT0Zj.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:39:13'),
(15, 1, 'Manage Open Requests', 'Manage Open Requests', 'attachment_11082021014225_7wihbKww9a.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:42:25'),
(16, 1, 'Manage Closed Requests', 'Manage Closed Requests', 'attachment_11082021014415_j09GQdlGuC.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 13:44:15'),
(17, 1, 'Add News ', 'Add News ', 'attachment_11082021041552_qyhdXswVxo.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:15:52'),
(18, 1, 'Manage Published News ', 'Manage Published News ', 'attachment_11082021041659_APigxV4lVn.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:16:59'),
(19, 1, 'Manage Employee', 'Manage Employee', 'attachment_11082021045614_grx2Wl4Oyf.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:56:14'),
(20, 1, 'Manage Departments', 'Manage Departments', 'attachment_11082021045648_pHSCFPebMD.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:56:48'),
(21, 1, '360 Virtual Tour Settings', '360 Virtual Tour Settings', 'attachment_11082021045709_fZ4kKxmMb5.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:57:09'),
(22, 1, 'Notification Emails', 'Notification Emails', 'attachment_11082021045733_MVMdMeNT96.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 16:57:33'),
(23, 1, 'Adhoc Reports', 'Adhoc Reports', 'attachment_11082021050957_R7NnjX3HFO.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:09:57'),
(24, 1, 'Lead Reports', 'Lead Reports', 'attachment_11082021051021_MI3vsrGihC.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:10:21'),
(25, 1, 'Customer Reports', 'Customer Reports', 'attachment_11082021051036_qs5LP9DCaS.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:10:36'),
(26, 1, 'KYC Reports', 'KYC Reports', 'attachment_11082021052424_wzp6Ld1FQu.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:24:24'),
(27, 1, 'Document Reports', 'Document Reports', 'attachment_11082021052807_Yt8HX86UAT.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:28:07'),
(28, 1, 'Lead Status Master', 'Lead Status Master', 'attachment_11082021052824_7SF7d4mIst.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:28:24'),
(29, 1, 'Activity Type Master', 'Activity Type Master', 'attachment_11082021054527_pPFfsz2cn3.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:45:27'),
(30, 1, 'Lead Source Master', 'Lead Source Master', 'attachment_11082021054548_SCtFRIsRrN.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-11 17:45:48'),
(31, 1, 'Lead Profile Master', 'Lead Profile Master', 'attachment_12082021101321_udfQrdlrlw.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:13:21'),
(32, 1, 'General Documents for all units Master', 'General Documents for all units Master', 'attachment_12082021101345_owoFogE4Kg.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:13:45'),
(33, 1, 'Private Document list for every unit Master', 'Private Document list for every unit Master', 'attachment_12082021101405_FhwuuJKFVT.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:14:05'),
(34, 1, 'Block Settings Master', 'Block Settings Master', 'attachment_12082021101419_4ZetPfXTDN.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:14:19'),
(35, 1, 'Floor Settings Master', 'Floor Settings Master', 'attachment_12082021101435_RwnvmoNLyl.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:14:35'),
(36, 1, 'Flat Type settings Master', 'Flat Type settings Master', 'attachment_12082021101457_FDcaLPnXY8.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:14:57'),
(37, 1, 'Property Brochure', 'Property Brochure', 'attachment_12082021101531_kYLsbv5aZx.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:15:31'),
(38, 1, 'Contact Information Master', 'Contact Information Master', 'attachment_12082021101603_TtPFBjDRkv.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:16:03'),
(39, 1, 'Payment Information  Master', 'Payment Information  Master', 'attachment_12082021101650_aHjqUwTGNv.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:16:50'),
(40, 1, 'Property Area Master', 'Property Area Master', 'attachment_12082021101715_RTTFw5xDpn.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:17:15'),
(41, 1, 'Department', 'Department', 'attachment_12082021101738_NkEUiuC2Be.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:17:38'),
(42, 1, 'Employee Role Master', 'Employee Role Master', 'attachment_12082021101758_QCZpgzcexr.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:17:58'),
(43, 1, 'KYC Document Types Master', 'KYC Document Types Master', 'attachment_12082021101816_CzZWuDbrgq.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:18:16'),
(44, 1, 'Company Settings', 'Company Settings', 'attachment_12082021101829_1TTAP3GMzz.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:18:29'),
(45, 1, 'Trash Property', 'Trash Property', 'attachment_12082021101855_rkSDqnakPf.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:18:55'),
(46, 1, 'Upload Videos', 'Upload Videos', 'attachment_12082021101918_vaQGtCaDIN.mp4', 'mp4', 1, 1, '2020-08-12 22:00:16', '2021-08-12 10:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `virtualtour_menusettings_tbl`
--

CREATE TABLE IF NOT EXISTS `virtualtour_menusettings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `menu` varchar(250) NOT NULL,
  `tour_code` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `virtualtour_menusettings_tbl`
--

INSERT INTO `virtualtour_menusettings_tbl` (`id`, `project_id`, `menu`, `tour_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'show', 'http://360view.gowthamhousing.com/?is=0-main-entrance', 1, '2020-08-24 00:26:02', '2020-08-24 00:26:02');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
