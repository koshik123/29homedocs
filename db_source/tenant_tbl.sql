-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 18, 2021 at 05:12 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `29homedocs_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tenant_tbl`
--

DROP TABLE IF EXISTS `tenant_tbl`;
CREATE TABLE IF NOT EXISTS `tenant_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT 0,
  `reg_date` varchar(150) DEFAULT NULL,
  `tenant_token` varchar(200) DEFAULT NULL,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `company` varchar(150) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant_tbl`
--

INSERT INTO `tenant_tbl` (`id`, `plan_id`, `reg_date`, `tenant_token`, `first_name`, `last_name`, `company`, `mobile`, `email`, `address`, `pincode`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-08-18', 'venpepsolution', 'Venpep', 'Solution', 'Venpep Solution', '+91 422 4379440', 'hello@venpep.com', '95A, Vyshnav Building,\r\nRace Course,\r\nCoimbatore', '641018', 1, '2020-08-12 22:00:16', '2020-08-12 22:00:16');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
