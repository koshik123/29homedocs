-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 24, 2020 at 06:45 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `postcrm_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_type_tbl`
--

DROP TABLE IF EXISTS `activity_type_tbl`;
CREATE TABLE IF NOT EXISTS `activity_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `activity_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_type_tbl`
--

INSERT INTO `activity_type_tbl` (`id`, `token`, `activity_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'follow-up', 'follow up', 1, 1, '2020-08-12 22:00:16', '2020-08-17 19:39:54'),
(2, 'follow-up-1', 'Follow Up 1', 1, 1, '2020-08-13 09:50:59', '2020-08-17 19:40:01'),
(3, 'follow-up-2', 'Follow Up 2', 1, 1, '2020-08-17 18:41:51', '2020-08-17 19:40:06'),
(4, 'processing', 'Processing', 1, 1, '2020-08-17 18:41:57', '2020-08-17 19:40:41'),
(5, 'completed', 'completed', 1, 1, '2020-08-17 18:42:02', '2020-08-17 19:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `adhocassign_logs_tbl`
--

DROP TABLE IF EXISTS `adhocassign_logs_tbl`;
CREATE TABLE IF NOT EXISTS `adhocassign_logs_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `adhoc_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adhocassign_logs_tbl`
--

INSERT INTO `adhocassign_logs_tbl` (`id`, `type`, `adhoc_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'assigned', 'manual', 1, 2, 27, 'SADAASDAS', 1, '2020-10-27 18:55:20', '2020-10-27 18:55:20'),
(2, 'reassigned', 'manual', 1, 3, 27, 'tesrt', 1, '2020-10-27 19:18:10', '2020-10-27 19:18:10'),
(3, 'reassigned', 'manual', 1, 4, 27, 'ty', 1, '2020-10-27 19:18:49', '2020-10-27 19:18:49'),
(4, 'assigned', 'manual', 1, 3, 26, 'asasas', 1, '2020-11-03 11:10:25', '2020-11-03 11:10:25'),
(5, 'reassigned', 'manual', 1, 4, 26, 'ss', 1, '2020-11-03 11:10:32', '2020-11-03 11:10:32'),
(6, 'assigned', 'manual', 1, 1, 28, 'tutyu', 1, '2020-11-03 11:59:30', '2020-11-03 11:59:30'),
(7, 'reassigned', 'manual', 1, 1, 27, 'sdsad', 1, '2020-11-03 11:59:42', '2020-11-03 11:59:42'),
(8, 'assigned', 'manual', 1, 1, 25, 'gg', 1, '2020-11-03 12:47:38', '2020-11-03 12:47:38'),
(9, 'assigned', 'manual', 1, 1, 18, 'dasdas', 1, '2020-11-03 13:45:04', '2020-11-03 13:45:04'),
(10, 'reassigned', 'manual', 1, 1, 18, 'dadqd', 1, '2020-11-03 13:45:17', '2020-11-03 13:45:17'),
(11, 'assigned', 'manual', 1, 1, 19, 'gjghjgh', 1, '2020-11-03 13:46:43', '2020-11-03 13:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_image_tbl`
--

DROP TABLE IF EXISTS `adhoc_image_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) NOT NULL,
  `adoc_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `image_name` varchar(250) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adhoc_image_tbl`
--

INSERT INTO `adhoc_image_tbl` (`id`, `ref_id`, `adoc_id`, `property_id`, `customer_id`, `image_name`, `file_type`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 2, 1, 1, 'EG7etmuHNfAZPKiwY6Mb0CyBhJVp4L.jpeg', 'jpeg', 's', 1, '2020-07-27 16:32:03', '2020-07-27 16:32:03'),
(2, 16, 5, 3, 1, 'uk34PI0wT167F_NMLYVXjSRGqZdoey.pdf', 'pdf', 'fsdf', 1, '2020-09-22 12:00:09', '2020-09-22 12:00:09'),
(3, 23, 23, 3, 1, '091nBq3jJ6SmVAYvNMQOoabTRp7rxd.jpg', 'jpg', 'eqwe', 1, '2020-09-26 00:22:14', '2020-09-26 00:22:14'),
(4, 24, 24, 3, 1, 'gBXb7GvP5AJshUC_tQIkNxW9M83Tip.jpg', 'jpg', 'sdfsd', 1, '2020-09-26 00:29:43', '2020-09-26 00:29:43'),
(5, 24, 24, 3, 1, 'zHPFZd47ox3eLSrARO8uKgbWUw9D62.jpg', 'jpg', 'uuuuuuuu', 1, '2020-09-26 00:29:43', '2020-09-26 00:29:43'),
(6, 25, 25, 3, 1, 'aGkW0t71gTrzLNSnviBKQZVw6MqYHJ.jpeg', 'jpeg', 'qqqqqqqqqq', 1, '2020-09-26 00:32:32', '2020-09-26 00:32:32'),
(7, 26, 26, 3, 1, 'azgMARuWFNs6vHKJxp0VwLUnGYe1Ik.jpg', 'jpg', 'sdfsd', 1, '2020-09-26 00:34:48', '2020-09-26 00:34:48'),
(8, 27, 27, 3, 1, 'g723fjoX0Kb6CT4qtBMUGkLwhZyQHA.jpg', 'jpg', 'dasdasd', 1, '2020-09-29 12:12:51', '2020-09-29 12:12:51'),
(9, 28, 28, 5, 1, 'TFH25JnajisDEheAgXxybfN8BCRO1I.jpg', 'jpg', 'adasa', 1, '2020-09-29 12:40:53', '2020-09-29 12:40:53'),
(10, 29, 28, 5, 1, 'fabRTIs0GjNESk96Arx53KYn_tDPyW.jpg', 'jpg', 'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, '2020-10-02 12:34:36', '2020-10-02 12:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_log_tbl`
--

DROP TABLE IF EXISTS `adhoc_log_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_log_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `request_type` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adhoc_log_tbl`
--

INSERT INTO `adhoc_log_tbl` (`id`, `type`, `request_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'created', 'user', 1, 0, 1, 'but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing', 1, '2020-07-27 15:35:52', '2020-07-27 15:35:52'),
(2, 'created', 'user', 1, 0, 2, 'sampleee', 1, '2020-07-27 15:47:45', '2020-07-27 15:47:45'),
(3, 'reply', 'user', 1, 0, 2, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2020-07-27 16:23:10', '2020-07-27 16:23:10'),
(4, 'reply', 'user', 1, 0, 2, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2020-07-27 16:31:43', '2020-07-27 16:31:43'),
(5, 'reply', 'user', 1, 0, 2, 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2020-07-27 16:32:03', '2020-07-27 16:32:03'),
(6, 'reply', 'user', 1, 0, 2, 't is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1, '2020-07-27 16:33:22', '2020-07-27 16:33:22'),
(7, 'reply', 'admin', 0, 1, 2, 'distracted by the readable content of a page when looking at its layout.', 1, '2020-07-27 20:51:55', '2020-07-27 20:51:55'),
(8, 'reply', 'admin', 0, 1, 2, 'Distracted by the readable content of a page when looking at its layout', 1, '2020-07-27 20:52:43', '2020-07-27 20:52:43'),
(11, 'reply', 'admin', 0, 1, 2, 'fghfgh', 1, '2020-08-18 14:24:03', '2020-08-18 14:24:03'),
(12, 'reply', 'admin', 0, 1, 2, 'sample', 1, '2020-08-18 14:30:30', '2020-08-18 14:30:30'),
(13, 'reply', 'admin', 0, 1, 2, 'gdfg', 1, '2020-08-21 00:41:48', '2020-08-21 00:41:48'),
(14, 'created', 'user', 1, 0, 3, 'asdasd', 1, '2020-09-22 11:55:24', '2020-09-22 11:55:24'),
(15, 'created', 'user', 1, 0, 4, 'sasdasd', 1, '2020-09-22 11:58:13', '2020-09-22 11:58:13'),
(16, 'created', 'user', 1, 0, 5, 'ertert', 1, '2020-09-22 12:00:09', '2020-09-22 12:00:09'),
(17, 'reply', 'admin', 0, 1, 20, 'utyutyu', 1, '2020-09-24 00:31:56', '2020-09-24 00:31:56'),
(18, 'reply', 'admin', 0, 1, 20, 'tyutyuytu', 1, '2020-09-24 00:32:04', '2020-09-24 00:32:04'),
(19, 'reply', 'admin', 0, 1, 20, 'hjkhjkj', 1, '2020-09-24 00:32:55', '2020-09-24 00:32:55'),
(20, 'reply', 'admin', 0, 1, 20, 'ghjghj', 1, '2020-09-24 00:34:40', '2020-09-24 00:34:40'),
(21, 'created', 'user', 1, 0, 21, 'tyrtyrty', 1, '2020-09-24 01:35:25', '2020-09-24 01:35:25'),
(22, 'reply', 'admin', 0, 1, 21, 'hfghf', 1, '2020-09-24 01:35:53', '2020-09-24 01:35:53'),
(23, 'reply', 'admin', 0, 1, 23, 'dqwe', 1, '2020-09-26 00:22:14', '2020-09-26 00:22:14'),
(24, 'created', 'user', 1, 0, 24, '567567', 1, '2020-09-26 00:29:43', '2020-09-26 00:29:43'),
(25, 'created', 'user', 1, 0, 25, 'q', 1, '2020-09-26 00:32:32', '2020-09-26 00:32:32'),
(26, 'created', 'admin', 1, 0, 26, 'aaa', 1, '2020-09-26 00:34:48', '2020-09-26 00:34:48'),
(27, 'created', 'user', 1, 0, 27, 'samu test', 1, '2020-09-29 12:12:51', '2020-09-29 12:12:51'),
(28, 'created', 'user', 1, 0, 28, 'dasdasdasdasdasdas', 1, '2020-09-29 12:40:53', '2020-09-29 12:40:53'),
(29, 'reply', 'user', 1, 0, 28, 'sa', 1, '2020-10-02 12:34:36', '2020-10-02 12:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `adhoc_request_tbl`
--

DROP TABLE IF EXISTS `adhoc_request_tbl`;
CREATE TABLE IF NOT EXISTS `adhoc_request_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_uid` varchar(255) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `request_type` varchar(250) NOT NULL,
  `requesttype_id` int(11) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `request_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `assign_status` int(11) NOT NULL,
  `closed_status` int(11) NOT NULL,
  `assign_employee_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adhoc_request_tbl`
--

INSERT INTO `adhoc_request_tbl` (`id`, `ticket_uid`, `property_id`, `customer_id`, `employee_id`, `ref_id`, `type`, `request_type`, `requesttype_id`, `subject`, `remarks`, `request_status`, `status`, `assign_status`, `closed_status`, `assign_employee_id`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'T0001', 1, 1, 0, 0, 'common', 'user', 1, 'Payment was rejected', 'but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing', 0, 1, 0, 0, 0, 1, '2020-07-27 15:35:44', '2020-07-27 15:35:44'),
(2, 'T0002', 1, 1, 0, 1, 'general document', 'user', 2, 'Request for approve payment', 'sampleee', 0, 1, 0, 0, 0, 1, '2020-07-27 15:47:37', '2020-08-21 00:41:48'),
(3, 'T0003', 3, 1, 0, 0, 'common', 'user', 1, 'asdasd', 'asdasd', 0, 1, 0, 0, 0, 1, '2020-09-22 11:55:16', '2020-09-22 11:55:16'),
(4, 'T0004', 3, 1, 0, 0, 'common', 'user', 1, 'a', 'sasdasd', 0, 1, 0, 0, 0, 1, '2020-09-22 11:58:00', '2020-09-22 11:58:00'),
(5, 'T0005', 3, 1, 0, 0, 'common', 'user', 1, 'dtert', 'ertert', 0, 1, 0, 0, 0, 1, '2020-09-22 11:59:56', '2020-09-22 11:59:56'),
(6, 'T0006', 3, 1, 0, 0, 'common', 'user', 2, 'ghjgjhg', 'ghjghjghghj', 0, 1, 0, 0, 0, 1, '2020-09-23 19:31:27', '2020-09-23 19:31:27'),
(7, 'T0007', 0, 0, 0, 0, 'common', 'admin', 1, 'Request for approve payment', 'tyutyu', 0, 1, 0, 0, 0, 1, '2020-09-23 23:46:32', '2020-09-23 23:46:32'),
(16, 'T0016', 3, 1, 0, 0, 'common', 'admin', 1, 'ewer', 'werwer', 0, 1, 0, 0, 0, 1, '2020-09-24 00:26:27', '2020-09-24 00:26:27'),
(17, 'T0017', 3, 1, 0, 0, 'common', 'admin', 1, 'ewer', 'werwer', 0, 1, 0, 0, 0, 1, '2020-09-24 00:27:12', '2020-09-24 00:27:12'),
(18, 'T0018', 3, 1, 0, 0, 'common', 'admin', 2, 'tyutyutyu', 'tyutyu', 0, 1, 1, 0, 1, 1, '2020-09-24 00:29:02', '2020-11-03 13:45:16'),
(19, 'T0019', 3, 1, 0, 0, 'common', 'admin', 2, 'tyutyutyu', 'tyutyu', 0, 1, 1, 0, 1, 1, '2020-09-24 00:29:34', '2020-09-24 00:29:34'),
(20, 'T0020', 3, 1, 0, 0, 'common', 'user', 1, 'uyi', 'yui', 0, 1, 0, 0, 0, 1, '2020-09-24 00:29:56', '2020-09-24 00:34:40'),
(21, 'T0021', 3, 1, 0, 5, 'community gallery', 'user', 2, 'Request for approve payment', 'tyrtyrty', 0, 1, 0, 0, 0, 1, '2020-09-24 01:35:18', '2020-09-24 01:35:53'),
(22, 'T0022', 3, 1, 0, 0, 'common', 'user', 2, 'iuio', 'uioui', 0, 1, 0, 0, 0, 1, '2020-09-24 01:36:17', '2020-09-24 01:36:17'),
(23, 'T0023', 3, 1, 0, 0, 'common', 'admin', 3, 'Request for approve payment', 'erwerwer', 0, 1, 0, 0, 0, 1, '2020-09-26 00:21:51', '2020-09-26 00:22:14'),
(24, 'T0024', 3, 1, 0, 0, 'common', 'admin', 2, '67567', '567567', 0, 1, 0, 0, 0, 1, '2020-09-26 00:29:35', '2020-09-26 00:29:35'),
(25, 'T0025', 3, 1, 0, 0, 'common', 'admin', 1, 'sdassas', 'q', 0, 1, 1, 0, 1, 1, '2020-09-26 00:32:24', '2020-09-26 00:32:24'),
(26, 'T0026', 3, 1, 0, 0, 'common', 'admin', 3, 'Request for approve payment', 'aaa', 0, 1, 1, 0, 4, 1, '2020-09-26 00:34:40', '2020-11-03 11:10:32'),
(27, 'T0027', 3, 1, 0, 0, 'common', 'user', 3, 'samu', 'samu test', 0, 1, 1, 0, 1, 1, '2020-09-29 12:12:42', '2020-11-03 11:59:42'),
(28, 'T0028', 5, 1, 0, 21, 'private document', 'user', 1, 'asdas', 'dasdasdasdasdasdas', 1, 1, 1, 0, 1, 1, '2020-09-29 12:40:44', '2020-09-29 12:40:44');

-- --------------------------------------------------------

--
-- Table structure for table `admin_tbl`
--

DROP TABLE IF EXISTS `admin_tbl`;
CREATE TABLE IF NOT EXISTS `admin_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ad_token` varchar(250) NOT NULL,
  `ad_name` varchar(250) NOT NULL,
  `ad_mobile` varchar(50) NOT NULL,
  `ad_email` varchar(250) NOT NULL,
  `ad_dob` varchar(250) NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `ad_city` varchar(250) NOT NULL,
  `ad_pincode` varchar(250) NOT NULL,
  `ad_password` varchar(250) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `ad_super_admin` int(1) NOT NULL,
  `ad_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_tbl`
--

INSERT INTO `admin_tbl` (`id`, `ad_token`, `ad_name`, `ad_mobile`, `ad_email`, `ad_dob`, `address1`, `address2`, `ad_city`, `ad_pincode`, `ad_password`, `ad_type`, `ad_super_admin`, `ad_status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '987654321', 'admin@postcrm.com', '2020-05-30', 'xxxxxx', 'yyyyy', 'cbe', 'tamil nadu', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 1, 1, 1, '2016-06-01 10:00:00', '2020-05-28 09:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `assignadhoc_tbl`
--

DROP TABLE IF EXISTS `assignadhoc_tbl`;
CREATE TABLE IF NOT EXISTS `assignadhoc_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `remove_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignadhoc_tbl`
--

INSERT INTO `assignadhoc_tbl` (`id`, `type`, `created_by`, `created_to`, `ref_id`, `priority`, `remarks`, `status`, `remove_status`, `created_at`, `updated_at`) VALUES
(1, 'assign', 1, 2, 27, 'medium', 'SADAASDAS', 0, 0, '2020-10-27 18:55:20', '2020-10-27 19:18:10'),
(2, 'reassign', 1, 3, 27, 'medium', 'tesrt', 0, 0, '2020-10-27 19:18:10', '2020-10-27 19:18:49'),
(3, 'reassign', 1, 4, 27, 'low', 'ty', 0, 0, '2020-10-27 19:18:49', '2020-11-03 11:59:42'),
(4, 'assign', 1, 3, 26, 'low', 'asasas', 0, 0, '2020-11-03 11:10:25', '2020-11-03 11:10:32'),
(5, 'reassign', 1, 4, 26, 'low', 'ss', 1, 0, '2020-11-03 11:10:32', '2020-11-03 11:10:32'),
(6, 'assign', 1, 1, 28, 'high', 'tutyu', 1, 0, '2020-11-03 11:59:30', '2020-11-03 11:59:30'),
(7, 'reassign', 1, 1, 27, 'medium', 'sdsad', 1, 0, '2020-11-03 11:59:42', '2020-11-03 11:59:42'),
(8, 'assign', 1, 1, 25, 'low', 'gg', 1, 0, '2020-11-03 12:47:38', '2020-11-03 12:47:38'),
(9, 'assign', 1, 1, 18, 'medium', 'dasdas', 0, 0, '2020-11-03 13:45:03', '2020-11-03 13:45:16'),
(10, 'reassign', 1, 1, 18, 'high', 'dadqd', 1, 0, '2020-11-03 13:45:16', '2020-11-03 13:45:16'),
(11, 'assign', 1, 1, 19, 'medium', 'gjghjgh', 1, 0, '2020-11-03 13:46:43', '2020-11-03 13:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `assignlead_tbl`
--

DROP TABLE IF EXISTS `assignlead_tbl`;
CREATE TABLE IF NOT EXISTS `assignlead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `remove_status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `block_tbl`
--

DROP TABLE IF EXISTS `block_tbl`;
CREATE TABLE IF NOT EXISTS `block_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `block` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_tbl`
--

INSERT INTO `block_tbl` (`id`, `token`, `block`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'blockc', 'Block c', 1, 1, '2020-05-28 16:09:22', '2020-09-26 11:07:00'),
(2, 'blockb', 'Block B', 1, 1, '2020-06-03 09:06:06', '2020-09-26 11:05:45'),
(3, 'blockd', 'Block D', 1, 1, '2020-09-26 10:58:35', '2020-09-26 11:05:50'),
(4, 'blocka', 'Block A', 1, 1, '2020-09-26 11:03:06', '2020-09-26 11:06:27'),
(5, 'blocke', 'Block e', 1, 1, '2020-09-26 11:06:16', '2020-09-26 11:06:16');

-- --------------------------------------------------------

--
-- Table structure for table `company_settings_tbl`
--

DROP TABLE IF EXISTS `company_settings_tbl`;
CREATE TABLE IF NOT EXISTS `company_settings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `website_address` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_settings_tbl`
--

INSERT INTO `company_settings_tbl` (`id`, `phone`, `email`, `website_address`, `address`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '9942387100', 'admin@postcrm.com', 'admin@postcrm.com', '99A vyasuva building', 1, 1, '2020-08-05 20:10:45', '2020-08-05 20:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_list_tbl`
--

DROP TABLE IF EXISTS `contactinfo_list_tbl`;
CREATE TABLE IF NOT EXISTS `contactinfo_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `designation` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactinfo_list_tbl`
--

INSERT INTO `contactinfo_list_tbl` (`id`, `contact_id`, `name`, `designation`, `mobile`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 'name 1', 'role 1', '994238100', 'koshik1@webykart.com', 1, '2020-06-06 10:39:58', '2020-06-06 10:39:58'),
(2, 3, 'name 2', 'role 2', '9942381001', 'koshik2@webykart.com', 1, '2020-06-06 10:39:58', '2020-06-06 10:39:58');

-- --------------------------------------------------------

--
-- Table structure for table `contactinfo_tbl`
--

DROP TABLE IF EXISTS `contactinfo_tbl`;
CREATE TABLE IF NOT EXISTS `contactinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contactinfo_tbl`
--

INSERT INTO `contactinfo_tbl` (`id`, `token`, `name`, `role`, `email`, `mobile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(3, 'sample-1', 'sample 1', 'Admin', 'admin@postcrm.com', '9942387100', 1, 1, '2020-06-06 10:39:58', '2020-07-22 21:47:09'),
(4, 'sample-2df', 'sample 2df', 'Staff', 'koshik@webykart.com', '9942387100', 1, 1, '2020-06-13 16:51:15', '2020-07-22 21:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `customerdatasheet_tbl`
--

DROP TABLE IF EXISTS `customerdatasheet_tbl`;
CREATE TABLE IF NOT EXISTS `customerdatasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerdatasheet_tbl`
--

INSERT INTO `customerdatasheet_tbl` (`id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(2, 'We7WOWsJtNKP87V', '05-09-2020_uKh8SdY9_samplecustomerdata1.xlsx', '2020-09-05 12:41:39', '2020-09-05 12:41:39', 0),
(3, 'T57EtxcJYa7Q07r', '08-09-2020_8vqPlDrf_emailcustomerdata (1).xlsx', '2020-09-08 00:24:10', '2020-09-08 00:24:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_profile_tbl`
--

DROP TABLE IF EXISTS `customer_profile_tbl`;
CREATE TABLE IF NOT EXISTS `customer_profile_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `customer_profile` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_profile_tbl`
--

INSERT INTO `customer_profile_tbl` (`id`, `token`, `customer_profile`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'profile-1', 'profile 1', 1, 1, '2020-09-22 15:38:24', '2020-09-22 15:43:01'),
(2, 'profile-2', 'profile 2', 1, 1, '2020-09-22 16:12:47', '2020-09-22 16:12:47'),
(4, 'profile1', 'profile 1', 1, 1, '2020-09-27 17:09:23', '2020-09-27 17:09:23'),
(5, 'profile3', 'profile 3', 1, 1, '2020-09-27 17:09:24', '2020-09-27 17:09:24'),
(6, 'business', 'Business', 1, 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(7, 'doctor', 'Doctor', 1, 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

DROP TABLE IF EXISTS `customer_tbl`;
CREATE TABLE IF NOT EXISTS `customer_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_token` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `priority` varchar(250) NOT NULL,
  `allotment_date` varchar(250) DEFAULT NULL,
  `assign_remarks` longtext DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `has_psw` varchar(250) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `nationality` varchar(250) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `ad_contact_person` varchar(250) DEFAULT NULL,
  `ad_relationship` varchar(250) DEFAULT NULL,
  `ad_mobile` varchar(250) DEFAULT NULL,
  `ad_email` varchar(250) DEFAULT NULL,
  `ad_address` varchar(250) DEFAULT NULL,
  `ad_city` varchar(250) DEFAULT NULL,
  `ad_state` varchar(250) DEFAULT NULL,
  `ad_pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `kyc_status` int(11) NOT NULL,
  `approved_on` varchar(250) DEFAULT NULL,
  `kyc_submittedon` varchar(250) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `kyc_remarks` longtext NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `resetpassword` int(11) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`id`, `import_token`, `token`, `type`, `lead_id`, `uid`, `priority`, `allotment_date`, `assign_remarks`, `name`, `gender`, `dob`, `mobile`, `email`, `password`, `has_psw`, `address`, `city`, `state`, `nationality`, `pincode`, `ad_contact_person`, `ad_relationship`, `ad_mobile`, `ad_email`, `ad_address`, `ad_city`, `ad_state`, `ad_pincode`, `description`, `status`, `kyc_status`, `approved_on`, `kyc_submittedon`, `approved_by`, `kyc_remarks`, `assign_status`, `resetpassword`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'tQkM9JiwxSm3Ruj', 'koshik', NULL, NULL, 'CU0001', 'low', '', '', 'Koshik', 'male', NULL, '9442264770', 'koshik5@webykart.com', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'TBfjCKOg', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-10-01 10:01:16', NULL, 1, 'approved', 1, NULL, 1, '2020-09-05 13:00:27', '2020-10-08 14:03:58'),
(2, 'tQkM9JiwxSm3Ruj', 'mrs.poorani.k.t', NULL, NULL, 'CU0002', 'low', NULL, NULL, 'MRS.POORANI.K.T', '', NULL, '9585523822', 'dhileepan62@gmail.com', '71b00f4f26efa57299889ae37e5175a7ab998a35', 'xlBKeDTU', '85,kalyani nagar,udumalaipettai,gandhi nagar,tirupur-642154', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:30', '2020-09-05 13:00:30'),
(3, 'tQkM9JiwxSm3Ruj', 'r.harirajan', NULL, NULL, 'CU0003', 'low', NULL, NULL, 'R.HARIRAJAN', '', NULL, '9818071456', 'rharirajan@gmail.com', 'c8b38f09dfd26cbff96783bb0560a68076f3455d', 'yqvQByKe', '22/A, D.D.A MIG-Flats,Pocket-2,Sector-7,Dwarka(west) West Delhi-110075', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:33', '2020-09-05 13:00:33'),
(4, 'tQkM9JiwxSm3Ruj', 'p.s-vishnoo-vardhan', NULL, NULL, 'CU0004', 'low', NULL, NULL, 'P.S VISHNOO VARDHAN', '', NULL, '8754589225', 'maddyvishnoo@gmail.com', 'e2b3e5cf8732bbd2dc55f3af2990103664426ce4', 'IpXZsyqn', 'PLOT NO.13,4Th Cross Street, Siva Sakthi Nagar, Madhavaram-Chennai-600060', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:36', '2020-09-05 13:00:36'),
(5, 'tQkM9JiwxSm3Ruj', 'somakumar-and-preetha', NULL, NULL, 'CU0005', 'low', NULL, NULL, 'SOMAKUMAR and PREETHA', '', NULL, '9894769561', 'rathina.sami@gmail.com', 'deb7bc1cf20a00c50a673894da691fd55a4571ae', 'oXROUXPT', '4/404,Kottilingal Manniah House,Pazhambalacode,Palakkad,Tarur,Kerala-678544', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:39', '2020-09-05 13:00:39'),
(6, 'tQkM9JiwxSm3Ruj', 'mr.-zohair-m.-kagalwala-and-mrs.-nisrin-zohair', NULL, NULL, 'CU0006', 'low', NULL, NULL, 'MR. ZOHAIR M. KAGALWALA AND MRS. NISRIN ZOHAIR', '', NULL, '9363102027', 'mbtools@yahoo.co.in', '11b3b221045b6f45686f3851230e6ab052d82ae8', 'VSdWoIsT', '14,Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:42', '2020-09-05 13:00:42'),
(7, 'tQkM9JiwxSm3Ruj', 'tn-prabhakaran', NULL, NULL, 'CU0007', 'low', NULL, NULL, 'TN PRABHAKARAN', '', NULL, '9952422004', 'trichyprabhaharan@gmail.com', '67aa624efcf37dddb3001106904612c1d5e456a5', 'XjPUBiKQ', 'B3, Srinivasa Residency, 45, Sankaran Pillai Road,Opp to Aadhi Kottai Mariamman Kovil, Tiruchirappalli, Tamilnadu- 620002', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:45', '2020-09-05 13:00:45'),
(8, 'tQkM9JiwxSm3Ruj', 'sathish-kumar', NULL, NULL, 'CU0008', 'low', NULL, NULL, 'SATHISH KUMAR', '', NULL, '9486071974', 'rsathishkumarr@gmail.com', '36ebb45f75cf26f002e6efdb3e9d7d1b235efa95', 'LVnZngbh', '2/102, Evadugapalayam Elavanthi,Palladam,Tiruppur-641664', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:48', '2020-09-05 13:00:48'),
(9, 'tQkM9JiwxSm3Ruj', 'mr.ranjith--g.manju', NULL, NULL, 'CU0009', 'low', NULL, NULL, 'MR.RANJITH & G.MANJU', '', NULL, '9940975500', 'ranjith@futurerea.in', '542bdc5e7bb51514dabf1a0c7ce29050209bd814', 'opwlYqhY', 'B2-4J -Gowtham ABC Avenue,Peelamedu', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:51', '2020-09-05 13:00:51'),
(10, 'tQkM9JiwxSm3Ruj', 'sree-vidya-bhupese', NULL, NULL, 'CU0010', 'low', NULL, NULL, 'SREE VIDYA BHUPESE', '', NULL, '9994924984', 'vidhujan87@gmail.com', '115ce4d7f81ade9ec3c11718f6cebbae7b23d695', 'rntwYcnx', 'B4-4G, Gowtham ABC Avenue Apartment, Avarampalayam Road, Coimbatore- 641004', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:54', '2020-09-05 13:00:54'),
(11, 'tQkM9JiwxSm3Ruj', 'anitha', NULL, NULL, 'CU0011', 'low', NULL, NULL, 'ANITHA', '', NULL, '9585523599', 'nithi_anii@yahoo.co.in', 'fe3a9dde5abb105ee820e91eabff0b58dfe60723', 'hiNOPiXG', '1B,3rd Block-,Coral Castle Apartment,Peelamedu,Cbe-4', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:57', '2020-09-05 13:00:57'),
(12, 'tQkM9JiwxSm3Ruj', 'mr.k.ayyavu', NULL, NULL, 'CU0012', 'low', NULL, NULL, 'MR.K.AYYAVU', '', NULL, '9443910804', 'ayyavus@gmail.com', 'c2cf8204a8469a8c1a6cc760c9fe8ea66eda3551', 'QURLNffx', 'A-11 Brindavan Nagar, Kalapatti , Coimbatore Aerodrome,Coimbatore-641014', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:00:59', '2020-09-05 13:00:59'),
(13, 'tQkM9JiwxSm3Ruj', 'saraswathi-jeyaraju', NULL, NULL, 'CU0013', 'low', NULL, NULL, 'SARASWATHI JEYARAJU', '', NULL, '9123589373', 'kamikki@hotmail.com', '46bf4e086a74dc3ab9bdc9b3050e0505fdfa2609', 'fewLVsMp', '6/236A,Muthu Gopal Nagar, Anuppurpalayam puthur,Periyar Colony,Tiruppur-641652', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:01:02', '2020-09-05 13:01:02'),
(14, 'tQkM9JiwxSm3Ruj', 'umamaheshwaran-and-priyadarsini.u', NULL, NULL, 'CU0014', 'low', NULL, NULL, 'UMAMAHESHWARAN and PRIYADARSINI.U', '', NULL, '9894471544', 'mahesh71554@gmail.com', '2961c3a6de4d09d2b002eca0f4401ec66be2c72c', 'swPWMMzY', '15-G Natesa Gounder Layout,Rathinapuri ,Coimbatore-27', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:01:05', '2020-09-05 13:01:05'),
(15, 'tQkM9JiwxSm3Ruj', 'm.s.gowtham', NULL, NULL, 'CU0015', 'low', NULL, NULL, 'M.S.GOWTHAM', '', NULL, '9842535570', 'msgowtham6@gmail.com', '92cb96e3d9c1894d622db502cfbe94a085befded', 'Jcunxrru', 'F0A,Block-2,Jains sri Saanvi Apartment,Ramasamy Nagar,Uppilipalayam(po) Coimbatore-641015', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:01:08', '2020-09-05 13:01:08'),
(16, 'tQkM9JiwxSm3Ruj', 'sharanya', NULL, NULL, 'CU0016', 'low', NULL, NULL, 'SHARANYA', '', NULL, '9894337199', 'nilavan@yahoo.co.in', '01962f5c96054060bbcb37efc5077b7385496185', 'arYMmDJT', '4/20,Naicken Palayam SRKV-Via,cbe-641020', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-05 13:01:11', '2020-09-05 13:01:11'),
(17, NULL, 'sample-sample', 'moved', 7, 'CU0017', 'low', NULL, NULL, 'sample sample', 'male', NULL, '9942387565', 'koshik@webykart.com', '9e1a7dffef9fa0ce77a47432bb568a893e331bd4', 'WzhZtqFG', '49w maini', 'cbe', 'tn', NULL, '641103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-21 09:20:24', '2020-09-21 09:20:24'),
(19, NULL, 'ram-kumar', 'moved', 4, 'CU0019', 'low', NULL, NULL, 'Ram Kumar', 'male', NULL, '994238710011', 'koshik1@webykart.com', '20cbd4709991ea065a2e8dff8bdd16af5f75d4a7', 'fjKHFWkc', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '', 0, NULL, 1, '2020-09-21 10:28:10', '2020-09-21 10:28:10');

-- --------------------------------------------------------

--
-- Table structure for table `datasheet_tbl`
--

DROP TABLE IF EXISTS `datasheet_tbl`;
CREATE TABLE IF NOT EXISTS `datasheet_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_token` varchar(50) NOT NULL,
  `uploaded_sheet` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datasheet_tbl`
--

INSERT INTO `datasheet_tbl` (`id`, `import_token`, `uploaded_sheet`, `created_at`, `updated_at`, `status`) VALUES
(1, '4ulsFbuBeO17tp5', '27-09-2020_p4lf84ap_sample_content.xlsx', '2020-09-27 15:01:35', '2020-09-27 17:09:24', 1),
(2, 'SyDxz7NhpBL0Y7L', '01-10-2020_qWrggt0w_01-10-2020.xlsx', '2020-10-01 16:10:42', '2020-10-01 16:10:49', 1),
(4, 'RQPhNbKBq3MJwRs', '05-10-2020_cvGSsgcQ_27.xlsx', '2020-10-05 16:04:01', '2020-10-05 16:04:09', 1),
(5, '72lovaei9GJZrm0', '05-10-2020_VjhWc5zs_27.xlsx', '2020-10-05 16:04:39', '2020-10-05 16:04:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `department_tbl`
--

DROP TABLE IF EXISTS `department_tbl`;
CREATE TABLE IF NOT EXISTS `department_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `short_description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_tbl`
--

INSERT INTO `department_tbl` (`id`, `token`, `name`, `short_description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'dep1', 'Dep 1', 'sam', 1, 1, '2020-10-12 20:41:22', '2020-10-23 09:23:54'),
(2, 'dep2', 'dep 2', '', 1, 1, '2020-10-15 09:41:25', '2020-10-15 09:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `employee_permission_tbl`
--

DROP TABLE IF EXISTS `employee_permission_tbl`;
CREATE TABLE IF NOT EXISTS `employee_permission_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(250) DEFAULT NULL,
  `lead` int(11) NOT NULL,
  `manage_lead` int(11) NOT NULL,
  `lead_type` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `lead_source` int(11) NOT NULL,
  `profile_master` int(11) NOT NULL,
  `import_lead` int(11) NOT NULL,
  `customers` varchar(250) DEFAULT NULL,
  `manage_customer` int(11) NOT NULL,
  `import_customer` int(11) NOT NULL,
  `kyc_documents` int(11) NOT NULL,
  `adhoc_request` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `adhoc` int(11) NOT NULL,
  `open_request` int(11) NOT NULL,
  `close_request` int(11) NOT NULL,
  `all_request` int(11) NOT NULL,
  `documents` int(11) NOT NULL,
  `general_documents` int(11) NOT NULL,
  `document_checklist` int(11) NOT NULL,
  `property` int(11) NOT NULL,
  `manage_property` int(11) NOT NULL,
  `property_chart` int(11) NOT NULL,
  `gallery` int(11) NOT NULL,
  `block` int(11) NOT NULL,
  `floor` int(11) NOT NULL,
  `flat_type` int(11) NOT NULL,
  `brochure` int(11) NOT NULL,
  `contact_info` int(11) NOT NULL,
  `payment_info` int(11) NOT NULL,
  `virtual_tour` int(11) NOT NULL,
  `area_master` int(11) NOT NULL,
  `department` int(11) NOT NULL,
  `settings` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `request_type` int(11) NOT NULL,
  `kycdoc_type` int(11) NOT NULL,
  `notification` int(11) NOT NULL,
  `company` int(11) NOT NULL,
  `role_master` int(11) NOT NULL,
  `news` int(11) NOT NULL,
  `reports` int(11) NOT NULL,
  `adhoc_report` int(11) NOT NULL,
  `lead_report` int(11) NOT NULL,
  `activity_report` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_permission_tbl`
--

INSERT INTO `employee_permission_tbl` (`id`, `employee_id`, `lead`, `manage_lead`, `lead_type`, `activity_type`, `lead_source`, `profile_master`, `import_lead`, `customers`, `manage_customer`, `import_customer`, `kyc_documents`, `adhoc_request`, `invoice`, `adhoc`, `open_request`, `close_request`, `all_request`, `documents`, `general_documents`, `document_checklist`, `property`, `manage_property`, `property_chart`, `gallery`, `block`, `floor`, `flat_type`, `brochure`, `contact_info`, `payment_info`, `virtual_tour`, `area_master`, `department`, `settings`, `employee`, `request_type`, `kycdoc_type`, `notification`, `company`, `role_master`, `news`, `reports`, `adhoc_report`, `lead_report`, `activity_report`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '1', 0, 0, 0, 0, 0, 0, 0, '1', 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, '2020-06-06 15:06:09', '2020-06-06 15:12:37'),
(2, '2', 1, 1, 1, 1, 1, 1, 0, '1', 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2020-06-13 19:03:11', '2020-11-09 14:12:40'),
(3, '3', 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-08-18 08:27:42', '2020-08-18 08:27:42'),
(4, '4', 0, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, '2020-10-15 10:20:38', '2020-10-15 10:20:38');

-- --------------------------------------------------------

--
-- Table structure for table `employee_role_tbl`
--

DROP TABLE IF EXISTS `employee_role_tbl`;
CREATE TABLE IF NOT EXISTS `employee_role_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `employee_role` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_role_tbl`
--

INSERT INTO `employee_role_tbl` (`id`, `token`, `employee_role`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'role1', 'role 1', 1, 1, '2020-10-13 00:37:28', '2020-10-13 10:21:53'),
(2, 'role2', 'Role 2', 1, 1, '2020-10-13 10:22:00', '2020-10-13 10:22:05'),
(3, 'role3', 'Role 3', 1, 1, '2020-10-13 10:22:10', '2020-10-13 10:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `employee_tbl`
--

DROP TABLE IF EXISTS `employee_tbl`;
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `is_manager` int(11) NOT NULL,
  `gender` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `super_admin` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_tbl`
--

INSERT INTO `employee_tbl` (`id`, `employee_uid`, `token`, `name`, `role_id`, `department_id`, `is_manager`, `gender`, `password`, `email`, `mobile`, `address`, `city`, `state`, `pincode`, `status`, `super_admin`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'EMP0001', 'admin', 'Admin', 3, 0, 1, 'female', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Admin@postcrm.com', '9942387100', '', 'coimbatore', '', '6441103', 1, 1, 1, '2020-06-06 15:06:09', '2020-10-08 10:25:31'),
(2, 'EMP0002', 'ilango', 'Ilango', 3, 1, 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Koshik@webykart.com', '99423871001', '99A vyasuva building', 'coimbatore', 'tamil nadu', '2222', 1, 0, 1, '2020-06-13 19:03:11', '2020-10-23 10:15:22'),
(3, 'EMP0003', 'srinivasan', 'Srinivasan', 1, 1, 0, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koshik1@webykart.com', '9942009450', '', '', '', '', 1, 0, 1, '2020-08-18 08:27:42', '2020-10-15 10:28:10'),
(4, 'EMP0004', 'koushik', 'koushik', 2, 2, 1, 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'koshikprabhur@venpep.com', '99423871012', '', '', '', '', 1, 0, 1, '2020-10-15 10:20:38', '2020-10-15 10:30:11');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_list_tbl`
--

DROP TABLE IF EXISTS `flatvilla_documenttype_list_tbl`;
CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_list_tbl`
--

INSERT INTO `flatvilla_documenttype_list_tbl` (`id`, `document_id`, `doc_name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'EB Form', 'test', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(2, 1, 'EB Bill', 'test', 1, '2020-06-16 09:54:06', '2020-06-16 09:54:06'),
(3, 2, 'Building  Ouside ', 'Building Ouside View ', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(4, 2, 'Building inner View', 'Building inner View', 1, '2020-06-16 09:55:52', '2020-06-16 09:55:52'),
(6, 0, 'Building Ouside View', 'Form', 1, '2020-07-07 14:21:23', '2020-07-07 14:21:23'),
(7, 0, 'EB Form', 'wdsadas', 1, '2020-07-07 14:22:44', '2020-07-07 14:22:44'),
(10, 5, 'Building Ouside View', 'eee', 1, '2020-07-07 14:23:32', '2020-07-07 14:23:32'),
(11, 6, 'te', 'tes', 1, '2020-09-12 14:28:05', '2020-09-12 14:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `flatvilla_documenttype_tbl`
--

DROP TABLE IF EXISTS `flatvilla_documenttype_tbl`;
CREATE TABLE IF NOT EXISTS `flatvilla_documenttype_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `doc_title` varchar(250) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `publish_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flatvilla_documenttype_tbl`
--

INSERT INTO `flatvilla_documenttype_tbl` (`id`, `token`, `doc_title`, `sort_order`, `status`, `publish_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'eb-documents', 'EB Documents', 1, 1, 1, 0, 0, '', 0, '', '', 1, '2020-06-16 09:54:06', '2020-09-12 15:24:39'),
(2, 'building-blue-print-n', 'Building Blue Print n', 2, 1, 0, 0, 0, '', 1, '2020-09-12 16:25:06', 'hjk', 1, '2020-06-16 09:55:52', '2020-09-12 16:25:06'),
(5, 'building-sketch', 'Building Sketch', 3, 1, 0, 0, 0, '', 1, '2020-09-12 16:24:59', 'yuhyu', 1, '2020-07-07 13:49:39', '2020-09-12 16:24:59'),
(6, 'tesrt', 'tesrt', 2, 1, 1, 0, 0, '', 0, '', '', 1, '2020-09-12 14:28:05', '2020-09-12 15:23:56');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_master`
--

DROP TABLE IF EXISTS `flat_type_master`;
CREATE TABLE IF NOT EXISTS `flat_type_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `flat_variant` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_master`
--

INSERT INTO `flat_type_master` (`id`, `token`, `flat_variant`, `description`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '1bhk', '1BHK', 'Sample', 1, 1, '2020-06-13 14:03:37', '2020-07-06 18:39:02'),
(2, '2bhk', '2BHK', 'sample', 1, 1, '2020-06-13 14:04:06', '2020-07-06 18:39:16'),
(3, '3bhk', '3BHK', 'sample', 1, 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(7, '7bhk', '7bhk', '', 1, 1, '2020-09-27 17:09:24', '2020-09-27 17:09:24');

-- --------------------------------------------------------

--
-- Table structure for table `flat_type_masterlist`
--

DROP TABLE IF EXISTS `flat_type_masterlist`;
CREATE TABLE IF NOT EXISTS `flat_type_masterlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flattype_id` int(11) NOT NULL,
  `room_type` varchar(250) NOT NULL,
  `sqft` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flat_type_masterlist`
--

INSERT INTO `flat_type_masterlist` (`id`, `flattype_id`, `room_type`, `sqft`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bed Room 1', '256', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(2, 1, 'Kitchen d', '100', 'sample', 1, '2020-06-13 14:03:37', '2020-06-13 14:03:37'),
(3, 2, 'Bed Room', '256', 'sample', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(4, 2, 'Kitchen', '100', 'sampl 1', 1, '2020-06-13 14:04:06', '2020-06-13 14:04:06'),
(5, 3, 'Bed Room', '123', 'sample', 1, '2020-06-13 14:04:21', '2020-06-13 14:04:21'),
(7, 2, 'kitcheneeee', '55', '666', 1, '2020-07-06 18:39:16', '2020-07-06 18:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `floor_tbl`
--

DROP TABLE IF EXISTS `floor_tbl`;
CREATE TABLE IF NOT EXISTS `floor_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `floor` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floor_tbl`
--

INSERT INTO `floor_tbl` (`id`, `token`, `floor`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(2, '1-floor', '1 Floor', 1, 1, '2020-05-28 16:42:59', '2020-07-08 00:10:15'),
(3, '2-floor', '2 Floor', 1, 1, '2020-06-03 09:07:41', '2020-06-03 09:07:41'),
(4, '3-floor', '3 Floor', 1, 1, '2020-06-13 17:29:44', '2020-07-08 00:10:32'),
(5, '4-floor', '4 Floor', 1, 1, '2020-06-16 23:23:02', '2020-07-08 00:10:39'),
(6, '5-floor', '5 floor', 1, 1, '2020-09-26 10:51:29', '2020-09-26 10:51:29'),
(7, '6floor', '6 floor', 1, 1, '2020-09-26 10:52:06', '2020-09-26 10:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image_tbl`
--

DROP TABLE IF EXISTS `gallery_image_tbl`;
CREATE TABLE IF NOT EXISTS `gallery_image_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` varchar(250) DEFAULT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_image_tbl`
--

INSERT INTO `gallery_image_tbl` (`id`, `gallery_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(3, '1', 'ydz0sHUKNl5boOZpP967uVWMvcfEa3.jpg', 1, '2020-06-18 11:54:17', '2020-06-18 11:54:17'),
(4, '1', 'DEXUHVW419TamP3wMCcn6gdRv2Q_zl.jpg', 1, '2020-06-18 11:54:17', '2020-06-18 11:54:17'),
(5, '2', 'jGCc5E9HgImkLMqJoi1DlhA73ryeWd.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(6, '2', 'NlU4q9bOpQB2PF0RfCcexSIt1YHGMK.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(7, '2', 'OZgbdswf4NPo1j5aYHuIEDrK0lnt3L.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24'),
(8, '2', 'GcqnorLmlPew92utC7XkF1j_OyJ3Vd.jpg', 1, '2020-06-18 11:54:24', '2020-06-18 11:54:24');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_tbl`
--

DROP TABLE IF EXISTS `gallery_tbl`;
CREATE TABLE IF NOT EXISTS `gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `album_title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_tbl`
--

INSERT INTO `gallery_tbl` (`id`, `token`, `album_title`, `description`, `image`, `sort_order`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'sample', 'Sample', 'sample', 'EIwq2zkBZH07072020113512pexels-photo-3785927.jpeg', 7, 1, 0, 0, '', 1, '2020-09-13 00:50:17', 'tyrty', 1, '2020-06-10 16:58:04', '2020-09-13 00:50:17'),
(2, 'sample.2', 'Sample 1', 'adada', 'DOFh1cdC3Y13062020053209whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 2, 1, 0, 0, '', 0, '', '', 1, '2020-06-13 17:32:09', '2020-06-13 17:32:09'),
(3, 'sample.3', 'Sample', '', '09MxPCblRz06072020010153whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 2, 1, 0, 0, '', 1, '2020-09-13 00:50:04', 'afaas', 1, '2020-07-06 13:01:53', '2020-09-13 00:50:04'),
(4, 'sample.4', 'Sample', 'dfgdfg', 'GSE2ZTgUpf13092020010701whatsapp-image-2020-09-04-at-4.53.40-pm.jpeg', 6, 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:07:01', '2020-09-13 01:07:01'),
(5, 'sample.5', 'Sample', '', 'pELZA5wFZp30092020045040loan.png', 10, 1, 0, 0, '', 0, '', '', 1, '2020-09-30 16:50:40', '2020-09-30 16:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `gendral_documents_tbl`
--

DROP TABLE IF EXISTS `gendral_documents_tbl`;
CREATE TABLE IF NOT EXISTS `gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gendral_documents_tbl`
--

INSERT INTO `gendral_documents_tbl` (`id`, `token`, `title`, `description`, `documents`, `document_type`, `added_by`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `created_at`, `updated_at`) VALUES
(1, 'zROQEkV0Jalp3rFbnyCmmrUzMFAp5w', 'sample test 1', 'sampleee', 'attachment_07072020011931_ugNXTB2NP2.jpeg', 'jpeg', 1, 1, 0, 0, '', 0, '', '', '2020-06-18 09:35:33', '2020-07-07 13:50:08'),
(2, 'v6VuH3VJHdd8ONOs8iwRQVOkrnMr4X', 'sample test', 'sasa', 'attachment_18062020111948_CBQEMMBYc9.pdf', 'pdf', 1, 1, 0, 0, '', 1, '2020-09-12 16:23:33', 'sample', '2020-06-18 11:19:48', '2020-09-12 16:23:33'),
(3, 'J86qUoJfvRkiJvYnRPXdOtBGpHNox0', 'new test', 'sampples', 'attachment_11092020111245_nxOTJJcLYt.pdf', 'pdf', 1, 1, 1, 1, '2020-09-12 15:45:44', 0, '', '', '2020-09-11 11:12:45', '2020-09-12 15:45:37'),
(4, '9TEB0uq49yCQHCGsfzi68aYdzon0hQ', '567', '76567', 'attachment_26092020123925_wLUEFjzIcm.xls', 'xls', 1, 1, 0, 0, '', 0, '', '', '2020-09-26 00:39:25', '2020-09-26 00:39:25');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_tbl`
--

DROP TABLE IF EXISTS `invoice_tbl`;
CREATE TABLE IF NOT EXISTS `invoice_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_uid` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `inv_number` varchar(255) DEFAULT NULL,
  `inv_date` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `raised_to` varchar(250) DEFAULT NULL,
  `documents` varchar(250) DEFAULT NULL,
  `document_type` varchar(250) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_tbl`
--

INSERT INTO `invoice_tbl` (`id`, `inv_uid`, `customer_id`, `inv_number`, `inv_date`, `type`, `raised_to`, `documents`, `document_type`, `remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'INV0001', 1, '0232', '1970-01-01', 'invoice', 'bank', 'attachment_30092020104541_Co9jYUQaMs.pdf', 'pdf', 'sample', 1, 0, 0, '', 0, '', '', 1, '2020-07-04 11:32:44', '2020-09-30 10:45:41'),
(2, 'INV0002', 4, '00111', '2020-08-23', 'invoice', 'bank', 'attachment_23082020101814_Rgk24ItSyP.pdf', 'pdf', 'sample', 1, 1, 1, '2020-09-13 00:43:49', 0, '', '', 1, '2020-08-23 22:18:14', '2020-09-13 00:37:28'),
(3, 'INV0003', 1, '444444444', '2020-09-14', 'invoice', 'bank', 'attachment_13092020010558_gc5iC80QyM.pdf', 'pdf', 'ghjghj', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:05:58', '2020-09-30 10:45:31'),
(4, 'INV0004', 1, '8799998', '2020-09-12', 'invoice', 'bank', 'attachment_13092020010621_ZhDnaghxQs.pdf', 'pdf', 'ghfgh', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:06:21', '2020-09-13 01:06:21');

-- --------------------------------------------------------

--
-- Table structure for table `kyc_list_tbl`
--

DROP TABLE IF EXISTS `kyc_list_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `kyctype_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `file_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kyc_list_tbl`
--

INSERT INTO `kyc_list_tbl` (`id`, `customer_id`, `kyctype_id`, `image_name`, `file_type`, `status`, `approval_status`, `created_at`, `updated_at`) VALUES
(6, 1, 1, '4uD7NEU2wOF0P9fWHJMVzgst6Spn8T.jpeg', 'jpeg', 1, 0, '2020-07-03 18:07:56', '2020-07-03 18:07:56'),
(10, 1, 2, 'mOeN5QXVBPct1CMfUy4SbzY3dnhZkG.pdf', 'pdf', 1, 0, '2020-07-06 17:28:06', '2020-07-06 17:28:06'),
(12, 1, 2, 'LtigKmZqvb0Gl4eYETrUsaNAf7k_IM.pdf', 'pdf', 1, 0, '2020-07-06 18:04:22', '2020-07-06 18:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `kyc_tbl`
--

DROP TABLE IF EXISTS `kyc_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kyc_uid` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `id_type` varchar(250) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `dob` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `address1` varchar(250) NOT NULL,
  `address2` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `nationiality` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `approval_status` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kyc_type_tbl`
--

DROP TABLE IF EXISTS `kyc_type_tbl`;
CREATE TABLE IF NOT EXISTS `kyc_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `kyc_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kyc_type_tbl`
--

INSERT INTO `kyc_type_tbl` (`id`, `token`, `kyc_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'aadhaar-card', 'Aadhaar Card', 0, 1, '2020-07-02 22:58:39', '2020-08-19 20:11:48'),
(2, 'pan-card', 'PAN Card', 1, 1, '2020-07-02 22:59:04', '2020-07-08 00:28:49'),
(3, 'ration-card', 'Ration Card', 1, 1, '2020-07-02 22:59:12', '2020-07-02 22:59:12'),
(4, 'election-commission-id-card', 'Election Commission ID Card', 1, 1, '2020-07-02 22:59:20', '2020-07-02 22:59:20'),
(5, 'driving-license', 'Driving License', 1, 1, '2020-07-02 22:59:27', '2020-07-02 22:59:27');

-- --------------------------------------------------------

--
-- Table structure for table `lead_activity_tbl`
--

DROP TABLE IF EXISTS `lead_activity_tbl`;
CREATE TABLE IF NOT EXISTS `lead_activity_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(250) NOT NULL,
  `lead_id` varchar(250) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lead_logs_tbl`
--

DROP TABLE IF EXISTS `lead_logs_tbl`;
CREATE TABLE IF NOT EXISTS `lead_logs_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `lead_type` varchar(255) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_to` int(11) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_logs_tbl`
--

INSERT INTO `lead_logs_tbl` (`id`, `type`, `lead_type`, `created_by`, `created_to`, `ref_id`, `remarks`, `status`, `created_at`, `updated_at`) VALUES
(1, 'created', 'manual', 1, 0, 3, '', 1, '2020-09-27 17:12:21', '2020-09-27 17:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `lead_source_tbl`
--

DROP TABLE IF EXISTS `lead_source_tbl`;
CREATE TABLE IF NOT EXISTS `lead_source_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `lead_source` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_source_tbl`
--

INSERT INTO `lead_source_tbl` (`id`, `token`, `lead_source`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'fb', 'fb', 1, 1, '2020-09-18 10:17:38', '2020-09-18 10:19:11'),
(2, 'google', 'Google', 1, 1, '2020-09-21 11:53:45', '2020-09-21 11:53:45'),
(3, 'instagram', 'Instagram', 1, 1, '2020-09-21 11:53:55', '2020-09-21 11:53:55'),
(4, 'fbs', 'fbs', 1, 1, '2020-09-22 11:42:37', '2020-09-22 11:42:37'),
(6, 'internet', 'internet', 1, 1, '2020-09-27 17:09:23', '2020-09-27 17:09:23'),
(7, 'facebook', 'Facebook', 1, 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47');

-- --------------------------------------------------------

--
-- Table structure for table `lead_tbl`
--

DROP TABLE IF EXISTS `lead_tbl`;
CREATE TABLE IF NOT EXISTS `lead_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `import_token` varchar(255) DEFAULT NULL,
  `uid` varchar(250) DEFAULT NULL,
  `auth_type` varchar(250) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `lead_date` varchar(250) DEFAULT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `leadsource` varchar(250) NOT NULL,
  `flattype_id` int(11) DEFAULT NULL,
  `site_visit_status` varchar(250) DEFAULT NULL,
  `customer_profile_id` int(11) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `secondary_mobile` varchar(255) DEFAULT NULL,
  `secondary_email` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `lead_status_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `assign_status` int(11) DEFAULT NULL,
  `closed_status` int(11) DEFAULT NULL,
  `moved_status` int(1) NOT NULL,
  `move_remarks` longtext DEFAULT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_tbl`
--

INSERT INTO `lead_tbl` (`id`, `token`, `import_token`, `uid`, `auth_type`, `property_id`, `customer_id`, `lead_date`, `fname`, `lname`, `leadsource`, `flattype_id`, `site_visit_status`, `customer_profile_id`, `gender`, `mobile`, `email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `pincode`, `description`, `lead_status_id`, `employee_id`, `assign_status`, `closed_status`, `moved_status`, `move_remarks`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'arunkumar', '4ulsFbuBeO17tp5', 'LE0000', 'excel', 0, 0, '2020-09-27', 'Arun', 'Kumar', '1', 1, 'yes', 4, 'male', '994238710000', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', '61104', 'sample 1', 2, 0, 0, 1, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:09:23', '2020-10-05 15:57:54'),
(2, 'vengetrajan', '4ulsFbuBeO17tp5', 'LE0001', 'excel', 0, 0, '2020-09-27', 'Venget', 'Rajan', '4', 3, 'no', 2, 'male', '994004123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', '614408', 'sample 2', 3, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:09:23', '2020-10-05 15:56:32'),
(3, 'test', NULL, 'LE0003', NULL, 0, 0, '2020-09-27', 'test', '', '6', 7, 'yes', 1, 'male', '9942009450', 'admin@postcrm.com', '', '', '', '', '', '', '', 2, 0, 0, 0, 0, NULL, 1, 0, 0, '', 0, '', '', 1, '2020-09-27 17:12:21', '2020-09-27 17:12:21'),
(4, 'swasthik', 'SyDxz7NhpBL0Y7L', 'LE0003', 'excel', 0, 0, '2020-10-01', 'Swasthik', '', '7', 2, 'Yes', 6, 'Female', '7373021332', '', '', '', '', 'Cbe', 'TamilNadu', '641027', 'Did not ans call', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:46', '2020-10-01 16:10:46'),
(5, 'duraiswamy', 'SyDxz7NhpBL0Y7L', 'LE0004', 'excel', 0, 0, '2020-10-01', 'Duraiswamy', '', '2', 3, 'No', 6, 'Male', '8825642733', '', '', '', '', 'Madurai', 'TamilNadu', '641028', 'Did not ans call', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(6, 'vetrivel', 'SyDxz7NhpBL0Y7L', 'LE0005', 'excel', 0, 0, '2020-10-01', 'Vetrivel', '', '7', 2, 'Yes', 6, 'Male', '9841051590', '', '', '', '', 'Chennai', 'TamilNadu', '641029', 'has in mind, will call back', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(7, 'ramoorthy', 'SyDxz7NhpBL0Y7L', 'LE0006', 'excel', 0, 0, '2020-10-01', 'Ramoorthy', '', '7', 2, 'No', 6, 'Male', '9737356233', '', '', '', '', 'Cbe', 'TamilNadu', '641030', 'thinking over multiple choices in chennai and cbe', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(8, 'akshay', 'SyDxz7NhpBL0Y7L', 'LE0007', 'excel', 0, 0, '2020-10-01', 'Akshay', '', '2', 3, 'Yes', 6, 'Male', '8610739811', '', '', '', '', 'Madurai', 'TamilNadu', '641031', 'Did not ans call', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(9, 'ramkumar', 'SyDxz7NhpBL0Y7L', 'LE0008', 'excel', 0, 0, '2020-10-01', 'Ramkumar', '', '2', 2, 'No', 6, 'Male', '9894013341', '', '', '', '', 'Chennai', 'TamilNadu', '641032', 'Room sizes are small, thinking off larger option', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(10, 'balasundaram', 'SyDxz7NhpBL0Y7L', 'LE0009', 'excel', 0, 0, '2020-10-01', 'Balasundaram', '', '7', 2, 'Yes', 6, 'Male', '9677622669', '', '', '', '', 'Cbe', 'TamilNadu', '641033', 'Wants L Type, Ready to buy with full payment', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(11, 'prabha', 'SyDxz7NhpBL0Y7L', 'LE0010', 'excel', 0, 0, '2020-10-01', 'Prabha', '', '7', 2, 'No', 7, 'Male', '6382735660', '', '', '', '', 'Madurai', 'TamilNadu', '641034', 'share details again, not replying', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:47', '2020-10-01 16:10:47'),
(12, 'raji', 'SyDxz7NhpBL0Y7L', 'LE0011', 'excel', 0, 0, '2020-10-01', 'Raji', '', '7', 2, 'Yes', 7, 'female', '9113282820', '', '', '', '', 'Chennai', 'TamilNadu', '641035', 'Did not ans call', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(13, 'antojeba', 'SyDxz7NhpBL0Y7L', 'LE0012', 'excel', 0, 0, '2020-10-01', 'Antojeba', '', '2', 2, 'No', 7, 'Male', '8098600910', '', '', '', '', 'Cbe', 'TamilNadu', '641036', 'His brother enquired it', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(14, 'murali', 'SyDxz7NhpBL0Y7L', 'LE0013', 'excel', 0, 0, '2020-10-01', 'Murali', '', '2', 2, 'Yes', 7, 'Male', '9486922011', '', '', '', '', 'Madurai', 'TamilNadu', '641037', 'Will visit again with family', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(15, 'naveen', 'SyDxz7NhpBL0Y7L', 'LE0014', 'excel', 0, 0, '2020-10-01', 'Naveen', '', '2', 2, 'No', 7, 'Male', '9894290431', '', '', '', '', 'Chennai', 'TamilNadu', '641038', 'Did not ans call, no response', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(16, 'gugan', 'SyDxz7NhpBL0Y7L', 'LE0015', 'excel', 0, 0, '2020-10-01', 'Gugan', '', '2', 2, 'Yes', 7, 'Male', '9980148996', '', '', '', '', 'Cbe', 'TamilNadu', '641039', 'not planning to buy now', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(17, 'keerthana', 'SyDxz7NhpBL0Y7L', 'LE0016', 'excel', 0, 0, '2020-10-01', 'Keerthana', '', '7', 2, 'No', 7, 'female', '9962009596', '', '', '', '', 'Madurai', 'TamilNadu', '641040', 'Did not ans call, no response', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(18, 'madhan', 'SyDxz7NhpBL0Y7L', 'LE0017', 'excel', 0, 0, '2020-10-01', 'Madhan', '', '7', 2, 'yes', 7, 'Male', '8122658371', '', '', '', '', 'Chennai', 'TamilNadu', '641041', 'out of their budget at present', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-01 16:10:48', '2020-10-01 16:10:48'),
(19, 'arunkumar', '72lovaei9GJZrm0', 'LE0019', 'excel', 0, 0, '2020-10-05', 'Arun', 'Kumar', '1', 1, 'yes', 4, 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', '61104', 'sample 1', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-05 16:04:44', '2020-10-05 16:04:44'),
(20, 'vengetrajan', '72lovaei9GJZrm0', 'LE0020', 'excel', 0, 0, '2020-10-05', 'Venget', 'Rajan', '6', 7, 'no', 5, 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', '614408', 'sample 2', 2, 0, 0, 0, 0, '', 1, 0, 0, '', 0, '', '', 1, '2020-10-05 16:04:44', '2020-10-05 16:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `lead_type_tbl`
--

DROP TABLE IF EXISTS `lead_type_tbl`;
CREATE TABLE IF NOT EXISTS `lead_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(250) DEFAULT NULL,
  `token` varchar(250) NOT NULL,
  `lead_type` varchar(250) NOT NULL,
  `default_settings` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lead_type_tbl`
--

INSERT INTO `lead_type_tbl` (`id`, `uid`, `token`, `lead_type`, `default_settings`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'LT0001', 'will-think-and-get-back', 'Will think and get back', 0, 1, 1, '2020-08-16 22:22:58', '2020-08-17 19:35:34'),
(2, 'LT0002', 'hot-lead', 'Hot Lead', 1, 1, 1, '2020-08-16 22:23:09', '2020-08-17 19:35:22'),
(3, 'LT0003', 'will-discuss-with-family', 'Will discuss with family', 0, 1, 1, '2020-08-16 22:26:03', '2020-08-17 19:35:41'),
(4, 'LT0004', 'just-for-collecting-info', 'Just for collecting info', 0, 1, 1, '2020-08-17 19:35:53', '2020-08-17 19:35:53'),
(5, 'LT0005', 'do-not-follow-up', 'Do not follow up', 0, 1, 1, '2020-08-17 19:35:59', '2020-08-17 19:35:59'),
(7, 'LT0007', 'ghjghjghjgh', 'ghjghjghjgh', 0, 1, 1, '2020-09-20 22:00:01', '2020-09-20 22:00:28'),
(8, 'LT0008', 'jjj', 'jjj', 0, 1, 1, '2020-09-20 22:00:32', '2020-09-20 22:00:32');

-- --------------------------------------------------------

--
-- Table structure for table `news_tbl`
--

DROP TABLE IF EXISTS `news_tbl`;
CREATE TABLE IF NOT EXISTS `news_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `email_notification` int(11) NOT NULL,
  `post_to` varchar(250) NOT NULL,
  `customer_ids` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `show_popup` int(11) NOT NULL,
  `popup_till` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `send_mail_status` int(11) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_tbl`
--

INSERT INTO `news_tbl` (`id`, `token`, `title`, `date`, `description`, `email_notification`, `post_to`, `customer_ids`, `image`, `show_popup`, `popup_till`, `status`, `send_mail_status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'nirala-appartment.1', 'Nirala Appartment', '2020-09-21', 'dasdasd', 0, 'selected', '1', '', 0, '', 1, 1, 1, 1, '2020-09-25 12:07:20', 0, '', '', 1, '2020-09-21 17:03:53', '2020-09-25 11:54:11'),
(2, 'nirala-appartment', 'Nirala Appartment', '2020-09-24', '<p>The Department of Mechanical Engineering, SNSCE organized the Alumni \r\nGuest lecture on 11.03.2020 in the topic Opportunities for Design \r\nEngineers in the field of Mechanical Engineering\" for the third-year \r\nstudents. Our esteemed Alumni Mr.B.Jagadeeshwaran, Design Engineer, \r\nEngineer Design and Development, M/s Enpro Engineering, Ambattur \r\nIndustrial Estate, Chennai addressed the students. He shared his \r\nexpertise in the area of Designing and Drafting.</p>', 0, 'selected', '1', 'AihHUczs5D30092020083929screenshot2020-07-18-sreevatsa-google-drive2.png', 1, '2020-10-01', 1, 0, 0, 0, '', 1, '2020-09-30 08:47:13', 'a', 1, '2020-09-24 18:39:36', '2020-09-30 09:34:51'),
(3, 'sample-test.3', 'sample test', '2020-09-24', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industryâ€™s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br><span style=\"color: rgb(29, 28, 29); font-family: Slack-Lato, appleLogo, sans-serif; font-size: 15px; font-style: normal; font-variant-ligatures: common-ligatures; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(248, 248, 248); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;\"></span></p>', 0, 'all', '', '', 0, '', 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-24 19:02:32', '2020-09-30 16:24:35'),
(4, 'sample-test', 'sample test', '2020-09-30', '<p>sadasdasasd<br></p>', 0, 'all', '', '', 0, '', 1, 0, 0, 0, '', 0, '', '', 1, '2020-09-30 16:36:05', '2020-09-30 16:36:05');

-- --------------------------------------------------------

--
-- Table structure for table `notification_email_tbl`
--

DROP TABLE IF EXISTS `notification_email_tbl`;
CREATE TABLE IF NOT EXISTS `notification_email_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `adhoc_permission` int(1) NOT NULL,
  `kyc_permission` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_email_tbl`
--

INSERT INTO `notification_email_tbl` (`id`, `token`, `email`, `adhoc_permission`, `kyc_permission`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'adminposyutyutytcrm.com', 'admin@posyutyutytcrm.com', 1, 1, 1, 1, '2020-06-10 11:55:26', '2020-08-05 20:10:36'),
(2, 'koshikwasasebykart.com', 'koshik@wasasebykart.com', 1, 0, 1, 1, '2020-07-06 13:05:49', '2020-07-08 00:50:38'),
(3, 'adminpostcrm.com', 'admin@postcrm.com', 0, 1, 1, 1, '2020-07-06 14:43:16', '2020-07-08 00:51:18'),
(4, 'koshikwebykart.com', 'koshik@webykart.com', 1, 1, 1, 1, '2020-07-06 17:06:45', '2020-07-06 17:06:45'),
(5, 'koshikwebykart.com', 'koshik@webykart.com', 0, 1, 1, 1, '2020-07-06 17:06:53', '2020-07-06 17:06:53'),
(6, 'koshikwebyukart.com', 'koshik@webyukart.com', 1, 1, 1, 1, '2020-07-08 00:45:48', '2020-07-08 00:51:25');

-- --------------------------------------------------------

--
-- Table structure for table `paymentinfo_tbl`
--

DROP TABLE IF EXISTS `paymentinfo_tbl`;
CREATE TABLE IF NOT EXISTS `paymentinfo_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `account_number` varchar(250) NOT NULL,
  `bank` varchar(250) NOT NULL,
  `ifcs_code` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paymentinfo_tbl`
--

INSERT INTO `paymentinfo_tbl` (`id`, `name`, `account_number`, `bank`, `ifcs_code`, `branch`, `city`, `state`, `pincode`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'sample', '1245', 'AXIS', 'AX002', 'CIMBATORE', 'COIMBATIORE', 'tamilnadu', 641103, 1, 1, '2020-06-06 12:06:56', '2020-07-15 16:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `project_type_tbl`
--

DROP TABLE IF EXISTS `project_type_tbl`;
CREATE TABLE IF NOT EXISTS `project_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `project_name` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_type_tbl`
--

INSERT INTO `project_type_tbl` (`id`, `token`, `project_name`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'royal-palm', 'Royal Palm', 1, 1, '2020-08-19 08:33:01', '2020-08-19 09:07:34');

-- --------------------------------------------------------

--
-- Table structure for table `property_area_tbl`
--

DROP TABLE IF EXISTS `property_area_tbl`;
CREATE TABLE IF NOT EXISTS `property_area_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `carpet_area` varchar(250) NOT NULL,
  `total_area` varchar(250) NOT NULL,
  `usable_area` varchar(250) NOT NULL,
  `common_area` varchar(250) NOT NULL,
  `uds` varchar(250) NOT NULL,
  `uds_perc` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_area_tbl`
--

INSERT INTO `property_area_tbl` (`id`, `token`, `title`, `carpet_area`, `total_area`, `usable_area`, `common_area`, `uds`, `uds_perc`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'testsmaplearea', 'test smaple area', '1230', '1454 sqft', '10', '13', '897', '31', 1, 1, '2020-10-12 14:49:14', '2020-10-12 15:48:35'),
(2, 'test1', 'test 1', '456', '456', '456', '456', '456', '546', 1, 1, '2020-10-13 10:29:21', '2020-10-13 10:29:21');

-- --------------------------------------------------------

--
-- Table structure for table `property_broucher_tbl`
--

DROP TABLE IF EXISTS `property_broucher_tbl`;
CREATE TABLE IF NOT EXISTS `property_broucher_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `property_id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `brochures` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_broucher_tbl`
--

INSERT INTO `property_broucher_tbl` (`id`, `token`, `title`, `property_id`, `description`, `brochures`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '2TFkc4FNM14oqCMCuhhcnkZctnf6Du', 'Brochure 1', 5, 'You can download all the application that available in your plan.', 'attachment_08072020013117_CJRAIVTwHm.jpg', 1, 0, 0, '', 0, '', '', 1, '2020-06-17 20:58:36', '2020-09-29 12:38:07'),
(2, 'Q9KIAS9yh1Nr3bfYk4fiN6AHInvO0p', 'new test yrty222', 5, 'ryrtyrtyrty222', 'attachment_08072020013442_gHqriSeync.jpg', 1, 0, 0, '', 0, '', '', 1, '2020-07-06 13:03:13', '2020-09-29 12:38:12'),
(3, 'tDoJdkdHsqTlADSHesWHQqSgNYhVMl', 'Nirala Appartment', 2, '', 'attachment_06072020010334_MjkeKpHX70.pdf', 1, 0, 0, '', 1, '2020-09-13 01:04:39', 'HFGHFGH', 1, '2020-07-06 13:03:34', '2020-09-13 01:04:39'),
(4, 'GW13yLUVvCRjuSmdpsENyL1dkLPRIZ', 'fsdf', 5, 'dfsdf', 'attachment_13092020010734_IgJZoZqXcz.pdf', 1, 0, 0, '', 0, '', '', 1, '2020-09-13 01:07:34', '2020-09-29 12:38:16'),
(5, 'XXKYOZIaC2lGWoQrM9JDcxbbf3sje1', 'Nirala Appartment', 1, '', 'attachment_22092020121117_9HdSa74OCg.pdf', 1, 0, 0, '', 0, '', '', 1, '2020-09-22 12:11:17', '2020-09-22 12:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_items`
--

DROP TABLE IF EXISTS `property_document_list_items`;
CREATE TABLE IF NOT EXISTS `property_document_list_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) NOT NULL,
  `document_list_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `doc_name` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `document_file` varchar(255) NOT NULL,
  `doc_type` varchar(250) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `doc_status` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_document_list_items`
--

INSERT INTO `property_document_list_items` (`id`, `document_id`, `document_list_id`, `property_id`, `doc_name`, `description`, `document_file`, `doc_type`, `status`, `doc_status`, `added_by`, `created_at`, `updated_at`) VALUES
(17, 1, 1, 1, 'EB Form', 'test', 'UZrLDwsSET28092020091658sample.pdf', 'pdf', 1, 1, 1, '2020-07-07 21:40:02', '2020-09-28 21:19:23'),
(18, 1, 2, 1, 'EB Bill', 'test', '', '', 1, 0, 1, '2020-07-07 21:40:02', '2020-09-25 10:34:29'),
(19, 1, 1, 5, 'EB Form', 'test', '', '', 1, 1, 1, '2020-07-17 00:24:08', '2020-09-26 01:58:45'),
(20, 1, 2, 5, 'EB Bill', 'test', '', '', 1, 1, 1, '2020-07-17 00:24:08', '2020-09-26 01:59:37'),
(21, 2, 3, 5, 'Building  Ouside ', 'Building Ouside View ', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(22, 2, 4, 5, 'Building inner View', 'Building inner View', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(23, 5, 10, 5, 'Building Ouside View', 'eee', '', NULL, 1, 0, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(24, 1, 1, 10, 'EB Form', 'test', '', NULL, 1, 0, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32'),
(25, 1, 2, 10, 'EB Bill', 'test', '', NULL, 1, 0, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32');

-- --------------------------------------------------------

--
-- Table structure for table `property_document_list_tbl`
--

DROP TABLE IF EXISTS `property_document_list_tbl`;
CREATE TABLE IF NOT EXISTS `property_document_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `documents_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_document_list_tbl`
--

INSERT INTO `property_document_list_tbl` (`id`, `property_id`, `documents_id`, `type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(10, 1, 1, 'common', 1, 1, '2020-07-07 21:40:02', '2020-07-07 21:40:02'),
(11, 5, 1, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(12, 5, 2, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(13, 5, 5, 'common', 1, 1, '2020-07-17 00:24:08', '2020-07-17 00:24:08'),
(14, 10, 1, 'common', 1, 1, '2020-10-13 11:09:32', '2020-10-13 11:09:32');

-- --------------------------------------------------------

--
-- Table structure for table `property_gallery_tbl`
--

DROP TABLE IF EXISTS `property_gallery_tbl`;
CREATE TABLE IF NOT EXISTS `property_gallery_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_gallery_tbl`
--

INSERT INTO `property_gallery_tbl` (`id`, `property_id`, `images`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 5, 'lVBiqJoTrwStRDZ0yUxeG2F1dn95hI.jpeg', 1, 1, '2020-07-17 09:48:05', '2020-07-17 09:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `property_gendral_documents_tbl`
--

DROP TABLE IF EXISTS `property_gendral_documents_tbl`;
CREATE TABLE IF NOT EXISTS `property_gendral_documents_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) DEFAULT NULL,
  `property_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `documents` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_gendral_documents_tbl`
--

INSERT INTO `property_gendral_documents_tbl` (`id`, `token`, `property_id`, `title`, `description`, `documents`, `document_type`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'fSwPEal3CLVVJvj8Cjut94JIyhRlVV', 1, 'Nirala Appartment', 'FGHFHFGH', '', 'pdf', 1, 1, '2020-06-16 23:35:09', '2020-09-29 00:55:40'),
(2, '2g1uY36GbFiHJOTk2BrF24NGDfs0HO', 1, 'sample test', '', '', '', 1, 1, '2020-06-16 23:37:18', '2020-09-29 00:50:59'),
(3, 'lPP546Bmrs3ekNzEvvDYB9SlkfKyJd', 1, 'owner', 'test', '', 'jpg', 1, 1, '2020-06-29 11:27:50', '2020-09-29 00:55:46'),
(4, 'hnAw2pvMtSSsUL2wjePDHV595h64Xl', 1, 'Nirala Appartment', 'asdasd', '01IapURg6025092020103523image-1.png', 'png', 1, 1, '2020-09-24 19:37:06', '2020-09-25 10:35:23'),
(5, 'plBVBGQ4VjCYbxuxWm3NhWrnGaK5XO', 1, 'saS', 'ASA', 'W0mOgh0OH329092020010110availability.pdf', 'pdf', 1, 1, '2020-09-24 19:38:50', '2020-09-29 01:01:14'),
(6, 't7IpGGnMNlGn4wv5wI3V44IC79j5SE', 1, 'wwwwwwwwwwww', 'wwwwwwww', '', '', 1, 1, '2020-09-24 19:38:58', '2020-09-29 00:57:44'),
(7, 'FNqJmKmOYgX5Z9nelbafzaDF0YVnM1', 1, 'yu', 'iyuiyui', '', 'pdf', 1, 1, '2020-09-25 10:32:45', '2020-09-29 00:57:12'),
(8, 'U3w1qEIAjUeXGgsPxyb8nlhVdlfuqE', 1, 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'yyyyyyyyyy', '', '', 1, 1, '2020-09-25 10:32:54', '2020-09-28 09:37:45'),
(9, 'HVqQ1L7uG5gUg6uXbBkPLm2XMwpgPK', 1, 'qqqqqqqqqqqqqqqqq', 'qqqqqqqqqqqq', '', '', 1, 1, '2020-09-25 10:33:19', '2020-09-28 09:37:27'),
(10, 'DMAML629ugaLDgHzqeoLpl9gk6A8IL', 1, 'aaaaaaaaaaa', 'aaaaaaaaaaa', '', '', 1, 1, '2020-09-25 10:34:43', '2020-09-28 09:26:56'),
(11, '7lyw3bnNmbxY1Sd40SmOTGxqrWyriv', 1, 'sdfs', 'fsdfsd', '', '', 1, 1, '2020-09-25 10:43:37', '2020-09-25 10:43:37'),
(12, 'GuF6MHmE5tNx3Puq49bD3l7u0bf19z', 5, 'sample', 'sa', '7hnDhBghoE26092020022039fileexamplexls10.xls', 'xls', 1, 1, '2020-09-25 10:46:50', '2020-09-26 02:20:39'),
(13, 'Uo4e5zKCV8WOut4keYBtU10LIRbgJ5', 1, 'sdf', 'sfsdf', '', '', 1, 1, '2020-09-25 10:49:33', '2020-09-25 10:49:33'),
(14, 'NwnLRlnzmR7ScgnzH8ZLv0cR6kutSe', 1, 'sdf', 'sfsdf', '', '', 1, 1, '2020-09-25 10:49:40', '2020-09-25 10:49:40'),
(15, '1fyizjEqPt2UKCKBTimclUoCaRi40E', 1, 'Nirala Appartment', 'tyutu', 'attachment_26092020123830_y7eQGziY4B.docx', 'docx', 1, 1, '2020-09-26 00:38:30', '2020-09-26 00:38:30'),
(16, 'jHEVRLK1IMczhGHEgJey9diFWI09A2', 5, 'Stop mode', 's', '', '', 1, 1, '2020-09-26 01:16:42', '2020-09-26 02:18:49'),
(17, 'O3ywyvWaV18hRlzSkvV2s2ZSw6JxE2', 5, 'ty', 'u', '', '', 1, 1, '2020-09-26 01:17:32', '2020-09-26 02:20:31'),
(18, 'hk0DORGEmqoKeTit6OHmf7Nqrjw9Pr', 1, 'Nirala Appartment', '', '', '', 1, 1, '2020-09-27 17:26:51', '2020-09-27 17:26:51');

-- --------------------------------------------------------

--
-- Table structure for table `property_rooms_list_tbl`
--

DROP TABLE IF EXISTS `property_rooms_list_tbl`;
CREATE TABLE IF NOT EXISTS `property_rooms_list_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `roomlist_id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_rooms_list_tbl`
--

INSERT INTO `property_rooms_list_tbl` (`id`, `property_id`, `roomlist_id`, `image`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(3, 1, 2, 'mv8LG_Hxwzja4kCV1XZWlpOQfMiPSh.jpg', 1, 1, '2020-06-18 12:19:56', '2020-06-18 12:19:56'),
(4, 1, 2, 'GTE3sJU9au7lrFcV8WBXCdZQLY0i5h.jpg', 1, 1, '2020-06-18 12:19:56', '2020-06-18 12:19:56'),
(5, 1, 1, 'wHysBS79_CA43MoEmWIpNPQDvajF2X.jpeg', 1, 1, '2020-06-18 17:51:31', '2020-06-18 17:51:31'),
(6, 1, 1, 'QITkYrEsSfWqMBOjJalR124nzgN7cK.jpeg', 1, 1, '2020-06-18 17:51:31', '2020-06-18 17:51:31'),
(8, 5, 1, 'kIbdQJBhCwy5ASuOZz_fsoWgtq148a.jpeg', 1, 1, '2020-09-26 01:20:06', '2020-09-26 01:20:06'),
(9, 1, 1, '_sZY5FWGTn2gz7rRykDcx0fqM4Uw1O.jpg', 1, 1, '2020-09-27 17:26:31', '2020-09-27 17:26:31');

-- --------------------------------------------------------

--
-- Table structure for table `property_tbl`
--

DROP TABLE IF EXISTS `property_tbl`;
CREATE TABLE IF NOT EXISTS `property_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `allotment_date` varchar(250) NOT NULL,
  `assign_remarks` longtext NOT NULL,
  `token` varchar(250) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `house_facing` varchar(250) NOT NULL,
  `projectsize` varchar(250) NOT NULL,
  `projectarea` varchar(250) NOT NULL,
  `cost` varchar(250) NOT NULL,
  `room_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `area_type_id` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pincode` varchar(250) NOT NULL,
  `content` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `assign_status` int(11) NOT NULL,
  `cancel_status` int(11) NOT NULL,
  `reason_cancellation` varchar(250) NOT NULL,
  `cancel_remarks` longtext NOT NULL,
  `cancel_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `deleted_status` int(11) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_on` varchar(250) NOT NULL,
  `restored_by` int(11) NOT NULL,
  `restored_on` varchar(250) NOT NULL,
  `restored_remarks` longtext NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `property_tbl`
--

INSERT INTO `property_tbl` (`id`, `customer_id`, `allotment_date`, `assign_remarks`, `token`, `title`, `type`, `house_facing`, `projectsize`, `projectarea`, `cost`, `room_id`, `block_id`, `floor_id`, `area_type_id`, `address`, `city`, `state`, `pincode`, `content`, `image`, `assign_status`, `cancel_status`, `reason_cancellation`, `cancel_remarks`, `cancel_by`, `status`, `deleted_status`, `deleted_by`, `deleted_on`, `restored_by`, `restored_on`, `restored_remarks`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 2, '', '', 'nirala-appartment', 'Flat-1', 'flat', 'east', '123', '2 acr', '1400000', 1, 1, 2, 0, '99A vyasuva building', 'coimbatore', 'tamil nadu', '641108', 'sa', 'Y4pQTeZuZq07072020085135converting-auto-cad-2d-files-to-3d-cad-modeling-files-500x500.jpg', 1, 1, 'test', 'test', 1, 1, 0, 0, '', 1, '2020-09-24 12:01:30', 'sample', 1, '2020-06-16 10:42:01', '2020-10-16 11:06:18'),
(2, 4, '', '', 'nirala-appartment.2', 'Flat-2', 'flat', 'north', '123', '2 acr', '50000', 1, 1, 2, 0, '99A vyasuva building', 'COIMBATIORE', 'tamil nadu', '641108', '', '7pW5B7fbda22062020081135whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 1, 1, '2020-10-16 11:10:43', 0, '', '', 1, '2020-06-22 20:11:35', '2020-09-24 12:01:34'),
(5, 1, '', '', 'a1', 'Flat-1aa', 'flat', 'east', '122', '2 acr', '11313', 1, 2, 3, 0, '', '', '', '', '', '7zow2e1ovy17072020122408whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 0, 0, '', 1, '2020-09-29 12:32:43', 's', 1, '2020-07-17 00:24:08', '2020-09-29 12:37:57'),
(6, 1, '', '', 'a4', 'Flat-4', 'flat', 'east', '1234', '2 acr', '65464', 2, 1, 2, 0, '', '', '', '', '', 'sDlRnfcdHt17072020110021whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 0, 0, '', 1, '2020-09-24 12:01:43', 'asdasd', 1, '2020-07-17 11:00:21', '2020-10-08 13:49:48'),
(7, 1, '', '', 'a5', 'Flat-5', 'flat', 'east', '123', '2 acr', '666546', 2, 1, 2, 0, '', '', '', '', '', 'gLPVRsdM3z17072020110047whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2020-07-17 11:00:47', '2020-10-08 13:50:01'),
(8, 1, '2020-10-08', 'adasasd', 'nirala-appartment-fsdfs', 'Flat-6', 'flat', 'east', 'sdfsdf', 'sdfsd', '654', 1, 1, 2, 0, '', '', '', '', '', '4YIjV4p8aF22072020080319whatsapp-image-2020-06-03-at-2.48.17-pm.jpeg', 1, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2020-07-22 20:03:19', '2020-10-08 14:03:58'),
(10, 0, '', '', 'flat-1020', 'flat-1020', 'flat', 'east', '123', '2 acr', '44646466', 1, 4, 2, 2, '', '', '', '', '', 'U8jFLBDXIF13102020110932screenshot2020-07-18-sreevatsa-google-drive2.png', 0, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2020-10-13 11:09:32', '2020-10-13 11:17:32'),
(11, 0, '', '', 'flat-1', 'Flat-1', 'flat', 'east', '123', '2 acr', '1400000', 1, 1, 2, 0, '99A vyasuva building', 'coimbatore', 'tamil nadu', '641108', 'sa', 'Y4pQTeZuZq07072020085135converting-auto-cad-2d-files-to-3d-cad-modeling-files-500x500.jpg', 0, 0, '', '', 0, 1, 0, 0, '', 0, '', '', 1, '2020-10-16 11:06:18', '2020-10-16 11:06:18');

-- --------------------------------------------------------

--
-- Table structure for table `request_type_tbl`
--

DROP TABLE IF EXISTS `request_type_tbl`;
CREATE TABLE IF NOT EXISTS `request_type_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `request_type` varchar(250) NOT NULL,
  `department_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_type_tbl`
--

INSERT INTO `request_type_tbl` (`id`, `token`, `request_type`, `department_id`, `status`, `added_by`, `created_at`, `updated_at`) VALUES
(1, 'request1', 'request 1', 1, 1, 1, '2020-06-10 11:34:39', '2020-10-23 09:56:15'),
(2, 'request2', 'Request 2', 2, 1, 1, '2020-06-19 11:21:25', '2020-10-23 09:56:22'),
(3, 'request3', 'Request 3', 2, 1, 1, '2020-06-19 11:21:33', '2020-10-23 09:56:27'),
(4, 'request4', 'Request 4', 1, 1, 1, '2020-10-23 09:42:20', '2020-10-23 09:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `session_tbl`
--

DROP TABLE IF EXISTS `session_tbl`;
CREATE TABLE IF NOT EXISTS `session_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logged_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `portal_type` varchar(100) NOT NULL,
  `auth_referer` varchar(50) NOT NULL,
  `auth_medium` varchar(50) NOT NULL,
  `auth_user_agent` text NOT NULL,
  `auth_ip_address` varchar(250) NOT NULL,
  `session_in` datetime NOT NULL,
  `session_out` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=296 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `session_tbl`
--

INSERT INTO `session_tbl` (`id`, `logged_id`, `user_type`, `portal_type`, `auth_referer`, `auth_medium`, `auth_user_agent`, `auth_ip_address`, `session_in`, `session_out`) VALUES
(75, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 18:56:34', NULL),
(76, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 19:07:56', NULL),
(77, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-26 19:09:23', NULL),
(78, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-27 15:58:14', NULL),
(79, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-27 15:58:29', NULL),
(80, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-28 06:31:35', NULL),
(81, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-05-28 06:31:36', NULL),
(82, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '::1', '2020-06-01 12:38:22', NULL),
(83, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '::1', '2020-06-01 12:38:23', NULL),
(84, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-06-02 09:49:55', NULL),
(85, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0', '127.0.0.1', '2020-06-02 09:49:57', NULL),
(86, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-03 08:06:54', NULL),
(87, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-03 08:06:55', NULL),
(88, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-04 13:41:15', NULL),
(89, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-04 13:41:15', NULL),
(90, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-05 11:10:46', NULL),
(91, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-05 11:10:48', NULL),
(92, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-06 07:57:00', NULL),
(93, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-08 10:03:37', NULL),
(94, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-08 10:03:38', NULL),
(95, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-09 21:16:28', NULL),
(96, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-10 11:22:05', NULL),
(97, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-10 11:22:07', NULL),
(98, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 10:08:20', NULL),
(99, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 10:08:22', NULL),
(100, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 12:15:14', NULL),
(101, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-13 19:03:22', NULL),
(102, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-15 10:05:08', NULL),
(103, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-16 09:48:47', NULL),
(104, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-17 09:36:11', NULL),
(105, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 11:24:11', NULL),
(106, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 19:39:46', NULL),
(107, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-17 20:48:54', NULL),
(108, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:32:36', NULL),
(109, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:35:13', NULL),
(110, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-06-18 09:35:15', NULL),
(111, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-19 10:02:53', NULL),
(112, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-19 11:21:11', NULL),
(113, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-20 10:03:18', NULL),
(114, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-20 11:24:36', NULL),
(115, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-22 08:58:22', NULL),
(116, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-22 08:59:04', NULL),
(117, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-25 15:51:01', NULL),
(118, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-26 10:37:36', NULL),
(119, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-27 10:06:09', NULL),
(120, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:42:55', NULL),
(121, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:57:57', NULL),
(122, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-29 10:57:58', NULL),
(123, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-30 09:02:53', NULL),
(124, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-06-30 09:07:32', NULL),
(125, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '127.0.0.1', '2020-07-01 19:39:52', NULL),
(126, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-02 13:40:49', NULL),
(127, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-02 19:03:59', NULL),
(128, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 10:18:26', NULL),
(129, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 10:18:27', NULL),
(130, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 11:53:00', NULL),
(131, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 11:53:01', NULL),
(132, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 14:08:35', NULL),
(133, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-03 14:08:57', NULL),
(134, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 09:20:44', NULL),
(135, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 09:20:45', NULL),
(136, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0', '::1', '2020-07-04 11:42:57', NULL),
(137, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-06 10:18:18', NULL),
(138, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-06 10:18:47', NULL),
(139, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-07 10:08:53', NULL),
(140, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-07 11:03:43', NULL),
(141, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 15:31:56', NULL),
(142, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:40:11', NULL),
(143, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:40:12', NULL),
(144, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-08 22:42:47', NULL),
(145, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-09 23:05:57', NULL),
(146, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-09 23:05:58', NULL),
(147, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-13 11:55:29', NULL),
(148, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 10:12:03', NULL),
(149, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 10:12:04', NULL),
(150, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-14 19:41:20', NULL),
(151, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:39:33', NULL),
(152, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:56:36', NULL),
(153, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '127.0.0.1', '2020-07-15 11:56:40', NULL),
(154, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-16 10:05:29', NULL),
(155, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-16 21:29:03', NULL),
(156, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-17 09:47:41', NULL),
(157, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-18 15:07:56', NULL),
(158, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-22 17:55:58', NULL),
(159, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-24 20:45:18', NULL),
(160, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-25 11:10:29', NULL),
(161, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-25 12:39:10', NULL),
(162, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:09', NULL),
(163, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:28', NULL),
(164, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', '::1', '2020-07-27 10:02:29', NULL),
(165, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', '::1', '2020-07-28 12:42:00', NULL),
(166, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 13:40:14', NULL),
(167, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:14:38', NULL),
(168, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:15:57', NULL),
(169, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-01 20:30:05', NULL),
(170, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-03 09:05:05', NULL),
(171, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 15:02:33', NULL),
(172, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:15:37', NULL),
(173, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:01', NULL),
(174, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:40', NULL),
(175, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-04 21:16:56', NULL),
(176, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 10:59:58', NULL),
(177, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:11:20', NULL),
(178, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:12:06', NULL),
(179, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-05 20:12:15', NULL),
(180, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-06 16:26:00', NULL),
(181, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-06 16:26:01', NULL),
(182, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-08 12:44:26', NULL),
(183, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-08 12:44:36', NULL),
(184, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-12 10:34:54', NULL),
(185, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-13 09:46:19', NULL),
(186, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-14 10:43:11', NULL),
(187, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-14 10:43:12', NULL),
(188, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-16 20:59:32', NULL),
(189, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-17 18:35:14', NULL),
(190, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-18 07:33:14', NULL),
(191, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-18 09:58:50', NULL),
(192, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-19 08:24:21', NULL),
(193, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-20 16:14:06', NULL),
(194, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-22 10:31:20', NULL),
(195, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-22 10:31:21', NULL),
(196, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-23 18:11:00', NULL),
(197, 4, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-23 22:17:22', NULL),
(198, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '::1', '2020-08-27 09:27:36', NULL),
(199, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0', '127.0.0.1', '2020-08-28 10:48:44', NULL),
(200, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-01 10:11:21', NULL),
(201, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-02 10:24:30', NULL),
(202, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-02 10:24:33', NULL),
(203, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-05 11:59:05', NULL),
(204, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-05 11:59:06', NULL),
(205, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-07 23:52:04', NULL),
(206, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-07 23:52:05', NULL),
(207, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-08 11:29:02', NULL),
(208, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-10 12:05:01', NULL),
(209, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-10 22:07:55', NULL),
(210, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-10 22:53:49', NULL),
(211, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-11 10:12:41', NULL),
(212, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-12 11:24:15', NULL),
(213, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-14 10:43:03', NULL),
(214, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-14 10:43:04', NULL),
(215, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-15 10:50:15', NULL),
(216, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-15 10:50:16', NULL),
(217, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 10:45:10', NULL),
(218, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 12:29:33', NULL),
(219, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-17 12:30:15', NULL),
(220, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-18 09:45:16', NULL),
(221, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-20 21:59:46', NULL),
(222, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-21 08:59:02', NULL),
(223, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-22 09:27:48', NULL),
(224, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-22 11:55:06', NULL),
(225, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-23 11:28:52', NULL),
(226, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-23 12:03:34', NULL),
(227, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-24 09:42:17', NULL),
(228, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-24 09:42:19', NULL),
(229, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-25 10:32:36', NULL),
(230, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '127.0.0.1', '2020-09-25 11:44:25', NULL),
(231, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0', '::1', '2020-09-25 23:14:08', NULL),
(232, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-26 10:22:08', NULL),
(233, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-28 09:02:11', NULL),
(234, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 10:32:08', NULL),
(235, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:03:21', NULL),
(236, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:34:34', NULL),
(237, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:35:48', NULL),
(238, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:37:08', NULL),
(239, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 12:37:34', NULL),
(240, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 15:25:21', NULL),
(241, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-29 15:28:24', NULL),
(242, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-30 08:31:43', NULL),
(243, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-09-30 08:36:44', NULL),
(244, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-01 09:58:26', NULL),
(245, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-01 09:58:31', NULL),
(246, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-02 12:26:29', NULL),
(247, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 12:37:58', NULL),
(248, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 13:26:32', NULL),
(249, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-05 13:26:32', NULL),
(250, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-06 09:11:17', NULL),
(251, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-08 10:25:23', NULL),
(252, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-08 11:08:04', NULL),
(253, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:08:20', NULL),
(254, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:46:40', NULL),
(255, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 00:54:22', NULL),
(256, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:56:19', NULL),
(257, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:56:36', NULL),
(258, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 00:57:15', NULL),
(259, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 10:11:26', NULL),
(260, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:08:22', NULL),
(261, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:10:31', NULL),
(262, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '127.0.0.1', '2020-10-09 19:10:55', NULL),
(263, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:13:01', NULL),
(264, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:17:26', NULL),
(265, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:17:47', NULL),
(266, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-09 19:51:44', NULL),
(267, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-10 11:06:01', NULL),
(268, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:51', NULL),
(269, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:56', NULL),
(270, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-12 11:01:57', NULL),
(271, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-13 10:18:46', NULL),
(272, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 10:34:28', NULL),
(273, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:40:11', NULL),
(274, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:44:16', NULL),
(275, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 11:44:55', NULL),
(276, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:09:52', NULL),
(277, 1, 'user', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:09:53', NULL),
(278, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0', '::1', '2020-10-16 12:28:42', NULL),
(279, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 09:06:59', NULL),
(280, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:07:28', NULL),
(281, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:07:38', NULL),
(282, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:01', NULL),
(283, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:12', NULL),
(284, 2, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 10:15:45', NULL),
(285, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-23 11:50:53', NULL),
(286, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-24 09:34:03', NULL),
(287, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-10-27 10:22:14', NULL),
(288, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-10-28 13:36:40', NULL),
(289, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-10-28 13:36:42', NULL),
(290, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-03 11:00:54', NULL),
(291, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-03 11:00:56', NULL),
(292, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-04 11:01:55', NULL),
(293, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-09 11:18:30', NULL),
(294, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '127.0.0.1', '2020-11-12 23:20:50', NULL),
(295, 1, 'admin', 'Post CRM', 'web', 'browser', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0', '::1', '2020-11-13 12:10:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `temp_customer_excel_tbl`
--

DROP TABLE IF EXISTS `temp_customer_excel_tbl`;
CREATE TABLE IF NOT EXISTS `temp_customer_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_token` varchar(50) NOT NULL,
  `name` varchar(250) NOT NULL,
  `addresss` varchar(255) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `email` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_customer_excel_tbl`
--

INSERT INTO `temp_customer_excel_tbl` (`id`, `upload_token`, `name`, `addresss`, `mobile`, `email`, `created_at`, `updated_at`, `status`) VALUES
(1, 'We7WOWsJtNKP87V', 'GOWARTHINI LC', '3/829 E, Sri Vigneshwara Nagar, 6th street , Palladam Road, Veerapandi post, Veerapandi, Tiruppur- 641605', '8870856666', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(2, 'We7WOWsJtNKP87V', 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(3, 'We7WOWsJtNKP87V', 'R.ARUNKUMAR', '1C,Golden Castle ,Kavetti Naidu Layout, Sowripalayam,Coimbatore-641028', '9944113440', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(4, 'We7WOWsJtNKP87V', 'SUGUMAR MUTHUSAMY and RESHMI R', 'No.56,Karunanithi Nagar, Sowripalayam,Cbe-641028', '8197159292', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(5, 'We7WOWsJtNKP87V', 'MRS.POORANI.K.T', '85,kalyani nagar,udumalaipettai,gandhi nagar,tirupur-642154', '9585523822', 'dhileepan62@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(6, 'We7WOWsJtNKP87V', 'SADHASIVAM', 'B3, Lakshimipuram, Peelamedu, Coimbatore 641004', '9442367722', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(7, 'We7WOWsJtNKP87V', 'DR RADHIKA', 'D21, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9363101831', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(8, 'We7WOWsJtNKP87V', 'R.HARIRAJAN', '22/A, D.D.A MIG-Flats,Pocket-2,Sector-7,Dwarka(west) West Delhi-110075', '9818071456', 'rharirajan@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(9, 'We7WOWsJtNKP87V', 'P.S VISHNOO VARDHAN', 'PLOT NO.13,4Th Cross Street, Siva Sakthi Nagar, Madhavaram-Chennai-600060', '8754589225', 'maddyvishnoo@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(10, 'We7WOWsJtNKP87V', 'THIRUNAVAKARASU and T.ROOPESH', '2/54, Arasu paniyalar nagar, Vialnkuruchi post, Coimbatore- 641035', '9443914955', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(11, 'We7WOWsJtNKP87V', 'RAJA KALYANA SUBRAMANIAN SAMBANTHAM', '701,Shanmuga Nagar, 7TH Cross,Uyakondan Thiramalai,Somasarasam Pettai, Trichy,-620102', '9842444466', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(12, 'We7WOWsJtNKP87V', 'KALYANASUNDARAM', '13/2,Rajkannan Garden ,Nehru Nagar,(west) Kalapatti(Po) Cbe-048', '9442264770', 'kalyancardio@yahoo.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(13, 'We7WOWsJtNKP87V', 'SOMAKUMAR and PREETHA', '4/404,Kottilingal Manniah House,Pazhambalacode,Palakkad,Tarur,Kerala-678544', '9894769561', 'rathina.sami@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(14, 'We7WOWsJtNKP87V', 'MR. ZOHAIR M. KAGALWALA AND MRS. NISRIN ZOHAIR', '14,Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(15, 'We7WOWsJtNKP87V', 'TN PRABHAKARAN', 'B3, Srinivasa Residency, 45, Sankaran Pillai Road,Opp to Aadhi Kottai Mariamman Kovil, Tiruchirappalli, Tamilnadu- 620002', '9952422004', 'trichyprabhaharan@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(16, 'We7WOWsJtNKP87V', 'A.NACHYAR', 'Sri Ganapathy,Subhase Nagar,Poonithvra,Cochin-682038', '9495538182', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(17, 'We7WOWsJtNKP87V', 'SATHISH KUMAR', '2/102, Evadugapalayam Elavanthi,Palladam,Tiruppur-641664', '9486071974', 'rsathishkumarr@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(18, 'We7WOWsJtNKP87V', 'AMBETHKAR', 'Plot No-29, Bhagyalakshmi Nagar, Madampakkam, Selaiyur, Kanchipuram, Selaiyur-600073', '7498167947', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(19, 'We7WOWsJtNKP87V', 'RAMESH & MRS.BHANU REKHA', '147,Rajivi Nagar,Manapparai,Trichy-621306', '9894769561', 'rathina.sami@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(20, 'We7WOWsJtNKP87V', 'Mr. MUFADDAL T MARHABA AND Mrs. MUNIRA M MARHABA', '12/E,Burhani Colony,Avarampalayam Road,Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(21, 'We7WOWsJtNKP87V', 'MUTHUSUBBIYAN', '38,Kandasamy street, Bazar post, T.C.Market, Tiruppur 641604', '9944235074', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(22, 'We7WOWsJtNKP87V', 'MANICKAVASAGAM', '100, Sengaliyappa nagar , Peelamedu, Coimbatore â€“ 641004', '9363045457', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(23, 'We7WOWsJtNKP87V', 'MR.RANJITH & G.MANJU', 'B2-4J -Gowtham ABC Avenue,Peelamedu', '9940975500', 'ranjith@futurerea.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(24, 'We7WOWsJtNKP87V', 'SREE VIDYA BHUPESE', 'B4-4G, Gowtham ABC Avenue Apartment, Avarampalayam Road, Coimbatore- 641004', '9994924984', 'vidhujan87@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(25, 'We7WOWsJtNKP87V', 'ANITHA', '1B,3rd Block-,Coral Castle Apartment,Peelamedu,Cbe-4', '9585523599', 'nithi_anii@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(26, 'We7WOWsJtNKP87V', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(27, 'We7WOWsJtNKP87V', 'MR.K.AYYAVU', 'A-11 Brindavan Nagar, Kalapatti , Coimbatore Aerodrome,Coimbatore-641014', '9443910804', 'ayyavus@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(28, 'We7WOWsJtNKP87V', 'SARASWATHI JEYARAJU', '6/236A,Muthu Gopal Nagar, Anuppurpalayam puthur,Periyar Colony,Tiruppur-641652', '9123589373', 'kamikki@hotmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(29, 'We7WOWsJtNKP87V', 'THIRUPATHI', 'G3,Gowtham Enclave 164-A Sarojini Street,Ram Nagar,Cbe-9', '9842232324', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(30, 'We7WOWsJtNKP87V', 'UMAMAHESHWARAN and PRIYADARSINI.U', '15-G Natesa Gounder Layout,Rathinapuri ,Coimbatore-27', '9894471544', 'mahesh71554@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(31, 'We7WOWsJtNKP87V', 'G.ANANDAKUMAR', 'SFS C-14, Housing unit, HUDCO Colony, VK Road, Peelamedu, Coimbatore-641004', '9843113160', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(32, 'We7WOWsJtNKP87V', 'ALEX ROBIN', '3B, 5th block, Sree Rose Appartments, near GV residency, Coimbatore- 641028', '9790696941', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(33, 'We7WOWsJtNKP87V', 'SRI VIGNESH and S.R.JANANI', '14/1, Kumaran street, Surampatti, Erode- 638009', '9629231445', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(34, 'We7WOWsJtNKP87V', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(35, 'We7WOWsJtNKP87V', 'KAVITHA RAJENDRAN and SUMITHA RAJESNDRAN. POA HOLDER RAJENDRAN', '269, 3rd cross, Ellai Thottam road, Coimbatore- 641004', '9894282292', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(36, 'We7WOWsJtNKP87V', 'Mr. MURTUZA Z VADNAGARWALA AND Mrs. FATEMA M VADNAGARWALA', '14,1st Floor Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(37, 'We7WOWsJtNKP87V', 'U.MITHUN and R.Thilagavathi', '1-1/58 Ponnagar, GanapathyPalayam, Salem-636030', '9884598226', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(38, 'We7WOWsJtNKP87V', 'TS SIVAPRIYA', 'A6, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '9600919888', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(39, 'We7WOWsJtNKP87V', 'JAISUNDER', '32/6, Hudco Colony, Peelamedu, Coimbatore-641004', '9842257160', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(40, 'We7WOWsJtNKP87V', 'SUBBURAJU', '104, Prime Enclave, Avinashi road, Gandhinagar, Tiruppur- 641603', '8754009505', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(41, 'We7WOWsJtNKP87V', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(42, 'We7WOWsJtNKP87V', 'M.S.GOWTHAM', 'F0A,Block-2,Jains sri Saanvi Apartment,Ramasamy Nagar,Uppilipalayam(po) Coimbatore-641015', '9842535570', 'msgowtham6@gmail.com', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(43, 'We7WOWsJtNKP87V', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '4222573196', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(44, 'We7WOWsJtNKP87V', 'SINDHUJA', 'No.8/1254-G Pappan Thottam Pooluvapatti(POST) Tiruppur -641602', '8220765295', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(45, 'We7WOWsJtNKP87V', 'SHARANYA', '4/20,Naicken Palayam SRKV-Via,cbe-641020', '9894337199', 'nilavan@yahoo.co.in', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(46, 'We7WOWsJtNKP87V', 'SUMATHA', 'A3, TWAD Board Quarters, Vilankurichi Road, Coimbatore 641004', '9842223698', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(47, 'We7WOWsJtNKP87V', 'Kaussal Kannan Rathinamani and Priyanka Sudarshan', '18, Kumarasamy Nagar First Street, Civil Aerodrome Road, Coimbatore South, Coimbatore Aerodrome, Coimbatore, Tamilnadu- 641014', '9600644411', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(48, 'We7WOWsJtNKP87V', 'ILANDURAIYAN', 'E-1, Block-1, Parijatha Apartment, Green Glen Layout, Bellandur, Bangalore 560103', '9916386964', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(49, 'We7WOWsJtNKP87V', 'KRISHNALATHA S', '6/149, KULATHUPALAYAM, SULTHANPET, SULUR TALUK, VARAPATTI, COIMBATORE- 641609', '9843266236', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(50, 'We7WOWsJtNKP87V', 'NIRMALA R', 'B 49, ANNA KUDIYIRUPPU, UDUMALAIPETTAI, TIRUPPUR- 642128', '9865853613', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(51, 'We7WOWsJtNKP87V', 'SARANYA', 'A1c, Isha Armonia Apartments, Vishweswara Nagar, Vilankuruchi road, Coimbatore- 641035', '9620611644', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(52, 'We7WOWsJtNKP87V', 'INDU', 'A4c, Isha Armonia apartment, Visweswara nagar, Vilankuruchi, Coimbatore- 641035', '9842789276', '', '2020-09-05 12:41:38', '2020-09-05 12:41:38', 0),
(53, 'T57EtxcJYa7Q07r', 'GOWARTHINI LC', '3/829 E, Sri Vigneshwara Nagar, 6th street , Palladam Road, Veerapandi post, Veerapandi, Tiruppur- 641605', '88708 56666', 'dr.vivekarumugham@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(54, 'T57EtxcJYa7Q07r', 'R.ARUNKUMAR', '1C,Golden Castle ,Kavetti Naidu Layout, Sowripalayam,Coimbatore-641028', '9944113440', 'shivaaniarun76@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(55, 'T57EtxcJYa7Q07r', 'SUGUMAR MUTHUSAMY and RESHMI R', 'No.56,Karunanithi Nagar, Sowripalayam,Cbe-641028', '81971 59292', 'sugu1030@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(56, 'T57EtxcJYa7Q07r', 'SADHASIVAM', 'B3, Lakshimipuram, Peelamedu, Coimbatore 641004', '94423 67722', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(57, 'T57EtxcJYa7Q07r', 'DR RADHIKA', 'D21, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '93631 01831', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(58, 'T57EtxcJYa7Q07r', 'THIRUNAVAKARASU and T.ROOPESH', '2/54, Arasu paniyalar nagar, Vialnkuruchi post, Coimbatore- 641035', '94439 14955', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(59, 'T57EtxcJYa7Q07r', 'RAJA KALYANA SUBRAMANIAN SAMBANTHAM', '701,Shanmuga Nagar, 7TH Cross,Uyakondan Thiramalai,Somasarasam Pettai, Trichy,-620102', '9842444466', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(60, 'T57EtxcJYa7Q07r', 'A.NACHYAR', 'Sri Ganapathy,Subhase Nagar,Poonithvra,Cochin-682038', '9495538182', 'kanathan@hotmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(61, 'T57EtxcJYa7Q07r', 'AMBETHKAR', 'Plot No-29, Bhagyalakshmi Nagar, Madampakkam, Selaiyur, Kanchipuram, Selaiyur-600073', '74981 67947', 'ambethkar_abus@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(62, 'T57EtxcJYa7Q07r', 'RAMESH & MRS.BHANU REKHA', '147,Rajivi Nagar,Manapparai,Trichy-621306', '9894769561', 'rathina.sami@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(63, 'T57EtxcJYa7Q07r', 'Mr. MUFADDAL T MARHABA AND Mrs. MUNIRA M MARHABA', '12/E,Burhani Colony,Avarampalayam Road,Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(64, 'T57EtxcJYa7Q07r', 'MUTHUSUBBIYAN', '38,Kandasamy street, Bazar post, T.C.Market, Tiruppur 641604', '99442 35074', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(65, 'T57EtxcJYa7Q07r', 'MANICKAVASAGAM', '100, Sengaliyappa nagar , Peelamedu, Coimbatore â€“ 641004', '93630 45457', 'kgmanickm2000@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(66, 'T57EtxcJYa7Q07r', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(67, 'T57EtxcJYa7Q07r', 'THIRUPATHI', 'G3,Gowtham Enclave 164-A Sarojini Street,Ram Nagar,Cbe-9', '9842232324', 'srts121213@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(68, 'T57EtxcJYa7Q07r', 'G.ANANDAKUMAR', 'SFS C-14, Housing unit, HUDCO Colony, VK Road, Peelamedu, Coimbatore-641004', '9843113160', 'anandkalpana10@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(69, 'T57EtxcJYa7Q07r', 'ALEX ROBIN', '3B, 5th block, Sree Rose Appartments, near GV residency, Coimbatore- 641028', '97906 96941', 'aruldossbsnl@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(70, 'T57EtxcJYa7Q07r', 'SRI VIGNESH and S.R.JANANI', '14/1, Kumaran street, Surampatti, Erode- 638009', '96292 31445', 'Sriviky@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(71, 'T57EtxcJYa7Q07r', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(72, 'T57EtxcJYa7Q07r', 'Kavitha Rajendran and Sumithaa Rajendran . POA Holder Rajendran', '269, 3rd cross, Ellai Thottam road, Coimbatore- 641004', '98942 82292', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(73, 'T57EtxcJYa7Q07r', 'Mr. MURTUZA Z VADNAGARWALA AND Mrs. FATEMA M VADNAGARWALA', '14,1st Floor Sakina Manzil Sumuga Gardens Peelamedu,cbe-4', '9363102027', 'mbtools@yahoo.co.in', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(74, 'T57EtxcJYa7Q07r', 'U.MITHUN and R.Thilagavathi', '1-1/58 Ponnagar, GanapathyPalayam, Salem-636030', '98845-98226', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(75, 'T57EtxcJYa7Q07r', 'TS SIVAPRIYA', 'A6, Mayflower Annapoorna garden, Dr. Munusamy nagar, Ramanathapuram, Coimbatore- 641045', '96009 19888', 'cpramaesh@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(76, 'T57EtxcJYa7Q07r', 'JAISUNDER', '32/6, Hudco Colony, Peelamedu, Coimbatore-641004', '9842257160', 'Sundarpeelamedu@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(77, 'T57EtxcJYa7Q07r', 'SUBBURAJU', '104, Prime Enclave, Avinashi road, Gandhinagar, Tiruppur- 641603', '87540 09505', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(78, 'T57EtxcJYa7Q07r', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(79, 'T57EtxcJYa7Q07r', 'AMSAVENI', '28/37, Valluvar Nagar, Peelamedu, Coimbatore â€“ 641004', '0422-2573196', 'keshsbm@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(80, 'T57EtxcJYa7Q07r', 'SINDHUJA', 'No.8/1254-G Pappan Thottam Pooluvapatti(POST) Tiruppur -641602', '82207 65295', 'vigneshp.ceg@gmail.com , sindhutpr@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(81, 'T57EtxcJYa7Q07r', 'SUMATHA', 'A3, TWAD Board Quarters, Vilankurichi Road, Coimbatore 641004', '98422 23698', 'jaikumarsuma@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(82, 'T57EtxcJYa7Q07r', 'Kaussal Kannan Rathinamani and Priyanka Sudarshan', '18, Kumarasamy Nagar First Street, Civil Aerodrome Road, Coimbatore South, Coimbatore Aerodrome, Coimbatore, Tamilnadu- 641014', '96006 44411 & 96000 03575', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(83, 'T57EtxcJYa7Q07r', 'ILANDURAIYAN', 'E-1, Block-1, Parijatha Apartment, Green Glen Layout, Bellandur, Bangalore 560103', '99163 86964', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(84, 'T57EtxcJYa7Q07r', 'KRISHNALATHA S', '6/149, KULATHUPALAYAM, SULTHANPET, SULUR TALUK, VARAPATTI, COIMBATORE- 641609', '98432 66236', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(85, 'T57EtxcJYa7Q07r', 'NIRMALA R', 'B 49, ANNA KUDIYIRUPPU, UDUMALAIPETTAI, TIRUPPUR- 642128', '98658 53613', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(86, 'T57EtxcJYa7Q07r', 'SARANYA', 'A1c, Isha Armonia Apartments, Vishweswara Nagar, Vilankuruchi road, Coimbatore- 641035', '9620611644', 'saranyaharidass@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(87, 'T57EtxcJYa7Q07r', 'INDU', 'A4c, Isha Armonia apartment, Visweswara nagar, Vilankuruchi, Coimbatore- 641035', '9842789276', 'indu.haridass@gmail.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(88, 'T57EtxcJYa7Q07r', 'PRAVEEN', '', '9976215566', '', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0),
(89, 'T57EtxcJYa7Q07r', 'DR SUBBAKANI', '', '8870944822', 'subbukani@yahoo.com', '2020-09-08 00:24:09', '2020-09-08 00:24:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `temp_excel_tbl`
--

DROP TABLE IF EXISTS `temp_excel_tbl`;
CREATE TABLE IF NOT EXISTS `temp_excel_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_token` varchar(50) NOT NULL,
  `uid` varchar(80) DEFAULT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `leadsource` varchar(200) NOT NULL,
  `flat_type` varchar(250) NOT NULL,
  `site_visited` varchar(250) NOT NULL,
  `profile` varchar(250) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `primary_mobile` varchar(200) NOT NULL,
  `primary_email` varchar(200) NOT NULL,
  `secondary_mobile` varchar(250) NOT NULL,
  `secondary_email` varchar(250) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `pincode` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_excel_tbl`
--

INSERT INTO `temp_excel_tbl` (`id`, `upload_token`, `uid`, `fname`, `lname`, `leadsource`, `flat_type`, `site_visited`, `profile`, `gender`, `primary_mobile`, `primary_email`, `secondary_mobile`, `secondary_email`, `address`, `city`, `state`, `description`, `pincode`, `created_at`, `updated_at`, `status`) VALUES
(1, '4ulsFbuBeO17tp5', NULL, 'Arun', 'Kumar', 'fb', '1 bhk', 'yes', 'profile 1', 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', 'sample 1', '61104', '2020-09-27 15:01:35', '2020-09-27 15:01:35', 1),
(2, '4ulsFbuBeO17tp5', NULL, 'Venget', 'Rajan', 'internet', '7bhk', 'no', 'profile 3', 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', 'sample 2', '614408', '2020-09-27 15:01:35', '2020-09-27 15:01:35', 1),
(3, 'SyDxz7NhpBL0Y7L', NULL, 'Swasthik', '', 'Facebook', '2 BHK', 'Yes', 'Business', 'Female', '7373021332', '', '', '', '', 'Cbe', 'TamilNadu', 'Did not ans call', '641027', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(4, 'SyDxz7NhpBL0Y7L', NULL, 'Duraiswamy', '', 'Google', '3BHK', 'No', 'Business', 'Male', '8825642733', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call', '641028', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(5, 'SyDxz7NhpBL0Y7L', NULL, 'Vetrivel', '', 'facebook', '2 BHK', 'Yes', 'Business', 'Male', '9841051590', '', '', '', '', 'Chennai', 'TamilNadu', 'has in mind, will call back', '641029', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(6, 'SyDxz7NhpBL0Y7L', NULL, 'Ramoorthy', '', 'facebook', '2 BHK', 'No', 'Business', 'Male', '9737356233', '', '', '', '', 'Cbe', 'TamilNadu', 'thinking over multiple choices in chennai and cbe', '641030', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(7, 'SyDxz7NhpBL0Y7L', NULL, 'Akshay', '', 'google', '3BHK', 'Yes', 'Business', 'Male', '8610739811', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call', '641031', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(8, 'SyDxz7NhpBL0Y7L', NULL, 'Ramkumar', '', 'google', '2 BHK', 'No', 'Business', 'Male', '9894013341', '', '', '', '', 'Chennai', 'TamilNadu', 'Room sizes are small, thinking off larger option', '641032', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(9, 'SyDxz7NhpBL0Y7L', NULL, 'Balasundaram', '', 'facebook', '2 BHK', 'Yes', 'Business', 'Male', '9677622669', '', '', '', '', 'Cbe', 'TamilNadu', 'Wants L Type, Ready to buy with full payment', '641033', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(10, 'SyDxz7NhpBL0Y7L', NULL, 'Prabha', '', 'facebook', '2 BHK', 'No', 'Doctor', 'Male', '6382735660', '', '', '', '', 'Madurai', 'TamilNadu', 'share details again, not replying', '641034', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(11, 'SyDxz7NhpBL0Y7L', NULL, 'Raji', '', 'facebook', '2 BHK', 'Yes', 'Doctor', 'female', '9113282820', '', '', '', '', 'Chennai', 'TamilNadu', 'Did not ans call', '641035', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(12, 'SyDxz7NhpBL0Y7L', NULL, 'Antojeba', '', 'google', '2 BHK', 'No', 'Doctor', 'Male', '8098600910', '', '', '', '', 'Cbe', 'TamilNadu', 'His brother enquired it', '641036', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(13, 'SyDxz7NhpBL0Y7L', NULL, 'Murali', '', 'google', '2 BHK', 'Yes', 'Doctor', 'Male', '9486922011', '', '', '', '', 'Madurai', 'TamilNadu', 'Will visit again with family', '641037', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(14, 'SyDxz7NhpBL0Y7L', NULL, 'Naveen', '', 'google', '2 BHK', 'No', 'Doctor', 'Male', '9894290431', '', '', '', '', 'Chennai', 'TamilNadu', 'Did not ans call, no response', '641038', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(15, 'SyDxz7NhpBL0Y7L', NULL, 'Gugan', '', 'google', '2 BHK', 'Yes', 'Doctor', 'Male', '9980148996', '', '', '', '', 'Cbe', 'TamilNadu', 'not planning to buy now', '641039', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(16, 'SyDxz7NhpBL0Y7L', NULL, 'Keerthana', '', 'facebook', '2 BHK', 'No', 'Doctor', 'female', '9962009596', '', '', '', '', 'Madurai', 'TamilNadu', 'Did not ans call, no response', '641040', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(17, 'SyDxz7NhpBL0Y7L', NULL, 'Madhan', '', 'facebook', '2 BHK', 'yes', 'Doctor', 'Male', '8122658371', '', '', '', '', 'Chennai', 'TamilNadu', 'out of their budget at present', '641041', '2020-10-01 16:10:41', '2020-10-01 16:10:41', 0),
(18, 'RQPhNbKBq3MJwRs', NULL, 'Arun', 'Kumar', 'fb', '1 bhk', 'yes', 'profile 1', 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', 'sample 1', '61104', '2020-10-05 16:04:01', '2020-10-05 16:04:01', 0),
(19, 'RQPhNbKBq3MJwRs', NULL, 'Venget', 'Rajan', 'internet', '7bhk', 'no', 'profile 3', 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', 'sample 2', '614408', '2020-10-05 16:04:01', '2020-10-05 16:04:01', 0),
(20, '72lovaei9GJZrm0', NULL, 'Arun', 'Kumar', 'fb', '1 bhk', 'yes', 'profile 1', 'male', '9942387100', 'koshik@webykart.com', '', '', '49 w mani nagar', 'coimbatore', 'tamil nadu', 'sample 1', '61104', '2020-10-05 16:04:39', '2020-10-05 16:04:39', 0),
(21, '72lovaei9GJZrm0', NULL, 'Venget', 'Rajan', 'internet', '7bhk', 'no', 'profile 3', 'male', '9944123022', 'koshikprabhur@venpep.net', '', '', '90 a Vaysnav building', 'coimbatore', 'tamil nadu', 'sample 2', '614408', '2020-10-05 16:04:39', '2020-10-05 16:04:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `virtualtour_menusettings_tbl`
--

DROP TABLE IF EXISTS `virtualtour_menusettings_tbl`;
CREATE TABLE IF NOT EXISTS `virtualtour_menusettings_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(250) NOT NULL,
  `tour_code` longtext NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `virtualtour_menusettings_tbl`
--

INSERT INTO `virtualtour_menusettings_tbl` (`id`, `menu`, `tour_code`, `status`, `created_at`, `updated_at`) VALUES
(1, 'show', 'http://360view.gowthamhousing.com/?is=0-main-entrance', 1, '2020-08-24 00:26:02', '2020-08-24 00:26:02');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
