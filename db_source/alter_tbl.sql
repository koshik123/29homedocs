ALTER TABLE `customer_tbl` ADD `approved_on` VARCHAR(250) NULL DEFAULT NULL AFTER `kyc_status`, ADD `approved_by` INT(11) NULL DEFAULT NULL AFTER `approved_on`; 

ALTER TABLE `customer_tbl` ADD `kyc_remarks` LONGTEXT NOT NULL AFTER `approved_by`; 

ALTER TABLE `customer_tbl` ADD `kyc_submittedon` VARCHAR(250) NULL DEFAULT NULL AFTER `approved_on`; 


/*12.08.2020*/

ALTER TABLE `employee_permission_tbl` ADD `lead_report` INT(11) NOT NULL AFTER `adhoc_report`, ADD `activity_report` INT(11) NOT NULL AFTER `lead_report`; 

ALTER TABLE `employee_permission_tbl` ADD `lead` INT(11) NOT NULL AFTER `employee_id`, ADD `manage_lead` INT(11) NOT NULL AFTER `lead`, ADD `lead_type` INT(11) NOT NULL AFTER `manage_lead`, ADD `activity_type` INT(11) NOT NULL AFTER `lead_type`; 

ALTER TABLE `customer_tbl` ADD `import_token` VARCHAR(250) NOT NULL AFTER `id`; 

/*11.09.2020*/

ALTER TABLE `gendral_documents_tbl` ADD `deleted_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

ALTER TABLE `flatvilla_documenttype_tbl` ADD `publish_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_status` INT(11) NOT NULL AFTER `publish_status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

ALTER TABLE `lead_tbl` ADD `deleted_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

ALTER TABLE `invoice_tbl` ADD `deleted_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

ALTER TABLE `gallery_tbl` ADD `deleted_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

ALTER TABLE `property_broucher_tbl` ADD `deleted_status` INT(11) NOT NULL AFTER `status`, ADD `deleted_by` INT(11) NOT NULL AFTER `deleted_status`, ADD `deleted_on` VARCHAR(250) NOT NULL AFTER `deleted_by`, ADD `restored_by` INT(11) NOT NULL AFTER `deleted_on`, ADD `restored_on` VARCHAR(250) NOT NULL AFTER `restored_by`, ADD `restored_remarks` LONGTEXT NOT NULL AFTER `restored_on`; 

/*27.09.2020*/

ALTER TABLE `temp_excel_tbl` ADD `flat_type` VARCHAR(250) NOT NULL AFTER `leadsource`, ADD `site_visited` VARCHAR(250) NOT NULL AFTER `flat_type`, ADD `profile` VARCHAR(250) NOT NULL AFTER `site_visited`; 

/*08.10.2020*/

ALTER TABLE `property_tbl` ADD `allotment_date` VARCHAR(250) NOT NULL AFTER `customer_id`, ADD `assign_remarks` LONGTEXT NOT NULL AFTER `allotment_date`; 

/*15.10.2020*/

ALTER TABLE `employee_tbl` ADD `role_id` INT(11) NOT NULL AFTER `role`, ADD `department_id` INT(11) NOT NULL AFTER `role_id`, ADD `is_manager` INT(11) NOT NULL AFTER `department_id`; 


/*24.10.20*/

ALTER TABLE `adhoc_request_tbl` ADD `assign_status` INT(11) NOT NULL AFTER `status`, ADD `closed_status` INT(11) NOT NULL AFTER `assign_status`, ADD `assign_employee_id` INT(11) NOT NULL AFTER `closed_status`; 

insert into `29homedocs_db`.`upload_video_tbl`(`id`,`type`,`title`,`page_video`,`added_by`,`status`,`created_at`,`updated_at`) values ( '2','','','','1','1','2020-08-12 22:00:16','2020-08-12 22:00:16')



ALTER TABLE `activity_type_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `adhocassign_logs_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_image_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_log_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_request_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `ticket_uid`; 

ALTER TABLE `admin_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `ad_token`; 

ALTER TABLE `assignadhoc_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `assignlead_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `block_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `company_settings_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `contactinfo_list_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `contact_id`; 

ALTER TABLE `contactinfo_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `customerdatasheet_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `customer_profile_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `customer_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `datasheet_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `department_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `employee_permission_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `employee_role_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `employee_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `flatvilla_documenttype_list_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `flatvilla_documenttype_tbl` ADD `project_id` INT(11) NOT NULL AFTER `token`; 

ALTER TABLE `flatvilla_documenttype_tbl` CHANGE `project_id` `project_id` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `flat_type_master` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `flat_type_masterlist` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `floor_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `gallery_image_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `gallery_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `gendral_documents_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `invoice_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_list_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_type_tbl` ADD `project_id` INT(11) NOT NULL AFTER `token`; 

ALTER TABLE `lead_activity_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_logs_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_source_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `lead_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `uid`; 

ALTER TABLE `lead_type_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `news_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `notification_email_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `paymentinfo_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `project_type_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_area_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `property_broucher_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `property_document_list_items` ADD `project_id` INT(11) NOT NULL AFTER `id`; 

ALTER TABLE `property_document_list_items` CHANGE `project_id` `project_id` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_document_list_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_gallery_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_gendral_documents_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `property_rooms_list_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `request_type_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `token`; 

ALTER TABLE `temp_excel_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `uid`; 

ALTER TABLE `temp_customer_excel_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `upload_token`; 

ALTER TABLE `upload_video_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `virtualtour_menusettings_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 


/*18.08.2021*/
/* Tenant id added to all table */

ALTER TABLE `activity_type_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhocassign_logs_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_image_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_log_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `adhoc_request_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `admin_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `assignadhoc_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `assignlead_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `block_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `company_settings_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `contactinfo_list_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `contactinfo_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `customerdatasheet_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `customer_profile_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `customer_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `datasheet_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 



ALTER TABLE `department_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `employee_permission_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `employee_role_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `employee_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `flatvilla_documenttype_list_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `flatvilla_documenttype_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `flat_type_master` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `flat_type_masterlist` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `floor_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `gallery_image_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `gallery_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `gendral_documents_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `invoice_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_list_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `kyc_type_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_activity_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_logs_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_source_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `lead_type_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `news_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `notification_email_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `paymentinfo_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `project_master_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `project_type_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 


ALTER TABLE `property_area_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_broucher_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_document_list_items` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_document_list_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_gallery_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_gendral_documents_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_rooms_list_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `property_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `request_type_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `session_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `temp_customer_excel_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `temp_excel_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `upload_video_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 

ALTER TABLE `virtualtour_menusettings_tbl` ADD `tenant_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`; 




ALTER TABLE `employee_tbl` ADD `account_type` VARCHAR(150) NULL DEFAULT NULL AFTER `status`; 

UPDATE `activity_type_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `adhocassign_logs_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `adhoc_image_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `adhoc_log_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `adhoc_request_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `admin_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `assignadhoc_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `assignlead_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `block_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `company_settings_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `contactinfo_list_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `contactinfo_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `customerdatasheet_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `customer_profile_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `customer_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `datasheet_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `department_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `employee_permission_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `employee_role_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `employee_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `flatvilla_documenttype_list_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `flatvilla_documenttype_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `flat_type_master` SET `tenant_id`='1' WHERE 1; 

UPDATE `flat_type_masterlist` SET `tenant_id`='1' WHERE 1 ;

UPDATE `flat_type_masterlist` SET `tenant_id`='1' WHERE 1 ;

UPDATE `floor_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `gallery_image_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `gallery_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `gendral_documents_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `invoice_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `kyc_list_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `kyc_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `kyc_type_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `lead_activity_tbl` SET `tenant_id`='1' WHERE 1;

UPDATE `lead_logs_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `lead_source_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `lead_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `lead_type_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `news_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `notification_email_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `paymentinfo_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `project_master_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `project_type_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_area_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_broucher_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_document_list_items` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_document_list_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_gallery_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_gendral_documents_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_rooms_list_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `property_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `request_type_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `session_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `temp_customer_excel_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `temp_excel_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `upload_video_tbl` SET `tenant_id`='1' WHERE 1 ;

UPDATE `virtualtour_menusettings_tbl` SET `tenant_id`='1' WHERE 1 ;

/*24.08.2021*/

ALTER TABLE `property_tbl` ADD `property_id` INT( 11 ) NOT NULL DEFAULT '0' AFTER `project_id` ;

ALTER TABLE `customer_tbl` ADD `property_id` INT(11) NOT NULL DEFAULT '0' AFTER `project_id`; 


/*26.08.2021*/

ALTER TABLE `employee_tbl` ADD `resetpassword` INT(11) NOT NULL DEFAULT '0' AFTER `super_admin`; 

/*27.08.2021*/

ALTER TABLE `activity_type_tbl` CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

ALTER TABLE `activity_type_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `activity_type` `activity_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `adhocassign_logs_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `adhoc_type` `adhoc_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT '0', CHANGE `created_to` `created_to` INT(11) NOT NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `adhoc_image_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `adoc_id` `adoc_id` INT(11) NULL DEFAULT '0', CHANGE `property_id` `property_id` INT(11) NULL DEFAULT '0', CHANGE `customer_id` `customer_id` INT(11) NULL DEFAULT '0', CHANGE `image_name` `image_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `file_type` `file_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `adhoc_log_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `request_type` `request_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NOT NULL DEFAULT '0', CHANGE `created_to` `created_to` INT(11) NOT NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `adhoc_request_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `ticket_uid` `ticket_uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `property_id` `property_id` INT(11) NULL DEFAULT '0', CHANGE `customer_id` `customer_id` INT(11) NULL DEFAULT '0', CHANGE `employee_id` `employee_id` INT(11) NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NULL DEFAULT '0', CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `request_type` `request_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `requesttype_id` `requesttype_id` INT(11) NOT NULL DEFAULT '0', CHANGE `subject` `subject` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `request_status` `request_status` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `assign_status` `assign_status` INT(11) NOT NULL DEFAULT '0', CHANGE `closed_status` `closed_status` INT(11) NOT NULL DEFAULT '0', CHANGE `assign_employee_id` `assign_employee_id` INT(11) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `admin_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `ad_token` `ad_token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_name` `ad_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_mobile` `ad_mobile` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_email` `ad_email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_dob` `ad_dob` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address1` `address1` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address2` `address2` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_city` `ad_city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_pincode` `ad_pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_password` `ad_password` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_type` `ad_type` INT(1) NOT NULL DEFAULT '0', CHANGE `ad_super_admin` `ad_super_admin` INT(1) NOT NULL DEFAULT '0', CHANGE `ad_status` `ad_status` INT(1) NOT NULL DEFAULT '0';

ALTER TABLE `assignadhoc_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NOT NULL DEFAULT '0', CHANGE `created_to` `created_to` INT(11) NOT NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `priority` `priority` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `remove_status` `remove_status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `assignlead_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NOT NULL DEFAULT '0', CHANGE `created_to` `created_to` INT(11) NOT NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `priority` `priority` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `remove_status` `remove_status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `block_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `block` `block` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `company_settings_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `phone` `phone` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `website_address` `website_address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address` `address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `contactinfo_list_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `contact_id` `contact_id` INT(11) NOT NULL DEFAULT '0', CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `designation` `designation` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `contactinfo_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `role` `role` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `customerdatasheet_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `import_token` `import_token` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uploaded_sheet` `uploaded_sheet` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(2) NOT NULL DEFAULT '0'; 

ALTER TABLE `customer_profile_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `customer_profile` `customer_profile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `customer_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `import_token` `import_token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_id` `lead_id` INT(11) NULL DEFAULT '0', CHANGE `uid` `uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `priority` `priority` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `allotment_date` `allotment_date` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `gender` `gender` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `dob` `dob` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `password` `password` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `has_psw` `has_psw` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `nationality` `nationality` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_contact_person` `ad_contact_person` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_relationship` `ad_relationship` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_mobile` `ad_mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_email` `ad_email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_address` `ad_address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_city` `ad_city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_state` `ad_state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ad_pincode` `ad_pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `kyc_status` `kyc_status` INT(11) NOT NULL DEFAULT '0', CHANGE `approved_on` `approved_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `kyc_submittedon` `kyc_submittedon` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `approved_by` `approved_by` INT(11) NULL DEFAULT '0', CHANGE `assign_status` `assign_status` INT(11) NULL DEFAULT '0', CHANGE `resetpassword` `resetpassword` INT(11) NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `datasheet_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `import_token` `import_token` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uploaded_sheet` `uploaded_sheet` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(2) NOT NULL DEFAULT '0'; 

ALTER TABLE `department_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `employee_permission_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `employee_id` `employee_id` INT(11) NULL DEFAULT '0', CHANGE `lead` `lead` INT(11) NOT NULL DEFAULT '0', CHANGE `manage_lead` `manage_lead` INT(11) NOT NULL DEFAULT '0', CHANGE `import_lead` `import_lead` INT(11) NOT NULL DEFAULT '0', CHANGE `customers` `customers` INT(11) NULL DEFAULT '0', CHANGE `manage_customer` `manage_customer` INT(11) NOT NULL DEFAULT '0', CHANGE `kyc_documents` `kyc_documents` INT(11) NOT NULL DEFAULT '0', CHANGE `invoice` `invoice` INT(11) NOT NULL DEFAULT '0', CHANGE `import_customer` `import_customer` INT(11) NOT NULL DEFAULT '0', CHANGE `property` `property` INT(11) NOT NULL DEFAULT '0', CHANGE `manage_property` `manage_property` INT(11) NOT NULL DEFAULT '0', CHANGE `property_chart` `property_chart` INT(11) NOT NULL DEFAULT '0', CHANGE `gallery` `gallery` INT(11) NOT NULL DEFAULT '0', CHANGE `adhoc` `adhoc` INT(11) NOT NULL DEFAULT '0', CHANGE `open_request` `open_request` INT(11) NOT NULL DEFAULT '0', CHANGE `close_request` `close_request` INT(11) NOT NULL DEFAULT '0', CHANGE `news` `news` INT(11) NOT NULL DEFAULT '0', CHANGE `manage_news` `manage_news` INT(11) NOT NULL DEFAULT '0', CHANGE `settings` `settings` INT(11) NOT NULL DEFAULT '0', CHANGE `employee` `employee` INT(11) NOT NULL DEFAULT '0', CHANGE `manage_department` `manage_department` INT(11) NOT NULL DEFAULT '0', CHANGE `virtual_tour` `virtual_tour` INT(11) NOT NULL DEFAULT '0', CHANGE `notification` `notification` INT(11) NOT NULL DEFAULT '0', CHANGE `reports` `reports` INT(11) NOT NULL DEFAULT '0', CHANGE `adhoc_report` `adhoc_report` INT(11) NOT NULL DEFAULT '0', CHANGE `lead_report` `lead_report` INT(11) NOT NULL DEFAULT '0', CHANGE `customer_report` `customer_report` INT(11) NOT NULL DEFAULT '0', CHANGE `kyc_report` `kyc_report` INT(11) NOT NULL DEFAULT '0', CHANGE `master_settings` `master_settings` INT(11) NOT NULL DEFAULT '0', CHANGE `lead_type` `lead_type` INT(11) NOT NULL DEFAULT '0', CHANGE `activity_type` `activity_type` INT(11) NOT NULL DEFAULT '0', CHANGE `lead_source` `lead_source` INT(11) NULL DEFAULT '0', CHANGE `profile_master` `profile_master` INT(11) NOT NULL DEFAULT '0', CHANGE `general_documents` `general_documents` INT(11) NOT NULL DEFAULT '0', CHANGE `document_checklist` `document_checklist` INT(11) NOT NULL DEFAULT '0', CHANGE `block` `block` INT(11) NOT NULL DEFAULT '0', CHANGE `floor` `floor` INT(11) NOT NULL DEFAULT '0', CHANGE `flat_type` `flat_type` INT(11) NOT NULL DEFAULT '0', CHANGE `brochure` `brochure` INT(11) NOT NULL DEFAULT '0', CHANGE `contact_info` `contact_info` INT(11) NULL DEFAULT '0', CHANGE `payment_info` `payment_info` INT(11) NOT NULL DEFAULT '0', CHANGE `area_master` `area_master` INT(11) NOT NULL DEFAULT '0', CHANGE `department` `department` INT(11) NOT NULL DEFAULT '0', CHANGE `role_master` `role_master` INT(11) NOT NULL DEFAULT '0', CHANGE `kycdoc_type` `kycdoc_type` INT(11) NOT NULL DEFAULT '0', CHANGE `company` `company` INT(11) NOT NULL DEFAULT '0', CHANGE `trash` `trash` INT(11) NOT NULL DEFAULT '0', CHANGE `upload_video` `upload_video` INT(11) NOT NULL DEFAULT '0', CHANGE `adhoc_request` `adhoc_request` INT(11) NOT NULL DEFAULT '0', CHANGE `all_request` `all_request` INT(11) NOT NULL DEFAULT '0', CHANGE `documents` `documents` INT(11) NOT NULL DEFAULT '0', CHANGE `request_type` `request_type` INT(11) NOT NULL DEFAULT '0', CHANGE `activity_report` `activity_report` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `employee_role_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `employee_role` `employee_role` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `employee_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `employee_uid` `employee_uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `role_id` `role_id` INT(11) NOT NULL DEFAULT '0', CHANGE `department_id` `department_id` INT(11) NOT NULL DEFAULT '0', CHANGE `is_manager` `is_manager` INT(11) NOT NULL DEFAULT '0', CHANGE `gender` `gender` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `password` `password` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address` `address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `super_admin` `super_admin` INT(11) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `flatvilla_documenttype_list_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `document_id` `document_id` INT(11) NOT NULL DEFAULT '0', CHANGE `doc_name` `doc_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `flatvilla_documenttype_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `doc_title` `doc_title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `sort_order` `sort_order` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `publish_status` `publish_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `flat_type_master` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `flat_variant` `flat_variant` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `flat_type_masterlist` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `flattype_id` `flattype_id` INT(11) NOT NULL DEFAULT '0', CHANGE `room_type` `room_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `sqft` `sqft` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `floor_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `floor` `floor` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `gallery_image_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `gallery_id` `gallery_id` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `gallery_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `album_title` `album_title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `sort_order` `sort_order` INT(11) NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `gendral_documents_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `documents` `documents` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `document_type` `document_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `invoice_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `inv_uid` `inv_uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `customer_id` `customer_id` INT(11) NULL DEFAULT '0', CHANGE `inv_number` `inv_number` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `inv_date` `inv_date` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `raised_to` `raised_to` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `documents` `documents` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `document_type` `document_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `kyc_list_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `customer_id` `customer_id` INT(11) NOT NULL DEFAULT '0', CHANGE `kyctype_id` `kyctype_id` INT(11) NOT NULL DEFAULT '0', CHANGE `image_name` `image_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `file_type` `file_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `approval_status` `approval_status` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `kyc_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `kyc_uid` `kyc_uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `property_id` `property_id` INT(11) NULL DEFAULT '0', CHANGE `customer_id` `customer_id` INT(11) NULL DEFAULT '0', CHANGE `id_type` `id_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `firstname` `firstname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lastname` `lastname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `dob` `dob` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address1` `address1` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address2` `address2` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `nationiality` `nationiality` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `approval_status` `approval_status` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `kyc_type_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `project_id` `project_id` INT(11) NOT NULL DEFAULT '0', CHANGE `kyc_type` `kyc_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `lead_activity_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `date` `date` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_id` `lead_id` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `employee_id` `employee_id` INT(11) NOT NULL DEFAULT '0', CHANGE `activity_id` `activity_id` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `lead_logs_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_type` `lead_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `created_by` `created_by` INT(11) NULL DEFAULT '0', CHANGE `created_to` `created_to` INT(11) NOT NULL DEFAULT '0', CHANGE `ref_id` `ref_id` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `lead_source_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_source` `lead_source` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `lead_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `import_token` `import_token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uid` `uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `auth_type` `auth_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `property_id` `property_id` INT(11) NULL DEFAULT '0', CHANGE `customer_id` `customer_id` INT(11) NULL DEFAULT '0', CHANGE `lead_date` `lead_date` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `fname` `fname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lname` `lname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `leadsource` `leadsource` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `flattype_id` `flattype_id` INT(11) NULL DEFAULT '0', CHANGE `site_visit_status` `site_visit_status` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `customer_profile_id` `customer_profile_id` INT(11) NULL DEFAULT '0', CHANGE `gender` `gender` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `secondary_mobile` `secondary_mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `secondary_email` `secondary_email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_status_id` `lead_status_id` INT(11) NOT NULL DEFAULT '0', CHANGE `employee_id` `employee_id` INT(11) NOT NULL DEFAULT '0', CHANGE `assign_status` `assign_status` INT(11) NULL DEFAULT '0', CHANGE `closed_status` `closed_status` INT(11) NULL DEFAULT '0', CHANGE `moved_status` `moved_status` INT(1) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `lead_type_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `uid` `uid` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lead_type` `lead_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `default_settings` `default_settings` INT(1) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `news_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `date` `date` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email_notification` `email_notification` INT(11) NOT NULL DEFAULT '0', CHANGE `post_to` `post_to` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `customer_ids` `customer_ids` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `show_popup` `show_popup` INT(11) NOT NULL DEFAULT '0', CHANGE `popup_till` `popup_till` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `send_mail_status` `send_mail_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `notification_email_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `adhoc_permission` `adhoc_permission` INT(1) NOT NULL DEFAULT '0', CHANGE `kyc_permission` `kyc_permission` INT(1) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `paymentinfo_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `account_number` `account_number` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `bank` `bank` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ifcs_code` `ifcs_code` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `branch` `branch` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` INT(11) NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `project_master_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `project_name` `project_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `project_type_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `project_name` `project_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_area_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `carpet_area` `carpet_area` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `total_area` `total_area` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `usable_area` `usable_area` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `common_area` `common_area` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uds` `uds` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `uds_perc` `uds_perc` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `property_broucher_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `property_id` `property_id` INT(11) NOT NULL DEFAULT '0', CHANGE `brochures` `brochures` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `property_document_list_items` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `document_id` `document_id` INT(11) NOT NULL DEFAULT '0', CHANGE `document_list_id` `document_list_id` INT(11) NOT NULL DEFAULT '0', CHANGE `property_id` `property_id` INT(11) NOT NULL DEFAULT '0', CHANGE `doc_name` `doc_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `document_file` `document_file` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `doc_type` `doc_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `doc_status` `doc_status` INT(11) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_document_list_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `property_id` `property_id` INT(11) NOT NULL DEFAULT '0', CHANGE `documents_id` `documents_id` INT(11) NOT NULL DEFAULT '0', CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_gallery_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `property_id` `property_id` INT(11) NOT NULL DEFAULT '0', CHANGE `images` `images` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_gendral_documents_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `documents` `documents` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `document_type` `document_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_rooms_list_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `property_id` `property_id` INT(11) NOT NULL DEFAULT '0', CHANGE `roomlist_id` `roomlist_id` INT(11) NOT NULL DEFAULT '0', CHANGE `image` `image` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `property_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `type` `type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `house_facing` `house_facing` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `projectsize` `projectsize` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `projectarea` `projectarea` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `cost` `cost` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `room_id` `room_id` INT(11) NOT NULL DEFAULT '0', CHANGE `block_id` `block_id` INT(11) NOT NULL DEFAULT '0', CHANGE `floor_id` `floor_id` INT(11) NOT NULL DEFAULT '0', CHANGE `area_type_id` `area_type_id` INT(11) NOT NULL DEFAULT '0', CHANGE `address` `address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `image` `image` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `assign_status` `assign_status` INT(11) NOT NULL DEFAULT '0', CHANGE `cancel_status` `cancel_status` INT(11) NOT NULL DEFAULT '0', CHANGE `reason_cancellation` `reason_cancellation` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `deleted_status` `deleted_status` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_by` `deleted_by` INT(11) NOT NULL DEFAULT '0', CHANGE `deleted_on` `deleted_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `restored_by` `restored_by` INT(11) NOT NULL DEFAULT '0', CHANGE `restored_on` `restored_on` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `request_type_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `token` `token` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `request_type` `request_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `department_id` `department_id` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0', CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0'; 

ALTER TABLE `session_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `logged_id` `logged_id` INT(11) NOT NULL DEFAULT '0', CHANGE `user_type` `user_type` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `portal_type` `portal_type` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `auth_referer` `auth_referer` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `auth_medium` `auth_medium` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `auth_ip_address` `auth_ip_address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

ALTER TABLE `temp_customer_excel_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `upload_token` `upload_token` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `name` `name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `addresss` `addresss` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `mobile` `mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `email` `email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(2) NOT NULL DEFAULT '0'; 

ALTER TABLE `temp_excel_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `upload_token` `upload_token` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `fname` `fname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `lname` `lname` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `leadsource` `leadsource` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `flat_type` `flat_type` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `site_visited` `site_visited` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `profile` `profile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `gender` `gender` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `primary_mobile` `primary_mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `primary_email` `primary_email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `secondary_mobile` `secondary_mobile` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `secondary_email` `secondary_email` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `address` `address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `city` `city` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `state` `state` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `pincode` `pincode` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(2) NOT NULL DEFAULT '0';

ALTER TABLE `tenant_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `pincode` `pincode` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 


ALTER TABLE `upload_video_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `title` `title` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `page_video` `page_video` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `video_type` `video_type` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `added_by` `added_by` INT(11) NOT NULL DEFAULT '0', CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

ALTER TABLE `virtualtour_menusettings_tbl` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `menu` `menu` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `status` `status` INT(1) NOT NULL DEFAULT '0'; 

/*16.09.2021*/

ALTER TABLE `stage_master_tbl` ADD `project_id` INT(11) NOT NULL DEFAULT '0' AFTER `tenant_id`; 