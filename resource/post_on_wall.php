<?php
session_start();
// require_once '../config/config.php';
require_once '../app/core/ajaxcontroller.php';
$route      = new Ajaxcontroller();
$page = @$_REQUEST["page"];
$arr = $_POST;


$images = array();
if(isset($arr) && isset($arr['post_data'])){
    $arrImageData = $arr['post_data'];
    if(is_array($arrImageData)){
        for($i=0; $i<count($arrImageData); $i++){
            if($arrImageData[$i]['image_data'] != ''){
                $varImageData = preg_replace('/^data:image\/(png|jpg|jpeg);base64,/', '', $arrImageData[$i]['image_data']);
                $varImageData = base64_decode($varImageData);
                $varImageIndex = $arrImageData[$i]['index'];
                $varImageName = preg_replace('/[^a-zA-Z0-9]/', '-', $arrImageData[$i]['name']);
                $varFileName = $route->generateRandomString("15").date("Ymdhis").$varImageIndex.'.jpg';             
                $images[] = $varFileName;
                $file = fopen($varFileName, 'wb');
                fwrite($file, $varImageData);
                fclose($file);
            }
        }
    }
}

// Move Files from One Location to another

if (count($images)>0) {
    $source = "";
    $destination = "uploads/srcimg/";

    foreach ( $images as $file ) {
        if (in_array($file, array(".",".."))) continue;
        // If we copied this successfully, mark it for deletion
        if (copy($source.$file, $destination.$file)) {
            $delete[] = $source.$file;
        }
    }

    // Delete all successfully-copied files

    foreach ( $delete as $file ) {
        unlink( $file );
    }
}


// Contents from Form

$content_array = $_POST['content'];
$new_content = array();
foreach ($content_array as $key => $value) {
   $new_content[$value['name']] = $value['value'];
}

//print_r($new_content);

// Save the Post
switch($page){

    case 'addAdhocRequests':       
        echo $route->postOnWallImg($images,$new_content);
    break;  


    case 'replayAdhocRequest':       
        echo $route->replayAdhocRequest($images,$new_content);
    break;  
}


//echo $route->postOnWallImg($images,$new_content);

