<?php 
session_start();
//require_once '../config/config.php';
require_once '../app/core/ajaxcontroller.php';
$route = new Ajaxcontroller();

$page = @$_REQUEST["page"];
$data = @$_REQUEST["element"];
$term = @$_REQUEST["term"];
$post = @$_POST["result"];


switch($page){

	/*-------------------------------------------------------------------------------------
								Login Auth
	---------------------------------------------------------------------------------------*/

	case 'userLogin':		
		echo $route -> userLogin($_POST);
	break;	

	case 'editProfile':		
		echo $route -> editProfile($_POST);
	break;

	case 'changePassword':		
		echo $route -> changePassword($_POST);
	break;

	case 'forgot_password':
			echo $route -> forgot_password($_POST);
	break;

	case 'resetPassword':
		echo $route -> resetPassword($post);
	break;

	/*=====================================
	        General Documents
	=======================================*/

	case 'viewGenDocInfo':
		$array 	=  $route -> viewGenDocInfo($post);
		echo json_encode($array);
	break;

	/*=====================================
	        Private Documents
	=======================================*/

	case 'viewPrivateDocInfo':
		$array 	=  $route -> viewPrivateDocInfo($post);
		echo json_encode($array);
	break;
	case 'viewPrivateCommonDocInfo':
		$array 	=  $route -> viewPrivateCommonDocInfo($post);
		echo json_encode($array);
	break;

	/*-------------------------------------------------------------------------------------
							Adhoc requests
	---------------------------------------------------------------------------------------*/

	case 'addAdhocRequest':		
		echo $route -> addAdhocRequest($_POST);
	break;	
	case 'replayAdhocRequest':		
		echo $route -> replayAdhocRequest($_POST);
	break;	
	case 'closeTicket':	
		echo $route->closeTicket($post);
	break;	

	/*-------------------------------------------------------------------------------------
							KYC
	---------------------------------------------------------------------------------------*/

	case 'deletKYCAttachmentImage':
		echo $route -> deletKYCAttachmentImage($post);
	break;	
	case 'editKYCAttachmentImage':
		$array 	=  $route -> editKYCAttachmentImage($post);
		echo json_encode($array);
	break;
	case 'removeKYCImg':
		$id 	= $post;
		$info 	= $route->getDetails(KYC_LIST,"image_name", " id= '$id' ");
		if ($info['image_name']!="") {
			unlink("uploads/srcimg/".$info['image_name']);
		}
		echo $route -> removeKYCImg($id);
	break;
	case 'updateKYCFiles':
		echo $route -> updateKYCFiles($_POST);
	break;

	case 'selectProperty':
		echo $route -> selectProperty($post);
	break;
	
	// Contact Form
	
	/*-------------------------------------------------------------------------------------
								Test Mail
	---------------------------------------------------------------------------------------*/

	case 'sendMail':
		$sender = COMPANY_NAME;
		$sender_mail = "no_reply";
		$receiver = "koshik@webykart.com";
		$subject = "Reset Password Mail";
		//$email_temp = "test1234567879";
		$email_temp = $route->newKYCSubmitNotification($ticket_uid="T0001",$user_id="1",$property_id="1"); 
		echo $email_temp;
		//echo $route->send_mail($sender,$sender_mail,$receiver,$subject,$email_temp,$bcc="");
	break;


}
?>