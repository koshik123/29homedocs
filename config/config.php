<?php	
	//require_once './global/global-config.php'; 
	
	//--------------------- WEBSITE PATH DEFINITIONS ---------------------//
	
	// Website Base Path
	define('BASEPATH', HOST_NAME.'/');
	// Website Admin Path
	define('COREPATH', HOST_NAME.ADMIN_PATH.'/');
	

	// Define image folder path
	define('IMGPATH', HOST_NAME.'/lib/images/');
	// Define css folder path
	define('CSSPATH', HOST_NAME.'/lib/css/');
	// Define js folder path
	define('JSPATH', HOST_NAME.'/lib/js/');
	// Define js folder path
	define('FONTPATH', HOST_NAME.'/lib/fonts/');
	// Define Plugin folder path
	define('PLUGINS', HOST_NAME.'/lib/plugins/');
	// Define Gift Image path
	define('UPLOADS', HOST_NAME.'/resource/uploads/');

	// Define Gift Image path
	define('FRONTSRCIMG', HOST_NAME.'/resource/uploads/srcimg/');
	// Define Source Image Path folder path
	define('SRCIMG', HOST_NAME.ADMIN_PATH.'/resource/uploads/srcimg/');
	// Define Admin Gallery folder path
	define('GALLERY', HOST_NAME.ADMIN_PATH.'/resource/uploads/gallery/');
	// Define Uploads Image path
	define('DOCUMENTS', HOST_NAME.ADMIN_PATH.'/resource/uploads/document/');

	
	//--------------------- DATABASE CONNECTION ---------------------//

	// Connect Mysql DB
	$connect = mysqli_connect(DB_SERVER , DB_USER, DB_PASSWORD, DB_DATABASE);
	
?>