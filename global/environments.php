<?php
	$host = $_SERVER['HTTP_HOST'];
	
	//--------------------- SELECT ENVIRONMENT ---------------------//

	switch ($host) {

		case 'localhost:8080':

			// Mail Settings
			require_once 'mail-local.php';
			require_once 'site-contents.php';

			// Database Configuration 
			$serverName = 'localhost';
			$dbUserName = 'root';
			$dbPassword = '';
			$dbName = 'postcrm_db';

			define("DB_SERVER", "localhost");
			define("DB_USER", "root");
			define("DB_PASSWORD", "");
			define("DB_DATABASE", "postcrm_db");

			// Host Name
	   		define('HOST_NAME', 'http://localhost:8080/venpep/29homedocs');
	   		// Site Mode
	   		define('SITE_MODE', 'development');   	

	   		/*--------------------------------------------------------------
	   			AWS S3 Bucket 
			--------------------------------------------------------------*/

			// AWS Public url
			define('AWS_URL', 'https://29homesdocs.s3.ap-south-1.amazonaws.com/');
			// AWS Bucket
			define('AWS_S3_BUCKET', '29homesdocs');


		break;

		case 'localhost':

			// Mail Settings
			require_once 'mail-local.php';
			require_once 'site-contents.php';

			// Database Configuration 
			$serverName = 'localhost';
			$dbUserName = 'root';
			$dbPassword = '';
			$dbName = '29homedocs_db';


			define("DB_SERVER", "localhost");
			define("DB_USER", "root");
			define("DB_PASSWORD", "");
			define("DB_DATABASE", "29homedocs_db");

			// Host Name
	   		define('HOST_NAME', 'http://localhost/venpep/29homedocs');
	   		// Site Mode
	   		define('SITE_MODE', 'development');   		


	   		/*--------------------------------------------------------------
	   			AWS S3 Bucket 
			--------------------------------------------------------------*/

			// AWS Public url
			define('AWS_URL', 'https://29homesdocs.s3.ap-south-1.amazonaws.com/');
			// AWS Bucket
			define('AWS_S3_BUCKET', '29homesdocs');
	   		
			

		break;

		case 'demo.venpep.com':
			
			// Mail Settings
			require_once 'mail-dev.php';
			require_once 'site-contents.php';

			// Database Configuration 
			$serverName = 'localhost';
			$dbUserName = 'koshik';
			$dbPassword = 'uvytu2yra';
			$dbName = 'zadmin_postsalescrm';

			define("DB_SERVER", "localhost");
			define("DB_USER", "koshik");
			define("DB_PASSWORD", "uvytu2yra");
			define("DB_DATABASE", "zadmin_postsalescrm");

			// Host Name
	   		define('HOST_NAME', 'http://demo.venpep.com/29homedocs');
			// Site Mode
	   		define('SITE_MODE', 'development');
	   		
	   		/*--------------------------------------------------------------
	   			AWS S3 Bucket 
			--------------------------------------------------------------*/

			// AWS Public url
			define('AWS_URL', 'https://29homesdocs.s3.ap-south-1.amazonaws.com/');
			// AWS Bucket
			define('AWS_S3_BUCKET', '29homesdocs');
	   		

		break;

		case 'dev.venpep.co.in':
			
			// Mail Settings
			require_once 'mail-dev.php';
			require_once 'site-contents.php';

			// Database Configuration 
			$serverName = 'localhost';
			$dbUserName = 'venpepdemo';
			$dbPassword = 'te4a6e7e8';
			$dbName = 'zadmin_29homedocs';

			define("DB_SERVER", "localhost");
			define("DB_USER", "venpepdemo");
			define("DB_PASSWORD", "te4a6e7e8");
			define("DB_DATABASE", "zadmin_29homedocs");

			// Host Name
	   		define('HOST_NAME', 'http://dev.venpep.co.in/29homedocs');
			// Site Mode
	   		define('SITE_MODE', 'development');
	   		
	   		/*--------------------------------------------------------------
	   			AWS S3 Bucket 
			--------------------------------------------------------------*/

			// AWS Public url
			define('AWS_URL', 'https://29homesdocs.s3.ap-south-1.amazonaws.com/');
			// AWS Bucket
			define('AWS_S3_BUCKET', '29homesdocs');
	   		

		break;
		
		default:
		break;
	}

?>