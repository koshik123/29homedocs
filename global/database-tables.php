<?php
	
	define("SESSION_TBL","session_tbl");
	define("ADMIN_TBL","admin_tbl"); 
	define("CUSTOMER_TBL","customer_tbl"); 
	define("BLOCK_TBL","block_tbl"); 
	define("FLOOR_TBL","floor_tbl"); 
	define("FLATVILLA_DOC_TYPE","flatvilla_documenttype_tbl"); 
	define("FLATVILLA_DOCTYPE_LIST","flatvilla_documenttype_list_tbl");
	define("PROPERTY_TBL","property_tbl");
	
	define("CONTACT_INFO","contactinfo_tbl");
	define("CONTACT_INFO_LIST","contactinfo_list_tbl");
	define("PAYMENT_INFO","paymentinfo_tbl");

	define("EMPLOYEE","employee_tbl"); 
	define("PERMISSION","employee_permission_tbl"); 
	define("GENDRAL_DOCUMENTS","gendral_documents_tbl");  
	define("PROPERTY_BROUCHERS","property_broucher_tbl"); 

	define("REQUEST_TYPE_TBL","request_type_tbl");  
	define("NOTIFICATION_EMAIL_TBL","notification_email_tbl");   
	define("GALLERY_TBL","gallery_tbl"); 
	define("GALLERY_IMAGE","gallery_image_tbl"); 

	/*13.06.2020*/
	define("FLATTYPE_MASTER","flat_type_master"); 
	define("FLATTYPE_MASTER_LIST","flat_type_masterlist"); 

	/*15.06.2020*/
	define("PROPERTY_ROOMS_LIST","property_rooms_list_tbl"); 
	define("PROPERTY_GALLERY","property_gallery_tbl"); 
	define("PROPERTY_DOCUMENTS","property_document_list_tbl"); 
	define("PROPERTY_DOCUMENTS_ITEMS","property_document_list_items"); 
	define("PROPERTY_GENDRAL_DOCUMENTS","property_gendral_documents_tbl"); 

	define("PROPERTY_AREA","property_area_tbl"); 

	define("ADOC_REQUEST","adhoc_request_tbl"); 
	define("REQUEST_LOGS","adhoc_log_tbl"); 

	define("ADOC_IMAGES","adhoc_image_tbl"); 
	
	define("KYC","kyc_tbl"); 
	define("KYC_LIST","kyc_list_tbl");

	define("KYC_TYPE","kyc_type_tbl"); 
	define("MENU_SETTINGS","virtualtour_menusettings_tbl");

	define("INVOICE_TBL","invoice_tbl");
	define("COMPANY_SETTINGS","company_settings_tbl");

	define("ACTIVITY_TYPE","activity_type_tbl");
	define("LEAD_TYPE","lead_type_tbl");
	define("LEAD_TBL","lead_tbl");
	define("ASSIGN_LEAD","assignlead_tbl"); 
	define("LEAD_LOGS","lead_logs_tbl");  
	define("LEAD_ACTIVITY","lead_activity_tbl");
	define("PROJECT_TYPE","project_type_tbl");

	define("TEMPEXCEL_TBL","temp_excel_tbl");
	define("DATASHEET_TBL","datasheet_tbl");

	define("TEMPCUSTOMEREXCEL_TBL","temp_customer_excel_tbl");
	define("CUSTOMERDATASHEET_TBL","customerdatasheet_tbl");

	define("NEWS_TBL","news_tbl");
	define("LEAD_SOURCE","lead_source_tbl");

	define("CUSTOMER_PROFILE","customer_profile_tbl");
	define("DEPARTMENT_TBL","department_tbl");
	define("EMPLOYEE_ROLE","employee_role_tbl");

	define("ASSIGN_ADHOC","assignadhoc_tbl");
	define("ASSIGN_ADHOC_LOGS","adhocassign_logs_tbl");

	
	define("UPLOAD_PAGE_VIDEO","upload_video_tbl");
	define("PROJECT_MASTER","project_master_tbl");


	define("TENANT_TBL","tenant_tbl");
	define("STAGE_MASTER","stage_master_tbl");
	define("PAYMENT_SCHEDULE","property_payment_schedule_tbl");
	define("PAYMENT_SCHEDULE_LIST","property_payment_schedule_list_tbl");


	

	
	
	
	
	

	
	
	
	

?>